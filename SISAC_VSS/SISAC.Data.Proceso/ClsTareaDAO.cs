﻿using SISAC.Data.DAO;
using SISAC.Entidad.Facturacion;
using System;
using System.Collections;
using System.Data;
using System.Threading;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Data.Proceso
{
    public sealed class ClsTareaDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<ClsTareaDAO> _Instancia = new Lazy<ClsTareaDAO>(() => new ClsTareaDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion

        #region Property

        public static ClsTareaDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion

        #region Constructor

        private ClsTareaDAO()
        {

        }

        #endregion

        #region Public

        public int CrearProgramarTarea(ClsTareaProgramacion tareaprogramacion)
        {
            Hashtable htparametros = new Hashtable();
            DateTime fechaUltimaEjecucion;

            try
            {
                #region Codigo de referencia

                //Hashtable param = entidad.Parametros.ToHashtableDAO();
                //DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresion", "ActualizarEstado", param);
                //return table;

                #endregion

                htparametros.Add("@idtarea", 0);
                htparametros.Add("@idempresaSelecc", tareaprogramacion.IdEmpresa);
                htparametros.Add("@idusuariologin", tareaprogramacion.IdUsuarioGenerador);
                htparametros.Add("@idprioridad", tareaprogramacion.IdPrioridad); //Siempre es es 0
                htparametros.Add("@idproceso", tareaprogramacion.IdProceso); //Viene del Servicio "Procesar", es el codigo de proceso
                htparametros.Add("@comandotarea", tareaprogramacion.DevolvercomandoSqlXml());
                //htparametros.Add("@comandotarea", tareaprogramacion.DevolvercomandoSqlXml(tareaprogramacion.ParametroTarea.GetType()));
                htparametros.Add("@idfrecuencia", tareaprogramacion.Frecuencia);
                htparametros.Add("@horainiciotarea", tareaprogramacion.HoraInicio);
                htparametros.Add("@esrecurrente", tareaprogramacion.EsRecurrente);
                htparametros.Add("@fechainicioprogramada", tareaprogramacion.FechaInicioProgramada);
                htparametros.Add("@fechafinprogramada", tareaprogramacion.FechaFinProgramada);
                htparametros.Add("@primerdia", tareaprogramacion.PrimerDia);
                htparametros.Add("@ultimodia", tareaprogramacion.UltimoDia);
                htparametros.Add("@dia", tareaprogramacion.Dia);
                htparametros.Add("@mes", tareaprogramacion.Mes);
                htparametros.Add("@fechafinindefinida", tareaprogramacion.FechaIndefinida);
                htparametros.Add("@intervaloensegundos", tareaprogramacion.IntervaloEnSegundos);
                htparametros.Add("@fechaultimaejecucion", null);
                htparametros.Add("@iduunnselecc", tareaprogramacion.IdUUNN); //CanviaDev_2020-05-27

                if (tareaprogramacion.IntervaloEnSegundos != null)
                {
                    fechaUltimaEjecucion = new DateTime(
                        tareaprogramacion.FechaInicioProgramada.Year,
                        tareaprogramacion.FechaInicioProgramada.Month,
                        tareaprogramacion.FechaInicioProgramada.Day,
                        tareaprogramacion.HoraInicio.Value.Hour,
                        tareaprogramacion.HoraInicio.Value.Minute,
                        tareaprogramacion.HoraInicio.Value.Second);

                    htparametros["@fechaultimaejecucion"] = fechaUltimaEjecucion.AddSeconds((double)(tareaprogramacion.IntervaloEnSegundos * -1));
                }

                _DAO.EjecutarComandoEntidad("tareaprogramacion", "insertardesdeweb", htparametros);

                #region Codigo de referencia

                //clsGestionDato.Instancia.EjecutarComando("tareaprogramacion", "insertar", _htparametros);

                #endregion

                return (int)htparametros["@idtarea"];
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ObtenerDatoServicio(int idproceso, ref string nombreServicio, ref string nombreServidor)
        {
            Hashtable htParametros = new Hashtable();
            htParametros.Add("@p_idtarea", idproceso);

            DataTable dtResultado = _DAO.EjecutarComandoEntidad("proceso", "obtenerdatoservicio", htParametros);

            nombreServicio = dtResultado.Rows[0]["ServicioTareaNGC"].ToString();
            nombreServidor = dtResultado.Rows[0]["ServidorTareaNGC"].ToString();
        }

        public void EliminarTarea(int idtarea)
        {
            Hashtable htParametros = new Hashtable();
            htParametros.Add("@p_idtarea", idtarea);

            #region Codigo referencia

            //DataTable dtResultado = _DAO.EjecutarComandoEntidad("tareaprogramacion", "eliminar", htParametros);

            #endregion

            _DAO.EjecutarComandoEntidad("tareaprogramacion", "eliminar", htParametros);
        }

        #endregion

    }
}
