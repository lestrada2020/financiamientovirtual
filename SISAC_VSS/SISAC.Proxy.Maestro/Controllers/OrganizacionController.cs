﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace SISAC.Proxy.Maestro.Controllers
{
    public class OrganizacionController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteMaestro";
                String controller = "Organizacion";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}/{3}", site, controller, action, param);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        [HttpGet]
        [Route("Api/Unidad_Negocio/Index/{id}")]
        public IHttpActionResult ListarUUNNxIdEmpresa(String id)
        {
            var result = SendData<clsResultadoMaestro>("ListarUUNNxIdEmpresa", id);

            return Ok(result);
        }

        [HttpGet]
        [Route("Api/Centro_Servicio/Index/{id}")]
        public IHttpActionResult ListarCCSSxIdUUNN(String id)
        {
            var result = SendData<clsResultadoMaestro>("ListarCCSSxIdUUNN", id);

            return Ok(result);
        }

        [HttpGet]
        [Route("Api/Sector_Servicio/Index/{id}")]
        public IHttpActionResult ListarSectoresxIdCCSS(String id)
        {
            var result = SendData<clsResultadoMaestro>("ListarSectoresxIdCCSS", id);

            return Ok(result);
        }

        [HttpGet]
        [Route("Api/Ruta_Lectura/Index/{id}")]
        public IHttpActionResult ListarRutasxIdSector(String id)
        {
            var result = SendData<clsResultadoMaestro>("ListarRutasxIdSector", id);

            return Ok(result);
        }

        #endregion Public
    }
}