﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Data.DAO
{
    public sealed class clsDataAccess
    {
        #region Fields

        private static Int32 _TimeOut = 20;
        private static String _KeyConnection = String.Empty;
        private static String _ConnectionString = String.Empty;
        private static Hashtable _Comands = new Hashtable();
        private static readonly Lazy<clsDataAccess> _Instancia = new Lazy<clsDataAccess>(() => new clsDataAccess(), LazyThreadSafetyMode.PublicationOnly);

        private static readonly clsDataAccess _objgestiondato = new clsDataAccess(); //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL

        #endregion Fields

        #region Property

        public static clsDataAccess Instancia
        {
            get { return _Instancia.Value; }
        }
        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        public static clsDataAccess ObtenerInstancia()
        {
            return _objgestiondato;
        }
        public SqlConnection ObtenerConexion()
        {
            //return UtilidadesNG.ObtenerConexion();
            SqlConnection cn = new SqlConnection();

            try
            {
                cn = ObtenerConexion(ObtenerCadenaConexion());
                //cn.InfoMessage += new SqlInfoMessageEventHandler(conexion_InfoMessage);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return cn;
            //el objeto _objconexion es reutilizado y no necesariamente para establecer conexion pasan por obtenerconexion()
        }

        public static SqlConnection ObtenerConexion(String cadenaconexion)
        {
            SqlConnection conexion = new SqlConnection();

            conexion.ConnectionString = cadenaconexion;
            //conexion.InfoMessage += new SqlInfoMessageEventHandler(conexion_InfoMessage);
            return conexion;
        }
        public static String ObtenerCadenaConexion()
        {
            string cadenaconexion = string.Empty;
            //string claveConexionDB = string.Empty;

            if (string.IsNullOrEmpty(cadenaconexion))
            {
                //if (string.IsNullOrEmpty(claveConexionDB))
                // claveConexionDB = ConfigurationManager.AppSettings["claveConexionDB"].ToString();

                cadenaconexion = ConfigurationManager.AppSettings["optimusngdbcsdesarrollo"].ToString();
            }

            return cadenaconexion;
        }
        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        #endregion Property

        #region Constructors

        private clsDataAccess()
        {
            _TimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["tiempoEsperaConexionDB"] ?? "30");
            _KeyConnection = GetKeyConnection(0);
            _ConnectionString = ConfigurationManager.ConnectionStrings[_KeyConnection].ConnectionString;
        }

        #endregion Constructors

        #region Enum

        public enum TypeExecute
        {
            NonQuery,
            Scalar,
            Row,
            Table,
            Set,
            TableNGC
        }

        #endregion Enum

        #region Private

        public DataTable EjecutarComandoBulkData(String entity, String action, Hashtable param, DataTable data, string dataparametro)
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            var table = (DataTable)null;

            connection.Open();

            var sp = $"{entity}_{action}_pa";

            SqlCommand cmd = new SqlCommand(sp, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            var parameters = new List<SqlParameter>();

            string parametro, valor;

            foreach (string key in param.Keys)
            {
                parametro = key;
                valor = param[key].ToString();

                parameters.Add(new SqlParameter(parametro, valor));
            }

            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = "@p_" + dataparametro;
            parameter.SqlDbType = System.Data.SqlDbType.Structured;
            parameter.Value = data;
            parameters.Add(parameter);

            cmd.Parameters.AddRange(parameters.ToArray());

            table = new DataTable();
            table.TableName = entity.ToLower();

            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd)) adapter.Fill(table);

            connection.Close();

            return table;
        }

        private Boolean SetValuesInput(SqlCommand comand, Hashtable param)
        {
            try
            {
                if (comand == null) return false;

                foreach (SqlParameter item in comand.Parameters)
                {
                    if (item.Direction == ParameterDirection.ReturnValue) continue;

                    String field = param.Keys.Cast<String>().FirstOrDefault(s => s.ToLower() == item.ParameterName.ToLower());

                    if (String.IsNullOrWhiteSpace(field))
                    {
                        StringBuilder message = new StringBuilder();
                        message.AppendLine($"Se modifico el objeto: {comand.CommandText} ");
                        message.AppendLine($"Parametro: {item.ParameterName} ");
                        message.AppendLine($"Database: {comand.Connection.Database} ");
                        message.AppendLine($"Server: {comand.Connection.DataSource} ");

                        throw new Exception(message.ToString());
                    }

                    item.Value = param[field];
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Boolean GetValuesOutput(SqlCommand comand, Hashtable param)
        {
            try
            {
                if (comand == null) return false;
                if (param == null) return true;

                foreach (SqlParameter item in comand.Parameters)
                {
                    if (item.Direction == ParameterDirection.Input) continue;

                    var field = param.Keys.Cast<String>().FirstOrDefault(s => s.ToLower() == item.ParameterName.ToLower()) ?? item.ParameterName;

                    param[field] = item.Value;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private SqlCommand GetComand(String entity, String action, Hashtable param, SqlConnection connection, SqlTransaction transaction)
        {
            var keyCmd = "";
            try
            {
                var cmd = (SqlCommand)null;
                var cnn = connection ?? new SqlConnection(_ConnectionString);
                var sp = $"{entity}_{action}_pa";

                keyCmd = $"{cnn.DataSource}.{cnn.Database}.{sp}";

                if (_Comands.Contains(keyCmd))
                {
                    cmd = (SqlCommand)_Comands[keyCmd];
                    cmd.Connection = cnn;
                    cmd.Transaction = transaction ?? cmd.Transaction;
                }
                else
                {
                    cmd = new SqlCommand();

                    cmd.CommandText = sp;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cnn;
                    cmd.Transaction = transaction;
                    cmd.CommandTimeout = _TimeOut;

                    var isConnection = true;
                    var context = (WindowsImpersonationContext)null;

                    if (cmd.Connection.State == ConnectionState.Closed)
                    {
                        if (WindowsIdentity.GetCurrent(true) != null) context = (WindowsIdentity.GetCurrent(true)).Impersonate();

                        isConnection = false;
                        cmd.Connection.Open();
                    }

                    SqlCommandBuilder.DeriveParameters(cmd);

                    if (isConnection == false)
                    {
                        if (context != null) context.Undo();

                        cmd.Connection.Close();
                    }

                    _Comands[keyCmd] = cmd;
                }

                if (cmd.Parameters.Count > 0) SetValuesInput(cmd, param);

                return cmd;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Object ExecuteCommand(String entity, String action, Hashtable param, SqlConnection connection, SqlTransaction transaction, TypeExecute type, params String[] listTableName)
        {
            try
            {
                var result = (Object)null;
                var table = (DataTable)null;
                var command = (SqlCommand)null;
                var isActive = true;
                var context = (WindowsImpersonationContext)null;

                connection = connection ?? new SqlConnection(_ConnectionString);
                command = GetComand(entity, action, param, connection, transaction);

                if (command == null) return null;
                if (command.Connection.State == ConnectionState.Closed)
                {
                    isActive = false;
                    if (WindowsIdentity.GetCurrent(true) != null) context = (WindowsIdentity.GetCurrent(true)).Impersonate();
                    command.Connection.Open();
                }

                #region Type

                switch (type)
                {
                    case TypeExecute.NonQuery:
                        result = command.ExecuteNonQuery();
                        break;

                    case TypeExecute.Scalar:
                        result = command.ExecuteScalar();
                        break;

                    case TypeExecute.Row:
                    case TypeExecute.Table:
                        table = new DataTable();
                        table.TableName = entity.ToLower();
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command)) adapter.Fill(table);
                        result = table;

                        break;

                    case TypeExecute.Set:
                        DataSet set = new DataSet(entity.ToLower());
                        set.Load(command.ExecuteReader(), LoadOption.OverwriteChanges, listTableName);
                        result = set;

                        break;

                    default:
                        result = null;
                        break;
                }

                #endregion Type

                if (!isActive)
                {
                    command.Connection.Close();
                    command.Connection = null;

                    if (context != null) context.Undo();
                }

                if (!GetValuesOutput(command, param)) return null;
                if (type == TypeExecute.Row) result = table.Rows.Count > 0 ? table.Rows[0] : null;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL

        /// <summary>
        /// Ejecuta una sentencia SQL || Abre abre conexión - NO CIERRA CONEXIÓN


        private Object ExecuteCommandSession(String entity, String action, Hashtable param, SqlConnection connection, SqlTransaction transaction, TypeExecute type, params String[] listTableName)
        {
            try
            {

                var result = (Object)null;
                var table = (DataTable)null;
                var command = (SqlCommand)null;
                //var isActive = true;
                //var context = (WindowsImpersonationContext)null;

                connection = connection ?? new SqlConnection(_ConnectionString);
                command = GetComand(entity, action, param, connection, transaction);

                if (command == null) return null;

                #region Type

                switch (type)
                {
                    case TypeExecute.NonQuery:
                        result = command.ExecuteNonQuery();
                        break;

                    case TypeExecute.Scalar:
                        result = command.ExecuteScalar();
                        break;

                    case TypeExecute.Row:
                    case TypeExecute.Table:
                        table = new DataTable();
                        table.TableName = entity.ToLower();
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command)) adapter.Fill(table);
                        result = table;

                        break;

                    case TypeExecute.Set:
                        DataSet set = new DataSet(entity.ToLower());
                        set.Load(command.ExecuteReader(), LoadOption.OverwriteChanges, listTableName);
                        result = set;

                        break;

                    default:
                        result = null;
                        break;
                }

                #endregion Type
                if (!GetValuesOutput(command, param)) return null;
                if (type == TypeExecute.Row) result = table.Rows.Count > 0 ? table.Rows[0] : null;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int ExecuteCommandSession(SqlTransaction transaction, string comandotextosql, bool closeConnection)
        {
            int resultado = 0;
            SqlCommand _objcomandotmp = new SqlCommand(comandotextosql);
            if (transaction.Connection.State == ConnectionState.Closed)
            {
                transaction.Connection.Open();
            }

            _objcomandotmp.Transaction = transaction;
            _objcomandotmp.Connection = transaction.Connection;
            resultado = _objcomandotmp.ExecuteNonQuery();
            if (closeConnection)
            {
                transaction.Connection.Close();
            }

            return resultado;
        }

        public DataSet ExecuteCommandSessionDataSet(SqlTransaction transaction, string comandotextosql, string[] tables)
        {
            DataSet dataSet = new DataSet();
            SqlCommand _objcomandotmp = new SqlCommand(comandotextosql);
            if (transaction.Connection.State == ConnectionState.Closed)
            {
                transaction.Connection.Open();
            }

            _objcomandotmp.Transaction = transaction;
            _objcomandotmp.Connection = transaction.Connection;


            dataSet.Load(_objcomandotmp.ExecuteReader(), LoadOption.OverwriteChanges, tables);
            //resultado = _objcomandotmp.ExecuteNonQuery();

            return dataSet;
        }

        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL

        #endregion Private

        #region Public

        #region Conexion

        public String GetKeyConnection(Int16 typeSite = 0)
        {
            try
            {
                var name = "";

                if (typeSite == 0) name = ConfigurationManager.AppSettings["claveConexionDB"];
                else if (typeSite == 1) name = ConfigurationManager.AppSettings["claveConexionDBAtencion"];
                else if (typeSite == 2) name = ConfigurationManager.AppSettings["claveConexionDBNTCSE"];
                else if (typeSite == 3) name = ConfigurationManager.AppSettings["claveConexionDBSeguridad"];

                if (String.IsNullOrWhiteSpace((String)name)) throw new Exception(String.Format("El archivo config no tiene el key({0}) solicitado.", typeSite));

                return name.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetConnectionString(Int16 typeSite = 0)
        {
            try
            {
                var key = GetKeyConnection(typeSite);
                var result = ConfigurationManager.ConnectionStrings[key].ConnectionString;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlConnection GetSqlConnection(Int16 typeSite = 0)
        {
            try
            {
                var cnn = GetConnectionString(typeSite);
                var result = new SqlConnection(cnn);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Conexion

        #region Ejecutar Comando Query

        public Int32 EjecutarComando(String comandText)
        {
            return EjecutarComando(comandText, String.Empty);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComando(String comandText, String connectionString)
        {
            SqlConnection sqlcon = new SqlConnection();

            if (String.IsNullOrEmpty(connectionString)) sqlcon = new SqlConnection(_ConnectionString);
            else sqlcon = new SqlConnection(connectionString);

            return EjecutarComando(comandText, sqlcon);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComando(String comandText, SqlConnection connection)
        {
            Int32 _intresultado;
            SqlCommand _objcomandotmp = new SqlCommand(comandText);
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp.Connection = connection;

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();

                _objcomandotmp.Connection.Open();
            }

            _intresultado = _objcomandotmp.ExecuteNonQuery();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                if (impersonationContext != null)
                { impersonationContext.Undo(); }
            }

            return _intresultado;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComando(String comandText, SqlTransaction transaction)
        {
            Int32 _intresultado;
            SqlCommand _objcomandotmp = new SqlCommand(comandText);
            Boolean _bolconexionactiva = true;
            WindowsImpersonationContext impersonationContext = null;

            _objcomandotmp.Transaction = transaction;
            _objcomandotmp.Connection = transaction.Connection;

            if (_objcomandotmp.Connection.State == ConnectionState.Closed)
            {
                _bolconexionactiva = false;

                if (WindowsIdentity.GetCurrent(true) != null)
                    impersonationContext = (WindowsIdentity.GetCurrent(true)).Impersonate();

                _objcomandotmp.Connection.Open();
            }

            _intresultado = _objcomandotmp.ExecuteNonQuery();

            if (!_bolconexionactiva)
            {
                _objcomandotmp.Connection.Close();
                _objcomandotmp.Connection = null;

                if (impersonationContext != null) { impersonationContext.Undo(); }
            }

            return _intresultado;
        }

        #endregion Ejecutar Comando Query

        #region Ejecutar Comando SP

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComando(String entity, String action, Hashtable param)
        {
            return EjecutarComando(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComando(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComando(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComando(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return Convert.ToInt32(ExecuteCommand(entity, action, param, connection, null, TypeExecute.NonQuery));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComando(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return Convert.ToInt32(ExecuteCommand(entity, action, param, transaction.Connection, transaction, TypeExecute.NonQuery));
        }

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL

        #region No Close Conexion   

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComandoSession(String entity, String action, Hashtable param)
        {
            return EjecutarComandoSession(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComandoSession(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComandoSession(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComandoSession(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return Convert.ToInt32(ExecuteCommandSession(entity, action, param, connection, null, TypeExecute.NonQuery));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Int32 EjecutarComandoSession(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return Convert.ToInt32(ExecuteCommandSession(entity, action, param, transaction.Connection, transaction, TypeExecute.NonQuery));
        }

        #endregion No Close Conexion

        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        #endregion Ejecutar Comando SP

        #region EjecutarComandoEscalar

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalar(String entity, String action, Hashtable param)
        {
            return EjecutarComandoEscalar(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalar(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComandoEscalar(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalar(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return ExecuteCommand(entity, action, param, connection, null, TypeExecute.Scalar);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalar(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return ExecuteCommand(entity, action, param, transaction.Connection, transaction, TypeExecute.Scalar);
        }

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        #region no close conection
        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalarSession(String entity, String action, Hashtable param)
        {
            return EjecutarComandoEscalarSession(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalarSession(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComandoEscalarSession(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalarSession(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return ExecuteCommandSession(entity, action, param, connection, null, TypeExecute.Scalar);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Object EjecutarComandoEscalarSession(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return ExecuteCommandSession(entity, action, param, transaction.Connection, transaction, TypeExecute.Scalar);
        }
        #endregion no close conection
        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        #endregion EjecutarComandoEscalar

        #region EjecutarComandoRegistro

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistro(String entity, String action, Hashtable param)
        {
            return EjecutarComandoRegistro(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistro(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComandoRegistro(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistro(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return (DataRow)ExecuteCommand(entity, action, param, connection, null, TypeExecute.Row);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistro(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return (DataRow)ExecuteCommand(entity, action, param, transaction.Connection, transaction, TypeExecute.Row);
        }

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        #region  no close conection
        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistroSession(String entity, String action, Hashtable param)
        {
            return EjecutarComandoRegistroSession(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistroSession(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComandoRegistroSession(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistroSession(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return (DataRow)ExecuteCommandSession(entity, action, param, connection, null, TypeExecute.Row);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataRow EjecutarComandoRegistroSession(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return (DataRow)ExecuteCommandSession(entity, action, param, transaction.Connection, transaction, TypeExecute.Row);
        }
        #endregion  no close conection

        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        #endregion EjecutarComandoRegistro

        #region EjecutarComandoEntidad

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidad(String entity, String action, Hashtable param)
        {
            return EjecutarComandoEntidad(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidad(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComandoEntidad(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidad(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return (DataTable)ExecuteCommand(entity, action, param, connection, null, TypeExecute.Table);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidad(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return (DataTable)ExecuteCommand(entity, action, param, transaction.Connection, transaction, TypeExecute.Table);
        }

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        #region no close conection
        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidadSession(String entity, String action, Hashtable param)
        {
            return EjecutarComandoEntidadSession(entity, action, param, (SqlConnection)null);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidadSession(String entity, String action, Hashtable param, String stringConnection)
        {
            return EjecutarComandoEntidadSession(entity, action, param, String.IsNullOrEmpty(stringConnection) ? null : new SqlConnection(stringConnection));
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidadSession(String entity, String action, Hashtable param, SqlConnection connection)
        {
            return (DataTable)ExecuteCommandSession(entity, action, param, connection, null, TypeExecute.Table);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataTable EjecutarComandoEntidadSession(String entity, String action, Hashtable param, SqlTransaction transaction)
        {
            return (DataTable)ExecuteCommandSession(entity, action, param, transaction.Connection, transaction, TypeExecute.Table);
        }

        #endregion no close conection

        //DISTRILUZ 036 FIN FINANCIAMIENTO VISTUAL
        #endregion EjecutarComandoEntidad

        #region EjecutarComandoConjuntoEntidad

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet EjecutarComandoConjuntoEntidad(String entity, String action, Hashtable param, params String[] listTableNames)
        {
            return EjecutarComandoConjuntoEntidad(entity, action, param, (SqlConnection)null, listTableNames);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet EjecutarComandoConjuntoEntidad(String entity, String action, Hashtable param, SqlConnection connection, params String[] listTableNames)
        {
            return (DataSet)ExecuteCommand(entity, action, param, connection, null, TypeExecute.Set, listTableNames);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet EjecutarComandoConjuntoEntidad(String entity, String action, Hashtable param, SqlTransaction transaction, params String[] listTableNames)
        {
            return (DataSet)ExecuteCommand(entity, action, param, transaction.Connection, transaction, TypeExecute.Set, listTableNames);
        }

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL

        #region no close conection
        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet EjecutarComandoConjuntoEntidadSession(String entity, String action, Hashtable param, params String[] listTableNames)
        {
            return EjecutarComandoConjuntoEntidadSession(entity, action, param, (SqlConnection)null, listTableNames);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet EjecutarComandoConjuntoEntidadSession(String entity, String action, Hashtable param, SqlConnection connection, params String[] listTableNames)
        {
            return (DataSet)ExecuteCommandSession(entity, action, param, connection, null, TypeExecute.Set, listTableNames);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public DataSet EjecutarComandoConjuntoEntidadSession(String entity, String action, Hashtable param, SqlTransaction transaction, params String[] listTableNames)
        {
            return (DataSet)ExecuteCommandSession(entity, action, param, transaction.Connection, transaction, TypeExecute.Set, listTableNames);
        }
        #endregion no close conection
        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        #endregion EjecutarComandoConjuntoEntidad

        #region BulkCopy

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void EjecutarBulkCopy(String tableName, DataTable table, Hashtable columns, SqlConnection connection)
        {
            try
            {
                using (SqlBulkCopy bkc = new SqlBulkCopy(connection))
                {
                    bkc.DestinationTableName = tableName;

                    foreach (var key in columns.Keys)
                    {
                        String k = key.ToString();
                        String v = columns[key].ToString();

                        if (!table.Columns.Contains(k)) throw new Exception(String.Format("<<<La columna [{0}] no existe en el archivo.>>>", k));

                        bkc.ColumnMappings.Add(k, v);
                    }

                    bkc.WriteToServer(table);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion BulkCopy

        #region Cache

        public IEnumerable ComandList(String filter)
        {
            try
            {
                var result = (IEnumerable)null;

                if (String.IsNullOrWhiteSpace(filter)) result = _Comands.Keys.Cast<String>();
                else result = _Comands.Keys.Cast<String>().ToList().Where(s => s.ToLower().Contains(filter.ToLower()));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String ComandDelete(String key)
        {
            try
            {
                var result = "";
                var isAll = String.IsNullOrWhiteSpace(key);

                if (isAll) result = "Elimino todo correctamente.";
                else if (_Comands.Contains(key)) result = $"Elimino key: {key}";
                else result = $"No existe key: {key}";

                if (isAll) _Comands.Clear();
                else _Comands.Remove(key);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Cache

        #endregion Public
    }

    public sealed class clsDataAccessAsync
    {
        #region Field

        private static readonly String _ConnectionString = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["claveConexionDB"]].ConnectionString;
        private static readonly Lazy<clsDataAccessAsync> _Instancia = new Lazy<clsDataAccessAsync>(() => new clsDataAccessAsync(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Enum

        private enum TypeExecute
        {
            NonQuery,
            Scalar,
            Table,
            Set
        }

        private enum TypeBD
        {
            OptimusNGC,
            OptimusNGCI
        }

        #endregion Enum

        #region Property

        public static clsDataAccessAsync Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsDataAccessAsync()
        {
        }

        #endregion Constructor

        #region Private

        private Boolean InputValues(SqlCommand command, Hashtable param)
        {
            try
            {
                if (command == null) return false;
                if (param == null) param = new Hashtable();

                foreach (SqlParameter item in command.Parameters)
                {
                    if (item.Direction == ParameterDirection.ReturnValue) continue;

                    var field = param.Keys.Cast<String>().FirstOrDefault(s => s.ToLower() == item.ParameterName.ToLower());

                    if (String.IsNullOrWhiteSpace(field))
                    {
                        var message = new StringBuilder();
                        message.AppendLine($"Se modifico el objeto: {command.CommandText} ");
                        message.AppendLine($"Parametro: {item.ParameterName} ");
                        message.AppendLine($"Database: {command.Connection.Database} ");
                        message.AppendLine($"Server: {command.Connection.DataSource} ");

                        throw new Exception(message.ToString());
                    }

                    item.Value = param[field];
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Boolean OutputValues(SqlCommand command, Hashtable param)
        {
            try
            {
                if (command == null) return false;
                if (param == null) return true;

                foreach (SqlParameter item in command.Parameters)
                {
                    if (item.Direction == ParameterDirection.Input) continue;

                    var field = param.Keys.Cast<String>().FirstOrDefault(s => s.ToLower() == item.ParameterName.ToLower()) ?? item.ParameterName;

                    param[field] = item.Value;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<Object> ExecuteTypeAsync(SqlCommand command, TypeExecute type, params String[] tableNames)
        {
            try
            {
                var result = (Object)null;
                var reader = (SqlDataReader)null;

                switch (type)
                {
                    case TypeExecute.NonQuery:
                        result = await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                        break;

                    case TypeExecute.Scalar:
                        result = await command.ExecuteScalarAsync().ConfigureAwait(false);
                        break;

                    case TypeExecute.Table:
                        reader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                        result = new DataTable();
                        ((DataTable)result).Load(reader);
                        break;

                    case TypeExecute.Set:
                        reader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                        result = new DataSet();
                        ((DataSet)result).Load(reader, LoadOption.OverwriteChanges, tableNames);
                        break;

                    default:
                        result = null;
                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<Object> ExecuteAsync(String entity, String action, Hashtable param, TypeExecute type, String connectionString = "", params String[] tableNames)
        {
            try
            {
                var result = (Object)null;
                var conexion = String.IsNullOrWhiteSpace(connectionString) ? _ConnectionString : connectionString;

                using (var cnn = new SqlConnection(conexion))
                {
                    await cnn.OpenAsync();

                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandText = $"{entity}_{action}_pa";
                        cmd.CommandType = CommandType.StoredProcedure;

                        await Task.Run(() => { SqlCommandBuilder.DeriveParameters(cmd); }).ConfigureAwait(false);

                        InputValues(cmd, param);
                        result = await ExecuteTypeAsync(cmd, type, tableNames).ConfigureAwait(false);
                        OutputValues(cmd, param);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Private

        #region Public

        public async Task<Int32> ExecuteNonQueryAsync(String entity, String action, Hashtable param, String connectionString = "")
        {
            return (Int32)await ExecuteAsync(entity, action, param, TypeExecute.NonQuery, connectionString).ConfigureAwait(false);
        }

        public async Task<Object> ExecuteScalarAsync(String entity, String action, Hashtable param, String connectionString = "")
        {
            return await ExecuteAsync(entity, action, param, TypeExecute.Scalar, connectionString).ConfigureAwait(false);
        }

        public async Task<DataRow> ExecuteRowAsync(String entity, String action, Hashtable param, String connectionString = "")
        {
            var table = (DataTable)await ExecuteAsync(entity, action, param, TypeExecute.Table, connectionString).ConfigureAwait(false);

            return table.Rows.Count > 0 ? table.Rows[0] : null;
        }

        public async Task<DataTable> ExecuteTableAsync(String entity, String action, Hashtable param, String connectionString = "")
        {
            return (DataTable)await ExecuteAsync(entity, action, param, TypeExecute.Table, connectionString).ConfigureAwait(false);
        }

        public async Task<DataSet> ExecuteDataSetAsync(String entity, String action, Hashtable param, String connectionString = "", params String[] tableNames)
        {
            return (DataSet)await ExecuteAsync(entity, action, param, TypeExecute.Set, connectionString, tableNames).ConfigureAwait(false);
        }

        public String GetConnectionString(String appKey)
        {
            try
            {
                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKey)) throw new Exception(String.Format("No existe el Key \"{0}\" en al archivo config.", appKey));

                var key = ConfigurationManager.AppSettings[appKey];
                var cnn = ConfigurationManager.ConnectionStrings[key].ConnectionString;

                return cnn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }
}