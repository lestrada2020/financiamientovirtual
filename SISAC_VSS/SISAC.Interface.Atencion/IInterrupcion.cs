﻿using SISAC.Entidad.Maestro;
using System.Web.Http;

namespace SISAC.Interface.Atencion
{
    public interface IInterrupcion
    {
        IHttpActionResult ObtenerConfiguracionReporte(clsParametro param);

        IHttpActionResult AyudaLista(clsParametro param);

        IHttpActionResult ObtenerMotivoFomatoNotifica(clsParametro param);

        IHttpActionResult ObtenerResumenxUUNNTiempo(clsParametro param);

        IHttpActionResult ObtenerResumenxUUNNAreaTiempo(clsParametro param);

        IHttpActionResult ObtenerResumenxUUNNMotivoTiempo(clsParametro param);

        IHttpActionResult ObtenerResumenxUUNNSEDTiempo(clsParametro param);

        IHttpActionResult ObtenerDetalleSuministros(clsParametro param);

        IHttpActionResult ObtenerDetalleSEDs(clsParametro param);

        IHttpActionResult GenerarTokenReporte(clsParametro param);

        IHttpActionResult ObtenerAtencionesxRecursoInterrupcion(clsParametro param);

        IHttpActionResult LimpiarTablaReporte(clsParametro param);
    }
}