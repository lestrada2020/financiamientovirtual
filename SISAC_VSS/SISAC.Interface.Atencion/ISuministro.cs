﻿using SISAC.Entidad.Maestro;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Interface.Atencion
{
    public interface ISuministro
    {
        Task<IHttpActionResult> DeudaFechaVence(clsParametro param);

        Task<IHttpActionResult> InterrupcionServicio(clsParametro param);

        Task<IHttpActionResult> CortePorDeuda(clsParametro param);

        Task<IHttpActionResult> ConsultarDatos(clsParametro param);

        Task<IHttpActionResult> RegistroInterrupcion(clsParametro param);

        Task<IHttpActionResult> UltimaLectura(clsParametro param);
    }
}