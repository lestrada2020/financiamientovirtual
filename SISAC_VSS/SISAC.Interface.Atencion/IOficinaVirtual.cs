﻿using SISAC.Entidad.Maestro;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Interface.Atencion
{
    public interface IOficinaVirtual
    {
        Task<IHttpActionResult> ConsultaAtencionesSuministro(clsParametro param);

        Task<IHttpActionResult> ConsultaAtencionNroAtencion(clsParametro param);

        Task<IHttpActionResult> RegistrarContactanos(clsParametro param);

        Task<IHttpActionResult> ListarInterrupciones(clsParametro param);

        Task<IHttpActionResult> ConfirmarEmail(clsParametro param);

        Task<IHttpActionResult> RegistrarReclamo(clsParametro param);

        Task<IHttpActionResult> InfoSuministro(clsParametro param);

        Task<IHttpActionResult> ObtenerObservacion(clsParametro param);

        Task<IHttpActionResult> DesasociarSuministro(clsParametro param);

        Task<IHttpActionResult> CambiarPassword(clsParametro param);

        Task<IHttpActionResult> ListarByNroAtencion(clsParametro param);

        Task<IHttpActionResult> RegistrarFotoLec(clsParametro param);

        Task<IHttpActionResult> VisaListFailed(clsParametro param);

        Task<IHttpActionResult> VisaSendMailPayFailed(clsParametro param);

        #region Cliente Empresa

        IHttpActionResult ValidaAccesoClienteEmpresa(clsParametro param);

        IHttpActionResult RegistrarClienteEmpresaAdmin(clsParametro param);

        IHttpActionResult BuscarPorIdentidad(clsParametro param);

        IHttpActionResult ListarClienteEmpresa(clsParametro param);

        IHttpActionResult ListarRoles(clsParametro param);

        IHttpActionResult TipoRelacion(clsParametro param);

        IHttpActionResult RegistrarClienteEmpresa(clsParametro param);

        IHttpActionResult EditarClienteEmpresa(clsParametro param);

        IHttpActionResult EliminarClienteEmpresa(clsParametro param);

        IHttpActionResult ListarTarifaActiva(clsParametro param);

        IHttpActionResult ListarSuministros(clsParametro param);

        IHttpActionResult ListarSuministrosDetallePago(clsParametro param);

        IHttpActionResult EliminarSuministroAsociado(clsParametro param);

        IHttpActionResult BuscarSuministroAsociar(clsParametro param);

        IHttpActionResult RegistrarSuministroAsociar(clsParametro param);

        IHttpActionResult ListarSuministrosCarga(clsParametroBulkData param);

        #endregion Cliente Empresa

        Task<IHttpActionResult> ObtenerFile(clsParametro param);

        #region Beneficiario

        Task<IHttpActionResult> ObtenerBeneficiario(clsParametro param);

        Task<IHttpActionResult> RegistrarBeneficiario(clsParametro param);

        #endregion Beneficiario

        Task<IHttpActionResult> RegistrarUsuario(clsParametro param);

        Task<IHttpActionResult> ActivarCuentaXsms(clsParametro param);

        Task<IHttpActionResult> ReenviarSMS(clsParametro param);

        Task<IHttpActionResult> ObtenerDatosConfirmacion(clsParametro param);

        Task<IHttpActionResult> SolicitarNewPassword(clsParametro param);

        Task<IHttpActionResult> DatosNroServicio(clsParametro param);

        Task<IHttpActionResult> ListarNroServicio(clsParametro param);

        Task<IHttpActionResult> LecturaRegistrar(clsParametro param);

        Task<IHttpActionResult> AyudaLista(clsParametro param);

        Task<IHttpActionResult> ListAppConfig(clsParametro param);

        #region Cobranza

        Task<IHttpActionResult> ConsultarDeudaDetalle(clsParametro param);

        Task<IHttpActionResult> GrabarIntentoDetalle(clsParametro param);

        Task<IHttpActionResult> ObtenerIntento(clsParametro param);

        #endregion Cobranza

        #region Bono Electrico

        Task<IHttpActionResult> ObtenerBonoElectrico(clsParametro param);

        Task<IHttpActionResult> ObtenerBonoElectricoDNI(clsParametro param);

        #endregion Bono Electrico

    }
}