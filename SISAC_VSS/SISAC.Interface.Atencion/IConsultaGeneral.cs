﻿using SISAC.Entidad.Atencion;
using System.Web.Http;

namespace SISAC.Interface.Atencion
{
    public interface IConsultaGeneral
    {
        #region Busqueda

        IHttpActionResult BuscarBySuministro(clsConsultaGeneralFiltro param);

        IHttpActionResult BuscarByNroDocumento(clsConsultaGeneralFiltro param);

        IHttpActionResult BuscarByNombre(clsConsultaGeneralFiltro param);

        IHttpActionResult BuscarByRecibo(clsConsultaGeneralFiltro param);

        #endregion Busqueda

        #region Datos

        IHttpActionResult DatosPago(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosRecibo(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosReclamo(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosMedidor(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosRefacfurado(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosFacturacion(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosTecnico(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosConsumo(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosSuministro(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosContrato(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosDeuda(clsConsultaGeneralFiltro param);

        IHttpActionResult DatosComercial(clsConsultaGeneralFiltro param);

        #endregion Datos

        #region NroServicio

        IHttpActionResult Resumen(clsConsultaGeneralFiltro param);

        IHttpActionResult Basico(clsConsultaGeneralFiltro param);

        IHttpActionResult Restriccion(clsConsultaGeneralFiltro param);

        IHttpActionResult CtaCte(clsConsultaGeneralFiltro param);

        IHttpActionResult Facturacion(clsConsultaGeneralFiltro param);

        IHttpActionResult FacturacionConcepto(clsConsultaGeneralFiltro param);

        IHttpActionResult FacturacionDocumento(clsConsultaGeneralFiltro param);

        IHttpActionResult ReciboBasico(clsConsultaGeneralFiltro param);

        IHttpActionResult ReciboMedidor(clsConsultaGeneralFiltro param);

        IHttpActionResult ReciboLectura(clsConsultaGeneralFiltro param);

        IHttpActionResult ReciboConcepto(clsConsultaGeneralFiltro param);

        IHttpActionResult Atencion(clsConsultaGeneralFiltro param);

        IHttpActionResult FotoLectura(clsConsultaGeneralFiltro param);

        IHttpActionResult Lectura(clsConsultaGeneralFiltro param);

        IHttpActionResult AFavor(clsConsultaGeneralFiltro param);

        IHttpActionResult Pagos(clsConsultaGeneralFiltro param);

        IHttpActionResult TecnicoBasico(clsConsultaGeneralFiltro param);

        IHttpActionResult TecnicoMagnitud(clsConsultaGeneralFiltro param);

        IHttpActionResult TecnicoPrecinto(clsConsultaGeneralFiltro param);

        IHttpActionResult TecnicoTransformador(clsConsultaGeneralFiltro param);

        IHttpActionResult OrdenTrabajo(clsConsultaGeneralFiltro param);

        IHttpActionResult FotoActividad(clsConsultaGeneralFiltro param);

        IHttpActionResult Foto(clsConsultaGeneralFiltro param);

        IHttpActionResult MedidorBasico(clsConsultaGeneralFiltro param);

        IHttpActionResult MedidorLista(clsConsultaGeneralFiltro param);

        IHttpActionResult MedidorLecturas(clsConsultaGeneralFiltro param);

        IHttpActionResult Cambios(clsConsultaGeneralFiltro param);

        IHttpActionResult ConvenioLista(clsConsultaGeneralFiltro param);

        IHttpActionResult ConvenioRegistro(clsConsultaGeneralFiltro param);

        IHttpActionResult ConvenioCreacion(clsConsultaGeneralFiltro param);

        IHttpActionResult ConvenioDocumentos(clsConsultaGeneralFiltro param);

        IHttpActionResult ConvenioGarantias(clsConsultaGeneralFiltro param);

        IHttpActionResult ConvenioAmortizaciones(clsConsultaGeneralFiltro param);

        IHttpActionResult ConvenioLetra(clsConsultaGeneralFiltro param);

        #endregion NroServicio
    }
}