﻿using SISAC.Entidad.Maestro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Interface.Atencion
{
    public interface IFinanciamiento
    {
        IHttpActionResult ListarSuministroAsociado(clsParametro param);
        IHttpActionResult ObtenerParametrosConvenio(clsParametro param);
        IHttpActionResult ObtenerDetalleFinanciamiento(clsParametro param);
        IHttpActionResult ObtenerConveniosPorEmpresa(clsParametro param);
        IHttpActionResult ObtenerConvenioPorId(clsParametro param);
        IHttpActionResult ObtenerDetalleDeuda(clsParametro param);
        IHttpActionResult ObtenerConfiguracionGlobal(clsParametro param);
        IHttpActionResult ObtenerDetalleOrdenCobro(clsParametro param);
        IHttpActionResult ObtenerCabeceraOrdenCobro(clsParametro param);
        IHttpActionResult GrabarIntentoDetalle(clsParametro param);
        IHttpActionResult ObtenerMetodosPago(clsParametro param);
        IHttpActionResult ObtenerListaPorOrdenCobro(clsParametro param);
        IHttpActionResult PagarDeuda(clsParametro param);

        IHttpActionResult GenerarOrdenCobro(clsParametro param);
        IHttpActionResult ObtenerDocumentoComprobantePago(clsParametro param);

        IHttpActionResult GenerarDocumentoTransaccionExtrajudicial(clsParametro param);

        Task<IHttpActionResult> VisaListFailed(clsParametro param);

        IHttpActionResult ActualizarMetodoPagoOrdenVisa(clsParametro param);

        IHttpActionResult EnviarEmail(clsParametro param);
        IHttpActionResult FinanciamientoEnviarEmailError(clsParametro param);

        IHttpActionResult EstadoPagoDistriluz(clsParametro param);

    }
}
