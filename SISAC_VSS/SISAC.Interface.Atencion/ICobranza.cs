﻿using SISAC.Entidad.Maestro;
using System.Web.Http;

namespace SISAC.Interface.Atencion
{
    public interface ICobranza
    {
        IHttpActionResult ConsultarDeuda(clsParametro param);

        IHttpActionResult PagarDeuda(clsParametro param);

        IHttpActionResult GrabarIntento(clsParametro param);

        IHttpActionResult ActualizarIntento(clsParametro param);

        IHttpActionResult ObtenerPagoResumen(clsParametro param);

        IHttpActionResult EnviarEmail(clsParametro param);

        IHttpActionResult GrabarIntentoEmpresa(clsParametroBulkData param);

        IHttpActionResult PagarDeudaEmpresa(clsParametro param);

        IHttpActionResult ListarBancos(clsParametro param);

        IHttpActionResult ListarCuentas(clsParametro param);

        IHttpActionResult ActualizarDepositoBancarioEstado(clsParametro param);

        IHttpActionResult GrabarDepositoBancarioIntento(clsParametroBulkData param);

        IHttpActionResult CobranzaOrdenVisaNET(clsParametro param);
    }
}