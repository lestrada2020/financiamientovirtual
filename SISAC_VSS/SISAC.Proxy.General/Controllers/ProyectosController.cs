﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.General;
using SISAC.Util.Tool;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.General.Controllers
{
    public class ProyectosController : ApiController, IProyectos
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly String _SiteProy = clsUtil.Instancia.GetValueOfConfig("SiteGeneral");

        #endregion Field

        #region Private

        private async Task<clsResultado> SendAsync(String action, Object param)
        {
            try
            {
                var url = String.Format("{0}/Api/Proyectos/{1}", _SiteProy, action);

                return await _Service.PostAsync<clsResultado>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> AyudaLista(clsParametro param)
        {
            var result = await SendAsync("AyudaLista", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListIniciativas(clsParametro param)
        {
            var result = await SendAsync("ListIniciativas", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerNivelesFase(clsParametro param)
        {
            var result = await SendAsync("ObtenerNivelesFase", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerUsuario(clsParametro param)
        {
            var result = await SendAsync("ObtenerUsuario", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerIniciativa(clsParametro param)
        {
            var result = await SendAsync("ObtenerIniciativa", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarHito(clsParametro param)
        {
            var result = await SendAsync("RegistrarHito", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerHito(clsParametro param)
        {
            var result = await SendAsync("ObtenerHito", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ExportarIniciativa(clsParametro param)
        {
            var result = await SendAsync("ExportarIniciativa", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ExportarImporte(clsParametro param)
        {
            var result = await SendAsync("ExportarImporte", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarImporte(clsParametro param)
        {
            var result = await SendAsync("RegistrarImporte", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerImporte(clsParametro param)
        {
            var result = await SendAsync("ObtenerImporte", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> EditarIniciativa(clsParametro param)
        {
            var result = await SendAsync("EditarIniciativa", param);

            return Ok(result);
        }

        #endregion Public
    }
}