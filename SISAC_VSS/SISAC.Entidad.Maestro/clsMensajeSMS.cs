﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SISAC.Entidad.Maestro
{
    /// <summary>
    /// Entidad MensajeSMS
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsMensajeSMS
    {
        #region Campos

        private Int32 _intidmensajesms;
        private Int16 _intidareatiposms;
        private Int16 _intidempresa;
        private Int16? _intiduunn;
        private Int16? _intidccss;
        private Int16? _intidciclo;
        private Int16? _intidsector;
        private Int16? _intidestrato;
        private String _strcartera;
        private Int32? _intidnroservicio;
        private String _strmensaje;
        private Int32? _intperiodo;
        private Boolean _bolesprogramable;
        private Int16? _intnroveces;
        private DateTime _datfechaenvio1;
        private DateTime _dathoraenvio1;
        private DateTime? _datfechaenvio2;
        private DateTime? _dathoraenvio2;
        private DateTime? _datfechaenvio3;
        private DateTime? _dathoraenvio3;
        private Int32? _intidelementoelectrico;
        private Int16 _intidestado;
        private Int32 _intperiodoinicio;
        private Int32 _intperiodofin;
        private Int16 _intidactividad;

        private Int16? _intvisualizasector;
        private Int16 _intalcance;
        private Int16 _inttienecronograma;
        private Int16 _inttipobusqueda;

        private Int16 _intidimportacionnroservicio;
        private Int32 _intidtiposms;
        private Int16 _intarchivoincluyesms;

        private DateTime _datfechaenviohasta1;
        private DateTime? _datfechaenviohasta2;
        private DateTime? _datfechaenviohasta3;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int32 IdMensajeSMS
        {
            get { return _intidmensajesms; }
            set { _intidmensajesms = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdAreaTipoSMS
        {
            get { return _intidareatiposms; }
            set { _intidareatiposms = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? IdCCSS
        {
            get { return _intidccss; }
            set { _intidccss = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? IdCiclo
        {
            get { return _intidciclo; }
            set { _intidciclo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? IdSector
        {
            get { return _intidsector; }
            set { _intidsector = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? IdEstrato
        {
            get { return _intidestrato; }
            set { _intidestrato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Cartera
        {
            get { return _strcartera; }
            set { _strcartera = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Mensaje
        {
            get { return _strmensaje; }
            set { _strmensaje = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Boolean EsProgramable
        {
            get { return _bolesprogramable; }
            set { _bolesprogramable = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? NroVeces
        {
            get { return _intnroveces; }
            set { _intnroveces = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaEnvio1
        {
            get { return _datfechaenvio1; }
            set { _datfechaenvio1 = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime HoraEnvio1
        {
            get { return _dathoraenvio1; }
            set { _dathoraenvio1 = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaEnvio2
        {
            get { return _datfechaenvio2; }
            set { _datfechaenvio2 = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? HoraEnvio2
        {
            get { return _dathoraenvio2; }
            set { _dathoraenvio2 = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaEnvio3
        {
            get { return _datfechaenvio3; }
            set { _datfechaenvio3 = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? HoraEnvio3
        {
            get { return _dathoraenvio3; }
            set { _dathoraenvio3 = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 PeriodoInicio
        {
            get { return _intperiodoinicio; }
            set { _intperiodoinicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 PeriodoFin
        {
            get { return _intperiodofin; }
            set { _intperiodofin = value; }
        }

        public Int16? VisualizaSector
        {
            get { return _intvisualizasector; }
            set { _intvisualizasector = value; }
        }

        public Int16 Alcance
        {
            get { return _intalcance; }
            set { _intalcance = value; }
        }

        public Int16 IdActividad
        {
            get { return _intidactividad; }
            set { _intidactividad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? IdElementoElectrico
        {
            get { return _intidelementoelectrico; }
            set { _intidelementoelectrico = value; }
        }

        public Int16 TieneCronograma
        {
            get { return _inttienecronograma; }
            set { _inttienecronograma = value; }
        }

        public Int16 TipoBusqueda
        {
            get { return _inttipobusqueda; }
            set { _inttipobusqueda = value; }
        }

        public Int16 ImportacionNroServicio
        {
            get { return _intidimportacionnroservicio; }
            set { _intidimportacionnroservicio = value; }
        }

        public Int32 IdTipoSMS
        {
            get { return _intidtiposms; }
            set { _intidtiposms = value; }
        }

        public Int16 ArchivoIncluyeSMS
        {
            get { return _intarchivoincluyesms; }
            set { _intarchivoincluyesms = value; }
        }

        public DateTime FechaEnvioHasta1
        {
            get { return _datfechaenviohasta1; }
            set { _datfechaenviohasta1 = value; }
        }

        public DateTime? FechaEnvioHasta2
        {
            get { return _datfechaenviohasta2; }
            set { _datfechaenviohasta2 = value; }
        }

        public DateTime? FechaEnvioHasta3
        {
            get { return _datfechaenviohasta3; }
            set { _datfechaenviohasta3 = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsMensajeSMS()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsMensajeSMS(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idmensajesms")) IdMensajeSMS = dr["idmensajesms"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmensajesms"]);
            if (dr.Table.Columns.Contains("idareatiposms")) IdAreaTipoSMS = dr["idareatiposms"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idareatiposms"]);
            if (dr.Table.Columns.Contains("idempresa")) IdEmpresa = dr["idempresa"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idempresa"]);
            if (dr.Table.Columns.Contains("iduunn")) IdUUNN = dr["iduunn"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["iduunn"]);
            if (dr.Table.Columns.Contains("idccss")) IdCCSS = dr["idccss"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idccss"]);
            if (dr.Table.Columns.Contains("idciclo")) IdCiclo = dr["idciclo"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idciclo"]);
            if (dr.Table.Columns.Contains("idsector")) IdSector = dr["idsector"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idsector"]);
            if (dr.Table.Columns.Contains("idestrato")) IdEstrato = dr["idestrato"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idestrato"]);
            if (dr.Table.Columns.Contains("cartera")) Cartera = dr["cartera"] == DBNull.Value ? String.Empty : dr["cartera"].ToString();
            if (dr.Table.Columns.Contains("idnroservicio")) IdNroServicio = dr["idnroservicio"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idnroservicio"]);
            if (dr.Table.Columns.Contains("mensaje")) Mensaje = dr["mensaje"] == DBNull.Value ? String.Empty : dr["mensaje"].ToString();
            if (dr.Table.Columns.Contains("periodo")) Periodo = dr["periodo"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["periodo"]);
            if (dr.Table.Columns.Contains("esprogramable")) EsProgramable = dr["esprogramable"] == DBNull.Value ? false : Convert.ToBoolean(dr["esprogramable"]);
            if (dr.Table.Columns.Contains("nroveces")) NroVeces = dr["nroveces"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["nroveces"]);
            if (dr.Table.Columns.Contains("fechaenvio1")) FechaEnvio1 = dr["fechaenvio1"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(dr["fechaenvio1"]);
            if (dr.Table.Columns.Contains("horaenvio1")) HoraEnvio1 = dr["horaenvio1"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(dr["horaenvio1"]);
            if (dr.Table.Columns.Contains("fechaenvio2")) FechaEnvio2 = dr["fechaenvio2"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechaenvio2"]);
            if (dr.Table.Columns.Contains("horaenvio2")) HoraEnvio2 = dr["horaenvio2"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["horaenvio2"]);
            if (dr.Table.Columns.Contains("fechaenvio3")) FechaEnvio3 = dr["fechaenvio3"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechaenvio3"]);
            if (dr.Table.Columns.Contains("horaenvio3")) HoraEnvio3 = dr["horaenvio3"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["horaenvio3"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idestado"]);
            if (dr.Table.Columns.Contains("visualizasector")) VisualizaSector = dr["visualizasector"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["visualizasector"]);
            if (dr.Table.Columns.Contains("alcance")) Alcance = dr["alcance"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["alcance"]);
            if (dr.Table.Columns.Contains("idactividad")) IdActividad = dr["idactividad"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idactividad"]);
            if (dr.Table.Columns.Contains("idelementoelectrico")) IdElementoElectrico = dr["idelementoelectrico"] == DBNull.Value ? (Int32?)null : Convert.ToInt32(dr["idelementoelectrico"]);
            if (dr.Table.Columns.Contains("importacionnroservicio")) ImportacionNroServicio = dr["importacionnroservicio"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["importacionnroservicio"]);
            if (dr.Table.Columns.Contains("idtiposms")) IdTipoSMS = dr["idtiposms"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idtiposms"]);
            if (dr.Table.Columns.Contains("archivoincluyesms")) ArchivoIncluyeSMS = dr["archivoincluyesms"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["archivoincluyesms"]);
            if (dr.Table.Columns.Contains("fechaenviohasta1")) FechaEnvioHasta1 = dr["fechaenviohasta1"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(dr["fechaenviohasta1"]);
            if (dr.Table.Columns.Contains("fechaenviohasta2")) FechaEnvioHasta2 = dr["fechaenviohasta2"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechaenviohasta2"]);
            if (dr.Table.Columns.Contains("fechaenviohasta3")) FechaEnvioHasta3 = dr["fechaenviohasta3"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(dr["fechaenviohasta3"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista clsListaMensajeSMS
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaMensajeSMS
    {
        #region Campos

        private List<clsMensajeSMS> _objelementos = new List<clsMensajeSMS>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsMensajeSMS> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaMensajeSMS()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaMensajeSMS(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsMensajeSMS(drw));
            }
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad clsNroServicioMensajeSMS
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsNroServicioMensajeSMS
    {
        #region Campos

        private Int32 _intidnroserviciomensajesms;
        private Int32 _intidmensajesms;
        private Int32 _intidnroservicio;
        private Int16 _intidestado;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroServicioMensajeSMS
        {
            get { return _intidnroserviciomensajesms; }
            set { _intidnroserviciomensajesms = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdMensajeSMS
        {
            get { return _intidmensajesms; }
            set { _intidmensajesms = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsNroServicioMensajeSMS()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsNroServicioMensajeSMS(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idnroserviciomensajesms")) IdNroServicioMensajeSMS = dr["idnroserviciomensajesms"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idnroserviciomensajesms"]);
            if (dr.Table.Columns.Contains("idmensajesms")) IdMensajeSMS = dr["idmensajesms"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmensajesms"]);
            if (dr.Table.Columns.Contains("idnroservicio")) IdNroServicio = dr["idnroservicio"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idnroservicio"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idestado"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista clsListaNroServicioMensajeSMS
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaNroServicioMensajeSMS
    {
        #region Campos

        private List<clsNroServicioMensajeSMS> _objelementos = new List<clsNroServicioMensajeSMS>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsNroServicioMensajeSMS> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaNroServicioMensajeSMS()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaNroServicioMensajeSMS(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsNroServicioMensajeSMS(drw));
            }
        }

        #endregion Constructor
    }

    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsSendSMSRespuesta
    {
        [JsonProperty("success")]
        public string success { get; set; }

        [JsonProperty("message_id")]
        public string message_id { get; set; }
    }

    //Clase para envio de mensajes SMS
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsSendSMS
    {
        #region Atributos

        private string _struserid;
        private string _strpwd;
        private string _struseridapisms;
        private string _strpwdapisms;
        private string _strurl;
        private string _strapisms;
        private string _strapiversion;
        private clsSendSMSRespuesta _osendsmsrpta;
        private string _strdescripcionexcepcion;
        private string _strprefixcelular;
        private short _intEsCfgEXternaSMS;
        private short _intIdMetodoEnvioSMS;
        private short _intUsaSSL;
        private short _intUsaOperadorSMS;
        private short _intIdOperadorSMS;
        private ClsMensajeSMSRptaOperadorExterno _oSendSMSRptaOperadorExterno; //Req-002 - Operador SMS (Mod. por: ECALDAS CANVIA)
        //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
        private string _strRequest;
        private string _strResponse;
        private string _strHeaderRequest;
        private string _strBodyRequest;
        //

        #endregion Atributos

        #region Propiedades

        public string UserId
        {
            get { return _struserid; }
            set { _struserid = value; }
        }

        public string Pwd
        {
            get { return _strpwd; }
            set { _strpwd = value; }
        }

        public string UserIdApiSMS
        {
            get { return _struseridapisms; }
            set { _struseridapisms = value; }
        }

        public string PwdApiSMS
        {
            get { return _strpwdapisms; }
            set { _strpwdapisms = value; }
        }

        public string Url
        {
            get { return _strurl; }
            set { _strurl = value; }
        }

        public string ApiSMS
        {
            get { return _strapisms; }
            set { _strapisms = value; }
        }

        public string ApiVersion
        {
            get { return _strapiversion; }
            set { _strapiversion = value; }
        }

        public string PrefijoCelular
        {
            get { return _strprefixcelular; }
            set { _strprefixcelular = value; }
        }

        public clsSendSMSRespuesta SendSMSRpta
        {
            get { return _osendsmsrpta; }
            set { _osendsmsrpta = value; }
        }

        public string DescripcionExcepcion
        {
            get { return _strdescripcionexcepcion; }
            set { _strdescripcionexcepcion = value; }
        }

        public short EsCfgEXternaSMS
        {
            get { return _intEsCfgEXternaSMS; }
            set { _intEsCfgEXternaSMS = value; }
        }

        public short IdMetodoEnvioSMS
        {
            get { return _intIdMetodoEnvioSMS; }
            set { _intIdMetodoEnvioSMS = value; }
        }

        public short UsaSSL
        {
            get { return _intUsaSSL; }
            set { _intUsaSSL = value; }
        }

        public short UsaOperadorSMS
        {
            get { return _intUsaOperadorSMS; }
            set { _intUsaOperadorSMS = value; }
        }

        public short IdOperadorSMS
        {
            get { return _intIdOperadorSMS; }
            set { _intIdOperadorSMS = value; }
        }

        //Req-002 - Operador SMS (Mod. por: ECALDAS CANVIA)
        public ClsMensajeSMSRptaOperadorExterno SendSMSRptaOperadorExterno
        {
            get { return _oSendSMSRptaOperadorExterno; }
            set { _oSendSMSRptaOperadorExterno = value; }
        }
        //

        //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
        public string Request
        {
            get { return _strRequest; }
            set { _strRequest = value; }
        }

        public string Response
        {
            get { return _strResponse; }
            set { _strResponse = value; }
        }

        public string HeaderRequest
        {
            get { return _strHeaderRequest; }
            set { _strHeaderRequest = value; }
        }

        public string BodyRequest
        {
            get { return _strBodyRequest; }
            set { _strBodyRequest = value; }
        }
        //

        #endregion Propiedades

        #region Constructor

        public clsSendSMS()
        {
            //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
            _strRequest = string.Empty;
            _strResponse = string.Empty;
            _strHeaderRequest = string.Empty;
            _strBodyRequest = string.Empty;
            //
        }

        #endregion Constructor

        #region Metodos Publicos

        public String EviarMensajeSMS(string nroComunicacion, string mensajeTexto, string canalsms)
        {
            Stream streamFile = null;
            StreamReader streamreader = null;
            string stResponse = "";
            HttpWebResponse httpResponse = null;
            try
            {
                //nroComunicacion = _strprefixcelular + nroComunicacion.Replace(_strprefixcelular, "").Replace("+", "").Trim();
                if (nroComunicacion.StartsWith(_strprefixcelular))
                {
                    nroComunicacion = nroComunicacion.Substring(_strprefixcelular.Length);
                }
                nroComunicacion = _strprefixcelular + nroComunicacion.Replace("+", "").Trim();

                UriBuilder urlBuilder = new UriBuilder();
                urlBuilder.Host = _strurl.EndsWith("/") ? _strurl.Substring(0, _strurl.Length - 1) : _strurl; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                urlBuilder.Query = string.Format("cmd=" + _strapisms + "&username=" + _struseridapisms + "&password=" + _strpwdapisms + "&content=" + mensajeTexto + "&destination=" + nroComunicacion + "&api_version=" + _strapiversion + "&channel=" + canalsms);

                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(urlBuilder.ToString()));
                _strRequest = httpReq.RequestUri.ToString(); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                httpReq.Credentials = new NetworkCredential(_struserid, _strpwd);
                httpReq.Proxy = null;
                httpResponse = (HttpWebResponse)(httpReq.GetResponse());

                streamFile = httpResponse.GetResponseStream();
                streamreader = new StreamReader(streamFile);
                stResponse = streamreader.ReadToEnd();

                _strResponse = stResponse; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                _osendsmsrpta = JsonConvert.DeserializeObject<clsSendSMSRespuesta>(stResponse);

                return stResponse;
            }
            catch (Exception ex)
            {
                _strdescripcionexcepcion = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        public String EviarMensajeSMS(string nroComunicacion, string mensajeTexto, string canalsms, ref string peticion)
        {
            Stream streamFile = null;
            StreamReader streamreader = null;
            string stResponse = "";
            HttpWebResponse httpResponse = null;
            try
            {
                //nroComunicacion = _strprefixcelular + nroComunicacion.Replace(_strprefixcelular, "").Replace("+", "").Trim();
                if (nroComunicacion.StartsWith(_strprefixcelular))
                {
                    nroComunicacion = nroComunicacion.Substring(_strprefixcelular.Length);
                }
                nroComunicacion = _strprefixcelular + nroComunicacion.Replace("+", "").Trim();

                UriBuilder urlBuilder = new UriBuilder();
                urlBuilder.Host = _strurl.EndsWith("/") ? _strurl.Substring(0, _strurl.Length - 1) : _strurl; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                urlBuilder.Query = string.Format("cmd=" + _strapisms + "&username=" + _struseridapisms + "&password=" + _strpwdapisms + "&content=" + mensajeTexto + "&destination=" + nroComunicacion + "&api_version=" + _strapiversion + "&channel=" + canalsms);

                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(urlBuilder.ToString()));
                peticion = httpReq.RequestUri.ToString();
                _strRequest = peticion; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                httpReq.Credentials = new NetworkCredential(_struserid, _strpwd);
                httpReq.Proxy = null;
                httpResponse = (HttpWebResponse)(httpReq.GetResponse());

                streamFile = httpResponse.GetResponseStream();
                streamreader = new StreamReader(streamFile);
                stResponse = streamreader.ReadToEnd();

                _strResponse = stResponse; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                _osendsmsrpta = JsonConvert.DeserializeObject<clsSendSMSRespuesta>(stResponse);

                return stResponse;
            }
            catch (Exception ex)
            {
                _strdescripcionexcepcion = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        //Req-002 - Operador SMS (Mod. por: ECALDAS CANVIA)
        #region Nuevos metodos para soportar proveedores externos de envio de mensajes SMS

        #region Metodos GET

        public string EnviarMensajeSMS(string nroComunicacion, string mensajeTexto, short usaSSL)
        {
            string stResponse = "";

            //Req-002 - Operador SMS - Observaciones 2 (Mod. por: ECALDAS CANVIA)
            if (usaSSL == 1)
            {
                //Intentar establecer el protocolo de seguridad, en caso no se soporte o se produja un error se 
                //continua con el flujo. En la invocacion al Servicio respectivo se producirá la Excepcion.

                try
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.DefaultConnectionLimit = 9999;
                    ServicePointManager.SecurityProtocol =
                        (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;

                    // Skip validation of SSL/TLS certificate
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }
                catch (Exception)
                { }
            }
            //

            try
            {
                if (nroComunicacion.StartsWith(_strprefixcelular))
                {
                    nroComunicacion = nroComunicacion.Substring(_strprefixcelular.Length);
                }

                nroComunicacion = nroComunicacion.Replace("+", "").Trim();
                UriBuilder urlBuilder = new UriBuilder();
                if (usaSSL == 1)
                {
                    urlBuilder.Scheme = "https";
                }
                urlBuilder.Host = _strurl.EndsWith("/") ? _strurl.Substring(0, _strurl.Length - 1) : _strurl; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                urlBuilder.Query = string.Format("country={0}&message={1}&messageFormat={2}&mobile={3}",
                    _strprefixcelular, mensajeTexto, 1, nroComunicacion);

                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(urlBuilder.ToString()));
                _strRequest = httpReq.RequestUri.ToString(); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                httpReq.Proxy = null;

                string authorization =
                    Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_struseridapisms + ":" + _strpwdapisms));
                httpReq.Headers.Add("Authorization", "Basic " + authorization);

                _strHeaderRequest = httpReq.Headers.ToString(); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                using (HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse()))
                {
                    using (Stream streamFile = httpResponse.GetResponseStream())
                    {
                        using (StreamReader streamreader = new StreamReader(streamFile))
                        {
                            stResponse = streamreader.ReadToEnd();
                        }
                    }
                }

                _strResponse = stResponse; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                _oSendSMSRptaOperadorExterno = JsonConvert.DeserializeObject<ClsMensajeSMSRptaOperadorExterno>(stResponse);

                return stResponse;
            }
            catch (Exception ex)
            {
                _strdescripcionexcepcion = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        public string EnviarMensajeSMS(string nroComunicacion, string mensajeTexto, short usaSSL, ref string peticion)
        {
            string stResponse = "";

            //Req-002 - Operador SMS - Observaciones 2 (Mod. por: ECALDAS CANVIA)
            if (usaSSL == 1)
            {
                //Intentar establecer el protocolo de seguridad, en caso no se soporte o se produja un error se 
                //continua con el flujo. En la invocacion al Servicio respectivo se producirá la Excepcion.

                try
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.DefaultConnectionLimit = 9999;
                    ServicePointManager.SecurityProtocol =
                        (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;

                    // Skip validation of SSL/TLS certificate
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }
                catch (Exception)
                { }
            }
            //

            try
            {
                if (nroComunicacion.StartsWith(_strprefixcelular))
                {
                    nroComunicacion = nroComunicacion.Substring(_strprefixcelular.Length);
                }

                nroComunicacion = nroComunicacion.Replace("+", "").Trim();
                UriBuilder urlBuilder = new UriBuilder();
                if (usaSSL == 1)
                {
                    urlBuilder.Scheme = "https";
                }
                urlBuilder.Host = _strurl.EndsWith("/") ? _strurl.Substring(0, _strurl.Length - 1) : _strurl; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                urlBuilder.Query = string.Format("country={0}&message={1}&messageFormat={2}&mobile={3}",
                    _strprefixcelular, mensajeTexto, 1, nroComunicacion);

                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(urlBuilder.ToString()));
                peticion = httpReq.RequestUri.ToString();
                _strRequest = peticion; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                httpReq.Proxy = null;

                string authorization =
                    Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_struseridapisms + ":" + _strpwdapisms));
                httpReq.Headers.Add("Authorization", "Basic " + authorization);

                _strHeaderRequest = httpReq.Headers.ToString(); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                using (HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse()))
                {
                    using (Stream streamFile = httpResponse.GetResponseStream())
                    {
                        using (StreamReader streamreader = new StreamReader(streamFile))
                        {
                            stResponse = streamreader.ReadToEnd();
                        }
                    }
                }

                _strResponse = stResponse; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                _oSendSMSRptaOperadorExterno = JsonConvert.DeserializeObject<ClsMensajeSMSRptaOperadorExterno>(stResponse);

                return stResponse;
            }
            catch (Exception ex)
            {
                _strdescripcionexcepcion = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Metodos POST

        /// <summary>
        /// Enviar el mismo mensaje de texto a varios destinatario. Si es un mensaje personalizado enviar en la lista de 
        /// numeros de comunicacion un solo elemento
        /// </summary>
        /// <param name="listaNroComunicacion"></param>
        /// <param name="mensajeTexto"></param>
        /// <param name="usaSSL"></param>
        /// <returns></returns>
        public string EnviarMensajeSMS(List<string> listaNroComunicacion, string mensajeTexto, short usaSSL)
        {
            string stResponse = "";

            //Req-002 - Operador SMS - Observaciones 2 (Mod. por: ECALDAS CANVIA)
            if (usaSSL == 1)
            {
                //Intentar establecer el protocolo de seguridad, en caso no se soporte o se produja un error se 
                //continua con el flujo. En la invocacion al Servicio respectivo se producirá la Excepcion.

                try
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.DefaultConnectionLimit = 9999;
                    ServicePointManager.SecurityProtocol =
                        (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;

                    // Skip validation of SSL/TLS certificate
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }
                catch (Exception)
                { }
            }
            //

            try
            {
                for (int i = 0; i < listaNroComunicacion.Count; i++)
                {
                    if (listaNroComunicacion[i].StartsWith(_strprefixcelular))
                    {
                        listaNroComunicacion[i] = listaNroComunicacion[i].Substring(_strprefixcelular.Length);
                    }

                    listaNroComunicacion[i] = listaNroComunicacion[i].Replace("+", "").Trim();
                }
                UriBuilder urlBuilder = new UriBuilder();
                if (usaSSL == 1)
                {
                    urlBuilder.Scheme = "https";
                }
                urlBuilder.Host = _strurl.EndsWith("/") ? _strurl.Substring(0, _strurl.Length - 1) : _strurl; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(urlBuilder.ToString()));
                _strRequest = httpReq.RequestUri.ToString(); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                httpReq.Proxy = null;
                httpReq.KeepAlive = true;
                httpReq.Method = "POST";
                httpReq.ContentType = "application/json; charset=utf-8";

                string authorization =
                    Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_struseridapisms + ":" + _strpwdapisms));
                httpReq.Headers.Add("Authorization", "Basic " + authorization);

                _strHeaderRequest = httpReq.Headers.ToString(); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                var listaComunicacion = new List<string>();
                foreach (string nroComunicacion in listaNroComunicacion)
                {
                    listaComunicacion.Add(string.Format("{0} \"url\": \" \", \"mobile\": \"{1}\" {2}",
                        "{", nroComunicacion, "}"));
                }
                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(
                    string.Concat("{ ",
                        "\"country\": \"", _strprefixcelular, "\",",
                        "\"dateToSend\": \"\",",
                        "\"message\": \"" + mensajeTexto + "\",",
                        "\"messageFormat\": 1,",
                        "\"addresseeList\": [ " + string.Join(",", listaComunicacion.ToArray()) + " ] }"));

                _strBodyRequest = System.Text.Encoding.UTF8.GetString(byteArray); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                using (var writer = httpReq.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse()))
                {
                    using (Stream streamFile = httpResponse.GetResponseStream())
                    {
                        using (StreamReader streamreader = new StreamReader(streamFile))
                        {
                            stResponse = streamreader.ReadToEnd();
                        }
                    }
                }

                _strResponse = stResponse; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                _oSendSMSRptaOperadorExterno = JsonConvert.DeserializeObject<ClsMensajeSMSRptaOperadorExterno>(stResponse);

                return stResponse;
            }
            catch (Exception ex)
            {
                _strdescripcionexcepcion = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Enviar el mismo mensaje de texto a varios destinatario. Si es un mensaje personalizado enviar en la lista de 
        /// numeros de comunicacion un solo elemento
        /// </summary>
        /// <param name="listaNroComunicacion"></param>
        /// <param name="mensajeTexto"></param>
        /// <param name="usaSSL"></param>
        /// <param name="peticion"></param>
        /// <returns></returns>
        public string EnviarMensajeSMS(List<string> listaNroComunicacion, string mensajeTexto, short usaSSL, ref string peticion)
        {
            string stResponse = "";

            //Req-002 - Operador SMS - Observaciones 2 (Mod. por: ECALDAS CANVIA)
            if (usaSSL == 1)
            {
                //Intentar establecer el protocolo de seguridad, en caso no se soporte o se produja un error se 
                //continua con el flujo. En la invocacion al Servicio respectivo se producirá la Excepcion.

                try
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.DefaultConnectionLimit = 9999;
                    ServicePointManager.SecurityProtocol =
                        (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;

                    // Skip validation of SSL/TLS certificate
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }
                catch (Exception)
                { }
            }
            //

            try
            {
                for (int i = 0; i < listaNroComunicacion.Count; i++)
                {
                    if (listaNroComunicacion[i].StartsWith(_strprefixcelular))
                    {
                        listaNroComunicacion[i] = listaNroComunicacion[i].Substring(_strprefixcelular.Length);
                    }

                    listaNroComunicacion[i] = listaNroComunicacion[i].Replace("+", "").Trim();
                }
                UriBuilder urlBuilder = new UriBuilder();
                if (usaSSL == 1)
                {
                    urlBuilder.Scheme = "https";
                }
                urlBuilder.Host = _strurl.EndsWith("/") ? _strurl.Substring(0, _strurl.Length - 1) : _strurl; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(urlBuilder.ToString()));
                peticion = httpReq.RequestUri.ToString();
                _strRequest = peticion; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)
                httpReq.Proxy = null;
                httpReq.KeepAlive = true;
                httpReq.Method = "POST";
                httpReq.ContentType = "application/json; charset=utf-8";

                string authorization =
                    Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_struseridapisms + ":" + _strpwdapisms));
                httpReq.Headers.Add("Authorization", "Basic " + authorization);

                _strHeaderRequest = httpReq.Headers.ToString(); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                var listaComunicacion = new List<string>();
                foreach (string nroComunicacion in listaNroComunicacion)
                {
                    listaComunicacion.Add(string.Format("{0} \"url\": \" \", \"mobile\": \"{1}\" {2}",
                        "{", nroComunicacion, "}"));
                }
                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(
                    string.Concat("{ ",
                        "\"country\": \"", _strprefixcelular, "\",",
                        "\"dateToSend\": \"\",",
                        "\"message\": \"" + mensajeTexto + "\",",
                        "\"messageFormat\": 1,",
                        "\"addresseeList\": [ " + string.Join(",", listaComunicacion.ToArray()) + " ] }"));

                _strBodyRequest = System.Text.Encoding.UTF8.GetString(byteArray); //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                using (var writer = httpReq.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (HttpWebResponse httpResponse = (HttpWebResponse)(httpReq.GetResponse()))
                {
                    using (Stream streamFile = httpResponse.GetResponseStream())
                    {
                        using (StreamReader streamreader = new StreamReader(streamFile))
                        {
                            stResponse = streamreader.ReadToEnd();
                        }
                    }
                }

                _strResponse = stResponse; //Req-002 - Operador SMS - Observaciones (Mod. por: ECALDAS CANVIA)

                _oSendSMSRptaOperadorExterno = JsonConvert.DeserializeObject<ClsMensajeSMSRptaOperadorExterno>(stResponse);

                return stResponse;
            }
            catch (Exception ex)
            {
                _strdescripcionexcepcion = ex.Message;
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #endregion
        //

        #endregion Metodos Publicos
    }

    /// <summary>
    /// Entidad clsCampoClave
    /// </summary>

    [Serializable]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsCampoClave
    {
        #region Campos

        private Int16 _intidcampoclave;
        private String _straliascampoclave;
        private String _strdescripcioncampoclave;
        private Int32 _intidorigencampoclave;
        private Int32 _intidcondicioncampoclave;
        private String _strcampoclave;
        private Int16 _intidestado;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 IdCampoClave
        {
            get { return _intidcampoclave; }
            set { _intidcampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String AliasCampoClave
        {
            get { return _straliascampoclave; }
            set { _straliascampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String DescripcionCampoClave
        {
            get { return _strdescripcioncampoclave; }
            set { _strdescripcioncampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdOrigenCampoClave
        {
            get { return _intidorigencampoclave; }
            set { _intidorigencampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdCondicionCampoClave
        {
            get { return _intidcondicioncampoclave; }
            set { _intidcondicioncampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String CampoClave
        {
            get { return _strcampoclave; }
            set { _strcampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsCampoClave()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsCampoClave(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idcampoclave")) IdCampoClave = dr["idcampoclave"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idcampoclave"]);
            if (dr.Table.Columns.Contains("aliascampoclave")) AliasCampoClave = dr["aliascampoclave"] == DBNull.Value ? String.Empty : dr["aliascampoclave"].ToString();
            if (dr.Table.Columns.Contains("descripcioncampoclave")) DescripcionCampoClave = dr["descripcioncampoclave"] == DBNull.Value ? String.Empty : dr["descripcioncampoclave"].ToString();
            if (dr.Table.Columns.Contains("idorigencampoclave")) IdOrigenCampoClave = dr["idorigencampoclave"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idorigencampoclave"]);
            if (dr.Table.Columns.Contains("idcondicioncampoclave")) IdCondicionCampoClave = dr["idcondicioncampoclave"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idcondicioncampoclave"]);
            if (dr.Table.Columns.Contains("campoclave")) CampoClave = dr["campoclave"] == DBNull.Value ? String.Empty : dr["campoclave"].ToString();
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idestado"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista clsListaCampoClave
    /// </summary>
    ///
    [Serializable]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaCampoClave
    {
        #region Campos

        private List<clsCampoClave> _objelementos = new List<clsCampoClave>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>

        public List<clsCampoClave> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaCampoClave()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaCampoClave(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsCampoClave(drw));
            }
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad clsMensajeSMSCampoClave
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsMensajeSMSCampoClave
    {
        #region Campos

        private Int32 _intidmensajesmscampoclave;
        private Int32 _intidmensajesms;
        private Int16 _intidcampoclave;
        private Int16 _intidestado;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int32 IdMensajeSMSCampoClave
        {
            get { return _intidmensajesmscampoclave; }
            set { _intidmensajesmscampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdMensajeSMS
        {
            get { return _intidmensajesms; }
            set { _intidmensajesms = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdCampoClave
        {
            get { return _intidcampoclave; }
            set { _intidcampoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsMensajeSMSCampoClave()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsMensajeSMSCampoClave(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idmensajesmscampoclave")) IdMensajeSMSCampoClave = dr["idmensajesmscampoclave"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmensajesmscampoclave"]);
            if (dr.Table.Columns.Contains("idmensajesms")) IdMensajeSMS = dr["idmensajesms"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmensajesms"]);
            if (dr.Table.Columns.Contains("idcampoclave")) IdCampoClave = dr["idcampoclave"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idcampoclave"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idestado"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista clsListaMensajeSMSCampoClave
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaMensajeSMSCampoClave
    {
        #region Campos

        private List<clsMensajeSMSCampoClave> _objelementos = new List<clsMensajeSMSCampoClave>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsMensajeSMSCampoClave> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaMensajeSMSCampoClave()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaMensajeSMSCampoClave(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsMensajeSMSCampoClave(drw));
            }
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad clsMensajeSMSEjecucion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsMensajeSMSEjecucion
    {
        #region Campos

        private Int32 _intidmensajeejecucion;
        private Int32 _intidmensajesms;
        private Int32 _intidtarea;
        private Int16 _inttipoprocesamiento;
        private Int32 _intidusuarioprocesamiento;
        private DateTime _datfechaprocesamiento;
        private Int16 _intidestado;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int32 IdMensajeEjecucion
        {
            get { return _intidmensajeejecucion; }
            set { _intidmensajeejecucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdMensajeSMS
        {
            get { return _intidmensajesms; }
            set { _intidmensajesms = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 idTarea
        {
            get { return _intidtarea; }
            set { _intidtarea = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 TipoProcesamiento
        {
            get { return _inttipoprocesamiento; }
            set { _inttipoprocesamiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdUsuarioProcesamiento
        {
            get { return _intidusuarioprocesamiento; }
            set { _intidusuarioprocesamiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaProcesamiento
        {
            get { return _datfechaprocesamiento; }
            set { _datfechaprocesamiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        ///
        /// </summary>

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsMensajeSMSEjecucion()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsMensajeSMSEjecucion(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idmensajeejecucion")) IdMensajeEjecucion = dr["idmensajeejecucion"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmensajeejecucion"]);
            if (dr.Table.Columns.Contains("idmensajesms")) IdMensajeSMS = dr["idmensajesms"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idmensajesms"]);
            if (dr.Table.Columns.Contains("idtarea")) idTarea = dr["idtarea"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idtarea"]);
            if (dr.Table.Columns.Contains("tipoprocesamiento")) TipoProcesamiento = dr["tipoprocesamiento"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["tipoprocesamiento"]);
            if (dr.Table.Columns.Contains("idusuarioprocesamiento")) IdUsuarioProcesamiento = dr["idusuarioprocesamiento"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(dr["idusuarioprocesamiento"]);
            if (dr.Table.Columns.Contains("fechaprocesamiento")) FechaProcesamiento = dr["fechaprocesamiento"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(dr["fechaprocesamiento"]);
            if (dr.Table.Columns.Contains("idestado")) IdEstado = dr["idestado"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["idestado"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista clsListaMensajeSMSEjecucion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaMensajeSMSEjecucion
    {
        #region Campos

        private List<clsMensajeSMSEjecucion> _objelementos = new List<clsMensajeSMSEjecucion>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsMensajeSMSEjecucion> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaMensajeSMSEjecucion()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaMensajeSMSEjecucion(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new clsMensajeSMSEjecucion(drw));
            }
        }

        #endregion Constructor
    }

    //Req-002 - Operador SMS (Mod. por: ECALDAS CANVIA)
    public class ClsMensajeSMSRptaOperadorExterno
    {
        #region Campos

        private int _intStatus;
        private string _strReason;
        private ClsMensajeSMSRptaOperadorExternoResult _objResult;

        #endregion

        #region Propiedades

        [JsonProperty("status")]
        public int Status
        {
            get { return _intStatus; }
            set { _intStatus = value; }
        }

        [JsonProperty("reason")]
        public string Reason
        {
            get { return _strReason; }
            set { _strReason = value; }
        }

        [JsonProperty("result")]
        public ClsMensajeSMSRptaOperadorExternoResult Result
        {
            get { return _objResult; }
            set { _objResult = value; }
        }

        #endregion

        #region Constructor

        public ClsMensajeSMSRptaOperadorExterno()
        {
        }

        #endregion
    }
    //

    //Req-002 - Operador SMS (Mod. por: ECALDAS CANVIA)
    public class ClsMensajeSMSRptaOperadorExternoResult
    {
        #region Campos

        private int _intTotalRequest;
        private int _intTotalFailed;
        private List<ClsMensajeSMSRptaOperadorExternoResultRequest> _objReceivedRequests;
        private List<ClsMensajeSMSRptaOperadorExternoResultRequest> _objFailedRequests;
        private string _strDateToSend;
        private string _strTimeZone;

        #endregion

        #region Propiedades

        [JsonProperty("totalRequest")]
        public int TotalRequest
        {
            get { return _intTotalRequest; }
            set { _intTotalRequest = value; }
        }

        [JsonProperty("totalFailed")]
        public int TotalFailed
        {
            get { return _intTotalFailed; }
            set { _intTotalFailed = value; }
        }

        [JsonProperty("receivedRequests")]
        public List<ClsMensajeSMSRptaOperadorExternoResultRequest> ReceivedRequests
        {
            get { return _objReceivedRequests; }
            set { _objReceivedRequests = value; }
        }

        [JsonProperty("failedRequests")]
        public List<ClsMensajeSMSRptaOperadorExternoResultRequest> FailedRequests
        {
            get { return _objFailedRequests; }
            set { _objFailedRequests = value; }
        }

        [JsonProperty("dateToSend")]
        public string DateToSend
        {
            get { return _strDateToSend; }
            set { _strDateToSend = value; }
        }

        [JsonProperty("timeZone")]
        public string TimeZone
        {
            get { return _strTimeZone; }
            set { _strTimeZone = value; }
        }

        #endregion

        #region Constructor

        public ClsMensajeSMSRptaOperadorExternoResult()
        {
        }

        #endregion
    }
    //

    //Req-002 - Operador SMS (Mod. por: ECALDAS CANVIA)
    public class ClsMensajeSMSRptaOperadorExternoResultRequest
    {
        #region Campos

        private string _strMobile;
        private string _strTransactionId;
        private int _intStatus;
        private string _strReason;

        #endregion

        #region Propiedades

        [JsonProperty("mobile")]
        public string Mobile
        {
            get { return _strMobile; }
            set { _strMobile = value; }
        }

        [JsonProperty("transactionId")]
        public string TransactionId
        {
            get { return _strTransactionId; }
            set { _strTransactionId = value; }
        }

        [JsonProperty("status")]
        public int Status
        {
            get { return _intStatus; }
            set { _intStatus = value; }
        }

        [JsonProperty("reason")]
        public string Reason
        {
            get { return _strReason; }
            set { _strReason = value; }
        }

        #endregion

        #region Constructor

        public ClsMensajeSMSRptaOperadorExternoResultRequest()
        {
        }

        #endregion

    }
    //

    //Req-002 - Operador SMS (Mod. por: ECALDAS CANVIA)
    public class ClsMensajeSMSOperadorExcepcion
    {
        #region Campos

        private short _intIdOperadorSMS;
        private short _intCodigo;
        private string _strDescripcion;
        private string _strNivel;

        #endregion

        #region Propiedades

        public short IdOperadorSMS
        {
            get { return _intIdOperadorSMS; }
            set { _intIdOperadorSMS = value; }
        }

        public short Codigo
        {
            get { return _intCodigo; }
            set { _intCodigo = value; }
        }

        public string Descripcion
        {
            get { return _strDescripcion; }
            set { _strDescripcion = value; }
        }

        public string Nivel
        {
            get { return _strNivel; }
            set { _strNivel = value; }
        }

        #endregion

        #region Constructor

        public ClsMensajeSMSOperadorExcepcion()
        {
        }

        public ClsMensajeSMSOperadorExcepcion(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("IdOperadorSMS")) IdOperadorSMS = dr["IdOperadorSMS"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdOperadorSMS"]);
            if (dr.Table.Columns.Contains("Codigo")) Codigo = dr["Codigo"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["Codigo"]);
            if (dr.Table.Columns.Contains("Descripcion")) Descripcion = dr["Descripcion"] == DBNull.Value ? string.Empty : dr["Descripcion"].ToString();
            if (dr.Table.Columns.Contains("Nivel")) Nivel = dr["Nivel"] == DBNull.Value ? string.Empty : dr["Nivel"].ToString();
        }

        #endregion
    }

    public class ClsListaMensajeSMSOperadorExcepcion
    {
        #region Campos

        private List<ClsMensajeSMSOperadorExcepcion> _objelementos = new List<ClsMensajeSMSOperadorExcepcion>();

        #endregion Campos

        #region Propiedades

        public List<ClsMensajeSMSOperadorExcepcion> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public ClsListaMensajeSMSOperadorExcepcion()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public ClsListaMensajeSMSOperadorExcepcion(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow drw in entidad.Rows)
            {
                Elementos.Add(new ClsMensajeSMSOperadorExcepcion(drw));
            }
        }

        #endregion Constructor
    }
    //


}
