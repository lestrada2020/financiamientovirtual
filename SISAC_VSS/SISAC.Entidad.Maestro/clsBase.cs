﻿using System;
using System.Collections.Generic;
using System.Data;

namespace SISAC.Entidad.Maestro
{
    #region clsBase

    public class clsBase
    {
        public String Token { get; set; }

        public Int32 Tried { get; set; }

        public Int16 IdAplicacion { get; set; }
    }

    #endregion clsBase

    #region clsResultado

    public class clsResultado : clsBase
    {
        #region Properties

        public Int32 IdError { get; set; }

        public String Mensaje { get; set; }

        public Object Datos { get; set; }

        #endregion Properties

        #region Public

        public void SetError(Int32 idError, String message)
        {
            IdError = idError;
            Mensaje = message;
        }

        #endregion Public
    }

    #endregion clsResultado

    #region clsParametro

    public class clsParametro : clsBase
    {
        #region Properties

        public Dictionary<String, String> Parametros { get; set; }

        #endregion Properties
    }

    public class clsParametroBulkData : clsBase
    {
        public Dictionary<String, String> Parametros { get; set; }
        public string DataParametro { get; set; }
        public DataTable Data { get; set; }
    }

    #endregion clsParametro
}