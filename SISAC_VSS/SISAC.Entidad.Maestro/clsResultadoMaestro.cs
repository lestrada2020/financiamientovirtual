﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Maestro
{
    public class clsResultadoMaestro
    {
        #region Properties

        public String error_code { get; set; }

        public Boolean success { get; set; }

        public String message { get; set; }

        public Object data { get; set; }

        #endregion Properties
    }
}
