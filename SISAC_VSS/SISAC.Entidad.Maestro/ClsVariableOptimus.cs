﻿using System.Data;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Entidad.Maestro
{
    public class ClsVariableOptimus
    {
        public string Id { get; set; }

        public string Nombre { get; set; }

        public string Valor { get; set; }

        public string Descripcion { get; set; }

        public ClsVariableOptimus()
        {
        }

        public ClsVariableOptimus(string nombre, string valor, string descripcion)
        {
            Descripcion = descripcion;
            Nombre = nombre;
            Valor = valor;
        }

        public ClsVariableOptimus(DataRow registro)
        {
            DataColumnCollection columnas = registro.Table.Columns;
            if (registro == null) { return; }
            if (columnas.Contains("nombre")) Nombre = registro["nombre"].ToString();
            Id = Nombre;
            if (columnas.Contains("valor")) Valor = registro["valor"].ToString();
            if (columnas.Contains("descripcion")) Descripcion = registro["descripcion"].ToString();
        }

    }
}
