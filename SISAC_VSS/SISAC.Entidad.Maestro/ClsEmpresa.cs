﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Maestro
{
    public class ClsEmpresa
    {
        #region Propiedades

        /// <summary>
        /// Id
        /// </summary>
        public short Id { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// IdOrganizacion.
        /// </summary>
        public short IdOrganizacion { get; set; }

        /// <summary>
        /// AbreviaEmpresa.
        /// </summary>
        public string AbreviaEmpresa { get; set; }

        /// <summary>
        /// Ruc.
        /// </summary>
        public string Ruc { get; set; }

        /// <summary>
        /// Estado.
        /// </summary>
        public short Estado { get; set; }

        /// <summary>
        /// NombreCompleto.
        /// </summary>
        public string NombreCompleto { get; set; }

        /// <summary>
        /// Direccion.
        /// </summary>
        public string Direccion { get; set; }

        /// <summary>
        /// DireccionComplementaria.
        /// </summary>
        public string DireccionComplementaria { get; set; }

        /// <summary>
        /// DireccionCodificada.
        /// </summary>
        public int DireccionCodificada { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string CodigoIdentificacion { get; set; }

        /// <summary>
        ///
        /// </summary>
        public byte[] Imagen { get; set; }

        public byte[] FirmaLegal { get; set; }

        public string Telefono { get; set; }

        public string PartidaRegistral { get; set; }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacio.
        /// </summary>
        public ClsEmpresa()
        {
        }

        /// <summary>
        /// Constructor pasandole todos los atributos de la entidad.
        /// </summary>
        /// <param name="idempresa"></param>
        /// <param name="nombreempresa"></param>
        /// <param name="abreviaempresa"></param>
        /// <param name="estado"></param>
        /// <param name="idorganizacion"></param>
        /// <param name="ruc"></param>
        /// <param name="nombrecompleto"></param>
        /// <param name="direccion"></param>
        /// <param name="direccioncomplementaria"></param>
        /// <param name="direccioncodificada"></param>
        public ClsEmpresa(short idempresa
                        , string nombreempresa
                        , string abreviaempresa
                        , short estado
                        , short idorganizacion
                        , string ruc
                        , string nombrecompleto
                        , string direccion
                        , string direccioncomplementaria
                        , int direccioncodificada
                        , string strcodigoidentificacion)
        {
            Id = idempresa;
            Nombre = nombreempresa;
            AbreviaEmpresa = abreviaempresa;
            IdOrganizacion = idorganizacion;
            Ruc = ruc;
            Estado = estado;
            NombreCompleto = nombrecompleto;
            Direccion = direccion;
            DireccionComplementaria = direccioncomplementaria;
            DireccionCodificada = direccioncodificada;
            CodigoIdentificacion = strcodigoidentificacion;
        }

        /// <summary>
        /// Constructor pasandole un datarow.
        /// </summary>
        /// <param name="registro"></param>
        public ClsEmpresa(DataRow registro)
        {
            if (registro == null) return;

            Id = (short)registro["idempresa"];
            Nombre = registro["NombreEmpresa"].ToString();
            AbreviaEmpresa = registro["abreviaempresa"].ToString();
            IdOrganizacion = (short)registro["idorganizacion"];
            Ruc = registro["Ruc"].ToString();
            Estado = (short)registro["estado"];

            NombreCompleto = registro["nombrecompleto"].ToString();
            Direccion = registro["direccion"].ToString();
            DireccionComplementaria = registro["direccioncomplementaria"].ToString();
            DireccionCodificada = Convert.IsDBNull(registro["iddireccioncodificada"]) ? (int)0 : (int)registro["iddireccioncodificada"];

            if (registro.Table.Columns.Contains("codigoidentificacion"))
                CodigoIdentificacion = registro["codigoidentificacion"] == null ? (string)null : registro["codigoidentificacion"].ToString();

            if (registro.Table.Columns.Contains("imagen"))
                Imagen = Convert.IsDBNull(registro["imagen"]) ? (byte[])(null) : (byte[])registro["imagen"];

            if (registro.Table.Columns.Contains("firmalegal"))
                FirmaLegal = Convert.IsDBNull(registro["firmalegal"]) ? (byte[])(null) : (byte[])registro["firmalegal"];

            if (registro.Table.Columns.Contains("PartidaRegistral")) PartidaRegistral = registro["PartidaRegistral"] == DBNull.Value ? string.Empty : registro["PartidaRegistral"].ToString();
        }

        #endregion Constructor

    }
    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
    public abstract class clsEntidadMaestro : ICombo
    {
        protected Int16 _intid;
        protected String _strnombre;
        protected Boolean _bolseleccionado = false;

        #region ICombo Members

        /// <summary>
        /// Id
        /// </summary>
        ///
        //[Campo()]
        public virtual Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        /// Nombre
        /// </summary>
        ///
        //[Campo()]
        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        public Boolean Seleccionado
        {
            get { return _bolseleccionado; }
            set { _bolseleccionado = value; }
        }

        #endregion ICombo Members
    }

    public interface ICombo
    {
        Int16 Id { get; set; }

        String Nombre { get; set; }
    }
    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL

}
