﻿using System; //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL
namespace SISAC.Entidad.Maestro
{
    #region TypeSite

    public enum TypeSite
    {
        Default = 0,
        Atencion = 1,
        NTCSE = 2,
        Seguridad = 3
    }

    #endregion TypeSite

    public enum ConceptosFacturar
    {
        EAT = 1,
        EAHP = 15,
        EAFP = 16,
        ER = 17,
        PHP = 18,
        PFP = 19,
        PURDHP = 22,
        PURDFP = 23,
        PGHP = 24,
        PGFP = 25,
        ExcPURDFP = 91,
        ExcPFP = 209,
        ExcPHP = 210,
        PotenciaActiva = 93,
        IGV = 4,
        SaldoRedondeo = 252,
        Redondeo = 202,
        CompInteNTCSE = 197,
        InteresConvenio = 8,
        CargoReposicion = 12,
        CargoMantenimiento = 13,
        AjusteTarifario = 320,
        CargoFijo = 2,
        CargoFijoSemestral = 269,
        CargoComercialRecargaPorTarjetas = 341,
        CargoComercialRecargaPorCodigo = 429,
        AlumbradoPublico = 3,
        ServicioGeneral = 291,
        APC = 174,
        AporteLey = 142,
        ExonerarIGV = 211,

        /// <summary>
        /// Consumo entre 0 - 30
        /// </summary>
        EAMenor30 = 73,

        /// <summary>
        /// Consumo entre 31 - 100
        /// para los 30 primeros Kwh,
        /// </summary>
        Ener30y1001ra = 165,

        /// <summary>
        /// Consumo entre 31 - 100
        /// para los restantes.
        /// </summary>
        Ener30y1002da = 74,

        /// <summary>
        /// Consumo > 100
        /// </summary>
        EnerMayor100 = 75,

        Corte = 192,

        Reconexion = 163,
        CuotaInicialConvenio = 161,
        ReintegroEnergia = 331,
        RecuperoEnergia = 240,
        RecuperoEnergiaA91 = 241,
        InteresCompensatorio = 6,
        InteresMoratorio = 7
    }

    public enum EstadoNGC
    {
        /// Estados generales para los objetos de negocio
        /// Rango: 0 a 19
        /// ---------------------------------------------
        /// <summary>
        /// Registro "inactivo" para la entidad
        /// </summary>
        Inactivo = 0,

        /// <summary>
        /// Registro "activo" de la entidad
        /// </summary>
        Activo = 1,

        /// <summary>
        /// Registro "anulado" de la entidad
        /// </summary>
        Anulado = 2,

        /// <summary>
        /// Registro "borrado" de la entidad
        /// </summary>
        Borrado = 3,

        /// <summary>
        /// Registro "eventual" de la entidad
        /// </summary>
        Eventual = 4,

        /// <summary>
        ///
        /// </summary>
        Pendiente = 5,

        /// ------------------------------------------------------------
        /// Estados para los objetos de negocio de Modulo de Facturación
        /// Rango: 20 a 49
        /// <summary>
        /// Registro cortado para la entidad
        /// </summary>
        Cortado = 20,

        /// <summary>
        /// Registro "retirado" de la entidad
        /// </summary>
        Retirado = 22,

        /// <summary>
        /// Facturacion en proceso.
        /// </summary>
        EnProceso = 21,

        /// <summary>
        /// Facturacion Vigente o Corriente o En Cobranza.
        /// </summary>
        Vigente = 23,

        NoVigente = 204,

        /// <summary>
        /// Registro retirado definitivo.
        /// </summary>
        RetiradoDefinitivo = 25,

        /// <summary>
        ///
        /// </summary>
        Desestimado = 26,

        /// <summary>
        /// Estado incial del NroServicio, al momento de insertar
        /// a partir de un cobro de presupuesto por solicitud del
        /// cliente por instalación de nuevo suministro.
        /// </summary>
        NuevoSuministro = 27,

        ///Estados para Cronograma
        /// <summary>
        /// Estado para Cronograma
        /// </summary>
        EnRevision = 30,

        /// <summary>
        ///
        /// </summary>
        PorAprobar = 31,

        /// <summary>
        ///
        /// </summary>
        Aprobado = 32,

        /// <summary>
        ///
        /// </summary>
        Incompleto = 203,

        // ---------------------------------------------------------------------------
        // A partir de aqui es para los estados de la compensación por Interrupciones.
        //
        /// <summary>
        /// Estado para cuando ya se ha realizado la compensación por interrupciones LCE.
        /// </summary>
        Compensado = 40,

        /// <summary>
        /// Estado para cuando ya se a incluido en la facturación el registro compensado.
        /// </summary>
        Facturado = 41,

        /// <summary>
        ///
        /// </summary>
        Rechazado = 166,

        /// ------------------------------------------------------------
        /// Estados para los objetos de negocios de Modulo de Cobranzas
        /// Rango: 50a 79
        /// <summary>
        /// Registro "no pagado" de la entidad
        /// </summary>
        NoPagado = 50,

        /// <summary>
        /// Registro "pagado" de la entidad
        /// </summary>
        Pagado = 52,

        /// <summary>
        /// Registro "financiado" de la entidad
        /// </summary>
        Financiado = 54,

        /// <summary>
        /// Registro "vencido" de la entidad
        /// </summary>
        Vencido = 62,

        /// <summary>
        /// Registro "moroso" de la entidad
        /// </summary>
        Moroso = 64,

        /// <summary>
        ///
        /// </summary>
        PorProvisionar = 69,

        /// <summary>
        /// Registro "provisionado" de la entidad
        /// </summary>
        Provisionado = 70,

        /// <summary>
        ///
        /// </summary>
        PorCastigar = 71,

        /// <summary>
        /// Registro "Castigado" de la entidad
        /// </summary>
        Castigado = 72,

        /// <summary>
        /// Registro "Por Confirmar" de la entidad
        /// </summary>
        PorConfirmar = 74,

        /// <summary>
        /// Registro "Pendiente Por Cuota Inicial" de la entidad
        /// </summary>
        PendientePorCI = 76,

        /// ------------------------------------------------------------
        /// Estados para los objetos de negocios de Modulo Contable
        /// Rango: 80 al 99
        /// <summary>
        /// Registro "Registrado" de la entidad
        /// </summary>
        Registrado = 80,

        /// <summary>
        /// Registro "Contabilizado" de la entidad
        /// </summary>
        Contabilizado = 82,

        /// <summary>
        /// Registro "extornado" de la entidad
        /// </summary>
        Extornado = 84,

        /// <summary>
        /// Registro "extorno parcial" de la entidad
        /// </summary>
        ExtornoParcial = 85,

        /// <summary>
        ///
        /// </summary>
        Abierto = 90,

        /// <summary>
        ///
        /// </summary>
        Abrir = 91,

        /// <summary>
        ///
        /// </summary>
        Suspendido = 92,

        /// <summary>
        ///
        /// </summary>
        Suspender = 93,

        /// <summary>
        ///
        /// </summary>
        QuitarSuspension,

        /// <summary>
        ///
        /// </summary>
        CerrarCajero = 95,

        /// <summary>
        ///
        /// </summary>
        EnCierre = 96,

        /// <summary>
        ///
        /// </summary>
        Bloqueado = 97,

        /// <summary>
        ///
        /// </summary>
        Cerrado = 98,

        /// Estado para el ENVIO DE DINERO.
        /// <summary>
        ///
        /// </summary>
        ABoveda = 100,

        /// <summary>
        ///
        /// </summary>
        EnCustodia = 101,

        /// <summary>
        ///
        /// </summary>
        Remitido = 102,

        /// <summary>
        /// Estado para el INTERCAMBIO.
        /// </summary>
        Creado = 110,

        /// <summary>
        ///
        /// </summary>
        Preparando = 111,

        /// <summary>
        ///
        /// </summary>
        Configurado = 112,

        /// <summary>
        ///
        /// </summary>
        Asignado = 113,

        /// <summary>
        /// Tambien se usa para cuando la fecha de validez de la solicitid de autorización para hacer
        /// un convenio fuera de los parametros se vence y no se hace el convenio. (NCA).
        /// </summary>
        Caducado = 114,

        /// <summary>
        ///
        /// </summary>
        Enviado = 115,

        /// <summary>
        ///
        /// </summary>
        Recepcionado = 116,

        /// <summary>
        ///
        /// </summary>
        Procesando = 117,

        /// <summary>
        ///
        /// </summary>
        Procesado = 118,

        /// <summary>
        ///
        /// </summary>
        Abortado = 119,

        /// <summary>
        ///
        /// </summary>
        EnDefinicion = 150,

        /// <summary>
        /// Tarea sin Iniciar o en cola
        /// </summary>
        EnCola = 160,

        /// <summary>
        /// Tarea Iniciada
        /// </summary>
        Ejecutando = 161,

        /// <summary>
        /// Tarea Pausada
        /// </summary>
        Pausado = 162,

        /// <summary>
        /// Tarea en proceso de cancelación
        /// </summary>
        Cancelando = 163,

        /// <summary>
        /// Tarea en proceso de reanudación
        /// </summary>
        Reanudar = 164,

        /// <summary>
        /// Tarea Finalizada
        /// </summary>
        Terminado = 165,

        /// <summary>
        /// Tarea en proceso de Reinicio
        /// </summary>
        Reiniciando = 167,

        /// <summary>
        /// Tarea Finalizada con Cancelación
        /// </summary>
        Cancelado = 168,

        /// <summary>
        /// Tarea Finalizada con Errores
        /// </summary>
        Fallado = 169,

        /// <summary>
        /// Orden de Trabajo Generado (cabecera).
        /// También se usa para la data que se genera en Cortes para poder crear las O.T.
        /// </summary>
        Generado = 170,

        /// <summary>
        /// Orden de Trabajo entregado a Contratista
        /// </summary>
        Entregado = 171,

        /// <summary>
        /// La OT esta procesandose
        /// </summary>
        EnEjecucion = 172,

        /// <summary>
        /// Se Recibio la totalidad de la OT.
        /// </summary>
        Recibido = 173,

        /// <summary>
        /// Se proceso totalmente la OT
        /// </summary>
        Ejecutado = 174,

        /// <summary>
        ///
        /// </summary>
        ParaInspeccion = 176,

        /// <summary>
        ///
        /// </summary>
        EnInspeccion = 178,

        /// <summary>
        ///
        /// </summary>
        ParaCalculo = 175,

        /// <summary>
        ///
        /// </summary>
        Validado = 177,

        /// <summary>
        ///
        /// </summary>
        Errado = 179,

        /// <summary>
        ///
        /// </summary>
        Valorizado = 191,

        /// <summary>
        ///
        /// </summary>
        ParaValidar = 192,

        /// <summary>
        ///
        /// </summary>
        Aplicado = 202,

        /// <summary>
        /// Estado Emitido de un Lote de Facturacion
        /// </summary>
        Emitido = 190,

        /// <summary>
        /// Estado para representar un Error en la Emisión.
        /// </summary>
        ErrorEmision = 193,

        /// <summary>
        /// Estado para representar que faltan sectores por Emitir.
        /// </summary>
        EmisionIncompleta = 194,

        /// <summary>
        /// Estado EnLote del Suministro
        /// </summary>
        EnLote = 207,

        /// <summary>
        /// Estado Normal de Suministro luego que ha sido impreso
        /// </summary>
        Normal = 201,

        /// <summary>
        /// Estado del Suministro en Lote cuando va a pasar a Retiro
        /// </summary>
        PorRetirar = 280,

        /// <summary>
        /// -----------------------------------------------------------------------------------------
        /// Estados para el convenio.
        /// Rango del 140 - 149
        /// --
        /// Todo convenio que no necesita autorización nace con el estado: PorFirmar.
        /// </summary>
        PorFirmar = 140,

        /// <summary>
        /// Todo convenio que necesita autorización nace con el estado: PorAutorizar.
        /// </summary>
        PorAutorizar = 141,

        /// <summary>
        /// Cuando el saldo de un convenio es nuevamente financiado.
        /// </summary>
        Refinanciado = 142,

        /// <summary>
        /// Cuando se ha calculado las cuotas del convenio y se esta a la espera del OK de facturación.
        /// </summary>
        EnFacturacion = 143,

        /// <summary>
        /// Cuando se va a retirar al NroServicio por acumulación de 8 meses de deuda.
        /// </summary>
        VencidoPorIncumplimiento = 144,

        /// <summary>
        /// Cuando la autorización para hacer un convenio fuera de los parametros fue dada.
        /// </summary>
        Autorizado = 145,

        Robado = 216,

        /// <summary>
        /// --------------------------------------------
        /// Estados para La Solicitud Servicio
        /// Los valores no son correlativos por que estan en distintos grupos
        ///
        /// Primer estado de la Solicitud
        /// </summary>
        Solicitado = 206,

        /// <summary>
        /// Estado de atendido de la solicitud
        /// </summary>
        Atendido = 221,

        /// <summary>
        /// Estado en que el Servicio Nuevo Suministro pasa a Factible, despues de la evaluacion tecnica
        /// </summary>
        Factible = 222,

        /// <summary>
        /// Estado en que el Servicio Nuevo Suministro para a no factible cuando no es aprobada la evaluacion tecnica
        /// </summary>
        NoFactible = 223,

        /// <summary>
        /// Estado de la solicitud que se asigna despues de determinado la factibilidad
        /// </summary>
        Presupuestado = 224,

        /// <summary>
        /// Estado de la solicitud que se asigna despues que el cliente paga su presupuesto
        /// </summary>
        EnInstalacion = 225,

        /// <summary>
        /// Estado de la solicitud que se asigna cuando ya instalaron el suministro
        /// </summary>
        Instalado = 226,

        /// <summary>
        /// Estado de solicitud que se asigna cuando de la orden de factibilidad no se especifica si
        /// es factible o no la instalación.
        /// </summary>
        ConObservacion = 227,

        /// <summary>
        /// Estado que adopta la solicitud luego de imprimir el contrato.
        /// </summary>
        ConContrato = 228,

        /// <summary>
        /// Estado que adopta la solicitud se nuevo suministro luego de cancelar la orden de cobro.
        /// </summary>
        CanceladoConContrato = 229,

        /// <summary>
        /// Estado de la solicitud antes de generar el presupuesto.
        /// </summary>
        PorPresupuesto = 262,

        /// <summary>
        /// --------------------------------------------
        /// Estados para Atención a Clientes y Gestión
        /// de reclamos.
        /// Los valores no son correlativos por que estan en distintos grupos
        Finalizado = 220,

        /// <summary>
        /// Incida que el solicitante presento parte de los documentos
        /// necesarios para admitir la atención
        /// </summary>
        PreAdmisible = 282,

        /// <summary>
        /// Indica que el solicitante presento todos los documentos necesarios
        /// para admitir la atencion.
        /// </summary>
        Admisible = 251,

        /// <summary>
        /// Indica que el solicitante no presento todos los documentos necesarios
        /// para admitir la atencion.
        /// </summary>
        Inadmisible = 252,

        /// <summary>
        /// Indica que la atencion se ha declarado procedente para la atención.
        /// </summary>
        Procedente = 253,

        /// <summary>
        /// Indica que la atencion se ha declarado improcedente para la atención.
        /// </summary>
        Improcedente = 254,

        /// <summary>
        /// Se concilio un acuerdo entre las partes tras el proceso de reclamo.
        /// </summary>
        Conciliado = 255,

        /// <summary>
        /// No se concilio un acuerdo entre las partes tras el proceso de reclamo.
        /// </summary>
        NoConciliado = 256,

        /// <summary>
        /// Se concilio un acuerdo parcial entre las partes tras el proceso de reclamo.
        /// </summary>
        ConciliadoParcial = 257,

        /// <summary>
        /// Se declaro fundado el reclamo en la resolución determinada por la concesionaria.
        /// </summary>
        Fundado = 258,

        /// <summary>
        /// Se declaro infundado el reclamo en la resolución determinada por la concesionaria.
        /// </summary>
        Infundado = 259,

        /// <summary>
        /// Se declaro fundado en parte el reclamo en la resolución determinada por la concesionaria.
        /// </summary>
        FundadoenParte = 260,

        /// <summary>
        /// El reclamo fue reconsiderado por la concesionario a petición del solicitante.
        /// </summary>
        Reconsiderado = 261,

        /// <summary>
        /// La solución del motivo es justificada.
        /// </summary>
        Justificado = 263,

        /// <summary>
        /// La solución del motivo es insjustificada.
        /// </summary>
        Injustificado = 264,

        /// <summary>
        /// La solucion del motivo es consultado.
        /// </summary>
        Consultado = 265,

        /// <summary>
        /// Sin Acta de Solución
        /// </summary>
        SinActaSolución = 266,

        /// <summary>
        /// Resolucion emitida por osinerg
        /// </summary>
        Nula = 272,

        /// <summary>
        /// No Asistió a Cita.
        /// </summary>
        NoAsistio = 277,

        /// <summary>
        /// --------------------------------------------
        /// Estados para Ordenes de Trabajo.
        /// Los valores no son correlativos por que estan en distintos grupos
        /// </summary>
        NoEjecutado = 240,

        /* Estado Pronunciamiento Ente Regulador */

        /// <summary>
        ///
        /// </summary>
        Revocar = 273,

        /// <summary>
        ///
        /// </summary>
        RevocarEnParte = 274,

        /// <summary>
        ///
        /// </summary>
        CareceDeObjeto = 275,

        /// <summary>
        ///
        /// </summary>
        Rescindido = 281,

        /// <summary>
        ///
        /// </summary>
        Acumulado = 283,

        /// <summary>
        ///
        /// </summary>
        EnTransicion = 284,

        /// <summary>
        ///
        /// </summary>
        Usado = 211,

        /// <summary>
        ///
        /// </summary>
        NoDisponible = 217,

        /// <summary>
        ///
        /// </summary>
        Ponderado = 205,

        /// <summary>
        ///
        /// </summary>
        ///

        ConfirmarParte = 276,

        /// <summary>
        ///
        /// </summary>
        PCPropuesta = 287,

        /// <summary>
        ///
        /// </summary>
        PCPreAutorizado = 288,

        /// <summary>
        ///
        /// </summary>
        PCAutorizado = 289,

        /// <summary>
        ///
        /// </summary>
        Grabando = 290,

        /// <summary>
        ///
        /// </summary>
        EnLlamada = 291,

        /// <summary>
        /// Estado para los lotes de Facturación que cuando se encuentran
        /// preparados para valorizar.
        /// </summary>
        ParaValorizar = 295,

        /// <summary>
        /// Estado para los lotes de Facturación para cuando se
        /// encuentran en proceso de Valorización.
        /// </summary>
        EnValorizacion = 296,

        /// <summary>
        /// Estado para los lotes de Facturación para cuando se
        /// encuentran en proceso de Emisión.
        /// </summary>
        EnEmision = 297,

        /// <summary>
        ///
        /// </summary>
        Desistido = 301,

        /// <summary>
        ///
        /// </summary>
        Inspeccionado = 320,

        /// <summary>
        ///
        /// </summary>
        EnIntervención = 321,

        /// <summary>
        ///
        /// </summary>
        Intervenido = 322,

        /// <summary>
        ///
        /// </summary>
        ConCodigoAutogenerado = 323,

        /// <summary>
        ///
        /// </summary>
        Pactado = 324,

        /// <summary>
        ///
        /// </summary>
        Calculado = 326,

        /// <summary>
        ///
        /// </summary>
        PagadoParcial = 330,

        /// <summary>
        ///
        /// </summary>
        PagoDeudaFacturada = 331,

        /// <summary>
        ///
        /// </summary>
        Distribuido = 332,

        /// <summary>
        ///
        /// </summary>
        Sustentado = 333,

        /// <summary>
        ///
        /// </summary>
        CargadoSAP = 334,

        // Estado de llamadas al CallCenter

        NoAtendida = 336,

        NoContestada = 337,
        NoCompensa = 42,
        SiCompensa = 43,
        NoDefinido = 44,
        EnTramite = 342,

        // Medidor

        Nuevo = 210,
        Adecuado = 212,
        Reciclado = 213,
        Decomisado = 214,
        DadoDeBaja = 215,
        Calidad = 310,
        MalaCalidad = 311,

        ConFiltro = 344,
        Confirmado = 345,

        EnReclamo = 335,

        PorNotifPreviaIntervencion = 346,
        NotifPreviaIntervencion = 347,
        PorNotifContraste = 348,
        NotifContraste = 349,
        PorContraste = 350,
        Contrastado = 351,
        PorNotifReintegroRecupero = 352,
        NotifReintegroRecupero = 353,
        Publicado = 338,
        PorConciliar = 51,

        PorConciliarFormaPago = 355,
        ConciliadoFormaPago = 356,

        NoSolicitado = 354,
        PorGenerarOC = 361,
        PorTransferir = 365,
        ConInforme = 371,
        Programado = 357,
        Confirmar = 267,
        Excluido = 380,
        AnalisisPruebas = 382,
        Personal = 383,
        Impreso = 384,
        Guardado = 385
    }

    public enum enumTipoIntercambio
    {
        Envio = 1,
        Recepcion = 2,
    }

    public enum Cartera
    {
        Todos = 0,
        Comun = 1,
        Mayor = 2,
        Ninguno = 3
    }

    public enum TipoConexionOLEDB
    {
        OleDbDBF = 1
    }
    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
    public enum TipoOrdenCobro
    {
        Normal = 1,
        CuotaInicialConvenio = 2,
        VentaPrepago = 3,
        NuevosSuministros = 4,
        VentasExtraordinarias = 5
    }

    public enum enumModalidadTransaccion
    {
        AdvertirVariacionSaldo,
        ForzarGrabacion
    }
    /*
    public enum TipoOperacionCobranza
    {
        Recaudacion = 1,
        Extorno = 2,
        Remito = 3,
        Derivado = 4
    }
    */
    public struct CacheConfiguracion
    {
        public const Int16 IdServicioEnergia = 1;
        public const Int16 IdFlujoFacturacion = 1;
        public const Int16 IdMonedaNacional = 1;
        public const Int16 IdOperacionCobranza = 1;
        public const Int16 IdOperacionExtorno = 2;
        public const Int16 IdDC_BoletaVenta = 2;
        public const Int16 IdDC_Factura = 2;
        public const Int16 IdFlujoAtencionCliente = 3;
        public const Int16 IdMC_FacilidadPago = 11;
        public const Int16 IdMC_ConvenioSolicitudServicio = 18;
        public const Int16 IdDC_ContratoSuministroEnergiaElectrica = 46;
        public const Int16 IdFlujoVentaNuevoServicio = 4;
        public const string RutaAlmacenarComprobanteXMLENOSA = "\\DTLCDPOPTBD03\\Descargas\\ArchivosXML\\ENOSA\\";
        public const string CertificadoDigitalENOSA = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";
        public const string ClaveCertificadoDigitalENOSA = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";

        public const string RutaAlmacenarComprobanteXMLENSA = "\\DTLCDPOPTBD03\\Descargas\\ArchivosXML\\ENOSA\\";
        public const string CertificadoDigitalENSA = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";
        public const string ClaveCertificadoDigitalENSA = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";

        public const string RutaAlmacenarComprobanteXMLHDNA = "\\DTLCDPOPTBD03\\Descargas\\ArchivosXML\\ENOSA\\";
        public const string CertificadoDigitalHDNA = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";
        public const string ClaveCertificadoDigitalHDNA = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";

        public const string RutaAlmacenarComprobanteXMLELCTRO = "\\DTLCDPOPTBD03\\Descargas\\ArchivosXML\\ENOSA\\";
        public const string CertificadoDigitalELCTRO = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";
        public const string ClaveCertificadoDigitalELCTRO = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";

        public const string RutaAlmacenarComprobanteXMLELOR = "\\DTLCDPOPTBD03\\Descargas\\ArchivosXML\\ENOSA\\";
        public const string CertificadoDigitalELOR = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";
        public const string ClaveCertificadoDigitalELOR = "\\DTLCDPOPTBD03\\Descargas\\CertificadosDigitales\\ENOSA\\llaveprivada2018.pfx";





    }

    public struct ConfiguracionGlobal
    {
        public const string VideoFinanciamiento = "RutaVideoFinanciamiento";
        public const string TerminosCondicionesFinanciamiento = "RutaTerminosCondicionesFinanciamiento";
        public const string TerminosCondicionesFinanciamientoNiubiz = "TerminosCondicionesFinanciamientoNiubiz";
        public const string MaximoFinanciamientoOficinaVirtual = "MaximoFinanciamientoOficinaVirtual";
        public const string RefinanciamientoVirtual = "RefVirt";
        public const string RutaAlmacenarComprobantePDFFinanciamiento = "RutaAlmacenarComprobantePDFFinanciamiento";
        public const string RutaServidorDocumentoFinanciamiento = "RutaServidorDocumentoFinanciamiento";
        public const string DireccionNuevaENSA = "DireccionENSADocumentos";

    }

    public struct ConfigurationManagerNGC
    {
        public const int prepagoActivarInterfase = 0;
        public const string prepagoUserName = "CDU";
        public const string prepagoClave = "123456";
        public const string prepagoClientIP = "http://10.237.2.225/Service.asmx";
        public const string prepagoServiceID = "";
        public const string ServiceID = "";
        public const string prepagoUserType = "1";
        public const string prepagoID = "0";
        public const int supplyGroupCode = 901156;
        public const int tarrifIndex = 1;
        public const int kv = 1;
        public const int ke = 255;
        public const int creditFunction = 0;
        public const string mensajeErrorCliente = "Ha ocurrido un error en el proceso. Contacte al personal de Soporte de Aplicaciones.";
        public const string userinterfase = "interfasengc";
        public const string passinterfase = "iNt3rfAs3%nGc";
        public const int tiempoEsperaConexionDB = 20;
    }

    public enum enumDocumentoComercial
    {
        efectivo = 1,
        BoletadeVenta = 2,
        Factura = 3,
        Ticket = 4,
        NotadeCredito = 5,
        NotadeDebito = 6,
        Recibodeenergia = 7,
        Cheque = 8,
        Temporalagua = 10,
        ConveniodePago = 11,
        PagoconTarjeta = 12,
        ordendePago = 16,
        DepositoBancario = 18,
        NotadeingresoaCaja = 22,
        Reciboagua = 23,
        RecibodeTelefono = 24,
        RecibodeCableMagico = 25,
        ReciboTercerosint = 28,
        ordendeTrabajo = 29,
        SolicitudPonderacion = 31,
        DepositoenGarantia = 33,
        ordendePagoCi = 37,
        Cuotainicialdeconvenio = 38,
        SolicitudNuevoSuministro = 39,
        Boletadeatencion = 41,
        Reclamo = 43,
        actadeConformidadconacuerdodePartes = 44,
        interesesDeudaenergia = 45,
        ContratoSuministroenergiaelectrica = 46,
        ConveniodeTerceros = 47,
        Resoluciondeimprocedencia = 48,
        actadeConformidadconacuerdoParcial = 49,
        actadeConformidadsinacuerdodePartes = 50,
        ResolucionFundada = 51,
        Resolucioninfundada = 52,
        ResolucionFundadaenParte = 53,
        actadeConciliacionAudienciaespecial = 61,
        SubcripcionConvenios = 62,
        Presupuesto = 63,
        NotificaciondeRetirodeServicio = 64,
        Resoluciondeinadmisibilidad = 65,
        actadeacuerdodentrodelos3dias = 66,
        actadeacuerdodentrodelos8dias = 67,
        actadeaudienciaconacuerdodepartesoconacuerdoparcial = 68,
        actadeaudienciasinacuerdodepartes = 69,
        actadeaudienciaunicaconinasistenciadealgunadelaspartes = 70,
        Notificacionparacontroldeadmisibilidad = 71,
        Citacionparatratodirectoasolicituddelaempresa = 72,
        Respuestaaserviciosysolicitudes = 73,

        //actadeaudienciaconacuerdodepartesoconacuerdoparcial = 78,
        //actadeaudienciasinacuerdodepartes = 81,
        //actadeaudienciaunicaconinasistenciadealgunadelaspartes = 82,
        //Notificacionparacontroldeadmisibilidad = 83,
        //Citacionparatratodirectoasolicituddelaempresa = 84,
        //Respuestaaserviciosysolicitudes = 85,
        ResoluciondeContrato = 86,

        Notificacionparaentregaderesoluciones = 87,
        Notadeingreso = 88,
        NotadeSalida = 89,
        NotadeTransferencia = 91,
        DocumentointernodeSalidadeMedidores = 92,
        NotadeRecepcion = 93,
        NotadeDespacho = 94,
        Citacionparaaudienciaespecial = 95,
        NotificacionporVencimientodeContrato = 96,
        NotificacionpreControldeadmisibilidad = 98,
        Provision = 99,
        Castigo = 100,
        Redondeo = 101,
        VoucherContable = 102,
        ReciboDuplicado = 103,
        actadeinstalaciondeNuevosSuministros = 104,
        Contratodealumbradocomplementario = 105,
        actadeacuerdo = 106,
        actadeinspeccion = 107,
        ResoluciondeReclamo = 108,
        FormaciondeexpedientedeReclamo = 109,
        actadeTrabajo = 110,
        SolicituddeServicio = 114,
        Citacionparatratodirectoasolicituddelreclamante = 115,
        CitacionasolicituddelreclamanteIncluyeempresas = 119,
        CitacionasolicituddelaempresaIncluyeempresas = 120,
        BoletadeQueja = 121,
        Boletadeapelacion = 122,
        MedidaCautelar = 123,
        ResolucionoSiNeRGMiN = 124,
        actadeCambiodeMedidor = 125,
        edicto = 126,
        actadeRetirodeMedidor = 127,
        actadeDenunciadealumbradoPublico = 128,
        CartadeReintegrosCausali = 129,
        CartadeReintegrosCausalii = 130,
        CartadeReintegrosCausaliii = 131,
        CartadeRecuperoCausali = 132,
        CartadeRecuperoCausalii = 133,
        CartadeRecuperoCausaliii = 134,
        CartadeRecuperoCausaliV = 135,
        CartadeRecuperoCausalV = 136,
        NotificacionCulminacionContratoenerelect = 137,
        MovimientodeaV = 138,
        Morosidad = 139,
        ResoluciondeSuspension = 140,
        NotificacionResoluciondeSuspension = 141,
        ControldeRegistrodeinterrupciones = 142,
        aporteLey = 143,
        Cobranzaadinelsa = 144,
        CobranzaGenerico = 145,
        alquilerequiposMateriales = 146,
        uTeFoNaVi = 147,
        VoucherDctoPlanilla = 148,
        actaNotificacionintervencion = 149,
        actaintervencion = 150,
        actaNotificacionContraste = 151,
        actaContraste = 152,
        actaNotificacionReintegroRecupero = 153,
        NotificaciondeMedicionNTCSe = 154,
        TarjetadeDebito = 155,
        DocumentodeDescuento = 156,
        SobranteFaltante = 157,
        ContratoSuministroenergiaelectricaDNi = 158,
        RegularizacionFacturacionNegativo = 159,
        Notadeabono = 160,
        CorteyReconexion = 161,
        encargoTerceros = 162,
        ReciboPrepago = 163,
        ComprobantedeRetencion = 164,
        CartadeRecuperodeenergia = 165,
        CartadeReintegrodeenergia = 166,
        CartadeRecuperodeenergiaMayores = 167,
        CartadeReintegrodeenergiaMayores = 168,
        NotadeabonoporCastigo = 169,
        PiNNTCSe = 170,
        RiNNTCSe = 171,
        RDiNTCSe = 172,
        RiMNTCSe = 173,
        Resolucionacumulada = 174,
        EmpresasContrastadoras = 177,
        CargoNotificacion = 178,
        actadeConformidadconacuerdodePartesTelefono = 183,

        CartaReintegrosMayoresCausal_I_II_III = 198,
        CartaRecuperosMayoresCausal_I_II_III = 199,
        CartaRecuperosMayoresCausal_IV_V = 200,

        CartaEmpresasContrastadoras = 205
    }

    public enum enumMotivoConstitucion
    {
        ReclamoProcesoJudicial = 5,
        PagoCuotaInicialFraccionada = 7,
        GarantiaServicioVentaEspecifica = 9,
        PagosDobles = 10,
        Refacturacion = 11,
        ProcesoManualDocumentos = 12,
        CastigoDeuda = 13,
        EmisionReciboNegativo = 14,
        ReintegroDescuentoRecibo = 15,
        ReintegroDevolucionEfectivo = 16
    }

    public enum TipoAvisoNG
    {
        /// <summary>
        ///
        /// </summary>
        Informativo = 1,

        /// <summary>
        ///
        /// </summary>
        Confirmacion = 2,

        /// <summary>
        ///
        /// </summary>
        Advertencia = 3,

        /// <summary>
        ///
        /// </summary>
        Error = 4,

        /// <summary>
        ///
        /// </summary>
        Espera = 5,
    }

    public enum Servicio
    {
        EnergiaPostpago = 1,
        RecaudacionAgua = 2,
        RecaudacionTelefono = 3,
        VentaNuevoSuministro = 4,
        FacturacionFiliales = 5,
        AlquilerPostes = 6,
        VentasExtraordinarias = 7,
        RecaudacionCableMagico = 8,
        EnergiaLibre = 10,
        EnergiaPrepago = 11,
        SuministrosTemporales = 12,
        ValidacionDatosLinea = 13,
        SuministrosProvisionales = 14,
        ServicioFISE = 17,
        ServicioFacturacionAdministrativa = 18,
    }

    public enum enumTipoRelacion
    {
        Contacto = 1
      , Hijo = 2
      , Propietario = 12
      , GerenteGeneral = 14
      , Apoderado = 16
      , RepresentanteLegal = 17
      , RepresentanteAdmin = 18
      , Encargado = 22
      , ContactoDePago = 28
      , ContactoTecnico = 29
      , Ttular = 43
    }



    public enum enumConsultaOperacionesSuministro
    {
        Cortes = 20,
        Reconexiones = 25,
        Retiros = 47,
        Reaperturas = 51,
        SupervisionCortes = 46,
        Todos = 0
    }

    public enum TipoFlujo
    {
        Facturacion = 1,
        Recaudacion = 2,
        AtencionCliente = 3,
        NuevoSuministro = 4,
        AtencionReclamos = 5,
        AsientoContable = 6,
        ReintegrosRecuperos = 7,
        NTCSECalidadSuministro = 8,
        NTCSECalidadProducto = 9,
    }

    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL

}