﻿using System;
using System.Collections.Generic;
using System.Data;

namespace SISAC.Entidad.Maestro
{
    #region clsException

    public class clsException : clsBase
    {
        #region Properties

        public Int32 IdException { get; set; }

        public String Mensaje { get; set; }

        public DateTime FechaRegistro { get; set; }

        public String Metodo { get; set; }

        public String Servicio { get; set; }

        public String Host { get; set; }

        public String DataIN { get; set; }

        public String DataOUT { get; set; }

        //Extra

        public Int16 IdAppBD { get; set; }

        #endregion Properties

        #region Constructors

        public clsException()
        {
        }

        public clsException(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdException") && !row.IsNull("IdException")) IdException = Convert.ToInt32(row["IdException"]);
            if (columns.Contains("Mensaje") && !row.IsNull("Mensaje")) Mensaje = Convert.ToString(row["Mensaje"]);
            if (columns.Contains("FechaRegistro") && !row.IsNull("FechaRegistro")) FechaRegistro = Convert.ToDateTime(row["FechaRegistro"]);
            if (columns.Contains("Metodo") && !row.IsNull("Metodo")) Metodo = Convert.ToString(row["Metodo"]);
            if (columns.Contains("Servicio") && !row.IsNull("Servicio")) Servicio = Convert.ToString(row["Servicio"]);
            if (columns.Contains("IdAplicacion") && !row.IsNull("IdAplicacion")) IdAplicacion = Convert.ToInt16(row["IdAplicacion"]);
            if (columns.Contains("Host") && !row.IsNull("Host")) Host = Convert.ToString(row["Host"]);
            if (columns.Contains("DataIN") && !row.IsNull("DataIN")) DataIN = Convert.ToString(row["DataIN"]);
            if (columns.Contains("DataOUT") && !row.IsNull("DataOUT")) DataOUT = Convert.ToString(row["DataOUT"]);
        }

        #endregion Constructors
    }

    #endregion clsException

    #region clsListaException

    public class clsListaException
    {
        #region Properties

        public List<clsException> Elementos { get; set; }

        #endregion Properties

        #region Constructors

        public clsListaException()
        {
            Elementos = new List<clsException>();
        }

        public clsListaException(DataTable table)
        {
            Elementos = new List<clsException>();

            if (table == null) return;

            foreach (DataRow row in table.Rows) Elementos.Add(new clsException(row));
        }

        #endregion Constructors
    }

    #endregion clsListaException
}