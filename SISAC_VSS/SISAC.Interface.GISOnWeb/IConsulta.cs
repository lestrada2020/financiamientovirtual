﻿using SISAC.Entidad.Maestro;
using System.Web.Http;

namespace SISAC.Interface.GISOnWeb
{
    public interface IConsulta
    {
        IHttpActionResult CapaListarGeografia(clsParametro param);

        IHttpActionResult Escalaslistar(clsParametro param);

        IHttpActionResult CapaListarEscalas(clsParametro param);

        IHttpActionResult EntidadListaBuscar(clsParametro param);

        IHttpActionResult EntidadListaPropiedad(clsParametro param);

        IHttpActionResult UsuarioValida(clsParametro param);

        IHttpActionResult UsuarioPerfil(clsParametro param);

        IHttpActionResult Poligonos(clsParametro param);

        IHttpActionResult Lineas(clsParametro param);

        IHttpActionResult Puntos(clsParametro param);

        IHttpActionResult EntidadBuscar(clsParametro param);

        IHttpActionResult EntidadListar(clsParametro param);

        IHttpActionResult EliminarCache(clsParametro param);
        IHttpActionResult EntidadListarLeyenda(clsParametro param);

    }
}