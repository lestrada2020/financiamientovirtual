﻿using SISAC.Data.Proceso;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.ServiceProcess;
using System.Threading;

namespace SISAC.Negocio.Proceso
{
    /// <summary>
    /// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
    /// </summary>
    public sealed class ClsNegocioTarea
    {
        #region Field

        //private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly Lazy<ClsNegocioTarea> _Instancia = new Lazy<ClsNegocioTarea>(() => new ClsNegocioTarea(), LazyThreadSafetyMode.PublicationOnly);
        private static readonly ClsTareaDAO _TareaDAO = ClsTareaDAO.Instancia;
        private ServiceController _objcontroladorservicio;

        #endregion

        #region Property

        public static ClsNegocioTarea Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion

        #region Constructor

        private ClsNegocioTarea()
        {

        }

        #endregion

        #region Private

        private int CrearProgramarTarea(ClsTareaProgramacion tareaProgramacion)
        {
            #region Codigo de referencia

            //Int32 idtarea = clsProcesadorTareaDAO.ObtenerInstancia().CrearProgramarTarea(tareaprogramacion);

            #endregion

            int idTarea = _TareaDAO.CrearProgramarTarea(tareaProgramacion);

            //DONE: Activar cuando se tenga la Tarea de Windows configurada
            if (tareaProgramacion.Frecuencia == Convert.ToInt16(1))
                EjecutarTareaInmediata(tareaProgramacion, idTarea);

            return idTarea;
        }

        private void EjecutarTareaInmediata(ClsTareaProgramacion tareaprogramacion, int idtarea)
        {
            // Validar que el servicio se encuentre operativo.
            try
            {
                _objcontroladorservicio = ObtenerControlador(tareaprogramacion.IdProceso, idtarea);
            }
            catch (Exception)
            {
                EliminarTarea(idtarea);

                throw;
            }

            // Validar que el servicio pueda ejecutar la tarea.
            try
            {
                if (_objcontroladorservicio.Status == ServiceControllerStatus.Stopped)
                    _objcontroladorservicio.Start();

                if (_objcontroladorservicio != null) _objcontroladorservicio.ExecuteCommand((int)EstadoNGC.Ejecutando);
            }
            catch (Exception ex)
            {
                EliminarTarea(idtarea);

                throw;

                #region Codigo de referencia

                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, false);
                //throw new Exception(string.Format("<<<El servicio {0} no se encuentra disponible para Ejecutar el proceso: {1}.  Favor reintente en unos minutos.>>>", _objcontroladorservicio.ServiceName, ex.Message));

                #endregion
            }
        }

        private ServiceController ObtenerControlador(int idproceso, int idtarea)
        {
            ServiceController _controladorServicio = null;

            string nombreServicio = string.Empty;
            string nombreServidor = string.Empty;

            // obtener el nombre del servicio y servidor que ejecutará el proceso.
            //clsProcesadorTareaDAO.ObtenerInstancia().ObtenerDatoServicio(idtarea, ref nombreServicio, ref nombreServidor);
            _TareaDAO.ObtenerDatoServicio(idtarea, ref nombreServicio, ref nombreServidor);

            if (string.IsNullOrEmpty(nombreServicio))
                throw new Exception(string.Format("No se ha definido nombre del servicio de tarea para el ID proceso: {0}", idproceso));

            if (string.IsNullOrEmpty(nombreServidor))
                throw new Exception(string.Format("No se ha definido nombre del servidor de tarea para el ID proceso: {0}", idproceso));

            try
            {
                _controladorServicio = new ServiceController(nombreServicio, nombreServidor);
            }
            catch (Exception ex)
            {
                //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error, false);
                throw new Exception(string.Format("El servicio {0} no se encuentra disponible.  Favor reintenar en unos minutos.", nombreServicio));
            }

            if (_controladorServicio == null)
                throw new Exception(string.Format("No se ha logrado obtener el controlador del servicio {0}", nombreServicio));

            return _controladorServicio;
        }

        private void EliminarTarea(int idtarea)
        {
            _TareaDAO.EliminarTarea(idtarea);
        }

        #endregion

        #region Public

        /// <summary>
        /// Permite crear una tarea para procesamiento en servidor de aplicaciones. 
        /// Para uso por medio de otros objetos de Negocio, no directamente.
        /// </summary>
        /// <param name="tarea"></param>
        /// <returns></returns>
        public int GrabarTareaInmediata(ClsTareaProgramacion tarea)
        {
            try
            {
                DateTime fechaactual = DateTime.Now;

                tarea.FechaInicioProgramada = fechaactual;
                tarea.FechaProgramacion = fechaactual;
                tarea.HoraInicio = fechaactual;
                if (tarea.Frecuencia == 0)
                {
                    tarea.Frecuencia = 1;
                }
                tarea.FechaFinProgramada = null;
                return CrearProgramarTarea(tarea);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
