﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.GISOnWeb;
using SISAC.Util.Tool;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace SISAC.Proxy.GISOnWeb.Controllers
{
    public class ConsultaController : ApiController, IConsulta
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteGisOnWeb";
                String controller = "Consulta";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public IHttpActionResult CapaListarGeografia(clsParametro param)
        {
            var result = SendData<clsResultado>("CapaListarGeografia", param);

            return Ok(result);
        }

        public IHttpActionResult Escalaslistar(clsParametro param)
        {
            var result = SendData<clsResultado>("Escalaslistar", param);

            return Ok(result);
        }

        public IHttpActionResult CapaListarEscalas(clsParametro param)
        {
            var result = SendData<clsResultado>("CapaListarEscalas", param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListaBuscar(clsParametro param)
        {
            var result = SendData<clsResultado>("EntidadListaBuscar", param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListaPropiedad(clsParametro param)
        {
            var result = SendData<clsResultado>("EntidadListaPropiedad", param);

            return Ok(result);
        }

        public IHttpActionResult UsuarioValida(clsParametro param)
        {
            var result = SendData<clsResultado>("UsuarioValida", param);

            return Ok(result);
        }

        public IHttpActionResult UsuarioPerfil(clsParametro param)
        {
            var result = SendData<clsResultado>("UsuarioPerfil", param);

            return Ok(result);
        }

        public IHttpActionResult Poligonos(clsParametro param)
        {
            var result = SendData<clsResultado>("Poligonos", param);

            return Ok(result);
        }

        public IHttpActionResult Lineas(clsParametro param)
        {
            var result = SendData<clsResultado>("Lineas", param);

            return Ok(result);
        }

        public IHttpActionResult Puntos(clsParametro param)
        {
            var result = SendData<clsResultado>("Puntos", param);

            return Ok(result);
        }

        public IHttpActionResult EntidadBuscar(clsParametro param)
        {
            var result = SendData<clsResultado>("EntidadBuscar", param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListar(clsParametro param)
        {
            var result = SendData<clsResultado>("EntidadListar", param);

            return Ok(result);
        }

        public IHttpActionResult EliminarCache(clsParametro param)
        {
            var result = SendData<clsResultado>("EliminarCache", param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListarLeyenda(clsParametro param)
        {
            var result = SendData<clsResultado>("EntidadListarLeyenda", param);

            return Ok(result);
        }

        #endregion Public
    }
}