﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Integracion.Controllers
{
    public class OsceController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;

        #endregion Field

        #region Private

        private async Task<clsResultado> SendData(String action, clsParametro param, String fields = "", String queryInitial = "out=json")
        {
            try
            {
                var result = _Util.ValidateTokenInConfig<clsResultado>("SessionToken", param.Parametros.ContainsKey("SessionToken") ? param.Parametros["SessionToken"] : "");

                if (result.IdError > 0) return result;
                if (!action.Contains("/")) action = "Osce/" + action;

                var query = param.Parametros.ToQueryString(fields, queryInitial);
                var msg = await _Service.GetAsync("SitePide", action, query);
                var data = (Object)null;

                if (msg.StartsWith("[")) data = _Convert.ToObject<List<Dictionary<String, Object>>>(msg);
                else data = _Convert.ToObject<Dictionary<String, Object>>(msg);

                result = new clsResultado() { Datos = data };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> VigenteProveedor(clsParametro param)
        {
            var result = await SendData("VigenteProveedor", param, "ruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> VigenteSancion(clsParametro param)
        {
            var result = await SendData("VigenteSancion", param, "ruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> SeleccionXExpediente(clsParametro param)
        {
            var result = await SendData("ROsce/SeleccionXExpediente", param, "idExpediente");

            return Ok(result);
        }

        public async Task<IHttpActionResult> SeleccionXRuc(clsParametro param)
        {
            var result = await SendData("ROsce/SeleccionXRuc", param, "ruc;anio;numMes");

            return Ok(result);
        }

        public async Task<IHttpActionResult> AdjudicadoXExpediente(clsParametro param)
        {
            var result = await SendData("ROsce/AdjudicadoXExpediente", param, "idExpediente");

            return Ok(result);
        }

        public async Task<IHttpActionResult> AdjudicadoXRuc(clsParametro param)
        {
            var result = await SendData("ROsce/AdjudicadoXRuc", param, "ruc;anio");

            return Ok(result);
        }

        #endregion Public
    }
}