﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Integracion.Controllers
{
    public class SunatController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;

        #endregion Field

        #region Private

        private async Task<clsResultado> SendData(String action, clsParametro param, String fields = "", String queryInitial = "out=json")
        {
            try
            {
                var result = _Util.ValidateTokenInConfig<clsResultado>("SessionToken", param.Parametros.ContainsKey("SessionToken") ? param.Parametros["SessionToken"] : "");

                if (result.IdError > 0) return result;
                if (!action.Contains("/")) action = "Sunat/" + action;

                var query = param.Parametros.ToQueryString(fields, queryInitial);
                var msg = await _Service.GetAsync("SitePide", action, query);
                var data = (Object)null;

                if (msg.StartsWith("[")) data = _Convert.ToObject<List<Dictionary<String, Object>>>(msg);
                else data = _Convert.ToObject<Dictionary<String, Object>>(msg);

                result = new clsResultado() { Datos = data };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> AnyService(clsParametro param)
        {
            var result = "";

            try
            {
                var valid = _Util.ValidateTokenInConfig<clsResultado>("SessionToken", param.Parametros.ContainsKey("SessionToken") ? param.Parametros["SessionToken"] : "");

                if (valid.IdError > 0) throw new Exception(valid.Mensaje);

                result = await _Service.GetAsync(param.Parametros["URL"]);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return Ok(result);
        }

        public async Task<IHttpActionResult> DatosPrincipales(clsParametro param)
        {
            var result = await SendData("DatosPrincipales", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> DatosSecundarios(clsParametro param)
        {
            var result = await SendData("DatosSecundarios", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> DatosT1144(clsParametro param)
        {
            var result = await SendData("DatosT1144", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> DatosT362(clsParametro param)
        {
            var result = await SendData("DatosT362", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> DomicilioLegal(clsParametro param)
        {
            var result = await SendData("DomicilioLegal", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> EstablecimientosAnexos(clsParametro param)
        {
            var result = await SendData("EstablecimientosAnexos", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> EstAnexosT1150(clsParametro param)
        {
            var result = await SendData("EstAnexosT1150", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> RepLegales(clsParametro param)
        {
            var result = await SendData("RepLegales", param, "numruc");

            return Ok(result);
        }

        public async Task<IHttpActionResult> RazonSocial(clsParametro param)
        {
            var result = await SendData("RazonSocial", param, "RSocial");

            return Ok(result);
        }

        #endregion Public
    }
}