﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Integracion.Controllers
{
    public class SbsController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;

        #endregion Field

        #region Private

        private async Task<clsResultado> SendData(String action, clsParametro param, String fields = "", String queryInitial = "out=json")
        {
            try
            {
                var result = _Util.ValidateTokenInConfig<clsResultado>("SessionToken", param.Parametros.ContainsKey("SessionToken") ? param.Parametros["SessionToken"] : "");

                if (result.IdError > 0) return result;
                if (!action.Contains("/")) action = "Sbs/" + action;

                var query = param.Parametros.ToQueryString(fields, queryInitial);
                var msg = await _Service.GetAsync("SitePide", action, query);
                var data = (Object)null;

                if (msg.StartsWith("[")) data = _Convert.ToObject<List<Dictionary<String, Object>>>(msg);
                else data = _Convert.ToObject<Dictionary<String, Object>>(msg);

                result = new clsResultado() { Datos = data };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> ContableFecha(clsParametro param)
        {
            var result = await SendData("ContableFecha", param, "fecha");

            return Ok(result);
        }

        public async Task<IHttpActionResult> ContableActual(clsParametro param)
        {
            var result = await SendData("ContableActual", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> UltimaFecha(clsParametro param)
        {
            var result = await SendData("UltimaFecha", param);

            return Ok(result);
        }

        #endregion Public
    }
}