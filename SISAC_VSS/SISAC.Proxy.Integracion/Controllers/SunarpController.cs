﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Integracion.Controllers
{
    public class SunarpController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;

        #endregion Field

        #region Private

        private async Task<clsResultado> SendData(String action, clsParametro param, String fields = "", String queryInitial = "out=json")
        {
            try
            {
                var result = _Util.ValidateTokenInConfig<clsResultado>("SessionToken", param.Parametros.ContainsKey("SessionToken") ? param.Parametros["SessionToken"] : "");

                if (result.IdError > 0) return result;
                if (!action.Contains("/")) action = "Sunarp/" + action;

                var query = param.Parametros.ToQueryString(fields, queryInitial);
                var msg = await _Service.GetAsync("SitePide", action, query);
                var data = (Object)null;

                if (msg.StartsWith("[")) data = _Convert.ToObject<List<Dictionary<String, Object>>>(msg);
                else data = _Convert.ToObject<Dictionary<String, Object>>(msg);

                result = new clsResultado() { Datos = data };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> NaveAeronave(clsParametro param)
        {
            var result = await SendData("NaveAeronave", param, "numeroMatricula");

            return Ok(result);
        }

        public async Task<IHttpActionResult> PJRazonSocial(clsParametro param)
        {
            var result = await SendData("PJRazonSocial", param, "razonSocial");

            return Ok(result);
        }

        public async Task<IHttpActionResult> TitularidadNatural(clsParametro param)
        {
            var result = await SendData("Titularidad", param, "apellidoPaterno;apellidoMaterno;nombres", "out=json&tipoParticipante=N&razonSocial=");

            return Ok(result);
        }

        public async Task<IHttpActionResult> TitularidadJuridica(clsParametro param)
        {
            var result = await SendData("Titularidad", param, "razonSocial", "out=json&tipoParticipante=J&apellidoPaterno=&apellidoMaterno=&nombres=");

            return Ok(result);
        }

        public async Task<IHttpActionResult> Oficinas(clsParametro param)
        {
            var result = await SendData("Oficinas", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListarAsientos(clsParametro param)
        {
            var result = await SendData("ListarAsientos", param, "zona;oficina;partida;registro");

            return Ok(result);
        }

        public async Task<IHttpActionResult> VerAsientos(clsParametro param)
        {
            var result = await SendData("VerAsientos", param, "transaccion;idImg;tipo;nroTotalPag;nroPagRef;pagina");

            return Ok(result);
        }

        public async Task<IHttpActionResult> VerDetalleRPV(clsParametro param)
        {
            var result = await SendData("VerDetalleRPV", param, "zona;oficina;placa");

            return Ok(result);
        }

        #endregion Public
    }
}