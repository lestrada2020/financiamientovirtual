﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using SISAC.Entidad.Atencion;
using Microsoft.Reporting.WinForms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;

namespace SISAC.Util.Tool
{
    public class clsUtilFinanciamiento
    {
        public static string ObtenerRutRDLCComprobantePago(short idTipoDocumento)
        {
            string nombreRdlUsar = "";

            switch (idTipoDocumento)
            {
                case (Int16)TipoDocumentoComercial.BoletaVenta:

                    nombreRdlUsar = "SISAC.Util.Tool.ComprobanteElectronico.rptBoletaVentaElectronica.rdlc";
                    break;
                case (Int16)TipoDocumentoComercial.Factura:
                    nombreRdlUsar = "SISAC.Util.Tool.ComprobanteElectronico.rptFacturaElectronica.rdlc";
                    break;
                case (Int16)TipoDocumentoComercial.NotaCredito:
                    nombreRdlUsar = "SISAC.Util.Tool.ComprobanteElectronico.rptNotaCreditoDebitoElectronico.rdlc";
                    break;
                case (Int16)TipoDocumentoComercial.NotaDebito:
                    nombreRdlUsar = "SISAC.Util.Tool.ComprobanteElectronico.rptNotaCreditoDebitoElectronico.rdlc";
                    break;
                default:
                    break;
            }

            return nombreRdlUsar;
        }



        public static string ComprobanteElectronico(clsComprobantePagoNGC _objcomprobante, Entidad.Atencion.clsEmpresa oEmpresa, string strdireccionimagenempresa, string strRutaCodBar, string rutaArchivo)
        {
            string nombreRdlUsar = ObtenerRutRDLCComprobantePago(_objcomprobante.IdTipoDocumento);
            string _importeTexto = clsConvert.ConvertirNumeroALetras(_objcomprobante.ImporteTotal, _objcomprobante.NombreMoneda, _objcomprobante.NombreMoneda);
            String strTieneConvenio = (_objcomprobante.ConvenioComprobantePago == null) ? "0" : "1";

            LocalReport reporteLocal = new LocalReport();
            clsListaComprobantePago oDatos = new clsListaComprobantePago();
            clsComprobantePago objComp = new clsComprobantePago();
            objComp = clsComprobantePagoNGC_to_clsListaComprobantePago(_objcomprobante);



            oDatos.Elementos.Add(objComp);

            reporteLocal.DataSources.Add(new ReportDataSource("DataSet1", oDatos.Elementos));
            reporteLocal.DataSources.Add(new ReportDataSource("DataSet3", objComp.DetalleComprobantePago));

            #region rellenando RDLC reporte PDF

            if (_objcomprobante.IdTipoDocumento == (Int16)TipoDocumentoComercial.BoletaVenta)
            {
                clsListaComprobantePagoConvenio oDatosConvenio = new clsListaComprobantePagoConvenio();
                clsComprobantePagoConvenio oParametroConvenio = _objcomprobante.ConvenioComprobantePago;
                if (oParametroConvenio == null)
                    oParametroConvenio = new clsComprobantePagoConvenio();
                oDatosConvenio.Elementos.Add(oParametroConvenio);
                //descomentar
                reporteLocal.DataSources.Add(new ReportDataSource("DataSet2", oDatosConvenio.Elementos));
                reporteLocal.ReportEmbeddedResource = nombreRdlUsar;
                reporteLocal.EnableExternalImages = true;
                //descomentar este metodo
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", _importeTexto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", _objcomprobante.ImporteTotal.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
            }
            else if (_objcomprobante.IdTipoDocumento == (Int16)TipoDocumentoComercial.Factura)
            {
                clsListaComprobantePagoConvenio oDatosConvenio = new clsListaComprobantePagoConvenio();

                clsComprobantePagoConvenio oParametroConvenio = _objcomprobante.ConvenioComprobantePago;

                if (oParametroConvenio == null)
                    oParametroConvenio = new clsComprobantePagoConvenio();

                oDatosConvenio.Elementos.Add(oParametroConvenio);

                reporteLocal.DataSources.Add(new ReportDataSource("DataSet2", oDatosConvenio.Elementos));

                /*
                 considerar ruta comprobante electronico
                 */
                reporteLocal.ReportEmbeddedResource = nombreRdlUsar;

                reporteLocal.EnableExternalImages = true;
                //descomentar
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", _importeTexto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("monto", _objcomprobante.ImporteTotal.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("igv", _objcomprobante.IGV.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", _objcomprobante.ImporteTotal.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
            }
            else if (_objcomprobante.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaCredito)
            {
                reporteLocal.ReportEmbeddedResource = nombreRdlUsar;

                String strTipoComprobante = _objcomprobante.EsEspecial == (Int16)1 ? "NOTA DE CRÉDITO ESPECIAL" : "NOTA CRÉDITO ELECTRÓNICO";
                //descomentar
                reporteLocal.EnableExternalImages = true;
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", _importeTexto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("monto", _objcomprobante.ImporteTotal.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("igv", _objcomprobante.IGV.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", _objcomprobante.ImporteTotal.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDocumento", strTipoComprobante) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
            }
            else if (_objcomprobante.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaDebito)
            {
                reporteLocal.ReportEmbeddedResource = nombreRdlUsar;

                String strTipoComprobante = _objcomprobante.EsEspecial == (Int16)1 ? "NOTA DE DÉBITO ESPECIAL" : "NOTA DÉBITO ELECTRÓNICO";
                //descomentar
                reporteLocal.EnableExternalImages = true;
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", _importeTexto) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("monto", _objcomprobante.ImporteTotal.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("igv", _objcomprobante.IGV.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", _objcomprobante.ImporteTotal.ToString("N2")) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDocumento", strTipoComprobante) });
                reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
            }

            #endregion


            #region Crear Archivo
            Microsoft.Reporting.WinForms.Warning[] warnings;

            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;
            String strFormato = "PDF";

            byte[] bytes = reporteLocal.Render(strFormato, null, out mimeType, out encoding, out filenameExtension, out streamIds, out warnings);

            string fileName = string.Concat(rutaArchivo, _objcomprobante.IdNroServicio, "_", _objcomprobante.NroDocumento.Trim(), ".pdf");

            if (!File.Exists(fileName))
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
            }

            FileInfo fi = new FileInfo(fileName);
            fileName = fi.Name;
            return fileName;

            #endregion
        }


        private static clsComprobantePago clsComprobantePagoNGC_to_clsListaComprobantePago(clsComprobantePagoNGC obj)
        {
            clsComprobantePago tmp = new clsComprobantePago();
            tmp.IdEmpresa = obj.IdEmpresa;
            tmp.DocumentoIdentidad = obj.DocumentoIdentidad;
            tmp.EsEspecial = obj.EsEspecial;
            tmp.CodigoBarra = obj.CodigoBarra;
            tmp.DireccionCentroServicio = obj.DireccionCentroServicio;
            tmp.DireccionEmpresa = obj.DireccionEmpresa;
            tmp.DireccionComplementariaEmpresa = obj.DireccionComplementariaEmpresa;
            tmp.NombrePuntoAtencion = obj.NombrePuntoAtencion;
            tmp.SimboloMoneda = obj.SimboloMoneda;
            tmp.NombreMoneda = obj.NombreMoneda;
            tmp.IdServicioPrincipal = obj.IdServicioPrincipal;
            tmp.IdNroServicio = obj.IdNroServicio;
            tmp.IdTipoDocumento = obj.IdTipoDocumento;
            tmp.NroDocumento = obj.NroDocumento;
            tmp.NombreTipoDocumento = obj.NombreTipoDocumento;
            tmp.NombreCliente = obj.NombreCliente;
            tmp.DireccionCliente = obj.DireccionCliente;
            tmp.IdTipoIdentidad = obj.IdTipoIdentidad;
            tmp.NroIdentidad = obj.NroIdentidad;
            tmp.FechaRegistro = obj.FechaRegistro;
            tmp.IdMoneda = obj.IdMoneda;
            tmp.IdUsuario = obj.IdUsuario;
            tmp.NombreUsuario = obj.NombreUsuario;
            tmp.IdCaja = obj.IdCaja;
            tmp.MensajeTotal = obj.MensajeTotal;
            tmp.ConvenioComprobantePago = obj.ConvenioComprobantePago;
            tmp.FechaVencimiento = obj.FechaVencimiento;
            tmp.SubTotal = obj.SubTotal;
            tmp.IGV = obj.IGV;
            tmp.ImporteTotal = obj.ImporteTotal;
            tmp.DireccionComplementariaCentroServicio = obj.DireccionComplementariaCentroServicio;
            tmp.MontoConPercepcion = obj.MontoConPercepcion;
            tmp.Percepcion = obj.Percepcion;
            tmp.OperacionesDescuentos = obj.OperacionesDescuentos;
            tmp.OperacionesInafectas = obj.OperacionesInafectas;
            tmp.OperacionesExoneradas = obj.OperacionesExoneradas;
            tmp.OperacionesGratuitas = obj.OperacionesGratuitas;
            tmp.OperacionesGravadas = obj.OperacionesGravadas;
            tmp.Observacion = obj.Observacion;
            tmp.MensajeRepresentacionElectronica = obj.MensajeRepresentacionElectronica;
            tmp.ImagenEmpresa = obj.ImagenEmpresa;
            tmp.SiglaMoneda = obj.SiglaMoneda;
            tmp.RutaImagenCodigoBarra = obj.RutaImagenCodigoBarra;
            tmp.RutaImagenEmpresa = obj.RutaImagenEmpresa;
            tmp.NombreEmpresaCorto = obj.NombreEmpresaCorto;
            tmp.RUCEmpresa = obj.RUCEmpresa;
            tmp.ImporteLetras = obj.ImporteLetras;
            tmp.TieneConvenio = obj.TieneConvenio;
            tmp.DetalleComprobantePagoImpresion = obj.DetalleComprobantePagoImpresion;
            tmp.DetalleComprobantePago = DetalleComprobantePago(obj.DetalleComprobantePago);


            return tmp;
        }

        private static List<clsComprobantePagoDetalle> DetalleComprobantePago(List<clsComprobantePagoDetalleNGC> lista)
        {
            List<clsComprobantePagoDetalle> lst = new List<clsComprobantePagoDetalle>();

            if (lista.Count == 0)
            {
                return lst;
            }

            foreach (clsComprobantePagoDetalleNGC item in lista)
            {
                clsComprobantePagoDetalle tmp = new clsComprobantePagoDetalle();
                tmp.IdEmpresa = item.IdEmpresa;
                tmp.IdTipoDocumento = item.IdTipoDocumento;
                tmp.NroDocumento = item.NroDocumento;
                tmp.EsVisible = item.EsVisible;
                tmp.ValorIGV = item.ValorIGV;
                tmp.IdConcepto = item.IdConcepto;
                tmp.EtiquetaConcepto = item.EtiquetaConcepto;
                tmp.AbreviaturaConcepto = item.AbreviaturaConcepto;
                tmp.Cantidad = item.Cantidad;
                tmp.Unidad = item.Unidad;
                tmp.Descripcion = item.Descripcion;
                tmp.PrecioUnitario = item.PrecioUnitario;
                tmp.Importe = item.Importe;
                tmp.IndicadorIGV = item.IndicadorIGV;
                tmp.ValorVenta = item.ValorVenta;
                lst.Add(tmp);
            }



            return lst;
        }







        #region generar XML PDF fluijo PAgo


        public static clsFacturacionResultadoXMLPDF ComprobanteGenerarPDF(clsFacturacionAdministrativaTransferirSAP oParametro, SISAC.Entidad.Atencion.clsEmpresa oEmpresa, clsComprobantePago oComprobante)
        {
            clsFacturacionResultadoXMLPDF oResultado = new clsFacturacionResultadoXMLPDF();
            try
            {
                #region Preparar Reporte
                if (oEmpresa != null && oComprobante != null)
                {
                    #region Preparar Imagen
                    String strdirecciontemp = clsArchivo.ObtenerCarpetaTemporal();
                    String strdireccionimagenempresa = String.Empty;

                    if (oComprobante.ImagenEmpresa != null)
                    {
                        String strdirecciontempie = strdirecciontemp + "ImagenEmpresa.jpg";
                        strdireccionimagenempresa = @"file://" + strdirecciontempie;
                        System.IO.File.WriteAllBytes(strdirecciontempie, oComprobante.ImagenEmpresa);
                    }
                    #endregion

                    #region Preparar parametros globales
                    String strMoneda = oComprobante.NombreMoneda;
                    Decimal decMonto = oComprobante.SubTotal;
                    Decimal decIGV = oComprobante.IGV;
                    Decimal decTotal = oComprobante.ImporteTotal;
                    String strTieneConvenio = (oComprobante.ConvenioComprobantePago == null) ? "0" : "1";
                    String strNombreArchivo = System.IO.Path.GetDirectoryName(oParametro.RutaArchivo) + "\\" +
                                            System.IO.Path.GetFileNameWithoutExtension(oParametro.RutaArchivo) + ".pdf";
                    String strFormato = "PDF";

                    String strImporteTexto = clsConvert.ConvertirNumeroALetras(decTotal, strMoneda, strMoneda);
                    #endregion

                    #region Datos para el Reporte
                    #region Cargar DATA
                    Int32 intTotalRegistros = ObtenerCantidadLineas(oComprobante.DetalleComprobantePago);



                    clsListaComprobantePago oDatos = new clsListaComprobantePago();
                    oDatos.Elementos.Add(oComprobante);

                    clsComprobantePagoDetalle oComprobanteDetalle = new clsComprobantePagoDetalle();

                    Int16 intCantidadTope = 0;

                    if (oComprobante.ConvenioComprobantePago == null)
                        intCantidadTope = 30;
                    else
                        intCantidadTope = 30 - 4;

                    if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.BoletaVenta)
                    {
                        if (intTotalRegistros < intCantidadTope) // Antes era 16, pero al agregar los datos de Afecto e inafecto cambio a 14
                        {
                            for (Int32 i = intTotalRegistros + 1; i <= intCantidadTope; i++)
                            {
                                oComprobante.DetalleComprobantePago.Add(oComprobanteDetalle);
                            }
                        }
                    }
                    else if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.Factura)
                    {
                        if (intTotalRegistros < intCantidadTope) // Antes era 20, pero al agregar los datos de Afecto e inafecto cambio a 18
                        {
                            for (Int32 i = intTotalRegistros + 1; i <= intCantidadTope; i++)
                            {
                                oComprobante.DetalleComprobantePago.Add(oComprobanteDetalle);
                            }
                        }
                    }
                    else if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaCredito ||
                            oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaDebito)
                    {
                        if (intTotalRegistros < intCantidadTope) // Antes era 20, pero al agregar los datos de Afecto e inafecto cambio a 18
                        {
                            for (Int32 i = intTotalRegistros + 1; i <= intCantidadTope; i++)
                            {
                                oComprobante.DetalleComprobantePago.Add(oComprobanteDetalle);
                            }
                        }
                    }

                    LocalReport reporteLocal = new LocalReport();
                    reporteLocal.DataSources.Add(new ReportDataSource("DataSet1", oDatos.Elementos));
                    reporteLocal.DataSources.Add(new ReportDataSource("DataSet3", oComprobante.DetalleComprobantePago));
                    String strRutaCodBar = GenerarCodigoBarraPDF47(oEmpresa.Id, oComprobante.CodigoBarra);

                    string nombreRdlUsar = clsUtilFinanciamiento.ObtenerRutRDLCComprobantePago(oParametro.IdTipoDocumento);

                    #endregion
                    #region Definir Reporte
                    if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.BoletaVenta)
                    {
                        clsListaComprobantePagoConvenio oDatosConvenio = new clsListaComprobantePagoConvenio();
                        clsComprobantePagoConvenio oParametroConvenio = oComprobante.ConvenioComprobantePago;
                        if (oParametroConvenio == null)
                            oParametroConvenio = new clsComprobantePagoConvenio();
                        oDatosConvenio.Elementos.Add(oParametroConvenio);
                        //descomentar
                        reporteLocal.DataSources.Add(new ReportDataSource("DataSet2", oDatosConvenio.Elementos));
                        reporteLocal.ReportEmbeddedResource = nombreRdlUsar;
                        reporteLocal.EnableExternalImages = true;
                        //descomentar este metodo
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", strImporteTexto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", decTotal.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
                    }
                    else if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.Factura)
                    {
                        clsListaComprobantePagoConvenio oDatosConvenio = new clsListaComprobantePagoConvenio();

                        clsComprobantePagoConvenio oParametroConvenio = oComprobante.ConvenioComprobantePago;

                        if (oParametroConvenio == null)
                            oParametroConvenio = new clsComprobantePagoConvenio();

                        oDatosConvenio.Elementos.Add(oParametroConvenio);

                        reporteLocal.DataSources.Add(new ReportDataSource("DataSet2", oDatosConvenio.Elementos));

                        /*
                         considerar ruta comprobante electronico
                         */
                        reporteLocal.ReportEmbeddedResource = nombreRdlUsar;

                        reporteLocal.EnableExternalImages = true;
                        //descomentar
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", strImporteTexto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("monto", decMonto.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("igv", decIGV.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", decTotal.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
                    }
                    else if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaCredito)
                    {
                        reporteLocal.ReportEmbeddedResource = nombreRdlUsar;

                        String strTipoComprobante = oComprobante.EsEspecial == (Int16)1 ? "NOTA DE CRÉDITO ESPECIAL" : "NOTA CRÉDITO ELECTRÓNICO";
                        //descomentar
                        reporteLocal.EnableExternalImages = true;
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", strImporteTexto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("monto", decMonto.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("igv", decIGV.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", decTotal.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDocumento", strTipoComprobante) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
                    }
                    else if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaDebito)
                    {
                        reporteLocal.ReportEmbeddedResource = nombreRdlUsar;

                        String strTipoComprobante = oComprobante.EsEspecial == (Int16)1 ? "NOTA DE DÉBITO ESPECIAL" : "NOTA DÉBITO ELECTRÓNICO";
                        //descomentar
                        reporteLocal.EnableExternalImages = true;
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaCorto", oEmpresa.Nombre) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamEmpresaLargo", oEmpresa.NombreCompleto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamRUC", oEmpresa.Ruc) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ParamDireccion", oEmpresa.Direccion) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("NroCaja", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("ImporteLetras", strImporteTexto) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("Original", "1") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("TieneConvenio", strTieneConvenio) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("monto", decMonto.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("igv", decIGV.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("total", decTotal.ToString("N2")) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDireccionENSA", " ") });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmImagenEmpresa", strdireccionimagenempresa) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDocumento", strTipoComprobante) });
                        reporteLocal.SetParameters(new ReportParameter[] { new ReportParameter("rpmDirCodigoBarra", strRutaCodBar) });
                    }
                    #endregion
                    #endregion

                    #region Crear Archivo
                    Warning[] warnings;

                    string[] streamIds;
                    string mimeType = string.Empty;
                    string encoding = string.Empty;
                    string filenameExtension = string.Empty;
                    string streamids = string.Empty;

                    byte[] bytes = reporteLocal.Render(strFormato, null, out mimeType, out encoding, out filenameExtension, out streamIds, out warnings);

                    using (FileStream fs = new FileStream(strNombreArchivo, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }
                    #endregion

                    #region Validar existe archivo
                    if (File.Exists(strNombreArchivo))
                    {
                        oResultado.GeneroXMLPDF = true;
                        oResultado.RutaXMLPDF = strNombreArchivo;
                    }
                    else
                    {
                        oResultado.GeneroXMLPDF = false;
                        oResultado.RutaXMLPDF = String.Empty;
                    }
                    #endregion
                }



                #endregion
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return oResultado;
        }

        private static Int32 ObtenerCantidadLineas(List<clsComprobantePagoDetalle> oListaDetalle)
        {
            Int32 intCantidadSaltos = 0;
            Int32 intCantidadLineasLongitudCadena = 0;

            Regex oExpresion = new Regex(@"\n");

            intCantidadSaltos = oListaDetalle.Count;

            for (Int32 i = 0; i < oListaDetalle.Count; i++)
            {
                MatchCollection oColeccion = oExpresion.Matches(oListaDetalle[i].AbreviaturaConcepto);
                intCantidadSaltos += oColeccion.Count;
            }

            for (Int32 i = 0; i < oListaDetalle.Count; i++)
            {
                if (oListaDetalle[i].AbreviaturaConcepto.Length > 74)
                {
                    intCantidadLineasLongitudCadena = oListaDetalle[i].AbreviaturaConcepto.Length / 74;

                    Decimal decTemporal = oListaDetalle[i].AbreviaturaConcepto.Length % 74;

                    if (decTemporal > 0)
                        intCantidadLineasLongitudCadena += 1;
                }
            }

            return intCantidadSaltos + Math.Abs(intCantidadSaltos - intCantidadLineasLongitudCadena);
        }

        public static String GenerarCodigoBarraPDF47(Int16 intIdEmpresa, String strParametro)
        {
            String strRuta = String.Empty;
            String strCertificadoBase64 = String.Empty;

            try
            {
                #region Definir Parametros de PDF417
                BarcodePDF417 oGeneraPDF417 = new BarcodePDF417();
                oGeneraPDF417.Options = BarcodePDF417.PDF417_FORCE_BINARY;
                oGeneraPDF417.ErrorLevel = 5;
                oGeneraPDF417.CodeRows = 5;
                oGeneraPDF417.CodeColumns = 18;
                oGeneraPDF417.LenCodewords = 999;
                #endregion

                #region Generar Codigo de Barra
                //String strSignature = CadenaSignatureValueCertificado(intIdEmpresa);

                String strParametroGeneral = strParametro + strCertificadoBase64 + "|";

                oGeneraPDF417.SetText(strParametroGeneral);

                System.Drawing.Image imgCodigoBarra = oGeneraPDF417.CreateDrawingImage(Color.Black, Color.White);

                strRuta = clsArchivo.ObtenerCarpetaTemporal() + @"CodigoBarra.bmp";

                imgCodigoBarra.Save(strRuta, ImageFormat.Bmp);

                strRuta = @"file://" + strRuta;
                #endregion
            }
            catch (Exception ex)
            {
                throw;
            }

            return strRuta;
        }
        #endregion generar XML PDF fluijo PAgo

    }
}