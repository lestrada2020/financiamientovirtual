﻿using Ionic.Zip;
using Microsoft.Win32;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Util.Tool
{
    #region clsUtil

    public sealed class clsUtil
    {
        #region Field

        private static String _MessageError = String.Empty;
        private static readonly Lazy<clsUtil> _Instancia = new Lazy<clsUtil>(() => new clsUtil(), LazyThreadSafetyMode.PublicationOnly);

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        private static readonly HttpClient _httpClient = new HttpClient();

        #endregion

        #endregion Field

        #region Property

        public static clsUtil Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsUtil()
        {
        }

        #endregion Constructor

        #region Public

        public T IsDBNullParse<T>(T valor)
        {
            try
            {
                if ((object)valor == DBNull.Value)
                    return (T)((object)null);
                else
                    return valor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InvokeAsync(Action<Object> methodo, Object parametro)
        {
            try
            {
                ParameterizedThreadStart parameterized = new ParameterizedThreadStart(methodo);
                Thread thread = new Thread(parameterized);
                thread.IsBackground = true;
                thread.Start(parametro);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String ErrorFormat(Exception exception)
        {
            try
            {
                if (exception == null) return "Exception is null...";

                StringBuilder result = new StringBuilder();

                if (exception is SqlException)
                {
                    #region SqlException

                    SqlException sqlException = (SqlException)exception;

                    if (sqlException.Errors == null
                        || sqlException.Errors.Count == 0)
                        return "sqlException.Errors is empty...";

                    foreach (SqlError error in sqlException.Errors)
                    {
                        result.AppendLine("Mensaje: " + error.Message);
                        result.AppendLine("Numero de Error: " + error.Number);
                        result.AppendLine("Linea de Error: " + error.LineNumber);
                        result.AppendLine("Procedimiento: " + error.Procedure);
                        result.AppendLine("Server: " + error.Server);
                        result.AppendLine("Origen: " + error.Source);
                        result.AppendLine("==============================");
                    }

                    #endregion SqlException
                }
                else
                {
                    #region Default

                    if (!String.IsNullOrEmpty(exception.Message))
                        result.AppendLine("Mensaje: " + exception.Message.Trim());

                    if (!String.IsNullOrEmpty(exception.StackTrace))
                        result.AppendLine("Llamada: " + exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        if (!String.IsNullOrEmpty(exception.InnerException.Message))
                            result.AppendLine("Detalle: " + exception.InnerException.Message.Trim());

                        if (!String.IsNullOrEmpty(exception.InnerException.StackTrace))
                            result.AppendLine("Llamada: " + exception.InnerException.StackTrace.Trim());
                    }

                    #endregion Default
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                return exception.Message;
            }
        }

        public String MessageError(Int32 idLog)
        {
            try
            {
                String result = String.Empty;

                if (String.IsNullOrEmpty(_MessageError))
                {
                    _MessageError = ConfigurationManager.AppSettings["MensajeError"] ?? String.Empty;

                    if (String.IsNullOrEmpty(_MessageError)) _MessageError = "Ha ocurrido un Error en el Proceso. Favor consulte con su Administrador Informático.";
                }

                if (idLog > 0) result = String.Format("{0}[IdLog: ({1})]", _MessageError, idLog);
                else result = String.Format("{0}", _MessageError);

                return result;
            }
            catch (Exception ex)
            {
                return "Ha ocurrido un Error en el Proceso.";
            }
        }

        public Boolean SaveEvent(String message)
        {
            try
            {
                System.Diagnostics.EventLog.WriteEntry("Servicio Tarea NGC", message, System.Diagnostics.EventLogEntryType.Error);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void GetTraceCall(ref String method, ref String service, ref String host, Boolean showError = true)
        {
            try
            {
                String trace = GetTrace();

                method = trace;
                service = "";
                host = Environment.MachineName;// System.Net.Dns.GetHostName();

                if (method.Contains("."))
                {
                    method = trace.Substring(trace.LastIndexOf(".") + 1);
                    service = trace.Substring(0, trace.LastIndexOf("."));
                }
            }
            catch (Exception ex)
            {
                if (showError) throw ex;
            }
        }

        public String GetTrace()
        {
            String traceFull = String.Empty;

            return GetTrace(out traceFull);
        }

        public String GetTrace(out String traceFull)
        {
            try
            {
                String result = String.Empty;
                Int32 skipFrames = 0;

                traceFull = GetTraceFull();
                String[] traceSplit = traceFull.Split(new String[] { "\r\n" }, StringSplitOptions.None);

                for (int i = 0; i < traceSplit.Length; i++)
                {
                    String trace = traceSplit[i].Trim();

                    if (!trace.Contains("."))
                    {
                        skipFrames = i >= 1 ? i - 1 : 0;
                        break;
                    }
                    else if (trace.Contains("System."))
                    {
                        skipFrames = i >= 2 ? i - 2 : 0;
                        break;
                    }
                }

                result = traceSplit[skipFrames].Trim();

                if (result.StartsWith("en ")
                    || result.StartsWith("at "))
                    result = result.Substring(3);

                return result;
            }
            catch (Exception ex)
            {
                traceFull = String.Empty;
                return String.Empty;
            }
        }

        public String GetTraceFull()
        {
            try
            {
                String result = String.Empty;

                StackTrace stackTrace = new StackTrace();//stackTrace.GetFrame(0).GetMethod().Name;
                result = stackTrace.ToString();

                return result;
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
        }

        public String GetPropertyName<T>(T item) where T : class
        {
            try
            {
                if (item == null) return String.Empty;

                return typeof(T).GetProperties()[0].Name;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetContentType(String fileName)
        {
            try
            {
                String result = "application/unknown";//MediaTypeNames.Application.Octet

                if (String.IsNullOrEmpty(fileName)) return result;

                String contentType = String.Empty;
                RegistryKey reg = Registry.ClassesRoot.OpenSubKey(Path.GetExtension(fileName).ToLower());

                if (reg != null) contentType = (String)reg.GetValue("Content Type");
                if (!String.IsNullOrEmpty(contentType)) result = contentType;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T SetError<T>(String message, Int32 idError = 1)
        {
            try
            {
                T result;

                Type type = typeof(T);
                MethodInfo methodInfo = type.GetMethod("SetError", new Type[] { typeof(Int32), typeof(String) });

                result = (T)Activator.CreateInstance(type);

                if (methodInfo != null) methodInfo.Invoke(result, new Object[] { idError, message });

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T SetError<T>(Exception exception, Int32 idError = 1)
        {
            try
            {
                String mensaje = ErrorFormat(exception);

                return SetError<T>(mensaje, idError);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String CompressFile(String url, String fileName)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(url, "");
                    zip.Save(fileName);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetValueOfConfig(String keyConfig)
        {
            try
            {
                if (!ConfigurationManager.AppSettings.AllKeys.Contains(keyConfig)) throw new Exception(String.Format("No existe el Key \"{0}\" en al archivo config.", keyConfig));

                var result = ConfigurationManager.AppSettings[keyConfig];

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T ValidateTokenInConfig<T>(String keyConfig, String valueCompare) where T : class
        {
            try
            {
                var sessionToken = GetValueOfConfig(keyConfig);
                var type = typeof(T);
                var result = (T)Activator.CreateInstance(type);

                if (String.IsNullOrWhiteSpace(valueCompare)) throw new Exception("Falta parametro de token de seguridad.");
                else if (valueCompare != sessionToken) throw new Exception("Parametro de token de seguridad no valido.");

                return result;
            }
            catch (Exception ex)
            {
                return SetError<T>(ex.Message);
            }
        }

        public static Int32 PeriodoADD(Int32 periodo, Int32 meses)
        {
            Int32 _intayo = Convert.ToInt32(periodo.ToString().Trim().Substring(0, 4));
            Int32 _intmes = Convert.ToInt32(periodo.ToString().Trim().Substring(4, 2));

            DateTime _datperiodo = new DateTime(_intayo, _intmes, 1).AddMonths(meses);

            _intayo = _datperiodo.Year;
            _intmes = _datperiodo.Month;

            Int32 periodoinicial = (_intayo * 100) + _intmes;

            return periodoinicial;
        }

        public static String ObtenerNombreMes(Int32 periodo)
        {
            Int16 numeromes = Convert.ToInt16(periodo.ToString().Trim().Substring(4, 2));
            String _strnombremes = "";

            switch (numeromes)
            {
                case 1:
                    _strnombremes = "Ene";
                    break;

                case 2:
                    _strnombremes = "Feb";
                    break;

                case 3:
                    _strnombremes = "Mar";
                    break;

                case 4:
                    _strnombremes = "Abr";
                    break;

                case 5:
                    _strnombremes = "May";
                    break;

                case 6:
                    _strnombremes = "Jun";
                    break;

                case 7:
                    _strnombremes = "Jul";
                    break;

                case 8:
                    _strnombremes = "Ago";
                    break;

                case 9:
                    _strnombremes = "Set";
                    break;

                case 10:
                    _strnombremes = "Oct";
                    break;

                case 11:
                    _strnombremes = "Nov";
                    break;

                case 12:
                    _strnombremes = "Dic";
                    break;

                default:
                    _strnombremes = "Numero mes: " + numeromes.ToString();
                    break;
            }

            return _strnombremes;
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public static async Task DownloadFileAsync(string downloadSourceUrl, string downloadDestinationPathAndFile)
        {
            string destinationPath = Path.GetDirectoryName(downloadDestinationPathAndFile);

            try
            {
                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }

                using (Stream streamToReadFrom = await _httpClient.GetStreamAsync(downloadSourceUrl))
                {
                    using (Stream streamToWriteTo = File.Open(downloadDestinationPathAndFile, FileMode.Create))
                    {
                        await streamToReadFrom.CopyToAsync(streamToWriteTo);
                    }
                }
            }
            catch (HttpRequestException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task CompressFileAsync(string sourceFile, bool deleteSourceFile)
        {
            string sourceDirectory = Path.GetDirectoryName(sourceFile);
            string sourceFileName = Path.GetFileNameWithoutExtension(sourceFile) + ".zip";
            string destinationFileName = Path.Combine(sourceDirectory, sourceFileName);

            await CompressFileAsync(sourceFile, destinationFileName, deleteSourceFile);
        }

        public static async Task CompressFileAsync(string sourceFile, string destinationFile, bool deleteSourceFile)
        {
            //Si ya existe el archivo en la pc cliente lo elimina
            if (File.Exists(destinationFile)) File.Delete(destinationFile);

            // Proceso de compresión de archivo.
            using (FileStream sourcefile = File.OpenRead(sourceFile))
            {
                using (FileStream destfile = File.Create(destinationFile))
                {
                    using (GZipStream compStream = new GZipStream(destfile, CompressionMode.Compress))
                    {
                        await WriteStreamToStreamAsync(sourcefile, compStream);
                    }
                }
            }

            // Eliminar archivo origen.
            if (deleteSourceFile)
                File.Delete(sourceFile);
        }

        public static async Task DecompressFileAsync(string sourceFile, string destinationFile, bool deleteSourceFile)
        {
            // Eliminando el archivo local (comprimido)
            if (File.Exists(destinationFile))
                File.Delete(destinationFile);

            // Descomprimir el archivo enviado
            using (FileStream sourcefile = File.OpenRead(sourceFile))
            {
                using (FileStream destfile = File.Create(destinationFile))
                {
                    using (GZipStream compStream = new GZipStream(sourcefile, CompressionMode.Decompress))
                    {
                        await WriteStreamToStreamAsync(compStream, destfile);
                    }
                }
            }

            // Eliminando el archivo comprimido copiado en el servidor
            if (deleteSourceFile)
                File.Delete(sourceFile);
        }

        public static async Task WriteStreamToStreamAsync(Stream input, Stream output)
        {
            byte[] bytes = new byte[4096];
            int i;
            while ((i = await input.ReadAsync(bytes, 0, bytes.Length)) != 0)
            {
                await output.WriteAsync(bytes, 0, i);
            }
        }

        public static void CreateEncryptedPdfFile(string sourceFile, string destinationFile, string userPassword,
            string ownerPassword, bool deleteSourceFile)
        {
            if (!Directory.Exists(Path.GetDirectoryName(destinationFile)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(destinationFile));
            }

            File.Copy(sourceFile, destinationFile, true);

            using (PdfSharp.Pdf.PdfDocument document = PdfSharp.Pdf.IO.PdfReader.Open(destinationFile))
            {
                PdfSharp.Pdf.Security.PdfSecuritySettings securitySettings = document.SecuritySettings;

                // Setting one of the passwords automatically sets the security level to 
                // PdfDocumentSecurityLevel.Encrypted128Bit.
                securitySettings.UserPassword = userPassword;
                securitySettings.OwnerPassword = ownerPassword;

                // Don't use 40 bit encryption unless needed for compatibility
                //securitySettings.DocumentSecurityLevel = PdfDocumentSecurityLevel.Encrypted40Bit;

                // Restrict some rights.
                securitySettings.PermitAccessibilityExtractContent = false;
                securitySettings.PermitAnnotations = false;
                securitySettings.PermitAssembleDocument = false;
                securitySettings.PermitExtractContent = false;
                securitySettings.PermitFormsFill = false;
                securitySettings.PermitModifyDocument = false;
                securitySettings.PermitFullQualityPrint = true;
                securitySettings.PermitPrint = true;

                // Save the document
                document.Save(destinationFile);
            }

            if (deleteSourceFile)
            {
                File.Delete(sourceFile);
            }
        }

        #endregion

        #endregion Public
    }
    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
    public static class UtilidadesNG
    {
        public static String ObtenerCarpetaTemporal()
        {
            return Path.GetTempPath();
        }

        public static String ConvertirNumeroALetras(Decimal value
                                          , String monedasingular
                                          , String monedaplural)
        {
            String cadena = "";
            cadena = ConvertirEnteroALetras(value);
            String _decimales = value.ToString("N2").Substring(value.ToString("N2").Length - 2, 2);
            _decimales = " Y " + _decimales + (value == 1 ? "/100 " + monedasingular.ToUpper() : "/100 " + monedaplural.ToUpper());
            cadena = cadena + _decimales;

            return cadena;
        }

        private static String ConvertirEnteroALetras(Decimal value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + ConvertirEnteroALetras(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + ConvertirEnteroALetras(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 10) * 10) + " Y " + ConvertirEnteroALetras(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + ConvertirEnteroALetras(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 100) * 100) + " " + ConvertirEnteroALetras(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + ConvertirEnteroALetras(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + ConvertirEnteroALetras(value % 1000);
            }
            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + ConvertirEnteroALetras(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + ConvertirEnteroALetras(value - Math.Truncate(value / 1000000) * 1000000);
            }
            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + ConvertirEnteroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            else
            {
                Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + ConvertirEnteroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }

            return Num2Text;
        }

        public static String ConvertirArrayBytesImagenJPG(Byte[] array)
        {
            //Creamos un array de bytes que contiene los bytes almacenados
            //en el campo Documento de la tabla
            byte[] bits = ((byte[])(array));
            //Vamos a guardar ese array de bytes como un fichero en el
            //disco duro, un fichero temporal que después se podrá descartar.
            //Para evitar problemas de concurrencia de usuarios,
            //generamos un nombre único para el mismo
            string sFile = "tmp" + GenerarNombreFichero() + ".jpg";
            //Creamos un nuevo FileStream, que esta vez servirá para
            //crear un fichero con el nombre especificado
            string sRuta = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + sFile;
            FileStream fs = new FileStream(sRuta, FileMode.Create);
            //Y escribimos en disco el array de bytes que conforman
            //el fichero JPG
            fs.Write(bits, 0, Convert.ToInt32(bits.Length));
            fs.Close();

            return sRuta;
        }
        public static String ConvertirArrayBytes_Fichero(Byte[] array)
        {
            //Creamos un array de bytes que contiene los bytes almacenados
            //en el campo Documento de la tabla
            byte[] bits = ((byte[])(array));
            //Vamos a guardar ese array de bytes como un fichero en el
            //disco duro, un fichero temporal que después se podrá descartar.
            //Para evitar problemas de concurrencia de usuarios,
            //generamos un nombre único para el mismo
            string sFile = "tmp" + GenerarNombreFichero() + ".rtf";
            //Creamos un nuevo FileStream, que esta vez servirá para
            //crear un fichero con el nombre especificado
            //string sRuta = Server.MapPath(".") + @"\" + sFile;
            //string sRuta = System.Environment.SpecialFolder.CurrentDirectory + @"\" + sFile;
            string sRuta = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + sFile;
            FileStream fs = new FileStream(sRuta, FileMode.Create);
            //Y escribimos en disco el array de bytes que conforman
            //el fichero Word
            fs.Write(bits, 0, Convert.ToInt32(bits.Length));
            fs.Close();

            return sRuta;
        }

        public static string GenerarNombreFichero()
        {
            int ultimoTick = 0;

            while (ultimoTick == Environment.TickCount)
            { System.Threading.Thread.Sleep(1); }

            ultimoTick = Environment.TickCount;

            return DateTime.Now.ToString("yyyyMMddhhmmss") + "." +
                ultimoTick.ToString();
        }

        public static String CopiarArchivo(String archivoOrigen, String nombreCopia)
        {
            try
            {
                String rutaOrigen = Path.GetDirectoryName(archivoOrigen);
                String rutaDestino = Path.Combine(rutaOrigen, nombreCopia);
                System.IO.File.Copy(archivoOrigen, rutaDestino, true);

                return rutaDestino;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void EliminarArchivo(String argArchivoEliminar)
        {
            try
            {
                if (File.Exists(argArchivoEliminar))
                    File.Delete(argArchivoEliminar);
            }
            catch (Exception ex)
            {
                StringBuilder mensajeError = new StringBuilder();

                mensajeError.AppendLine("El archivo ya existe y se ha intentado eliminar sin éxito.");
                mensajeError.AppendLine();
                mensajeError.AppendLine("Indicaciones:");
                mensajeError.AppendLine("1.Verifique que el archivo no sea de solo lectura.");
                mensajeError.AppendLine("2.Verifique que el archivo no esté siendo usado por otro proceso.");
                mensajeError.AppendLine();
                mensajeError.AppendLine("Detalle:");
                mensajeError.AppendLine(ex.Message);

                throw new Exception(mensajeError.ToString());
            }
        }

    }

    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
    #endregion clsUtil
}