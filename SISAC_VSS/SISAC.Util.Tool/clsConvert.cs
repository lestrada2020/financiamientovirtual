﻿using Microsoft.SqlServer.Types;
using Newtonsoft.Json;
using SpreadsheetGear;
using SpreadsheetGear.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace SISAC.Util.Tool
{
    public sealed class clsConvert
    {
        #region Field

        private static readonly Lazy<clsConvert> _Instancia = new Lazy<clsConvert>(() => new clsConvert(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsConvert Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsConvert()
        {
        }

        #endregion Constructor

        #region Public

        #region JsonSerialize

        public String ToJson(Object data)
        {
            try
            {
                var result = JsonConvert.SerializeObject(data);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String ToJson(Stream data)
        {
            try
            {
                var result = String.Empty;

                if (data == null) return result;

                using (var sr = new StreamReader(data, Encoding.UTF8))
                {
                    result = sr.ReadToEnd();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Byte[] ToJsonBytes(Object data)
        {
            try
            {
                Byte[] result = new Byte[0];

                if (data == null) return result;

                var serializer = new JsonSerializer();

                using (var ms = new MemoryStream())
                {
                    using (var sw = new StreamWriter(ms, Encoding.UTF8))
                    {
                        using (var writer = new JsonTextWriter(sw))
                        {
                            //writer.Formatting = Formatting.Indented;
                            serializer.Serialize(writer, data);
                            sw.Flush();
                            result = ms.ToArray();
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MemoryStream ToJsonStream(Object data)
        {
            try
            {
                if (data == null) return new MemoryStream();

                var bytes = ToJsonBytes(data);

                return new MemoryStream(bytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion JsonSerialize

        #region JsonDeserialize

        public T ToObject<T>(String data)
        {
            try
            {
                T result;

                if (String.IsNullOrEmpty(data)) return default(T);

                result = (T)JsonConvert.DeserializeObject(data, typeof(T));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T ToObject<T>(Byte[] data)
        {
            try
            {
                T result;

                if (data == null || data.Length == 0)
                    return default(T);

                var serializer = new JsonSerializer();

                using (var ms = new MemoryStream(data))
                {
                    result = ToObject<T>(ms);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T ToObject<T>(Stream data)
        {
            try
            {
                T result;

                if (data == null) return default(T);

                var serializer = new JsonSerializer();

                using (var sr = new StreamReader(data, Encoding.UTF8))
                {
                    using (var reader = new JsonTextReader(sr))
                    {
                        result = (T)serializer.Deserialize(reader, typeof(T));
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion JsonDeserialize

        #region String

        public String ToBase64(Byte[] bytes)
        {
            try
            {
                var result = Convert.ToBase64String(bytes);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String ToString2(Byte[] bytes)
        {
            try
            {
                var result = Encoding.UTF8.GetString(bytes);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String ToBase64(String text)
        {
            try
            {
                var bytes = ToBytes(text);

                return ToBase64(bytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String FromBase64String(String text)
        {
            try
            {
                var bytes = FromBase64(text);
                var result = ToString2(bytes);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String ToQueryString(Dictionary<String, String> param, String fields = "", String initial = "")
        {
            try
            {
                var result = initial;
                var arr = fields.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in arr)
                {
                    if (!param.ContainsKey(item)) throw new Exception($"Falta el parametro {item}");
                    if (result.Length > 0) result += "&";

                    result += $"{item}={param[item]}";
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion String

        #region Dictionary

        public Dictionary<String, String> ToDictionary(DataRow row)
        {
            return ToDictionary<String>(row);
        }

        public Dictionary<String, T> ToDictionary<T>(DataRow row)
        {
            try
            {
                if (row == null) return null;

                var result = new Dictionary<String, T>();

                foreach (DataColumn col in row.Table.Columns)
                    result.Add(col.ColumnName, (T)Convert.ChangeType(row[col.ColumnName], typeof(T)));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<String, String>> ToDictionary(DataTable table)
        {
            return ToDictionary<String>(table);
        }

        public List<Dictionary<String, T>> ToDictionary<T>(DataTable table)
        {
            try
            {
                var result = new List<Dictionary<String, T>>();

                if (table == null) return result;

                foreach (DataRow row in table.Rows)
                    result.Add(ToDictionary<T>(row));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dictionary<String, String> ToDictionary(Hashtable param)
        {
            return ToDictionary<String>(param);
        }

        public Dictionary<String, T> ToDictionary<T>(Hashtable param)
        {
            try
            {
                if (param == null) return new Dictionary<String, T>();
                var result = param.Cast<DictionaryEntry>().ToDictionary(d => d.Key.ToString(), d => (T)Convert.ChangeType(d.Value, typeof(T)));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Dictionary

        #region Hashtable

        public Hashtable ToHashtableDAO(Dictionary<String, String> param)
        {
            try
            {
                var preFix = "p_";
                var dateFormat = "";
                var fromBase64 = "";
                var hParam = new Hashtable();

                if (param == null) return hParam;
                if (param.ContainsKey("AddPreFix") && param["AddPreFix"] == "0") preFix = "";
                if (param.ContainsKey("DateFormat")) dateFormat = param["DateFormat"];
                if (param.ContainsKey("FromBase64")) fromBase64 = param["FromBase64"];

                foreach (var item in param)
                {
                    var key = $"@{preFix}{item.Key}";
                    var val = (Object)item.Value;

                    if (!String.IsNullOrWhiteSpace(dateFormat) && key.ToLower().Contains("fecha") && val != null) val = DateTime.ParseExact((String)val, dateFormat, CultureInfo.InvariantCulture);
                    if (!String.IsNullOrWhiteSpace(fromBase64) && val != null && !String.IsNullOrWhiteSpace((String)val) && fromBase64.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Count(s => s.ToString() == item.Key.ToString()) > 0) val = val.ToString().FromBase64();

                    hParam[key] = val;
                }

                return hParam;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Hashtable ToParamDAO<T>(T entidad) where T : class
        {
            try
            {
                var result = new Hashtable();
                var type = typeof(T);
                var typesDato = new List<String>() { "String", "Int16", "Int32", "Int64", "DateTime", "Byte", "Byte[]",
                           "Object" , "Decimal", "Double", "Single", "Boolean", "Char", "Nullable`1" };

                foreach (var item in type.GetProperties())
                {
                    var t = item.PropertyType;

                    if (typesDato.Exists(s => s == t.Name)) result[String.Format("@p_{0}", item.Name)] = item.GetValue(entidad, null);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Hashtable

        #region Encrypt

        public String ToMD5(String message)
        {
            try
            {
                var result = "";

                using (var md5 = MD5.Create())
                {
                    var bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(message));

                    result = String.Join("", bytes.Select(s => s.ToString("X2")).ToArray());
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Encrypt

        #region Excel

        public DataSet ToDataSet(Byte[] bytesExcel)
        {
            try
            {
                IWorkbookSet workbookSet = Factory.GetWorkbookSet();
                IWorkbook workbook = workbookSet.Workbooks.OpenFromMemory(bytesExcel);

                var dataSet = workbook.GetDataSet(GetDataFlags.None);

                return dataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ToDataTable(Byte[] bytesExcel, String sheetName)
        {
            try
            {
                var dataSet = ToDataSet(bytesExcel);
                var table = dataSet.Tables[sheetName];

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ToDataTable(Byte[] bytesExcel)
        {
            try
            {
                var dataSet = ToDataSet(bytesExcel);
                var table = dataSet.Tables[0];

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Excel

        #region Byte[]

        public Byte[] FromBase64(String text)
        {
            try
            {
                var result = Convert.FromBase64String(text);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Byte[] ToBytes(String text)
        {
            try
            {
                var result = Encoding.UTF8.GetBytes(text);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Byte[] ToThumbnail(Byte[] image, Int32 width, Int32 height)
        {
            try
            {
                var bytes = (Byte[])null;

                using (var ms = new MemoryStream())
                {
                    using (var thumbnail = Image.FromStream(new MemoryStream(image)).GetThumbnailImage(width, height, null, new IntPtr()))
                    {
                        thumbnail.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        bytes = ms.ToArray();
                    }
                }

                return bytes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Byte[] ToExcel(DataTable table)
        {
            try
            {
                var rowIni = 0;
                var columnIni = 0;
                var rowsTotal = table.Rows.Count;
                var columnsTotal = table.Columns.Count - 1;

                IWorkbook book = Factory.GetWorkbook();
                IWorksheet sheet = book.Worksheets["Sheet1"];
                IRange range = sheet.Cells[rowIni, columnIni, rowIni, columnsTotal];

                range.CopyFromDataTable(table, SpreadsheetGear.Data.SetDataFlags.None);
                range.Range.Font.Bold = true;
                range.HorizontalAlignment = HAlign.Center;
                range.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(184, 204, 228);

                sheet.Name = String.IsNullOrEmpty(table.TableName) ? "Data" : table.TableName;
                sheet.Cells[rowIni, columnIni, rowIni + rowsTotal, columnsTotal].Range.Borders.LineStyle = LineStyle.Continous;

                sheet.UsedRange.Columns.AutoFit();

                return book.SaveToMemory(FileFormat.OpenXMLWorkbook);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Byte[]

        #region String[][]

        public String[][] ToCoordinates(Object sqlGeography)
        {
            try
            {
                var polygon = (SqlGeography)sqlGeography;
                var numPoints = polygon.STNumPoints();
                var result = new String[(Int32)numPoints][];

                for (Int32 i = 0; i < numPoints; i++)
                {
                    var point = polygon.STPointN(i + 1);
                    var arr = new String[2] { point.Long.ToString(), point.Lat.ToString() };

                    result[i] = arr;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion String[][]

        #endregion Public

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRUAL
        public static String ConvertirNumeroALetras(Decimal value
                                                  , String monedasingular
                                                  , String monedaplural)
        {
            String cadena = "";
            cadena = ConvertirEnteroALetras(value);
            String _decimales = value.ToString("N2").Substring(value.ToString("N2").Length - 2, 2);
            _decimales = " Y " + _decimales + (value == 1 ? "/100 " + monedasingular.ToUpper() : "/100 " + monedaplural.ToUpper());
            cadena = cadena + _decimales;

            return cadena;
        }
        private static String ConvertirEnteroALetras(Decimal value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + ConvertirEnteroALetras(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + ConvertirEnteroALetras(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 10) * 10) + " Y " + ConvertirEnteroALetras(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + ConvertirEnteroALetras(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 100) * 100) + " " + ConvertirEnteroALetras(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + ConvertirEnteroALetras(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + ConvertirEnteroALetras(value % 1000);
            }
            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + ConvertirEnteroALetras(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + ConvertirEnteroALetras(value - Math.Truncate(value / 1000000) * 1000000);
            }
            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + ConvertirEnteroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            else
            {
                Num2Text = ConvertirEnteroALetras(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + ConvertirEnteroALetras(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }

            return Num2Text;
        }

        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRUAL

    }
}