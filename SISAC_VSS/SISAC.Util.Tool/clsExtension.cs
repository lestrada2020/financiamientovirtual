﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace SISAC.Util.Tool
{
    public static class clsExtension
    {
        #region Field

        private static readonly clsConvert _Convert = clsConvert.Instancia;

        #endregion Field

        #region Public

        #region DataTable

        public static Byte[] ToExcel(this DataTable table)
        {
            return _Convert.ToExcel(table);
        }

        public static String ToExcel(this DataTable table, String fileName)
        {
            try
            {
                var bytes = ToExcel(table);

                bytes.WriteDisk(fileName);

                return fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Byte[] ToJsonBytes(this DataTable table)
        {
            return _Convert.ToJsonBytes(table);
        }

        public static List<Dictionary<String, String>> ToDictionary(this DataTable table)
        {
            return _Convert.ToDictionary(table);
        }

        public static List<Dictionary<String, T>> ToDictionary<T>(this DataTable table)
        {
            return _Convert.ToDictionary<T>(table);
        }

        #endregion DataTable

        #region DataRow

        public static Dictionary<String, String> ToDictionary(this DataRow row)
        {
            return _Convert.ToDictionary(row);
        }

        public static Dictionary<String, T> ToDictionary<T>(this DataRow row)
        {
            return _Convert.ToDictionary<T>(row);
        }

        #endregion DataRow

        #region String

        public static String AddDateTime(this String fileName)
        {
            try
            {
                var ext = Path.GetExtension(fileName);
                var name = Path.GetFileNameWithoutExtension(fileName);

                if (String.IsNullOrEmpty(ext)) ext = ".xlsx";
                if (String.IsNullOrEmpty(name)) name = "File";

                return String.Format("{0} - {1:yyyy-MM-dd HH.mm.ss.fff}{2}", name, DateTime.Now, ext);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Byte[] ToBytes(this String text)
        {
            return _Convert.ToBytes(text);
        }

        public static String ToBase64(this String text)
        {
            return _Convert.ToBase64(text);
        }

        public static Byte[] FromBase64(this String text)
        {
            return _Convert.FromBase64(text);
        }

        public static String FromBase64String(this String text)
        {
            return _Convert.FromBase64String(text);
        }

        public static Byte[] OpenFile(this String text)
        {
            try
            {
                var result = File.ReadAllBytes(text);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String ToQueryString(this Dictionary<String, String> param, String fields = "", String initial = "")
        {
            return _Convert.ToQueryString(param, fields, initial);
        }

        #endregion String

        #region Byte[]

        public static String WriteDisk(this Byte[] bytes, String fileName)
        {
            try
            {
                var d = Path.GetDirectoryName(fileName);

                if (!Directory.Exists(d)) Directory.CreateDirectory(d);

                File.WriteAllBytes(fileName, bytes);

                return fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String ToBase64(this Byte[] bytes)
        {
            return _Convert.ToBase64(bytes);
        }

        public static String ToString2(this Byte[] bytes)
        {
            return _Convert.ToString2(bytes);
        }

        public static DataTable FromExcel(this Byte[] bytes)
        {
            return _Convert.ToDataTable(bytes);
        }

        public static DataTable FromExcel(this Byte[] bytes, String sheetName)
        {
            return _Convert.ToDataTable(bytes, sheetName);
        }

        public static Byte[] ToThumbnail(this Byte[] image, Int32 width, Int32 height)
        {
            return _Convert.ToThumbnail(image, width, height);
        }

        #endregion Byte[]

        #region Dictionary

        public static Hashtable ToHashtableDAO(this Dictionary<String, String> param)
        {
            return _Convert.ToHashtableDAO(param);
        }

        #endregion Dictionary

        #region Class

        public static Hashtable ToParamDAO<T>(this T entidad) where T : class
        {
            return _Convert.ToParamDAO<T>(entidad);
        }

        #endregion Class

        #region Hashtable

        public static Dictionary<String, String> ToDictionary(this Hashtable param)
        {
            return _Convert.ToDictionary(param);
        }

        public static Dictionary<String, T> ToDictionary<T>(this Hashtable param)
        {
            return _Convert.ToDictionary<T>(param);
        }

        #endregion Hashtable

        #region

        public static String[][] ToCoordinates(this Object sqlGeography)
        {
            return _Convert.ToCoordinates(sqlGeography);
        }

        #endregion Public

        #endregion Public
    }
}