﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Util.Tool
{
    #region clsService

    public sealed class clsService
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly Lazy<HttpClient> _HttpClient = new Lazy<HttpClient>(LazyHttpClient, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<clsService> _Instancia = new Lazy<clsService>(() => new clsService(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsService Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsService()
        {
        }

        #endregion Constructor

        #region Private

        private void CallService(String URL, Object data, String method)
        {
            try
            {
                using (Stream stream = ServiceRequest(URL, data, method)) { }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private T CallService<T>(String URL, Object data, String method)
        {
            try
            {
                T result;

                using (Stream stream = ServiceRequest(URL, data, method))
                {
                    result = _Convert.ToObject<T>(stream);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Stream ServiceRequest(String URL, Object data, String method)
        {
            try
            {
                Stream result = null;

                Byte[] bytes = _Convert.ToJsonBytes(data);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.ServicePoint.Expect100Continue = false;
                request.Expect = String.Empty;
                request.UseDefaultCredentials = true;
                request.PreAuthenticate = true;
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ImpersonationLevel = TokenImpersonationLevel.Delegation;
                request.ContentLength = bytes.Length;
                request.Method = method;

                if (!(data is Stream))
                {
                    request.ContentType = "application/json; charset=utf-8";
                    request.Expect = "application/json; charset=utf-8";
                }

                if (method.Equals("POST") && data != null)
                    request.GetRequestStream().Write(bytes, 0, bytes.Length);

                result = request.GetResponse().GetResponseStream();

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        #region Metodos asincronos

        private async Task CallServiceAsync(string url, object data, string method)
        {
            try
            {
                using (Stream stream = await ServiceRequestAsync(url, data, method)) { }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<T> CallServiceAsync<T>(string url, object data, string method)
        {
            try
            {
                T result;

                using (Stream stream = await ServiceRequestAsync(url, data, method))
                {
                    result = _Convert.ToObject<T>(stream);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<Stream> ServiceRequestAsync(string url, object data, string method)
        {
            try
            {
                Stream result = null;

                byte[] bytes = _Convert.ToJsonBytes(data);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ServicePoint.Expect100Continue = false;
                request.Expect = string.Empty;
                request.UseDefaultCredentials = true;
                request.PreAuthenticate = true;
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ImpersonationLevel = TokenImpersonationLevel.Delegation;
                request.ContentLength = bytes.Length;
                request.Method = method;

                if (!(data is Stream))
                {
                    request.ContentType = "application/json; charset=utf-8";
                    request.Expect = "application/json; charset=utf-8";
                }

                if (method.Equals("POST") && data != null)
                    (await request.GetRequestStreamAsync()).Write(bytes, 0, bytes.Length);

                result = (await request.GetResponseAsync()).GetResponseStream();

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<Stream> ServiceRequestReturnStreamAsync(string url, object data, string method, double timeoutInSeconds = 100)
        {
            //Solo se soportan los metodos GET y POST
            using (var requestMessage = new HttpRequestMessage(
                method.ToUpper() == "POST" ? HttpMethod.Post : HttpMethod.Get, url))
            {
                if (method.ToUpper() == "POST" && data != null)
                {
                    var dataInput = JsonConvert.SerializeObject(data);
                    requestMessage.Content = new StringContent(dataInput, Encoding.UTF8, "application/json");
                }

                using (var httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(timeoutInSeconds);
                    try
                    {
                        using (var responseMessage = await httpClient.SendAsync(requestMessage,
                            HttpCompletionOption.ResponseHeadersRead))
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                using (var stream = await responseMessage.Content.ReadAsStreamAsync())
                                {
                                    var memoryStream = new MemoryStream();
                                    await stream.CopyToAsync(memoryStream);
                                    memoryStream.Position = 0;

                                    return memoryStream;
                                }
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        #endregion Metodos asincronos

        #endregion Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        private static HttpClient LazyHttpClient()
        {
            var result = new HttpClient()
            {
                Timeout = TimeSpan.FromMinutes(30)
            };

            return result;
        }

        private HttpContent CreateHttpContent(Object data)
        {
            try
            {
                var json = "{}";

                if (data != null)
                {
                    if (data is String) json = data.ToString();
                    else json = _Convert.ToJson(data);
                }

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                return content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private HttpRequestMessage CreateHttpRequestMessage(String url, HttpContent content, String token, HttpMethod httpMethod)
        {
            try
            {
                var request = new HttpRequestMessage()
                {
                    Method = httpMethod,
                    RequestUri = new Uri(url),
                };

                if (httpMethod != HttpMethod.Get) request.Content = content;
                if (token != null) request.Headers.Add("Authorization", token);

                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                return request;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Private

        #region Public

        public void SendData(String URL, Object data, String method = "POST")
        {
            CallService(URL, data, method);
        }

        public T SendData<T>(String URL, Object data, String method = "POST")
        {
            try
            {
                T result = CallService<T>(URL, data, method);

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message, 1);
            }
        }

        public async Task<String> GetAsync(String url)
        {
            try
            {
                var result = "";

                using (var response = await _HttpClient.Value.GetAsync(new Uri(url)))
                {
                    response.EnsureSuccessStatusCode();
                    result = await response.Content.ReadAsStringAsync();
                }

                return result;
            }
            catch (Exception ex)
            {
                var error = url + " :: " + ex.Message;

                throw new Exception(error);
            }
        }

        public async Task<String> GetAsync(String keyConfig, String action, String queryString)
        {
            try
            {
                var site = _Util.GetValueOfConfig(keyConfig);
                var url = String.Format("{0}/{1}?{2}", site, action, queryString);

                var result = await GetAsync(url);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<T> GetAsync<T>(String url, Object data, String token = null, Boolean forcedDatos = false)
        {
            return await SendAsync<T>(url, data, HttpMethod.Get, token, forcedDatos).ConfigureAwait(false);
        }

        public async Task<T> PostAsync<T>(String url, Object data, String token = null, Boolean forcedDatos = false)
        {
            return await SendAsync<T>(url, data, HttpMethod.Post, token, forcedDatos).ConfigureAwait(false);
        }

        public async Task<T> SendAsync<T>(String url, Object data, HttpMethod httpMethod, String token = null, Boolean forcedDatos = false)
        {
            try
            {
                var result = default(T);

                using (var content = CreateHttpContent(data))
                {
                    using (var request = CreateHttpRequestMessage(url, content, token, httpMethod))
                    {
                        using (var response = await _HttpClient.Value.SendAsync(request).ConfigureAwait(false))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var read = await response.Content.ReadAsStringAsync();

                                if (typeof(T) == typeof(String)) result = (T)(Object)read;
                                else
                                {
                                    if (forcedDatos) read = _Convert.ToJson(new { Datos = read });

                                    result = _Convert.ToObject<T>(read);
                                }
                            }
                            else
                            {
                                result = _Util.SetError<T>("Favor de reintentar**", (Int32)response.StatusCode);
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        #region Metodos asincronos

        public async Task SendDataAsync(string url, object data, string method = "POST")
        {
            await CallServiceAsync(url, data, method);
        }

        public async Task<T> SendDataAsync<T>(string url, object data, string method = "POST")
        {
            try
            {
                T result = await CallServiceAsync<T>(url, data, method);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Stream> SendDataReturnStreamAsync(string url, object data, string method = "POST", double timeoutInSeconds = 100)
        {
            try
            {
                var result = await ServiceRequestReturnStreamAsync(url, data, method, timeoutInSeconds);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Metodos asincronos

        #endregion Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        #endregion Public
    }

    #endregion clsService
}