﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Threading;

namespace SISAC.Data.Maestro
{
    #region clsControlDAO

    public sealed class clsControlDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsControlDAO> _Instancia = new Lazy<clsControlDAO>(() => new clsControlDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsControlDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsControlDAO()
        {
        }

        #endregion Constructor

        #region Public

        public IEnumerable CacheList(clsParametro param)
        {
            try
            {
                var result = _DAO.ComandList(param.Parametros["Filter"]);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String CacheClear(clsParametro param)
        {
            try
            {
                var result = _DAO.ComandDelete(param.Parametros["Key"]);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AyudaLista(clsParametro entidad)
        {
            try
            {
                Int16 idSite = 0;
                String entity = "";
                String action = "AyudaLista";

                if (entidad.Parametros.ContainsKey("IdSite")) idSite = Convert.ToInt16(entidad.Parametros["IdSite"]);
                if (entidad.Parametros.ContainsKey("Entity")) entity = entidad.Parametros["Entity"];
                if (entidad.Parametros.ContainsKey("Action")) action = entidad.Parametros["Action"];

                var cnn = _DAO.GetConnectionString(idSite);
                var param = entidad.Parametros.ToHashtableDAO();
                var table = _DAO.EjecutarComandoEntidad(entity, action, param, cnn);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }

    #endregion clsControlDAO
}