﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;

namespace SISAC.Data.Maestro
{
    public sealed class clsExceptionDAO
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly clsExceptionDAO _Instancia = new clsExceptionDAO();

        #endregion Field

        #region Property

        public static clsExceptionDAO Instancia
        {
            get { return _Instancia; }
        }

        #endregion Property

        #region Constructors

        private clsExceptionDAO()
        {
        }

        #endregion Constructors

        #region Public

        public Int32 Registrar(clsException entidad)
        {
            try
            {
                Int32 result = 0;
                String cn = clsDataAccess.Instancia.GetConnectionString(entidad.IdAppBD);
                Hashtable param = entidad.ToParamDAO();

                _DAO.EjecutarComando("log.Exception", "Insertar", param, cn);

                result = Convert.ToInt32(param["@p_IdException"]);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }
}