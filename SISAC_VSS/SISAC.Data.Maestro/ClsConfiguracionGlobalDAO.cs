﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Data.Maestro
{
    public class ClsConfiguracionGlobalDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<ClsConfiguracionGlobalDAO> _Instancia = new Lazy<ClsConfiguracionGlobalDAO>(() => new ClsConfiguracionGlobalDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion

        #region Property

        public static ClsConfiguracionGlobalDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion

        #region Constructor

        private ClsConfiguracionGlobalDAO()
        {
        }

        #endregion Constructor

        #region Public

        public ClsVariableOptimus Obtenervariable(String nombre)
        {
            var param = new Hashtable
            {
                { "@nombre", nombre }
            };

            try
            {
                var dtResultado = _DAO.EjecutarComandoEntidad("ParametroConfiguracionGlobal", "ObtenerRegistro", param);

                if (dtResultado.Rows.Count != 1) return null;
                return new ClsVariableOptimus(dtResultado.Rows[0]);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
