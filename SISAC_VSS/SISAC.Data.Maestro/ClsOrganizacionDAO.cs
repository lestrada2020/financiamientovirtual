﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Data.Maestro
{
    public class ClsOrganizacionDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<ClsOrganizacionDAO> _Instancia = new Lazy<ClsOrganizacionDAO>(() => new ClsOrganizacionDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion

        #region Property

        public static ClsOrganizacionDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion

        #region Constructor

        private ClsOrganizacionDAO()
        {
        }

        #endregion Constructor

        #region Public

        public ClsEmpresa ObtenerRegistroEmpresa(short idEmpresa)
        {
            var param = new Hashtable
            {
                { "@idregistro", idEmpresa }
            };

            try
            {
                var dtResultado = _DAO.EjecutarComandoEntidad("Empresa", "ObtenerRegistro", param);

                if (dtResultado.Rows.Count != 1) return null;
                return new ClsEmpresa(dtResultado.Rows[0]);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Listas maestras para SIGOF

        public DataTable ListarUUNNxIdEmpresa(Int16 id_empresa)
        {
            try
            {
                var param = new Hashtable
                {
                    { "@p_idempresa", id_empresa }
                };

                var dtResultado = _DAO.EjecutarComandoEntidad("UnidadNegocio", "ObtenerxIdEmpresaMaestro", param);

                return dtResultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarCCSSxIdUUNN(Int16 id_uunn)
        {
            try
            {
                var param = new Hashtable
                {
                    { "@p_iduunn", id_uunn }
                };

                var dtResultado = _DAO.EjecutarComandoEntidad("CentroServicio", "ObtenerxIdUUNNMaestro", param);

                return dtResultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarSectoresxIdCCSS(Int16 id_centroservicio)
        {
            try
            {
                var param = new Hashtable
                {
                    { "@p_idcentroservicio", id_centroservicio }
                };

                var dtResultado = _DAO.EjecutarComandoEntidad("SectorServicio", "ObtenerxIdCentroServicioMaestro", param);

                return dtResultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarRutasxIdSector(Int16 id_sectorservicio)
        {
            try
            {
                var param = new Hashtable
                {
                    { "@p_idsector", id_sectorservicio }
                };

                var dtResultado = _DAO.EjecutarComandoEntidad("RutaLectura", "ObtenerxIdSectorMaestro", param);

                return dtResultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion
    }
}
