﻿using SISAC.Data.DAO;
using SISAC.Entidad.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace SISAC.Data.Atencion
{
    public class clsCobranzaEmpresaDAO

    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsCobranzaEmpresaDAO> _Instancia = new Lazy<clsCobranzaEmpresaDAO>(() => new clsCobranzaEmpresaDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsCobranzaEmpresaDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsCobranzaEmpresaDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataTable GrabarIntentoEmpresa(clsParametroBulkData entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var bulkdata = entidad.Data;
                var dataparametro = entidad.DataParametro;
                var result = _DAO.EjecutarComandoBulkData("ClienteEmpresa", "CobranzaGrabarIntento", param, bulkdata, dataparametro);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataRow PagarDeudaEmpresa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("ClienteEmpresa", "CobranzaVisaPagarDeuda", param);




                return result;
            }
            catch (Exception ex)
            {
                #region Error

                var transaccion = new Hashtable();
                transaccion["NroPedido"] = entidad.Parametros["NroPedido"];
                transaccion["IdCobranzaExterna"] = 0;
                transaccion["NroTransaccion"] = 0;
                transaccion["NroTarjeta"] = "";
                transaccion["MensajeExcepcion"] = ex.Message;

                transaccion = transaccion.ToDictionary().ToHashtableDAO();
                _DAO.EjecutarComandoRegistro("CobranzaVisa", "ActualizarIntentoPago", transaccion);

                #endregion Error

                throw ex;
            }
        }






        public DataTable ListarBancos(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Banco", "ListarActivo", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable ListarCuentas(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "ListarCuentasBanco", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable GrabarDepositoBancarioIntento(clsParametroBulkData entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var bulkdata = entidad.Data;
                var dataparametro = entidad.DataParametro;
                var result = _DAO.EjecutarComandoBulkData("ClienteEmpresa", "DepositoBancarioGrabarIntento", param, bulkdata, dataparametro);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable ActualizarDepositoBancarioEstado(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "DepositoBancarioActualizarEstado", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        #endregion Public



    }



}
