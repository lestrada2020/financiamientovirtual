﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Data.Atencion
{
    public sealed class clsOficinaVirtualDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly clsDataAccessAsync _DAA = clsDataAccessAsync.Instancia;
        private static readonly String _CnnI = _DAA.GetConnectionString("claveConexionDBI");
        private static readonly Lazy<clsOficinaVirtualDAO> _Instancia = new Lazy<clsOficinaVirtualDAO>(() => new clsOficinaVirtualDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsOficinaVirtualDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsOficinaVirtualDAO()
        {
        }

        #endregion Constructor

        #region Public

        public async Task<DataTable> ConsultaAtencionesSuministro(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("AtencionesConsulta", "ObtenerPorSuministro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> ConsultaAtencionNroAtencion(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("AtencionesConsulta", "ObtenerDetallePorAtencion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Int32> RegistrarContactanos(clsParametro entidad)
        {
            try
            {
                if (!entidad.Parametros.ContainsKey("IdNroServicio")) entidad.Parametros["IdNroServicio"] = "0";

                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("Contactanos", "Insertar", param);

                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Int32> RegistrarContactanosArchivo(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("ContactanosArchivo", "Insertar", param);

                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ListarInterrupciones(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("OficinaVirtual", "ObtenerInterrupcionesxSuministro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> ConfirmarEmail(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "ConfirmarEmail", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Int32> RegistrarReclamo(clsParametro entidad)
        {
            try
            {
                var result = 0;
                var param = entidad.Parametros.ToHashtableDAO();

                await _DAA.ExecuteNonQueryAsync("aic.ReclamoOV", "Insertar", param);

                result = Convert.ToInt32(param["@p_IdReclamoOV"]);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> InfoSuministro(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("CobranzaVisa", "InfoSuministro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> ObtenerObservacion(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("tco.ObservacionResultadoOperacion", "GetObservacion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> DesasociarSuministro(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "DesasociarSuministro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> CambiarPassword(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "CambiarPassword", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataSet> ListarByNroAtencion(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteDataSetAsync("OficinaVirtual", "ListarByNroAtencion", param, null, "Header", "Detail");

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Int32> RegistrarFotoLec(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("OficinaVirtual", "RegistrarFotoLec", param);

                return Convert.ToInt32(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> VisaListFailed(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("OrdenVisaNet", "ListFailed", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> VisaSendMailPayFailed(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OrdenVisaNet", "SendMailPayFailed", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Cliente Empresa

        public DataTable ValidaAccesoClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "ValidaCredenciales", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string RegistrarClienteEmpresaAdmin(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEscalar("ClienteEmpresa", "RegistrarClienteEmpresaAdmin", param);

                return Convert.ToString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable BuscarPorIdentidad(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "BuscarPorIdentidad", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "ListarClienteEmpresa", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarRoles(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "ListarRoles", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable TipoRelacion(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("TipoRelacion", "ObtenerLista", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string RegistrarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEscalar("ClienteEmpresa", "RegistrarClienteEmpresa", param);

                return Convert.ToString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string EditarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEscalar("ClienteEmpresa", "EditarClienteEmpresa", param);

                return Convert.ToString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string EliminarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEscalar("ClienteEmpresa", "EliminarClienteEmpresa", param);

                return Convert.ToString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarTarifaActiva(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Tarifa", "ListarActivo", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarSuministros(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "ListarSuministros", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarSuministrosDetallePago(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "ListarSuministros_DetallePago", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EliminarSuministroAsociado(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "EliminarSuministro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable BuscarSuministroAsociar(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "ValidarSuministroAsociar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable RegistrarSuministroAsociar(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ClienteEmpresa", "RegistrarSuministroAsociar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListarSuministrosCarga(clsParametroBulkData entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var bulkdata = entidad.Data;
                var dataparametro = entidad.DataParametro;
                var result = _DAO.EjecutarComandoBulkData("ClienteEmpresa", "ListarSuministrosUpload", param, bulkdata, dataparametro);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Cliente Empresa

        public async Task<DataRow> ObtenerFile(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("aic.Contactanos", "GetFile", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Beneficiario

        public async Task<DataRow> ObtenerBeneficiario(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "IdentificaBeneficiario", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> RegistrarBeneficiario(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "GrabaDatosBeneficiario", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Beneficiario

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<String> RegistrarUsuario(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("OficinaVirtual", "RegistrarUsuario", param);

                return Convert.ToString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<String> ActivarCuentaXsms(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("OficinaVirtual", "ActivarCuentaXsms", param);

                return Convert.ToString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<String> ActualizarCodigoSMS(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("OficinaVirtual", "ActualizarCodigoSMS", param);

                return Convert.ToString(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<DataTable> ObtenerConfiguracionGateWaySMS(int idEmpresa)
        {
            var htParametros = new Hashtable();
            htParametros["@p_idempresa"] = idEmpresa;

            var dtCfgGateWay = await _DAA.ExecuteTableAsync("MensajeSMS", "DevolverConfiguracionGateWaySMS", htParametros);

            return dtCfgGateWay;
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<Int16> ObtenerCanalSMS(Int32 idEmpresa)
        {
            var htParametros = new Hashtable();
            htParametros["@p_IdEmpresa"] = idEmpresa;

            short idCanal = Convert.ToInt16(await _DAA.ExecuteScalarAsync("GateWaySMS", "ObtenerIdCanalActivo", htParametros));

            return idCanal;
        }

        public async Task<DataRow> ObtenerDatosConfirmacion(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "ObtenerDatosConfirmacion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> SolicitarNewPassword(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "SolicitarNewPassword", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> DatosNroServicio(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "DatosNroServicio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ListarNroServicio(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("OficinaVirtual", "ListarNroServicio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> LecturaRegistrar(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtualLectura", "Insertar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> AyudaLista(clsParametro entidad)
        {
            try
            {
                var entity = "";
                var action = "AyudaLista";

                if (entidad.Parametros.ContainsKey("Entity")) entity = entidad.Parametros["Entity"];
                if (entidad.Parametros.ContainsKey("Action")) action = entidad.Parametros["Action"];

                var param = entidad.Parametros.ToHashtableDAO();
                var table = await _DAA.ExecuteTableAsync(entity, action, param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ListAppConfig(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var table = await _DAA.ExecuteTableAsync("SeguridadWeb", "ListAppConfig", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Cobranza

        public async Task<DataTable> ConsultarDeudaDetalle(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("CobranzaVisa", "ConsultarDeudaDetalle", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> GrabarIntentoDetalle(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("CobranzaVisa", "GrabarIntentoDetalle", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> ObtenerIntento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("CobranzaVisa", "ObtenerIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Cobranza

        #region BonoElectrico

        public async Task<DataRow> ObtenerBonoElectrico(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "ObtenerBonoElectrico", param, _CnnI);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> ObtenerBonoElectricoDNI(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("OficinaVirtual", "ObtenerBonoElectricoDNI", param, _CnnI);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion BonoElectrico

        #endregion Public
    }
}