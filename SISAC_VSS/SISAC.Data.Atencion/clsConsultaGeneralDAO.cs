﻿using SISAC.Data.DAO;
using SISAC.Entidad.Atencion;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;

namespace SISAC.Data.Atencion
{
    #region clsConsultaGeneralDAO

    public sealed class clsConsultaGeneralDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsConsultaGeneralDAO> _Instancia = new Lazy<clsConsultaGeneralDAO>(() => new clsConsultaGeneralDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsConsultaGeneralDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsConsultaGeneralDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataTable GetData(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                String action = "";

                #region TipoConsulta

                switch (entidad.TipoConsulta)
                {
                    case TypeConsultaGeneral.BuscarBySuministro:
                        action = "BusquedaPorNroServicioAsync";
                        break;

                    case TypeConsultaGeneral.BuscarByNroDocumento:
                        action = "BusquedaPorNroDocumentoAsync";
                        break;

                    case TypeConsultaGeneral.BuscarByNombre:
                        action = "BusquedaPorNombreAsync";
                        break;

                    case TypeConsultaGeneral.BuscarByRecibo:
                        action = "BusquedaPorReciboAsync";
                        break;

                    case TypeConsultaGeneral.DatosPago:
                        action = "ObtenerDatosUltimoPago";
                        break;

                    case TypeConsultaGeneral.DatosRecibo:
                        action = "ObtenerDatosUltimoReciboAsync";
                        break;

                    case TypeConsultaGeneral.DatosReclamo:
                        action = "ObtenerDatosUltimoReclamo";
                        break;

                    case TypeConsultaGeneral.DatosMedidor:
                        action = "ObtenerDatosMedidor";
                        break;

                    case TypeConsultaGeneral.DatosRefacfurado:
                        action = "ObtenerDatosUltimoRefacturado";
                        break;

                    case TypeConsultaGeneral.DatosFacturacion:
                        action = "ObtenerDatosFacturacion";
                        break;

                    case TypeConsultaGeneral.DatosTecnico:
                        action = "ObtenerDatosTecnicos";
                        break;

                    case TypeConsultaGeneral.DatosConsumo:
                        action = "ObtenerDatosConsumo";
                        break;

                    case TypeConsultaGeneral.DatosSuministro:
                        action = "ObtenerDatosSuministro";
                        break;

                    case TypeConsultaGeneral.DatosContrato:
                        action = "ObtenerDatosContrato";
                        break;

                    case TypeConsultaGeneral.DatosDeuda:
                        action = "ObtenerDatosDeudaAsync";
                        break;

                    case TypeConsultaGeneral.DatosComercial:
                        action = "ObtenerDatosComercialesAsync";
                        break;

                    default:
                        throw new Exception(String.Format("<<<Tipo de consulta no válido ({0})", entidad.TipoConsulta));
                }

                #endregion TipoConsulta

                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ConsultaGeneral", action, param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Resumen(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerDatosResumen", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Basico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerRegistroBasico", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Restriccion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("AlertaRestriccion", "ConsultarExistenciaRestricciones", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet CtaCte(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoConjuntoEntidad("Cobranza", "EstadoCtaCteCabeceraServicio", param, "A", "B");

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Facturacion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                param["@IdNroServicio"] = entidad.Parametros["IdNroServicio"];
                var result = _DAO.EjecutarComandoEntidad("IngresoProvision", "BuscarNroServicio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable FacturacionConcepto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("IngresoProvision", "MostrarDetalleFacturacion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable FacturacionDocumento(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("IngresoProvision", "MostrarDocumentosFacturacion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReciboBasico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Recibos", "ObtenerDatosBasicos", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReciboMedidor(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Recibos", "obtenerdatosmedidor", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReciboLectura(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Recibos", "ObtenerDatosLecturas", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReciboConcepto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Recibos", "ObtenerDatosConceptos", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Atencion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                entidad.Parametros["Cadena"] = $" ate.idestado not in (0, 2) and ate.fecharegistro between '{entidad.Parametros["FechaInicio"]}' and '{entidad.Parametros["FechaFin"]}' and ate.idnroservicio = {entidad.Parametros["IdNroServicio"]};;;;";

                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Atencion", "ObtenerListaGestion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable FotoLectura(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("OrdenTrabajoFoto", "ObtenerListaFotos", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Lectura(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerHistoricoLecturas", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AFavor(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                entidad.Parametros["DateFormat"] = "yyyyMMdd";

                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("DocumentosAFavor", "BuscarPorNroServicio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Pagos(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                entidad.Parametros["DateFormat"] = "yyyyMMdd";

                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Transaccion", "ObtenerInformacionPagoPorServicio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable TecnicoBasico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerDatosTecnicosCompletos", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable TecnicoMagnitud(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerMagnitudes", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable TecnicoPrecinto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerPrecintosMedidor", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable TecnicoTransformador(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerTransformadorMedidor", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable OrdenTrabajo(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                entidad.Parametros["DateFormat"] = "yyyyMMdd";

                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("OrdenTrabajo", "ConsultarOTPorSuministro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable FotoActividad(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["DateFormat"] = "yyyyMMdd";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("OrdenTrabajoFoto", "ObtenerLista", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Foto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("OrdenTrabajoFoto", "ObtenerRegistroxId", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet MedidorBasico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoConjuntoEntidad("NroServicioMedidor", "ObtenerInformacion", param, "A", "B");

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable MedidorLista(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicioMedidor", "ObtenerListaPorNroServicio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable MedidorLecturas(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("HistoricoLecturaEnergia", "ObtenerListaPorMedidor", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet Cambios(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var tables = new List<String>();
                for (int i = 1; i <= 15; i++) tables.Add($"Table{i}");

                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoConjuntoEntidad("HistoricoCambioNroServicio", "Listar", param, tables.ToArray());

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvenioLista(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("NroServicio", "ObtenerConvenios", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvenioRegistro(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Convenio", "ObtenerRegistro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvenioCreacion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Convenio", "ObtenerDatosCreacion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvenioDocumentos(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Convenio", "ObtenerDocumentos", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvenioGarantias(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ConvenioGarantia", "ObtenerGarantias", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvenioAmortizaciones(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("ConvenioAmortizaciones", "CalcularObtener", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConvenioLetra(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Convenio", "LetraCambio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }

    #endregion clsConsultaGeneralDAO
}