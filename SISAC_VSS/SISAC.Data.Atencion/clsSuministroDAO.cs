﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Data.Atencion
{
    public sealed class clsSuministroDAO
    {
        #region Field

        private static readonly clsDataAccessAsync _DAA = clsDataAccessAsync.Instancia;
        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;//Distriluz-036
        private static readonly Lazy<clsSuministroDAO> _Instancia = new Lazy<clsSuministroDAO>(() => new clsSuministroDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsSuministroDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsSuministroDAO()
        {
        }

        #endregion Constructor

        #region Public
        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        public DataTable ListarSuministroAsociado(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("DistriluzMobil", "ObtenerListaInformacionAsociado", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        public async Task<DataRow> ConsultarDatos(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("NroServicio", "ObtenerRegistroBasico", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> RegistrarInterrupcion(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("Atencion", "RegistrarInterrupcion", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ListarParaReporteCCSS(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("CentroServicio", "ListarParaReporte", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ConsultarRHD(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("Atencion", $"RHD{param["@p_IdReporte"]}", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> UltimaLectura(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("Atencion", "UltimaLectura", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }
}