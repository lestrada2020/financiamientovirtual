﻿using SISAC.Data.DAO;
using SISAC.Entidad.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SISAC.Entidad.Atencion.sunat;
using SISAC.Entidad.Atencion.sunat.Model;
using SISAC.Entidad.Atencion.sunat.Estructuras;

namespace SISAC.Data.Atencion
{
    public class clsFinanciamientoDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly clsDataAccessAsync _DAA = clsDataAccessAsync.Instancia;
        private static readonly Lazy<clsFinanciamientoDAO> _Instancia = new Lazy<clsFinanciamientoDAO>(() => new clsFinanciamientoDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsFinanciamientoDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsFinanciamientoDAO()
        {
        }

        #endregion Constructor

        #region Public

        public clsFinanciamientoOrdenVisaNet ObtenerOrdenVisaNetPorId(clsParametro entidad)
        {
            clsParametro parametro = new clsParametro();
            parametro.Parametros = new Dictionary<string, string>();
            parametro.Parametros.Add("IdNroOrden", entidad.Parametros["IdNroOrden"].ToString());
            var param = parametro.Parametros.ToHashtableDAO();
            DataRow row = _DAO.EjecutarComandoRegistro("FinanciamientoOrdenVisaNet", "Obtener", param);
            clsFinanciamientoOrdenVisaNet ordenVisaNet = new clsFinanciamientoOrdenVisaNet(row);
            return ordenVisaNet;
        }

        public DataTable ListarSuministroAsociado(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Financiamiento", "ObtenerListaSuministroAsociado", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "CMASSA"
        public DataTable ObtenerDetalleFinanciamiento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Financiamiento", "ObtenerDetalleFinanciamiento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Traza Financiamiento AL PAGAR CON VISA EN NIUBIZ
        //  ObtenerIntento hecho por Mario
        // ValidarToken
        public DataRow ValidarToken(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Financiamiento", "ValidarToken", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // ActualizarIntento_pa
        public DataRow ActualizarIntento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Financiamiento", "ActualizarIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataRow EnviarEmail(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Financiamiento", "EnviarEmail", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow FinanciamientoEnviarEmailError(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Financiamiento", "SendMailPayFailed", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConsultarDeudaDetalle(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Financiamiento", "ConsultarDeudaDetalle", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region comprobante de pago
        public DataTable ObtenerRegistroParametroConfiguracionGlobal(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.ParametroConfiguracionGlobal", "ObtenerRegistro_2", param);
                return result;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataSet ObtenerInformacionComprobantePago(clsParametro entidad)
        {
            var param = entidad.Parametros.ToHashtableDAO();
            string[] tables = { "Cabecera", "Detalle", "Convenio" };
            var result = _DAO.EjecutarComandoConjuntoEntidad("dbo.IngresoProvision", "ObtenerPorNroDocumento", param, tables);
            return result;
        }

        public DataTable ObtenerSuminitroObtenerDatosOrganizacion(clsParametro entidad)
        {
            var param = entidad.Parametros.ToHashtableDAO();
            var result = _DAO.EjecutarComandoEntidad("dbo.Suminitro", "ObtenerDatosOrganizacion", param);
            return result;
        }

        public DataTable ObtenerPagoFinanciamientoCabecera(clsParametro entidad)
        {
            var param = entidad.Parametros.ToHashtableDAO();
            var result = _DAO.EjecutarComandoEntidad("dbo.financiamiento", "datosPagoCabecera", param);
            return result;
        }

        public DataTable ObtenerPagoFinanciamientoDetalle(clsParametro entidad)
        {
            var param = entidad.Parametros.ToHashtableDAO();
            var result = _DAO.EjecutarComandoEntidad("dbo.financiamiento", "datosPagoDetalle", param);
            return result;
        }

        #endregion


        #region LESTRADA

        public DataTable ObtenerParametrosConvenio(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerParametroConvenio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet ObtenerConvenioParametros(clsParametro entidad)
        {
            try
            {
                clsParametro parametro = new clsParametro();
                parametro.Parametros = new Dictionary<string, string>();
                parametro.Parametros.Add("idconveniotipo", entidad.Parametros["IdConvenioTipo"].ToString());
                parametro.Parametros.Add("idusuario", entidad.Parametros["IdUsuario"].ToString());
                parametro.Parametros.Add("iduunn", entidad.Parametros["IdUUNN"].ToString());
                parametro.Parametros.Add("idcentroservicio", entidad.Parametros["IdCentroServicio"].ToString());
                parametro.Parametros.Add("puntoatencion", null);
                var param = parametro.Parametros.ToHashtableDAO();



                var resultPrueba = _DAO.EjecutarComandoConjuntoEntidad("ConvenioParametro",
                "Obtener",
                param,
                "Condicion",
                "CondicionGeneral",
                "Autorizaciones",
                "AreasAutorizacion",
                "AreaUsuario"
                );
                var result = _DAO.EjecutarComandoEntidad("ConvenioParametro", "Obtener", param);



                return resultPrueba;
            }
            catch (Exception ex)
            {
                throw ex;
            }




        }

        public Object ObtenerInteresFechaActual(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEscalar("dbo.Financiamiento", "ObtenerInteresFechaActual", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerRutaVideoFinanciamiento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerRutaVideoTutorial", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerPorcentajesConvenio(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerPorcentajesConvenio", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerCentroServicios(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerCentroServicios", param);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerConveniosPorEmpresa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerConveniosPorEmpresa", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerConvenioPorId(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerConvenioFinanciamiento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDetalleDeuda(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerDetalleDeuda", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerConfiguracionGlobal(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerConfiguracionGlobal", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDetalleOrdenCobro(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ObtenerDetalleOrdenCobro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow ObtenerCabeceraOrdenCobro(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("dbo.Financiamiento", "ObtenerCabeceraOrdenCobro", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow GrabarIntento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Financiamiento", "GrabarIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> GenerarNroPedido(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("Financiamiento", "GenerarNroPedido", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region OBTNER COMPROBANTE DE PAGO

        public DataRow ObtenerParametrosComprobantePago(int nroServicio, int nroOrden)
        {
            Hashtable htparam = new Hashtable();
            try
            {
                htparam["@p_idnroservicio"] = nroServicio;
                htparam["@p_idnumeroorden"] = nroOrden;

                var result = _DAO.EjecutarComandoRegistro("FinanciamientoParametros", "GestorDocumentos", htparam);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDocumentoComprobantePago(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("notacreditodebitocontrol", "obtenergestor", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SISAC.Entidad.Atencion.clsEmpresa ObtenerRegistroEmpresa(Int16 idempresa)
        {
            //clsGestionDato _objdato = clsGestionDato.ObtenerInstancia();
            DataRow _drow;
            SISAC.Entidad.Atencion.clsEmpresa _objempresa = null;
            Hashtable _htparametros = new Hashtable();
            _htparametros.Add("@idregistro", idempresa);

            try
            {
                _drow = _DAO.EjecutarComandoRegistro("empresa", "obtenerregistro", _htparametros);

                _objempresa = new SISAC.Entidad.Atencion.clsEmpresa(_drow);

                if (_objempresa.DireccionCodificada != null)
                    _objempresa.ODireccionCodificada = DireccionCodificadaObtenerId(_objempresa.DireccionCodificada);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _objempresa;
        }

        public static clsDireccionCodificada DireccionCodificadaObtenerId(Int32 iddireccioncodificada)
        {
            //clsGestionDato objGestionDato = clsGestionDato.ObtenerInstancia();
            Hashtable _htparametro = new Hashtable();

            DataRow _drwdireccioncodificada;

            _htparametro.Add("@id", iddireccioncodificada);

            _drwdireccioncodificada = _DAO.EjecutarComandoRegistro("direccioncodificada"
                                                                           , "buscarid"
                                                                           , _htparametro);
            clsDireccionCodificada _objdireccioncodificada = new clsDireccionCodificada(_drwdireccioncodificada);

            return _objdireccioncodificada;
        }

        public Object ObtenerDatosComplemento(Int16 intIdEmpresa, Int16 intIdComplemento)
        {
            try
            {
                Object result = null;

                Hashtable param = new Hashtable();
                param["@p_idempresa"] = intIdEmpresa;
                param["@p_idcomplemento"] = intIdComplemento;

                result = _DAO.EjecutarComandoEscalar("empresacomplemento", "obtenervalor", param);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public clsComprobantePagoNGC ObtenerInformacionDocumentoIngresoProvisionNGC
                                            (
                                                    Int16 intidempresa
                                                , Int16 intidtipodocumento
                                                , String strnrodocumento
                                            )
        {
            clsComprobantePagoNGC _objcomprobantepago = new clsComprobantePagoNGC();
            //clsGestionDato _objgestiondatos = clsGestionDato.ObtenerInstancia();
            DataSet _dscomprobantepago;

            Hashtable _htparametros = new Hashtable();
            _htparametros.Add("@p_idempresa", intidempresa);
            _htparametros.Add("@p_idtipodocumento", intidtipodocumento);
            _htparametros.Add("@p_nrodocumento", strnrodocumento);

            _dscomprobantepago = _DAO.EjecutarComandoConjuntoEntidad("ingresoprovision", "obtenerpornrodocumento", _htparametros, "cabecera", "detalle", "convenio");

            if (_dscomprobantepago == null) { return null; }

            if (_dscomprobantepago.Tables["cabecera"].Rows.Count == 0)
            {
                throw new Exception("<<<No se encontró registro para Ingreso Provision.>>>");
            }

            if (_dscomprobantepago.Tables["convenio"].Rows.Count <= 0)
                _objcomprobantepago = new clsComprobantePagoNGC(_dscomprobantepago.Tables["cabecera"].Rows[0], _dscomprobantepago.Tables["detalle"], (DataRow)null);
            else
                _objcomprobantepago = new clsComprobantePagoNGC(_dscomprobantepago.Tables["cabecera"].Rows[0], _dscomprobantepago.Tables["detalle"], _dscomprobantepago.Tables["convenio"].Rows[0]);

            return _objcomprobantepago;
        }

        #endregion OBTENER COMPROBANTE DE PAGO

        #region OBTENER TRANSACCION EXTRAJUDICIAL
        public DataRow ObtenerParametrosTransaccionExtrajudicial(clsParametro entidad)
        {
            var param = entidad.Parametros.ToHashtableDAO();

            try
            {
                var result = _DAO.EjecutarComandoRegistro("FinanciamientoParametros", "TransaccionExtrajudicial", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public clsListaCampoDato ObtenerDatosPlantillaTransaccionExtrajudicial(Int16 idempresa, Int16 iduunn, String nroconvenio)
        {
            //clsGestionDato _objgestionadatos = clsGestionDato.ObtenerInstancia();
            Hashtable _htparametros = new Hashtable();
            clsListaCampoDato _objdatos;
            DataRow _drdatos;

            _htparametros.Add("@idempresa", idempresa);
            _htparametros.Add("@idunegocio", iduunn);
            _htparametros.Add("@nroconvenio", nroconvenio);

            _drdatos = _DAO.EjecutarComandoRegistro("Convenio", "TransaccionExtrajudicial", _htparametros);

            _objdatos = new clsListaCampoDato(_drdatos);

            return _objdatos;
        }

        public Byte[] FirmaDocumentoObtener(Int32 IdFirmaDocumento)
        {
            byte[] firma;
            //clsGestionDato _objgestor = clsGestionDato.ObtenerInstancia();
            Hashtable _htparam = new Hashtable();
            DataRow _dr;

            _htparam.Add("@id", IdFirmaDocumento);

            _dr = _DAO.EjecutarComandoRegistro("FirmaDocumento", "ObtenerRegistro", _htparam);

            firma = (Byte[])(_dr["firma"]); ;

            return firma;
        }

        #region Plantilla
        public Byte[] PlantillaObtenerDocumento2(Int16 IdTipoDocumentoComercial, Int16 IdServicio, Int16 IdFlujo)
        {
            //clsGestionDato gestorDato = clsGestionDato.ObtenerInstancia();
            Hashtable _htparam = new Hashtable();
            DataRow dr;
            Byte[] doc = null;

            _htparam.Add("@p_idtipodocumentocomercial", IdTipoDocumentoComercial);
            _htparam.Add("@p_idservicio", IdServicio);
            _htparam.Add("@p_idflujo", IdFlujo);

            try
            {
                dr = _DAO.EjecutarComandoRegistro("plantilla", "ObtenerDocumentoServicioFlujo", _htparam);

                if (dr == null)
                { throw new Exception("<<<No existe una plantilla asociada a este tipo de documento>>>"); }
                else
                {
                    doc = (Byte[])dr["plantilla"];
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            return doc;
        }
        #endregion Plantilla

        #endregion OBTENER TRANSACCION EXTRAJUDICIAL

        #region PAGO NIUBIZ

        public DataRow PagarDeuda(clsParametro entidad)
        {
            try
            {
                var idNumOrden = Convert.ToInt32(entidad.Parametros["NroPedido"]);
                var idOrdenCobro = Convert.ToInt32(entidad.Parametros["IdOrdenCobro"]);
                var metodoPago = Convert.ToInt32(entidad.Parametros["MetodoPago"]);
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Financiamiento", "PagarDeuda", param);

                var transaccion = clsFinanciamientoNGCDAO.Instancia.RegistrarTransaccionPagoVisa(idNumOrden, idOrdenCobro, metodoPago);

                transaccion = transaccion.ToDictionary().ToHashtableDAO();
                _DAO.EjecutarComandoRegistro("Financiamiento", "ActualizarIntentoPago", transaccion);

                return result;
            }
            catch (Exception ex)
            {
                #region Error

                var transaccion = new Hashtable();
                transaccion["NroPedido"] = entidad.Parametros["NroPedido"];
                transaccion["IdCobranzaExterna"] = 0;
                transaccion["NroTransaccion"] = 0;
                transaccion["NroTarjeta"] = "";
                transaccion["MensajeExcepcion"] = ex.Message;

                transaccion = transaccion.ToDictionary().ToHashtableDAO();
                _DAO.EjecutarComandoRegistro("Financiamiento", "ActualizarIntentoPago", transaccion);

                #endregion Error

                throw ex;
            }
        }

        public DataRow AnularOrdenCobroPendiente(clsParametro parametro)
        {
            try
            {
                var param = parametro.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Financiamiento", "AnularOrdenCobroPendiente", param);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Hashtable CobranzaOrdenVisaNET(clsParametro entidad, clsTransaccion transaccion)
        {
            Hashtable oResultado = new Hashtable();
            Hashtable oParametro = new Hashtable();
            clsOrdenVisaNet oVisaNETCabecera = new clsOrdenVisaNet();
            clsListaOrdenVisaNetDetalle oVisaNETDetalle = new clsListaOrdenVisaNetDetalle();

            clsUsuarioPuntoAtencion oUsuarioPuntoAtencion = new clsUsuarioPuntoAtencion();
            clsMediosPagoExterno oMediosPagoExterno = new clsMediosPagoExterno();
            clsCajaEstadisticaDiarioMoneda oCajaMoneda = new clsCajaEstadisticaDiarioMoneda();
            clsTransaccion oTransaccion = new clsTransaccion();

            List<clsDocumentosDescargoPropio> oDocumentosPropios = new List<clsDocumentosDescargoPropio>(); ;
            List<clsDocumentosDescargoBase> oDocumentosBase = new List<clsDocumentosDescargoBase>(); ;

            DateTime today = DateTime.Now;

            using (SqlConnection oConexion = _DAO.GetSqlConnection())
            {
                oConexion.Open();

                #region Obtener Datos de VISA NET

                var idNumOrden = Convert.ToInt32(entidad.Parametros["NroPedido"]);

                oParametro["@p_idnroorden"] = idNumOrden;
                DataRow oFilaCabecera = _DAO.EjecutarComandoRegistro("OrdenVisaNet", "ObtenerPorConciliar", oParametro, oConexion);
                oVisaNETCabecera = new clsOrdenVisaNet(oFilaCabecera);

                oParametro["@p_idnroorden"] = idNumOrden;
                DataTable tTablaDetalle = _DAO.EjecutarComandoEntidad("OrdenVisaNetDetalle", "Obtener", oParametro, oConexion);
                oVisaNETDetalle = new clsListaOrdenVisaNetDetalle(tTablaDetalle);

                #endregion

                #region Clases Generales

                #region PuntoAtencion

                oUsuarioPuntoAtencion.IdEmpresa = oVisaNETCabecera.IdEmpresa;
                oUsuarioPuntoAtencion.IdUnidadNegocio = oVisaNETCabecera.IdUUNN;
                oUsuarioPuntoAtencion.IdCentroServicio = Convert.ToInt16(oVisaNETCabecera.IdCentroServicio);
                oUsuarioPuntoAtencion.IdTipoAtencion = oVisaNETCabecera.IdTipoAtencion;
                oUsuarioPuntoAtencion.IdPuntoAtencion = Convert.ToInt16(oVisaNETCabecera.IdPuntoAtencion);
                oUsuarioPuntoAtencion.IdUsuario = oVisaNETCabecera.IdUsuario;

                #endregion PuntoAtencion

                #region Medios

                oMediosPagoExterno.OrdenDePago = 1;
                oMediosPagoExterno.NroTarjeta = oVisaNETCabecera.NroTarjeta;
                oMediosPagoExterno.NroDocumento = oVisaNETCabecera.eTicket;
                oMediosPagoExterno.DescripcionTarjeta = oVisaNETCabecera.eTicket;
                oMediosPagoExterno.EnviaABoveda = false;
                oMediosPagoExterno.EsIngreso = 1;
                oMediosPagoExterno.EsEfectivo = false;
                oMediosPagoExterno.EsCheque = false;
                oMediosPagoExterno.FechaDocumento = today;
                oMediosPagoExterno.GeneraVuelto = false;
                oMediosPagoExterno.IdTipoDocumento = (Int16)TipoDocumentoComercial.TarjetaCredito;
                oMediosPagoExterno.IdTipoTarjeta = (Int16)3;//Tarjeta visa// enumTipoTarjeta.Visa;
                oMediosPagoExterno.ImporteCambioTotal = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oMediosPagoExterno.ImporteOrigenTotal = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oMediosPagoExterno.TipoCambio = (Decimal)1;
                oMediosPagoExterno.IdMonedaOrigen = (Int16)Moneda.Soles;
                oMediosPagoExterno.IdMonedaCambio = (Int16)Moneda.Soles;

                #endregion Medios

                #region CajaMoneda

                oCajaMoneda.IdMoneda = (Int16)Moneda.Soles;
                oCajaMoneda.IdMonedaCambio = (Int16)Moneda.Soles;
                oCajaMoneda.IdNroCaja = 0;
                oCajaMoneda.IdTipoDocumentoComercial = (Int16)TipoDocumentoComercial.TarjetaCredito;
                oCajaMoneda.Importe = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.ImporteBovedaCambio = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.ImporteBoveda = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.ImporteDescargo = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.NroTransaccion = 0;
                oCajaMoneda.Seleccionar = true;
                oCajaMoneda.TipoCambio = (Decimal)1;

                #endregion CajaMoneda

                #region ListaDocumentosBase

                oParametro["@p_idnroorden"] = idNumOrden;
                DataTable oDocumentos = _DAO.EjecutarComandoEntidad("VisaNet", "ObtenerDocumentosPorConciliar", oParametro, oConexion);

                foreach (DataRow oItemFila in oDocumentos.Rows)
                {
                    clsDocumentosDescargoPropio oDocumentoPropio = new clsDocumentosDescargoPropio();
                    oDocumentoPropio.ImporteOrigenDescargo = Convert.ToDecimal(oItemFila["Importe"]);
                    oDocumentoPropio.ImporteOrigenDeuda = Convert.ToDecimal(oItemFila["Importe"]);
                    oDocumentoPropio.IdEmpresa = oVisaNETCabecera.IdEmpresa;
                    oDocumentoPropio.IdTipoDocumento = Convert.ToInt16(oItemFila["IdTipoDocumento"]);
                    oDocumentoPropio.NroDocumento = oItemFila["NroDocumento"].ToString();
                    oDocumentoPropio.IdTipoDocumentoSustento = Convert.ToInt16(oItemFila["IdTipoDocumentoSustento"]);
                    oDocumentoPropio.NroDocumentoSustento = oItemFila["NroDocumentoSustento"].ToString();
                    oDocumentoPropio.IdServicio = Convert.ToInt16(oItemFila["idServicio"]);
                    oDocumentoPropio.OrdenPagoServicio = Convert.ToInt32(oItemFila["OrdendePago"]);
                    oDocumentoPropio.IdNroServicio = oVisaNETCabecera.IdNroServicio;

                    oDocumentosBase.Add(oDocumentoPropio);
                }

                #endregion ListaDocumentosBase

                #region Transaccion

                oTransaccion.FechaPago = today;
                oTransaccion.FechaRegistro = today;
                oTransaccion.IdMovComercial = (Int16)enumMovimientoComercial.Convenios; //verificar no es cobranza es convnio
                oTransaccion.NroTransaccion = 0;
                oTransaccion.IdMonedaNacional = (Int16)Moneda.Soles;
                oTransaccion.ImporteNeto = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.TipoOperacion = TipoOperacionCobranza.Recaudacion;
                oTransaccion.IdEstado = (Int16)Estado.Activo;
                oTransaccion.IdNroServicio = oVisaNETCabecera.IdNroServicio;
                oTransaccion.ImporteEfectivo = 0;
                oTransaccion.ImporteTotalVuelto = 0;
                oTransaccion.ImporteNeto = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.ImporteRecibido = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.ImporteValores = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.UsuarioPuntoAtencion = oUsuarioPuntoAtencion;
                oTransaccion.ElementosDocumentoDescargo = oDocumentosBase;
                oTransaccion.ElementosMedioPago = new List<clsMediosPagoBase>();
                oTransaccion.ElementosMedioPago.Add(oMediosPagoExterno);
                oTransaccion.ElementosCajaServicioMoneda = new List<clsCajaEstadisticaDiarioMoneda>();
                oTransaccion.ElementosCajaServicioMoneda.Add(oCajaMoneda);

                #endregion Transaccion

                #endregion

                SqlTransaction oTransaccionSQL = oConexion.BeginTransaction(IsolationLevel.ReadUncommitted);

                try
                {
                    #region Variables Locales

                    Int16 _intpositivonegativo = 1;
                    Int32 _intidtransaccionlog = 0;
                    Int32 _nrodocumentos = 0;
                    Int32 _nroservicios = 0;
                    Int32 _idnroservicioppago = 0;
                    Hashtable htDepositos = new Hashtable();

                    Int32 periodoPago = oTransaccion.PeriodoPago;

                    #endregion Variables Locales

                    #region Validaciones previas

                    if (oTransaccion.ElementosDocumentoDescargo.Count == 0)
                        throw new Exception("<<<Ingrese Documentos de Descargo.>>>");

                    if (oTransaccion.ElementosMedioPago.Count == 0)
                        throw new Exception("<<<Ingrese forma de Pago.>>>");

                    if (oTransaccion.IdMonedaNacional <= 0)
                        throw new Exception("<<<El Tipo de Moneda Nacional no se ha especificado para está transacción " + oTransaccion.IdMonedaNacional + ".>>>");

                    if (oTransaccion.ImporteNeto == 0)
                    {
                        StringBuilder _sbmensaje = new StringBuilder();
                        _sbmensaje.AppendLine("<<<Importe Neto de la Transacción es Cero.");
                        _sbmensaje.AppendLine("No se Aplico las Formas de Pago a los Documentos Cargados.>>>");

                        throw new Exception(_sbmensaje.ToString());
                    }

                    #endregion Validaciones previas

                    #region Verificar Caja Aperturada

                    if (oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo == 0)
                    {
                        Int32 IntCantidadPuntosAbiertos = clsCobranzaNGCDAO.Instancia.CantidadAbiertos(oTransaccion.UsuarioPuntoAtencion.IdUsuario);

                        if (IntCantidadPuntosAbiertos == 0)
                        {
                            clsCajaEstadisticaDiario oCajaAperturar = new clsCajaEstadisticaDiario();
                            oCajaAperturar.UsuarioPuntoAtencion = oTransaccion.UsuarioPuntoAtencion;
                            oCajaAperturar.IdNroCaja = 0;
                            oCajaAperturar.FechaApertura = DateTime.Now;
                            oCajaAperturar.FechaPago = DateTime.Now;

                            if (clsCobranzaNGCDAO.Instancia.AperturarFechaPago(oTransaccionSQL, oCajaAperturar, Decimal.Zero))
                            {
                                oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaAperturar.FechaApertura;
                                oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaAperturar.FechaPago;
                                oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaAperturar.IdNroCaja;
                            }
                            else
                                throw new Exception("<<<No se logró realizar la operación de cobranza.>>>");
                        }
                        else if (IntCantidadPuntosAbiertos == 1)
                        {
                            clsCajaEstadisticaDiario oCajaActual = clsCobranzaNGCDAO.Instancia.ObtenerCajaAtencionUltima(oTransaccion.UsuarioPuntoAtencion.IdEmpresa
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdUnidadNegocio
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdCentroServicio
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdTipoAtencion
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdPuntoAtencion
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdUsuario,
                                                                                            (DateTime?)null);

                            if (oCajaActual != null)
                            {
                                DateTime datFechaServidor = DateTime.Now;

                                if (oCajaActual.FechaApertura.Date.CompareTo(datFechaServidor.Date) == 0) //Debería entrar aca para obtener IdCaja
                                {
                                    oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaActual.FechaApertura;
                                    oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaActual.FechaPago;
                                    oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaActual.IdNroCaja;
                                }
                            }
                        }
                    }

                    #endregion Verificar Caja Aperturada

                    #region Grabando en Transaccion

                    DateTime fInicio = DateTime.Now;

                    oTransaccion.IdTransaccionLog = _intidtransaccionlog;
                    oTransaccion.IdTipoDocumento = 4;
                    oTransaccion.IdEstado = 1;
                    oTransaccion.ImporteNeto *= _intpositivonegativo;
                    oTransaccion.ImporteRecibido *= _intpositivonegativo;
                    oTransaccion.ImporteTotalVuelto *= _intpositivonegativo;
                    oTransaccion.ImporteEfectivo *= _intpositivonegativo;
                    oTransaccion.ImporteValores *= _intpositivonegativo;

                    oTransaccion.NroTransaccion = clsCobranzaNGCDAO.Instancia.TransaccionInsertar(oTransaccionSQL, oTransaccion);

                    if (oTransaccion.NroTransaccion <= 0)
                        throw new Exception("<<<No se registro en Transaccion.>>>");

                    #endregion Grabando en Transaccion

                    #region Grabando en Transaccion Documentos Descargo

                    _idnroservicioppago = 0;

                    foreach (clsDocumentosDescargoBase itemdocumentodescargo in oTransaccion.ElementosDocumentoDescargo)
                    {
                        clsDocumentosDescargoPropio itemdocumentodescargopropio = null;
                        itemdocumentodescargo.EsTerceros = false;

                        itemdocumentodescargopropio = (clsDocumentosDescargoPropio)itemdocumentodescargo;

                        if (!_idnroservicioppago.Equals(itemdocumentodescargopropio.IdNroServicio))
                        {
                            _idnroservicioppago = itemdocumentodescargopropio.IdNroServicio;

                            oTransaccion.PeriodoPago = clsCobranzaNGCDAO.Instancia.ObtenerPeriodoPago(_idnroservicioppago);
                            itemdocumentodescargopropio.Periodo = oTransaccion.PeriodoPago;

                            if (oTransaccion.PeriodoPago == 0)
                            {
                                oTransaccion.PeriodoPago = periodoPago;
                                itemdocumentodescargopropio.Periodo = periodoPago;
                            }
                        }

                        if (itemdocumentodescargopropio.ImporteOrigenDescargo != 0)
                        {
                            itemdocumentodescargopropio.NroTransaccion = oTransaccion.NroTransaccion;
                            itemdocumentodescargopropio.ImporteOrigenCobrar *= _intpositivonegativo;
                            itemdocumentodescargopropio.ImporteOrigenDescargo *= _intpositivonegativo;
                            itemdocumentodescargopropio.ImporteOrigenDeuda *= _intpositivonegativo;

                            clsCobranzaNGCDAO.Instancia.Insertar(oTransaccionSQL
                                    , oTransaccion
                                    , itemdocumentodescargopropio
                                    , oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                        }
                    }

                    #endregion Grabando en Transaccion Documentos Descargo

                    #region Grabando en Transaccion Formas de Pago

                    foreach (clsMediosPagoBase itemmediopago in oTransaccion.ElementosMedioPago)
                    {
                        itemmediopago.NroTransaccion = oTransaccion.NroTransaccion;
                        itemmediopago.ImporteCambioTotal *= _intpositivonegativo;
                        itemmediopago.ImporteOrigenTotal *= _intpositivonegativo;
                        itemmediopago.ImporteCambioDescargo *= _intpositivonegativo;
                        itemmediopago.ImporteOrigenDescargo *= _intpositivonegativo;

                        clsMediosPagoExterno itemmediopagoexterno = (clsMediosPagoExterno)itemmediopago;

                        itemmediopagoexterno.EsIngreso = 1;

                        if (itemmediopagoexterno.NroDocumento == "[Vuelto]")
                            itemmediopagoexterno.EsIngreso = 0;

                        clsCobranzaNGCDAO.Instancia.InsertarMedioDePago(oTransaccionSQL, itemmediopagoexterno, oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                    }

                    #endregion Grabando en Transaccion Formas de Pago

                    #region Actualizando Control de Caja

                    foreach (clsCajaEstadisticaDiarioMoneda item in oTransaccion.ElementosCajaServicioMoneda)
                    {
                        item.ImporteCambio *= _intpositivonegativo;
                        item.Importe *= _intpositivonegativo;
                        item.IdNroCaja = oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo;

                        clsCobranzaNGCDAO.Instancia.InsertarCaja(oTransaccionSQL, item);
                    }

                    /////////////
                    RegistrarCajaestadisticaservicio(oTransaccionSQL, transaccion, ref _nroservicios, ref _nrodocumentos, CacheConfiguracion.IdOperacionCobranza);


                    clsCobranzaNGCDAO.Instancia.ActualizarCaja(oTransaccionSQL, oTransaccion.NroTransaccion, _nrodocumentos, _nroservicios);

                    oTransaccionSQL.Commit();

                    #endregion Actualizando Control de Caja

                    #region Resultado
                    if (oTransaccion.NroTransaccion > 0)
                    {
                        oResultado = new Hashtable();
                        oResultado["NroPedido"] = entidad.Parametros["NroPedido"];
                        oResultado["NroTransaccion"] = oTransaccion.NroTransaccion;
                        oResultado["NroTarjeta"] = "";
                        oResultado["MensajeExcepcion"] = "Sin mensaje";

                        oResultado["Mensaje"] = "Operación de Pago con Éxito.";
                        oResultado["IdError"] = 1;
                    }
                    else
                    {
                        oResultado = new Hashtable();
                        oResultado["NroPedido"] = entidad.Parametros["NroPedido"];
                        oResultado["NroTransaccion"] = 0;
                        oResultado["NroTarjeta"] = "";
                        oResultado["MensajeExcepcion"] = "Sin mensaje";

                        oResultado["Mensaje"] = "Operación de Pago Sin Éxito.";
                        oResultado["IdError"] = 0;
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    #region Error

                    oTransaccionSQL.Rollback();

                    oResultado = new Hashtable();
                    oResultado["NroPedido"] = entidad.Parametros["NroPedido"];
                    oResultado["NroTransaccion"] = 0;
                    oResultado["NroTarjeta"] = "";
                    oResultado["IdCobranzaExterna"] = 0;
                    oResultado["MensajeExcepcion"] = ex.Message;

                    oResultado["Mensaje"] = "Error en el proceso de Cobranza";
                    oResultado["IdError"] = 0;

                    oResultado = oResultado.ToDictionary().ToHashtableDAO();

                    _DAO.EjecutarComandoRegistro("CobranzaVisa", "ActualizarIntentoPago", oResultado);

                    #endregion Error
                }
                finally
                {
                    if (oConexion.State == ConnectionState.Open)
                        oConexion.Close();
                }
            }

            return oResultado;
        }

        public Boolean RegistrarCajaestadisticaservicio(SqlTransaction sqltransaccion
                             , clsTransaccion transaccion
                             , ref Int32 totalservicios
                             , ref Int32 totaldocumentos
                             , Int16 idoperacion)
        {
            //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
            Hashtable _htparametros = new Hashtable();
            totalservicios = 0;
            totaldocumentos = 0;

            _htparametros.Add("@nrotransaccion", transaccion.NroTransaccion);



            DataTable dtresumen = _DAO.EjecutarComandoEntidad("cajaestadisticaservicio", "obtenerresumen", _htparametros, sqltransaccion);



            if (dtresumen == null || dtresumen.Rows.Count == 0)
            { throw new Exception("<<<Error al obtener resumen de Caja Servicios.>>>"); }



            foreach (DataRow dr in dtresumen.Rows)
            {
                totalservicios = totalservicios + Convert.ToInt32(dr["totalservicios"]);
                totaldocumentos = totaldocumentos + Convert.ToInt32(dr["totaldocumentos"]);



                _htparametros = new Hashtable();
                _htparametros.Add("@idnrocaja", transaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                _htparametros.Add("@idservicio", Convert.ToInt16(dr["idservicio"]));
                _htparametros.Add("@nroservicios", Convert.ToInt32(dr["totalservicios"]));
                _htparametros.Add("@nrodocumentos", Convert.ToInt32(dr["totaldocumentos"]));



                if (idoperacion == CacheConfiguracion.IdOperacionCobranza)
                    _DAO.EjecutarComando("cajaestadisticaservicio", "insertar", _htparametros, sqltransaccion);
                else
                    _DAO.EjecutarComando("cajaestadisticaservicio", "revertir", _htparametros, sqltransaccion);
            }



            return true;
        }


        public void ActualizarEstadoOrdenVisa(clsParametro entidad)
        {
            Hashtable _objparametros = new Hashtable();
            var param = entidad.Parametros.ToHashtableDAO();

            try
            {
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ActualizarEstadoOrdenVisa", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> VisaListFailed(clsParametro entidad)
        {
            try
            {
                var param = new Hashtable();
                if (entidad != null)
                    param = entidad.Parametros.ToHashtableDAO();

                var result = await _DAA.ExecuteTableAsync("Financiamiento", "ListFailed", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarMetodoPagoOrdenVisa(clsParametro entidad)
        {
            Hashtable _objparametros = new Hashtable();
            var param = entidad.Parametros.ToHashtableDAO();

            try
            {
                var result = _DAO.EjecutarComandoEntidad("dbo.Financiamiento", "ActualizarMetodoPago", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion PAGO NIUBIZ

        #region NGC ESCRITORIO
        public static clsListaOrdenCobroDocumento ObtenerListaPorOrdenCobro(Int32 idordencobro, Int16 idmonedanacional, SqlTransaction transaction)
        {
            //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
            Hashtable _objparametros = new Hashtable();
            clsListaOrdenCobroDocumento _lstordencobro = new clsListaOrdenCobroDocumento();
            DataSet _dsordencobro;

            _objparametros.Add("@idordencobro", idordencobro);
            _objparametros.Add("@idmonedanacional", idmonedanacional);

            try
            {
                _dsordencobro = _DAO.EjecutarComandoConjuntoEntidad("OrdenCobroDocumento",
                                                                           "ObtenerListaPorOrdenCobro"
                                                                      , _objparametros, transaction
                                                                      , "Documentos"
                                                                      , "Detalle"
                                                                      , "Referencia");
                if (_dsordencobro == null) { return null; }
                _lstordencobro = new clsListaOrdenCobroDocumento(_dsordencobro.Tables["Documentos"], _dsordencobro.Tables["Detalle"], _dsordencobro.Tables["Referencia"]);
            }
            catch (SqlException ex)
            { throw new Exception(ex.Message + ex.StackTrace); }
            catch (Exception ex)
            { throw new Exception(ex.Message + ex.StackTrace); }

            return _lstordencobro;
        }

        #endregion NGC ESCRITORIO

        #endregion LESTRADA

        #region RBERROSPI 

        public async Task<DataTable> ObtenerMetodosPago(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("dbo.Financiamiento", "ObtenerMetodosPago", param);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Boolean> ValidarPagosMultiples(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                param["@p_Count"] = 0;
                var result = await _DAA.ExecuteTableAsync("dbo.Financiamiento", "ValidarPagosMultiples", param);

                Boolean _validadarNroServicio = Convert.ToInt32(param["@p_Count"]) > 0;
                Boolean _validadarOrdenCobro = param["@p_IdOrdenCobro"] != null;
                return _validadarNroServicio && _validadarOrdenCobro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region MCASTRO

        public async Task<DataRow> ObtenerIntento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("Financiamiento", "ObtenerIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Datos para validaciones 
        public int ObtenerUsuarioFinanciamientoWeb(string usuario)
        {
            var param = new Hashtable();
            int result = 0;
            try
            {
                param["@p_usuariofinweb"] = usuario;
                object idusuario;

                idusuario = _DAO.EjecutarComandoEscalar("Financiamiento", "obtenerusuariofinanciamiento", param);

                if (idusuario == null)
                    result = 0;

                result = Convert.ToInt32(idusuario);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow ConsultaExisteRestriccion(int idnroservicio)
        {
            var param = new Hashtable();
            var Row = (DataRow)null;

            try
            {
                param["@idnroservicio"] = idnroservicio;
                Row = _DAO.EjecutarComandoRegistro("alertarestriccion", "consultarexistenciarestricciones", param);

                return Row;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerPeriodoVigente(int idempresa, int iduunn)
        {
            var param = new Hashtable();
            int result = 0;
            try
            {
                param["@p_idempresa"] = idempresa;
                param["@p_idunidadnegocio"] = iduunn;
                param["@p_idservicio"] = 1;
                param["@p_idflujo"] = 1;

                object periodoVigente;

                periodoVigente = _DAO.EjecutarComandoEscalar("PeriodoComercialVigente", "ObtenerPorUUNN", param);

                if (periodoVigente == null)
                    result = 0;

                result = Convert.ToInt32(periodoVigente);
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public enteListaNroServicioIntereses ObtenerIntereses(int idnroservicio)
        {

            var param = new Hashtable();
            enteListaNroServicioIntereses listaresult = new enteListaNroServicioIntereses();
            var table = (DataTable)null;

            try
            {
                param["@p_idnroservicio"] = idnroservicio;

                table = _DAO.EjecutarComandoEntidad("convenio", "ObtenerIntereses", param);

                foreach (DataRow oItemFila in table.Rows)
                {
                    enteNroServicioIntereses intereses = new enteNroServicioIntereses();

                    intereses.IdConcepto = Convert.ToInt16(oItemFila["IdConcepto"]);
                    intereses.NombreConcepto = Convert.ToString(oItemFila["Nombre"]);
                    intereses.Importe = Convert.ToDecimal(oItemFila["Importe"]);
                    intereses.EsTributable = Convert.ToBoolean(oItemFila["EsTributable"]);
                    intereses.TasaImpuesto = Convert.ToDecimal(oItemFila["TasaImpuesto"]);

                    listaresult.Elementos.Add(intereses);
                }

                return listaresult;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public enteDatoSuministro ObtenerDatoSuministro(int idnroservicio)
        {
            enteDatoSuministro result;
            var param = new Hashtable();
            var Row = (DataRow)null;

            try
            {
                param["@p_idnroservicio"] = idnroservicio;
                Row = _DAO.EjecutarComandoRegistro("convenio", "obtenerdatosuministro", param);
                result = new enteDatoSuministro(Row);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region OrdenCobro
        public string GenerarOrdencobroConvenioGuardar(enteOrdenDeCobro ordenCobro, int idnroservicio)
        {
            string nroOrdenCobro = string.Empty;
            string idRegistroConvenio = string.Empty;
            string nroOrdenCobroDocumento = string.Empty;

            try
            {
                // -------------------------------------------------
                // Guarda la cabecera de la orden de cobro.
                // -------------------------------------------------
                nroOrdenCobro = this.GuardarCabecera(ordenCobro);

                // -------------------------------------------------
                // Iterar por cada documento de la orden de cobro.
                // -------------------------------------------------
                foreach (enteOrdenCobroDocumento ordenDocumento in ordenCobro.ListaOrdenCobroDocumento.Elementos)
                {
                    nroOrdenCobroDocumento = this.GuardarDocumento(nroOrdenCobro, ordenDocumento);

                    // ----------------------------------------------------
                    // Iterar por el detalle de cada documento.
                    // ----------------------------------------------------
                    foreach (enteOrdenCobroDocumentoDetalle ordenDocumentoDetalle in ordenDocumento.ListaDetalleDocumento.Elementos)
                    {
                        this.GuardarDocumentoDetalle(nroOrdenCobroDocumento, ordenDocumentoDetalle);
                    }
                }



                return nroOrdenCobro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GuardarCabecera(enteOrdenDeCobro ordenCobro)
        {
            // var transaction = (SqlTransaction)null;
            string result;
            Hashtable htparam = new Hashtable();
            try
            {
                htparam["@idordencobro"] = 0;
                htparam["@idusuarioautoriza"] = ordenCobro.IdUsuarioAutoriza;
                htparam["@idmovimientocomercial"] = ordenCobro.IdMovimientoComercial;
                htparam["@idnromovimientocomercial"] = ordenCobro.IdNroMovimientoComercial;
                htparam["@idnroservicio"] = ordenCobro.IdNroServicio;
                htparam["@importe"] = ordenCobro.Importe;
                htparam["@idmoneda"] = ordenCobro.IdMoneda;

                _DAO.EjecutarComando("OrdenDeCobro", "Insertar", htparam);

                var obj = Convert.ToInt32(htparam["@idordencobro"]);

                result = Convert.ToString(obj);
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GuardarDocumento(string nroOrdenCobro, enteOrdenCobroDocumento ordenDocumento)
        {
            string result;
            Hashtable htparam = new Hashtable();

            try
            {
                htparam["@idordencobrodoc"] = 0;
                htparam["@idordencobro"] = nroOrdenCobro;
                htparam["@idempresa"] = ordenDocumento.IdEmpresa;
                htparam["@idtipodocumento"] = ordenDocumento.IdTipoDocumento;
                htparam["@importe"] = ordenDocumento.Importe;
                htparam["@idmoneda"] = ordenDocumento.IdMoneda;
                htparam["@descripcion"] = ordenDocumento.Descripcion;
                htparam["@escobrable"] = ordenDocumento.EsCobrable;

                _DAO.EjecutarComando("OrdenCobroDocumento", "Insertar", htparam);

                var obj = Convert.ToInt32(htparam["@idordencobrodoc"]);

                result = Convert.ToString(obj);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GuardarDocumentoDetalle(object nroOrdenCobroDocumento, enteOrdenCobroDocumentoDetalle ordenDocumentoDetalle)
        {
            Hashtable htparam = new Hashtable();

            try
            {
                htparam["@idordencobrodoc"] = nroOrdenCobroDocumento;
                htparam["@idconcepto"] = ordenDocumentoDetalle.IdConcepto;
                htparam["@importe"] = ordenDocumentoDetalle.Importe;
                htparam["@preciounitario"] = ordenDocumentoDetalle.PrecioUnitario;
                htparam["@preciounitariosinfose"] = ordenDocumentoDetalle.PrecioUnitario;
                htparam["@cantidad"] = ordenDocumentoDetalle.Cantidad;
                htparam["@indicadorigv"] = ordenDocumentoDetalle.IndicadorIGV;
                htparam["@descripcionconcepto"] = ordenDocumentoDetalle.DescripcionConcepto;
                htparam["@idtipodocumentosustento"] = ordenDocumentoDetalle.IdTipoDocumentoSustento;

                _DAO.EjecutarComando("OrdenCobroDocumentoDetalle", "Insertar", htparam);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion OrdenCobro

        #region Guardar Datos Convenio temporal

        public void AnularConvenioTemporalAnterior(int idnroservicio)
        {
            Hashtable htparam = new Hashtable();
            try
            {
                htparam["@idnroservicio"] = idnroservicio;
                _DAO.EjecutarComando("convenio", "AnularConvenioTemporal", htparam);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public string GuardarCabeceraConvenioTemporal(string nroOrdenCobro, enteOrdenDeCobro convenioResumen, clsParametro param)
        {
            Hashtable htparam = new Hashtable();
            var TotalFacturado = Convert.ToDecimal(param.Parametros["TotalFacturado"]);
            var InteresAFecha = Convert.ToDecimal(param.Parametros["InteresAFecha"]);
            var MontoInicial = Convert.ToDecimal(param.Parametros["MontoInicial"]);
            var InicialExigida = Convert.ToDecimal(param.Parametros["InicialExigida"]);
            var SaldoAFavorUsar = Convert.ToDecimal(param.Parametros["SaldoAFavorUsar"]);
            var MontoFinanciar = Convert.ToDecimal(param.Parametros["MontoFinanciar"]);
            var ValorTasa = Convert.ToDecimal(param.Parametros["ValorTasa"]);
            var MontoCuota = Convert.ToDecimal(param.Parametros["ImporteCuota"]);
            var TotalDeuda = TotalFacturado + InteresAFecha;


            try
            {
                htparam["@idregistro"] = 0;
                htparam["@idconveniotipo"] = Convert.ToInt16(param.Parametros["IdConvenioTipo"]);
                htparam["@idnroservicio"] = Convert.ToInt32(param.Parametros["IdNroServicio"]);
                htparam["@anocomercial"] = param.Parametros["PeriodoPago"].ToString().Substring(0, 4);
                htparam["@mescomercial"] = param.Parametros["PeriodoPago"].ToString().Substring(4, 2);
                htparam["@fechaconvenio"] = DateTime.Now;
                htparam["@idmoneda"] = Convert.ToInt16(param.Parametros["IdMoneda"]);
                htparam["@deudapendiente"] = TotalDeuda;
                htparam["@deudaxnotificar"] = TotalDeuda - MontoInicial;
                htparam["@descuento"] = 0;//Financiamiento por web no se puede descontar
                htparam["@deuda"] = TotalDeuda - MontoInicial;
                htparam["@inicialexigida"] = InicialExigida;
                htparam["@pagoinicial"] = MontoInicial; //+ SaldoAFavorUsar;
                htparam["@capital"] = MontoFinanciar;
                htparam["@nrocuotas"] = Convert.ToInt16(param.Parametros["NumeroCuotas"]);
                htparam["@tasainteres"] = ValorTasa;
                htparam["@valorcuota"] = MontoCuota;
                htparam["@nrodeoportunidades"] = 1;
                htparam["@fechainicioanotificar"] = DateTime.Now;
                htparam["@observacion"] = null;
                htparam["@esfirmasolotitular"] = 0; //Firma del solo titular
                htparam["@idtipoidentidad"] = Convert.ToInt16(param.Parametros["IdTipoIdentidad"]);
                htparam["@nroidentidad"] = param.Parametros["NroIdentidad"].ToString();
                htparam["@idnroserviciopropietario"] = Convert.ToInt32(param.Parametros["IdNroServicio"]);
                htparam["@iduunn"] = Convert.ToInt16(param.Parametros["IdUUNN"]);
                htparam["@idcentroservicio"] = Convert.ToInt16(param.Parametros["IdCentroServicio"]);
                htparam["@idtipoatencion"] = null;
                htparam["@idpuntoatencion"] = null;
                htparam["@idusuario"] = Convert.ToInt32(param.Parametros["IdUsuario"]);
                htparam["@generacorte"] = 0;
                htparam["@idmovimientocomercial"] = enumMovimientoComercial.Convenios;
                htparam["@idordencobro"] = nroOrdenCobro;

                _DAO.EjecutarComando("convenio", "insertartemporal", htparam);

                return htparam["@idregistro"].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void GuardarConveniosFinanciado(string idRegistroConvenio, enteNroServicioDeuda deudaNroServicio)
        {
            Hashtable htparam = new Hashtable();

            // Iterar por convenio.
            foreach (enteNroServicioDeudaPorFacturar odeudaPorFacturada in deudaNroServicio.ListaDeudaPorFacturar.Elementos)
            {
                htparam["@idregistroconvenio"] = idRegistroConvenio;
                htparam["@nroconvenio"] = odeudaPorFacturada.NumeroDocumento;

                _DAO.EjecutarComando("ConveniosRefinanciado", "InsertarTemporal", htparam);
            }

        }

        public void GuardarDocumentosFinanciado(string idRegistroConvenio, enteNroServicioDeuda deudaNroServicio)
        {
            Hashtable htparam = new Hashtable();
            Hashtable htparamint = new Hashtable();

            // Iterar por recibo del suministro.
            foreach (enteNroServicioDeudaFacturada odeudaFacturada in deudaNroServicio.ListaDeudaFacturada.ElementosGrilla)
            {
                // Iterar por documentos sustentos del recibo.
                foreach (enteNroServicioDeudaFacturadaSustento odeudaSustento in odeudaFacturada.ListaDocumentosSustentos.Elementos)
                {

                    htparam["@idregistroconvenio"] = idRegistroConvenio;
                    htparam["@idempresa"] = odeudaSustento.IdEmpresa;
                    htparam["@idnroservicio"] = odeudaFacturada.IdNroServicio;
                    htparam["@idtipodocumento"] = odeudaFacturada.IdTipoDocumento;
                    htparam["@nrodocumento"] = odeudaFacturada.NroDocumento;
                    htparam["@idtipodocumentosustento"] = odeudaSustento.IdTipoDocumentoSustento;
                    htparam["@nrodocumentosustento"] = odeudaSustento.NroDocumentoSustento;
                    htparam["@espropio"] = odeudaSustento.EsPropio;

                    htparamint["@p_idempresa"] = odeudaSustento.IdEmpresa;
                    htparamint["@p_idtipodocumento"] = odeudaFacturada.IdTipoDocumento;
                    htparamint["@p_nrodocumento"] = odeudaFacturada.NroDocumento;
                    htparamint["@p_idtipodocumentosustento"] = odeudaSustento.IdTipoDocumentoSustento;
                    htparamint["@p_nrodocumentosustento"] = odeudaSustento.NroDocumentoSustento;
                    htparamint["@p_idnroservicio"] = odeudaFacturada.IdNroServicio;

                    _DAO.EjecutarComando("Convenio", "ActualizarValorizadoInteresesDeudaFinanciar", htparamint);
                    _DAO.EjecutarComando("conveniodocumentosfinanciado", "insertartemporal", htparam);

                }
            }

        }

        //public void GuardarNotaAbono(string idRegistroConvenio, enteConvenioResumen convenioResumen)
        //{
        //    if (convenioResumen.MontoADescontar == 0) return;

        //    Hashtable htparam = new Hashtable();

        //    htparam["@p_idregistroconvenio"] = idRegistroConvenio;
        //    htparam["@p_idtipodocumento"] = convenioResumen.IdTipoDocumentoDescuento;
        //    htparam["@p_importe"] = convenioResumen.MontoADescontar;

        //    _DAO.EjecutarComando(_osqltran, "ConvenioDescuento", "InsertarTemporal", htparam);
        //}

        #endregion Guardar Datos Convenio Temporal

        #region Datos Reutilizar
        public enteNroServicioDeuda ObtenerDatoSuministro(clsParametro param)
        {

            Hashtable htparam = new Hashtable();
            DataRow drregistro = null;
            enteNroServicioDeuda onroservicio = null;

            htparam.Clear();
            htparam["@p_idnroservicio"] = Convert.ToInt32(param.Parametros["IdNroServicio"]);

            drregistro = _DAO.EjecutarComandoRegistro
                ("Convenio"
                , "ObtenerDatoSuministro"
                , htparam);

            if (drregistro != null)
            {
                onroservicio = new enteNroServicioDeuda();
                onroservicio.IdNroServicio = Convert.ToInt32(param.Parametros["IdNroServicio"]);
                onroservicio.Nombre = drregistro["nombrenroservicio"].ToString();
                onroservicio.Direccion = drregistro["direccion"].ToString();
                onroservicio.IdEstado = (EstadoGenerales)Convert.ToInt16(drregistro["estado"]);
                onroservicio.IdNivelTension = Convert.ToInt16(drregistro["IdNivelTension"]);
                onroservicio.IdTipoIdentidadPropietario = Convert.ToInt16(drregistro["idtipoidentidadpropietario"]);
                onroservicio.NroIdentidadPropietario = drregistro["nroidentidadpropietario"].ToString();
                onroservicio.IdTipoIdentidad = Convert.ToInt16(drregistro["idtipoidentidad"]);
                onroservicio.NroIdentidad = drregistro["nroidentidad"].ToString();
                onroservicio.NombreCartera = drregistro["nombrecartera"].ToString();
                onroservicio.Tarifa = drregistro["tarifa"].ToString();
            }

            return onroservicio;
        }
        public enteListaNroServicioDeudaFacturada ObtenerDeudaFacturada(int idnroservicio)
        {
            Hashtable htparam = new Hashtable();
            DataTable dtdeuda = null;
            DataTable dtdeudasustento = null;
            enteNroServicioDeudaFacturada orecibo = new enteNroServicioDeudaFacturada();
            enteListaNroServicioDeudaFacturada oresultado = new enteListaNroServicioDeudaFacturada();
            enteListaNroServicioDeudaFacturadaSustento osustento = null;

            htparam.Clear();
            htparam["@p_idnroservicio"] = idnroservicio;

            try
            {
                // Obtener deuda a nivel de IngresoProvision.
                dtdeuda = _DAO.EjecutarComandoEntidad("Convenio", "ObtenerDeudaIP", htparam);

                // Si existe deuda, obtener los documentos sustentos.
                if (dtdeuda != null)
                {
                    htparam.Clear();

                    // Iterar por documento IngresoProvision para obtener la deuda por Documentos Sustentos.
                    foreach (DataRow drrecibo in dtdeuda.Rows)
                    {
                        htparam["@p_idempresa"] = Convert.ToInt16(drrecibo["idempresa"]);
                        htparam["@p_idtipodocumento"] = Convert.ToInt16(drrecibo["idtipodocumento"]);
                        htparam["@p_nrodocumento"] = drrecibo["nrodocumento"].ToString();
                        orecibo = new enteNroServicioDeudaFacturada(drrecibo);

                        // Obtener deuda sustentos.
                        dtdeudasustento = (DataTable)_DAO.EjecutarComandoEntidad("Convenio", "ObtenerDeudaIPD", htparam);

                        if (dtdeudasustento != null)
                        {
                            osustento = new enteListaNroServicioDeudaFacturadaSustento(dtdeudasustento);
                            orecibo.AgregarSustento(osustento);

                            // Validar que el total deuda de la cabecera sea igual al total deuda del sustento.
                            if (osustento.TotalDeudaSustento != orecibo.Saldo)
                                orecibo.MensajeResultado = "Error: total deuda cabecera no cuada con total deuda de Detalle.";
                        }

                        oresultado.AgregarRecibo(orecibo);
                    }
                }

                return oresultado;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public enteListaNroServicioDeudaPorFacturar ObtenerDeudaPorFacturar(int idnroservicio)
        {
            Hashtable htparam = new Hashtable();
            htparam.Clear();

            try
            {
                htparam["@p_idnroservicio"] = idnroservicio;

                DataTable dtresultado = null;
                enteListaNroServicioDeudaPorFacturar odeudaporfacturar = null;

                dtresultado = _DAO.EjecutarComandoEntidad("Convenio", "ObtenerDeudaPorFacturar", htparam);

                odeudaporfacturar = new enteListaNroServicioDeudaPorFacturar(dtresultado);

                return odeudaporfacturar;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public enteListaNroServicioDocumentoAFavor ObtenerDocumentosAFavor(int idnroservicio)
        {
            Hashtable htparam = new Hashtable();
            htparam.Clear();
            try
            {
                htparam["@p_idnroservicio"] = idnroservicio;

                DataTable dtresultado = null;
                enteListaNroServicioDocumentoAFavor odeudadocafavor = null;

                dtresultado = _DAO.EjecutarComandoEntidad("Convenio", "ObtenerDocumentosAFavor", htparam);

                odeudadocafavor = new enteListaNroServicioDocumentoAFavor(dtresultado);

                return odeudadocafavor;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        #endregion Datos Reutilizar



        #region clsFinanciamientoNGCDAO


        public sealed class clsFinanciamientoNGCDAO
        {
            #region Field

            private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
            private static readonly Lazy<clsFinanciamientoNGCDAO> _Instancia = new Lazy<clsFinanciamientoNGCDAO>(() => new clsFinanciamientoNGCDAO(), LazyThreadSafetyMode.PublicationOnly);

            #endregion Field

            #region Property

            public static clsFinanciamientoNGCDAO Instancia
            {
                get { return _Instancia.Value; }
            }

            #endregion Property

            #region Constructor

            private clsFinanciamientoNGCDAO()
            {
            }

            #endregion Constructor

            #region Public

            public void GeneraNroDocumentoPuntoAtencionS(int idcentroservicio, int idpuntoatencion, Int16 idtipodocumento, Int16 esespecial, int idNumOrden, int idOrdenCobroDocumento, SqlTransaction transaction)
            {
                clsContadorDocumentoComercial _objcontador;
                Int16 _intidcontador;
                Int32 _intnumeroactual = -1;
                StringBuilder _strmensaje = new StringBuilder();
                String _strnumerodocumento;
                DataRow _drresultado;

                //clsGestionDato _objgestor = clsGestionDato.ObtenerInstancia();
                Hashtable _htparam = new Hashtable();

                _htparam["@idcentroservicio"] = idcentroservicio;
                _htparam["@idpuntoatencion"] = Convert.ToInt16(idpuntoatencion);
                _htparam["@idtipodocumento"] = idtipodocumento;
                _htparam["@esespecial"] = esespecial;
                _htparam["@nombrecentroservicio"] = "";
                _htparam["@nombretipoatencion"] = "";
                _htparam["@nombretipodocumento"] = "";

                _drresultado = _DAO.EjecutarComandoRegistro("contadordocumentocomercial"
                                                                , "obtenerdocumento"
                                                                , _htparam, transaction);
                if (_drresultado == null)
                    _objcontador = new clsContadorDocumentoComercial();
                else
                    _objcontador = new clsContadorDocumentoComercial(_drresultado);

                if (_objcontador.IdContador <= 0)
                {
                    String nombrecentroservicio = _htparam["@nombrecentroservicio"].ToString();
                    String nombretipoatencion = _htparam["@nombretipoatencion"].ToString();
                    String nombretipodocumento = _htparam["@nombretipodocumento"].ToString();

                    _strmensaje.AppendLine("<<<Aun no se ha asignado contador de Documento Comercial para los siguientes datos:");
                    _strmensaje.AppendLine("\nCentro Servicio: " + nombrecentroservicio + " (" + idcentroservicio.ToString() + ")");
                    _strmensaje.AppendLine("Punto Atencion: " + nombretipoatencion + " (" + idpuntoatencion.ToString() + ")");
                    _strmensaje.AppendLine("Tipo Documento: " + nombretipodocumento + " (" + idtipodocumento.ToString() + ")");
                    _strmensaje.AppendLine("\nResponsable: Atención a Clientes.>>>");

                    throw new Exception(_strmensaje.ToString());
                }

                // ---------------------------------------------------------------------
                // Validar Serie, Número de Posiciones definida.
                // ---------------------------------------------------------------------
                if (_objcontador.Serie.Trim().Length == 0)
                    throw new Exception("<<<No se ha definido serie para el documento.>>>");

                if (_objcontador.NPosiciones == 0)
                    throw new Exception("<<<No se ha definido número de posiciones.>>>");

                // ---------------------------------------------------------------------
                // Verificar si ya existe el Documento.
                // ---------------------------------------------------------------------
                String nrodoctemp = (_objcontador.Serie).Trim()
                                  + (_objcontador.NumeroActual
                                  + 1).ToString().Trim().PadLeft(_objcontador.NPosiciones, '0');

                _htparam.Clear();

                _htparam["@idempresa"] = _objcontador.IdEmpresa;
                _htparam["@idtipodocumento"] = idtipodocumento;
                _htparam["@nrodocumento"] = nrodoctemp;
                _htparam["@existe"] = 0;
                _htparam["@nombredocumento"] = "";
                _htparam["@nombreempresa"] = "";

                _DAO.EjecutarComando("contadordocumentocomercial"
                                         , "verificarexistedocumento"
                                         , _htparam, transaction);

                Boolean _bolexiste = Convert.ToBoolean(_htparam["@existe"]);

                if (_bolexiste)
                {
                    String nombredocumento = _htparam["@nombredocumento"].ToString();
                    String nombreempresa = _htparam["@nombreempresa"].ToString();

                    throw new Exception("<<<El Documento "
                                        + nombredocumento + " ("
                                        + idtipodocumento
                                        + ") con Número "
                                        + nrodoctemp + " para la empresa "
                                        + nombreempresa + " (" + _objcontador.IdEmpresa.ToString() + ") ya existe." + ">>>");
                }

                // ---------------------------------------------------------------------
                // Generar Número.
                // ---------------------------------------------------------------------
                _intnumeroactual = _objcontador.NumeroActual;
                _intnumeroactual++;
                _htparam.Clear();
                _htparam.Add("@idcentroservicio", idcentroservicio);
                _htparam.Add("@idpuntoatencion", idpuntoatencion);
                _htparam.Add("@idtipodocumento", idtipodocumento);
                _htparam.Add("@numeroactual", _intnumeroactual);
                _htparam.Add("@esespecial", esespecial);
                _htparam.Add("@numero", "");

                _DAO.EjecutarComandoEscalar("contadordocumentocomercial", "grabanumeroepecial", _htparam, transaction);
                _strnumerodocumento = _htparam["@numero"].ToString();
                if (String.IsNullOrEmpty(_strnumerodocumento))
                    throw new Exception("<<<No existe contador comercial para el centro de servicios .>>>");
                //_strnumerodocumento = (_objcontador.Serie).Trim()
                //                     + _intnumeroactual.ToString().Trim().PadLeft(_objcontador.NPosiciones, '0');
                _objcontador.NumeroActual = _intnumeroactual;
                _objcontador.NombreDocumento = _strnumerodocumento;
                _htparam.Clear();
                _htparam["@p_nrodocumento"] = _strnumerodocumento;
                _htparam["@p_NroOrden"] = idNumOrden;
                _htparam["@p_IdOrdenCobroDocumento"] = idOrdenCobroDocumento;

                _DAO.EjecutarComandoEntidad("Financiamiento", "ActualizarNroDocumento", _htparam, transaction);
                //return _objcontador;
            }


            public Hashtable RegistrarTransaccionPagoVisa(Int32 idNumOrden, Int32 idOrdenCobro, int metodoPago)
            {
                var result = new Hashtable();
                var param = new Hashtable();
                var ordenVisaNet = (clsFinanciamientoOrdenVisaNet)null;
                var financiamientoOrdenVisaNetDetalle = (clsListaFinanciamientoOrdenVisaNetDetalle)null;
                var idCobranzaExterna = 0;
                var idTransaccion = 0;
                var table = (DataTable)null;
                var row = (DataRow)null;
                var transaction = (SqlTransaction)null;
                var documentosPropios = new List<clsFinanciamientoDocumentosDescargoPropio>(); ;
                var documentosBase = new List<clsFinanciamientoDocumentosDescargoBase>();
                var documentoPropio = (clsFinanciamientoDocumentosDescargoPropio)null;
                var usuarioPuntoAtencion = new clsFinanciamientoUsuarioPuntoAtencion();
                var mediosPagoExterno = new clsFinanciamientoMediosPagoExterno();
                var cajaMoneda = new clsFinanciamientoCajaEstadisticaDiarioMoneda();
                var transaccion = new clsFinanciamientoTransaccion();
                var today = DateTime.Now;
                SqlConnection osqlcon = null;
                try
                {
                    osqlcon = _DAO.GetSqlConnection();

                    osqlcon.Open();
                    transaction = osqlcon.BeginTransaction();

                    #region OrdenVisaNet

                    param["@p_idnroorden"] = idNumOrden;
                    row = _DAO.EjecutarComandoRegistro("FinanciamientoOrdenVisaNet", "Obtener", param, transaction);
                    ordenVisaNet = new clsFinanciamientoOrdenVisaNet(row);

                    #endregion OrdenVisaNet

                    #region Generar OrdenTrabajo = NroDocumento
                    param["@p_IdOrdenCobro"] = idOrdenCobro;
                    param["@p_IdMonedaNacional"] = CacheConfiguracion.IdMonedaNacional;

                    DataTable dtOrdenCobro = new DataTable();
                    dtOrdenCobro = _DAO.EjecutarComandoEntidad("Financiamiento", "ObtenerListaPorOrdenCobroControl", param, transaction);
                    string nroDocumento;
                    //if dtOrdenCobro != null)
                    foreach (DataRow item in dtOrdenCobro.Rows)
                    {
                        if (Convert.ToInt32(item["Control"].ToString()) == 1)
                        {

                            GeneraNroDocumentoPuntoAtencionS(ordenVisaNet.IdCentroServicio, ordenVisaNet.IdPuntoAtencion, Convert.ToInt16(item["IdTipoDocumento"]), (Int16)0, idNumOrden, Convert.ToInt32(item["IdOrdenCobroDoc"].ToString()), transaction);
                        }
                        else
                        {
                            param["@idordentrabajo"] = "";
                            param["@idcentroservicio"] = Convert.ToInt16(ordenVisaNet.IdCentroServicio);
                            param["@idtipodocumentocomercial"] = Convert.ToInt16(item["IdTipoDocumento"].ToString());

                            _DAO.EjecutarComandoEscalar("contadordocumentocomercial", "generanrodocumento", param, transaction);
                            nroDocumento = param["@idordentrabajo"].ToString();
                            if (String.IsNullOrEmpty(nroDocumento))
                                throw new Exception("<<<No existe contador comercial para el centro de servicios .>>>");

                            param["@p_nrodocumento"] = nroDocumento;
                            param["@p_NroOrden"] = idNumOrden;
                            param["@p_IdOrdenCobroDocumento"] = Convert.ToInt32(item["IdOrdenCobroDoc"].ToString());

                            _DAO.EjecutarComandoEntidad("Financiamiento", "ActualizarNroDocumento", param, transaction);
                        }

                    }

                    #endregion Generar OrdenTrabajo = NroDocumento

                    /// probar con el if de creación de tablas temporales

                    #region OrdenVisaNetDetalle

                    param["@p_idnroorden"] = idNumOrden;
                    table = _DAO.EjecutarComandoEntidad("FinanciamientoOrdenVisaNetDetalle", "Obtener", param);

                    financiamientoOrdenVisaNetDetalle = new clsListaFinanciamientoOrdenVisaNetDetalle(table);
                    financiamientoOrdenVisaNetDetalle.Elementos.ForEach(x => x.IdEmpresa = ordenVisaNet.IdEmpresa);


                    #endregion OrdenVisaNetDetalle

                    ordenVisaNet.NroTarjeta = string.IsNullOrEmpty(ordenVisaNet.NroTarjeta) ? "***" : ordenVisaNet.NroTarjeta;

                    try
                    {
                        clsListaOrdenCobroDocumento _clsListaOrdenCobroDocumento = new clsListaOrdenCobroDocumento();
                        _clsListaOrdenCobroDocumento = ObtenerListaPorOrdenCobro(idOrdenCobro, (Int16)Moneda.Soles, transaction);
                        clsListaIngresoProvision _clsListaIngresoProvision = new clsListaIngresoProvision();


                        transaccion.PeriodoPago = financiamientoOrdenVisaNetDetalle.Elementos.First().Periodo;
                        string periodo = transaccion.PeriodoPago.ToString();
                        short anioPeriodo = short.Parse(periodo.Substring(0, 4));
                        short mesPeriodo = short.Parse(periodo.Substring(periodo.Length - 2, 2));

                        #region ListaDocumentosBase
                        param.Clear();
                        param["@p_idnroorden"] = idNumOrden;
                        //Cambiar ingreprovisionDocumento por temporal.convenio y ordencobrodocumento
                        //no se obtiene idtipodocumento, numerodocumento, idservicio
                        //No se modifico el SP
                        table = _DAO.EjecutarComandoEntidad("Financiamiento", "VisaNetObtenerDocumentos", param, transaction);
                        int Periodo = 0;
                        short idServicio = 0;
                        foreach (DataRow item in table.Rows)
                        {
                            documentoPropio = new clsFinanciamientoDocumentosDescargoPropio();
                            documentoPropio.ImporteOrigenDescargo = Convert.ToDecimal(item["Importe"]);
                            documentoPropio.ImporteOrigenDeuda = Convert.ToDecimal(item["Importe"]);
                            documentoPropio.IdEmpresa = ordenVisaNet.IdEmpresa;
                            documentoPropio.IdOrdenCobro = Convert.ToInt32(item["IdOrdenCobro"]);
                            documentoPropio.NroDocumentoSustento = item["NroDocumento"].ToString();
                            documentoPropio.IdTipoDocumentoSustento = Convert.ToInt16(item["idTipoDocumento"]);
                            documentoPropio.IdTipoDocumentoSustento = Int16.Parse(item["IdTipoDocumento"].ToString());
                            documentoPropio.IdServicio = Convert.ToInt16(item["idServicio"]);
                            idServicio = documentoPropio.IdServicio;
                            documentoPropio.OrdenPagoServicio = Convert.ToInt32(item["OrdendePago"]);
                            documentoPropio.IdNroServicio = ordenVisaNet.IdNroServicio;
                            documentoPropio.Periodo = Convert.ToInt32(item["Periodo"]);
                            Periodo = documentoPropio.Periodo;
                            documentosBase.Add(documentoPropio);
                        }

                        #region provisiones
                        //clsListaOrdenCobroDocumento _clsListaOrdenCobroDocumento = new clsListaOrdenCobroDocumento();
                        _clsListaOrdenCobroDocumento = ObtenerListaPorOrdenCobro(idOrdenCobro, (Int16)Moneda.Soles, transaction);
                        #endregion

                        #region PuntoAtencion

                        usuarioPuntoAtencion.IdEmpresa = ordenVisaNet.IdEmpresa;
                        usuarioPuntoAtencion.IdUnidadNegocio = ordenVisaNet.IdUUNN;
                        usuarioPuntoAtencion.IdCentroServicio = Convert.ToInt16(ordenVisaNet.IdCentroServicio);
                        usuarioPuntoAtencion.IdTipoAtencion = ordenVisaNet.IdTipoAtencion;
                        usuarioPuntoAtencion.IdPuntoAtencion = Convert.ToInt16(ordenVisaNet.IdPuntoAtencion);
                        usuarioPuntoAtencion.IdUsuario = ordenVisaNet.IdUsuario;

                        #endregion PuntoAtencion

                        #endregion ListaDocumentosBase

                        transaccion.FechaPago = today;
                        transaccion.IdOrdenCobro = idOrdenCobro;
                        _clsListaIngresoProvision = _clsListaOrdenCobroDocumento.ObtenerListaIngresoProvision(anioPeriodo, mesPeriodo, short.Parse(ordenVisaNet.IdCentroServicio.ToString()), idServicio, usuarioPuntoAtencion.IdPuntoAtencion, usuarioPuntoAtencion.IdUsuario, transaccion.FechaPago);


                        if (TransaccionIngresoGrabar(_clsListaIngresoProvision, transaction))
                        {
                            #region TransaccionExterna

                            param["@p_idcobranzaexterna"] = 0;
                            param["@p_idnrocaja"] = 0;
                            param["@p_idempresa"] = ordenVisaNet.IdEmpresa;
                            param["@p_importetotal"] = ordenVisaNet.Importe;
                            //No es necesario crear nuevo SP
                            _DAO.EjecutarComando("TransaccionCobranzaExterna", "visanetinsertar", param, transaction);
                            idCobranzaExterna = Convert.ToInt32(param["@p_idcobranzaexterna"]);
                            param.Clear();
                            param["@p_idtransaccion"] = 0;
                            param["@p_idcobranzaexterna"] = idCobranzaExterna;
                            param["@p_idnroservicio"] = ordenVisaNet.IdNroServicio;
                            param["@p_importetotal"] = ordenVisaNet.Importe;
                            param["@p_eticket"] = ordenVisaNet.eTicket;
                            //No es necesario crear nuevo SP
                            _DAO.EjecutarComando("TransaccionExternaNroServicio", "visanetinsertar", param, transaction);
                            idTransaccion = Convert.ToInt32(param["@p_idtransaccion"]);
                            param.Clear();
                            foreach (clsFinanciamientoOrdenVisaNetDetalle item in financiamientoOrdenVisaNetDetalle.Elementos)
                            {
                                param["@p_idtransaccion"] = idTransaccion;
                                param["@p_idempresa"] = ordenVisaNet.IdEmpresa;
                                param["@p_idtipodocumento"] = item.IdTipoDocumento;
                                param["@p_nrodocumento"] = item.NroDocumento;
                                param["@p_periodopago"] = item.Periodo;
                                param["@p_saldo"] = item.Saldo;
                                transaccion.PeriodoPago = item.Periodo;
                                _DAO.EjecutarComandoEscalar("FinanciamientoTransaccionExternaDocumentos", "visanetinsertar", param, transaction);
                            }
                            param.Clear();
                            param["@p_idtransaccion"] = idTransaccion;
                            param["@p_parametroxml"] = ordenVisaNet.Trama;

                            _DAO.EjecutarComando("FinanciamientoTransaccionExternaParametros", "visanetinsertar", param, transaction);

                            #endregion TransaccionExterna


                            #region Medios

                            mediosPagoExterno.OrdenDePago = 1;
                            mediosPagoExterno.NroTarjeta = ordenVisaNet.NroTarjeta;
                            mediosPagoExterno.IdOrdenCobroDocumento = financiamientoOrdenVisaNetDetalle.Elementos.Find(x => x.IdTipoDocumento == (Int16)FinanciamientoTipoDocumentoComercial.CuotaInicialConvenio).IdOrdenCobroDocumento;
                            mediosPagoExterno.DescripcionTarjeta = ordenVisaNet.eTicket;
                            mediosPagoExterno.EnviaABoveda = false;
                            mediosPagoExterno.EsIngreso = 1;
                            mediosPagoExterno.EsEfectivo = false;
                            mediosPagoExterno.EsCheque = false;
                            mediosPagoExterno.FechaDocumento = today;
                            mediosPagoExterno.GeneraVuelto = false;
                            mediosPagoExterno.IdOrdenCobro = (Int16)FinanciamientoTipoDocumentoComercial.TarjetaCredito;
                            mediosPagoExterno.IdTipoTarjeta = (Int16)3;//Tarjeta visa// enumTipoTarjeta.Visa;
                            mediosPagoExterno.ImporteCambioTotal = Convert.ToDecimal(ordenVisaNet.Importe);
                            mediosPagoExterno.ImporteOrigenTotal = Convert.ToDecimal(ordenVisaNet.Importe);
                            mediosPagoExterno.TipoCambio = (Decimal)1;
                            mediosPagoExterno.IdMonedaOrigen = (Int16)Moneda.Soles;
                            mediosPagoExterno.IdMonedaCambio = (Int16)Moneda.Soles;
                            mediosPagoExterno.idTipoDocumento = (Int16)FinanciamientoTipoDocumentoComercial.CuotaInicialConvenio;
                            mediosPagoExterno.NroDocumento = financiamientoOrdenVisaNetDetalle.Elementos.Find(x => x.IdTipoDocumento == (Int16)FinanciamientoTipoDocumentoComercial.CuotaInicialConvenio).NroDocumento;

                            #endregion Medios

                            #region CajaMoneda

                            cajaMoneda.IdMoneda = (Int16)Moneda.Soles;
                            cajaMoneda.IdMonedaCambio = (Int16)Moneda.Soles;
                            cajaMoneda.IdNroCaja = 0;
                            cajaMoneda.IdTipoDocumentoComercial = (Int16)TipoDocumentoComercial.TarjetaCredito;
                            cajaMoneda.Importe = Convert.ToDecimal(ordenVisaNet.Importe);
                            cajaMoneda.ImporteBovedaCambio = Convert.ToDecimal(ordenVisaNet.Importe);
                            cajaMoneda.ImporteBoveda = Convert.ToDecimal(ordenVisaNet.Importe);
                            cajaMoneda.ImporteDescargo = Convert.ToDecimal(ordenVisaNet.Importe);
                            cajaMoneda.NroTransaccion = 0;
                            cajaMoneda.Seleccionar = true;
                            cajaMoneda.TipoCambio = (Decimal)1;

                            #endregion CajaMoneda

                            //Generar ListaIngresoProvision y guardar en transaccion 
                            //dis-negocio cliente
                            //Windows.Cobranza.frmC13ConsultaOrdenPago.cs (Front Escritorio)
                            //Linea 1572





                            #region ListaIngresoProvision
                            // PREGUNTAR SI LA FECHA PAGO ==> INGRESA A LAS TABLAS TEMPORALES COMO FECHA DE EMISIÓN

                            //clsListaIngresoProvision _clsListaIngresoProvision = new clsListaIngresoProvision();
                            _clsListaIngresoProvision = _clsListaOrdenCobroDocumento.ObtenerListaIngresoProvision(anioPeriodo, mesPeriodo, short.Parse(ordenVisaNet.IdCentroServicio.ToString()), idServicio, usuarioPuntoAtencion.IdPuntoAtencion, usuarioPuntoAtencion.IdUsuario, transaccion.FechaPago);


                            /// ARMADO TEMPORAL VER SI SE TIENE QUE ELIMINAR
                            clsListaFinanciamientoIngresoProvision _objlistaingresoprovision = armarDeclsListaIngresoProvision(_clsListaIngresoProvision);
                            //_objlistaingresoprovision = ObtenerListaIngresoProvision(ordenVisaNet.IdNroServicio, ordenVisaNet.IdEmpresa);


                            #endregion ListaIngresoProvision

                            #region Transaccion
                            transaccion.ListaIngresoProvision = _objlistaingresoprovision;
                            transaccion.ListaFinanciamientoIngresoProvision = _clsListaIngresoProvision;
                            transaccion.FechaPago = today;
                            transaccion.FechaRegistro = today;
                            transaccion.IdMovComercial = (Int16)enumMovimientoComercial.Convenios;
                            transaccion.NroTransaccion = 0;
                            transaccion.IdMonedaNacional = (Int16)Moneda.Soles;
                            transaccion.ImporteNeto = Convert.ToDecimal(ordenVisaNet.Importe);
                            transaccion.TipoOperacion = FinanciamientoTipoOperacionCobranza.Recaudacion;
                            transaccion.IdEstado = (Int16)Estado.Activo;
                            transaccion.IdNroServicio = ordenVisaNet.IdNroServicio;
                            transaccion.ImporteEfectivo = 0;
                            transaccion.ImporteTotalVuelto = 0;
                            transaccion.ImporteNeto = Convert.ToDecimal(ordenVisaNet.Importe);
                            transaccion.ImporteRecibido = Convert.ToDecimal(ordenVisaNet.Importe);
                            transaccion.ImporteValores = Convert.ToDecimal(ordenVisaNet.Importe);
                            transaccion.UsuarioPuntoAtencion = usuarioPuntoAtencion;
                            transaccion.ElementosDocumentoDescargo = documentosBase;
                            transaccion.ElementosMedioPago = new List<clsFinanciamientoMediosPagoBase>();
                            transaccion.ElementosMedioPago.Add(mediosPagoExterno);
                            transaccion.ElementosCajaServicioMoneda = new List<clsFinanciamientoCajaEstadisticaDiarioMoneda>();
                            transaccion.ElementosCajaServicioMoneda.Add(cajaMoneda);
                            ////////////////////////////////////////////////////
                            transaccion.PeriodoPago = Periodo;//202011;


                            #endregion Transaccion

                            #region Result

                            result["NroPedido"] = idNumOrden;
                            result["IdCobranzaExterna"] = idCobranzaExterna;
                            result["NroTransaccion"] = idTransaccion;
                            result["NroTarjeta"] = ordenVisaNet.NroTarjeta;
                            result["MensajeExcepcion"] = "";

                            #endregion Result
                            #region Tablas temporales


                            //if (TransaccionIngresoGrabar(_clsListaIngresoProvision, transaction))
                            //{
                            result["NroTransaccion"] = GrabarCobranzaMejoradaSedalib(transaccion, transaction, CacheConfiguracion.IdOperacionCobranza);

                            #region NG Escritorio

                            #region Actualizar Orden de Cobro

                            ActualizarOrdenDeCobro(transaction
                                                    , financiamientoOrdenVisaNetDetalle.Elementos[0].IdOrdenCobro
                                                    , transaccion.UsuarioPuntoAtencion.IdUsuario
                                                    , transaccion.FechaRegistro
                                                    , (Int16)Estado.Inactivo);

                            for (Int32 i = 0; i < financiamientoOrdenVisaNetDetalle.Elementos.Count; i++)
                            {
                                if (financiamientoOrdenVisaNetDetalle.Elementos[i].IdTipoDocumento != (Int16)TipoDocumentoComercial.ReciboEnergia)
                                {
                                    ActualizarOdenCobroDocumento(transaction
                                                                                      , Convert.ToInt32(financiamientoOrdenVisaNetDetalle.Elementos[i].IdOrdenCobroDocumento)
                                                                                      , financiamientoOrdenVisaNetDetalle.Elementos[i].IdTipoDocumento
                                                                                      , (Int16)financiamientoOrdenVisaNetDetalle.Elementos[i].IdEmpresa
                                                                                      , financiamientoOrdenVisaNetDetalle.Elementos[i].NroDocumento);
                                }
                            }

                            #endregion Actualizar Orden de Cobro

                            #region Agregado por Carlos López. 19/08/2009

                            DataRow drfila = null;

                            // EGM. 05/02/2010. IF agregado para que no entre si es una Facilidad de Pago
                            // donde tambien se generar Convenis Temporales.
                            if (transaccion.IdMovComercial != (Int16)enumMovimientoComercial.Convenios)
                            {
                                // ------------------------------------------------------------------------------------------------
                                // Agregado por Carlos López. --> 19/08/2009
                                // Esto es para que se genere el convenio en la tabla física y si esta orden de cobro es por
                                // solcitud de Servicio se grabe en la tablas de SOL.SolicitudServicio los datos del documento
                                // generado.
                                Int16 _intidempresa = 0;
                                Int16 _intidtipodocumento = 0;
                                String _strnrodocumento = "";
                                Int32 _intidnroservicio = 0;

                                //DESCOMENTAR ESTE BLOQUE

                                for (Int32 i = 0; i < transaccion.ListaFinanciamientoIngresoProvision.Elementos.Count; i++)
                                {

                                    if (transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdTipoDocumento == (Int16)enumDocumentoComercial.BoletadeVenta ||
                                        transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdTipoDocumento == (Int16)enumDocumentoComercial.Factura)
                                    {
                                        _intidempresa = transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdEmpresa;
                                        _intidtipodocumento = transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdTipoDocumento;
                                        _strnrodocumento = transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].NroDocumento;
                                        _intidnroservicio = transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdNroServicioPrincipal;
                                    }
                                }

                                // agregar parametros de idtipoatencion / idpunto atención
                                drfila = GrabarNuevoObtenerNroConvenio(transaction
                                                                    , Convert.ToInt32(transaccion.IdOrdenCobro)
                                                                    , _intidempresa
                                                                    , _intidtipodocumento
                                                                    , _strnrodocumento, transaccion.PeriodoPago, transaccion.FechaPago, usuarioPuntoAtencion);

                                if (transaccion.IdSolicitud != null && transaccion.IdSolicitud > 0)
                                {
                                    Int32 _intidsolicitud = Convert.ToInt32(transaccion.IdSolicitud);

                                    DateTime _datfecharegistro = transaccion.FechaRegistro;

                                    //Actualizar Estado de la Solicitud y B-F asociado.
                                    ActualizarDatosCobranzaServicio(_intidsolicitud
                                                                        , _intidempresa
                                                                        , _intidtipodocumento
                                                                        , _strnrodocumento
                                                                        , _datfecharegistro
                                                                        , transaction);

                                    //Actualizar el Nro. del Contrato.
                                    ActualizarNroContratoenContrato(transaction, _intidsolicitud
                                                                              , _intidnroservicio);
                                }

                                // EGM. 05/05/2010. Para que registre Documentos a Favor, en caso la O/C tuviera.
                                RegistrarDocFavor(transaction
                                                        , Convert.ToInt32(transaccion.IdOrdenCobro)
                                                        , transaccion.NroTransaccion
                                                        , transaccion.UsuarioPuntoAtencion.IdTipoAtencion
                                                        , transaccion.UsuarioPuntoAtencion.IdPuntoAtencion
                                                        , transaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);

                                // ------------------------------------------------------------------------------------------------
                            }

                            #endregion Agregado por Carlos López. 19/08/2009

                            //Generar ListaIngresoProvision y guardar en transaccion 
                            //dis-negocio cliente
                            //Windows.Cobranza.frmC13ConsultaOrdenPago.cs (Front Escritorio)
                            //Linea 1468
                            //_lstordencobrodoc = clsProxyServicioOrdenCobro.Instancia.OrdenCobroDocumentoObtenerListaPorOrden(ordencobro, CacheConfiguracion.IdMonedaNacional);

                            //linea 1468 obtiene la lista de ordenes de cobro por número de ordern datatable ==> dtOrdenCobro


                            //DESCOMENTAR ESTE BLOQUE
                            #region CuotaInicialConvenio
                            param.Clear();
                            param["@p_IdOrdenCobro"] = idOrdenCobro;
                            param["@p_IdMonedaNacional"] = CacheConfiguracion.IdMonedaNacional;
                            dtOrdenCobro = _DAO.EjecutarComandoEntidad("Financiamiento", "ObtenerListaPorOrdenCobroControl", param, transaction);
                            clsListaOrdenCobroDocumento listadocumentosordencobro = new clsListaOrdenCobroDocumento(dtOrdenCobro);


                            // EGM. 25/01/2010
                            // Copia de las tablas temporales de convenio a las tablas fisicas.
                            String nroconvenio = "";
                            if (transaccion.IdMovComercial == (Int16)enumMovimientoComercial.Convenios)
                            {
                                nroconvenio = GrabarDefinitivo(transaction
                                                                                        , Convert.ToInt32(transaccion.IdOrdenCobro)
                                                                                        , transaccion.PeriodoPago
                                                                                        , transaccion.FechaRegistro, transaccion, metodoPago, idNumOrden);
                            }


                            //////// NO SE ENCUENTRA EL NRO DE CONVENIO
                            // Asigna el Nro de Convenio a la Lista de Orden de Cobro.
                            foreach (clsOrdenCobroDocumento ordendoc in listadocumentosordencobro.Elementos)
                            {
                                ordendoc.IdNroMovimientoComercial = nroconvenio;
                            }

                            // Para poder obtener el tipo y numero de documento bajo el cual se han facturado los intereses por deuda.
                            Int16 _inttipodocu = 0;
                            String _strnumedocu = "";

                            /// seteamos a financiados en la tabla ingresoprovision e ingresoprovisiondocumentos
                            /// los documentos de la ordencobrodocumento que fueron generados como no cobrables .
                            foreach (clsOrdenCobroDocumento _item in listadocumentosordencobro.Elementos)
                            {
                                if (_item.EsCobrable == 0)
                                {
                                    ActualizarEstadoFinanciadoIngresoProvision
                                                                        (transaction
                                                                        , _item.IdEmpresa.Value
                                                                        , _item.IdTipoDocumento
                                                                        , _item.NroDocumento);

                                    ActualizarEstadoPagado(transaction
                                                                                            , _item.IdEmpresa.Value
                                                                                            , _item.IdTipoDocumento
                                                                                            , _item.NroDocumento
                                                                                            , transaccion.PeriodoPago
                                                                                            , transaccion.FechaPago);

                                    RegistrarConvenioDocumentoFinanciado
                                                                        (transaction
                                                                        , listadocumentosordencobro.Elementos[0].IdNroMovimientoComercial.Trim()
                                                                        , _item.IdEmpresa.Value
                                                                        , _item.IdTipoDocumento
                                                                        , _item.NroDocumento);

                                    // Obtener el tipo y numero de documento bajo el cual se han facturado los intereses por deuda.
                                    if (_item.Descripcion == "Facturación de intereses por deuda para el convenio de facilidades de pago Nº " +
                                        listadocumentosordencobro.Elementos[0].IdNroMovimientoComercial.Trim())
                                    {
                                        _inttipodocu = _item.IdTipoDocumento;
                                        _strnumedocu = _item.NroDocumento;
                                    }
                                }
                                //} end iffff

                                /*INICIO SECTOR COMENTADO EN EL NG NEGOCIO
                                // actualizamos el estado del convenio a habilitado.
                                //this.ProxyConvenio.ConvenioActualizarEstadoHabilitado(listadocumentosordencobro.Elementos[0].IdNroMovimientoComercial.ToString().Trim());
                                //clsConvenioDAO.Instancia.ActualizarEstadoConvenioHabilitado(_objtransaccion
                                //                                                           , listadocumentosordencobro.Elementos[0].IdNroMovimientoComercial.ToString().Trim());
                                FIN SECTOR COMENTADO EN EL NG NEGOCIO */
                                // Actualizar el esatdo a APLICADO y tipo/numero de documento bajo el cual se han facturado los intereses.

                                ////////////////
                                if (_inttipodocu > 0 && _strnumedocu.Length > 0)
                                    ConvenioActualizarAplicadoInteresesDeudaFinanciar(transaction
                                                                                        , listadocumentosordencobro.Elementos[0].IdNroServicio
                                                                                        , _inttipodocu
                                                                                        , _strnumedocu);


                                #endregion CuotaInicialConvenio



                                #endregion NG Escritorio

                            }

                            #endregion


                        }

                        //Si el metodo devuelve falso
                        //hacer rollback



                        //TERMINA EL PROCESO Y GENERA LOS DOCUMENTOS
                        // llamar al metodo GenerarPdfXML
                        #region Generación de PDF y XML
                        /*
                        SE COMENTA POR PRRBLEMAS CON EL CERTIFICADO PARA FIRMAR EL XML PARA EL ENVIO A SUNAT
                        SE COMENTA POR PRRBLEMAS CON EL CERTIFICADO PARA FIRMAR EL XML PARA EL ENVIO A SUNAT
                        SE COMENTA POR PRRBLEMAS CON EL CERTIFICADO PARA FIRMAR EL XML PARA EL ENVIO A SUNAT
                        SE COMENTA POR PRRBLEMAS CON EL CERTIFICADO PARA FIRMAR EL XML PARA EL ENVIO A SUNAT
                        SE COMENTA POR PRRBLEMAS CON EL CERTIFICADO PARA FIRMAR EL XML PARA EL ENVIO A SUNAT
                         */
                        //GenerarPdfXML(transaccion);
                        #endregion Generación de PDF y XML

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    osqlcon.Close();
                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    if (osqlcon.State == ConnectionState.Open)
                        osqlcon.Close();
                    throw ex;
                }
            }


            private void GenerarPdfXML(clsFinanciamientoTransaccion transaccion)
            {
                Int16 _intidempresass = 0;
                Int16 _intidtipodocumentoss = 0;
                String _strnrodocumentoss = "";
                Boolean bolProcesar = false;
                //ListaFinanciamientoIngresoProvision
                for (Int32 i = 0; i < transaccion.ListaFinanciamientoIngresoProvision.Elementos.Count; i++)
                {
                    if (transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdTipoDocumento == CacheConfiguracion.IdDC_BoletaVenta ||
                        transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdTipoDocumento == CacheConfiguracion.IdDC_Factura)
                    {
                        _intidempresass = transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdEmpresa;
                        _intidtipodocumentoss = transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].IdTipoDocumento;
                        _strnrodocumentoss = transaccion.ListaFinanciamientoIngresoProvision.Elementos[i].NroDocumento;
                        bolProcesar = true;
                    }
                }

                ////////////////////////////////
                if (bolProcesar)
                {
                    clsFacturacionAdministrativaTransferirSAP oParametroTemp = new clsFacturacionAdministrativaTransferirSAP();
                    oParametroTemp.IdEmpresa = _intidempresass;
                    oParametroTemp.IdTipoDocumento = _intidtipodocumentoss;
                    oParametroTemp.NroDocumento = _strnrodocumentoss;

                    clsFacturacionAdministrativaRespuesta oResultadoTemporal = GenerarXMLPDFEnvioSunat(oParametroTemp);

                    clsIngresoProvisionComplemento oParametroComplemento = new clsIngresoProvisionComplemento();
                    oParametroComplemento.IdEmpresa = oParametroTemp.IdEmpresa;
                    oParametroComplemento.IdTipoDocumento = oParametroTemp.IdTipoDocumento;
                    oParametroComplemento.NroDocumento = oParametroTemp.NroDocumento;
                    oParametroComplemento.RutaArchivoXML = oResultadoTemporal.RutaXML;
                    oParametroComplemento.RutaArchivoPDF = oResultadoTemporal.RutaPDF;
                    oParametroComplemento.RutaRespuestaSUNAT = oResultadoTemporal.RutaRespuestaSUNAT;
                    oParametroComplemento.CodigoRespuestaSUNAT = oResultadoTemporal.CodigoSUNAT;
                    oParametroComplemento.MensajeRespuestaSUNAT = oResultadoTemporal.MensajeSUNAT;

                    IngresoProvisionComplementoActualizarRutas(oParametroComplemento);
                    //llamar a la función  ==> GenerarXmlEnvioSunar
                }
            }

            private bool IngresoProvisionComplementoActualizarRutas(clsIngresoProvisionComplemento oParametro)
            {
                bool bolResultado = false;
                Hashtable htParametro = new Hashtable();
                try
                {
                    htParametro.Clear();
                    htParametro.Add("@p_idempresa", oParametro.IdEmpresa);
                    htParametro.Add("@p_idtipodocumento", oParametro.IdTipoDocumento);
                    htParametro.Add("@p_nrodocumento", oParametro.NroDocumento);
                    htParametro.Add("@p_rutaarchivoxml", oParametro.RutaArchivoXML);
                    htParametro.Add("@p_rutaarchivopdf", oParametro.RutaArchivoPDF);
                    htParametro.Add("@p_rutarespuestasunat", oParametro.RutaRespuestaSUNAT);
                    htParametro.Add("@p_codigorespuestasunat", oParametro.CodigoRespuestaSUNAT);
                    htParametro.Add("@p_mensajerespuestasunat", oParametro.MensajeRespuestaSUNAT);
                    htParametro.Add("@p_ticket", oParametro.Ticket);

                    _DAO.EjecutarComando("ingresoprovisioncomplemento", "actualizarrutasarchivos", htParametro);
                    bolResultado = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
                return bolResultado;
            }

            #region Generar XML
            public clsFacturacionAdministrativaRespuesta GenerarXMLPDFEnvioSunat(clsFacturacionAdministrativaTransferirSAP oParametro)
            {
                clsFacturacionAdministrativaRespuesta oResultado = new clsFacturacionAdministrativaRespuesta();

                clsFacturacionResultadoXMLPDF oResultadoXML = ComprobanteGenerarXML(oParametro);

                oResultado.SeGeneroXML = oResultadoXML.GeneroXMLPDF;
                oResultado.RutaXML = oResultadoXML.RutaXMLPDF;


                #region Generar PDF
                //descomentar
                oParametro.RutaArchivo = oResultado.RutaXML;


                #region Obtener Datos
                SISAC.Entidad.Atencion.clsEmpresa oEmpresa = ObtenerRegistroEmpresa(oParametro.IdEmpresa);
                clsComprobantePago oComprobante = ObtenerInformacionDocumentoIngresoProvision(oParametro.IdEmpresa, oParametro.IdTipoDocumento, oParametro.NroDocumento);
                #endregion

                clsFacturacionResultadoXMLPDF oResultadoPDF = clsUtilFinanciamiento.ComprobanteGenerarPDF(oParametro, oEmpresa, oComprobante);

                oResultado.SeGeneroPDF = oResultadoPDF.GeneroXMLPDF;
                oResultado.RutaPDF = oResultadoPDF.RutaXMLPDF;

                #endregion

                #region Enviar SUNAT
                // COMENTADO EN LA APP DE ESCRITORIO
                //////if (oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.Factura ||
                //////    oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.BoletaVenta ||
                //////    oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaCredito ||
                //////    oParametro.IdTipoDocumento == (Int16)TipoDocumentoComercial.NotaDebito)
                //////{
                //////    clsEmpresa oEmpresa = clsEmpresaDAO.Instancia.ObtenerRegistroEmpresa(oParametro.IdEmpresa);
                //////    Object UserName = clsEmpresaDAO.Instancia.ObtenerDatosComplemento(oParametro.IdEmpresa, 2623);
                //////    Object Password = clsEmpresaDAO.Instancia.ObtenerDatosComplemento(oParametro.IdEmpresa, 2624);

                //////    clsEnvio oParametroTemp = new clsEnvio();
                //////    oParametroTemp.IdEmpresa = oParametro.IdEmpresa;
                //////    oParametroTemp.Tramaxml = oResultado.RutaXML;
                //////    oParametroTemp.UserName = UserName.ToString();
                //////    oParametroTemp.Password = Password.ToString();
                //////    oParametroTemp.RUC = oEmpresa.Ruc;

                //////    clsRespuesta oResultadoTemp = clsGestionarServicioSunat.Instancia.EnviarDocumento(oParametroTemp);

                //////    if (oResultadoTemp != null)
                //////    {
                //////        oResultado.SeEnvioSUNAT = true;
                //////        oResultado.MensajeSUNAT = oResultadoTemp.Respuesta;
                //////        oResultado.RutaRespuestaSUNAT = oResultadoTemp.RutaRespuesta;
                //////        oResultado.CodigoSUNAT = oResultadoTemp.Codigo;
                //////    }
                //////}
                //////#endregion

                return oResultado;
            }

            public clsFacturacionResultadoXMLPDF ComprobanteGenerarXML(clsFacturacionAdministrativaTransferirSAP oParametro)
            {
                clsFacturacionResultadoXMLPDF oResultado = new clsFacturacionResultadoXMLPDF();

                Boolean bolResultado = false;
                //clsGestionDato oGestorDAO = clsGestionDato.ObtenerInstancia();
                Hashtable htParametro = new Hashtable();
                DataSet oResultados = null;
                String strRutaXML = String.Empty;

                try
                {
                    #region Parametros
                    htParametro.Add("@p_idempresa", oParametro.IdEmpresa);
                    htParametro.Add("@p_idtipodocumento", oParametro.IdTipoDocumento);
                    htParametro.Add("@p_nrodocumento", oParametro.NroDocumento);
                    #endregion

                    #region Generar XML
                    if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.Factura ||
                        oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.BoletadeVenta)
                    {
                        if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.Factura)
                            oResultados = _DAO.EjecutarComandoConjuntoEntidad("ComprobanteFactura", "GenerarDataXML", htParametro, "Generico", "Leyendas", "Items");
                        else if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.BoletadeVenta)
                            oResultados = _DAO.EjecutarComandoConjuntoEntidad("ComprobanteBoleta", "GenerarDataXML", htParametro, "Generico", "Leyendas", "Items");

                        //string[] tables = { "Cabecera", "Detalle", "Convenio" };
                        //var result = _DAO.EjecutarComandoConjuntoEntidad("dbo.IngresoProvision", "ObtenerPorNroDocumento", param, tables);
                        if (oResultados != null && oResultados.Tables.Count > 0)
                        {
                            #region Definicion de Tablas que viene de OptimusNGC
                            DataTable oTablaGenerico = oResultados.Tables["Generico"];
                            DataTable oTablaLeyendas = oResultados.Tables["Leyendas"];
                            DataTable oTablaItems = oResultados.Tables["Items"];
                            #endregion

                            #region Generar Comprobante Electrinico
                            DocumentoElectronico oDocumentoElectronico = new DocumentoElectronico();

                            #region Almacenar Datos del Contribuyente Emisor
                            oDocumentoElectronico.Emisor = new Contribuyente();
                            oDocumentoElectronico.Emisor.Departamento = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionDepartamento"]).ToString();
                            oDocumentoElectronico.Emisor.Direccion = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccion"]).ToString();
                            oDocumentoElectronico.Emisor.Distrito = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionDistrito"]).ToString();
                            oDocumentoElectronico.Emisor.NombreComercial = (oTablaGenerico.Rows[0]["EmpresaEmiteNombreComercial"]).ToString();
                            oDocumentoElectronico.Emisor.NombreLegal = (oTablaGenerico.Rows[0]["EmpresaEmiteNombreRazonSocial"]).ToString();
                            oDocumentoElectronico.Emisor.NroDocumento = (oTablaGenerico.Rows[0]["EmpresaEmiteNroDocumento"]).ToString();
                            oDocumentoElectronico.Emisor.Provincia = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionProvincia"]).ToString();
                            oDocumentoElectronico.Emisor.TipoDocumento = (oTablaGenerico.Rows[0]["EmpresaEmiteTipoDocumento"]).ToString();
                            oDocumentoElectronico.Emisor.Ubigeo = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionUBIGEO"]).ToString();
                            oDocumentoElectronico.Emisor.Urbanizacion = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionUrganizacion"]).ToString();

                            oDocumentoElectronico.Emisor.Serie = (oTablaGenerico.Rows[0]["Serie"]).ToString();
                            oDocumentoElectronico.Emisor.Correlativo = (oTablaGenerico.Rows[0]["Correlativo"]).ToString();
                            #endregion

                            #region Almacenar Datos del Contribuyente Receptor
                            oDocumentoElectronico.Receptor = new Contribuyente();
                            oDocumentoElectronico.Receptor.TipoDocumento = (oTablaGenerico.Rows[0]["ClienteTipoDocumentoIdentidad"]).ToString();
                            oDocumentoElectronico.Receptor.NroDocumento = (oTablaGenerico.Rows[0]["ClienteNroDocumentoIdentidad"]).ToString();
                            oDocumentoElectronico.Receptor.NombreComercial = (oTablaGenerico.Rows[0]["ClienteNombre"]).ToString();
                            oDocumentoElectronico.Receptor.NombreLegal = (oTablaGenerico.Rows[0]["ClienteNombre"]).ToString();
                            oDocumentoElectronico.Receptor.Direccion = (oTablaGenerico.Rows[0]["ClienteDireccion"]).ToString();
                            #endregion

                            #region Almacenar Datos Generales del Comprobante
                            oDocumentoElectronico.IdDocumento = (oTablaGenerico.Rows[0]["Serie"]).ToString() + "-" + (oTablaGenerico.Rows[0]["Correlativo"]).ToString();  // (oTablaGenerico.Rows[0]["ComprobanteNumero"]).ToString();
                            oDocumentoElectronico.TipoDocumento = (oTablaGenerico.Rows[0]["ComprobanteTipoDocumento"]).ToString();
                            oDocumentoElectronico.FechaEmision = (oTablaGenerico.Rows[0]["FechaEmision"]).ToString();
                            oDocumentoElectronico.Moneda = (oTablaGenerico.Rows[0]["ComprobanteTipoMoneda"]).ToString();
                            oDocumentoElectronico.MontoEnLetras = (oTablaLeyendas.Rows[0]["DescripcionLeyenda"]).ToString();
                            oDocumentoElectronico.CalculoIgv = 0.18m;
                            oDocumentoElectronico.CalculoIsc = 0.10m;
                            oDocumentoElectronico.CalculoDetraccion = 0.04m;

                            oDocumentoElectronico.TotalIgv = Convert.ToDecimal(oTablaGenerico.Rows[0]["SumatoriaIGV"]);
                            oDocumentoElectronico.TotalIsc = Convert.ToDecimal(oTablaGenerico.Rows[0]["SumatoriaISC"]);
                            oDocumentoElectronico.TotalOtrosTributos = Convert.ToDecimal(oTablaGenerico.Rows[0]["SumatoriaOtrosTributos"]);
                            oDocumentoElectronico.TotalVenta = Convert.ToDecimal(oTablaGenerico.Rows[0]["ImporteTotal"]);

                            oDocumentoElectronico.Gravadas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperGravadas"]);
                            oDocumentoElectronico.Inafectas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperInafectas"]);
                            oDocumentoElectronico.Exoneradas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperExoneradas"]);
                            oDocumentoElectronico.Gratuitas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperGratuitas"]);
                            #endregion

                            #region Descuento
                            oDocumentoElectronico.DescuentoGlobal = Convert.ToDecimal(oTablaGenerico.Rows[0]["DescuentoGlobal"]);
                            #endregion

                            #region Almacenar Datos Por Item
                            oDocumentoElectronico.Items = new List<DetalleDocumento>();

                            foreach (DataRow oFila in oTablaItems.Rows)
                            {
                                DetalleDocumento oDocumento = new DetalleDocumento();

                                oDocumento.Cantidad = Convert.ToDecimal(oFila["Cantidad"]);
                                oDocumento.CodigoItem = oFila["CodigoProducto"].ToString();
                                oDocumento.Descripcion = oFila["Descripcion"].ToString();
                                oDocumento.Id = Convert.ToInt16(oFila["NroItem"]);
                                oDocumento.Impuesto = Convert.ToDecimal(oFila["ImporteIGV"]);
                                oDocumento.ImpuestoSelectivo = Convert.ToDecimal(oFila["MontoISC"]);
                                oDocumento.OtroImpuesto = Decimal.Zero;
                                oDocumento.PrecioReferencial = Convert.ToDecimal(oFila["ValorReferencial"]);
                                oDocumento.PrecioUnitario = Convert.ToDecimal(oFila["ValorUnitario"]);
                                oDocumento.Suma = Convert.ToDecimal(oFila["ValorVenta"]);
                                oDocumento.TipoImpuesto = oFila["AfectacionIGV"].ToString();
                                oDocumento.TipoPrecio = oFila["PrecioVentaUnitarioTipo"].ToString();
                                oDocumento.TotalVenta = Convert.ToDecimal(oFila["ValorVenta"]);
                                oDocumento.UnidadMedida = oFila["UnidadMedida"].ToString();

                                oDocumentoElectronico.Items.Add(oDocumento);
                            }
                            #endregion

                            #region Definir Parametros
                            String strNombreArchivo = String.Empty;
                            String strRutaPFX = String.Empty;
                            String strClavePFX = String.Empty;

                            if (oParametro.IdEmpresa == 1)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLENOSA.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalENOSA.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalENOSA.ToString();
                            }
                            else if (oParametro.IdEmpresa == 2)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLENSA.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalENSA.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalENSA.ToString();
                            }
                            else if (oParametro.IdEmpresa == 3)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLHDNA.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalHDNA.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalHDNA.ToString();
                            }
                            else if (oParametro.IdEmpresa == 4)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLELCTRO.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalELCTRO.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalELCTRO.ToString();
                            }
                            else if (oParametro.IdEmpresa == 5)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLELOR.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalELOR.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalELOR.ToString();
                            }

                            strRutaXML = strRutaXML + oDocumentoElectronico.FechaEmision;

                            if (!Directory.Exists(strRutaXML))
                                Directory.CreateDirectory(strRutaXML);

                            if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.Factura)
                                strNombreArchivo = oDocumentoElectronico.Emisor.NroDocumento
                                                + "-"
                                                + "01"
                                                + "-"
                                                + oDocumentoElectronico.Emisor.Serie
                                                + "-"
                                                + oDocumentoElectronico.Emisor.Correlativo
                                                + ".xml";
                            else if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.BoletadeVenta)
                                strNombreArchivo = oDocumentoElectronico.Emisor.NroDocumento
                                                + "-"
                                                + "03"
                                                + "-"
                                                + oDocumentoElectronico.Emisor.Serie
                                                + "-"
                                                + oDocumentoElectronico.Emisor.Correlativo
                                                + ".xml";
                            #endregion

                            #region Generar XML
                            String strSignature = "Signature" + (
                                                                    oParametro.IdEmpresa == 1 ? "ENOSA"
                                                                        : oParametro.IdEmpresa == 2 ? "ENSA"
                                                                        : oParametro.IdEmpresa == 3 ? "HDNA"
                                                                        : oParametro.IdEmpresa == 4 ? "ELCTRO"
                                                                        : oParametro.IdEmpresa == 5 ? "ELOR"
                                                                        : "DISTRI"
                                                                );

                            Invoice oTramaClase = Generador.GenerarInvoice(oDocumentoElectronico);

                            String strCertificadoBase64 = Convert.ToBase64String(File.ReadAllBytes(strRutaPFX));

                            var serializador = new Serializador();
                            serializador.RutaCertificadoDigital = strCertificadoBase64;
                            serializador.PasswordCertificado = strClavePFX;

                            String strXMLBase64 = serializador.GenerarXml(oTramaClase);
                            String strXMLFirmaBase64 = serializador.FirmarXml(strXMLBase64, strSignature);

                            strRutaXML = strRutaXML + "\\" + strNombreArchivo;


                            File.WriteAllBytes(strRutaXML, Convert.FromBase64String(strXMLFirmaBase64));
                            #endregion

                            #region Validación de que el archivo ha sido Creado
                            bolResultado = File.Exists(strRutaXML);
                            #endregion
                            #endregion
                        }
                    }
                    else if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeCredito ||
                        oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeDebito)
                    {
                        if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeCredito)
                            oResultados = _DAO.EjecutarComandoConjuntoEntidad("ComprobanteNotaCredito", "GenerarDataXML", htParametro, "ComprobanteFactura", "Generico", "Items");
                        else if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeDebito)
                            oResultados = _DAO.EjecutarComandoConjuntoEntidad("ComprobanteNotaDebito", "GenerarDataXML", htParametro, "ComprobanteFactura", "Generico", "Items");

                        if (oResultados != null && oResultados.Tables.Count > 0)
                        {
                            #region Definicion de Tablas que viene de OptimusNGC
                            DataTable oTablaGenerico = oResultados.Tables["Generico"];
                            DataTable oTablaItems = oResultados.Tables["Items"];
                            #endregion

                            #region Generar Comprobante Electrinico
                            DocumentoElectronico oDocumentoElectronico = new DocumentoElectronico();

                            #region Almacenar Datos del Contribuyente Emisor
                            oDocumentoElectronico.Emisor = new Contribuyente();
                            oDocumentoElectronico.Emisor.Departamento = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionDepartamento"]).ToString();
                            oDocumentoElectronico.Emisor.Direccion = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccion"]).ToString();
                            oDocumentoElectronico.Emisor.Distrito = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionDistrito"]).ToString();
                            oDocumentoElectronico.Emisor.NombreComercial = (oTablaGenerico.Rows[0]["EmpresaEmiteNombreComercial"]).ToString();
                            oDocumentoElectronico.Emisor.NombreLegal = (oTablaGenerico.Rows[0]["EmpresaEmiteNombreRazonSocial"]).ToString();
                            oDocumentoElectronico.Emisor.NroDocumento = (oTablaGenerico.Rows[0]["EmpresaEmiteNroDocumento"]).ToString();
                            oDocumentoElectronico.Emisor.Provincia = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionProvincia"]).ToString();
                            oDocumentoElectronico.Emisor.TipoDocumento = (oTablaGenerico.Rows[0]["EmpresaEmiteTipoDocumento"]).ToString();
                            oDocumentoElectronico.Emisor.Ubigeo = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionUBIGEO"]).ToString();
                            oDocumentoElectronico.Emisor.Urbanizacion = (oTablaGenerico.Rows[0]["EmpresaEmiteDireccionUrganizacion"]).ToString();

                            oDocumentoElectronico.Emisor.Serie = (oTablaGenerico.Rows[0]["Serie"]).ToString();
                            oDocumentoElectronico.Emisor.Correlativo = (oTablaGenerico.Rows[0]["Correlativo"]).ToString();
                            #endregion

                            #region Almacenar Datos del Contribuyente Receptor
                            oDocumentoElectronico.Receptor = new Contribuyente();
                            oDocumentoElectronico.Receptor.TipoDocumento = (oTablaGenerico.Rows[0]["ClienteTipoDocumentoIdentidad"]).ToString();
                            oDocumentoElectronico.Receptor.NroDocumento = (oTablaGenerico.Rows[0]["ClienteNroDocumentoIdentidad"]).ToString();
                            oDocumentoElectronico.Receptor.NombreComercial = (oTablaGenerico.Rows[0]["ClienteNombre"]).ToString();
                            oDocumentoElectronico.Receptor.NombreLegal = (oTablaGenerico.Rows[0]["ClienteNombre"]).ToString();
                            oDocumentoElectronico.Receptor.Direccion = (oTablaGenerico.Rows[0]["ClienteDireccion"]).ToString();
                            #endregion

                            #region Almacenar Datos Generales del Comprobante
                            oDocumentoElectronico.IdDocumento = (oTablaGenerico.Rows[0]["Serie"]).ToString() + "-" + (oTablaGenerico.Rows[0]["Correlativo"]).ToString();
                            oDocumentoElectronico.TipoDocumento = (oTablaGenerico.Rows[0]["ComprobanteTipoDocumento"]).ToString();
                            oDocumentoElectronico.FechaEmision = (oTablaGenerico.Rows[0]["FechaEmision"]).ToString();
                            oDocumentoElectronico.Moneda = (oTablaGenerico.Rows[0]["ComprobanteTipoMoneda"]).ToString();
                            //oDocumentoElectronico.MontoEnLetras = (oTablaLeyendas.Rows[0]["DescripcionLeyenda"]).ToString();
                            oDocumentoElectronico.CalculoIgv = 0.18m;
                            oDocumentoElectronico.CalculoIsc = 0.10m;
                            oDocumentoElectronico.CalculoDetraccion = 0.04m;

                            oDocumentoElectronico.TotalIgv = Convert.ToDecimal(oTablaGenerico.Rows[0]["SumatoriaIGV"]);
                            oDocumentoElectronico.TotalIsc = Convert.ToDecimal(oTablaGenerico.Rows[0]["SumatoriaISC"]);
                            oDocumentoElectronico.TotalOtrosTributos = Convert.ToDecimal(oTablaGenerico.Rows[0]["SumatoriaOtrosTributos"]);
                            oDocumentoElectronico.TotalVenta = Convert.ToDecimal(oTablaGenerico.Rows[0]["ImporteTotal"]);

                            oDocumentoElectronico.Gravadas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperGravadas"]);
                            oDocumentoElectronico.Inafectas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperInafectas"]);
                            oDocumentoElectronico.Exoneradas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperExoneradas"]);
                            //oDocumentoElectronico.Gratuitas = Convert.ToDecimal(oTablaGenerico.Rows[0]["ValorVentaTotalOperGratuitas"]);
                            #endregion

                            #region Documentos Modificados
                            if ((oTablaGenerico.Rows[0]["SerieNumeroComprobanteModifica"]).ToString() != String.Empty)
                            {
                                oDocumentoElectronico.Relacionados = new List<DocumentoRelacionado>();

                                DocumentoRelacionado oDocumentoRelacionado = new DocumentoRelacionado();
                                oDocumentoRelacionado.NroDocumento = (oTablaGenerico.Rows[0]["SerieNumeroComprobanteModifica"]).ToString();
                                oDocumentoRelacionado.TipoDocumento = (oTablaGenerico.Rows[0]["TipoDocumentoComprobanteModifica"]).ToString();

                                oDocumentoElectronico.Relacionados.Add(oDocumentoRelacionado);
                            }
                            #endregion

                            #region Discrepancias - Mismo Documento con Tipo segun Escenario
                            oDocumentoElectronico.Discrepancias = new List<Discrepancia>();

                            Discrepancia oDocumentoModificado = new Discrepancia();
                            oDocumentoModificado.NroReferencia = (oTablaGenerico.Rows[0]["SerieNumeroComprobanteModifica"]).ToString(); // (oTablaGenerico.Rows[0]["Serie"]).ToString() + "-" + (oTablaGenerico.Rows[0]["Correlativo"]).ToString(); //(oTablaGenerico.Rows[0]["SerieNumeroComprobanteModifica"]).ToString();
                            oDocumentoModificado.Tipo = (oTablaGenerico.Rows[0]["TipoNotaDetalle"]).ToString();
                            oDocumentoModificado.Descripcion = (oTablaGenerico.Rows[0]["MotivoSustento"]).ToString(); ;

                            oDocumentoElectronico.Discrepancias.Add(oDocumentoModificado);
                            #endregion

                            #region Almacenar Datos Por Item
                            oDocumentoElectronico.Items = new List<DetalleDocumento>();

                            foreach (DataRow oFila in oTablaItems.Rows)
                            {
                                DetalleDocumento oDocumento = new DetalleDocumento();

                                oDocumento.Cantidad = Convert.ToDecimal(oFila["Cantidad"]);
                                oDocumento.CodigoItem = oFila["CodigoProducto"].ToString();
                                oDocumento.Descripcion = oFila["Descripcion"].ToString();
                                oDocumento.Id = Convert.ToInt16(oFila["NroItem"]);
                                oDocumento.Impuesto = Convert.ToDecimal(oFila["ImporteIGV"]);
                                oDocumento.ImpuestoSelectivo = Convert.ToDecimal(oFila["MontoISC"]);
                                oDocumento.OtroImpuesto = Decimal.Zero;
                                oDocumento.PrecioReferencial = Convert.ToDecimal(oFila["ValorReferencial"]);
                                oDocumento.PrecioUnitario = Convert.ToDecimal(oFila["ValorUnitario"]);
                                oDocumento.Suma = Convert.ToDecimal(oFila["ValorVenta"]);
                                oDocumento.TipoImpuesto = oFila["AfectacionIGV"].ToString();
                                oDocumento.TipoPrecio = oFila["PrecioVentaUnitarioTipo"].ToString();
                                oDocumento.TotalVenta = Convert.ToDecimal(oFila["ValorVenta"]);
                                oDocumento.UnidadMedida = oFila["UnidadMedida"].ToString();

                                oDocumentoElectronico.Items.Add(oDocumento);
                            }
                            #endregion

                            #region Definir Parametros
                            String strNombreArchivo = String.Empty;
                            String strRutaPFX = String.Empty;
                            String strClavePFX = String.Empty;

                            if (oParametro.IdEmpresa == 1)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLENOSA.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalENOSA.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalENOSA.ToString();
                            }
                            else if (oParametro.IdEmpresa == 2)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLENSA.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalENSA.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalENSA.ToString();
                            }
                            else if (oParametro.IdEmpresa == 3)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLHDNA.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalHDNA.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalHDNA.ToString();
                            }
                            else if (oParametro.IdEmpresa == 4)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLELCTRO.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalELCTRO.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalELCTRO.ToString();
                            }
                            else if (oParametro.IdEmpresa == 5)
                            {
                                strRutaXML = CacheConfiguracion.RutaAlmacenarComprobanteXMLELOR.ToString();
                                strRutaPFX = CacheConfiguracion.CertificadoDigitalELOR.ToString();
                                strClavePFX = CacheConfiguracion.ClaveCertificadoDigitalELOR.ToString();
                            }

                            strRutaXML = strRutaXML + oDocumentoElectronico.FechaEmision;

                            if (!Directory.Exists(strRutaXML))
                                Directory.CreateDirectory(strRutaXML);

                            if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeCredito)
                                strNombreArchivo = oDocumentoElectronico.Emisor.NroDocumento
                                                    + "-"
                                                    + "07"
                                                    + "-"
                                                    + oDocumentoElectronico.Emisor.Serie
                                                    + "-"
                                                    + oDocumentoElectronico.Emisor.Correlativo
                                                    + ".xml";
                            else if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeDebito)
                                strNombreArchivo = oDocumentoElectronico.Emisor.NroDocumento
                                                    + "-"
                                                    + "08"
                                                    + "-"
                                                    + oDocumentoElectronico.Emisor.Serie
                                                    + "-"
                                                    + oDocumentoElectronico.Emisor.Correlativo
                                                    + ".xml";
                            #endregion

                            #region Generar XML
                            String strSignature = "Signature" + (
                                            oParametro.IdEmpresa == 1 ? "ENOSA"
                                                : oParametro.IdEmpresa == 2 ? "ENSA"
                                                : oParametro.IdEmpresa == 3 ? "HDNA"
                                                : oParametro.IdEmpresa == 4 ? "ELCTRO"
                                                : oParametro.IdEmpresa == 5 ? "ELOR"
                                                : "DISTRI"
                                        );

                            if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeCredito)
                            {
                                CreditNote oTramaClase = Generador.GenerarCreditNote(oDocumentoElectronico);

                                String strCertificadoBase64 = Convert.ToBase64String(File.ReadAllBytes(strRutaPFX));

                                var serializador = new Serializador();
                                serializador.RutaCertificadoDigital = strCertificadoBase64;
                                serializador.PasswordCertificado = strClavePFX;

                                String strXMLBase64 = serializador.GenerarXml(oTramaClase);
                                String strXMLFirmaBase64 = serializador.FirmarXml(strXMLBase64, strSignature);

                                strRutaXML = strRutaXML + "\\" + strNombreArchivo;

                                File.WriteAllBytes(strRutaXML, Convert.FromBase64String(strXMLFirmaBase64));
                            }
                            else if (oParametro.IdTipoDocumento == (Int16)enumDocumentoComercial.NotadeDebito)
                            {
                                DebitNote oTramaClase = Generador.GenerarDebitNote(oDocumentoElectronico);

                                String strCertificadoBase64 = Convert.ToBase64String(File.ReadAllBytes(strRutaPFX));

                                var serializador = new Serializador();
                                serializador.RutaCertificadoDigital = strCertificadoBase64;
                                serializador.PasswordCertificado = strClavePFX;

                                String strXMLBase64 = serializador.GenerarXml(oTramaClase);
                                String strXMLFirmaBase64 = serializador.FirmarXml(strXMLBase64, strSignature);

                                strRutaXML = strRutaXML + "\\" + strNombreArchivo;

                                File.WriteAllBytes(strRutaXML, Convert.FromBase64String(strXMLFirmaBase64));
                            }
                            #endregion

                            #region Validación de que el archivo ha sido Creado
                            bolResultado = File.Exists(strRutaXML);
                            #endregion
                            #endregion
                        }
                    }
                    #endregion

                    oResultado.GeneroXMLPDF = bolResultado;
                    oResultado.RutaXMLPDF = strRutaXML;
                }
                catch (Exception ex)
                {
                    throw ex;
                    //ExcepcionNG.Guardar(ex, enumTipoExcepcion.Error);
                }

                return oResultado;
            }


            #endregion Generar XML


            /// <summary>
            /// ARMADO TEMPORAL VER SI SE TIENE QUE ELIMINAR
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            private clsListaFinanciamientoIngresoProvision armarDeclsListaIngresoProvision(clsListaIngresoProvision obj)
            {
                clsListaFinanciamientoIngresoProvision tmp = new clsListaFinanciamientoIngresoProvision();

                foreach (clsIngresoProvisionBase x in obj.Elementos)
                {
                    //clsFinanciamientoIngresoProvisionBase j = new clsFinanciamientoIngresoProvisionBase();

                }

                return tmp;
            }



            #region Tablas temporales
            /// <summary>
            /// clsGestionarCobranza.cs -> NGC BACK ESCRITOTIO DIS NEGOCIO // linea 2825
            /// Se procede a crear las temporales y se realiza el ingreso correspondiente de los datos asociados
            /// <returns></returns>
            /// </summary>
            public bool TransaccionIngresoGrabar(clsListaIngresoProvision listaIngresoProvision, SqlTransaction transaction)
            {
                if (listaIngresoProvision == null || listaIngresoProvision.Elementos.Count == 0) return true;
                CrearTablasTemporalesIngresoProvision(transaction);
                IngresoProvisionInsertarTmp(listaIngresoProvision, transaction);


                //solo es para verificar los valores ingresados en la tabla temporal
                DataSet dTemporales = new DataSet();
                dTemporales = obtenerTablasTemporales(transaction);


                Hashtable _htparametros = new Hashtable();
                _htparametros.Add("@resultado", 0);
                _htparametros.Add("@p_idempresa", listaIngresoProvision.Elementos[0].IdEmpresa);
                _DAO.EjecutarComando("IngresoProvision", "Grabar", _htparametros, transaction);
                return (((Int32)_htparametros["@resultado"]) > 0);
            }

            /// <summary>
            /// Ingresa los datos en las tablas temporales
            /// </summary>
            /// <param name="listaIngresoProvision"></param>
            private void IngresoProvisionInsertarTmp(clsListaIngresoProvision listaIngresoProvision, SqlTransaction transaction)
            {
                Hashtable _htparametros = new Hashtable();
                foreach (clsIngresoProvision item in listaIngresoProvision.Elementos)
                {
                    if (item.IdTipoDocumento == (Int16)TipoDocumentoComercial.ReciboEnergia) { continue; }
                    if (item.IdTipoDocumento == (Int16)TipoDocumentoComercial.ConvenioDePago) { continue; }
                    if (item.IdTipoDocumento == (Int16)TipoDocumentoComercial.DocumentoDescuento) { continue; }
                    _htparametros["@idtipoidentidad"] = item.IdTipoIdentidad;
                    _htparametros["@nroidentidad"] = item.NroIdentidad;
                    _htparametros["@idmoneda"] = item.IdMonedaOrigen;
                    _htparametros["@idtipodocumento"] = item.IdTipoDocumento;
                    _htparametros["@nrodocumento"] = item.NroDocumento;
                    _htparametros["@importetotal"] = item.ImporteOrigenTotal;
                    _htparametros["@montodescargado"] = item.ImporteOrigenDescargo;
                    _htparametros["@saldo"] = item.ImporteOrigenSaldo;
                    _htparametros["@fecharegistro"] = DateTime.Now; //item.FechaRegistro; // Se comento la linea de 30.01.2018 ya que existe inconsistencia en dicho campo.
                    _htparametros["@fechaemision"] = item.FechaEmision;
                    _htparametros["@fechavencimiento"] = item.FechaVencimiento;
                    _htparametros["@estadodeuda"] = item.IdEstadoDeuda;
                    _htparametros["@idusuario"] = item.IdUsuario;
                    _htparametros["@idnroservicioprincipal"] = item.IdNroServicioPrincipal;
                    _htparametros["@idservicioprincipal"] = item.IdServicioPrincipal;
                    _htparametros["@idanocomercial"] = item.IdAnoComercial;
                    _htparametros["@idmescomercial"] = item.IdMesComercial;
                    /////////////////////////////////////////////////////
                    _htparametros["@serie"] = item.Serie;
                    _htparametros["@lotefacturacion"] = item.LoteFacturacion;
                    _htparametros["@idtipoatencion"] = item.IdTipoAtencion;
                    _htparametros["@idpuntoatencion"] = item.IdPuntoAtencion;
                    _htparametros["@nromesesdeuda"] = item.NroMesesDeuda;
                    _htparametros["@deudaanteriorinicial"] = item.DeudaAnteriorInicial;
                    _htparametros["@idcentroservicio"] = item.IdCentroServicio;
                    _htparametros["@idmovimientocomercial"] = item.IdMovimientoComercial;

                    _DAO.EjecutarComando("IngresoProvision", "InsertarTmp", _htparametros, transaction);


                    //Grabar IngresoProvisionComplemento
                    if (item.oIngresoProvisionComplemento != null &&
                    item.oIngresoProvisionComplemento.Serie != null &&
                    item.oIngresoProvisionComplemento.Serie.Length > 0)
                    {
                        IngresoprovisioncomplementoInsertartemp(item.oIngresoProvisionComplemento, transaction);
                    }
                    //Grabar IngresoProvisionDocumentos
                    IngresoProvisionDocumentos(item.ListaIngresoProvisionDocumentos, transaction);
                    ////Grabar IngresoProvisionDetalle
                    IngresoProvisionDetalleInsertarTmp(item.ListaIngresoProvisionDetalles, transaction);
                }
            }

            private bool IngresoProvisionDetalleInsertarTmp(System.ComponentModel.BindingList<clsIngresoProvisionDetalle> listaingresoprovisiondetalle, SqlTransaction transaction)
            {
                Hashtable _htparametros = new Hashtable();
                foreach (clsIngresoProvisionDetalle item in listaingresoprovisiondetalle)
                {
                    _htparametros["@idconcepto"] = item.idConcepto;
                    _htparametros["@importe"] = item.Importe;
                    _htparametros["@cantidad"] = item.Cantidad;
                    _htparametros["@preciounitario"] = item.PrecioUnitario;
                    _htparametros["@preciounitariosinfose"] = item.PrecioUnitarioSinFose;
                    _htparametros["@idtipodocumento"] = item.idTipoDocumento;
                    _htparametros["@idtipodocumentosustento"] = item.idTipoDocumentoSustento;
                    _htparametros["@nrodocumento"] = item.NroDocumento;
                    _htparametros["@nrodocumentosustento"] = item.NroDocumentoSustento;
                    _htparametros["@idnroservicio"] = item.IdNroServicio;
                    _htparametros["@indicadorigv"] = item.IndicadorIGV;
                    _DAO.EjecutarComando("IngresoProvisionDetalle", "InsertarTmp", _htparametros, transaction);
                    ingresoprovisiondetallecomplementoinsertartemp(item.oIngresoProvisionDetalleComplemento, transaction);
                }
                return true;
            }

            private bool ingresoprovisiondetallecomplementoinsertartemp(clsIngresoProvisionDetalleComplemento oIngresoProvisionDetalleComplemento, SqlTransaction transaction)
            {
                Hashtable htParametro = new Hashtable();

                htParametro.Add("@p_idempresa", oIngresoProvisionDetalleComplemento.IdEmpresa);
                htParametro.Add("@p_idtipodocumento", oIngresoProvisionDetalleComplemento.idTipoDocumento);
                htParametro.Add("@p_nrodocumento", oIngresoProvisionDetalleComplemento.NroDocumento);
                htParametro.Add("@p_idtipodocumentosustento", oIngresoProvisionDetalleComplemento.idTipoDocumentoSustento);
                htParametro.Add("@p_nrodocumentosustento", oIngresoProvisionDetalleComplemento.NroDocumentoSustento);
                htParametro.Add("@p_idnroservicio", oIngresoProvisionDetalleComplemento.idNroServicio);
                htParametro.Add("@p_idconcepto", oIngresoProvisionDetalleComplemento.idConcepto);
                htParametro.Add("@p_esexonerada", oIngresoProvisionDetalleComplemento.EsExonerada);
                htParametro.Add("@p_esretiropremio", oIngresoProvisionDetalleComplemento.EsRetiroPremio);
                htParametro.Add("@p_esretirotrabajador", oIngresoProvisionDetalleComplemento.EsRetiroTrabajador);
                htParametro.Add("@p_aplicadescuento", oIngresoProvisionDetalleComplemento.AplicaDescuento);
                htParametro.Add("@p_importedescuento", oIngresoProvisionDetalleComplemento.ImporteDescuento);
                htParametro.Add("@p_idunidadmedida", oIngresoProvisionDetalleComplemento.IdUnidadMedida);
                htParametro.Add("@p_observacion", (oIngresoProvisionDetalleComplemento.Observacion == null ? String.Empty : oIngresoProvisionDetalleComplemento.Observacion));
                htParametro.Add("@p_preciounitario", oIngresoProvisionDetalleComplemento.PrecioUnitario);
                _DAO.EjecutarComando("ingresoprovisiondetallecomplemento", "insertartemp", htParametro, transaction);
                return true;
            }

            /// <summary>
            /// Grabar IngresoProvisionComplemento
            /// </summary>
            /// <param name="_clsIngresoProvisionComplemento"></param>
            /// <returns>true / false</returns>
            private bool IngresoprovisioncomplementoInsertartemp(clsIngresoProvisionComplemento _clsIngresoProvisionComplemento, SqlTransaction transaction)
            {
                Hashtable htParametro = new Hashtable();
                htParametro.Add("@p_idempresa", _clsIngresoProvisionComplemento.IdEmpresa);
                htParametro.Add("@p_idtipodocumento", _clsIngresoProvisionComplemento.IdTipoDocumento);
                htParametro.Add("@p_nrodocumento", _clsIngresoProvisionComplemento.NroDocumento);
                htParametro.Add("@p_serie", _clsIngresoProvisionComplemento.Serie);
                htParametro.Add("@p_correlativo", _clsIngresoProvisionComplemento.Correlativo);
                htParametro.Add("@p_fechacontabilizacion", _clsIngresoProvisionComplemento.FechaContabilizacion);
                htParametro.Add("@p_tipocambio", _clsIngresoProvisionComplemento.TipoCambio);
                htParametro.Add("@p_detalle", _clsIngresoProvisionComplemento.Detalle);
                htParametro.Add("@p_aplicadetraccion", _clsIngresoProvisionComplemento.AplicaDetraccion);
                htParametro.Add("@p_nombre", _clsIngresoProvisionComplemento.Nombre);
                htParametro.Add("@p_direccion", _clsIngresoProvisionComplemento.Direccion);
                htParametro.Add("@p_codigosap", _clsIngresoProvisionComplemento.CodigoSAP);
                htParametro.Add("@p_email", _clsIngresoProvisionComplemento.EMail);
                _DAO.EjecutarComando("ingresoprovisioncomplemento", "insertartemp", htParametro, transaction);

                return true;
            }

            /// <summary>
            /// //Grabar IngresoProvisionDocumentos
            /// </summary>
            /// <param name="_clsIngresoProvisionDocumentos"></param>
            /// <returns>true /false</returns>
            private bool IngresoProvisionDocumentos(System.ComponentModel.BindingList<clsIngresoProvisionDocumentos> _clsIngresoProvisionDocumentos, SqlTransaction transaction)
            {
                Hashtable _htparametros = new Hashtable();
                foreach (clsIngresoProvisionDocumentos item in _clsIngresoProvisionDocumentos)
                {
                    _htparametros["@serie"] = item.Serie;
                    _htparametros["@nrodocumentosustento"] = item.NroDocumentoSustento;
                    _htparametros["@importetotal"] = item.ImporteTotal;
                    _htparametros["@montodescargado"] = item.MontoDescargado;
                    _htparametros["@saldo"] = item.Saldo;
                    _htparametros["@estadodeuda"] = item.IdEstadoDeuda;
                    _htparametros["@idservicio"] = item.IdServicio;
                    _htparametros["@idnroservicio"] = item.IdNroServicio;
                    _htparametros["@impuesto"] = item.Impuesto;
                    _htparametros["@indicadordeuda"] = item.IndicadorDeuda;
                    _htparametros["@estadodocumento"] = item.IdEstadoDocumento;
                    _htparametros["@idtipodocumento"] = item.IdTipoDocumento;
                    _htparametros["@nrodocumento"] = item.NroDocumento;
                    _htparametros["@idtipodocumentosustento"] = item.IdTipoDocumentoSustento;
                    _htparametros["@indicadorreclamo"] = item.IndicadorReclamo;
                    _DAO.EjecutarComando("IngresoProvisionDocumentos", "InsertarTmp", _htparametros, transaction);

                }
                return true;
            }
            private void OrdenCobroDocumentoObtenerListaPorOrdenCobro(int idOrdenCobro)
            {
                DataSet dts = new DataSet();
                Hashtable htparam = new Hashtable();
                htparam["@IdOrdenCobro"] = idOrdenCobro;
                htparam["@IdMonedaNacional"] = Moneda.Soles;


                //using (SqlConnection oConexion = _DAO.GetSqlConnection())
                //{
                string[] tables = { "documentosPorOrdenCobro", "ordenDocumentoDetalle", "documentosReferenciados" };
                //oConexion.Open();
                dts = _DAO.EjecutarComandoConjuntoEntidad("OrdenCobroDocumento", "ObtenerListaPorOrdenCobro", htparam, tables);

                //    oConexion.Close();
                //}


            }

            private void CrearTablasTemporalesIngresoProvision(SqlTransaction transaction)
            {
                CrearTablaTemporalIngresoProvision(transaction);
                CrearTablaTemporalIngresoProvisionComplemento(transaction);
                CrearTablaTemporalIngresoProvisionDocumentos(transaction);
                CrearTablaTemporalIngresoProvisionDetalle(transaction);
                CrearTablaTemporalIngresoProvisionDetalleComplemento(transaction);
            }
            /// <summary>
            /// Crea la tabla temporal #IngresoProvision
            /// </summary>
            private DataSet obtenerTablasTemporales(SqlTransaction transaction)
            {
                DataSet dt = new DataSet();
                StringBuilder _sbcreartemporal = new StringBuilder();
                _sbcreartemporal.AppendLine("SELECT * FROM #IngresoProvision");
                _sbcreartemporal.AppendLine("SELECT * FROM #IngresoProvisionComplemento");
                _sbcreartemporal.AppendLine("SELECT * FROM #IngresoProvisionDocumentos");
                _sbcreartemporal.AppendLine("SELECT * FROM #IngresoProvisionDetalle");
                _sbcreartemporal.AppendLine("SELECT * FROM #IngresoProvisionDetalleComplemento");

                string[] tables = { "IngresoProvision", "IngresoProvisionComplemento", "IngresoProvisionDocumentos", "IngresoProvisionDetalle", "IngresoProvisionDetalleComplemento" };

                dt = _DAO.ExecuteCommandSessionDataSet(transaction, _sbcreartemporal.ToString(), tables);
                return dt;
            }
            private void CrearTablaTemporalIngresoProvision(SqlTransaction transaction)
            {
                StringBuilder _sbcreartemporal = new StringBuilder();
                _sbcreartemporal.AppendLine("IF OBJECT_ID('tempdb..#IngresoProvision') IS NOT NULL ");
                _sbcreartemporal.AppendLine("   DROP TABLE #IngresoProvision ");
                _sbcreartemporal.AppendLine("CREATE TABLE #IngresoProvision ");
                _sbcreartemporal.AppendLine("( idTipoIdentidad smallint NOT NULL, NroIdentidad varchar(20)  NOT NULL, idMoneda smallint NOT NULL ");
                _sbcreartemporal.AppendLine(", idTipoDocumento smallint NOT NULL, NroDocumento char(13) NOT NULL, Serie char(3)  NOT NULL ");
                _sbcreartemporal.AppendLine(", ImporteTotal Money NOT NULL, MontoDescargado Money NOT NULL, Saldo Money NOT NULL ");
                _sbcreartemporal.AppendLine(", FechaRegistro datetime NOT NULL, FechaEmision datetime NOT NULL, FechaVencimiento smalldatetime NOT NULL ");
                _sbcreartemporal.AppendLine(", LoteFacturacion int NOT NULL, EstadoDeuda smallint NOT NULL ");
                _sbcreartemporal.AppendLine(", idTipoAtencion smallint NOT NULL, idPuntoAtencion smallint NOT NULL, idUsuario int NOT NULL ");
                _sbcreartemporal.AppendLine(", NroMesesDeuda smallint NOT NULL, idNroServicioPrincipal int NOT NULL, DeudaAnteriorInicial Money NOT NULL ");
                _sbcreartemporal.AppendLine(", idServicioPrincipal smallint NOT NULL, idCentroServicio smallint NOT NULL, idMovimientoComercial smallint NOT NULL ");
                _sbcreartemporal.AppendLine(", idAnoComercial smallint NOT NULL, idMesComercial smallint NOT NULL ) ");
                _DAO.ExecuteCommandSession(transaction, _sbcreartemporal.ToString(), false);
            }

            private void CrearTablaTemporalIngresoProvisionComplemento(SqlTransaction transaction)
            {
                StringBuilder _sbcreartemporal = new StringBuilder();
                _sbcreartemporal.AppendLine("IF OBJECT_ID('tempdb..#IngresoProvisionComplemento') > 0 ");
                _sbcreartemporal.AppendLine("   DROP TABLE #IngresoProvisionComplemento ");
                _sbcreartemporal.AppendLine("CREATE TABLE #IngresoProvisionComplemento ");
                _sbcreartemporal.AppendLine("( IdEmpresa smallint NOT NULL, IdTipoDocumento smallint NOT NULL, NroDocumento char(13) NOT NULL, ");
                _sbcreartemporal.AppendLine(" Serie char(4) NOT NULL, Correlativo varchar(10) NOT NULL, FechaContabilizacion datetime NOT NULL, ");
                _sbcreartemporal.AppendLine(" TipoCambio money NOT NULL, Detalle varchar(1024) NOT NULL, AplicaDetraccion smallint NOT NULL, ");
                _sbcreartemporal.AppendLine(" Nombre varchar(128) NOT NULL, Direccion varchar(128) NOT NULL, CodigoSAP varchar(10) NOT NULL ) ");

                _DAO.ExecuteCommandSession(transaction, _sbcreartemporal.ToString(), false);
            }


            private void CrearTablaTemporalIngresoProvisionDocumentos(SqlTransaction transaction)
            {
                StringBuilder _sbcreartemporal = new StringBuilder();
                _sbcreartemporal.AppendLine("IF OBJECT_ID('tempdb..#IngresoProvisionDocumentos') IS NOT NULL ");
                _sbcreartemporal.AppendLine("   DROP TABLE #IngresoProvisionDocumentos ");
                _sbcreartemporal.AppendLine("CREATE TABLE #IngresoProvisionDocumentos ");
                _sbcreartemporal.AppendLine("( Serie CHAR(3) NOT NULL , NroDocumentoSustento char(13) NOT NULL, ImporteTotal Money NOT NULL ");
                _sbcreartemporal.AppendLine(", MontoDescargado Money NOT NULL, Saldo Money NOT NULL, EstadoDeuda SMALLINT NOT NULL ");
                _sbcreartemporal.AppendLine(", idServicio smallint NOT NULL, idNroServicio int NOT NULL  ");
                _sbcreartemporal.AppendLine(", impuesto Money NOT NULL, IndicadorDeuda smallint NOT NULL, EstadoDocumento smallint NOT NULL ");
                _sbcreartemporal.AppendLine(", idTipoDocumento smallint NOT NULL, NroDocumento char(13) NOT NULL ");
                _sbcreartemporal.AppendLine(", idTipoDocumentoSustento smallint NOT NULL, IndicadorReclamo smallint NOT NULL ) ");
                _DAO.ExecuteCommandSession(transaction, _sbcreartemporal.ToString(), false);
            }

            private void CrearTablaTemporalIngresoProvisionDetalle(SqlTransaction transaction)
            {
                StringBuilder _sbcreartemporal = new StringBuilder();
                _sbcreartemporal.AppendLine("IF OBJECT_ID('tempdb..#IngresoProvisionDetalle') IS NOT NULL ");
                _sbcreartemporal.AppendLine("   DROP TABLE #IngresoProvisionDetalle ");
                _sbcreartemporal.AppendLine("CREATE TABLE #IngresoProvisionDetalle");
                _sbcreartemporal.AppendLine("( IdConcepto SMALLINT NOT NULL, Importe MONEY NOT NULL, CRC INT NOT NULL, Cantidad DECIMAL(10, 2) NOT NULL ");
                _sbcreartemporal.AppendLine(", PrecioUnitario MONEY NOT NULL, PrecioUnitarioSinFose MONEY NULL, IdTipoDocumento SMALLINT NOT NULL, IdTipoDocumentoSustento SMALLINT NOT NULL ");
                _sbcreartemporal.AppendLine(", NroDocumento char(13) NOT NULL, NroDocumentoSustento char(13) NOT NULL, IdNroServicio int NOT NULL , IndicadorIGV smallint NOT NULL) ");
                _DAO.ExecuteCommandSession(transaction, _sbcreartemporal.ToString(), false);
            }


            private void CrearTablaTemporalIngresoProvisionDetalleComplemento(SqlTransaction transaction)
            {
                StringBuilder _sbcreartemporal = new StringBuilder();
                _sbcreartemporal.AppendLine("IF OBJECT_ID('tempdb..#IngresoProvisionDetalleComplemento') IS NOT NULL");
                _sbcreartemporal.AppendLine("   DROP TABLE #IngresoProvisionDetalleComplemento ");
                _sbcreartemporal.AppendLine("CREATE TABLE #IngresoProvisionDetalleComplemento");
                _sbcreartemporal.AppendLine(" ( IdEmpresa smallint NOT NULL, idTipoDocumento smallint NOT NULL, NroDocumento char(13) NOT NULL, ");
                _sbcreartemporal.AppendLine(" idTipoDocumentoSustento smallint NOT NULL, NroDocumentoSustento char(13) NOT NULL, idNroServicio int NOT NULL, ");
                _sbcreartemporal.AppendLine(" idConcepto smallint NOT NULL, EsExonerada smallint NOT NULL, EsRetiroPremio smallint NOT NULL, ");
                _sbcreartemporal.AppendLine(" EsRetiroTrabajador smallint NOT NULL, AplicaDescuento smallint NOT NULL, ImporteDescuento MONEY NOT NULL, ");
                _sbcreartemporal.AppendLine(" IdUnidadMedida smallint NOT NULL, Observacion varchar(1024) NOT NULL, PrecioUnitario DECIMAL(18, 8)) ");

                _DAO.ExecuteCommandSession(transaction, _sbcreartemporal.ToString(), false);
            }


            //_DAO.ExecuteCommandSession(transaction, _sbcreartemporal.ToString());
            #endregion

            public clsListaFinanciamientoIngresoProvision ObtenerListaIngresoProvision(int idNroServicio, short idEmpresa)
            {
                Hashtable htparam = new Hashtable();
                DataSet dsresultado = null;
                clsListaFinanciamientoIngresoProvision oListaRecibos = null;
                //using (SqlConnection oConexion = _DAO.GetSqlConnection())
                //{
                //    oConexion.Open();
                htparam["@idempresa"] = idEmpresa;
                htparam["@idnroservicioprincipal"] = idNroServicio;
                htparam["@idmonedanacional"] = (Int16)Moneda.Soles;
                dsresultado = _DAO.EjecutarComandoConjuntoEntidad
               ("CobranzaOnLine", "BuscarDeudaPorNroServicio", htparam, "cabecera", "detalle");

                //    oConexion.Close();
                //}
                if (dsresultado != null)
                {
                    oListaRecibos = new clsListaFinanciamientoIngresoProvision(dsresultado.Tables["cabecera"], dsresultado.Tables["detalle"]);
                }

                return oListaRecibos;
            }


            public Int32 GrabarCobranzaMejoradaSedalib(clsFinanciamientoTransaccion oTransaccion, SqlTransaction sqTransaccion, Int16 operacion)
            {
                try
                {
                    #region Variables Locales

                    Int16 _intpositivonegativo = 1;
                    Int32 _intidtransaccionlog = 0;
                    Int32 _nrodocumentos = 0;
                    Int32 _nroservicios = 0;
                    Int32 _idnroservicioppago = 0;
                    Hashtable htDepositos = new Hashtable();

                    Int32 periodoPago = oTransaccion.PeriodoPago;

                    #endregion Variables Locales

                    #region Validaciones previas

                    if (oTransaccion.ElementosDocumentoDescargo.Count == 0)
                        throw new Exception("<<<Ingrese Documentos de Descargo.>>>");

                    if (oTransaccion.ElementosMedioPago.Count == 0)
                        throw new Exception("<<<Ingrese forma de Pago.>>>");

                    if (oTransaccion.IdMonedaNacional <= 0)
                        throw new Exception("<<<El Tipo de Moneda Nacional no se ha especificado para está transacción " + oTransaccion.IdMonedaNacional + ".>>>");

                    if (oTransaccion.ImporteNeto == 0)
                    {
                        StringBuilder _sbmensaje = new StringBuilder();
                        _sbmensaje.AppendLine("<<<Importe Neto de la Transacción es Cero.");
                        _sbmensaje.AppendLine("No se Aplico las Formas de Pago a los Documentos Cargados.>>>");

                        throw new Exception(_sbmensaje.ToString());
                    }

                    //if (!oGestionarCaja.EsCajaHabilitada(oTransaccion.UsuarioPuntoAtencion))
                    //    throw new Exception("<<<Caja No Esta Habilitada Para Cobranza.>>>");

                    #endregion Validaciones previas

                    #region Verificar Caja Aperturada

                    if (oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo == 0)
                    {
                        Int32 IntCantidadPuntosAbiertos = CantidadAbiertos(oTransaccion.UsuarioPuntoAtencion.IdUsuario);

                        if (IntCantidadPuntosAbiertos == 0)
                        {
                            clsFinanciamientoCajaEstadisticaDiario oCajaAperturar = new clsFinanciamientoCajaEstadisticaDiario();
                            oCajaAperturar.UsuarioPuntoAtencion = oTransaccion.UsuarioPuntoAtencion;
                            oCajaAperturar.IdNroCaja = 0;
                            oCajaAperturar.FechaApertura = DateTime.Now;
                            oCajaAperturar.FechaPago = DateTime.Now;

                            if (AperturarFechaPago(sqTransaccion, oCajaAperturar, Decimal.Zero))
                            {
                                oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaAperturar.FechaApertura;
                                oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaAperturar.FechaPago;
                                oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaAperturar.IdNroCaja;
                            }
                            else
                                throw new Exception("<<<No se logró realizar la operación de cobranza.>>>");
                        }
                        else if (IntCantidadPuntosAbiertos > 0)
                        {
                            clsFinanciamientoCajaEstadisticaDiario oCajaActual = ObtenerCajaAtencionUltima(oTransaccion.UsuarioPuntoAtencion.IdEmpresa
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdUnidadNegocio
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdCentroServicio
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdTipoAtencion
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdPuntoAtencion
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdUsuario,
                                                                                            (DateTime?)null);

                            if (oCajaActual != null)
                            {
                                DateTime datFechaServidor = DateTime.Now;

                                if (oCajaActual.FechaApertura.Date.CompareTo(datFechaServidor.Date) == 0)
                                {
                                    oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaActual.FechaApertura;
                                    oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaActual.FechaPago;
                                    oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaActual.IdNroCaja;
                                }
                                else
                                {
                                    //Forzar Cierre y Aperturar nueva Caja.
                                }
                            }
                        }
                    }

                    #endregion Verificar Caja Aperturada

                    #region Grabando en Transaccion

                    DateTime fInicio = DateTime.Now;

                    oTransaccion.IdTransaccionLog = _intidtransaccionlog;
                    oTransaccion.ImporteNeto *= _intpositivonegativo;
                    oTransaccion.ImporteRecibido *= _intpositivonegativo;
                    oTransaccion.ImporteTotalVuelto *= _intpositivonegativo;
                    oTransaccion.ImporteEfectivo *= _intpositivonegativo;
                    oTransaccion.ImporteValores *= _intpositivonegativo;
                    oTransaccion.IdEstado = 1;
                    oTransaccion.IdTipoDocumento = 4; //Lo setean en duro en NGC Escritorio

                    oTransaccion.NroTransaccion = TransaccionInsertar(sqTransaccion, oTransaccion);

                    if (oTransaccion.NroTransaccion <= 0)
                        throw new Exception("<<<No se registro en Transaccion.>>>");

                    #endregion Grabando en Transaccion

                    #region Grabando en Transaccion Documentos Descargo

                    _idnroservicioppago = 0;

                    foreach (clsFinanciamientoDocumentosDescargoBase itemdocumentodescargo in oTransaccion.ElementosDocumentoDescargo)
                    {
                        clsFinanciamientoDocumentosDescargoPropio itemdocumentodescargopropio = null;
                        itemdocumentodescargo.EsTerceros = false;

                        itemdocumentodescargopropio = (clsFinanciamientoDocumentosDescargoPropio)itemdocumentodescargo;

                        if (!_idnroservicioppago.Equals(itemdocumentodescargopropio.IdNroServicio))
                        {
                            _idnroservicioppago = itemdocumentodescargopropio.IdNroServicio;

                            //oTransaccion.PeriodoPago = ObtenerPeriodoPago(_idnroservicioppago);
                            itemdocumentodescargopropio.Periodo = oTransaccion.PeriodoPago;

                            if (oTransaccion.PeriodoPago == 0)
                            {
                                oTransaccion.PeriodoPago = periodoPago;
                                itemdocumentodescargopropio.Periodo = periodoPago;
                            }
                        }

                        if (itemdocumentodescargopropio.ImporteOrigenDescargo != 0)
                        {
                            itemdocumentodescargopropio.NroTransaccion = oTransaccion.NroTransaccion;
                            itemdocumentodescargopropio.ImporteOrigenCobrar *= _intpositivonegativo;
                            itemdocumentodescargopropio.ImporteOrigenDescargo *= _intpositivonegativo;
                            itemdocumentodescargopropio.ImporteOrigenDeuda *= _intpositivonegativo;

                            Insertar(sqTransaccion
                                    , oTransaccion
                                    , itemdocumentodescargopropio
                                    , oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                        }
                    }

                    #endregion Grabando en Transaccion Documentos Descargo

                    #region Grabando en Transaccion Formas de Pago

                    foreach (clsFinanciamientoMediosPagoBase itemmediopago in oTransaccion.ElementosMedioPago)
                    {
                        itemmediopago.NroTransaccion = oTransaccion.NroTransaccion;
                        itemmediopago.ImporteCambioTotal *= _intpositivonegativo;
                        itemmediopago.ImporteOrigenTotal *= _intpositivonegativo;
                        itemmediopago.ImporteCambioDescargo *= _intpositivonegativo;
                        itemmediopago.ImporteOrigenDescargo *= _intpositivonegativo;

                        clsFinanciamientoMediosPagoExterno itemmediopagoexterno = (clsFinanciamientoMediosPagoExterno)itemmediopago;

                        itemmediopagoexterno.EsIngreso = 1;

                        if (itemmediopagoexterno.IdOrdenCobroDocumento == "[Vuelto]")
                            itemmediopagoexterno.EsIngreso = 0;

                        InsertarMedioDePago(sqTransaccion, itemmediopagoexterno, oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                    }

                    #endregion Grabando en Transaccion Formas de Pago

                    #region Actualizando Saldos del Cliente

                    //TransaccionActualizarSaldos(sqTransaccion
                    //                            , oTransaccion.NroTransaccion);

                    #endregion Actualizando Saldos del Cliente

                    #region Actualizando Control de Caja

                    foreach (clsFinanciamientoCajaEstadisticaDiarioMoneda item in oTransaccion.ElementosCajaServicioMoneda)
                    {
                        item.ImporteCambio *= _intpositivonegativo;
                        item.Importe *= _intpositivonegativo;
                        item.IdNroCaja = oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo;

                        InsertarCaja(sqTransaccion, item);
                    }

                    //oCajaServicioDAO.Registrar(sqTransaccion, oTransaccion, ref _nroservicios, ref _nrodocumentos, CacheConfiguracion.IdOperacionCobranza);
                    ActualizarCaja(sqTransaccion, oTransaccion.NroTransaccion, _nrodocumentos, _nroservicios);

                    #endregion Actualizando Control de Caja
                    //NGC Escritorio
                    #region Ultimas operaciones

                    if (operacion == CacheConfiguracion.IdOperacionCobranza)
                    {
                        // Fecha Hora Fin
                        DateTime fFin = DateTime.Now;
                        TimeSpan segundos = fFin - fInicio;

                        // Grabando en Transaccion Tiempo
                        TransaccionInsertarTiempos(sqTransaccion, oTransaccion, (Int32)segundos.TotalSeconds, _nrodocumentos);
                    }

                    #endregion Ultimas operaciones

                    #region Validación 
                    /*
                     * Validación implementada por los errores reportados por el punto de atención de CARS La Noria, en la cobranza de recibo de agua
                     * La cobranza se 
                    */

                    //Esta Parte no va, segun Cobranza no es recaudacion terceros
                    /*
                    if (bolEsRecaudacionTerceros)
                    {
                        DataTable dtResultado = clsTransaccionDAO.Instancia.TransaccionValidarCantidadCajaEstado(sqltransaccion, transaccion.NroTransaccion);

                        if (dtResultado != null && dtResultado.Rows.Count > 0)
                        {
                            if (dtResultado.Rows.Count > 1)
                                throw new Exception("<<<No se logró procesar la cobranza de manera satisfactoria, favor reintentar.>>>");

                            foreach (DataRow oFila in dtResultado.Rows)
                            {
                                if (Convert.ToInt16(oFila["IdEstadoCobranza"]) == (Int16)Estado.EnProceso)
                                {
                                    throw new Exception("<<<No se logró procesar la cobranza de manera satisfactoria, favor reintentar.>>>");
                                    break;
                                }
                            }
                        }
                    }
                    */
                    #endregion


                    return oTransaccion.NroTransaccion;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            #region Verificar Caja Aperturada

            public Int32 CantidadAbiertos(Int32 IdUsuario)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();
                    _htparametros.Add("@idusuario", IdUsuario);

                    Int32 NroPuntosAbiertoPorUsuario = 0;
                    Object resultado = _DAO.EjecutarComandoEscalar("UsuarioPuntoAtencion", "CantidadAbiertos", _htparametros);

                    if (resultado != null) NroPuntosAbiertoPorUsuario = (Int32)resultado;

                    return NroPuntosAbiertoPorUsuario;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Boolean AperturarFechaPago(SqlTransaction objtransaccion, clsFinanciamientoCajaEstadisticaDiario caja, Decimal saldoinicial)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();

                    _htparametros.Add("@idnrocaja", null);
                    _htparametros.Add("@fechaapertura", null);
                    _htparametros.Add("@idempresacajero", caja.UsuarioPuntoAtencion.IdEmpresa);
                    _htparametros.Add("@iduunncajero", caja.UsuarioPuntoAtencion.IdUnidadNegocio);
                    _htparametros.Add("@idcentroserviciocajero", caja.UsuarioPuntoAtencion.IdCentroServicio);
                    _htparametros.Add("@idtipoatencion", caja.UsuarioPuntoAtencion.IdTipoAtencion);
                    _htparametros.Add("@idpuntoatencion", caja.UsuarioPuntoAtencion.IdPuntoAtencion);
                    _htparametros.Add("@idusuario", caja.UsuarioPuntoAtencion.IdUsuario);
                    _htparametros.Add("@fechapago", caja.FechaPago);
                    _htparametros.Add("@fechaini", caja.FechaPago.Date);
                    _htparametros.Add("@fechafin", caja.FechaPago.AddDays(1).Date);

                    _DAO.EjecutarComando("cajaestadisticadiario", "abrirfechapago", _htparametros, objtransaccion);

                    caja.IdNroCaja = (Int32)_htparametros["@idnrocaja"];
                    caja.FechaApertura = (DateTime)_htparametros["@fechaapertura"];

                    return (caja.IdNroCaja > 0);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public clsFinanciamientoCajaEstadisticaDiario ObtenerCajaAtencionUltima(Int16 IdEmpresa
                                                                   , Int16 IdUUNN
                                                                   , Int16 IdCCSS
                                                                   , Int16 IdTipoAtencion
                                                                   , Int16 IdPuntoAtencion
                                                                   , Int32 IdUsuario
                                                                   , DateTime? fechaApertura)
            {
                try
                {
                    clsFinanciamientoCajaEstadisticaDiario objcajas;

                    objcajas = Buscar(IdEmpresa, IdUUNN, IdCCSS, IdTipoAtencion, IdPuntoAtencion, IdUsuario, fechaApertura);

                    if (objcajas == null) { return objcajas; }

                    if (objcajas.FechaCierre != null && ((Estado)objcajas.UsuarioPuntoAtencion.IdEstadoCobranza != Estado.Cerrado))
                    {
                        StringBuilder strMensaje = new StringBuilder();
                        strMensaje.AppendLine("<<<Error en inconsistencia de datos");
                        strMensaje.AppendLine("tiene el punto de atencion en estado " + ((Estado)objcajas.UsuarioPuntoAtencion.IdEstadoCobranza).ToString() + " y tiene asignado la FechaCierre " + objcajas.FechaCierre.ToString() + ".>>>");

                        throw new Exception(strMensaje.ToString());
                    }

                    objcajas.CantidadPendienteABoveda = ObtenerPendientesABoveda(objcajas.IdNroCaja);

                    return objcajas;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public clsFinanciamientoCajaEstadisticaDiario Buscar(Int16 idEmpresaCajero
                                                 , Int16 idUUNNCajero
                                                 , Int16 idCentroServicioCajero
                                                 , Int16 idTipoAtencion
                                                 , Int16 idPuntoAtencion
                                                 , Int32 IdUsuario
                                                 , DateTime? fechaApertura)
            {
                try
                {
                    clsFinanciamientoCajaEstadisticaDiario _objcaja;
                    Hashtable _htparametros = new Hashtable();
                    DataRow _drwregistro;

                    _htparametros.Add("@idempresacajero", idEmpresaCajero);
                    _htparametros.Add("@iduunncajero", idUUNNCajero);
                    _htparametros.Add("@idcentroserviciocajero", idCentroServicioCajero);
                    _htparametros.Add("@idtipoatencion", idTipoAtencion);
                    _htparametros.Add("@idpuntoatencion", idPuntoAtencion);
                    _htparametros.Add("@idusuario", IdUsuario);
                    _htparametros.Add("@fechaapertura", fechaApertura);

                    _drwregistro = _DAO.EjecutarComandoRegistro("cajaestadisticadiario", "buscarultimacajaporusuario", _htparametros);

                    if (_drwregistro == null) { return (clsFinanciamientoCajaEstadisticaDiario)null; }

                    _objcaja = new clsFinanciamientoCajaEstadisticaDiario(_drwregistro);

                    return _objcaja;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Int32 ObtenerPendientesABoveda(Int32 idnrocaja)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();

                    _htparametros.Add("@idnrocaja", idnrocaja);

                    return (Int32)_DAO.EjecutarComandoEscalar("TransaccionFormaPagoExternoNoefectivo", "ObtenerPendientesABoveda", _htparametros);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Boolean TransaccionInsertarTiempos(SqlTransaction sqltransaccion
                                        , clsFinanciamientoTransaccion transaccion
                                        , Int32 nroSegundosCobranza
                                        , Int32 nroDocumentos)
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@idnrocaja", transaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                _htparametros.Add("@idtransaccion", transaccion.NroTransaccion);
                _htparametros.Add("@segundosusuario", transaccion.SegundosAtencionUsuario);
                _htparametros.Add("@segundoslog", 0);
                _htparametros.Add("@segundossp", nroSegundosCobranza);
                _htparametros.Add("@nrodocumentos", nroDocumentos);

                _DAO.EjecutarComando("transaccion", "insertartiempo", _htparametros, sqltransaccion);

                return true;
            }
            #endregion Verificar Caja Aperturada

            #region Grabando en Transaccion

            public Int32 TransaccionInsertar(SqlTransaction sqltransaccion, clsFinanciamientoTransaccion oTransaccion)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();

                    _htparametros.Add("@nrotransaccion", 0);
                    _htparametros.Add("@idtipodocumento", oTransaccion.IdTipoDocumento);
                    _htparametros.Add("@fecharegistro", oTransaccion.FechaRegistro);
                    _htparametros.Add("@periodopago", oTransaccion.PeriodoPago);
                    _htparametros.Add("@fechapago", oTransaccion.FechaPago);
                    _htparametros.Add("@importe", oTransaccion.ImporteNeto);
                    _htparametros.Add("@importerecibido", oTransaccion.ImporteRecibido);
                    _htparametros.Add("@importeefectivo", oTransaccion.ImporteEfectivo);
                    _htparametros.Add("@importevalores", oTransaccion.ImporteValores);
                    _htparametros.Add("@vuelto", oTransaccion.ImporteTotalVuelto);
                    _htparametros.Add("@idusuario", oTransaccion.UsuarioPuntoAtencion.IdUsuario);
                    _htparametros.Add("@idtipoatencion", oTransaccion.UsuarioPuntoAtencion.IdTipoAtencion);
                    _htparametros.Add("@idpuntoatencion", oTransaccion.UsuarioPuntoAtencion.IdPuntoAtencion);
                    _htparametros.Add("@idcentroserviciocajero", oTransaccion.UsuarioPuntoAtencion.IdCentroServicio);
                    _htparametros.Add("@iduunncajero", oTransaccion.UsuarioPuntoAtencion.IdUnidadNegocio);
                    _htparametros.Add("@idempresacajero", oTransaccion.UsuarioPuntoAtencion.IdEmpresa);
                    _htparametros.Add("@idmoneda", oTransaccion.IdMonedaNacional);
                    _htparametros.Add("@idnrocaja", oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo); //1458844, no la obtiene por fechas
                    _htparametros.Add("@idmovimientocomercial", oTransaccion.IdMovComercial);
                    _htparametros.Add("@estado", oTransaccion.IdEstado);


                    //_htparametros.Add("@idordencobro", oTransaccion.IdOrdenCobro);


                    _DAO.EjecutarComando("transaccion", "insertarregistro", _htparametros, sqltransaccion);

                    oTransaccion.PeriodoPago = Convert.ToInt32(_htparametros["@periodopago"]);

                    return Convert.ToInt32(_htparametros["@nrotransaccion"]);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public int ObtenerPeriodoPago(Int32 idnroservicio)
            {
                try
                {
                    Int32 periodoPago = 0;

                    Hashtable htparametros = new Hashtable();
                    htparametros.Add("@p_idnroservicio", idnroservicio);

                    Object resultado = _DAO.EjecutarComandoEscalar("Transaccion", "DeterminarPeriodoPago", htparametros);

                    if (resultado != null) periodoPago = Convert.ToInt32(resultado);

                    return periodoPago;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Boolean Insertar(SqlTransaction sqltransaccion, clsFinanciamientoTransaccion transaccion, clsFinanciamientoDocumentosDescargoPropio documentodescargopropio, Int32 idnrocaja)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();

                    _htparametros.Add("@nrotransaccion", documentodescargopropio.NroTransaccion);
                    _htparametros.Add("@importedescargo", documentodescargopropio.ImporteOrigenDescargo);
                    _htparametros.Add("@importedeuda", documentodescargopropio.ImporteOrigenDeuda);
                    _htparametros.Add("@idempresa", documentodescargopropio.IdEmpresa);
                    _htparametros.Add("@idTipoDocumento", documentodescargopropio.IdTipoDocumentoSustento);
                    _htparametros.Add("@NroDocumento", documentodescargopropio.NroDocumentoSustento);
                    //_htparametros.Add("@idordencobro", documentodescargopropio.IdOrdenCobro);
                    //_htparametros.Add("@idordencobrodocumento", documentodescargopropio.IdOrdenCobroDocumento);
                    //_htparametros.Add("@idordencobrosustento", documentodescargopropio.IdOrdenCobroSustento);
                    //_htparametros.Add("@idordencobrodocumentosustento", documentodescargopropio.IdOrdenCobroDocumentoSustento);
                    _htparametros.Add("@idTipoDocumentosustento", documentodescargopropio.IdTipoDocumentoSustento);
                    _htparametros.Add("@NroDocumentosustento", documentodescargopropio.NroDocumentoSustento);
                    _htparametros.Add("@idservicio", documentodescargopropio.IdServicio);
                    _htparametros.Add("@ordenpago", documentodescargopropio.OrdenPagoServicio);
                    _htparametros.Add("@idnroservicio", documentodescargopropio.IdNroServicio);
                    _htparametros.Add("@estercero", documentodescargopropio.EsTerceros);
                    _htparametros.Add("@idnrocaja", idnrocaja);
                    _htparametros.Add("@fechapago", transaccion.FechaPago);
                    _htparametros.Add("@periodopago", transaccion.PeriodoPago);

                    if (documentodescargopropio.EsTerceros) _DAO.EjecutarComando("transacciondocumentosdescargo", "terceroinsertarregistro", _htparametros, sqltransaccion);
                    else _DAO.EjecutarComando("transacciondocumentosdescargo", "propioinsertarregistro", _htparametros, sqltransaccion);

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Boolean InsertarMedioDePago(SqlTransaction sqltransaccion, clsFinanciamientoMediosPagoExterno mediopagoexterno, Int32 idnrocaja)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();
                    _htparametros.Add("@nrotransaccion", mediopagoexterno.NroTransaccion);
                    _htparametros.Add("@importepago", mediopagoexterno.ImporteOrigenTotal);
                    _htparametros.Add("@idmonedapago", mediopagoexterno.IdMonedaOrigen);
                    _htparametros.Add("@idmonedacambio", mediopagoexterno.IdMonedaCambio);
                    _htparametros.Add("@tipocambio", mediopagoexterno.TipoCambio);
                    _htparametros.Add("@importecambio", mediopagoexterno.ImporteCambioTotal);
                    _htparametros.Add("@idTipoDocumento", mediopagoexterno.idTipoDocumento);
                    _htparametros.Add("@NroDocumento", mediopagoexterno.NroDocumento);
                    // @idTipoDocumento
                    // @NroDocumento
                    _htparametros.Add("@esingreso", mediopagoexterno.EsIngreso);
                    _htparametros.Add("@idbanco", mediopagoexterno.IdBanco);
                    _htparametros.Add("@idtipotarjeta", mediopagoexterno.IdTipoTarjeta);
                    _htparametros.Add("@nrotarjeta", mediopagoexterno.NroTarjeta);
                    _htparametros.Add("@idcuentabancaria", mediopagoexterno.IdCuentaBancaria); //mod
                    _htparametros.Add("@fechadeposito", mediopagoexterno.FechaDocumento);
                    _htparametros.Add("@idnrocaja", idnrocaja);
                    _htparametros.Add("@cuentabancaria", mediopagoexterno.NroCuentaBancaria);

                    //_htparametros.Add("@idordencobro", mediopagoexterno.IdOrdenCobro);
                    //_htparametros.Add("@idordencobrodocumento", mediopagoexterno.IdOrdenCobroDocumento);

                    _DAO.EjecutarComando("transaccionformapagoexterno", "insertarregistro", _htparametros, sqltransaccion);

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            #endregion Grabando en Transaccion

            #region Caja

            public Boolean ActualizarCaja(SqlTransaction sqltransaccion, Int32 nrotransaccion, Int32 nrodocumentos, Int32 nroservicios)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();

                    _htparametros.Add("@nrotransaccion", nrotransaccion);
                    _htparametros.Add("@nrodocumentos", nrodocumentos);
                    _htparametros.Add("@nroservicios", nroservicios);

                    _DAO.EjecutarComando("cajaestadisticadiario", "actualizarcaja", _htparametros, sqltransaccion);

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public Boolean InsertarCaja(SqlTransaction sqltransaccion, clsFinanciamientoCajaEstadisticaDiarioMoneda cajaestadisticamoneda)
            {
                try
                {
                    Hashtable _htparametros = new Hashtable();

                    _htparametros.Add("@idnrocaja", cajaestadisticamoneda.IdNroCaja);
                    _htparametros.Add("@idmonedapago", cajaestadisticamoneda.IdMoneda);
                    _htparametros.Add("@idmonedacambio", cajaestadisticamoneda.IdMonedaCambio);
                    _htparametros.Add("@importepago", cajaestadisticamoneda.Importe);
                    _htparametros.Add("@tipocambio", cajaestadisticamoneda.TipoCambio);
                    _htparametros.Add("@importecambio", cajaestadisticamoneda.ImporteCambio);
                    _htparametros.Add("@idtipodocumento", cajaestadisticamoneda.IdTipoDocumentoComercial);

                    _DAO.EjecutarComando("cajaestadisticadiariomoneda", "insertar", _htparametros, sqltransaccion);

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            #endregion Caja

            #region COBRO NGC Escritorio
            public Boolean ActualizarOrdenDeCobro(SqlTransaction transaccion
                             , Int32 idordencobro
                             , Int32 idusuariocaja
                             , DateTime fechacaja
                             , Int16 idEstado)
            {
                //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
                Hashtable _objparametros = new Hashtable();

                _objparametros.Add("@idordencobro", idordencobro);
                _objparametros.Add("@idusuariocaja", idusuariocaja);
                _objparametros.Add("@fechacaja", fechacaja);
                _objparametros.Add("@idestado", idEstado);

                _DAO.EjecutarComando("OrdenDeCobro"
                                              , "ActualizarCobro"
                                              , _objparametros, transaccion);

                return true;
            }

            public Boolean ActualizarOdenCobroDocumento(SqlTransaction transaccion
                             , Int32 idordencobro
                             , Int16 idtipodocumento
                             , Int16 intidempresa
                             , String nrodocumento)
            {
                //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
                Hashtable _objparametros = new Hashtable();
                _objparametros.Add("@idordencobrodocumento", idordencobro);
                _objparametros.Add("@idtipodocumento", idtipodocumento);
                _objparametros.Add("@idempresa", intidempresa);
                _objparametros.Add("@nrodocumento", nrodocumento);

                _DAO.EjecutarComando("OrdenCobroDocumento"
                                              , "ActualizarCobro"
                                              , _objparametros, transaccion);

                return true;
            }

            public DataRow GrabarNuevoObtenerNroConvenio(SqlTransaction sqltransaccion
                   , Int32 intidordencobro
                   , Int16 intidempresadocumento
                   , Int16 intidtipodocumento
                   , String strnrodocumento
                   , Int32 periodopago
                   , DateTime fechapago
                   , clsFinanciamientoUsuarioPuntoAtencion usuarioPuntoAtencion
                )
            {
                DataRow drfila = null;
                //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
                Hashtable _htparametros = new Hashtable();
                _htparametros.Add("@p_idordencobro", intidordencobro);
                _htparametros.Add("@p_idempresa", intidempresadocumento);
                _htparametros.Add("@p_idtipodocumento", intidtipodocumento);
                _htparametros.Add("@p_nrodocumento", strnrodocumento);
                _htparametros.Add("@p_periodopago", periodopago);
                _htparametros.Add("@p_fechapago", fechapago);
                _htparametros.Add("@P_idTipoAtencion", usuarioPuntoAtencion.IdTipoAtencion);
                _htparametros.Add("@P_idPuntoAtencion", usuarioPuntoAtencion.IdPuntoAtencion);


                try
                {
                    DataTable drdatos = (DataTable)_DAO.EjecutarComandoEntidad("FinanciamientoConvenio", "InsertarTemporalFisicaNumero", _htparametros, sqltransaccion);

                    if (drdatos != null && drdatos.Rows.Count > 0)
                        drfila = drdatos.Rows[0];
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return drfila;
            }

            public Boolean ActualizarDatosCobranzaServicio(Int32 intidsolicitud
                                            , Int16 intidempresa
                                            , Int16 intidtipodocumento
                                            , String strnrodocumento
                                            , DateTime datfecharegistro
                                            , SqlTransaction sqltransaccion)
            {
                Boolean _bolresultado = false;
                //clsGestionDato _objgestor = clsGestionDato.ObtenerInstancia();
                Hashtable _htparametro = new Hashtable();
                _htparametro.Add("@p_idsolicitud", intidsolicitud);
                _htparametro.Add("@p_idempresa", intidempresa);
                _htparametro.Add("@p_idtipodocumento", intidtipodocumento);
                _htparametro.Add("@p_nrodocumento", strnrodocumento);
                _htparametro.Add("@p_fecharegistro", datfecharegistro);

                try
                {
                    _DAO.EjecutarComando("solicitudservicio", "actualizardatoscobranzaservicio", _htparametro, sqltransaccion);
                    _bolresultado = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _bolresultado;
            }


            public Boolean ActualizarNroContratoenContrato(SqlTransaction sqltransaccion
                                                , Int32 intidsolicitud
                                                , Int32 intidnroservicio)
            {
                Boolean _bolresultado = false;
                //clsGestionDato _objgestor = clsGestionDato.ObtenerInstancia();
                Hashtable _htparametro = new Hashtable();
                _htparametro.Add("@p_idsolicitud", intidsolicitud);
                _htparametro.Add("@p_idnroservicio", intidnroservicio);

                try
                {
                    _DAO.EjecutarComando("solicitudserviciocontrato", "actualizarnumero", _htparametro, sqltransaccion);
                    _bolresultado = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return _bolresultado;
            }


            public Boolean RegistrarDocFavor(SqlTransaction transaccion
                               , Int32 idordencobro
                               , Int32 nrotransaccion
                               , Int16 idtipoatencion
                               , Int16 idpuntoatencion
                               , Int32 idnrocaja)
            {
                //clsGestionDato ogestordao = clsGestionDato.Instancia;
                Hashtable htParametros = new Hashtable();

                htParametros.Add("@p_idordencobro", idordencobro);
                htParametros.Add("@p_nrotransaccion", nrotransaccion);
                htParametros.Add("@p_idtipoatencion", idtipoatencion);
                htParametros.Add("@p_idpuntoatencion", idpuntoatencion);
                htParametros.Add("@p_idnrocaja", idnrocaja);

                _DAO.EjecutarComando("ordendecobro"
                                         , "registrardocafavor"
                                         , htParametros, transaccion);

                return true;
            }

            public String GrabarDefinitivo(SqlTransaction sqltransaccion
                             , Int32 intidordencobro
                             , Int32 periodopago
                             , DateTime fechapago
                             , clsFinanciamientoTransaccion transaccion
                             , int metodoPago
                             , int idNumOrden
                )
            {
                //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
                Hashtable _htparametros = new Hashtable();
                _htparametros.Add("@p_idordencobro", intidordencobro);
                _htparametros.Add("@p_nroConvenio", "");
                _htparametros.Add("@p_periodopago", periodopago);
                _htparametros.Add("@p_fechapago", fechapago);
                _htparametros.Add("@p_IdTipoAtencion", transaccion.UsuarioPuntoAtencion.IdTipoAtencion);
                _htparametros.Add("@p_IdPuntoAtencion", transaccion.UsuarioPuntoAtencion.IdPuntoAtencion);
                _htparametros.Add("@p_metodopago", metodoPago);
                _htparametros.Add("@p_nropedido", idNumOrden);

                try
                {
                    // en remplazo del store procedure Convenio_GrabarDefinitivo_pa // por existencia de campos vacios
                    _DAO.EjecutarComando("FinanciamientoConvenio"
                                                  , "grabardefinitivo"
                                                  , _htparametros, sqltransaccion);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                return _htparametros["@p_nroConvenio"].ToString().Trim();
            }

            public Boolean ActualizarEstadoFinanciadoIngresoProvision(SqlTransaction transaccion, Int16 idempresa, Int16 idtipodocumento, String nrodocumento)
            {
                //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
                Hashtable _objparametros = new Hashtable();

                _objparametros.Add("@idempresa", idempresa);
                _objparametros.Add("@idtipodocumento", idtipodocumento);
                _objparametros.Add("@nrodocumento", nrodocumento);
                //VALOR POR DEFECTO PARA PASAR VALIDACIÓN DEL TRIGGER
                _DAO.EjecutarComando("IngresoProvision"
                                              , "ActualizarEstadoFinanciado"
                                              , _objparametros, transaccion);

                return true;
            }

            public Boolean ActualizarEstadoPagado(SqlTransaction transaccion, Int16 idempresa, Int16 idtipodocumento, String nrodocumento, Int32 periodopago, DateTime fechapago)
            {
                //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
                Hashtable _objparametros = new Hashtable();

                _objparametros.Add("@idempresa", idempresa);
                _objparametros.Add("@idtipodocumento", idtipodocumento);
                _objparametros.Add("@nrodocumento", nrodocumento);
                _objparametros.Add("@periodopago", periodopago);
                _objparametros.Add("@fechapago", fechapago);

                _DAO.EjecutarComando("ingresoprovision"
                                              , "actualizarestadopagado"
                                              , _objparametros, transaccion);

                return true;
            }




            public Boolean RegistrarConvenioDocumentoFinanciado(SqlTransaction transaccion, String nroconvenio, Int16 idempresa, Int16 idtipodocumento, String nrodocumento)
            {
                //clsGestionDato _objgestiondato = clsGestionDato.ObtenerInstancia();
                Hashtable _objparametros = new Hashtable();

                _objparametros.Add("@nroconvenio", nroconvenio);
                _objparametros.Add("@idempresa", idempresa);
                _objparametros.Add("@idtipodocumento", idtipodocumento);
                _objparametros.Add("@nrodocumento", nrodocumento);

                _DAO.EjecutarComando("conveniodocumentosfinanciado"
                                              , "insertardocumentos"
                                              , _objparametros, transaccion);

                return true;
            }

            public Boolean ConvenioActualizarAplicadoInteresesDeudaFinanciar(SqlTransaction transaccion
                                                               , Int32 idnroservicio
                                                               , Int16 idtipodocumento
                                                               , String nrodocumento)
            {
                //clsGestionDato _objgestionadatos = clsGestionDato.ObtenerInstancia();
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@idnroservicio", idnroservicio);
                _htparametros.Add("@idtipodocumento", idtipodocumento);
                _htparametros.Add("@numerodocumento", nrodocumento);

                _DAO.EjecutarComando("Convenio"
                                                , "ActualizarAplicadoInteresesDeudaFinanciar"
                                                , _htparametros, transaccion);
                return true;
            }
            #endregion COBRO NGC Escritorio
            #endregion Public
        }

        #endregion

        #endregion

        #endregion Public


        #region generar PDF doc generar
        public static clsComprobantePago ObtenerInformacionDocumentoIngresoProvision
                                                 (
                                                         Int16 intidempresa
                                                     , Int16 intidtipodocumento
                                                     , String strnrodocumento
                                                 )
        {
            clsComprobantePago _objcomprobantepago = new clsComprobantePago();

            DataSet _dscomprobantepago;

            Hashtable _htparametros = new Hashtable();
            _htparametros.Add("@p_idempresa", intidempresa);
            _htparametros.Add("@p_idtipodocumento", intidtipodocumento);
            _htparametros.Add("@p_nrodocumento", strnrodocumento);

            _dscomprobantepago = _DAO.EjecutarComandoConjuntoEntidad("ingresoprovision", "obtenerpornrodocumento", _htparametros, "cabecera", "detalle", "convenio");

            if (_dscomprobantepago == null) { return null; }

            if (_dscomprobantepago.Tables["cabecera"].Rows.Count == 0)
            {
                throw new Exception("<<<No se encontró registro para Ingreso Provision.>>>");
            }

            if (_dscomprobantepago.Tables["convenio"].Rows.Count <= 0)
                _objcomprobantepago = new clsComprobantePago(_dscomprobantepago.Tables["cabecera"].Rows[0], _dscomprobantepago.Tables["detalle"], (DataRow)null);
            else
                _objcomprobantepago = new clsComprobantePago(_dscomprobantepago.Tables["cabecera"].Rows[0], _dscomprobantepago.Tables["detalle"], _dscomprobantepago.Tables["convenio"].Rows[0]);

            return _objcomprobantepago;
        }

        public clsComprobantePago ObtenerInformacionDocumentoIngresoProvisionElectronico
                                          (
                                                  SqlConnection oConexion
                                                , Int16 intidempresa
                                              , Int16 intidtipodocumento
                                              , String strnrodocumento
                                          )
        {
            clsComprobantePago _objcomprobantepago = new clsComprobantePago();
            //clsGestionDato _objgestiondatos = clsGestionDato.ObtenerInstancia();
            DataSet _dscomprobantepago;

            Hashtable _htparametros = new Hashtable();
            _htparametros.Add("@p_idempresa", intidempresa);
            _htparametros.Add("@p_idtipodocumento", intidtipodocumento);
            _htparametros.Add("@p_nrodocumento", strnrodocumento);

            _dscomprobantepago = _DAO.EjecutarComandoConjuntoEntidad("ingresoprovision", "obtenerpornrodocumento", _htparametros, "cabecera", "detalle", "convenio");

            if (_dscomprobantepago == null) { return null; }

            if (_dscomprobantepago.Tables["cabecera"].Rows.Count == 0)
            {
                throw new Exception("<<<No se encontró registro para Ingreso Provision.>>>");
            }

            if (_dscomprobantepago.Tables["convenio"].Rows.Count <= 0)
                _objcomprobantepago = new clsComprobantePago(_dscomprobantepago.Tables["cabecera"].Rows[0], _dscomprobantepago.Tables["detalle"], (DataRow)null);
            else
                _objcomprobantepago = new clsComprobantePago(_dscomprobantepago.Tables["cabecera"].Rows[0], _dscomprobantepago.Tables["detalle"], _dscomprobantepago.Tables["convenio"].Rows[0]);

            return _objcomprobantepago;
        }


        public SISAC.Entidad.Maestro.ClsEmpresa ObtenerRegistroEmpresa2(Int16 idempresa)
        {

            DataRow _drow;
            SISAC.Entidad.Maestro.ClsEmpresa _objempresa;
            Hashtable _htparametros = new Hashtable();

            _htparametros.Add("@idregistro", idempresa);
            _drow = _DAO.EjecutarComandoRegistro("empresa", "obtenerregistro", _htparametros);

            _objempresa = new SISAC.Entidad.Maestro.ClsEmpresa(_drow);

            return _objempresa;
        }

        #endregion generar PDF doc generar
    }


}