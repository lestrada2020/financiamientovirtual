﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SISAC.Util.Tool;

namespace SISAC.Data.Atencion
{

    #region clsInterrupcionDAO

    public sealed class clsInterrupcionDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsInterrupcionDAO> _Instancia = new Lazy<clsInterrupcionDAO>(() => new clsInterrupcionDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsInterrupcionDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsInterrupcionDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataTable ObtenerConfiguracionReporte(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerConfiguracionReporte", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public DataTable AyudaLista(clsParametro entidad)
        {
            try
            {
                String entity = "";
                String action = "AyudaLista";

                if (entidad.Parametros.ContainsKey("Entity")) entity = entidad.Parametros["Entity"];
                if (entidad.Parametros.ContainsKey("Action")) action = entidad.Parametros["Action"];

                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad(entity, action, param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable ObtenerMotivoFomatoNotifica(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.Motivo", "ObtenerMotivoFomatoNotifica", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerResumenxUUNNTiempo(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerResumenxUUNNTiempo", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerResumenxUUNNAreaTiempo(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerResumenxUUNNAreaTiempo", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerResumenxUUNNMotivoTiempo(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerResumenxUUNNMotivoTiempo", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerResumenxUUNNSEDTiempo(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerResumenxUUNNSEDTiempo", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDetalleSuministros(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerDetalleSuministros", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDetalleSEDs(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerDetalleSEDs", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GenerarTokenReporte(clsParametro entidad)
        {
            try
            {
                DataTable table;
                DataRow fila;
                Int32 idToken;

                Hashtable param = entidad.Parametros.ToHashtableDAO();
                idToken = _DAO.EjecutarComando("rpts.RptAtencion", "GenerarTokenReporte", param);

                idToken = Convert.ToInt32(param["@p_IdToken"]);
                               
                table = new DataTable();
                table.Columns.Add("IdToken", typeof(Int32));
                fila = table.NewRow();
                fila["IdToken"] = idToken;
                table.Rows.Add(fila);               

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerAtencionesxRecursoInterrupcion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "ObtenerAtencionesxRecursoInterrupcion", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable LimpiarTablaReporte(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("rpts.RptAtencion", "LimpiarTablaReporte", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        #endregion Public
    }

    #endregion clsInterrupcionDAO
}
