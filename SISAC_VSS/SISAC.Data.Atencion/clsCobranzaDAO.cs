﻿using SISAC.Data.DAO;
using SISAC.Entidad.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace SISAC.Data.Atencion
{
    public sealed class clsCobranzaDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsCobranzaDAO> _Instancia = new Lazy<clsCobranzaDAO>(() => new clsCobranzaDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsCobranzaDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsCobranzaDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataRow ValidarToken(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "ValidarToken", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow ConsultarDeuda(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "ConsultarDeuda", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConsultarDeudaDetalle(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("CobranzaVisa", "ConsultarDeudaDetalle", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow PagarDeuda(clsParametro entidad)
        {
            try
            {
                var idNumOrden = Convert.ToInt32(entidad.Parametros["NroPedido"]);
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "PagarDeuda", param);

                var transaccion = clsCobranzaNGCDAO.Instancia.RegistrarTransaccionPagoVisa(idNumOrden);

                transaccion = transaccion.ToDictionary().ToHashtableDAO();
                _DAO.EjecutarComandoRegistro("CobranzaVisa", "ActualizarIntentoPago", transaccion);

                return result;
            }
            catch (Exception ex)
            {
                #region Error

                var transaccion = new Hashtable();
                transaccion["NroPedido"] = entidad.Parametros["NroPedido"];
                transaccion["IdCobranzaExterna"] = 0;
                transaccion["NroTransaccion"] = 0;
                transaccion["NroTarjeta"] = "";
                transaccion["MensajeExcepcion"] = ex.Message;

                transaccion = transaccion.ToDictionary().ToHashtableDAO();
                _DAO.EjecutarComandoRegistro("CobranzaVisa", "ActualizarIntentoPago", transaccion);

                #endregion Error

                throw ex;
            }
        }

        public DataRow GrabarIntento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "GrabarIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow GrabarIntentoDetalle(clsParametro entidad)
        {
            

            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "GrabarIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow ActualizarIntento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "ActualizarIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow ObtenerIntento(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "ObtenerIntento", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow EnviarEmail(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("CobranzaVisa", "EnviarEmail", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Hashtable CobranzaOrdenVisaNET(clsParametro entidad)
        {
            Hashtable oResultado = new Hashtable();
            Hashtable oParametro = new Hashtable();
            clsOrdenVisaNet oVisaNETCabecera = new clsOrdenVisaNet();
            clsListaOrdenVisaNetDetalle oVisaNETDetalle = new clsListaOrdenVisaNetDetalle();

            clsUsuarioPuntoAtencion oUsuarioPuntoAtencion = new clsUsuarioPuntoAtencion();
            clsMediosPagoExterno oMediosPagoExterno = new clsMediosPagoExterno();
            clsCajaEstadisticaDiarioMoneda oCajaMoneda = new clsCajaEstadisticaDiarioMoneda();
            clsTransaccion oTransaccion = new clsTransaccion();

            List<clsDocumentosDescargoPropio> oDocumentosPropios = new List<clsDocumentosDescargoPropio>(); ;
            List<clsDocumentosDescargoBase> oDocumentosBase = new List<clsDocumentosDescargoBase>(); ;

            DateTime today = DateTime.Now;

            using (SqlConnection oConexion = _DAO.GetSqlConnection())
            {
                oConexion.Open();

                #region Obtener Datos de VISA NET

                var idNumOrden = Convert.ToInt32(entidad.Parametros["NroPedido"]);

                oParametro["@p_idnroorden"] = idNumOrden;
                DataRow oFilaCabecera = _DAO.EjecutarComandoRegistro("OrdenVisaNet", "ObtenerPorConciliar", oParametro, oConexion);
                oVisaNETCabecera = new clsOrdenVisaNet(oFilaCabecera);

                oParametro["@p_idnroorden"] = idNumOrden;
                DataTable tTablaDetalle = _DAO.EjecutarComandoEntidad("OrdenVisaNetDetalle", "Obtener", oParametro, oConexion);
                oVisaNETDetalle = new clsListaOrdenVisaNetDetalle(tTablaDetalle);

                #endregion

                #region Clases Generales

                #region PuntoAtencion

                oUsuarioPuntoAtencion.IdEmpresa = oVisaNETCabecera.IdEmpresa;
                oUsuarioPuntoAtencion.IdUnidadNegocio = oVisaNETCabecera.IdUUNN;
                oUsuarioPuntoAtencion.IdCentroServicio = Convert.ToInt16(oVisaNETCabecera.IdCentroServicio);
                oUsuarioPuntoAtencion.IdTipoAtencion = oVisaNETCabecera.IdTipoAtencion;
                oUsuarioPuntoAtencion.IdPuntoAtencion = Convert.ToInt16(oVisaNETCabecera.IdPuntoAtencion);
                oUsuarioPuntoAtencion.IdUsuario = oVisaNETCabecera.IdUsuario;

                #endregion PuntoAtencion

                #region Medios

                oMediosPagoExterno.OrdenDePago = 1;
                oMediosPagoExterno.NroTarjeta = oVisaNETCabecera.NroTarjeta;
                oMediosPagoExterno.NroDocumento = oVisaNETCabecera.eTicket;
                oMediosPagoExterno.DescripcionTarjeta = oVisaNETCabecera.eTicket;
                oMediosPagoExterno.EnviaABoveda = false;
                oMediosPagoExterno.EsIngreso = 1;
                oMediosPagoExterno.EsEfectivo = false;
                oMediosPagoExterno.EsCheque = false;
                oMediosPagoExterno.FechaDocumento = today;
                oMediosPagoExterno.GeneraVuelto = false;
                oMediosPagoExterno.IdTipoDocumento = (Int16)TipoDocumentoComercial.TarjetaCredito;
                oMediosPagoExterno.IdTipoTarjeta = (Int16)3;//Tarjeta visa// enumTipoTarjeta.Visa;
                oMediosPagoExterno.ImporteCambioTotal = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oMediosPagoExterno.ImporteOrigenTotal = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oMediosPagoExterno.TipoCambio = (Decimal)1;
                oMediosPagoExterno.IdMonedaOrigen = (Int16)Moneda.Soles;
                oMediosPagoExterno.IdMonedaCambio = (Int16)Moneda.Soles;

                #endregion Medios

                #region CajaMoneda

                oCajaMoneda.IdMoneda = (Int16)Moneda.Soles;
                oCajaMoneda.IdMonedaCambio = (Int16)Moneda.Soles;
                oCajaMoneda.IdNroCaja = 0;
                oCajaMoneda.IdTipoDocumentoComercial = (Int16)TipoDocumentoComercial.TarjetaCredito;
                oCajaMoneda.Importe = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.ImporteBovedaCambio = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.ImporteBoveda = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.ImporteDescargo = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oCajaMoneda.NroTransaccion = 0;
                oCajaMoneda.Seleccionar = true;
                oCajaMoneda.TipoCambio = (Decimal)1;

                #endregion CajaMoneda

                #region ListaDocumentosBase

                oParametro["@p_idnroorden"] = idNumOrden;
                DataTable oDocumentos = _DAO.EjecutarComandoEntidad("VisaNet", "ObtenerDocumentosPorConciliar", oParametro, oConexion);

                foreach (DataRow oItemFila in oDocumentos.Rows)
                {
                    clsDocumentosDescargoPropio oDocumentoPropio = new clsDocumentosDescargoPropio();
                    oDocumentoPropio.ImporteOrigenDescargo = Convert.ToDecimal(oItemFila["Importe"]);
                    oDocumentoPropio.ImporteOrigenDeuda = Convert.ToDecimal(oItemFila["Importe"]);
                    oDocumentoPropio.IdEmpresa = oVisaNETCabecera.IdEmpresa;
                    oDocumentoPropio.IdTipoDocumento = Convert.ToInt16(oItemFila["IdTipoDocumento"]);
                    oDocumentoPropio.NroDocumento = oItemFila["NroDocumento"].ToString();
                    oDocumentoPropio.IdTipoDocumentoSustento = Convert.ToInt16(oItemFila["IdTipoDocumentoSustento"]);
                    oDocumentoPropio.NroDocumentoSustento = oItemFila["NroDocumentoSustento"].ToString();
                    oDocumentoPropio.IdServicio = Convert.ToInt16(oItemFila["idServicio"]);
                    oDocumentoPropio.OrdenPagoServicio = Convert.ToInt32(oItemFila["OrdendePago"]);
                    oDocumentoPropio.IdNroServicio = oVisaNETCabecera.IdNroServicio;

                    oDocumentosBase.Add(oDocumentoPropio);
                }

                #endregion ListaDocumentosBase

                #region Transaccion

                oTransaccion.FechaPago = today;
                oTransaccion.FechaRegistro = today;
                oTransaccion.IdMovComercial = (Int16)enumMovimientoComercial.Cobranza;
                oTransaccion.NroTransaccion = 0;
                oTransaccion.IdMonedaNacional = (Int16)Moneda.Soles;
                oTransaccion.ImporteNeto = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.TipoOperacion = TipoOperacionCobranza.Recaudacion;
                oTransaccion.IdEstado = (Int16)Estado.Activo;
                oTransaccion.IdNroServicio = oVisaNETCabecera.IdNroServicio;
                oTransaccion.ImporteEfectivo = 0;
                oTransaccion.ImporteTotalVuelto = 0;
                oTransaccion.ImporteNeto = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.ImporteRecibido = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.ImporteValores = Convert.ToDecimal(oVisaNETCabecera.Importe);
                oTransaccion.UsuarioPuntoAtencion = oUsuarioPuntoAtencion;
                oTransaccion.ElementosDocumentoDescargo = oDocumentosBase;
                oTransaccion.ElementosMedioPago = new List<clsMediosPagoBase>();
                oTransaccion.ElementosMedioPago.Add(oMediosPagoExterno);
                oTransaccion.ElementosCajaServicioMoneda = new List<clsCajaEstadisticaDiarioMoneda>();
                oTransaccion.ElementosCajaServicioMoneda.Add(oCajaMoneda);

                #endregion Transaccion

                #endregion

                SqlTransaction oTransaccionSQL = oConexion.BeginTransaction(IsolationLevel.ReadUncommitted);

                try
                {
                    #region Variables Locales

                    Int16 _intpositivonegativo = 1;
                    Int32 _intidtransaccionlog = 0;
                    Int32 _nrodocumentos = 0;
                    Int32 _nroservicios = 0;
                    Int32 _idnroservicioppago = 0;
                    Hashtable htDepositos = new Hashtable();

                    Int32 periodoPago = oTransaccion.PeriodoPago;

                    #endregion Variables Locales

                    #region Validaciones previas

                    if (oTransaccion.ElementosDocumentoDescargo.Count == 0)
                        throw new Exception("<<<Ingrese Documentos de Descargo.>>>");

                    if (oTransaccion.ElementosMedioPago.Count == 0)
                        throw new Exception("<<<Ingrese forma de Pago.>>>");

                    if (oTransaccion.IdMonedaNacional <= 0)
                        throw new Exception("<<<El Tipo de Moneda Nacional no se ha especificado para está transacción " + oTransaccion.IdMonedaNacional + ".>>>");

                    if (oTransaccion.ImporteNeto == 0)
                    {
                        StringBuilder _sbmensaje = new StringBuilder();
                        _sbmensaje.AppendLine("<<<Importe Neto de la Transacción es Cero.");
                        _sbmensaje.AppendLine("No se Aplico las Formas de Pago a los Documentos Cargados.>>>");

                        throw new Exception(_sbmensaje.ToString());
                    }

                    #endregion Validaciones previas

                    #region Verificar Caja Aperturada

                    if (oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo == 0)
                    {
                        Int32 IntCantidadPuntosAbiertos = clsCobranzaNGCDAO.Instancia.CantidadAbiertos(oTransaccion.UsuarioPuntoAtencion.IdUsuario);

                        if (IntCantidadPuntosAbiertos == 0)
                        {
                            clsCajaEstadisticaDiario oCajaAperturar = new clsCajaEstadisticaDiario();
                            oCajaAperturar.UsuarioPuntoAtencion = oTransaccion.UsuarioPuntoAtencion;
                            oCajaAperturar.IdNroCaja = 0;
                            oCajaAperturar.FechaApertura = DateTime.Now;
                            oCajaAperturar.FechaPago = DateTime.Now;

                            if (clsCobranzaNGCDAO.Instancia.AperturarFechaPago(oTransaccionSQL, oCajaAperturar, Decimal.Zero))
                            {
                                oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaAperturar.FechaApertura;
                                oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaAperturar.FechaPago;
                                oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaAperturar.IdNroCaja;
                            }
                            else
                                throw new Exception("<<<No se logró realizar la operación de cobranza.>>>");
                        }
                        else if (IntCantidadPuntosAbiertos == 1)
                        {
                            clsCajaEstadisticaDiario oCajaActual = clsCobranzaNGCDAO.Instancia.ObtenerCajaAtencionUltima(oTransaccion.UsuarioPuntoAtencion.IdEmpresa
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdUnidadNegocio
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdCentroServicio
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdTipoAtencion
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdPuntoAtencion
                                                                                            , oTransaccion.UsuarioPuntoAtencion.IdUsuario,
                                                                                            (DateTime?)null);

                            if (oCajaActual != null)
                            {
                                DateTime datFechaServidor = DateTime.Now;

                                if (oCajaActual.FechaApertura.Date.CompareTo(datFechaServidor.Date) == 0)
                                {
                                    oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaActual.FechaApertura;
                                    oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaActual.FechaPago;
                                    oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaActual.IdNroCaja;
                                }
                            }
                        }
                    }

                    #endregion Verificar Caja Aperturada

                    #region Grabando en Transaccion

                    DateTime fInicio = DateTime.Now;

                    oTransaccion.IdTransaccionLog = _intidtransaccionlog;
                    oTransaccion.IdTipoDocumento = 4;
                    oTransaccion.IdEstado = 1;
                    oTransaccion.ImporteNeto *= _intpositivonegativo;
                    oTransaccion.ImporteRecibido *= _intpositivonegativo;
                    oTransaccion.ImporteTotalVuelto *= _intpositivonegativo;
                    oTransaccion.ImporteEfectivo *= _intpositivonegativo;
                    oTransaccion.ImporteValores *= _intpositivonegativo;

                    oTransaccion.NroTransaccion = clsCobranzaNGCDAO.Instancia.TransaccionInsertar(oTransaccionSQL, oTransaccion);

                    if (oTransaccion.NroTransaccion <= 0)
                        throw new Exception("<<<No se registro en Transaccion.>>>");

                    #endregion Grabando en Transaccion

                    #region Grabando en Transaccion Documentos Descargo

                    _idnroservicioppago = 0;

                    foreach (clsDocumentosDescargoBase itemdocumentodescargo in oTransaccion.ElementosDocumentoDescargo)
                    {
                        clsDocumentosDescargoPropio itemdocumentodescargopropio = null;
                        itemdocumentodescargo.EsTerceros = false;

                        itemdocumentodescargopropio = (clsDocumentosDescargoPropio)itemdocumentodescargo;

                        if (!_idnroservicioppago.Equals(itemdocumentodescargopropio.IdNroServicio))
                        {
                            _idnroservicioppago = itemdocumentodescargopropio.IdNroServicio;

                            oTransaccion.PeriodoPago = clsCobranzaNGCDAO.Instancia.ObtenerPeriodoPago(_idnroservicioppago);
                            itemdocumentodescargopropio.Periodo = oTransaccion.PeriodoPago;

                            if (oTransaccion.PeriodoPago == 0)
                            {
                                oTransaccion.PeriodoPago = periodoPago;
                                itemdocumentodescargopropio.Periodo = periodoPago;
                            }
                        }

                        if (itemdocumentodescargopropio.ImporteOrigenDescargo != 0)
                        {
                            itemdocumentodescargopropio.NroTransaccion = oTransaccion.NroTransaccion;
                            itemdocumentodescargopropio.ImporteOrigenCobrar *= _intpositivonegativo;
                            itemdocumentodescargopropio.ImporteOrigenDescargo *= _intpositivonegativo;
                            itemdocumentodescargopropio.ImporteOrigenDeuda *= _intpositivonegativo;

                            clsCobranzaNGCDAO.Instancia.Insertar(oTransaccionSQL
                                    , oTransaccion
                                    , itemdocumentodescargopropio
                                    , oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                        }
                    }

                    #endregion Grabando en Transaccion Documentos Descargo

                    #region Grabando en Transaccion Formas de Pago

                    foreach (clsMediosPagoBase itemmediopago in oTransaccion.ElementosMedioPago)
                    {
                        itemmediopago.NroTransaccion = oTransaccion.NroTransaccion;
                        itemmediopago.ImporteCambioTotal *= _intpositivonegativo;
                        itemmediopago.ImporteOrigenTotal *= _intpositivonegativo;
                        itemmediopago.ImporteCambioDescargo *= _intpositivonegativo;
                        itemmediopago.ImporteOrigenDescargo *= _intpositivonegativo;

                        clsMediosPagoExterno itemmediopagoexterno = (clsMediosPagoExterno)itemmediopago;

                        itemmediopagoexterno.EsIngreso = 1;

                        if (itemmediopagoexterno.NroDocumento == "[Vuelto]")
                            itemmediopagoexterno.EsIngreso = 0;

                        clsCobranzaNGCDAO.Instancia.InsertarMedioDePago(oTransaccionSQL, itemmediopagoexterno, oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                    }

                    #endregion Grabando en Transaccion Formas de Pago

                    #region Actualizando Control de Caja

                    foreach (clsCajaEstadisticaDiarioMoneda item in oTransaccion.ElementosCajaServicioMoneda)
                    {
                        item.ImporteCambio *= _intpositivonegativo;
                        item.Importe *= _intpositivonegativo;
                        item.IdNroCaja = oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo;

                        clsCobranzaNGCDAO.Instancia.InsertarCaja(oTransaccionSQL, item);
                    }

                    clsCobranzaNGCDAO.Instancia.ActualizarCaja(oTransaccionSQL, oTransaccion.NroTransaccion, _nrodocumentos, _nroservicios);

                    oTransaccionSQL.Commit();

                    #endregion Actualizando Control de Caja

                    #region Resultado
                    if (oTransaccion.NroTransaccion > 0)
                    {
                        oResultado = new Hashtable();
                        oResultado["NroPedido"] = entidad.Parametros["NroPedido"];
                        oResultado["NroTransaccion"] = oTransaccion.NroTransaccion;
                        oResultado["NroTarjeta"] = "";
                        oResultado["MensajeExcepcion"] = "Sin mensaje";

                        oResultado["Mensaje"] = "Operación de Pago con Éxito.";
                        oResultado["IdError"] = 1;
                    }
                    else
                    {
                        oResultado = new Hashtable();
                        oResultado["NroPedido"] = entidad.Parametros["NroPedido"];
                        oResultado["NroTransaccion"] = 0;
                        oResultado["NroTarjeta"] = "";
                        oResultado["MensajeExcepcion"] = "Sin mensaje";

                        oResultado["Mensaje"] = "Operación de Pago Sin Éxito.";
                        oResultado["IdError"] = 0;
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    #region Error

                    oTransaccionSQL.Rollback();

                    oResultado = new Hashtable();
                    oResultado["NroPedido"] = entidad.Parametros["NroPedido"];
                    oResultado["NroTransaccion"] = 0;
                    oResultado["NroTarjeta"] = "";
                    oResultado["IdCobranzaExterna"] = 0;
                    oResultado["MensajeExcepcion"] = ex.Message;

                    oResultado["Mensaje"] = "Error en el proceso de Cobranza";
                    oResultado["IdError"] = 0;

                    oResultado = oResultado.ToDictionary().ToHashtableDAO();

                    _DAO.EjecutarComandoRegistro("CobranzaVisa", "ActualizarIntentoPago", oResultado);

                    #endregion Error
                }
                finally
                {
                    if (oConexion.State == ConnectionState.Open)
                        oConexion.Close();
                }
            }

            return oResultado;
        }
        #endregion Public
    }

    #region clsCobranzaNGCDAO

    public sealed class clsCobranzaNGCDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsCobranzaNGCDAO> _Instancia = new Lazy<clsCobranzaNGCDAO>(() => new clsCobranzaNGCDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsCobranzaNGCDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsCobranzaNGCDAO()
        {
        }

        #endregion Constructor

        #region Public

        public Hashtable RegistrarTransaccionPagoVisa(Int32 idNumOrden)
        {
            var result = new Hashtable();
            var param = new Hashtable();
            var ordenVisaNet = (clsOrdenVisaNet)null;
            var ordenVisaNetDetalle = (clsListaOrdenVisaNetDetalle)null;
            var idCobranzaExterna = 0;
            var idTransaccion = 0;
            var table = (DataTable)null;
            var row = (DataRow)null;
            var transaction = (SqlTransaction)null;
            var documentosPropios = new List<clsDocumentosDescargoPropio>(); ;
            var documentosBase = new List<clsDocumentosDescargoBase>(); ;
            var documentoPropio = (clsDocumentosDescargoPropio)null;
            var usuarioPuntoAtencion = new clsUsuarioPuntoAtencion();
            var mediosPagoExterno = new clsMediosPagoExterno();
            var cajaMoneda = new clsCajaEstadisticaDiarioMoneda();
            var transaccion = new clsTransaccion();
            var today = DateTime.Now;

            try
            {
                using (SqlConnection osqlcon = _DAO.GetSqlConnection())
                {
                    osqlcon.Open();
                    transaction = osqlcon.BeginTransaction();

                    #region OrdenVisaNet

                    param["@p_idnroorden"] = idNumOrden;
                    row = _DAO.EjecutarComandoRegistro("OrdenVisaNet", "Obtener", param);
                    ordenVisaNet = new clsOrdenVisaNet(row);

                    param["@p_idnroorden"] = idNumOrden;
                    table = _DAO.EjecutarComandoEntidad("OrdenVisaNetDetalle", "Obtener", param);
                    ordenVisaNetDetalle = new clsListaOrdenVisaNetDetalle(table);

                    #endregion OrdenVisaNet

                    try
                    {
                        if (String.IsNullOrEmpty(ordenVisaNet.NroTarjeta)) ordenVisaNet.NroTarjeta = "***";
                    }
                    catch (Exception ex){}

                    try
                    {
                        #region TransaccionExterna

                        param["@p_idcobranzaexterna"] = 0;
                        param["@p_idnrocaja"] = 0;
                        param["@p_idempresa"] = ordenVisaNet.IdEmpresa;
                        param["@p_importetotal"] = ordenVisaNet.Importe;
                        _DAO.EjecutarComando("TransaccionCobranzaExterna", "visanetinsertar", param, transaction);
                        idCobranzaExterna = Convert.ToInt32(param["@p_idcobranzaexterna"]);

                        param["@p_idtransaccion"] = 0;
                        param["@p_idcobranzaexterna"] = idCobranzaExterna;
                        param["@p_idnroservicio"] = ordenVisaNet.IdNroServicio;
                        param["@p_importetotal"] = ordenVisaNet.Importe;
                        param["@p_eticket"] = ordenVisaNet.eTicket;
                        _DAO.EjecutarComando("TransaccionExternaNroServicio", "visanetinsertar", param, transaction);
                        idTransaccion = Convert.ToInt32(param["@p_idtransaccion"]);

                        foreach (clsOrdenVisaNetDetalle item in ordenVisaNetDetalle.Elementos)
                        {
                            param["@p_idtransaccion"] = idTransaccion;
                            param["@p_idempresa"] = ordenVisaNet.IdEmpresa;
                            param["@p_idtipodocumento"] = item.IdTipoDocumento;
                            param["@p_nrodocumento"] = item.NroDocumento;
                            param["@p_periodopago"] = item.Periodo;
                            param["@p_saldo"] = item.Saldo;
                            _DAO.EjecutarComando("TransaccionExternaDocumentos", "visanetinsertar", param, transaction);
                        }

                        param["@p_idtransaccion"] = idTransaccion;
                        param["@p_parametroxml"] = ordenVisaNet.Trama;
                        _DAO.EjecutarComando("TransaccionExternaParametros", "visanetinsertar", param, transaction);

                        #endregion TransaccionExterna

                        #region PuntoAtencion

                        usuarioPuntoAtencion.IdEmpresa = ordenVisaNet.IdEmpresa;
                        usuarioPuntoAtencion.IdUnidadNegocio = ordenVisaNet.IdUUNN;
                        usuarioPuntoAtencion.IdCentroServicio = Convert.ToInt16(ordenVisaNet.IdCentroServicio);
                        usuarioPuntoAtencion.IdTipoAtencion = ordenVisaNet.IdTipoAtencion;
                        usuarioPuntoAtencion.IdPuntoAtencion = Convert.ToInt16(ordenVisaNet.IdPuntoAtencion);
                        usuarioPuntoAtencion.IdUsuario = ordenVisaNet.IdUsuario;

                        #endregion PuntoAtencion

                        #region Medios

                        mediosPagoExterno.OrdenDePago = 1;
                        mediosPagoExterno.NroTarjeta = ordenVisaNet.NroTarjeta;
                        mediosPagoExterno.NroDocumento = ordenVisaNet.eTicket;
                        mediosPagoExterno.DescripcionTarjeta = ordenVisaNet.eTicket;
                        mediosPagoExterno.EnviaABoveda = false;
                        mediosPagoExterno.EsIngreso = 1;
                        mediosPagoExterno.EsEfectivo = false;
                        mediosPagoExterno.EsCheque = false;
                        mediosPagoExterno.FechaDocumento = today;
                        mediosPagoExterno.GeneraVuelto = false;
                        mediosPagoExterno.IdTipoDocumento = (Int16)TipoDocumentoComercial.TarjetaCredito;
                        mediosPagoExterno.IdTipoTarjeta = (Int16)3;//Tarjeta visa// enumTipoTarjeta.Visa;
                        mediosPagoExterno.ImporteCambioTotal = Convert.ToDecimal(ordenVisaNet.Importe);
                        mediosPagoExterno.ImporteOrigenTotal = Convert.ToDecimal(ordenVisaNet.Importe);
                        mediosPagoExterno.TipoCambio = (Decimal)1;
                        mediosPagoExterno.IdMonedaOrigen = (Int16)Moneda.Soles;
                        mediosPagoExterno.IdMonedaCambio = (Int16)Moneda.Soles;

                        #endregion Medios

                        #region CajaMoneda

                        cajaMoneda.IdMoneda = (Int16)Moneda.Soles;
                        cajaMoneda.IdMonedaCambio = (Int16)Moneda.Soles;
                        cajaMoneda.IdNroCaja = 0;
                        cajaMoneda.IdTipoDocumentoComercial = (Int16)TipoDocumentoComercial.TarjetaCredito;
                        cajaMoneda.Importe = Convert.ToDecimal(ordenVisaNet.Importe);
                        cajaMoneda.ImporteBovedaCambio = Convert.ToDecimal(ordenVisaNet.Importe);
                        cajaMoneda.ImporteBoveda = Convert.ToDecimal(ordenVisaNet.Importe);
                        cajaMoneda.ImporteDescargo = Convert.ToDecimal(ordenVisaNet.Importe);
                        cajaMoneda.NroTransaccion = 0;
                        cajaMoneda.Seleccionar = true;
                        cajaMoneda.TipoCambio = (Decimal)1;

                        #endregion CajaMoneda

                        #region ListaDocumentosBase

                        param["@p_idnroorden"] = idNumOrden;
                        table = _DAO.EjecutarComandoEntidad("VisaNet", "ObtenerDocumentos", param);

                        foreach (DataRow item in table.Rows)
                        {
                            documentoPropio = new clsDocumentosDescargoPropio();
                            documentoPropio.ImporteOrigenDescargo = Convert.ToDecimal(item["Importe"]);
                            documentoPropio.ImporteOrigenDeuda = Convert.ToDecimal(item["Importe"]);
                            documentoPropio.IdEmpresa = ordenVisaNet.IdEmpresa;
                            documentoPropio.IdTipoDocumento = Convert.ToInt16(item["IdTipoDocumento"]);
                            documentoPropio.NroDocumento = item["NroDocumento"].ToString();
                            documentoPropio.IdTipoDocumentoSustento = Convert.ToInt16(item["IdTipoDocumentoSustento"]);
                            documentoPropio.NroDocumentoSustento = item["NroDocumentoSustento"].ToString();
                            documentoPropio.IdServicio = Convert.ToInt16(item["idServicio"]);
                            documentoPropio.OrdenPagoServicio = Convert.ToInt32(item["OrdendePago"]);
                            documentoPropio.IdNroServicio = ordenVisaNet.IdNroServicio;

                            documentosBase.Add(documentoPropio);
                        }

                        #endregion ListaDocumentosBase

                        #region Transaccion

                        transaccion.FechaPago = today;
                        transaccion.FechaRegistro = today;
                        transaccion.IdMovComercial = (Int16)enumMovimientoComercial.Cobranza;
                        transaccion.NroTransaccion = 0;
                        transaccion.IdMonedaNacional = (Int16)Moneda.Soles;
                        transaccion.ImporteNeto = Convert.ToDecimal(ordenVisaNet.Importe);
                        transaccion.TipoOperacion = TipoOperacionCobranza.Recaudacion;
                        transaccion.IdEstado = (Int16)Estado.Activo;
                        transaccion.IdNroServicio = ordenVisaNet.IdNroServicio;
                        transaccion.ImporteEfectivo = 0;
                        transaccion.ImporteTotalVuelto = 0;
                        transaccion.ImporteNeto = Convert.ToDecimal(ordenVisaNet.Importe);
                        transaccion.ImporteRecibido = Convert.ToDecimal(ordenVisaNet.Importe);
                        transaccion.ImporteValores = Convert.ToDecimal(ordenVisaNet.Importe);
                        transaccion.UsuarioPuntoAtencion = usuarioPuntoAtencion;
                        transaccion.ElementosDocumentoDescargo = documentosBase;
                        transaccion.ElementosMedioPago = new List<clsMediosPagoBase>();
                        transaccion.ElementosMedioPago.Add(mediosPagoExterno);
                        transaccion.ElementosCajaServicioMoneda = new List<clsCajaEstadisticaDiarioMoneda>();
                        transaccion.ElementosCajaServicioMoneda.Add(cajaMoneda);

                        #endregion Transaccion

                        #region Result

                        result["NroPedido"] = idNumOrden;
                        result["IdCobranzaExterna"] = idCobranzaExterna;
                        result["NroTransaccion"] = idTransaccion;
                        result["NroTarjeta"] = ordenVisaNet.NroTarjeta;
                        result["MensajeExcepcion"] = "";

                        #endregion Result

                        result["NroTransaccion"] = GrabarCobranzaMejoradaSedalib(transaccion, transaction);

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();

                        throw ex;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 GrabarCobranzaMejoradaSedalib(clsTransaccion oTransaccion, SqlTransaction sqTransaccion)
        {
            try
            {
                #region Variables Locales

                Int16 _intpositivonegativo = 1;
                Int32 _intidtransaccionlog = 0;
                Int32 _nrodocumentos = 0;
                Int32 _nroservicios = 0;
                Int32 _idnroservicioppago = 0;
                Hashtable htDepositos = new Hashtable();

                Int32 periodoPago = oTransaccion.PeriodoPago;

                #endregion Variables Locales

                #region Validaciones previas

                if (oTransaccion.ElementosDocumentoDescargo.Count == 0)
                    throw new Exception("<<<Ingrese Documentos de Descargo.>>>");

                if (oTransaccion.ElementosMedioPago.Count == 0)
                    throw new Exception("<<<Ingrese forma de Pago.>>>");

                if (oTransaccion.IdMonedaNacional <= 0)
                    throw new Exception("<<<El Tipo de Moneda Nacional no se ha especificado para está transacción " + oTransaccion.IdMonedaNacional + ".>>>");

                if (oTransaccion.ImporteNeto == 0)
                {
                    StringBuilder _sbmensaje = new StringBuilder();
                    _sbmensaje.AppendLine("<<<Importe Neto de la Transacción es Cero.");
                    _sbmensaje.AppendLine("No se Aplico las Formas de Pago a los Documentos Cargados.>>>");

                    throw new Exception(_sbmensaje.ToString());
                }

                //if (!oGestionarCaja.EsCajaHabilitada(oTransaccion.UsuarioPuntoAtencion))
                //    throw new Exception("<<<Caja No Esta Habilitada Para Cobranza.>>>");

                #endregion Validaciones previas

                #region Verificar Caja Aperturada

                if (oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo == 0)
                {
                    Int32 IntCantidadPuntosAbiertos = CantidadAbiertos(oTransaccion.UsuarioPuntoAtencion.IdUsuario);

                    if (IntCantidadPuntosAbiertos == 0)
                    {
                        clsCajaEstadisticaDiario oCajaAperturar = new clsCajaEstadisticaDiario();
                        oCajaAperturar.UsuarioPuntoAtencion = oTransaccion.UsuarioPuntoAtencion;
                        oCajaAperturar.IdNroCaja = 0;
                        oCajaAperturar.FechaApertura = DateTime.Now;
                        oCajaAperturar.FechaPago = DateTime.Now;

                        if (AperturarFechaPago(sqTransaccion, oCajaAperturar, Decimal.Zero))
                        {
                            oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaAperturar.FechaApertura;
                            oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaAperturar.FechaPago;
                            oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaAperturar.IdNroCaja;
                        }
                        else
                            throw new Exception("<<<No se logró realizar la operación de cobranza.>>>");
                    }
                    else if (IntCantidadPuntosAbiertos == 1)
                    {
                        clsCajaEstadisticaDiario oCajaActual = ObtenerCajaAtencionUltima(oTransaccion.UsuarioPuntoAtencion.IdEmpresa
                                                                                        , oTransaccion.UsuarioPuntoAtencion.IdUnidadNegocio
                                                                                        , oTransaccion.UsuarioPuntoAtencion.IdCentroServicio
                                                                                        , oTransaccion.UsuarioPuntoAtencion.IdTipoAtencion
                                                                                        , oTransaccion.UsuarioPuntoAtencion.IdPuntoAtencion
                                                                                        , oTransaccion.UsuarioPuntoAtencion.IdUsuario,
                                                                                        (DateTime?)null);

                        if (oCajaActual != null)
                        {
                            DateTime datFechaServidor = DateTime.Now;

                            if (oCajaActual.FechaApertura.Date.CompareTo(datFechaServidor.Date) == 0)
                            {
                                oTransaccion.UsuarioPuntoAtencion.FechaApertura = oCajaActual.FechaApertura;
                                oTransaccion.UsuarioPuntoAtencion.FechaPago = oCajaActual.FechaPago;
                                oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo = oCajaActual.IdNroCaja;
                            }
                            else
                            {
                                //Forzar Cierre y Aperturar nueva Caja.
                            }
                        }
                    }
                }

                #endregion Verificar Caja Aperturada

                #region Grabando en Transaccion

                DateTime fInicio = DateTime.Now;

                oTransaccion.IdTransaccionLog = _intidtransaccionlog;
                oTransaccion.IdTipoDocumento = 4;
                oTransaccion.IdEstado = 1;
                oTransaccion.ImporteNeto *= _intpositivonegativo;
                oTransaccion.ImporteRecibido *= _intpositivonegativo;
                oTransaccion.ImporteTotalVuelto *= _intpositivonegativo;
                oTransaccion.ImporteEfectivo *= _intpositivonegativo;
                oTransaccion.ImporteValores *= _intpositivonegativo;

                oTransaccion.NroTransaccion = TransaccionInsertar(sqTransaccion, oTransaccion);

                if (oTransaccion.NroTransaccion <= 0)
                    throw new Exception("<<<No se registro en Transaccion.>>>");

                #endregion Grabando en Transaccion

                #region Grabando en Transaccion Documentos Descargo

                _idnroservicioppago = 0;

                foreach (clsDocumentosDescargoBase itemdocumentodescargo in oTransaccion.ElementosDocumentoDescargo)
                {
                    clsDocumentosDescargoPropio itemdocumentodescargopropio = null;
                    itemdocumentodescargo.EsTerceros = false;

                    itemdocumentodescargopropio = (clsDocumentosDescargoPropio)itemdocumentodescargo;

                    if (!_idnroservicioppago.Equals(itemdocumentodescargopropio.IdNroServicio))
                    {
                        _idnroservicioppago = itemdocumentodescargopropio.IdNroServicio;

                        oTransaccion.PeriodoPago = ObtenerPeriodoPago(_idnroservicioppago);
                        itemdocumentodescargopropio.Periodo = oTransaccion.PeriodoPago;

                        if (oTransaccion.PeriodoPago == 0)
                        {
                            oTransaccion.PeriodoPago = periodoPago;
                            itemdocumentodescargopropio.Periodo = periodoPago;
                        }
                    }

                    if (itemdocumentodescargopropio.ImporteOrigenDescargo != 0)
                    {
                        itemdocumentodescargopropio.NroTransaccion = oTransaccion.NroTransaccion;
                        itemdocumentodescargopropio.ImporteOrigenCobrar *= _intpositivonegativo;
                        itemdocumentodescargopropio.ImporteOrigenDescargo *= _intpositivonegativo;
                        itemdocumentodescargopropio.ImporteOrigenDeuda *= _intpositivonegativo;

                        Insertar(sqTransaccion
                                , oTransaccion
                                , itemdocumentodescargopropio
                                , oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                    }
                }

                #endregion Grabando en Transaccion Documentos Descargo

                #region Grabando en Transaccion Formas de Pago

                foreach (clsMediosPagoBase itemmediopago in oTransaccion.ElementosMedioPago)
                {
                    itemmediopago.NroTransaccion = oTransaccion.NroTransaccion;
                    itemmediopago.ImporteCambioTotal *= _intpositivonegativo;
                    itemmediopago.ImporteOrigenTotal *= _intpositivonegativo;
                    itemmediopago.ImporteCambioDescargo *= _intpositivonegativo;
                    itemmediopago.ImporteOrigenDescargo *= _intpositivonegativo;

                    clsMediosPagoExterno itemmediopagoexterno = (clsMediosPagoExterno)itemmediopago;

                    itemmediopagoexterno.EsIngreso = 1;

                    if (itemmediopagoexterno.NroDocumento == "[Vuelto]")
                        itemmediopagoexterno.EsIngreso = 0;

                    InsertarMedioDePago(sqTransaccion, itemmediopagoexterno, oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                }

                #endregion Grabando en Transaccion Formas de Pago

                #region Actualizando Saldos del Cliente

                //TransaccionActualizarSaldos(sqTransaccion
                //                            , oTransaccion.NroTransaccion);

                #endregion Actualizando Saldos del Cliente

                #region Actualizando Control de Caja

                foreach (clsCajaEstadisticaDiarioMoneda item in oTransaccion.ElementosCajaServicioMoneda)
                {
                    item.ImporteCambio *= _intpositivonegativo;
                    item.Importe *= _intpositivonegativo;
                    item.IdNroCaja = oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo;

                    InsertarCaja(sqTransaccion, item);
                }

                //oCajaServicioDAO.Registrar(sqTransaccion, oTransaccion, ref _nroservicios, ref _nrodocumentos, CacheConfiguracion.IdOperacionCobranza);
                ActualizarCaja(sqTransaccion, oTransaccion.NroTransaccion, _nrodocumentos, _nroservicios);

                #endregion Actualizando Control de Caja

                return oTransaccion.NroTransaccion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Verificar Caja Aperturada

        public Int32 CantidadAbiertos(Int32 IdUsuario)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();
                _htparametros.Add("@idusuario", IdUsuario);

                Int32 NroPuntosAbiertoPorUsuario = 0;
                Object resultado = _DAO.EjecutarComandoEscalar("UsuarioPuntoAtencion", "CantidadAbiertos", _htparametros);

                if (resultado != null) NroPuntosAbiertoPorUsuario = (Int32)resultado;

                return NroPuntosAbiertoPorUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean AperturarFechaPago(SqlTransaction objtransaccion, clsCajaEstadisticaDiario caja, Decimal saldoinicial)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@idnrocaja", null);
                _htparametros.Add("@fechaapertura", null);
                _htparametros.Add("@idempresacajero", caja.UsuarioPuntoAtencion.IdEmpresa);
                _htparametros.Add("@iduunncajero", caja.UsuarioPuntoAtencion.IdUnidadNegocio);
                _htparametros.Add("@idcentroserviciocajero", caja.UsuarioPuntoAtencion.IdCentroServicio);
                _htparametros.Add("@idtipoatencion", caja.UsuarioPuntoAtencion.IdTipoAtencion);
                _htparametros.Add("@idpuntoatencion", caja.UsuarioPuntoAtencion.IdPuntoAtencion);
                _htparametros.Add("@idusuario", caja.UsuarioPuntoAtencion.IdUsuario);
                _htparametros.Add("@fechapago", caja.FechaPago);
                _htparametros.Add("@fechaini", caja.FechaPago.Date);
                _htparametros.Add("@fechafin", caja.FechaPago.AddDays(1).Date);

                _DAO.EjecutarComando("cajaestadisticadiario", "abrirfechapago", _htparametros, objtransaccion);

                caja.IdNroCaja = (Int32)_htparametros["@idnrocaja"];
                caja.FechaApertura = (DateTime)_htparametros["@fechaapertura"];

                return (caja.IdNroCaja > 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public clsCajaEstadisticaDiario ObtenerCajaAtencionUltima(Int16 IdEmpresa
                                                               , Int16 IdUUNN
                                                               , Int16 IdCCSS
                                                               , Int16 IdTipoAtencion
                                                               , Int16 IdPuntoAtencion
                                                               , Int32 IdUsuario
                                                               , DateTime? fechaApertura)
        {
            try
            {
                clsCajaEstadisticaDiario objcajas;

                objcajas = Buscar(IdEmpresa, IdUUNN, IdCCSS, IdTipoAtencion, IdPuntoAtencion, IdUsuario, fechaApertura);

                if (objcajas == null) { return objcajas; }

                if (objcajas.FechaCierre != null && ((Estado)objcajas.UsuarioPuntoAtencion.IdEstadoCobranza != Estado.Cerrado))
                {
                    StringBuilder strMensaje = new StringBuilder();
                    strMensaje.AppendLine("<<<Error en inconsistencia de datos");
                    strMensaje.AppendLine("tiene el punto de atencion en estado " + ((Estado)objcajas.UsuarioPuntoAtencion.IdEstadoCobranza).ToString() + " y tiene asignado la FechaCierre " + objcajas.FechaCierre.ToString() + ".>>>");

                    throw new Exception(strMensaje.ToString());
                }

                objcajas.CantidadPendienteABoveda = ObtenerPendientesABoveda(objcajas.IdNroCaja);

                return objcajas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public clsCajaEstadisticaDiario Buscar(Int16 idEmpresaCajero
                                             , Int16 idUUNNCajero
                                             , Int16 idCentroServicioCajero
                                             , Int16 idTipoAtencion
                                             , Int16 idPuntoAtencion
                                             , Int32 IdUsuario
                                             , DateTime? fechaApertura)
        {
            try
            {
                clsCajaEstadisticaDiario _objcaja;
                Hashtable _htparametros = new Hashtable();
                DataRow _drwregistro;

                _htparametros.Add("@idempresacajero", idEmpresaCajero);
                _htparametros.Add("@iduunncajero", idUUNNCajero);
                _htparametros.Add("@idcentroserviciocajero", idCentroServicioCajero);
                _htparametros.Add("@idtipoatencion", idTipoAtencion);
                _htparametros.Add("@idpuntoatencion", idPuntoAtencion);
                _htparametros.Add("@idusuario", IdUsuario);
                _htparametros.Add("@fechaapertura", fechaApertura);

                _drwregistro = _DAO.EjecutarComandoRegistro("cajaestadisticadiario", "buscarultimacajaporusuario", _htparametros);

                if (_drwregistro == null) { return (clsCajaEstadisticaDiario)null; }

                _objcaja = new clsCajaEstadisticaDiario(_drwregistro);

                return _objcaja;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 ObtenerPendientesABoveda(Int32 idnrocaja)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@idnrocaja", idnrocaja);

                return (Int32)_DAO.EjecutarComandoEscalar("TransaccionFormaPagoExternoNoefectivo", "ObtenerPendientesABoveda", _htparametros);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Verificar Caja Aperturada

        #region Grabando en Transaccion

        public Int32 TransaccionInsertar(SqlTransaction sqltransaccion, clsTransaccion oTransaccion)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@nrotransaccion", 0);
                _htparametros.Add("@idtipodocumento", oTransaccion.IdTipoDocumento);
                _htparametros.Add("@fecharegistro", oTransaccion.FechaRegistro);
                _htparametros.Add("@periodopago", oTransaccion.PeriodoPago);
                _htparametros.Add("@fechapago", oTransaccion.FechaPago);
                _htparametros.Add("@importe", oTransaccion.ImporteNeto);
                _htparametros.Add("@importerecibido", oTransaccion.ImporteRecibido);
                _htparametros.Add("@importeefectivo", oTransaccion.ImporteEfectivo);
                _htparametros.Add("@importevalores", oTransaccion.ImporteValores);
                _htparametros.Add("@vuelto", oTransaccion.ImporteTotalVuelto);
                _htparametros.Add("@idusuario", oTransaccion.UsuarioPuntoAtencion.IdUsuario);
                _htparametros.Add("@idtipoatencion", oTransaccion.UsuarioPuntoAtencion.IdTipoAtencion);
                _htparametros.Add("@idpuntoatencion", oTransaccion.UsuarioPuntoAtencion.IdPuntoAtencion);
                _htparametros.Add("@idcentroserviciocajero", oTransaccion.UsuarioPuntoAtencion.IdCentroServicio);
                _htparametros.Add("@iduunncajero", oTransaccion.UsuarioPuntoAtencion.IdUnidadNegocio);
                _htparametros.Add("@idempresacajero", oTransaccion.UsuarioPuntoAtencion.IdEmpresa);
                _htparametros.Add("@idmoneda", oTransaccion.IdMonedaNacional);
                _htparametros.Add("@idnrocaja", oTransaccion.UsuarioPuntoAtencion.IdNroCajaUltimo);
                _htparametros.Add("@idmovimientocomercial", oTransaccion.IdMovComercial);
                _htparametros.Add("@estado", oTransaccion.IdEstado);

                _DAO.EjecutarComando("transaccion", "insertarregistro", _htparametros, sqltransaccion);

                oTransaccion.PeriodoPago = Convert.ToInt32(_htparametros["@periodopago"]);

                return Convert.ToInt32(_htparametros["@nrotransaccion"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerPeriodoPago(Int32 idnroservicio)
        {
            try
            {
                Int32 periodoPago = 0;

                Hashtable htparametros = new Hashtable();
                htparametros.Add("@p_idnroservicio", idnroservicio);

                Object resultado = _DAO.EjecutarComandoEscalar("Transaccion", "DeterminarPeriodoPago", htparametros);

                if (resultado != null) periodoPago = Convert.ToInt32(resultado);

                return periodoPago;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean Insertar(SqlTransaction sqltransaccion, clsTransaccion transaccion, clsDocumentosDescargoPropio documentodescargopropio, Int32 idnrocaja)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@nrotransaccion", documentodescargopropio.NroTransaccion);
                _htparametros.Add("@importedescargo", documentodescargopropio.ImporteOrigenDescargo);
                _htparametros.Add("@importedeuda", documentodescargopropio.ImporteOrigenDeuda);
                _htparametros.Add("@idempresa", documentodescargopropio.IdEmpresa);
                _htparametros.Add("@idtipodocumento", documentodescargopropio.IdTipoDocumento);
                _htparametros.Add("@nrodocumento", documentodescargopropio.NroDocumento);
                _htparametros.Add("@idtipodocumentosustento", documentodescargopropio.IdTipoDocumentoSustento);
                _htparametros.Add("@nrodocumentosustento", documentodescargopropio.NroDocumentoSustento);
                _htparametros.Add("@idservicio", documentodescargopropio.IdServicio);
                _htparametros.Add("@ordenpago", documentodescargopropio.OrdenPagoServicio);
                _htparametros.Add("@idnroservicio", documentodescargopropio.IdNroServicio);
                _htparametros.Add("@estercero", documentodescargopropio.EsTerceros);
                _htparametros.Add("@idnrocaja", idnrocaja);
                _htparametros.Add("@fechapago", transaccion.FechaPago);
                _htparametros.Add("@periodopago", transaccion.PeriodoPago);

                if (documentodescargopropio.EsTerceros) _DAO.EjecutarComando("transacciondocumentosdescargo", "terceroinsertarregistro", _htparametros, sqltransaccion);
                else _DAO.EjecutarComando("transacciondocumentosdescargo", "propioinsertarregistro", _htparametros, sqltransaccion);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean InsertarMedioDePago(SqlTransaction sqltransaccion, clsMediosPagoExterno mediopagoexterno, Int32 idnrocaja)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();
                _htparametros.Add("@nrotransaccion", mediopagoexterno.NroTransaccion);
                _htparametros.Add("@importepago", mediopagoexterno.ImporteOrigenTotal);
                _htparametros.Add("@idmonedapago", mediopagoexterno.IdMonedaOrigen);
                _htparametros.Add("@idmonedacambio", mediopagoexterno.IdMonedaCambio);
                _htparametros.Add("@tipocambio", mediopagoexterno.TipoCambio);
                _htparametros.Add("@importecambio", mediopagoexterno.ImporteCambioTotal);
                _htparametros.Add("@idtipodocumento", mediopagoexterno.IdTipoDocumento);
                _htparametros.Add("@nrodocumento", mediopagoexterno.NroDocumento);
                _htparametros.Add("@esingreso", mediopagoexterno.EsIngreso);
                _htparametros.Add("@idbanco", mediopagoexterno.IdBanco);
                _htparametros.Add("@idtipotarjeta", mediopagoexterno.IdTipoTarjeta);
                _htparametros.Add("@nrotarjeta", mediopagoexterno.NroTarjeta);
                _htparametros.Add("@idcuentabancaria", mediopagoexterno.IdCuentaBancaria); //mod
                _htparametros.Add("@cuentabancaria", mediopagoexterno.NroCuentaBancaria);
                _htparametros.Add("@fechadeposito", mediopagoexterno.FechaDocumento);
                _htparametros.Add("@idnrocaja", idnrocaja);

                _DAO.EjecutarComando("transaccionformapagoexterno", "insertarregistro", _htparametros, sqltransaccion);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Grabando en Transaccion

        #region Caja

        public Boolean ActualizarCaja(SqlTransaction sqltransaccion, Int32 nrotransaccion, Int32 nrodocumentos, Int32 nroservicios)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@nrotransaccion", nrotransaccion);
                _htparametros.Add("@nrodocumentos", nrodocumentos);
                _htparametros.Add("@nroservicios", nroservicios);

                _DAO.EjecutarComando("cajaestadisticadiario", "actualizarcaja", _htparametros, sqltransaccion);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean InsertarCaja(SqlTransaction sqltransaccion, clsCajaEstadisticaDiarioMoneda cajaestadisticamoneda)
        {
            try
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros.Add("@idnrocaja", cajaestadisticamoneda.IdNroCaja);
                _htparametros.Add("@idmonedapago", cajaestadisticamoneda.IdMoneda);
                _htparametros.Add("@idmonedacambio", cajaestadisticamoneda.IdMonedaCambio);
                _htparametros.Add("@importepago", cajaestadisticamoneda.Importe);
                _htparametros.Add("@tipocambio", cajaestadisticamoneda.TipoCambio);
                _htparametros.Add("@importecambio", cajaestadisticamoneda.ImporteCambio);
                _htparametros.Add("@idtipodocumento", cajaestadisticamoneda.IdTipoDocumentoComercial);

                _DAO.EjecutarComando("cajaestadisticadiariomoneda", "insertar", _htparametros, sqltransaccion);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Caja

        #endregion Public
    }

    #endregion clsCobranzaNGCDAO
}