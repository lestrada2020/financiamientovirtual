﻿using SISAC.Entidad.Atencion;
using SISAC.Interface.Atencion;
using SISAC.Negocio.Atencion;
using System.Web.Http;

namespace SISAC.Site.Atencion.Controllers
{
    public class ConsultaGeneralController : ApiController, IConsultaGeneral
    {
        #region Field

        private static readonly clsNegocioConsultaGeneral _ConsultaGeneral = clsNegocioConsultaGeneral.Instancia;

        #endregion Field

        #region Public

        #region Busqueda

        public IHttpActionResult BuscarBySuministro(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.BuscarBySuministro;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult BuscarByNroDocumento(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.BuscarByNroDocumento;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult BuscarByNombre(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.BuscarByNombre;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult BuscarByRecibo(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.BuscarByRecibo;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        #endregion Busqueda

        #region Datos

        public IHttpActionResult DatosPago(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosPago;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosRecibo(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosRecibo;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosReclamo(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosReclamo;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosMedidor(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosMedidor;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosRefacfurado(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosRefacfurado;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosFacturacion(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosFacturacion;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosTecnico(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosTecnico;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosConsumo(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosConsumo;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosSuministro(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosSuministro;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosContrato(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosContrato;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosDeuda(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosDeuda;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        public IHttpActionResult DatosComercial(clsConsultaGeneralFiltro param)
        {
            param.TipoConsulta = TypeConsultaGeneral.DatosComercial;

            var result = _ConsultaGeneral.GetData(param);

            return Ok(result);
        }

        #endregion Datos

        #region NroServicio

        public IHttpActionResult Resumen(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Resumen(param);

            return Ok(result);
        }

        public IHttpActionResult Basico(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Basico(param);

            return Ok(result);
        }

        public IHttpActionResult Restriccion(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Restriccion(param);

            return Ok(result);
        }

        public IHttpActionResult CtaCte(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.CtaCte(param);

            return Ok(result);
        }

        public IHttpActionResult Facturacion(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Facturacion(param);

            return Ok(result);
        }

        public IHttpActionResult FacturacionConcepto(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.FacturacionConcepto(param);

            return Ok(result);
        }

        public IHttpActionResult FacturacionDocumento(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.FacturacionDocumento(param);

            return Ok(result);
        }

        public IHttpActionResult ReciboBasico(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ReciboBasico(param);

            return Ok(result);
        }

        public IHttpActionResult ReciboMedidor(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ReciboMedidor(param);

            return Ok(result);
        }

        public IHttpActionResult ReciboLectura(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ReciboLectura(param);

            return Ok(result);
        }

        public IHttpActionResult ReciboConcepto(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ReciboConcepto(param);

            return Ok(result);
        }

        public IHttpActionResult Atencion(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Atencion(param);

            return Ok(result);
        }

        public IHttpActionResult FotoLectura(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.FotoLectura(param);

            return Ok(result);
        }

        public IHttpActionResult Lectura(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Lectura(param);

            return Ok(result);
        }

        public IHttpActionResult AFavor(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.AFavor(param);

            return Ok(result);
        }

        public IHttpActionResult Pagos(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Pagos(param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoBasico(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.TecnicoBasico(param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoMagnitud(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.TecnicoMagnitud(param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoPrecinto(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.TecnicoPrecinto(param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoTransformador(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.TecnicoTransformador(param);

            return Ok(result);
        }

        public IHttpActionResult OrdenTrabajo(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.OrdenTrabajo(param);

            return Ok(result);
        }

        public IHttpActionResult FotoActividad(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.FotoActividad(param);

            return Ok(result);
        }

        public IHttpActionResult Foto(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Foto(param);

            return Ok(result);
        }

        public IHttpActionResult MedidorBasico(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.MedidorBasico(param);

            return Ok(result);
        }

        public IHttpActionResult MedidorLista(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.MedidorLista(param);

            return Ok(result);
        }

        public IHttpActionResult MedidorLecturas(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.MedidorLecturas(param);

            return Ok(result);
        }

        public IHttpActionResult Cambios(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.Cambios(param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioLista(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ConvenioLista(param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioRegistro(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ConvenioRegistro(param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioCreacion(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ConvenioCreacion(param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioDocumentos(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ConvenioDocumentos(param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioGarantias(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ConvenioGarantias(param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioAmortizaciones(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ConvenioAmortizaciones(param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioLetra(clsConsultaGeneralFiltro param)
        {
            var result = _ConsultaGeneral.ConvenioLetra(param);

            return Ok(result);
        }

        #endregion NroServicio

        #endregion Public
    }
}