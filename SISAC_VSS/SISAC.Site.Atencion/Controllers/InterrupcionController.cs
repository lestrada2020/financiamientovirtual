﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Negocio.Atencion;
using System.Web.Http;

namespace SISAC.Site.Atencion.Controllers
{
    public class InterrupcionController : ApiController, IInterrupcion
    {
        #region Field

        private static readonly clsNegocioInterrupcion _AtencionInterrupcion = clsNegocioInterrupcion.Instancia;

        #endregion Field

        #region Public

        public IHttpActionResult ObtenerConfiguracionReporte(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerConfiguracionReporte(param);

            return Ok(result);
        }

        public IHttpActionResult AyudaLista(clsParametro param)
        {
            var result = _AtencionInterrupcion.AyudaLista(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerMotivoFomatoNotifica(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerMotivoFomatoNotifica(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNTiempo(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerResumenxUUNNTiempo(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNAreaTiempo(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerResumenxUUNNAreaTiempo(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNMotivoTiempo(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerResumenxUUNNMotivoTiempo(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNSEDTiempo(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerResumenxUUNNSEDTiempo(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleSuministros(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerDetalleSuministros(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleSEDs(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerDetalleSEDs(param);

            return Ok(result);
        }

        public IHttpActionResult GenerarTokenReporte(clsParametro param)
        {
            var result = _AtencionInterrupcion.GenerarTokenReporte(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerAtencionesxRecursoInterrupcion(clsParametro param)
        {
            var result = _AtencionInterrupcion.ObtenerAtencionesxRecursoInterrupcion(param);

            return Ok(result);
        }

        public IHttpActionResult LimpiarTablaReporte(clsParametro param)
        {
            var result = _AtencionInterrupcion.LimpiarTablaReporte(param);

            return Ok(result);
        }

        #endregion Public
    }
}