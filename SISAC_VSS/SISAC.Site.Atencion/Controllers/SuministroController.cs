﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Negocio.Atencion;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Site.Atencion.Controllers
{
    public class SuministroController : ApiController, ISuministro
    {
        #region Field

        private static readonly clsNegocioSuministro _Sumin = clsNegocioSuministro.Instancia;

        #endregion Field

        #region Private

        private clsResultado ValidarToken(clsParametro param, String KeyToken)
        {
            var result = new clsResultado();

            if (param.Parametros["SessionToken"].Equals(KeyToken)) return result;

            result.IdError = 1;
            result.Mensaje = "Token no valido.";

            return result;
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> DeudaFechaVence(clsParametro param)
        {
            var result = await _Sumin.DeudaFechaVence(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> InterrupcionServicio(clsParametro param)
        {
            var result = await _Sumin.InterrupcionServicio(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> CortePorDeuda(clsParametro param)
        {
            var result = await _Sumin.CortePorDeuda(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConsultarDatos(clsParametro param)
        {
            var result = await _Sumin.ConsultarDatos(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistroInterrupcion(clsParametro param)
        {
            var result = await _Sumin.RegistroInterrupcion(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConsultarRHD(clsParametro param)
        {
            var result = await _Sumin.ConsultarRHD(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> UltimaLectura(clsParametro param)
        {
            var result = await _Sumin.UltimaLectura(param);

            return Ok(result);
        }

        #endregion Public
    }
}