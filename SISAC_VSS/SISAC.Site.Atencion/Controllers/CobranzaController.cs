﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Negocio.Atencion;
using System.Collections.Generic;
using System.Web.Http;

namespace SISAC.Site.Atencion.Controllers
{
    public class CobranzaController : ApiController, ICobranza
    {
        #region Field

        private static readonly clsNegocioCobranza _Cobranza = clsNegocioCobranza.Instancia;
        private static readonly clsNegocioCobranzaEmpresa _CobranzaEmpresa = clsNegocioCobranzaEmpresa.Instancia;

        #endregion Field

        #region Public

        #region ClienteNatural

        public IHttpActionResult ConsultarDeuda(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.ConsultarDeuda(param);

            return Ok(result);
        }

        public IHttpActionResult ConsultarDeudaDetalle(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.ConsultarDeudaDetalle(param);

            return Ok(result);
        }

        public IHttpActionResult PagarDeuda(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.PagarDeuda(param);

            return Ok(result);
        }


        public IHttpActionResult GrabarIntento(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.GrabarIntento(param);

            return Ok(result);
        }

        public IHttpActionResult GrabarIntentoDetalle(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.GrabarIntentoDetalle(param);

            return Ok(result);
        }

        public IHttpActionResult ActualizarIntento(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.ActualizarIntento(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerPagoResumen(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.ObtenerPagoResumen(param);

            return Ok(result);
        }

        public IHttpActionResult EnviarEmail(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.EnviarEmail(param);

            return Ok(result);
        }

        #endregion ClienteNatural

        #region ClienteEmpresa

        public IHttpActionResult GrabarIntentoEmpresa(clsParametroBulkData param)
        {

            clsParametro validatoken = new clsParametro();
            validatoken.Parametros = new Dictionary<string, string>();
            validatoken.Parametros["SessionToken"] = param.Token;

            var result = _Cobranza.ValidarToken(validatoken);

            if (result.IdError == 0) result = _CobranzaEmpresa.GrabarIntentoEmpresa(param);

            return Ok(result);

        }


        public IHttpActionResult PagarDeudaEmpresa(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _CobranzaEmpresa.PagarDeudaEmpresa(param);

            return Ok(result);
        }

        public IHttpActionResult ListarBancos(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _CobranzaEmpresa.ListarBancos(param);

            return Ok(result);
        }

        public IHttpActionResult ListarCuentas(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _CobranzaEmpresa.ListarCuentas(param);

            return Ok(result);
        }


        public IHttpActionResult GrabarDepositoBancarioIntento(clsParametroBulkData param)
        {

            clsParametro validatoken = new clsParametro();
            validatoken.Parametros = new Dictionary<string, string>();
            validatoken.Parametros["SessionToken"] = param.Token;

            var result = _Cobranza.ValidarToken(validatoken);

            if (result.IdError == 0) result = _CobranzaEmpresa.GrabarDepositoBancarioIntento(param);

            return Ok(result);

        }


        public IHttpActionResult ActualizarDepositoBancarioEstado(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _CobranzaEmpresa.ActualizarDepositoBancarioEstado(param);

            return Ok(result);
        }


        #endregion ClienteEmpresa

        #region Cobranza Visa - Orden Visa
        public IHttpActionResult CobranzaOrdenVisaNET(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0) result = _Cobranza.CobranzaOrdenVisaNET(param);

            return Ok(result);
        }
        #endregion

        #endregion Public
    }
}