﻿using Newtonsoft.Json;
using SISAC.Entidad.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Atencion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Site.Atencion.Controllers
{
    public class FinanciamientoController : ApiController
    {
        #region Field

        private static readonly clsNegocioFinanciamiento _Financiamiento = clsNegocioFinanciamiento.Instancia;
        private static readonly clsNegocioCobranza _Cobranza = clsNegocioCobranza.Instancia;
        #endregion Field

        #region Suministros Asociado
        public IHttpActionResult ListarSuministroAsociado(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ListarSuministroAsociado(param);

            return Ok(result);
        }
        #endregion

        #region "CMM"
        public IHttpActionResult ObtenerDetalleFinanciamiento(clsParametro param)
        {

            var result = _Financiamiento.ObtenerDetalleFinanciamiento(param);
            return Ok(result);
        }


        #region Pago Niubiz
        /// <summary>
        /// Genera el numero de pedido
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> GenerarNroPedido(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = await _Financiamiento.GenerarNroPedido(param);

            return Ok(result);
        }
        /// <summary>
        /// Obtiene los datos de pago
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> ObtenerIntentoPagoFinanciamiento(clsParametro param)
        {
            var result = await _Financiamiento.ObtenerIntento(param);

            return Ok(result);
        }

        /// <summary>
        /// Metodo que se encarga de actualizar el preview pago 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 
        public IHttpActionResult ActualizarIntentoPago(clsParametro param)
        {
            var result = _Financiamiento.ValidarToken(param);
            if (result.IdError == 0) result = _Financiamiento.ActualizarIntento(param);
            return Ok(result);
        }

        public IHttpActionResult PagarDeuda(clsParametro param)
        {
            var result = _Financiamiento.ValidarToken(param);
            if (result.IdError == 0) result = _Financiamiento.PagarDeuda(param);
            return Ok(result);
        }
        #endregion Pago Niubiz

        public IHttpActionResult EnviarEmail(clsParametro param)
        {
            var result = _Financiamiento.ValidarToken(param);
            if (result.IdError == 0) result = _Financiamiento.EnviarEmail(param);
            return Ok(result);
        }

        public IHttpActionResult FinanciamientoEnviarEmailError(clsParametro param)
        {
            var result = _Financiamiento.FinanciamientoEnviarEmailError(param);
            return Ok(result);
        }

        public IHttpActionResult ConsultarDeudaDetalle(clsParametro param)
        {
            var result = _Financiamiento.ValidarToken(param);
            if (result.IdError == 0) result = _Financiamiento.ConsultarDeudaDetalle(param);
            return Ok(result);
        }


        #endregion

        //public IHttpActionResult GenerarComprobantePago(clsParametro param)
        //{
        //    var res = _Financiamiento.ObtenerRutaBaseComprobante(param);
        //    return Ok(res);
        //}

        public IHttpActionResult ObtenerDocumentoComprobantePago(clsParametro param)
        {
            var res = _Financiamiento.ObtenerDocumentoComprobantePago(param);
            return Ok(res);
        }

        public IHttpActionResult GenerarDocumentoTransaccionExtrajudicial(clsParametro param)
        {
            var res = _Financiamiento.GenerarDocumentoTransaccionExtrajudicial(param);
            return Ok(res);
        }

        #region LESTRADA

        public IHttpActionResult ObtenerParametrosConvenio(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            clsResultado result = _Financiamiento.ObtenerParametrosConvenio(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerRutaVideoFinanciamiento(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerRutaVideoFinanciamiento(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerPorcentajesConvenio(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerPorcentajesConvenio(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerCentroServicios(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);
            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerCentroServicios(param);
            return Ok(result);
        }


        public IHttpActionResult ObtenerConveniosPorEmpresa(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerConveniosPorEmpresa(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerConvenioPorId(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerConvenioPorId(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleDeuda(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerDetalleDeuda(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerConfiguracionGlobal(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerConfiguracionGlobal(param);

            return Ok(result);
        }
        public IHttpActionResult ObtenerDetalleOrdenCobro(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerDetalleOrdenCobro(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerCabeceraOrdenCobro(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerCabeceraOrdenCobro(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerListaPorOrdenCobro(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ObtenerListaPorOrdenCobro(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> VisaListFailed(clsParametro param)
        {
            var result = await _Financiamiento.VisaListFailed(param);

            return Ok(result);
        }

        public IHttpActionResult ActualizarMetodoPagoOrdenVisa(clsParametro param)
        {
            //var result = _Cobranza.ValidarToken(param);

            //if (result.IdError == 0)
            var result = _Financiamiento.ActualizarMetodoPagoOrdenVisa(param);

            return Ok(result);
        }
        #endregion LESTRADA

        #region RBERROSPI

        public async Task<IHttpActionResult> ObtenerMetodosPago(clsParametro param)
        {
            var result = await _Financiamiento.ObtenerMetodosPago(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerRutaComprobante(clsParametro param)
        {
            var result = _Financiamiento.ObtenerRutaComprobante(param);
            return Ok(result);
        }

        public IHttpActionResult ObtenerRutaTransaccion(clsParametro param)
        {
            var result = _Financiamiento.ObtenerRutaTransaccion(param);
            return Ok(result);
        }

        public async Task<IHttpActionResult> ValidarPagosMultiples(clsParametro param)
        {
            var result = await _Financiamiento.ValidarPagosMultiples(param);
            return Ok(result);
        }


        #endregion


        //public IHttpActionResult AnularOrdenCobroPendiente(clsParametro param)
        //{        

        //    var result = _Financiamiento.AnularOrdenCobroPendiente(param);
        //    return Ok(result);
        //}

        public IHttpActionResult GenerarOrdenCobro(clsParametro param)
        {
            var result = _Cobranza.ValidarToken(param);

            if (result.IdError == 0)

                result = _Financiamiento.GenerarOrdenCobro(param);

            return Ok(result);
        }

        public IHttpActionResult EstadoPagoDistriluz(clsParametro param)
        {
            clsFinanciamientoOrdenVisaNet result = _Financiamiento.ObtenerOrdenVisaNetPorId(param);

            clsResultado res = new clsResultado();

            res.IdError = 0;
            res.Datos = result.IdEstado;

            return Ok(res);

        }


    }
}
