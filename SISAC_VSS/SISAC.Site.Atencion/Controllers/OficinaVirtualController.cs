﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Negocio.Atencion;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Site.Atencion.Controllers
{
    public class OficinaVirtualController : ApiController, IOficinaVirtual
    {
        #region Field

        private static readonly clsNegocioOficinaVirtual _OficinaVirtual = clsNegocioOficinaVirtual.Instancia;
        private static readonly String _TokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];

        #endregion Field

        #region Private

        private clsResultado ValidarToken(clsParametro param)
        {
            var result = new clsResultado();

            if (param.Parametros["SessionToken"].Equals(_TokenDistriluz)) return result;

            result.IdError = 1;
            result.Mensaje = "Token no valido.";

            return result;
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> ConsultaAtencionesSuministro(clsParametro param)
        {
            var result = await _OficinaVirtual.ConsultaAtencionesSuministro(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConsultaAtencionNroAtencion(clsParametro param)
        {
            var result = await _OficinaVirtual.ConsultaAtencionNroAtencion(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarContactanos(clsParametro param)
        {
            var result = await _OficinaVirtual.RegistrarContactanos(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListarInterrupciones(clsParametro param)
        {
            var result = await _OficinaVirtual.ListarInterrupciones(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConfirmarEmail(clsParametro param)
        {
            var result = await _OficinaVirtual.ConfirmarEmail(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarReclamo(clsParametro param)
        {
            var result = await _OficinaVirtual.RegistrarReclamo(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> InfoSuministro(clsParametro param)
        {
            var result = await _OficinaVirtual.InfoSuministro(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerObservacion(clsParametro param)
        {
            var result = await _OficinaVirtual.ObtenerObservacion(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> DesasociarSuministro(clsParametro param)
        {
            var result = await _OficinaVirtual.DesasociarSuministro(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> CambiarPassword(clsParametro param)
        {
            var result = await _OficinaVirtual.CambiarPassword(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListarByNroAtencion(clsParametro param)
        {
            var result = await _OficinaVirtual.ListarByNroAtencion(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarFotoLec(clsParametro param)
        {
            var result = await _OficinaVirtual.RegistrarFotoLec(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> VisaListFailed(clsParametro param)
        {
            var result = await _OficinaVirtual.VisaListFailed(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> VisaSendMailPayFailed(clsParametro param)
        {
            var result = await _OficinaVirtual.VisaSendMailPayFailed(param);

            return Ok(result);
        }

        #region Cliente Empresa

        public IHttpActionResult ValidaAccesoClienteEmpresa(clsParametro param)
        {
            var result = _OficinaVirtual.ValidaAccesoClienteEmpresa(param);
            return Ok(result);
        }

        public IHttpActionResult RegistrarClienteEmpresaAdmin(clsParametro param)
        {
            var result = _OficinaVirtual.RegistrarClienteEmpresaAdmin(param);
            return Ok(result);
        }

        public IHttpActionResult BuscarPorIdentidad(clsParametro param)
        {
            var result = _OficinaVirtual.BuscarPorIdentidad(param);
            return Ok(result);
        }

        public IHttpActionResult ListarClienteEmpresa(clsParametro param)
        {
            var result = _OficinaVirtual.ListarClienteEmpresa(param);
            return Ok(result);
        }

        public IHttpActionResult ListarRoles(clsParametro param)
        {
            var result = _OficinaVirtual.ListarRoles(param);
            return Ok(result);
        }

        public IHttpActionResult TipoRelacion(clsParametro param)
        {
            var result = _OficinaVirtual.TipoRelacion(param);
            return Ok(result);
        }

        public IHttpActionResult RegistrarClienteEmpresa(clsParametro param)
        {
            var result = _OficinaVirtual.RegistrarClienteEmpresa(param);
            return Ok(result);
        }

        public IHttpActionResult EditarClienteEmpresa(clsParametro param)
        {
            var result = _OficinaVirtual.EditarClienteEmpresa(param);
            return Ok(result);
        }

        public IHttpActionResult EliminarClienteEmpresa(clsParametro param)
        {
            var result = _OficinaVirtual.EliminarClienteEmpresa(param);
            return Ok(result);
        }

        public IHttpActionResult ListarTarifaActiva(clsParametro param)
        {
            var result = _OficinaVirtual.ListarTarifaActiva(param);
            return Ok(result);
        }

        public IHttpActionResult ListarSuministros(clsParametro param)
        {
            var result = _OficinaVirtual.ListarSuministros(param);
            return Ok(result);
        }

        public IHttpActionResult ListarSuministrosDetallePago(clsParametro param)
        {
            var result = _OficinaVirtual.ListarSuministrosDetallePago(param);
            return Ok(result);
        }

        public IHttpActionResult EliminarSuministroAsociado(clsParametro param)
        {
            var result = _OficinaVirtual.EliminarSuministroAsociado(param);
            return Ok(result);
        }

        public IHttpActionResult BuscarSuministroAsociar(clsParametro param)
        {
            var result = _OficinaVirtual.BuscarSuministroAsociar(param);
            return Ok(result);
        }

        public IHttpActionResult RegistrarSuministroAsociar(clsParametro param)
        {
            var result = _OficinaVirtual.RegistrarSuministroAsociar(param);
            return Ok(result);
        }

        public IHttpActionResult ListarSuministrosCarga(clsParametroBulkData param)
        {
            var result = _OficinaVirtual.ListarSuministrosCarga(param);
            return Ok(result);
        }

        #endregion Cliente Empresa

        public async Task<IHttpActionResult> ObtenerFile(clsParametro param)
        {
            var result = await _OficinaVirtual.ObtenerFile(param);

            return Ok(result);
        }

        #region Beneficiario

        public async Task<IHttpActionResult> ObtenerBeneficiario(clsParametro param)
        {
            var result = await _OficinaVirtual.ObtenerBeneficiario(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarBeneficiario(clsParametro param)
        {
            var result = await _OficinaVirtual.RegistrarBeneficiario(param);

            return Ok(result);
        }

        #endregion Beneficiario

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<IHttpActionResult> RegistrarUsuario(clsParametro param)
        {
            var result = await _OficinaVirtual.RegistrarUsuario(param);

            return Ok(result);
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<IHttpActionResult> ActivarCuentaXsms(clsParametro param)
        {
            var result = await _OficinaVirtual.ActivarCuentaXsms(param);

            return Ok(result);
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<IHttpActionResult> ReenviarSMS(clsParametro param)
        {
            var result = await _OficinaVirtual.ReenviarSMS(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerDatosConfirmacion(clsParametro param)
        {
            var result = await _OficinaVirtual.ObtenerDatosConfirmacion(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> SolicitarNewPassword(clsParametro param)
        {
            var result = await _OficinaVirtual.SolicitarNewPassword(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> DatosNroServicio(clsParametro param)
        {
            var result = await _OficinaVirtual.DatosNroServicio(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListarNroServicio(clsParametro param)
        {
            var result = await _OficinaVirtual.ListarNroServicio(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> LecturaRegistrar(clsParametro param)
        {
            var result = await _OficinaVirtual.LecturaRegistrar(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> AyudaLista(clsParametro param)
        {
            var result = await _OficinaVirtual.AyudaLista(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListAppConfig(clsParametro param)
        {
            var result = ValidarToken(param);

            if (result.IdError == 0) result = await _OficinaVirtual.ListAppConfig(param);

            return Ok(result);
        }

        #region Cobranza

        public async Task<IHttpActionResult> ConsultarDeudaDetalle(clsParametro param)
        {
            var result = await _OficinaVirtual.ConsultarDeudaDetalle(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> GrabarIntentoDetalle(clsParametro param)
        {
            var result = await _OficinaVirtual.GrabarIntentoDetalle(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerIntento(clsParametro param)
        {
            var result = await _OficinaVirtual.ObtenerIntento(param);

            return Ok(result);
        }

        #endregion Cobranza

        #region BonoElectrico

        public async Task<IHttpActionResult> ObtenerBonoElectrico(clsParametro param)
        {
            var result = await _OficinaVirtual.ObtenerBonoElectrico(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerBonoElectricoDNI(clsParametro param)
        {
            var result = await _OficinaVirtual.ObtenerBonoElectricoDNI(param);

            return Ok(result);
        }

        #endregion BonoElectrico

        #endregion Public
    }
}