﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SISAC.Proxy.NTCSE.Controllers
{
    public class ConsultaInterrupcionController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteNTCSE";
                String controller = "ConsultaInterrupcion";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public IHttpActionResult ObtenerDatosInterrupcion(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDatosInterrupcion", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosReposicionParcialInterrupcion(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDatosReposicionParcialInterrupcion", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosGPSSuministro(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDatosGPSSuministro", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosGPSSuministroLejano(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDatosGPSSuministroLejano", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerSuministrosAfectadosGPS(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerSuministrosAfectadosGPS", param);

            return Ok(result);
        }

        #endregion Public
    }

}