﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SISAC.Proxy.NTCSE.Controllers
{
    public class IndicadorSaifiSaidiController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteNTCSE";
                String controller = "IndicadorSaifiSaidi";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public IHttpActionResult ObtenerTabularIndicadorAcumulado(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerTabularIndicadorAcumulado", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerIndicadorAcumulado(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerIndicadorAcumulado", param);

            return Ok(result);
        }


        public IHttpActionResult ObtenerEvolucionIndicadorAcumulado(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerEvolucionIndicadorAcumulado", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerProcesosxSimulacion(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerProcesosxSimulacion", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerIndicadorAcumuladoMovil(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerIndicadorAcumuladoMovil", param);

            return Ok(result);
        }

        #endregion Public
    }
}