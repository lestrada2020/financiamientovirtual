﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SISAC.Entidad.Facturacion
{
    #region Lista Entidad Maestra
    public interface ISeleccionable
    {
        Boolean Seleccionado
        {
            get;
            set;
        }
    }

    #region RegistroMaestroObject

    public class clsRegistroMaestroObject : ISeleccionable
    {
        private Object _objid;
        private String _strnombre;
        private Int16 _intidestado = 1;
        private Object _objtag;
        private String _observacion;
        //adiconado 31/07/2012
        private String _descripcion;
        private String _codigointerno;

        /// <summary>
        /// Identificador ID.
        /// </summary>
        public Object Id
        {
            get { return _objid; }
            set { _objid = value; }
        }

        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        /// <summary>
        /// Estado.
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        /// Valor Alternativo.
        /// </summary>
        public Object Tag
        {
            get { return _objtag; }
            set { _objtag = value; }
        }

        /// <summary>
        /// Observacion
        /// </summary>
        public String Observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        public String Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        public String CodigoInterno
        {
            get { return _codigointerno; }
            set { _codigointerno = value; }
        }

        /// <summary>
        /// Constructor predeterminado.
        /// </summary>
        public clsRegistroMaestroObject() { }

        /// <summary>
        /// Constructor datarow.
        /// </summary>
        /// <param name="dr"></param>
        public clsRegistroMaestroObject(DataRow dr)
        {
            if (dr == null) { return; }

            _objid = dr["id"];
            if (dr.Table.Columns.Contains("nombre")) { _strnombre = dr["nombre"].ToString(); }

            if (dr.Table.Columns.Contains("idestado"))
            {
                IdEstado = Convert.ToInt16(dr["idestado"]);

                if (IdEstado == (Int16)0) { Nombre += " (Inactivo)"; }
                else if (IdEstado == (Int16)2) { Nombre += " (Anulado)"; }
            }

            if (dr.Table.Columns.Contains("tag")) { _objtag = dr["tag"]; }
            if (dr.Table.Columns.Contains("observacion")) Observacion = dr["observacion"].ToString();
            //adicionado 31/07/2012
            if (dr.Table.Columns.Contains("descripcion")) { _descripcion = dr["descripcion"].ToString(); }
            if (dr.Table.Columns.Contains("codigointerno")) { _codigointerno = dr["codigointerno"].ToString(); }
        }

        /// <summary>
        /// Constructor parametros.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nombre"></param>
        public clsRegistroMaestroObject(Object id, String nombre)
        {
            Id = id;
            Nombre = nombre;
            IdEstado = (Int16)1;
        }

        /// <summary>
        /// Constructor parametros estado.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nombre"></param>
        /// <param name="idestado"></param>
        public clsRegistroMaestroObject(Object id, String nombre, Int16 idestado)
        {
            Id = id;
            Nombre = nombre;
            IdEstado = idestado;
            if (IdEstado == (Int16)0) { Nombre += " (Inactivo)"; }
            else if (IdEstado == (Int16)2) { Nombre += " (Anulado)"; }
        }

        #region ISeleccionable Members

        Boolean seleccionado;

        public bool Seleccionado
        {
            get
            {
                return seleccionado;
            }
            set
            {
                seleccionado = value;
            }
        }

        #endregion ISeleccionable Members
    }

    #region Clase Abstracta
    public interface ICombo
    {
        Int16 Id { get; set; }

        String Nombre { get; set; }
    }

    public abstract class clsEntidadMaestro : ICombo
    {
        protected Int16 _intid;
        protected String _strnombre;
        protected Boolean _bolseleccionado = false;

        #region ICombo Members

        /// <summary>
        /// Id
        /// </summary>
        ///
        [Campo()]
        public virtual Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        /// Nombre
        /// </summary>
        ///
        [Campo()]
        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        public Boolean Seleccionado
        {
            get { return _bolseleccionado; }
            set { _bolseleccionado = value; }
        }

        #endregion ICombo Members
    }

    #endregion Clase Abstracta 

    public class clsRegistroMaestro : clsEntidadMaestro, ISeleccionable
    {
        Boolean selector = false;
        //Int16? _intidestado;

        public Boolean Seleccionado
        {
            get { return selector; }
            set { selector = value; }
        }

        //public Int16? IdEstado
        //{
        //    get { return _intidestado; }
        //    set { _intidestado = value; }
        //}

        public clsRegistroMaestro()
        {
        }

        public clsRegistroMaestro(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt16((dr["id"]));
            Nombre = dr["nombre"].ToString();

            //if (dr.Table.Columns.Contains("idestado"))
            //    IdEstado = Convert.IsDBNull(dr["idestado"]) ? (Int16?)null : Convert.ToInt16(dr["idestado"]);
        }

        public clsRegistroMaestro(Int16 id, String nombre)
        {
            Id = id;
            Nombre = nombre;
        }
    }

    #endregion RegistroMaestroObject

    [Serializable()]
    public class clsListaEntidadMaestra : ICloneable
    {
        private List<clsEntidadMaestro> _objelementos = new List<clsEntidadMaestro>();
        private DateTime _datnacimiento = DateTime.Now;
        private TimeSpan _tsntiempovida = new TimeSpan(23, 59, 59);

        /// <summary>
        /// Lista elementos.
        /// </summary>
        public List<clsEntidadMaestro> Elementos
        {
            get { return _objelementos; }
        }

        /// <summary>
        /// Nacimiento.
        /// </summary>
        public DateTime Nacimiento
        {
            get { return _datnacimiento; }
        }

        /// <summary>
        /// TiempoVida
        /// </summary>
        public TimeSpan TiempoVida
        {
            get { return _tsntiempovida; }
            set { _tsntiempovida = value; }
        }

        /// <summary>
        /// clsListaEntidadMaestra
        /// </summary>
        public clsListaEntidadMaestra()
        { }

        /// <summary>
        /// clsListaEntidadMaestra
        /// </summary>
        /// <param name="elemento"></param>
        public clsListaEntidadMaestra(clsEntidadMaestro elemento)
        {
            Elementos.Add(elemento);
        }

        /// <summary>
        /// clsListaEntidadMaestra
        /// </summary>
        /// <param name="dt"></param>
        public clsListaEntidadMaestra(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0) { return; }

            foreach (DataRow dr in dt.Rows)
            {
                Elementos.Add(new clsRegistroMaestro(dr));
            }
        }

        #region BusquedaPorId

        Int16 _intidtmp;
        Predicate<clsEntidadMaestro> PredicadoBuscar;

        public clsEntidadMaestro BuscarPorId(Int16 id)
        {
            _intidtmp = id;
            PredicadoBuscar = new Predicate<clsEntidadMaestro>(ExisteId);
            return Elementos.Find(PredicadoBuscar);
        }

        private Boolean ExisteId(clsEntidadMaestro entidadmaestro)
        {
            return (entidadmaestro.Id == _intidtmp);
        }

        #endregion BusquedaPorId

        #region ICloneable Members

        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion ICloneable Members
    }

    [Serializable()]
    public class clsListaEntidadMaestraObject
    {
        //private System.ComponentModel.BindingList<clsRegistroMaestroObject> _objelementos = new System.ComponentModel.BindingList<clsRegistroMaestroObject>();
        private List<clsRegistroMaestroObject> _objelementos = new List<clsRegistroMaestroObject>();
        private DateTime _datnacimiento = DateTime.Now;
        private TimeSpan _tsntiempovida = new TimeSpan(23, 59, 59);

        /// <summary>
        ///
        /// </summary>
        //public System.ComponentModel.BindingList<clsRegistroMaestroObject> Elementos
        public List<clsRegistroMaestroObject> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime Nacimiento
        {
            get { return _datnacimiento; }
        }

        /// <summary>
        ///
        /// </summary>
        public TimeSpan TiempoVida
        {
            get { return _tsntiempovida; }
            set { _tsntiempovida = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public clsListaEntidadMaestraObject()
        { }

        /// <summary>
        ///
        /// </summary>
        /// <param name="elemento"></param>
        public clsListaEntidadMaestraObject(clsRegistroMaestroObject elemento)
        {
            Elementos.Add(elemento);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dt"></param>
        public clsListaEntidadMaestraObject(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0) { return; }

            foreach (DataRow dr in dt.Rows)
            {
                Elementos.Add(new clsRegistroMaestroObject(dr));
            }
        }
    }

    #endregion Lista Entidad Maestra

    #region Campo Atributo
    public class CampoAttribute : Attribute
    {
        String nombreCabecera = "";
        Int32 anchoCabecera = 0;
        Boolean soloLectura = true;

        /// <summary>
        /// Nombre a mostrar en la cabecera de la columna.
        /// </summary>
        public String NombreCabecera
        {
            get { return nombreCabecera; }
            set { nombreCabecera = value; }
        }

        /// <summary>
        /// Ancho de la columna.
        /// </summary>
        public Int32 AnchoCabecera
        {
            get { return anchoCabecera; }
            set { anchoCabecera = value; }
        }

        /// <summary>
        /// Indicador de solo lectura.
        /// </summary>
        public Boolean SoloLectura
        {
            get { return soloLectura; }
            set { soloLectura = value; }
        }

        /// <summary>
        /// Constructor predeterminado.
        /// </summary>
        public CampoAttribute() { }

        /// <summary>
        /// Constructor Nombre Cabecera.
        /// </summary>
        /// <param name="_nombrecabecera"></param>
        public CampoAttribute(String _nombrecabecera)
        {
            nombreCabecera = _nombrecabecera;
        }
    }

    public class CampoListaAttribute : Attribute
    {
        public CampoListaAttribute() { }
    }
    #endregion

    #region NroServicio
    public class clsNroServicioBasico
    {
        #region Campos

        private Int32 _intidnroservicio;
        private String _strestado;
        private String _strcategoria;
        private String _strservicio;
        private String _strdireccion;
        private String _strnombrenroservicio;
        private String _strtarifa;
        private String _strabreviatarifa;
        private String _strdepartamento;
        private String _strprovincia;
        private String _strdistrito;
        private String _strlocalidad;
        private Int16 _intidtiposervicio;
        private Int16 _intidtipoidentidadpropietario;
        private String _strnroidentidadpropietario;

        private Int16 _intidempresa;
        private Int16 _intiduunn;
        private Int16 _intidsector;

        private Int16 _intidservicio;
        private Int16 _intidestadofacturacion;
        private String strcartera;
        private String _strnombrecartera;
        private Int16 _intidcentroservicio;
        private Int32 _iddireccioncodificada;
        private Int16 _intidtarifa;
        private String _strdireccioncomplementaria;
        private String _strdireccionreferencial;

        private Int16? _intidestado;
        private Int16? _intcortado;
        private Int16? _intidsistemaelectrico;
        private String _strmodalidad;

        private Int16 _intidfase;
        private Int32 _intidelementoelectrico;
        private String _strnombrefase;

        private String _strabreviasector;
        private String _strdocumentoidentidad;

        private String _Mercado;
        private String _PuntoMedicion;
        private String _TipoServicio;
        private String _EsLocalidadAfecta;

        private String _NivelTension;
        private Int16 _SectorTipico;

        //Adicionar campos para el historico de Consumo

        private String _strseriefabrica;
        private Decimal? _decfactorinterno;
        private Decimal? _decfactormedicion;
        private Decimal? _decconsumoactual;
        private Decimal? _decconsumoanterior;
        private Decimal? _decconsumoantanterior;
        private Decimal? _declectura;
        private Decimal? _decconsumopromedio;
        private Int16 _intidciiu;
        private String _strnombreciiu;
        private Decimal? _decconsumooptimo;
        //    

        //Adicionar 
        private Decimal? _decconsumo1;
        private Decimal? _decconsumo2;
        private Decimal? _decconsumo3;
        private Decimal? _decpromediosemestral;
        private Decimal? _decpromedioanual;
        private Decimal? _decpromediohistorico;
        private Decimal? _decmaximoconsumo;

        //
        private short _idPlanCondicion;

        //Datos de DireccionCodificada
        private Int16? _intiddepartamento;
        private Int16? _intidprovincia;
        private Int16? _intiddistrito;
        private String _strubigeo;

        #endregion Campos

        #region Propiedades
        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String NombreFase
        {
            get { return _strnombrefase; }
            set { _strnombrefase = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdFase
        {
            get { return _intidfase; }
            set { _intidfase = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String NombreCartera
        {
            get { return _strnombrecartera; }
            set { _strnombrecartera = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String Modalidad
        {
            get { return _strmodalidad; }
            set { _strmodalidad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String DireccionReferencial
        {
            get { return _strdireccionreferencial; }
            set { _strdireccionreferencial = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int32 IdDireccionCodificada
        {
            get { return _iddireccioncodificada; }
            set { _iddireccioncodificada = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        /// <summary>
        /// NroServicio.
        /// </summary>
        [Campo()]
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int32 IdElementoElectrico
        {
            get { return _intidelementoelectrico; }
            set { _intidelementoelectrico = value; }
        }

        /// <summary>
        /// Estado.
        /// </summary>
        [Campo()]
        public String Estado
        {
            get { return _strestado; }
            set { _strestado = value; }
        }

        /// <summary>
        /// Cartera.
        /// </summary>
        [Campo()]
        public String Categoria
        {
            get { return _strcategoria; }
            set { _strcategoria = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String Servicio
        {
            get { return _strservicio; }
            set { _strservicio = value; }
        }

        /// <summary>
        /// Direccion.
        /// </summary>
        [Campo()]
        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }

        /// <summary>
        /// NombreNroServicio
        /// </summary>
        [Campo()]
        public String NombreNroServicio
        {
            get { return _strnombrenroservicio; }
            set { _strnombrenroservicio = value; }
        }

        /// <summary>
        /// Tarifa.
        /// </summary>
        [Campo()]
        public String Tarifa
        {
            get { return _strtarifa; }
            set { _strtarifa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String AbreviaTarifa
        {
            get { return _strabreviatarifa; }
            set { _strabreviatarifa = value; }
        }

        /// <summary>
        /// Departamento.
        /// </summary>
        [Campo()]
        public String Departamento
        {
            get { return _strdepartamento; }
            set { _strdepartamento = value; }
        }

        /// <summary>
        /// Provincia.
        /// </summary>
        [Campo()]
        public String Provincia
        {
            get { return _strprovincia; }
            set { _strprovincia = value; }
        }

        /// <summary>
        /// Distrito.
        /// </summary>
        [Campo()]
        public String Distrito
        {
            get { return _strdistrito; }
            set { _strdistrito = value; }
        }

        /// <summary>
        /// Localidad.
        /// </summary>
        [Campo()]
        public String Localidad
        {
            get { return _strlocalidad; }
            set { _strlocalidad = value; }
        }

        /// <summary>
        /// IdTipoServicio
        /// </summary>
        [Campo()]
        public Int16 IdTipoServicio
        {
            get { return _intidtiposervicio; }
            set { _intidtiposervicio = value; }
        }

        /// <summary>
        /// IdTipoIdentidadPropietario
        /// </summary>
        [Campo()]
        public Int16 IdTipoIdentidadPropietario
        {
            get { return _intidtipoidentidadpropietario; }
            set { _intidtipoidentidadpropietario = value; }
        }

        /// <summary>
        /// NroIdentidadPropietario
        /// </summary>
        [Campo()]
        public String NroIdentidadPropietario
        {
            get { return _strnroidentidadpropietario; }
            set { _strnroidentidadpropietario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdSector
        {
            get { return _intidsector; }
            set { _intidsector = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        /// <summary>
        /// IdEstadoFacturacion
        /// </summary>
        [Campo()]
        public Int16 IdEstadoFacturacion
        {
            get { return _intidestadofacturacion; }
            set { _intidestadofacturacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String Cartera
        {
            get { return strcartera; }
            set { strcartera = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdTarifa
        {
            get { return _intidtarifa; }
            set { _intidtarifa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String DireccionComplementaria
        {
            get { return _strdireccioncomplementaria; }
            set { _strdireccioncomplementaria = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16? IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16? Cortado
        {
            get { return _intcortado; }
            set { _intcortado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16? IdSistemaElectrico
        {
            get { return _intidsistemaelectrico; }
            set { _intidsistemaelectrico = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String AbreviaSector
        {
            get { return _strabreviasector; }
            set { _strabreviasector = value; }
        }
        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String DocumentoIdentidad
        {
            get { return _strdocumentoidentidad; }
            set { _strdocumentoidentidad = value; }
        }
        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String Mercado
        {
            get { return _Mercado; }
            set { _Mercado = value; }
        }
        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String PuntoMedicion
        {
            get { return _PuntoMedicion; }
            set { _PuntoMedicion = value; }
        }
        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String TipoServicio
        {
            get { return _TipoServicio; }
            set { _TipoServicio = value; }
        }
        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String EsLocalidadAfecta
        {
            get { return _EsLocalidadAfecta; }
            set { _EsLocalidadAfecta = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NivelTension
        {
            get { return _NivelTension; }
            set { _NivelTension = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 SectorTipico
        {
            get { return _SectorTipico; }
            set { _SectorTipico = value; }
        }
        /// <summary>
        /// SerieFabrica
        /// </summary>
        public String SerieFabrica
        {
            get { return _strseriefabrica; }
            set { _strseriefabrica = value; }
        }
        /// <summary>
        /// FactorMedicion
        /// </summary>
        public Decimal? FactorMedicion
        {
            get { return _decfactormedicion; }
            set { _decfactormedicion = value; }
        }
        /// <summary>
        /// FactorInterno
        /// </summary>
        public Decimal? FactorInterno
        {
            get { return _decfactorinterno; }
            set { _decfactorinterno = value; }
        }
        /// <summary>
        /// ConsumoActual
        /// </summary>
        public Decimal? ConsumoActual
        {
            get { return _decconsumoactual; }
            set { _decconsumoactual = value; }
        }
        /// <summary>
        /// ConsumoAnterior
        /// </summary>
        public Decimal? ConsumoAnterior
        {
            get { return _decconsumoanterior; }
            set { _decconsumoanterior = value; }
        }
        /// <summary>
        /// ConsumoAntAnterior
        /// </summary>
        public Decimal? ConsumoAntAnterior
        {
            get { return _decconsumoantanterior; }
            set { _decconsumoantanterior = value; }
        }
        /// <summary>
        /// Lectura
        /// </summary>
        public Decimal? Lectura
        {
            get { return _declectura; }
            set { _declectura = value; }
        }
        /// <summary>
        /// ConsumoPromedio
        /// </summary>
        public Decimal? ConsumoPromedio
        {
            get { return _decconsumopromedio; }
            set { _decconsumopromedio = value; }
        }
        /// <summary>
        /// Consumo1
        /// </summary>
        public Decimal? Consumo1
        {
            get { return _decconsumo1; }
            set { _decconsumo1 = value; }
        }
        /// <summary>
        /// Consumo2
        /// </summary>
        public Decimal? Consumo2
        {
            get { return _decconsumo2; }
            set { _decconsumo2 = value; }
        }
        /// <summary>
        /// Consumo3
        /// </summary>
        public Decimal? Consumo3
        {
            get { return _decconsumo3; }
            set { _decconsumo3 = value; }
        }
        /// <summary>
        /// PromedioSemestral
        /// </summary>
        public Decimal? PromedioSemestral
        {
            get { return _decpromediosemestral; }
            set { _decpromediosemestral = value; }
        }
        /// <summary>
        /// PromedioAnual
        /// </summary>
        public Decimal? PromedioAnual
        {
            get { return _decpromedioanual; }
            set { _decpromedioanual = value; }
        }
        /// <summary>
        /// PromedioHistorico
        /// </summary>
        public Decimal? PromedioHistorico
        {
            get { return _decpromediohistorico; }
            set { _decpromediohistorico = value; }
        }
        /// <summary>
        /// MaximoConsumo
        /// </summary>
        public Decimal? ConsumoMaximo
        {
            get { return _decmaximoconsumo; }
            set { _decmaximoconsumo = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public short IdPlanCondicion
        {
            get { return _idPlanCondicion; }
            set { _idPlanCondicion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo()]
        public Int16? IdDepartamento
        {
            get { return _intiddepartamento; }
            set { _intiddepartamento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo()]
        public Int16? IdProvincia
        {
            get { return _intidprovincia; }
            set { _intidprovincia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo()]
        public Int16? IdDistrito
        {
            get { return _intiddistrito; }
            set { _intiddistrito = value; }
        }

        [Campo()]
        public String UBIGEO
        {
            get { return _strubigeo; }
            set { _strubigeo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public Int16 IdCIIU
        {
            get { return _intidciiu; }
            set { _intidciiu = value; }
        }

        /// <summary>
        ///
        /// </summary>
        [Campo()]
        public String NombreCIIU
        {
            get { return _strnombreciiu; }
            set { _strnombreciiu = value; }
        }

        public Decimal? ConsumoOptimo
        {
            get { return _decconsumooptimo; }
            set { _decconsumooptimo = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacio.
        /// </summary>
        public clsNroServicioBasico()
        {
            _strdepartamento = "";
            _strprovincia = "";
            _strdistrito = "";
            _strlocalidad = "";
            //
            _intidnroservicio = 0;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dr"></param>
        public clsNroServicioBasico(DataRow dr)
        {
            if (dr == null)
                return;

            IdNroServicio = dr.Table.Columns.Contains("idnroservicio") ? Convert.ToInt32(dr["idnroservicio"]) : (Int32)0;
            Estado = dr.Table.Columns.Contains("estado") ? dr["estado"].ToString() : String.Empty;
            Categoria = dr.Table.Columns.Contains("categoria") ? dr["categoria"].ToString() : String.Empty;
            Servicio = dr.Table.Columns.Contains("servicio") ? dr["servicio"].ToString() : String.Empty;
            Direccion = dr.Table.Columns.Contains("direccion") ?
                Convert.IsDBNull(dr["direccion"]) ? String.Empty : dr["direccion"].ToString() : String.Empty;
            DireccionReferencial = dr.Table.Columns.Contains("direccionreferencial") ?
                Convert.IsDBNull(dr["direccionreferencial"]) ? String.Empty : dr["direccionreferencial"].ToString() : String.Empty;
            NombreNroServicio = dr.Table.Columns.Contains("nombrenroservicio") ?
                Convert.IsDBNull(dr["nombrenroservicio"]) ? String.Empty : dr["nombrenroservicio"].ToString() : String.Empty;
            Tarifa = dr.Table.Columns.Contains("tarifa") ? dr["tarifa"].ToString() : String.Empty;
            AbreviaTarifa = dr.Table.Columns.Contains("abreviatarifa") ? Convert.ToString(dr["abreviatarifa"]) : "";
            Departamento = dr.Table.Columns.Contains("departamento") ? dr["departamento"].ToString() : String.Empty;
            Provincia = dr.Table.Columns.Contains("provincia") ? dr["provincia"].ToString() : String.Empty;
            Distrito = dr.Table.Columns.Contains("distrito") ? dr["distrito"].ToString() : String.Empty;
            Localidad = dr.Table.Columns.Contains("localidad") ? Convert.ToString(dr["localidad"]) : "";
            IdTipoServicio = dr.Table.Columns.Contains("tiposervicio") ?
                Convert.IsDBNull(dr["tiposervicio"]) ? (Int16)0 : Convert.ToInt16(dr["tiposervicio"]) : (Int16)0;
            IdTipoIdentidadPropietario = dr.Table.Columns.Contains("tipoidentidadpropietario") ?
                Convert.IsDBNull(dr["tipoidentidadpropietario"]) ? (Int16)0 : Convert.ToInt16(dr["tipoidentidadpropietario"]) : (Int16)0;
            NroIdentidadPropietario = dr.Table.Columns.Contains("identidadpropietario") ? dr["identidadpropietario"].ToString() : String.Empty;

            IdEmpresa = dr.Table.Columns.Contains("idempresa") ?
                Convert.IsDBNull(dr["idempresa"]) ? (Int16)0 : Convert.ToInt16(dr["idempresa"]) : (Int16)0;
            IdUUNN = dr.Table.Columns.Contains("iduunn") ?
                Convert.IsDBNull(dr["iduunn"]) ? (Int16)0 : Convert.ToInt16(dr["iduunn"]) : (Int16)0;
            IdSector = dr.Table.Columns.Contains("idsector") ?
                Convert.IsDBNull(dr["idsector"]) ? (Int16)0 : Convert.ToInt16(dr["idsector"]) : (Int16)0;
            IdServicio = dr.Table.Columns.Contains("idservicio") ?
                Convert.IsDBNull(dr["idservicio"]) ? (Int16)0 : Convert.ToInt16(dr["idservicio"]) : (Int16)0;
            IdEstadoFacturacion = dr.Table.Columns.Contains("estadofacturacion") ?
                Convert.IsDBNull(dr["estadofacturacion"]) ? (Int16)0 : Convert.ToInt16(dr["estadofacturacion"]) : (Int16)0;
            IdCentroServicio = dr.Table.Columns.Contains("idcentroservicio") ?
                Convert.IsDBNull(dr["idcentroservicio"]) ? (Int16)0 : Convert.ToInt16(dr["idcentroservicio"]) : (Int16)0;
            Cartera = dr.Table.Columns.Contains("cartera") ? dr["cartera"].ToString() : String.Empty;

            IdDireccionCodificada = dr.Table.Columns.Contains("iddireccioncodificada") ?
                Convert.IsDBNull(dr["iddireccioncodificada"]) ? (Int32)0 : Convert.ToInt32(dr["iddireccioncodificada"]) : (Int32)0;

            IdElementoElectrico = dr.Table.Columns.Contains("idelementoelectrico") ?
                Convert.IsDBNull(dr["idelementoelectrico"]) ? (Int32)0 : Convert.ToInt32(dr["idelementoelectrico"]) : (Int32)0;

            IdTarifa = dr.Table.Columns.Contains("idtarifa") ?
                Convert.IsDBNull(dr["idtarifa"]) ? (Int16)0 : Convert.ToInt16(dr["idtarifa"]) : (Int16)0;

            DireccionComplementaria = dr.Table.Columns.Contains("direccioncomplementaria") ?
                Convert.IsDBNull(dr["direccioncomplementaria"]) ? "" : dr["direccioncomplementaria"].ToString().Trim() : "";

            IdEstado = dr.Table.Columns.Contains("idestado") ?
                Convert.IsDBNull(dr["idestado"]) ? (Int16?)null : Convert.ToInt16(dr["idestado"]) : (Int16?)null;

            Cortado = dr.Table.Columns.Contains("cortado") ?
                Convert.IsDBNull(dr["cortado"]) ? (Int16?)null : Convert.ToInt16(dr["cortado"]) : (Int16?)null;

            IdSistemaElectrico = dr.Table.Columns.Contains("idsistemaelectrico") ?
                Convert.IsDBNull(dr["idsistemaelectrico"]) ? (Int16?)null : Convert.ToInt16(dr["idsistemaelectrico"]) : (Int16?)null;

            Modalidad = dr.Table.Columns.Contains("modalidad") ?
                Convert.IsDBNull(dr["modalidad"]) ? String.Empty : dr["modalidad"].ToString() : String.Empty;

            if (dr.Table.Columns.Contains("nombrecartera"))
                NombreCartera = Convert.IsDBNull(dr["nombrecartera"]) ? String.Empty : dr["nombrecartera"].ToString().Trim();

            if (dr.Table.Columns.Contains("idfase"))
                IdFase = Convert.IsDBNull(dr["idfase"]) ? (Int16)0 : Convert.ToInt16(dr["idfase"]);

            if (dr.Table.Columns.Contains("nombrefase"))
                NombreFase = Convert.IsDBNull(dr["nombrefase"]) ? String.Empty : dr["nombrefase"].ToString().Trim();

            if (dr.Table.Columns.Contains("abreviasector"))
                AbreviaSector = Convert.IsDBNull(dr["abreviasector"]) ? String.Empty : dr["abreviasector"].ToString().Trim();

            if (dr.Table.Columns.Contains("documentoidentidad"))
                DocumentoIdentidad = Convert.IsDBNull(dr["documentoidentidad"]) ? String.Empty : dr["documentoidentidad"].ToString().Trim();

            if (dr.Table.Columns.Contains("mercado"))
                Mercado = Convert.IsDBNull(dr["mercado"]) ? String.Empty : dr["mercado"].ToString().Trim();

            if (dr.Table.Columns.Contains("puntomedicion"))
                PuntoMedicion = Convert.IsDBNull(dr["puntomedicion"]) ? String.Empty : dr["puntomedicion"].ToString().Trim();

            if (dr.Table.Columns.Contains("tiposervicionombre"))
                TipoServicio = Convert.IsDBNull(dr["tiposervicionombre"]) ? String.Empty : dr["tiposervicionombre"].ToString().Trim();

            if (dr.Table.Columns.Contains("eslocalidadafecta"))
                EsLocalidadAfecta = Convert.IsDBNull(dr["eslocalidadafecta"]) ? String.Empty : dr["eslocalidadafecta"].ToString().Trim();

            if (dr.Table.Columns.Contains("sectortipico"))
                SectorTipico = Convert.IsDBNull(dr["sectortipico"]) ? (Int16)0 : Convert.ToInt16(dr["sectortipico"]);

            if (dr.Table.Columns.Contains("niveltension"))
                NivelTension = Convert.IsDBNull(dr["niveltension"]) ? String.Empty : dr["niveltension"].ToString().Trim();

            if (dr.Table.Columns.Contains("seriefabrica")) SerieFabrica = dr["seriefabrica"] == DBNull.Value ? String.Empty : dr["seriefabrica"].ToString();
            if (dr.Table.Columns.Contains("factormedicion")) FactorMedicion = dr["factormedicion"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["factormedicion"]);
            if (dr.Table.Columns.Contains("factorinterno")) FactorInterno = dr["factorinterno"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["factorinterno"]);
            if (dr.Table.Columns.Contains("consumoactual")) ConsumoActual = dr["consumoactual"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["consumoactual"]);
            if (dr.Table.Columns.Contains("consumoanterior")) ConsumoAnterior = dr["consumoanterior"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["consumoanterior"]);
            if (dr.Table.Columns.Contains("consumoantanterior")) ConsumoAntAnterior = dr["consumoantanterior"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["consumoantanterior"]);
            if (dr.Table.Columns.Contains("lectura")) Lectura = dr["lectura"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["lectura"]);
            if (dr.Table.Columns.Contains("consumopromedio")) ConsumoPromedio = dr["consumopromedio"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["consumopromedio"]);

            if (dr.Table.Columns.Contains("Consumo1")) Consumo1 = dr["Consumo1"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["Consumo1"]);
            if (dr.Table.Columns.Contains("Consumo2")) Consumo2 = dr["Consumo2"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["Consumo2"]);
            if (dr.Table.Columns.Contains("Consumo3")) Consumo3 = dr["Consumo3"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["Consumo3"]);
            if (dr.Table.Columns.Contains("PromedioSemestral")) PromedioSemestral = dr["PromedioSemestral"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PromedioSemestral"]);
            if (dr.Table.Columns.Contains("PromedioAnual")) PromedioAnual = dr["PromedioAnual"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PromedioAnual"]);
            if (dr.Table.Columns.Contains("PromedioHistorico")) PromedioHistorico = dr["PromedioHistorico"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["PromedioHistorico"]);
            if (dr.Table.Columns.Contains("ConsumoMaximo")) ConsumoMaximo = dr["ConsumoMaximo"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["ConsumoMaximo"]);

            IdPlanCondicion = dr.Table.Columns.Contains("idplancondicion") ? Convert.ToInt16(dr["idplancondicion"]) : (short)0;

            if (dr.Table.Columns.Contains("iddepartamento")) IdDepartamento = dr["iddepartamento"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["iddepartamento"]);
            if (dr.Table.Columns.Contains("idprovincia")) IdProvincia = dr["idprovincia"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["idprovincia"]);
            if (dr.Table.Columns.Contains("iddistrito")) IdDistrito = dr["iddistrito"] == DBNull.Value ? (Int16?)null : Convert.ToInt16(dr["iddistrito"]);

            if (dr.Table.Columns.Contains("ubigeo")) UBIGEO = dr["ubigeo"] == DBNull.Value ? String.Empty : dr["ubigeo"].ToString();
            if (dr.Table.Columns.Contains("IdCIIU")) IdCIIU = dr["IdCIIU"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(dr["IdCIIU"]);
            if (dr.Table.Columns.Contains("NombreCIIU")) NombreCIIU = dr["NombreCIIU"] == DBNull.Value ? String.Empty : dr["NombreCIIU"].ToString();
            if (dr.Table.Columns.Contains("ConsumoOptimo")) ConsumoOptimo = dr["ConsumoOptimo"] == DBNull.Value ? (Decimal?)null : Convert.ToDecimal(dr["ConsumoOptimo"]);
        }

        /// <summary>
        /// Constructor para establecer valores a las propiedades:
        /// IdEmpresa y IdUnidadNegocio.
        /// </summary>
        /// <param name="argIdEmpresa"></param>
        /// <param name="argIdUnidadNegocio"></param>
        public clsNroServicioBasico(Int16 argIdEmpresa, Int16 argIdUnidadNegocio)
        {
            _intidempresa = argIdEmpresa;
            _intiduunn = argIdUnidadNegocio;
        }

        #endregion Constructor
    }
    #endregion

    #region Generales
    public static class CargarEntidad
    {
        /// <summary>
        /// Establece los valores del DataRow para cada propiedad de la Entidad.
        /// </summary>
        /// <param name="entidad"></param>
        /// <param name="registro"></param>
        public static void EstablecerDataRow(Object entidad, DataRow registro)
        {
            if (registro == null) return;

            Type tipoentidad = entidad.GetType();
            String _strnombrepropiedad = "";
            String _strtipodato;
            String _strtipodatofull;

            foreach (PropertyInfo pi in tipoentidad.GetProperties())
            {
                Attribute atributo = Attribute.GetCustomAttribute(pi, typeof(CampoAttribute));

                if (atributo != null)
                {
                    _strnombrepropiedad = pi.Name;

                    if (registro.Table.Columns.Contains(_strnombrepropiedad))
                    {
                        // Si el valor del campo DataRow es nulo, no asignar valor.

                        if (Convert.IsDBNull(registro[_strnombrepropiedad])) { continue; }

                        // Obtener el tipo de dato de la propiedad.

                        _strtipodato = pi.PropertyType.Name.ToString();
                        _strtipodatofull = pi.PropertyType.FullName.ToString();

                        // Comprar tipos de datos para asignar valor correcto.

                        Object valor = registro[_strnombrepropiedad];

                        // Asignar valor a la propiedad según tipo dato.

                        if (_strtipodatofull.Contains(TypeCode.Int32.ToString()))
                            //pi.SetValue(entidad, (_strtipodato.Contains("Nullable") ? (Int32?)valor : Convert.ToInt32(valor)), null);
                            pi.SetValue(entidad, Convert.ToInt32(valor), null);
                        else if (_strtipodatofull.Contains(TypeCode.Int16.ToString()))
                            //pi.SetValue(entidad, (_strtipodato.Contains("Nullable") ? (Int16?)valor : Convert.ToInt16(valor)), null);
                            pi.SetValue(entidad, Convert.ToInt16(valor), null);
                        else if (_strtipodatofull.Contains(TypeCode.Decimal.ToString()))
                            //pi.SetValue(entidad, (_strtipodato.Contains("Nullable") ? (Decimal?)valor : Convert.ToDecimal(valor)), null);
                            pi.SetValue(entidad, Convert.ToDecimal(valor), null);
                        else if (_strtipodatofull.Contains(TypeCode.DateTime.ToString()))
                            //pi.SetValue(entidad, (_strtipodato.Contains("Nullable") ? (DateTime?)valor : Convert.ToDateTime(valor)), null);
                            pi.SetValue(entidad, Convert.ToDateTime(valor), null);
                        else if (_strtipodatofull.Contains(TypeCode.Boolean.ToString()))
                            //pi.SetValue(entidad, (_strtipodato.Contains("Nullable") ? (Boolean?)valor : Convert.ToBoolean(valor)), null);
                            pi.SetValue(entidad, Convert.ToBoolean(valor), null);
                        else
                            pi.SetValue(entidad, registro[_strnombrepropiedad].ToString(), null);
                    }
                }
            }
        }
    }

    public class clsDeviceInfo
    {
        private String _strformato;
        private Decimal _decpagewidth;
        private Decimal _decpageheight;
        private Decimal _decmargintop;
        private Decimal _decmarginleft;
        private Decimal _decmarginright;
        private Decimal _decmarginbottom;
        private Boolean _bolesvertical;
        private Boolean _bolescentrimetros;
        private Int16 _intnrocolumnas;

        public String Formato
        {
            get { return _strformato; }
            set { _strformato = value; }
        }
        public Decimal PageWidht
        {
            get { return _decpagewidth; }
            set { _decpagewidth = value; }
        }
        public Decimal PageHeight
        {
            get { return _decpageheight; }
            set { _decpageheight = value; }
        }
        public Decimal MarginTop
        {
            get { return _decmargintop; }
            set { _decmargintop = value; }
        }
        public Decimal MarginLeft
        {
            get { return _decmarginleft; }
            set { _decmarginleft = value; }
        }
        public Decimal MarginRight
        {
            get { return _decmarginright; }
            set { _decmarginright = value; }
        }
        public Decimal MarginBottom
        {
            get { return _decmarginbottom; }
            set { _decmarginbottom = value; }
        }
        public Boolean EsVertical
        {
            get { return _bolesvertical; }
            set { _bolesvertical = value; }
        }
        public Int16 NroColumnas
        {
            get { return _intnrocolumnas; }
            set { _intnrocolumnas = value; }
        }
        public Boolean EsCentimetros
        {
            get { return _bolescentrimetros; }
            set { _bolescentrimetros = value; }
        }

        public clsDeviceInfo()
        {
            this.Formato = "EMF";
            this.PageWidht = (Decimal)21.5;
            this.PageHeight = (Decimal)29.7;
            this.MarginTop = (Decimal)0;
            this.MarginLeft = (Decimal)0;
            this.MarginRight = (Decimal)0;
            this.MarginBottom = (Decimal)0;
            this.EsVertical = true;
            this.EsCentimetros = true;
            this.NroColumnas = 1;
        }

        public String DeviceInfo()
        {
            String _strmedida = (this.EsCentimetros == true ? "cm" : "in");
            Decimal Factor = this.EsCentimetros ? (Decimal)1 : (Decimal)0.3937;

            String deviceInfo =
                 "<DeviceInfo>" +
                 "  <OutputFormat>" + this.Formato.ToString() + "</OutputFormat>" +
                 "  <Columns>" + this.NroColumnas.ToString() + "</Columns>";

            if (this.EsVertical)
            {
                deviceInfo = deviceInfo +
                    "  <PageWidth>" + (this.PageWidht * Factor).ToString() + _strmedida + "</PageWidth>" +
                    "  <PageHeight>" + (this.PageHeight * Factor).ToString() + _strmedida + "</PageHeight>";
            }
            else
            {
                deviceInfo = deviceInfo +
                    "  <PageWidth>" + (this.PageHeight * Factor).ToString() + _strmedida + "</PageWidth>" +
                    "  <PageHeight>" + (this.PageWidht * Factor).ToString() + _strmedida + "</PageHeight>";
            }

            deviceInfo = deviceInfo +
                "  <MarginTop>" + (this.MarginTop * Factor).ToString() + _strmedida + "</MarginTop>" +
                "  <MarginLeft>" + (this.MarginLeft * Factor).ToString() + _strmedida + "</MarginLeft>" +
                "  <MarginRight>" + (this.MarginRight * Factor).ToString() + _strmedida + "</MarginRight>" +
                "  <MarginBottom>" + (this.MarginBottom * Factor).ToString() + _strmedida + "</MarginBottom>" +
                "</DeviceInfo>";

            return deviceInfo.ToString();
        }
    }

    public class clsSuministroRecibo
    {
        private Int32 _intperiodo;
        private Int32 _intidnroservicio;

        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public clsSuministroRecibo()
        {
        }

        public clsSuministroRecibo(Int32 IdNroServicio, Int32 Periodo)
        {
            this.IdNroServicio = IdNroServicio;
            this.Periodo = Periodo;
        }

        public clsSuministroRecibo(DataRow dr)
        {
            if (dr == null) return;

            if (dr.Table.Columns.Contains("idnroservicio")) this.IdNroServicio = (Int32)dr["idnroservicio"];
            if (dr.Table.Columns.Contains("periodo")) this.Periodo = (Int32)dr["periodo"];
        }
    }

    public class clsListaSuministroRecibo
    {
        private List<clsSuministroRecibo> _objelementos = new List<clsSuministroRecibo>();

        public List<clsSuministroRecibo> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        public clsListaSuministroRecibo()
        { }

        public clsListaSuministroRecibo(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsSuministroRecibo(_drw));
            }
        }
    }
    #endregion

    #region ReciboMenor
    public class clsImprimirReciboMenor
    {
        #region Campos

        private Int32 _intidnroservicio;
        private Int32 _intperiodo;
        private Int16 _esduplicado;
        private Int16 _esvistaprevia;
        private String _strnombreimpresora;
        private String _strformato;
        private String _strrender;
        private String _strrutasalidareporte;
        private String _strcartera;
        private Int16 _esruta;
        public Boolean _bolesimpresora;
        private Int16 _paginastotales;
        private Int32 _registrostotales;
        public clsDeviceInfo _objdevice = new clsDeviceInfo();
        private clsListaSuministroRecibo _listasuminitros = new clsListaSuministroRecibo();
        private clsListaReciboMenorCabecera _objlistarecibomenorcabecera = new clsListaReciboMenorCabecera();
        private clsListaReciboMenorConcepto _objlistarecibomenorconcepto = new clsListaReciboMenorConcepto();
        private clsListaReciboMenorConsumo _objlistarecibomenorconsumo = new clsListaReciboMenorConsumo();
        private clsListaReciboMenorLectura _objlistarecibomenorlectura = new clsListaReciboMenorLectura();

        #endregion

        #region Propiedades

        public clsDeviceInfo DeviceInfo
        {
            get { return _objdevice; }
            set { _objdevice = value; }
        }

        public String NombreImpresora
        {
            get { return _strnombreimpresora; }
            set { _strnombreimpresora = value; }
        }
        public String Formato
        {
            get { return _strformato; }
            set { _strformato = value; }
        }
        public String Render
        {
            get { return _strrender; }
            set { _strrender = value; }
        }

        public String RutaSalidaReporte
        {
            get { return _strrutasalidareporte; }
            set { _strrutasalidareporte = value; }
        }

        public Int16 EsDuplicado
        {
            get { return _esduplicado; }
            set { _esduplicado = value; }
        }
        public Int16 EsRuta
        {
            get { return _esruta; }
            set { _esruta = value; }
        }
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }
        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }
        public Boolean EsImpresora
        {
            get { return _bolesimpresora; }
            set { _bolesimpresora = value; }
        }
        public clsListaReciboMenorCabecera ListaReciboMenorCabecera
        {
            get { return _objlistarecibomenorcabecera; }
            set { _objlistarecibomenorcabecera = value; }
        }
        public clsListaReciboMenorConcepto ListaReciboMenorConcepto
        {
            get { return _objlistarecibomenorconcepto; }
            set { _objlistarecibomenorconcepto = value; }
        }
        public clsListaReciboMenorConsumo ListaReciboMenorConsumo
        {
            get { return _objlistarecibomenorconsumo; }
            set { _objlistarecibomenorconsumo = value; }
        }
        public clsListaReciboMenorLectura ListaReciboMenorLectura
        {
            get { return _objlistarecibomenorlectura; }
            set { _objlistarecibomenorlectura = value; }
        }
        public clsListaSuministroRecibo ListaSuministro
        {
            get { return _listasuminitros; }
            set { _listasuminitros = value; }
        }
        public String EsCartera
        {
            get { return _strcartera; }
            set { _strcartera = value; }
        }
        public Int16 EsVistaPrevia
        {
            get { return _esvistaprevia; }
            set { _esvistaprevia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 PaginasTotales
        {
            get { return _paginastotales; }
            set { _paginastotales = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 RegistrosTotales
        {
            get { return _registrostotales; }
            set { _registrostotales = value; }
        }

        #endregion

        #region Constructor

        public clsImprimirReciboMenor()
        { }

        public clsImprimirReciboMenor(DataRow dr)
        {
            if (dr == null) return;

            if (dr.Table.Columns.Contains("idnroservicio")) this.IdNroServicio = (Int32)dr["idnroservicio"];
            if (dr.Table.Columns.Contains("periodo")) this.Periodo = (Int32)dr["periodo"];
        }

        public clsImprimirReciboMenor(Int32 IdNroServicio, Int32 Periodo)
        {
            this.IdNroServicio = IdNroServicio;
            this.Periodo = Periodo;
        }
        #endregion
    }

    public class clsReciboMenorCabecera
    {
        #region Campos
        private Int16 iIdTipoDocumento;
        private String sNroDocumento;
        private String sNombreDistrito;
        private String sNombreProvincia;
        private Int32 iIdNroServicio;
        private Int32 iPeriodo;
        private String sNombreNroServicio;
        private String sDireccion;
        private String sRUC;
        private Int32 iIdAnoComercial;
        private Int16 iIdMesComercial;
        private String sPeriodo;
        private DateTime dFechaEmision;
        private DateTime dFechaVencimiento;
        private Decimal deTotalMes;
        private Decimal deTotalAPagar;
        private Decimal deImporteAplicado;
        private Decimal deDeudaAnteriorInicial;
        private Int16 iNroMesesDeuda;
        private String sRutaReparto;
        private Int32? iIdDireccionCodificadaReparto;
        private String sDireccionCodificadaReparto;
        private Int16 iIdEmpresaServicio;
        private Int16 iIdMoneda;
        private String sSimboloMoneda;
        private Int16 iIdSector;
        private Boolean bParaCortar;
        private String _strimagenempresa;
        private String _strimagensector;
        private Boolean _bduplicado;
        private String _strrangconsumo;
        private String _strdireccionreferencial;
        private Int16 _intesimagen;
        private Int32 _intnropagina;

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        private int _intnropaginaA4;

        #endregion

        private Int32 _intidrutareparto;
        private String _strimagencara1;
        private String _strimagencara2;
        private String _strimagencara3;
        private Int16 _intidestrato1;
        private Int16 _intidestrato2;
        private Int16 _intidestrato3;
        private String _strnombreperiodocomercial;
        private String _strcodigobarras;
        private Int16 _intincluircaritas;
        private String _datosvarios;
        private Int16 _intidempresa;

        private string _strRucEmpresa;
        private short _intMostrarCaritas; //CanviaDev_2020-05-22
        private short _intMostrarCortado; //CanviaDev_2020-05-22

        private string _strmensajecortadocaritas; // Carlos López 20082020
        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>

        public String CodigoBarras
        {
            get { return _strcodigobarras; }
            set { _strcodigobarras = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public String NombrePeriodoComercial
        {
            get { return _strnombreperiodocomercial; }
            set { _strnombreperiodocomercial = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public Int16 IncluirCaritas
        {
            get { return _intincluircaritas; }
            set { _intincluircaritas = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public String ImagenCara1
        {
            get { return _strimagencara1; }
            set { _strimagencara1 = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public String ImagenCara2
        {
            get { return _strimagencara2; }
            set { _strimagencara2 = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public String ImagenCara3
        {
            get { return _strimagencara3; }
            set { _strimagencara3 = value; }
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //
        //public String CaraFeliz
        //{
        //    get { return _strcarafeliz; }
        //    set { _strcarafeliz = value; }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //
        //public String CaraTriste
        //{
        //    get { return _strcaratriste; }
        //    set { _strcaratriste = value; }
        //}

        /// <summary>
        /// 
        /// </summary>

        public Int16 IdEstrato1
        {
            get { return _intidestrato1; }
            set { _intidestrato1 = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public Int16 IdEstrato2
        {
            get { return _intidestrato2; }
            set { _intidestrato2 = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public Int16 IdEstrato3
        {
            get { return _intidestrato3; }
            set { _intidestrato3 = value; }
        }

        /// <summary>
        /// 
        /// </summary>

        public Int16 EsImagen
        {
            get { return _intesimagen; }
            set { _intesimagen = value; }
        }


        public String DireccionReferencial
        {
            get { return _strdireccionreferencial; }
            set { _strdireccionreferencial = value; }
        }


        public Int16 IdTipoDocumento
        {
            get { return iIdTipoDocumento; }
            set { iIdTipoDocumento = value; }
        }


        public String NroDocumento
        {
            get { return sNroDocumento; }
            set { sNroDocumento = value; }
        }


        public String NombreDistrito
        {
            get { return sNombreDistrito; }
            set { sNombreDistrito = value; }
        }


        public String NombreProvincia
        {
            get { return sNombreProvincia; }
            set { sNombreProvincia = value; }
        }


        public Int32 IdNroServicio
        {
            get { return iIdNroServicio; }
            set { iIdNroServicio = value; }
        }


        public String NombreNroServicio
        {
            get { return sNombreNroServicio; }
            set { sNombreNroServicio = value; }
        }


        public String Direccion
        {
            get { return sDireccion; }
            set { sDireccion = value; }
        }


        public String RUC
        {
            get { return sRUC; }
            set { sRUC = value; }
        }


        public Int32 IdAnoComercial
        {
            get { return iIdAnoComercial; }
            set { iIdAnoComercial = value; }
        }


        public Int16 IdMesComercial
        {
            get { return iIdMesComercial; }
            set { iIdMesComercial = value; }
        }


        public Int32 Periodo
        {
            get { return iPeriodo; }
            set { iPeriodo = value; }
        }


        public DateTime FechaEmision
        {
            get { return dFechaEmision; }
            set { dFechaEmision = value; }
        }


        public DateTime FechaVencimiento
        {
            get { return dFechaVencimiento; }
            set { dFechaVencimiento = value; }
        }


        public Decimal TotalMes
        {
            get { return deTotalMes; }
            set { deTotalMes = value; }
        }


        public Decimal TotalAPagar
        {
            get { return deTotalAPagar; }
            set { deTotalAPagar = value; }
        }


        public Decimal ImporteAplicado
        {
            get { return deImporteAplicado; }
            set { deImporteAplicado = value; }
        }


        public Decimal DeudaAnteriorInicial
        {
            get { return deDeudaAnteriorInicial; }
            set { deDeudaAnteriorInicial = value; }
        }


        public Int16 NroMesesDeuda
        {
            get { return iNroMesesDeuda; }
            set { iNroMesesDeuda = value; }
        }


        public String RutaReparto
        {
            get { return sRutaReparto; }
            set { sRutaReparto = value; }
        }


        public Int32? IdDireccionCodificadaReparto
        {
            get { return iIdDireccionCodificadaReparto; }
            set { iIdDireccionCodificadaReparto = value; }
        }


        public String DireccionCodificadaReparto
        {
            get { return sDireccionCodificadaReparto; }
            set { sDireccionCodificadaReparto = value; }
        }


        public Int16 IdEmpresaServicio
        {
            get { return iIdEmpresaServicio; }
            set { iIdEmpresaServicio = value; }
        }


        public Int16 IdMoneda
        {
            get { return iIdMoneda; }
            set { iIdMoneda = value; }
        }


        public String SimboloMoneda
        {
            get { return sSimboloMoneda; }
            set { sSimboloMoneda = value; }
        }


        public Int16 IdSector
        {
            get { return iIdSector; }
            set { iIdSector = value; }
        }


        public Boolean ParaCortar
        {
            get { return bParaCortar; }
            set { bParaCortar = value; }
        }


        public String ImagenEmpresa
        {
            get { return _strimagenempresa; }
            set { _strimagenempresa = value; }
        }


        public String ImagenSector
        {
            get { return _strimagensector; }
            set { _strimagensector = value; }
        }

        public Boolean Duplicado
        {
            get { return _bduplicado; }
            set { _bduplicado = value; }
        }


        public String RangoConsumo
        {
            get { return _strrangconsumo; }
            set { _strrangconsumo = value; }
        }


        public Int32 NroPagina
        {
            get { return _intnropagina; }
            set { _intnropagina = value; }
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public int NroPaginaA4
        {
            get { return _intnropaginaA4; }
            set { _intnropaginaA4 = value; }
        }

        #endregion

        public Int32 IdRutaReparto
        {
            get { return _intidrutareparto; }
            set { _intidrutareparto = value; }
        }

        /// <summary>
        /// Sirve para incluir valores adicionales separados por Coma (;).
        /// Se debe incluir el nombre del campo y el valor del mismo separado por dos puntos (:).
        /// Por ejemplo "codigofise:501001000;"
        /// </summary>

        public String DatosVarios
        {
            get { return _datosvarios; }
            set { _datosvarios = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public string RucEmpresa
        {
            get { return _strRucEmpresa; }
            set { _strRucEmpresa = value; }
        }

        [Campo()]
        public short MostrarCaritas
        {
            get { return _intMostrarCaritas; }
            set { _intMostrarCaritas = value; }
        }

        [Campo()]
        public short MostrarCortado
        {
            get { return _intMostrarCortado; }
            set { _intMostrarCortado = value; }
        }

        [Campo()]
        public string MensajeCortadoCaritas
        {
            get { return _strmensajecortadocaritas; }
            set { _strmensajecortadocaritas = value; }
        }

        public override bool Equals(object obj)
        {
            clsReciboMenorCabecera oCabecera = (clsReciboMenorCabecera)obj;
            return (this.IdNroServicio == oCabecera.IdNroServicio && this.Periodo == oCabecera.Periodo);
        }

        public override int GetHashCode()
        {
            return this.IdNroServicio ^ this.Periodo;
        }

        #endregion

        public clsReciboMenorCabecera()
        { }

        public clsReciboMenorCabecera(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }

        public String ObtenerValorDatosVarios(string clave)
        {
            String valor = String.Empty;

            if (!String.IsNullOrEmpty(this.DatosVarios) && this.DatosVarios.Contains(clave))
            {
                String[] listadatosvarios = this.DatosVarios.Split(';');
                String[] cadenavalor;

                for (int i = 0; i < listadatosvarios.Length; i++)
                {
                    if (listadatosvarios[i].Contains(":"))
                    {
                        cadenavalor = listadatosvarios[i].Split(':');

                        if (cadenavalor[0].Equals(clave))
                            valor = cadenavalor[1];
                    }
                }
            }

            return valor;
        }
    }

    public class clsListaReciboMenorCabecera
    {
        #region Campos

        private List<clsReciboMenorCabecera> _objelementos = new List<clsReciboMenorCabecera>();

        #endregion Campos

        #region Propiedades

        [CampoLista()]
        public List<clsReciboMenorCabecera> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMenorCabecera()
        {
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMenorCabecera(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];

            Int32 colIdTipoDocumento = dr.GetOrdinal("IdTipoDocumento");
            Int32 colNroDocumento = dr.GetOrdinal("NroDocumento");
            Int32 colNombreDistrito = dr.GetOrdinal("NombreDistrito");
            Int32 colNombreProvincia = dr.GetOrdinal("NombreProvincia");
            Int32 colIdNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colNombreNroServicio = dr.GetOrdinal("NombreNroServicio");
            Int32 colDireccion = dr.GetOrdinal("Direccion");
            Int32 colRUC = dr.GetOrdinal("RUC");
            Int32 colIdAnoComercial = dr.GetOrdinal("IdAnoComercial");
            Int32 colIdMesComercial = dr.GetOrdinal("IdMesComercial");
            Int32 colFechaEmision = dr.GetOrdinal("FechaEmision");
            Int32 colFechaVencimiento = dr.GetOrdinal("FechaVencimiento");
            Int32 colTotalMes = dr.GetOrdinal("TotalMes");
            Int32 colTotalAPagar = dr.GetOrdinal("TotalAPagar");
            Int32 colImporteAplicado = dr.GetOrdinal("ImporteAplicado");
            Int32 colDeudaAnteriorInicial = dr.GetOrdinal("DeudaAnteriorInicial");
            Int32 colNroMesesDeuda = dr.GetOrdinal("NroMesesDeuda");
            Int32 colRutaReparto = dr.GetOrdinal("RutaReparto");
            Int32 colIdDireccionCodificadaReparto = dr.GetOrdinal("IdDireccionCodificadaReparto");
            Int32 colDireccionCodificadaReparto = dr.GetOrdinal("DireccionCodificadaReparto");
            Int32 colIdEmpresaServicio = dr.GetOrdinal("IdEmpresaServicio");
            Int32 colIdMoneda = dr.GetOrdinal("IdMoneda");
            Int32 colSimboloMoneda = dr.GetOrdinal("SimboloMoneda");
            Int32 colIdSector = dr.GetOrdinal("IdSector");
            Int32 colImagenEmpresa = dr.GetOrdinal("ImagenEmpresa");
            Int32 colImagenSector = dr.GetOrdinal("ImagenSector");
            Int32 colDuplicado = dr.GetOrdinal("Duplicado");
            Int32 colrangoconsumo = dr.GetOrdinal("RangoConsumo");
            Int32 coldireccionreferencial = dr.GetOrdinal("direccionreferencial");
            Int32 colesimagen = dr.GetOrdinal("EsImagen");
            Int32 colnropagina = dr.GetOrdinal("NroPagina");
            Int32 colidrutareparto = dr.GetOrdinal("IdRutaReparto");
            Int32 colidempresa = dr.GetOrdinal("IdEmpresa");
            int colRucEmpresa = dr.GetOrdinal("RucEmpresa");

            Int32 colidestrato1 = 0;
            Int32 colidestrato2 = 0;
            Int32 colidestrato3 = 0;
            Int32 colincluircaritas = 0;

            DataTable _dt = dr.GetSchemaTable();

            Int16 _intidestrato1 = 0;
            Int16 _intidestrato2 = 0;
            Int16 _intidestrato3 = 0;
            Int16 _intincluircaritas = 0;

            Int32 colMostrarCaritas = dr.GetOrdinal("MostrarCaritas");
            Int32 colMostrarCortado = dr.GetOrdinal("MostrarCortado");
            //

            /*
             * Agregado el 20082020 - Carlos López
             */
            Int32 colMensajeCortadoCaritas = dr.GetOrdinal("MensajeCortadoCaritas");

            foreach (DataRow item in _dt.Rows)
            {
                if (item[0].ToString() == "IdEstrato1")
                {
                    colidestrato1 = dr.GetOrdinal("IdEstrato1");
                    _intidestrato1 = 1;
                }
                if (item[0].ToString() == "IdEstrato2")
                {
                    colidestrato2 = dr.GetOrdinal("IdEstrato2");
                    _intidestrato2 = 1;
                }
                if (item[0].ToString() == "IdEstrato3")
                {
                    colidestrato3 = dr.GetOrdinal("IdEstrato3");
                    _intidestrato3 = 1;
                }
                if (item[0].ToString() == "IncluirCaritas")
                {
                    colincluircaritas = dr.GetOrdinal("IncluirCaritas");
                    _intincluircaritas = 1;
                }
            }

            while (dr.Read())
            {
                clsReciboMenorCabecera oReciboMenorCabecera = new clsReciboMenorCabecera();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                oReciboMenorCabecera.DatosVarios = dr["DatosVarios"].ToString();
                // asignar los valores al objeto.
                oReciboMenorCabecera.IdTipoDocumento = Convert.ToInt16(values[colIdTipoDocumento]);
                oReciboMenorCabecera.NroDocumento = Convert.ToString(values[colNroDocumento]);
                oReciboMenorCabecera.NombreDistrito = Convert.ToString(values[colNombreDistrito]);
                oReciboMenorCabecera.NombreProvincia = Convert.ToString(values[colNombreProvincia]);
                oReciboMenorCabecera.IdNroServicio = Convert.ToInt32(values[colIdNroServicio]);
                oReciboMenorCabecera.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMenorCabecera.NombreNroServicio = Convert.ToString(values[colNombreNroServicio]);
                oReciboMenorCabecera.Direccion = Convert.ToString(values[colDireccion]);
                oReciboMenorCabecera.RUC = Convert.ToString(values[colRUC]);
                oReciboMenorCabecera.IdAnoComercial = Convert.ToInt32(values[colIdAnoComercial]);
                oReciboMenorCabecera.IdMesComercial = Convert.ToInt16(values[colIdMesComercial]);
                oReciboMenorCabecera.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMenorCabecera.FechaEmision = Convert.ToDateTime(values[colFechaEmision]);
                oReciboMenorCabecera.FechaVencimiento = Convert.ToDateTime(values[colFechaVencimiento]);
                oReciboMenorCabecera.TotalMes = Convert.ToDecimal(values[colTotalMes]);
                oReciboMenorCabecera.TotalAPagar = Convert.ToDecimal(values[colTotalAPagar]);
                oReciboMenorCabecera.ImporteAplicado = Convert.ToDecimal(values[colImporteAplicado]);
                oReciboMenorCabecera.DeudaAnteriorInicial = Convert.ToDecimal(values[colDeudaAnteriorInicial]);
                oReciboMenorCabecera.NroMesesDeuda = Convert.ToInt16(values[colNroMesesDeuda]);
                oReciboMenorCabecera.RutaReparto = Convert.ToString(values[colRutaReparto]);
                oReciboMenorCabecera.IdDireccionCodificadaReparto = Convert.IsDBNull(values[colIdDireccionCodificadaReparto]) ? (Int32?)null : Convert.ToInt32(values[colIdDireccionCodificadaReparto]);
                oReciboMenorCabecera.DireccionCodificadaReparto = Convert.ToString(values[colDireccionCodificadaReparto]);
                oReciboMenorCabecera.IdEmpresaServicio = Convert.ToInt16(values[colIdEmpresaServicio]);
                oReciboMenorCabecera.IdMoneda = Convert.ToInt16(values[colIdMoneda]);
                oReciboMenorCabecera.SimboloMoneda = Convert.ToString(values[colSimboloMoneda]);
                oReciboMenorCabecera.IdSector = Convert.ToInt16(values[colIdSector]);
                oReciboMenorCabecera.ImagenEmpresa = values[colImagenEmpresa].ToString();
                oReciboMenorCabecera.ImagenSector = values[colImagenSector].ToString();
                oReciboMenorCabecera.Duplicado = Convert.ToBoolean(values[colDuplicado]);
                oReciboMenorCabecera.RangoConsumo = values[colrangoconsumo].ToString();
                oReciboMenorCabecera.DireccionReferencial = values[coldireccionreferencial].ToString();
                oReciboMenorCabecera.EsImagen = Convert.ToInt16(values[colesimagen].ToString());
                oReciboMenorCabecera.NroPagina = Convert.ToInt32(values[colnropagina].ToString());
                oReciboMenorCabecera.IdRutaReparto = Convert.ToInt32(values[colidrutareparto]);
                oReciboMenorCabecera.IdEstrato1 = _intidestrato1 == 0 ? (Int16)0 : Convert.ToInt16(values[colidestrato1]);
                oReciboMenorCabecera.IdEstrato2 = _intidestrato2 == 0 ? (Int16)0 : Convert.ToInt16(values[colidestrato2]);
                oReciboMenorCabecera.IdEstrato3 = _intidestrato3 == 0 ? (Int16)0 : Convert.ToInt16(values[colidestrato3]);
                oReciboMenorCabecera.IncluirCaritas = _intincluircaritas == 0 ? (Int16)0 : Convert.ToInt16(values[colincluircaritas]);
                oReciboMenorCabecera.IdEmpresa = Convert.ToInt16(values[colidempresa]);
                oReciboMenorCabecera.RucEmpresa = values[colRucEmpresa].ToString();

                oReciboMenorCabecera.MostrarCaritas = Convert.ToInt16(values[colMostrarCaritas]);
                oReciboMenorCabecera.MostrarCortado = Convert.ToInt16(values[colMostrarCortado]);
                //

                /*
                 * Agregado el 20082020 - Carlos López
                 */
                oReciboMenorCabecera.MensajeCortadoCaritas = values[colMensajeCortadoCaritas].ToString();

                this.Elementos.Add(oReciboMenorCabecera);
            }

            dr.Close();
        }

        #endregion
    }

    public class clsReciboMenorConcepto
    {
        private Int32 iIdNroServicio;
        private Int32 iPeriodo;
        private String sNombreConcepto;
        private String sImporte;

        public Int32 IdNroServicio
        {
            get { return iIdNroServicio; }
            set { iIdNroServicio = value; }
        }

        public Int32 Periodo
        {
            get { return iPeriodo; }
            set { iPeriodo = value; }
        }

        public String NombreConcepto
        {
            get { return sNombreConcepto; }
            set { sNombreConcepto = value; }
        }

        public String Importe
        {
            get { return sImporte; }
            set { sImporte = value; }
        }

        public override bool Equals(object obj)
        {
            clsReciboMenorConcepto oConcepto = (clsReciboMenorConcepto)obj;
            return (this.IdNroServicio == oConcepto.IdNroServicio && oConcepto.Periodo == this.Periodo);
        }

        public override int GetHashCode()
        {
            return this.IdNroServicio ^ this.Periodo;
        }

        public clsReciboMenorConcepto()
        { }

        public clsReciboMenorConcepto(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }

    }

    public class clsListaReciboMenorConcepto
    {
        #region Campos

        private List<clsReciboMenorConcepto> _objelementos = new List<clsReciboMenorConcepto>();

        #endregion Campos

        #region Propiedades
        //[CampoLista()]
        public List<clsReciboMenorConcepto> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMenorConcepto()
        {
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMenorConcepto(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];

            Int32 colNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colNombreConcepto = dr.GetOrdinal("NombreConcepto");
            Int32 colImporte = dr.GetOrdinal("Importe");

            while (dr.Read())
            {
                clsReciboMenorConcepto oReciboMenorConcepto = new clsReciboMenorConcepto();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                // asignar los valores al objeto.
                oReciboMenorConcepto.IdNroServicio = Convert.ToInt32(values[colNroServicio]);
                oReciboMenorConcepto.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMenorConcepto.NombreConcepto = Convert.ToString(values[colNombreConcepto]);
                oReciboMenorConcepto.Importe = Convert.ToString(values[colImporte]);

                this.Elementos.Add(oReciboMenorConcepto);
            }

            dr.Close();
        }

        #endregion

    }

    public class clsReciboMenorConsumo
    {
        #region Campos
        private Int32 iIdNroServicio;
        private Int32 iPeriodo;
        private Int32 iPeriodoConsumo;
        private Decimal deCantidadConsumo;
        private Int16 iIdConcepto;
        private String sAbreviaturaConcepto;
        private String sMes;
        private Decimal decimportetotal;

        #endregion Campos

        #region Propiedades

        [Campo()]
        public Int32 idNroServicio
        {
            get { return iIdNroServicio; }
            set { iIdNroServicio = value; }
        }

        [Campo()]
        public Int32 Periodo
        {
            get { return iPeriodo; }
            set { iPeriodo = value; }
        }

        [Campo()]
        public Int32 PeriodoConsumo
        {
            get { return iPeriodoConsumo; }
            set { iPeriodoConsumo = value; }
        }

        [Campo()]
        public Decimal CantidadConsumo
        {
            get { return deCantidadConsumo; }
            set { deCantidadConsumo = value; }
        }

        [Campo()]
        public Int16 IdConcepto
        {
            get { return iIdConcepto; }
            set { iIdConcepto = value; }
        }

        [Campo()]
        public String AbreviaturaConcepto
        {
            get { return sAbreviaturaConcepto; }
            set { sAbreviaturaConcepto = value; }
        }

        [Campo()]
        public String Mes
        {
            get { return sMes; }
            set { sMes = value; }
        }

        [Campo()]
        public Decimal ImporteTotal
        {
            get { return decimportetotal; }
            set { decimportetotal = value; }
        }

        #endregion Propiedades

        public override bool Equals(object obj)
        {
            clsReciboMenorConsumo oConsumo = (clsReciboMenorConsumo)obj;
            return (this.idNroServicio == oConsumo.idNroServicio && oConsumo.Periodo == this.Periodo);
        }

        public override int GetHashCode()
        {
            return this.idNroServicio ^ this.Periodo;
        }

        public clsReciboMenorConsumo()
        { }

        public clsReciboMenorConsumo(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }
    }

    public class clsListaReciboMenorConsumo
    {
        #region Campos

        private List<clsReciboMenorConsumo> _objelementos = new List<clsReciboMenorConsumo>();

        #endregion Campos

        #region Propiedades

        public List<clsReciboMenorConsumo> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMenorConsumo()
        {
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMenorConsumo(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];


            Int32 colIdNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colPeriodoConsumo = dr.GetOrdinal("PeriodoConsumo");
            Int32 colIdConcepto = dr.GetOrdinal("IdConcepto");
            Int32 colCantidadConsumo = dr.GetOrdinal("CantidadConsumo");
            Int32 colAbreviaturaConcepto = dr.GetOrdinal("AbreviaturaConcepto");
            Int32 colMes = dr.GetOrdinal("Mes");
            Int32 colImporteTotal = dr.GetOrdinal("ImporteTotal");

            while (dr.Read())
            {
                clsReciboMenorConsumo oReciboMenorConsumo = new clsReciboMenorConsumo();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                // asignar los valores al objeto.
                oReciboMenorConsumo.idNroServicio = Convert.ToInt32(values[colIdNroServicio]);
                oReciboMenorConsumo.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMenorConsumo.PeriodoConsumo = Convert.ToInt32(values[colPeriodoConsumo]);
                oReciboMenorConsumo.IdConcepto = Convert.ToInt16(values[colIdConcepto]);
                oReciboMenorConsumo.CantidadConsumo = Convert.ToDecimal(values[colCantidadConsumo]);
                oReciboMenorConsumo.AbreviaturaConcepto = Convert.ToString(values[colAbreviaturaConcepto]);
                oReciboMenorConsumo.Mes = Convert.ToString(values[colMes]);
                oReciboMenorConsumo.ImporteTotal = Convert.ToDecimal(values[colImporteTotal]);

                this.Elementos.Add(oReciboMenorConsumo);
            }

            dr.Close();
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMenorConsumo(DataTable entidad)
        {
            foreach (DataRow item in entidad.Rows)
            {
                Elementos.Add(new clsReciboMenorConsumo(item));
            }

            //int colCount = dr.FieldCount;
            //object[] values = new object[colCount];


            //Int32 colIdNroServicio = dr.GetOrdinal("IdNroServicio");
            //Int32 colPeriodo = dr.GetOrdinal("Periodo");
            //Int32 colPeriodoConsumo = dr.GetOrdinal("PeriodoConsumo");
            //Int32 colIdConcepto = dr.GetOrdinal("IdConcepto");
            //Int32 colCantidadConsumo = dr.GetOrdinal("CantidadConsumo");
            //Int32 colAbreviaturaConcepto = dr.GetOrdinal("AbreviaturaConcepto");
            //Int32 colMes = dr.GetOrdinal("Mes");

            //while (dr.Read())
            //{
            //    clsReciboMenorConsumo oReciboMenorConsumo = new clsReciboMenorConsumo();

            //    // Llamar los datos del metadato.
            //    dr.GetValues(values);

            //    // asignar los valores al objeto.
            //    oReciboMenorConsumo.idNroServicio = Convert.ToInt32(values[colIdNroServicio]);
            //    oReciboMenorConsumo.Periodo = Convert.ToInt32(values[colPeriodo]);
            //    oReciboMenorConsumo.PeriodoConsumo = Convert.ToInt32(values[colPeriodoConsumo]);
            //    oReciboMenorConsumo.IdConcepto = Convert.ToInt16(values[colIdConcepto]);
            //    oReciboMenorConsumo.CantidadConsumo = Convert.ToDecimal(values[colCantidadConsumo]);
            //    oReciboMenorConsumo.AbreviaturaConcepto = Convert.ToString(values[colAbreviaturaConcepto]);
            //    oReciboMenorConsumo.Mes = Convert.ToString(values[colMes]);

            //    this.Elementos.Add(oReciboMenorConsumo);
            //}

            //dr.Close();
        }

        #endregion

    }

    public class clsReciboMenorLectura
    {
        #region Campos

        private Int32 _intidnroservicio;
        private Int32 _periodo;
        private Int32? _intidmedidor;
        private String _strnromedidor;
        private String _strtipomedidor;
        private Int16 _inthilosmedidor;
        private DateTime? _datfechalecturaanterior;
        private Decimal? _declecturaanterior;
        private DateTime? _datfechalecturaactual;
        private Decimal? _declecturaactual;
        private Decimal? _decdiferencia;
        private Decimal? _decfactormedicion;
        private Decimal? _decfactortransformacion;
        private Decimal? _decconsumoleido;
        private Int16? _intidmagnitud;
        private String _strsimbolo;
        private String _strnombretarifa;
        private String _strfase;
        private String _strnombretipoacometida;
        private String _strsubtipo;
        private String _strtension;
        private DateTime? _datfechacontrato;
        private DateTime? _datfechacontratotermino;
        private Decimal? _decpotenciacontratada;
        private String _strpontenciamedida;
        private String _strsed;
        private DateTime? _datfechaemision;
        private DateTime? _datfechavencimiento;
        private Boolean _bolparacorte;
        private Boolean _bolduplicado;
        private Decimal? _decpromedioseismeses;
        private String _strobservacioncorrecion;
        private Int16? _intnroservicioscolectivos;
        private Int16 _intcortado;

        private String _strmesesanterior;
        private String _strmesesanterioranterior;

        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>

        public Int16 Cortado
        {
            get { return _intcortado; }
            set { _intcortado = value; }
        }


        public Int16? NroServiciosColectivos
        {
            get { return _intnroservicioscolectivos; }
            set { _intnroservicioscolectivos = value; }
        }


        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }


        public Int32 Periodo
        {
            get { return _periodo; }
            set { _periodo = value; }
        }

        public Int32? IdMedidor
        {
            get { return _intidmedidor; }
            set { _intidmedidor = value; }
        }


        public String NroMedidor
        {
            get { return _strnromedidor; }
            set { _strnromedidor = value; }
        }


        public String TipoMedidor
        {
            get { return _strtipomedidor; }
            set { _strtipomedidor = value; }
        }


        public Int16 HilosMedidor
        {
            get { return _inthilosmedidor; }
            set { _inthilosmedidor = value; }
        }


        public DateTime? FechaLecturaAnterior
        {
            get { return _datfechalecturaanterior; }
            set { _datfechalecturaanterior = value; }
        }

        public Decimal? LecturaAnterior
        {
            get { return _declecturaanterior; }
            set { _declecturaanterior = value; }
        }

        public DateTime? FechaLecturaActual
        {
            get { return _datfechalecturaactual; }
            set { _datfechalecturaactual = value; }
        }

        public Decimal? LecturaActual
        {
            get { return _declecturaactual; }
            set { _declecturaactual = value; }
        }

        public Decimal? Diferencia
        {
            get { return _decdiferencia; }
            set { _decdiferencia = value; }
        }

        public Decimal? FactorMedicion
        {
            get { return _decfactormedicion; }
            set { _decfactormedicion = value; }
        }

        public Decimal? factorTransformacion
        {
            get { return _decfactortransformacion; }
            set { _decfactortransformacion = value; }
        }

        public Decimal? ConsumoLeido
        {
            get { return _decconsumoleido; }
            set { _decconsumoleido = value; }
        }

        public Int16? Idmagnitud
        {
            get { return _intidmagnitud; }
            set { _intidmagnitud = value; }
        }

        public String Simbolo
        {
            get { return _strsimbolo; }
            set { _strsimbolo = value; }
        }

        public String NombreTarifa
        {
            get { return _strnombretarifa; }
            set { _strnombretarifa = value; }
        }

        public String Fase
        {
            get { return _strfase; }
            set { _strfase = value; }
        }

        public String NombreTipoAcometida
        {
            get { return _strnombretipoacometida; }
            set { _strnombretipoacometida = value; }
        }

        public String SubTipo
        {
            get { return _strsubtipo; }
            set { _strsubtipo = value; }
        }

        public String Tension
        {
            get { return _strtension; }
            set { _strtension = value; }
        }

        public DateTime? FechaContrato
        {
            get { return _datfechacontrato; }
            set { _datfechacontrato = value; }
        }

        public DateTime? FechaContratoTermino
        {
            get { return _datfechacontratotermino; }
            set { _datfechacontratotermino = value; }
        }

        public Decimal? PotenciaContratada
        {
            get { return _decpotenciacontratada; }
            set { _decpotenciacontratada = value; }
        }

        public String PontenciaMedida
        {
            get { return _strpontenciamedida; }
            set { _strpontenciamedida = value; }
        }

        public String SED
        {
            get { return _strsed; }
            set { _strsed = value; }
        }


        public DateTime? FechaEmision
        {
            get { return _datfechaemision; }
            set { _datfechaemision = value; }
        }

        public Boolean ParaCorte
        {
            get { return _bolparacorte; }
            set { _bolparacorte = value; }
        }

        public DateTime? FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }


        public Boolean Duplicado
        {
            get { return _bolduplicado; }
            set { _bolduplicado = value; }
        }


        public Decimal? PromedioSeisMeses
        {
            get { return _decpromedioseismeses; }
            set { _decpromedioseismeses = value; }
        }


        public String ObservacionCorrecion
        {
            get { return _strobservacioncorrecion; }
            set { _strobservacioncorrecion = value; }
        }


        public String RecibosAnterior
        {
            get { return _strmesesanterior; }
            set { _strmesesanterior = value; }
        }


        public String RecibosAnteriorAnterior
        {
            get { return _strmesesanterioranterior; }
            set { _strmesesanterioranterior = value; }
        }

        #endregion

        public override bool Equals(object obj)
        {
            clsReciboMenorLectura oLectura = (clsReciboMenorLectura)obj;
            return (this.IdNroServicio == oLectura.IdNroServicio && oLectura.Periodo == this.Periodo);
        }

        public override int GetHashCode()
        {
            return this.IdNroServicio ^ this.Periodo;
        }

        public clsReciboMenorLectura(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }

        public clsReciboMenorLectura()
        { }
    }

    public class clsListaReciboMenorLectura
    {
        #region Campos

        private List<clsReciboMenorLectura> _objelementos = new List<clsReciboMenorLectura>();

        #endregion Campos

        #region Propiedades

        public List<clsReciboMenorLectura> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMenorLectura()
        {
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMenorLectura(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];

            Int32 colNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colMedidor = dr.GetOrdinal("IdMedidor");
            Int32 colNroMedidor = dr.GetOrdinal("NroMedidor");
            Int32 colTipoMedidor = dr.GetOrdinal("TipoMedidor");
            Int32 colHilosMedidor = dr.GetOrdinal("HilosMedidor");
            Int32 colFechaLecturaAnterior = dr.GetOrdinal("FechaLecturaAnterior");
            Int32 colLecturaAnterior = dr.GetOrdinal("LecturaAnterior");
            Int32 colFechaLecturaActual = dr.GetOrdinal("FechaLecturaActual");
            Int32 colLecturaActual = dr.GetOrdinal("LecturaActual");
            Int32 colDiferencia = dr.GetOrdinal("Diferencia");
            Int32 colFactorMedicion = dr.GetOrdinal("FactorMedicion");
            Int32 colfactorTransformacion = dr.GetOrdinal("factorTransformacion");
            Int32 colConsumoLeido = dr.GetOrdinal("ConsumoLeido");
            Int32 colIdmagnitud = dr.GetOrdinal("Idmagnitud");
            Int32 colSimbolo = dr.GetOrdinal("Simbolo");
            Int32 colFechaContrato = dr.GetOrdinal("FechaContrato");
            Int32 colFechaContratoTermino = dr.GetOrdinal("FechaContratoTermino");
            Int32 colPontenciaMedida = dr.GetOrdinal("PontenciaMedida");
            Int32 colPontenciaContratada = dr.GetOrdinal("PotenciaContratada");
            Int32 colNombreTarifa = dr.GetOrdinal("NombreTarifa");
            Int32 colFase = dr.GetOrdinal("Fase");
            Int32 colNombreTipoAcometida = dr.GetOrdinal("NombreTipoAcometida");
            Int32 colSubTipo = dr.GetOrdinal("SubTipo");
            Int32 colTension = dr.GetOrdinal("Tension");
            Int32 colFechaEmision = dr.GetOrdinal("FechaEmision");
            Int32 colParaCorte = dr.GetOrdinal("ParaCorte");
            Int32 colFechaVencimiento = dr.GetOrdinal("FechaVencimiento");
            Int32 colduplicado = dr.GetOrdinal("Duplicado");
            Int32 colSed = dr.GetOrdinal("Sed");
            Int32 colPromedioSeisMeses = dr.GetOrdinal("PromedioSeisUltimosMeses");
            Int32 colObservacionCorrecion = dr.GetOrdinal("ObservacionCorrecion");
            Int32 colNroServiciosColectivos = dr.GetOrdinal("NroServiciosColectivos");
            Int32 colRecibosAnterior = dr.GetOrdinal("RecibosAnterior");
            Int32 colRecibosAnteriorAnterior = dr.GetOrdinal("RecibosAnteriorAnterior");

            Int32 colcortado = 0;
            DataTable _dt = dr.GetSchemaTable();
            Int16 _intcortado = 0;

            foreach (DataRow item in _dt.Rows)
            {
                if (item[0].ToString() == "Cortado")
                {
                    colcortado = dr.GetOrdinal("Cortado");
                    _intcortado = 1;
                }
            }

            while (dr.Read())
            {
                clsReciboMenorLectura oReciboMenorLectura = new clsReciboMenorLectura();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                // asignar los valores al objeto.
                oReciboMenorLectura.IdNroServicio = Convert.ToInt32(values[colNroServicio]);
                oReciboMenorLectura.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMenorLectura.IdMedidor = Convert.IsDBNull(
                                                values[colMedidor])
                                                ? (Int32?)null :
                                                Convert.ToInt32(values[colMedidor]);
                oReciboMenorLectura.NroMedidor = Convert.ToString(values[colNroMedidor]);
                oReciboMenorLectura.TipoMedidor = Convert.ToString(values[colTipoMedidor]);
                oReciboMenorLectura.HilosMedidor = Convert.ToInt16(values[colHilosMedidor]);
                oReciboMenorLectura.FechaLecturaAnterior = Convert.IsDBNull(
                                                values[colFechaLecturaAnterior])
                                                ? (DateTime?)null :
                                                Convert.ToDateTime(values[colFechaLecturaAnterior]);
                oReciboMenorLectura.LecturaAnterior = Convert.IsDBNull(values[colLecturaAnterior])
                                                ? (decimal?)null :
                                                Convert.ToDecimal(values[colLecturaAnterior]);
                oReciboMenorLectura.FechaLecturaActual = Convert.IsDBNull(values[colFechaLecturaActual])
                                                ? (DateTime?)null :
                                                Convert.ToDateTime(values[colFechaLecturaActual]);
                oReciboMenorLectura.LecturaActual = Convert.IsDBNull(values[colLecturaActual])
                                                ? (Decimal?)null :
                                                Convert.ToDecimal(values[colLecturaActual]);
                oReciboMenorLectura.Diferencia = Convert.IsDBNull(values[colDiferencia])
                                                ? (Decimal?)null :
                                                Convert.ToDecimal(values[colDiferencia]);
                oReciboMenorLectura.FactorMedicion = Convert.IsDBNull(
                                                values[colFactorMedicion])
                                                ? (Decimal?)null :
                                                Convert.ToDecimal(values[colFactorMedicion]);
                oReciboMenorLectura.factorTransformacion = Convert.IsDBNull(
                                                values[colfactorTransformacion])
                                                ? (Decimal?)null :
                                                Convert.ToDecimal(values[colfactorTransformacion]);
                oReciboMenorLectura.ConsumoLeido = Convert.ToDecimal(values[colConsumoLeido]);
                oReciboMenorLectura.Idmagnitud = Convert.IsDBNull(values[colIdmagnitud])
                                                ? (Int16?)null :
                                                Convert.ToInt16(values[colIdmagnitud]);
                oReciboMenorLectura.Simbolo = Convert.ToString(values[colSimbolo]);

                oReciboMenorLectura.FechaContrato = Convert.IsDBNull(values[colFechaContrato])
                                                    ? (DateTime?)null :
                                                    Convert.ToDateTime(values[colFechaContrato]);
                oReciboMenorLectura.FechaContratoTermino = Convert.IsDBNull(values[colFechaContratoTermino])
                                                    ? (DateTime?)null :
                                                    Convert.ToDateTime(values[colFechaContratoTermino]);

                oReciboMenorLectura.PotenciaContratada = Convert.IsDBNull(values[colPontenciaContratada])
                                                    ? (Decimal?)null :
                                                    Convert.ToDecimal(values[colPontenciaContratada]);
                oReciboMenorLectura.PontenciaMedida = values[colPontenciaMedida].ToString();
                oReciboMenorLectura.NombreTarifa = values[colNombreTarifa].ToString();
                oReciboMenorLectura.Fase = values[colFase].ToString();
                oReciboMenorLectura.NombreTipoAcometida = values[colNombreTipoAcometida].ToString();
                oReciboMenorLectura.SubTipo = values[colSubTipo].ToString();
                oReciboMenorLectura.Tension = values[colTension].ToString();
                oReciboMenorLectura.FechaEmision = Convert.IsDBNull(values[colFechaEmision])
                                                    ? (DateTime?)null :
                                                    Convert.ToDateTime(values[colFechaEmision].ToString());
                oReciboMenorLectura.ParaCorte = Convert.ToBoolean(values[colParaCorte]);
                oReciboMenorLectura.FechaVencimiento = Convert.IsDBNull(values[colFechaVencimiento])
                                                    ? (DateTime?)null :
                                                    Convert.ToDateTime(values[colFechaVencimiento].ToString());
                oReciboMenorLectura.Duplicado = Convert.ToBoolean(values[colduplicado]);
                oReciboMenorLectura.SED = values[colSed].ToString();
                oReciboMenorLectura.PromedioSeisMeses = Convert.IsDBNull(values[colPromedioSeisMeses])
                                                    ? (Decimal?)null :
                                                    Convert.ToDecimal(values[colPromedioSeisMeses]);
                oReciboMenorLectura.ObservacionCorrecion = values[colObservacionCorrecion].ToString();
                oReciboMenorLectura.NroServiciosColectivos = Convert.IsDBNull(
                                                values[colNroServiciosColectivos])
                                                ? (Int16?)0 :
                                                Convert.ToInt16(values[colNroServiciosColectivos]);
                oReciboMenorLectura.Cortado = _intcortado == 0 ? (Int16)0 : Convert.ToInt16(values[colcortado].ToString());

                oReciboMenorLectura.RecibosAnterior = Convert.IsDBNull(
                                values[colRecibosAnterior])
                                ? String.Empty :
                                values[colRecibosAnterior].ToString();

                oReciboMenorLectura.RecibosAnteriorAnterior = Convert.IsDBNull(
                                values[colRecibosAnteriorAnterior])
                                ? String.Empty :
                                values[colRecibosAnteriorAnterior].ToString();

                this.Elementos.Add(oReciboMenorLectura);
            }

            dr.Close();
        }


        #endregion

    }

    #endregion ReciboMenor

    #region ReciboMayor

    public class clsImprimirReciboMayor
    {
        #region Campos

        private Int32 _intidnroservicio;
        private Int32 _intperiodo;
        private Int16 _esduplicado;
        private Int16 _esruta;
        private Int16 _esvistaprevia;
        private String _strnombreimpresora;
        private String _strformato;
        private String _strrender;
        private String _strrutasalidareporte;
        private String _strcartera;
        public Boolean _bolesimpresora;
        public Int16 _paginastotales;
        public Int32 _registrostotales;
        public clsDeviceInfo _objdevice = new clsDeviceInfo();
        private clsListaSuministroRecibo _listasuminitros = new clsListaSuministroRecibo();
        private clsListaReciboMayorCabecera _objlistarecibomayorcabecera = new clsListaReciboMayorCabecera();
        private clsListaReciboMayorLectura _objlistarecibomayorlectura = new clsListaReciboMayorLectura();
        private clsListaReciboMayorConcepto _objlistarecibomayorconcepto = new clsListaReciboMayorConcepto();
        private clsListaReciboMayorConsumo _objlistarecibomayorconsumo = new clsListaReciboMayorConsumo();

        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public Int16 PaginasTotales
        {
            get { return _paginastotales; }
            set { _paginastotales = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 RegistrosTotales
        {
            get { return _registrostotales; }
            set { _registrostotales = value; }
        }

        public String NombreImpresora
        {
            get { return _strnombreimpresora; }
            set { _strnombreimpresora = value; }
        }
        public String Formato
        {
            get { return _strformato; }
            set { _strformato = value; }
        }
        public String Render
        {
            get { return _strrender; }
            set { _strrender = value; }
        }

        public String RutaSalidaReporte
        {
            get { return _strrutasalidareporte; }
            set { _strrutasalidareporte = value; }
        }

        public Int16 EsDuplicado
        {
            get { return _esduplicado; }
            set { _esduplicado = value; }
        }
        public Int16 EsRuta
        {
            get { return _esruta; }
            set { _esruta = value; }
        }
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }
        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }
        public Boolean EsImpresora
        {
            get { return _bolesimpresora; }
            set { _bolesimpresora = value; }
        }
        public clsListaReciboMayorCabecera ListaReciboMayorCabecera
        {
            get { return _objlistarecibomayorcabecera; }
            set { _objlistarecibomayorcabecera = value; }
        }
        public clsListaReciboMayorConsumo ListaReciboMayorConsumo
        {
            get { return _objlistarecibomayorconsumo; }
            set { _objlistarecibomayorconsumo = value; }
        }
        public clsListaReciboMayorLectura ListaReciboMayorLectura
        {
            get { return _objlistarecibomayorlectura; }
            set { _objlistarecibomayorlectura = value; }
        }


        public clsListaReciboMayorConcepto ListaReciboMayorConcepto
        {
            get { return _objlistarecibomayorconcepto; }
            set { _objlistarecibomayorconcepto = value; }
        }

        public clsListaSuministroRecibo ListaSuministro
        {
            get { return _listasuminitros; }
            set { _listasuminitros = value; }
        }
        public String EsCartera
        {
            get { return _strcartera; }
            set { _strcartera = value; }
        }
        public Int16 EsVistaPrevia
        {
            get { return _esvistaprevia; }
            set { _esvistaprevia = value; }
        }
        public clsDeviceInfo DeviceInfo
        {
            get { return _objdevice; }
            set { _objdevice = value; }
        }

        #endregion

        #region Constructor

        public clsImprimirReciboMayor()
        { }

        public clsImprimirReciboMayor(Int32 IdNroServicio, Int32 Periodo)
        {
            this.IdNroServicio = IdNroServicio;
            this.Periodo = Periodo;
        }

        public clsImprimirReciboMayor(DataRow dr)
        {
            if (dr == null) return;

            this.IdNroServicio = (Int32)dr["idnroservicio"];
            this.Periodo = (Int32)dr["periodo"];
        }
        #endregion
    }

    public class clsListaImprimirReciboMayor
    {
        private List<clsImprimirReciboMayor> _objelementos = new List<clsImprimirReciboMayor>();

        public List<clsImprimirReciboMayor> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        public clsListaImprimirReciboMayor(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsImprimirReciboMayor(_drw));
            }
        }

        public clsListaImprimirReciboMayor()
        { }
    }

    public class clsReciboMayorCabecera
    {
        #region Campos

        private Int16 iIdTipoDocumento;
        private String sNroDocumento;
        private String sNombreDistrito;
        private String sNombreProvincia;
        private Int32 iIdNroServicio;
        private Int32 iPeriodo;
        private String sNombreNroServicio;
        private String sDireccion;
        private String sDireccionReferencial;
        private String sRUC;
        private Int32 iIdAnoComercial;
        private Int16 iIdMesComercial;
        private String sPeriodo;
        private DateTime dFechaEmision;
        private DateTime dFechaVencimiento;
        private Decimal deTotalMes;
        private Decimal deTotalAPagar;
        private Decimal deImporteAplicado;
        private Decimal deDeudaAnteriorInicial;
        private Int16 iNroMesesDeuda;
        private String sRutaReparto;
        private Int32? iIdDireccionCodificadaReparto;
        private String sDireccionCodificadaReparto;
        private Int16 iIdEmpresaServicio;
        private Int16 iIdMoneda;
        private String sSimboloMoneda;
        private Int16 iIdSector;
        private Boolean bParaCorte;
        private Boolean bDuplicado;
        private String sNroenLetras;
        private String sCalificacion;
        private String sModalidadFacturacion;
        private String sTarifa;
        private String sMedicion;
        private String sTension;
        private String sSED;
        private String sTipoConexion;
        private String sSerieMedidor;
        private String sModalidad;
        private DateTime tInicioContrato;
        private DateTime tTerminoContrato;
        private Decimal? dPromedioMaximaDemanda;
        private Decimal? dPotenciaContratada;
        private Decimal? dPromedioMaximaDemandaPunta;
        private Decimal? dPromedioMaximaDemandaFueraPunta;
        private Decimal? dPotenciaContratadaPunta;
        private Decimal? dPotenciaContratadaFueraPunta;
        private DateTime tConsumoDesde;
        private DateTime tConsumoHasta;
        private String sMesComercial;
        private Int32 iNroHilos;
        private String _imagenempresa;
        private String _imagensector;
        private Int16 _iindicadorpotencia;
        private Int16 _iindicadormaximademanda;
        private Int32 _intinrohoraspunta;
        private Int16 _intesimagen;
        private Int32 _intnropagina;
        private String _strcodigotecnico;

        private String _strmesesanterior;
        private String _strmesesanterioranterior;
        private Int16 _intidempresa;

        #endregion

        #region Propiedades

        [Campo()]
        public Int16 EsImagen
        {
            get { return _intesimagen; }
            set { _intesimagen = value; }
        }

        [Campo()]
        public Int32 NroHorasPunta
        {
            get { return _intinrohoraspunta; }
            set { _intinrohoraspunta = value; }
        }

        [Campo()]
        public Int16 IdTipoDocumento
        {
            get { return iIdTipoDocumento; }
            set { iIdTipoDocumento = value; }
        }

        [Campo()]
        public String NroDocumento
        {
            get { return sNroDocumento; }
            set { sNroDocumento = value; }
        }

        [Campo()]
        public String NombreDistrito
        {
            get { return sNombreDistrito; }
            set { sNombreDistrito = value; }
        }

        [Campo()]
        public String NombreProvincia
        {
            get { return sNombreProvincia; }
            set { sNombreProvincia = value; }
        }

        [Campo()]
        public Int32 IdNroServicio
        {
            get { return iIdNroServicio; }
            set { iIdNroServicio = value; }
        }

        [Campo()]
        public String NombreNroServicio
        {
            get { return sNombreNroServicio; }
            set { sNombreNroServicio = value; }
        }

        [Campo()]
        public String Direccion
        {
            get { return sDireccion; }
            set { sDireccion = value; }
        }

        [Campo()]
        public String DireccionReferencial
        {
            get { return sDireccionReferencial; }
            set { sDireccionReferencial = value; }
        }

        [Campo()]
        public String RUC
        {
            get { return sRUC; }
            set { sRUC = value; }
        }

        [Campo()]
        public Int32 IdAnoComercial
        {
            get { return iIdAnoComercial; }
            set { iIdAnoComercial = value; }
        }

        [Campo()]
        public Int16 IdMesComercial
        {
            get { return iIdMesComercial; }
            set { iIdMesComercial = value; }
        }

        [Campo()]
        public Int32 Periodo
        {
            get { return iPeriodo; }
            set { iPeriodo = value; }
        }

        [Campo()]
        public DateTime FechaEmision
        {
            get { return dFechaEmision; }
            set { dFechaEmision = value; }
        }

        [Campo()]
        public DateTime FechaVencimiento
        {
            get { return dFechaVencimiento; }
            set { dFechaVencimiento = value; }
        }

        [Campo()]
        public Decimal TotalMes
        {
            get { return deTotalMes; }
            set { deTotalMes = value; }
        }

        [Campo()]
        public Decimal TotalAPagar
        {
            get { return deTotalAPagar; }
            set { deTotalAPagar = value; }
        }

        [Campo()]
        public Decimal ImporteAplicado
        {
            get { return deImporteAplicado; }
            set { deImporteAplicado = value; }
        }

        [Campo()]
        public Decimal DeudaAnteriorInicial
        {
            get { return deDeudaAnteriorInicial; }
            set { deDeudaAnteriorInicial = value; }
        }

        [Campo()]
        public Int16 NroMesesDeuda
        {
            get { return iNroMesesDeuda; }
            set { iNroMesesDeuda = value; }
        }

        [Campo()]
        public String RutaReparto
        {
            get { return sRutaReparto; }
            set { sRutaReparto = value; }
        }

        [Campo()]
        public Int32? IdDireccionCodificadaReparto
        {
            get { return iIdDireccionCodificadaReparto; }
            set { iIdDireccionCodificadaReparto = value; }
        }

        [Campo()]
        public String DireccionCodificadaReparto
        {
            get { return sDireccionCodificadaReparto; }
            set { sDireccionCodificadaReparto = value; }
        }

        [Campo()]
        public Int16 IdEmpresaServicio
        {
            get { return iIdEmpresaServicio; }
            set { iIdEmpresaServicio = value; }
        }

        [Campo()]
        public Int16 IdMoneda
        {
            get { return iIdMoneda; }
            set { iIdMoneda = value; }
        }

        [Campo()]
        public String SimboloMoneda
        {
            get { return sSimboloMoneda; }
            set { sSimboloMoneda = value; }
        }

        [Campo()]
        public Int16 IdSector
        {
            get { return iIdSector; }
            set { iIdSector = value; }
        }

        [Campo()]
        public Boolean ParaCorte
        {
            get { return bParaCorte; }
            set { bParaCorte = value; }
        }

        [Campo()]
        public Boolean Duplicado
        {
            get { return bDuplicado; }
            set { bDuplicado = value; }
        }

        [Campo()]
        public String NroenLetras
        {
            get { return sNroenLetras; }
            set { sNroenLetras = value; }
        }

        [Campo()]
        public String Calificacion
        {
            get { return sCalificacion; }
            set { sCalificacion = value; }
        }

        [Campo()]
        public String ModalidadFacturacion
        {
            get { return sModalidadFacturacion; }
            set { sModalidadFacturacion = value; }
        }

        [Campo()]
        public String Tarifa
        {
            get { return sTarifa; }
            set { sTarifa = value; }
        }

        [Campo()]
        public String Medicion
        {
            get { return sMedicion; }
            set { sMedicion = value; }
        }

        [Campo()]
        public String Tension
        {
            get { return sTension; }
            set { sTension = value; }
        }

        [Campo()]
        public String SED
        {
            get { return sSED; }
            set { sSED = value; }
        }

        [Campo()]
        public String TipoConexion
        {
            get { return sTipoConexion; }
            set { sTipoConexion = value; }
        }

        [Campo()]
        public String SerieMedidor
        {
            get { return sSerieMedidor; }
            set { sSerieMedidor = value; }
        }

        [Campo()]
        public String Modalidad
        {
            get { return sModalidad; }
            set { sModalidad = value; }
        }

        [Campo()]
        public DateTime InicioContrato
        {
            get { return tInicioContrato; }
            set { tInicioContrato = value; }
        }

        [Campo()]
        public DateTime TerminoContrato
        {
            get { return tTerminoContrato; }
            set { tTerminoContrato = value; }
        }

        [Campo()]
        public Decimal? PromedioMaximaDemanda
        {
            get { return dPromedioMaximaDemanda; }
            set { dPromedioMaximaDemanda = value; }
        }

        [Campo()]
        public Decimal? PotenciaContratada
        {
            get { return dPotenciaContratada; }
            set { dPotenciaContratada = value; }
        }

        [Campo()]
        public Decimal? PromedioMaximaDemandaPunta
        {
            get { return dPromedioMaximaDemandaPunta; }
            set { dPromedioMaximaDemandaPunta = value; }
        }

        [Campo()]
        public Decimal? PotenciaContratadaPunta
        {
            get { return dPotenciaContratadaPunta; }
            set { dPotenciaContratadaPunta = value; }
        }

        [Campo()]
        public Decimal? PromedioMaximaDemandaFueraPunta
        {
            get { return dPromedioMaximaDemandaFueraPunta; }
            set { dPromedioMaximaDemandaFueraPunta = value; }
        }

        [Campo()]
        public Decimal? PotenciaContratadaFueraPunta
        {
            get { return dPotenciaContratadaFueraPunta; }
            set { dPotenciaContratadaFueraPunta = value; }
        }

        [Campo()]
        public DateTime ConsumoDesde
        {
            get { return tConsumoDesde; }
            set { tConsumoDesde = value; }
        }

        [Campo()]
        public DateTime ConsumoHasta
        {
            get { return tConsumoHasta; }
            set { tConsumoHasta = value; }
        }

        [Campo()]
        public String MesComercial
        {
            get { return sMesComercial; }
            set { sMesComercial = value; }
        }

        [Campo()]
        public Int32 NroHilos
        {
            get { return iNroHilos; }
            set { iNroHilos = value; }
        }

        [Campo()]
        public String ImagenEmpresa
        {
            get { return _imagenempresa; }
            set { _imagenempresa = value; }
        }

        [Campo()]
        public String ImagenSector
        {
            get { return _imagensector; }
            set { _imagensector = value; }
        }

        [Campo()]
        public Int16 IndicadorPotencia
        {
            get { return _iindicadorpotencia; }
            set { _iindicadorpotencia = value; }
        }

        [Campo()]
        public Int16 IndicadorMaximaDemanda
        {
            get { return _iindicadormaximademanda; }
            set { _iindicadormaximademanda = value; }
        }

        [Campo()]
        public Int32 NroPagina
        {
            get { return _intnropagina; }
            set { _intnropagina = value; }
        }

        [Campo()]
        public String CodigoTecnico
        {
            get { return _strcodigotecnico; }
            set { _strcodigotecnico = value; }
        }

        [Campo()]
        public String MesesAnterior
        {
            get { return _strmesesanterior; }
            set { _strmesesanterior = value; }
        }

        [Campo()]
        public String MesesAnteriorAnterior
        {
            get { return _strmesesanterioranterior; }
            set { _strmesesanterioranterior = value; }
        }
        #endregion

        private String _datosvarios;

        /// <summary>
        /// Sirve para incluir valores adicionales separados por Coma (;).
        /// Se debe incluir el nombre del campo y el valor del mismo separado por dos puntos (:).
        /// Por ejemplo "codigofise:501001000;"
        /// </summary>
        [Campo()]
        public String DatosVarios
        {
            get { return _datosvarios; }
            set { _datosvarios = value; }
        }

        [Campo()]
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }



        public override bool Equals(object obj)
        {
            clsReciboMayorCabecera oCabecera = (clsReciboMayorCabecera)obj;
            return (this.IdNroServicio == oCabecera.IdNroServicio && this.Periodo == oCabecera.Periodo);
        }

        public override int GetHashCode()
        {
            return this.IdNroServicio ^ this.Periodo;
        }

        public clsReciboMayorCabecera()
        {
        }

        public clsReciboMayorCabecera(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }
    }

    public class clsListaReciboMayorCabecera
    {
        #region Campos

        private List<clsReciboMayorCabecera> _objelementos = new List<clsReciboMayorCabecera>();

        #endregion Campos

        #region Propiedades

        public List<clsReciboMayorCabecera> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMayorCabecera()
        {
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMayorCabecera(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];

            Int32 colIdTipoDocumento = dr.GetOrdinal("IdTipoDocumento");
            Int32 colNroDocumento = dr.GetOrdinal("NroDocumento");
            Int32 colNombreDistrito = dr.GetOrdinal("NombreDistrito");
            Int32 colNombreProvincia = dr.GetOrdinal("NombreProvincia");
            Int32 colIdNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colNombreNroServicio = dr.GetOrdinal("NombreNroServicio");
            Int32 colDireccion = dr.GetOrdinal("Direccion");
            Int32 colRUC = dr.GetOrdinal("RUC");
            Int32 colIdAnoComercial = dr.GetOrdinal("IdAnoComercial");
            Int32 colIdMesComercial = dr.GetOrdinal("IdMesComercial");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colFechaEmision = dr.GetOrdinal("FechaEmision");
            Int32 colFechaVencimiento = dr.GetOrdinal("FechaVencimiento");
            Int32 colTotalMes = dr.GetOrdinal("TotalMes");
            Int32 colTotalAPagar = dr.GetOrdinal("TotalAPagar");
            Int32 colImporteAplicado = dr.GetOrdinal("ImporteAplicado");
            Int32 colDeudaAnteriorInicial = dr.GetOrdinal("DeudaAnteriorInicial");
            Int32 colNroMesesDeuda = dr.GetOrdinal("NroMesesDeuda");
            Int32 colRutaReparto = dr.GetOrdinal("RutaReparto");
            Int32 colIdDireccionCodificadaReparto = dr.GetOrdinal("IdDireccionCodificadaReparto");
            Int32 colDireccionCodificadaReparto = dr.GetOrdinal("DireccionCodificadaReparto");
            Int32 colIdEmpresaServicio = dr.GetOrdinal("IdEmpresaServicio");
            Int32 colIdMoneda = dr.GetOrdinal("IdMoneda");
            Int32 colSimboloMoneda = dr.GetOrdinal("SimboloMoneda");
            Int32 colIdSector = dr.GetOrdinal("IdSector");

            Int32 colbDuplicado = dr.GetOrdinal("Duplicado");
            Int32 colsNroenLetras = dr.GetOrdinal("NroenLetras");
            Int32 colsCalificacion = dr.GetOrdinal("Calificacion");
            Int32 colsModalidadFacturacion = dr.GetOrdinal("ModalidadFacturacion");
            Int32 colsTarifa = dr.GetOrdinal("Tarifa");
            Int32 colsMedicion = dr.GetOrdinal("Medicion");
            Int32 colsTension = dr.GetOrdinal("Tension");
            Int32 colsSED = dr.GetOrdinal("SED");
            Int32 colsTipoConexion = dr.GetOrdinal("TipoConexion");
            Int32 colsSerieMedidor = dr.GetOrdinal("SerieMedidor");
            Int32 colsModalidad = dr.GetOrdinal("Modalidad");
            Int32 coltInicioContrato = dr.GetOrdinal("InicioContrato");
            Int32 coltTerminoContrato = dr.GetOrdinal("TerminoContrato");
            Int32 coldPromedioMaximaDemanda = dr.GetOrdinal("PromedioMaximaDemanda");
            Int32 coldPotenciaContratada = dr.GetOrdinal("PotenciaContratada");
            Int32 coldPromedioMaximaDemandaPunta = dr.GetOrdinal("PromedioMaximaDemandaPunta");
            Int32 coldPromedioMaximaDemandaFueraPunta = dr.GetOrdinal("PromedioMaximaDemandaFueraPunta");
            Int32 coldPotenciaContratadaPunta = dr.GetOrdinal("PotenciaContratadaPunta");
            Int32 coldPotenciaContratadaFueraPunta = dr.GetOrdinal("PotenciaContratadaFueraPunta");
            Int32 coldConsumoDesde = dr.GetOrdinal("ConsumoDesde");
            Int32 coldConsumoHasta = dr.GetOrdinal("ConsumoHasta");
            Int32 colMesComercial = dr.GetOrdinal("MesComercial");
            Int32 colDireccionReferencial = dr.GetOrdinal("DireccionReferencial");
            Int32 colImagenEmpresa = dr.GetOrdinal("ImagenEmpresa");
            Int32 colImagenSector = dr.GetOrdinal("ImagenSector");
            Int32 colIndicadorPotencia = dr.GetOrdinal("IndicadorPotencia");
            Int32 colIndicadorMaximaDemanda = dr.GetOrdinal("IndicadorMaximaDemanda");
            Int32 colNroHilos = dr.GetOrdinal("NroHilos");
            Int32 colNroHorasPunta = dr.GetOrdinal("NroHorasPunta");
            Int32 colEsImagen = dr.GetOrdinal("EsImagen");
            Int32 colParaCorte = dr.GetOrdinal("ParaCorte");
            Int32 colNroPagina = dr.GetOrdinal("NroPagina");
            Int32 colDatosVarios = dr.GetOrdinal("DatosVarios");
            Int32 colMesesAnterior = dr.GetOrdinal("MesesAnterior");
            Int32 colMesesAnteriorAnterior = dr.GetOrdinal("MesesAnteriorAnterior");
            Int32 colidempresa = dr.GetOrdinal("IdEmpresa");

            Int32 colcodigosed = 0;

            DataTable _dt = dr.GetSchemaTable();

            Int16 _intcodigotecnico = 0;

            foreach (DataRow item in _dt.Rows)
            {
                if (item[0].ToString() == "CodigoTecnico")
                {
                    colcodigosed = dr.GetOrdinal("CodigoTecnico");
                    _intcodigotecnico = 1;
                }
            }

            while (dr.Read())
            {

                clsReciboMayorCabecera oReciboMayorCabecera = new clsReciboMayorCabecera();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                // asignar los valores al objeto.
                oReciboMayorCabecera.IdTipoDocumento = Convert.ToInt16(values[colIdTipoDocumento]);
                oReciboMayorCabecera.NroDocumento = Convert.ToString(values[colNroDocumento]);
                oReciboMayorCabecera.NombreDistrito = Convert.ToString(values[colNombreDistrito]);
                oReciboMayorCabecera.NombreProvincia = Convert.ToString(values[colNombreProvincia]);
                oReciboMayorCabecera.IdNroServicio = Convert.ToInt32(values[colIdNroServicio]);
                oReciboMayorCabecera.NombreNroServicio = Convert.ToString(values[colNombreNroServicio]);
                oReciboMayorCabecera.Direccion = Convert.ToString(values[colDireccion]);
                oReciboMayorCabecera.RUC = Convert.ToString(values[colRUC]);
                oReciboMayorCabecera.IdAnoComercial = Convert.ToInt32(values[colIdAnoComercial]);
                oReciboMayorCabecera.IdMesComercial = Convert.ToInt16(values[colIdMesComercial]);
                oReciboMayorCabecera.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMayorCabecera.FechaEmision = Convert.ToDateTime(values[colFechaEmision]);
                oReciboMayorCabecera.FechaVencimiento = Convert.ToDateTime(values[colFechaVencimiento]);
                oReciboMayorCabecera.TotalMes = Convert.ToDecimal(values[colTotalMes]);
                oReciboMayorCabecera.TotalAPagar = Convert.ToDecimal(values[colTotalAPagar]);
                oReciboMayorCabecera.ImporteAplicado = Convert.ToDecimal(values[colImporteAplicado]);
                oReciboMayorCabecera.DeudaAnteriorInicial = Convert.ToDecimal(values[colDeudaAnteriorInicial]);
                oReciboMayorCabecera.NroMesesDeuda = Convert.ToInt16(values[colNroMesesDeuda]);
                oReciboMayorCabecera.RutaReparto = Convert.ToString(values[colRutaReparto]);
                oReciboMayorCabecera.IdDireccionCodificadaReparto = Convert.IsDBNull(values[colIdDireccionCodificadaReparto]) ? (Int32?)null : Convert.ToInt32(values[colIdDireccionCodificadaReparto]);
                oReciboMayorCabecera.DireccionCodificadaReparto = Convert.ToString(values[colDireccionCodificadaReparto]);
                oReciboMayorCabecera.IdEmpresaServicio = Convert.ToInt16(values[colIdEmpresaServicio]);
                oReciboMayorCabecera.IdMoneda = Convert.ToInt16(values[colIdMoneda]);
                oReciboMayorCabecera.SimboloMoneda = Convert.ToString(values[colSimboloMoneda]);
                oReciboMayorCabecera.IdSector = Convert.ToInt16(values[colIdSector]);
                oReciboMayorCabecera.Duplicado = Convert.ToBoolean(values[colbDuplicado]);
                oReciboMayorCabecera.NroenLetras = values[colsNroenLetras].ToString();
                oReciboMayorCabecera.Calificacion = values[colsCalificacion].ToString();
                oReciboMayorCabecera.ModalidadFacturacion = values[colsModalidadFacturacion].ToString();
                oReciboMayorCabecera.Tarifa = values[colsTarifa].ToString();
                oReciboMayorCabecera.Medicion = values[colsMedicion].ToString();
                oReciboMayorCabecera.Tension = values[colsTension].ToString();
                oReciboMayorCabecera.SED = values[colsSED].ToString();
                oReciboMayorCabecera.TipoConexion = values[colsTipoConexion].ToString();
                oReciboMayorCabecera.SerieMedidor = values[colsSerieMedidor].ToString();
                oReciboMayorCabecera.Modalidad = values[colsModalidad].ToString();
                oReciboMayorCabecera.InicioContrato = Convert.ToDateTime(values[coltInicioContrato]);
                oReciboMayorCabecera.TerminoContrato = Convert.ToDateTime(values[coltTerminoContrato]);
                oReciboMayorCabecera.PromedioMaximaDemanda = Convert.ToDecimal(values[coldPromedioMaximaDemanda]);
                oReciboMayorCabecera.PotenciaContratada = Convert.ToDecimal(values[coldPotenciaContratada]);
                oReciboMayorCabecera.PromedioMaximaDemandaPunta = Convert.ToDecimal(values[coldPromedioMaximaDemandaPunta]);
                oReciboMayorCabecera.PromedioMaximaDemandaFueraPunta = Convert.ToDecimal(values[coldPromedioMaximaDemandaFueraPunta]);
                oReciboMayorCabecera.PotenciaContratadaPunta = Convert.ToDecimal(values[coldPotenciaContratadaPunta]);
                oReciboMayorCabecera.PotenciaContratadaFueraPunta = Convert.ToDecimal(values[coldPotenciaContratadaFueraPunta]);
                oReciboMayorCabecera.ConsumoDesde = Convert.ToDateTime(values[coldConsumoDesde]);
                oReciboMayorCabecera.ConsumoHasta = Convert.ToDateTime(values[coldConsumoHasta]);
                oReciboMayorCabecera.MesComercial = values[colMesComercial].ToString();
                oReciboMayorCabecera.DireccionReferencial = values[colDireccionReferencial].ToString();
                oReciboMayorCabecera.ImagenEmpresa = values[colImagenEmpresa].ToString();
                oReciboMayorCabecera.ImagenSector = values[colImagenSector].ToString();
                oReciboMayorCabecera.IndicadorPotencia = Convert.ToInt16(values[colIndicadorPotencia].ToString());
                oReciboMayorCabecera.IndicadorMaximaDemanda = Convert.ToInt16(values[colIndicadorMaximaDemanda].ToString());
                oReciboMayorCabecera.NroHilos = Convert.ToInt32(values[colNroHilos].ToString());
                oReciboMayorCabecera.NroHorasPunta = Convert.ToInt32(values[colNroHorasPunta].ToString());
                oReciboMayorCabecera.EsImagen = Convert.ToInt16(values[colEsImagen].ToString());
                oReciboMayorCabecera.ParaCorte = Convert.ToBoolean(values[colParaCorte]);
                oReciboMayorCabecera.NroPagina = Convert.ToInt32(values[colNroPagina]);
                oReciboMayorCabecera.CodigoTecnico = _intcodigotecnico == 0 ? "" : values[colcodigosed].ToString();
                oReciboMayorCabecera.DatosVarios = values[colDatosVarios].ToString();
                oReciboMayorCabecera.MesesAnterior = values[colMesesAnterior].ToString();
                oReciboMayorCabecera.MesesAnteriorAnterior = values[colMesesAnteriorAnterior].ToString();
                oReciboMayorCabecera.IdEmpresa = Convert.ToInt16(values[colidempresa]);

                this.Elementos.Add(oReciboMayorCabecera);
            }

            dr.Close();
        }

        #endregion

    }

    public class clsReciboMayorLectura
    {
        private Int32 iIdNroServicio;
        private Int32 iPeriodo;
        private String sNombreLectura;
        private Decimal dlecturaanterior;
        private Decimal dlecturaactual;
        private Decimal ddiferencia;
        private Decimal dfactormedicion;
        private Decimal dfactortransformacion;
        private Decimal dfactorcalificacion;
        private Int16 _intnocalifica;
        private Decimal dconsumo;

        [Campo()]
        public Int32 IdNroServicio
        {
            get { return iIdNroServicio; }
            set { iIdNroServicio = value; }
        }
        [Campo()]
        public Int32 Periodo
        {
            get { return iPeriodo; }
            set { iPeriodo = value; }
        }

        [Campo()]
        public String NombreLectura
        {
            get { return sNombreLectura; }
            set { sNombreLectura = value; }
        }

        [Campo()]
        public Decimal LecturaAnterior
        {
            get { return dlecturaanterior; }
            set { dlecturaanterior = value; }
        }

        [Campo()]
        public Decimal LecturaActual
        {
            get { return dlecturaactual; }
            set { dlecturaactual = value; }
        }

        [Campo()]
        public Decimal Diferencia
        {
            get { return ddiferencia; }
            set { ddiferencia = value; }
        }
        [Campo()]
        public Decimal FactorMedicion
        {
            get { return dfactormedicion; }
            set { dfactormedicion = value; }
        }
        [Campo()]
        public Decimal FactorTransformacion
        {
            get { return dfactortransformacion; }
            set { dfactortransformacion = value; }
        }
        [Campo()]
        public Decimal FactorCalificacion
        {
            get { return dfactorcalificacion; }
            set { dfactorcalificacion = value; }
        }
        [Campo()]
        public Int16 NoCalifica
        {
            get { return _intnocalifica; }
            set { _intnocalifica = value; }
        }
        [Campo()]
        public Decimal Consumo
        {
            get { return dconsumo; }
            set { dconsumo = value; }
        }

        public override bool Equals(object obj)
        {
            clsReciboMayorLectura oLectura = (clsReciboMayorLectura)obj;
            return (this.IdNroServicio == oLectura.IdNroServicio && this.Periodo == oLectura.Periodo);
        }

        public override int GetHashCode()
        {
            return this.IdNroServicio ^ this.Periodo;
        }

        public clsReciboMayorLectura()
        { }

        public clsReciboMayorLectura(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }

    }

    public class clsListaReciboMayorLectura
    {
        #region Campos

        private List<clsReciboMayorLectura> _objelementos = new List<clsReciboMayorLectura>();

        #endregion Campos

        #region Propiedades
        //[CampoLista()]
        public List<clsReciboMayorLectura> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMayorLectura()
        {
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMayorLectura(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];

            Int32 colNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colNombreLectura = dr.GetOrdinal("NombreLectura");
            Int32 colLecturaAnterior = dr.GetOrdinal("LecturaAnterior");
            Int32 colLecturaActual = dr.GetOrdinal("LecturaActual");
            Int32 colDiferencia = dr.GetOrdinal("Diferencia");
            Int32 colFactorMedicion = dr.GetOrdinal("FactorMedicion");
            Int32 colFactorTransformacion = dr.GetOrdinal("FactorTransformacion");
            Int32 colFactorCalificacion = dr.GetOrdinal("FactorCalificacion");
            Int32 colNoCalifica = dr.GetOrdinal("NoCalifica");
            Int32 colConsumo = dr.GetOrdinal("Consumo");
            while (dr.Read())
            {
                clsReciboMayorLectura oReciboMayorLectura = new clsReciboMayorLectura();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                // asignar los valores al objeto.
                oReciboMayorLectura.IdNroServicio = Convert.ToInt32(values[colNroServicio]);
                oReciboMayorLectura.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMayorLectura.NombreLectura = Convert.ToString(values[colNombreLectura]);
                oReciboMayorLectura.LecturaAnterior = Convert.ToDecimal(values[colLecturaAnterior]);
                oReciboMayorLectura.LecturaActual = Convert.ToDecimal(values[colLecturaActual]);
                oReciboMayorLectura.Diferencia = Convert.ToDecimal(values[colDiferencia]);
                oReciboMayorLectura.FactorMedicion = Convert.ToDecimal(values[colFactorMedicion]);
                oReciboMayorLectura.FactorTransformacion = Convert.ToDecimal(values[colFactorTransformacion]);
                oReciboMayorLectura.FactorCalificacion = Convert.ToDecimal(values[colFactorCalificacion]);
                oReciboMayorLectura.NoCalifica = Convert.ToInt16(values[colNoCalifica]);
                oReciboMayorLectura.Consumo = Convert.ToDecimal(values[colConsumo]);

                this.Elementos.Add(oReciboMayorLectura);
            }

            dr.Close();
        }

        #endregion

    }

    public class clsReciboMayorConcepto
    {
        private Int32 iIdNroServicio;
        private Int32 iPeriodo;
        private String sNombreConcepto;
        private String scantidad;
        private String sprecio;
        private String sImporte;
        [Campo()]
        public Int32 IdNroServicio
        {
            get { return iIdNroServicio; }
            set { iIdNroServicio = value; }
        }
        [Campo()]
        public String NombreConcepto
        {
            get { return sNombreConcepto; }
            set { sNombreConcepto = value; }
        }
        [Campo()]
        public Int32 Periodo
        {
            get { return iPeriodo; }
            set { iPeriodo = value; }
        }

        [Campo()]
        public String Cantidad
        {
            get { return scantidad; }
            set { scantidad = value; }
        }
        [Campo()]
        public String Precio
        {
            get { return sprecio; }
            set { sprecio = value; }
        }
        [Campo()]
        public String Importe
        {
            get { return sImporte; }
            set { sImporte = value; }
        }

        public override bool Equals(object obj)
        {
            clsReciboMayorConcepto oConcepto = (clsReciboMayorConcepto)obj;
            return (this.IdNroServicio == oConcepto.IdNroServicio && this.Periodo == oConcepto.Periodo);
        }

        public override int GetHashCode()
        {
            return this.IdNroServicio ^ this.Periodo;
        }

        public clsReciboMayorConcepto()
        { }

        public clsReciboMayorConcepto(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }

    }

    public class clsListaReciboMayorConcepto
    {
        #region Campos

        private List<clsReciboMayorConcepto> _objelementos = new List<clsReciboMayorConcepto>();

        #endregion Campos

        #region Propiedades
        //[CampoLista()]
        public List<clsReciboMayorConcepto> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMayorConcepto()
        {
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMayorConcepto(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];

            Int32 colNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colNombreConcepto = dr.GetOrdinal("NombreConcepto");
            Int32 colCantidad = dr.GetOrdinal("Cantidad");
            Int32 colPrecio = dr.GetOrdinal("Precio");
            Int32 colImporte = dr.GetOrdinal("Importe");

            while (dr.Read())
            {
                clsReciboMayorConcepto oReciboMayorConcepto = new clsReciboMayorConcepto();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                // asignar los valores al objeto.
                oReciboMayorConcepto.IdNroServicio = Convert.ToInt32(values[colNroServicio]);
                oReciboMayorConcepto.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMayorConcepto.NombreConcepto = Convert.ToString(values[colNombreConcepto]);
                oReciboMayorConcepto.Cantidad = Convert.ToString(values[colCantidad]);
                oReciboMayorConcepto.Precio = Convert.ToString(values[colPrecio]);
                oReciboMayorConcepto.Importe = Convert.ToString(values[colImporte]);

                this.Elementos.Add(oReciboMayorConcepto);
            }

            dr.Close();
        }

        #endregion

    }

    public class clsReciboMayorConsumo
    {
        #region Campos
        private Int32 iIdNroServicio;
        private Int32 iPeriodo;
        private Int32 iPeriodoConsumo;
        private Decimal deCantidadConsumo;
        private Int16 iIdConcepto;
        private String sAbreviaturaConcepto;
        private String sMes;
        private String _sesenergia;
        private Decimal decimportetotal;
        #endregion Campos

        #region Propiedades
        [Campo()]
        public Int32 idNroServicio
        {
            get { return iIdNroServicio; }
            set { iIdNroServicio = value; }
        }
        [Campo()]
        public Int32 Periodo
        {
            get { return iPeriodo; }
            set { iPeriodo = value; }
        }
        [Campo()]
        public Int32 PeriodoConsumo
        {
            get { return iPeriodoConsumo; }
            set { iPeriodoConsumo = value; }
        }
        [Campo()]
        public Decimal CantidadConsumo
        {
            get { return deCantidadConsumo; }
            set { deCantidadConsumo = value; }
        }
        [Campo()]
        public Int16 IdConcepto
        {
            get { return iIdConcepto; }
            set { iIdConcepto = value; }
        }
        [Campo()]
        public String AbreviaturaConcepto
        {
            get { return sAbreviaturaConcepto; }
            set { sAbreviaturaConcepto = value; }
        }
        [Campo()]
        public String Mes
        {
            get { return sMes; }
            set { sMes = value; }
        }
        [Campo()]
        public String EsEnergia
        {
            get { return _sesenergia; }
            set { _sesenergia = value; }
        }
        [Campo()]
        public Decimal ImporteTotal
        {
            get { return decimportetotal; }
            set { decimportetotal = value; }
        }

        #endregion Propiedades

        public override bool Equals(object obj)
        {
            clsReciboMayorConsumo oConsumo = (clsReciboMayorConsumo)obj;
            return (this.idNroServicio == oConsumo.idNroServicio && this.Periodo == oConsumo.Periodo);
        }

        public override int GetHashCode()
        {
            return this.idNroServicio ^ this.Periodo;
        }

        public clsReciboMayorConsumo()
        { }

        public clsReciboMayorConsumo(DataRow dr)
        {
            CargarEntidad.EstablecerDataRow(this, dr);
        }
    }

    public class clsListaReciboMayorConsumo
    {
        #region Campos

        private List<clsReciboMayorConsumo> _objelementos = new List<clsReciboMayorConsumo>();

        #endregion Campos

        #region Propiedades

        public List<clsReciboMayorConsumo> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        public clsListaReciboMayorConsumo()
        {
        }

        public clsListaReciboMayorConsumo(DataTable entidad)
        {
            foreach (DataRow item in entidad.Rows)
            {
                Elementos.Add(new clsReciboMayorConsumo(item));
            }
        }

        /// <summary>
        /// Constructor desde un SqlDataReader
        /// </summary>
        public clsListaReciboMayorConsumo(SqlDataReader dr)
        {
            int colCount = dr.FieldCount;
            object[] values = new object[colCount];


            Int32 colIdNroServicio = dr.GetOrdinal("IdNroServicio");
            Int32 colPeriodo = dr.GetOrdinal("Periodo");
            Int32 colPeriodoConsumo = dr.GetOrdinal("PeriodoConsumo");
            Int32 colIdConcepto = dr.GetOrdinal("IdConcepto");
            Int32 colCantidadConsumo = dr.GetOrdinal("CantidadConsumo");
            Int32 colAbreviaturaConcepto = dr.GetOrdinal("AbreviaturaConcepto");
            Int32 colMes = dr.GetOrdinal("Mes");
            Int32 colEsEnergia = dr.GetOrdinal("EsEnergia");

            while (dr.Read())
            {
                clsReciboMayorConsumo oReciboMayorConsumo = new clsReciboMayorConsumo();

                // Llamar los datos del metadato.
                dr.GetValues(values);

                // asignar los valores al objeto.
                oReciboMayorConsumo.idNroServicio = Convert.ToInt32(values[colIdNroServicio]);
                oReciboMayorConsumo.Periodo = Convert.ToInt32(values[colPeriodo]);
                oReciboMayorConsumo.PeriodoConsumo = Convert.ToInt32(values[colPeriodoConsumo]);
                oReciboMayorConsumo.IdConcepto = Convert.ToInt16(values[colIdConcepto]);
                oReciboMayorConsumo.CantidadConsumo = Convert.ToDecimal(values[colCantidadConsumo]);
                oReciboMayorConsumo.AbreviaturaConcepto = Convert.ToString(values[colAbreviaturaConcepto]);
                oReciboMayorConsumo.Mes = Convert.ToString(values[colMes]);
                oReciboMayorConsumo.EsEnergia = values[colEsEnergia].ToString();

                this.Elementos.Add(oReciboMayorConsumo);
            }

            dr.Close();
        }

        #endregion
    }

    #endregion ReciboMayor

    #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

    #region Tareas

    [XmlInclude(typeof(clsParametroTarea))]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class ClsTareaProgramacion
    {
        #region propiedades

        /// <summary>
        ///
        /// </summary>
        public clsParametroTarea ParametroTarea { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string Proceso { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int NroUltimaIteracion { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool FechaIndefinida { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool UltimoDia { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool PrimerDia { get; set; }

        /// <summary>
        ///
        /// </summary>
        public short? Dia { get; set; }

        /// <summary>
        ///
        /// </summary>
        public short? Mes { get; set; }

        /// <summary>
        ///
        /// </summary>
        public bool EsRecurrente { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaInicioProgramada { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaFinProgramada { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int IdTarea { get; set; }

        /// <summary>
        ///
        /// </summary>
        public short Frecuencia { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int IdUsuarioGenerador { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int IdProceso { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int IdPrioridad { get; set; }

        /// <summary>
        ///
        /// </summary>
        public short? Estado { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaProgramacion { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaUltimaEjecucion { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime? HoraInicio { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string TagResultado { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string TagValida { get; set; }

        public int? IntervaloEnSegundos { get; set; }

        public short IdEmpresa { get; set; }

        public short IdUUNN { get; set; } //CanviaDev_2020-05-27

        #endregion propiedades

        #region metodos serializacion

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public SqlXml DevolvercomandoSqlXml()
        {
            var Serializer = new XmlSerializer(typeof(clsParametroTarea));

            //using (var ms = new MemoryStream())
            //{
            //    Serializer.Serialize(ms, ParametroTarea);
            //    return new SqlXml(ms);
            //}

            MemoryStream ms = new MemoryStream();
            Serializer.Serialize(ms, ParametroTarea);
            return new SqlXml(ms);
        }

        public string DevolvercomandoSqlXml(Type type)
        {
            var Serializer = new XmlSerializer(type);

            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, ParametroTarea);

                using (var stringWriter = new StringWriter())
                {
                    Serializer.Serialize(stringWriter, ParametroTarea);

                    return stringWriter.ToString();
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="xmlString"></param>
        public void EstablecerComando(string xmlString)
        {
            var Serializer = new XmlSerializer(typeof(clsParametroTarea));
            var xtrReader = new XmlTextReader(xmlString, XmlNodeType.Document, null);
            ParametroTarea = (clsParametroTarea)Serializer.Deserialize(xtrReader);
        }

        #endregion metodos serializacion

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        public ClsTareaProgramacion(DataRow registro)
        {
            DataColumnCollection columnas = registro.Table.Columns;
            if (registro == null) { return; }
            if (columnas.Contains("idtarea")) IdTarea = Convert.ToInt32(registro["idtarea"]);
            if (columnas.Contains("idusuariogenerador")) IdUsuarioGenerador = Convert.ToInt32(registro["idusuariogenerador"]);
            if (columnas.Contains("idproceso")) IdProceso = Convert.ToInt32(registro["idproceso"]);
            if (columnas.Contains("idprioridad")) IdPrioridad = Convert.ToInt32(registro["idprioridad"]);
            if (columnas.Contains("horainicio")) FechaInicioProgramada = Convert.ToDateTime(registro["horainicio"]);
            if (columnas.Contains("idfrecuencia")) Frecuencia = Convert.ToInt16(registro["idfrecuencia"]);
            if (columnas.Contains("comandotarea")) EstablecerComando(registro["comandotarea"].ToString());
            if (columnas.Contains("fechaprogramaciontarea")) FechaProgramacion = (!registro.IsNull("fechaprogramaciontarea")) ? (DateTime?)Convert.ToDateTime(registro["fechaprogramaciontarea"]) : null;
            if (columnas.Contains("fechaultimaejecucion")) FechaUltimaEjecucion = (!registro.IsNull("fechaultimaejecucion")) ? (DateTime?)Convert.ToDateTime(registro["fechaultimaejecucion"]) : null;
            if (columnas.Contains("fechafinprogramada")) FechaFinProgramada = (!registro.IsNull("fechafinprogramada")) ? (DateTime?)Convert.ToDateTime(registro["fechafinprogramada"]) : null;
            if (columnas.Contains("esrecurrente")) EsRecurrente = Convert.ToBoolean(registro["esrecurrente"]);
            if (columnas.Contains("primerdia")) PrimerDia = Convert.ToBoolean(registro["primerdia"]);
            if (columnas.Contains("ultimodia")) UltimoDia = Convert.ToBoolean(registro["ultimodia"]);
            if (columnas.Contains("fechafinindefinida")) FechaIndefinida = Convert.ToBoolean(registro["fechafinindefinida"]);
            if (columnas.Contains("dia")) Dia = (registro.IsNull("dia")) ? 0 : (Int16?)(registro["dia"]);
            if (columnas.Contains("mes")) Dia = (registro.IsNull("mes")) ? 0 : (Int16?)(registro["mes"]);
            if (columnas.Contains("estado")) Estado = (Int16?)(registro["estado"]);
            if (columnas.Contains("nroultimaiteracion")) NroUltimaIteracion = (Int32)(registro["nroultimaiteracion"]);
            if (columnas.Contains("proceso")) Proceso = registro["proceso"].ToString();
            if (columnas.Contains("tagresultado")) TagResultado = registro["tagresultado"].ToString();
            if (columnas.Contains("intervaloensegundos")) IntervaloEnSegundos = Convert.ToInt32(registro["intervaloensegundos"]);
            if (columnas.Contains("tagvalida")) TagValida = registro["tagvalida"].ToString();
        }

        /// <summary>
        ///
        /// </summary>
        public ClsTareaProgramacion()
        {
        }
    }

    [XmlInclude(typeof(clsParametroTarea))]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsParametroImprimirRecibos : clsParametroTarea
    {
        #region campos

        private ClsListaSuministrosSectorRecibo _objlistasectores;
        private clsListaSuministrosRutaRecibo _objlistarutas;
        private Int16 _intiduunn;
        private Int16 _intidcentroservicio;
        private Int16 _intiduunnpadre;
        private Int16 _intidcentroserviciopadre;
        private Int32 _intperiodo;
        private Int16 _intidano;
        private Int16 _intidmes;
        private Int16 _intidestrato;
        private String _strcartera;
        private Int16 _intesruta;
        private Int16 _intesduplicado;
        private String _strarchivoblanco;
        private Int16 _intidproveedoractividad;
        private String _strnombrearchivo;
        private Int16 _intidempresa;
        private Int16 _intidempresaservicio;
        private Int16 _intesexportarmayor;
        private Int32 _intidlote;
        private Boolean _bolesfacturacionmensual;
        private Int16 _intidsector;
        private Int32 _intidusuario;
        private Int32 _intidrutareparto;
        private Int16 _intindicadorpostal;
        private Int16 _intidservicioflujo;
        private TipoImpresionEnum _enumtipoimp = TipoImpresionEnum.ImpresionRuta;
        private Int32 _intidnroserviciodesde;
        private Int32 _intidnroserviciohasta;
        private Int32 _intidciclo;
        private Boolean _esEnviarCorreo;

        #region xxx

        private int _intIdSolicitudImpresion;

        #endregion

        #endregion campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public int IdNroServicioHasta
        {
            get { return _intidnroserviciohasta; }
            set { _intidnroserviciohasta = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int IdNroServicioDesde
        {
            get { return _intidnroserviciodesde; }
            set { _intidnroserviciodesde = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public TipoImpresionEnum enumTipoImpresion
        {
            get { return _enumtipoimp; }
            set { _enumtipoimp = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int IdRutaReparto
        {
            get { return _intidrutareparto; }
            set { _intidrutareparto = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IndicadorPostal
        {
            get { return _intindicadorpostal; }
            set { _intindicadorpostal = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdServicioFlujo
        {
            get { return _intidservicioflujo; }
            set { _intidservicioflujo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdSector
        {
            get { return _intidsector; }
            set { _intidsector = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public bool EsFacturacionMensual
        {
            get { return _bolesfacturacionmensual; }
            set { _bolesfacturacionmensual = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdEmpresaServicio
        {
            get { return _intidempresaservicio; }
            set { _intidempresaservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int IdLote
        {
            get { return _intidlote; }
            set { _intidlote = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short EsExportarMayor
        {
            get { return _intesexportarmayor; }
            set { _intesexportarmayor = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public string NombreArchivo
        {
            get { return _strnombrearchivo; }
            set { _strnombrearchivo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public clsListaSuministrosRutaRecibo ListaRutas
        {
            get { return _objlistarutas; }
            set { _objlistarutas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ClsListaSuministrosSectorRecibo ListaSectores
        {
            get { return _objlistasectores; }
            set { _objlistasectores = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdUUNNPadre
        {
            get { return _intiduunnpadre; }
            set { _intiduunnpadre = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdCentroServicioPadre
        {
            get { return _intidcentroserviciopadre; }
            set { _intidcentroserviciopadre = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdAno
        {
            get { return _intidano; }
            set { _intidano = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdMes
        {
            get { return _intidmes; }
            set { _intidmes = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdEstrato
        {
            get { return _intidestrato; }
            set { _intidestrato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public string Cartera
        {
            get { return _strcartera; }
            set { _strcartera = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short EsRuta
        {
            get { return _intesruta; }
            set { _intesruta = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short EsDuplicado
        {
            get { return _intesduplicado; }
            set { _intesduplicado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public string ArchivoBlanco
        {
            get { return _strarchivoblanco; }
            set { _strarchivoblanco = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdProveedorActividad
        {
            get { return _intidproveedoractividad; }
            set { _intidproveedoractividad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int IdCiclo
        {
            get { return _intidciclo; }
            set { _intidciclo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool EsEnviarCorreo
        {
            get { return _esEnviarCorreo; }
            set { _esEnviarCorreo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int IdSolicitudImpresion
        {
            get { return _intIdSolicitudImpresion; }
            set { _intIdSolicitudImpresion = value; }
        }

        /// <summary>
        /// Guarda información teniendo en cuenta la siguiente estructura:
        /// clave:valor
        /// por ejemplo: se desea enviar el código de un suministro y su correo electrónico:
        /// idnroservicio:46118192;correo:alguien@ejemplo.com
        /// En la Clase Utilidades existe el método ObtenerValorDatosVarios() para descubrir cada dato.
        /// </summary>
        public string DatosVarios { get; set; }

        #endregion Propiedades

        /// <summary>
        ///
        /// </summary>
        public clsParametroImprimirRecibos()
        {
            base.ClaseComando = "OptimusNG.ReglasNegocio.Procesos.clsGestionarServicioRecibo";
        }
    }

    [XmlInclude(typeof(clsParametroImprimirRecibos))]
    [XmlInclude(typeof(ClsParametroImpresionRecibosNotificar))]
    [XmlInclude(typeof(ClsParametroRegistroTransaccionPagoVisa))]
    [XmlInclude(typeof(clsParametroTareaDepositoBancarioClienteEmpresa))]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public abstract class clsParametroTarea
    {
        public string ClaseComando { get; set; }
    }

    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    [Serializable()]
    public class clsListaSuministrosRutaRecibo
    {
        #region Campos

        private List<clsSuministrosRutaRecibo> _objelementos = new List<clsSuministrosRutaRecibo>();
        private List<clsSuministrosRutaRecibo> _objsuminitros = new List<clsSuministrosRutaRecibo>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>   
        [CampoLista()]
        public List<clsSuministrosRutaRecibo> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        /// <summary>
        /// 
        /// </summary>        
        public List<clsSuministrosRutaRecibo> ListaSuministrosRecibo
        {
            get { return _objsuminitros; }
            set { _objsuminitros = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaSuministrosRutaRecibo()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaSuministrosRutaRecibo(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsSuministrosRutaRecibo(_drw));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entidad"></param>
        public void CargarSuministros(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow item in entidad.Rows)
            {
                ListaSuministrosRecibo.Add(new clsSuministrosRutaRecibo(item));
            }
        }

        #endregion

    }

    [Serializable()]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsSuministrosRutaRecibo
    {
        #region Campos

        private Int16 _intidsector;
        private String _strnombresector;
        private Int32 _intidrutareparto;
        private String _strnombreruta;
        private Int32 _intnroservicio;
        private String _strnombrenroservicio;
        private String _strdireccion;
        private Int32 _intcantidadtotal;
        private Int32 _intcantidademitidos;
        private Int32 _intcantidadimprimidos;
        private Int16 _intidestado;
        private String _strdescripcionestado;
        private String _strnombrecentroservicio;
        private String _strnombreproveedor;
        private String _strcartera;
        private Int32 _intperiodo;
        private Int16 _intindicaascendente;
        private Boolean _bolsel;
        private Int16 _intespostal;
        private Int32 _intidciclo;
        //ini
        private String _strnombrerutareparto;
        private String _strnombreuunn;
        private String _strnombreciclo;
        private Int16 _intidestrato;
        private Int32 _intnormal;
        private Int32 _intpostal;
        private Int32 _intgrupopostal;
        //
        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        [Campo("EsPostal")]
        public Int16 EsPostal
        {
            get { return _intespostal; }
            set { _intespostal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IndicaAscendente")]
        public Int16 IndicaAscendente
        {
            get { return _intindicaascendente; }
            set { _intindicaascendente = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Sel")]
        public Boolean Sel
        {
            get { return _bolsel; }
            set { _bolsel = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Sel")]
        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdSector")]
        public Int16 IdSector
        {
            get { return _intidsector; }
            set { _intidsector = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreSector")]
        public String NombreSector
        {
            get { return _strnombresector; }
            set { _strnombresector = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreProveedor")]
        public String NombreProveedor
        {
            get { return _strnombreproveedor; }
            set { _strnombreproveedor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdRutaReparto")]
        public Int32 IdRutaReparto
        {
            get { return _intidrutareparto; }
            set { _intidrutareparto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreRuta")]
        public String NombreRuta
        {
            get { return _strnombreruta; }
            set { _strnombreruta = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("IdNroServicio")]
        public Int32 IdNroServicio
        {
            get { return _intnroservicio; }
            set { _intnroservicio = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreNroServicio")]
        public String NombreNroServicio
        {
            get { return _strnombrenroservicio; }
            set { _strnombrenroservicio = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("Direccion")]
        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("CantidadTotal")]
        public Int32 CantidadTotal
        {
            get { return _intcantidadtotal; }
            set { _intcantidadtotal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("CantidadEmitidos")]
        public Int32 CantidadEmitidos
        {
            get { return _intcantidademitidos; }
            set { _intcantidademitidos = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("CantidadImprimidos")]
        public Int32 CantidadImprimidos
        {
            get { return _intcantidadimprimidos; }
            set { _intcantidadimprimidos = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("IdEstado")]
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("DescripcionEstado")]
        public String DescripcionEstado
        {
            get { return _strdescripcionestado; }
            set { _strdescripcionestado = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreCentroServicio")]
        public String NombreCentroServicio
        {
            get { return _strnombrecentroservicio; }
            set { _strnombrecentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Cartera")]
        public String Cartera
        {
            get { return _strcartera; }
            set { _strcartera = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdCiclo")]
        public Int32 IdCiclo
        {
            get { return _intidciclo; }
            set { _intidciclo = value; }
        }

        //ini
        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreRutaReparto")]
        public String NombreRutaReparto
        {
            get { return _strnombrerutareparto; }
            set { _strnombrerutareparto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("UnidadNegocio")]
        public String NombreUUNN
        {
            get { return _strnombreuunn; }
            set { _strnombreuunn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Ciclo")]
        public String NombreCiclo
        {
            get { return _strnombreciclo; }
            set { _strnombreciclo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdEstrato")]
        public Int16 IdEstrato
        {
            get { return _intidestrato; }
            set { _intidestrato = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Normal")]
        public Int32 Normal
        {
            get { return _intnormal; }
            set { _intnormal = value; }
        }

        /// <summary>
        /// Postal
        /// </summary>
        [Campo("Postal")]
        public Int32 Postal
        {
            get { return _intpostal; }
            set { _intpostal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("GrupoPostal")]
        public Int32 GrupoPostal
        {
            get { return _intgrupopostal; }
            set { _intgrupopostal = value; }
        }

        //

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsSuministrosRutaRecibo()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsSuministrosRutaRecibo(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idsector")) IdSector = (short)dr["idsector"];
            if (dr.Table.Columns.Contains("idciclo")) IdCiclo = (short)dr["idciclo"];

            if (dr.Table.Columns.Contains("nombresector")) NombreSector = dr["nombresector"].ToString();
            if (dr.Table.Columns.Contains("sector")) NombreSector = dr["sector"].ToString();

            if (dr.Table.Columns.Contains("idrutareparto")) IdRutaReparto = (int)dr["idrutareparto"];

            if (dr.Table.Columns.Contains("nombreruta")) NombreRuta = dr["nombreruta"].ToString();
            if (dr.Table.Columns.Contains("rutareparto")) NombreRuta = dr["rutareparto"].ToString();

            if (dr.Table.Columns.Contains("nombrecentroservicio")) NombreCentroServicio = dr["nombrecentroservicio"].ToString();
            if (dr.Table.Columns.Contains("idnroservicio")) IdNroServicio = (int)dr["idnroservicio"];
            if (dr.Table.Columns.Contains("nombrenroservicio")) NombreNroServicio = dr["nombrenroservicio"].ToString();
            if (dr.Table.Columns.Contains("direccion")) Direccion = dr["direccion"].ToString();

            if (dr.Table.Columns.Contains("cantidadtotal")) CantidadTotal = (int)dr["cantidadtotal"];
            if (dr.Table.Columns.Contains("totalsumin")) CantidadTotal = (int)dr["totalsumin"];

            if (dr.Table.Columns.Contains("cantidademitidos")) CantidadEmitidos = (int)dr["cantidademitidos"];
            if (dr.Table.Columns.Contains("totalemitidos")) CantidadEmitidos = (int)dr["totalemitidos"];


            if (dr.Table.Columns.Contains("cantidadimprimidos")) CantidadImprimidos = (int)dr["cantidadimprimidos"];
            if (dr.Table.Columns.Contains("idestado")) IdEstado = (short)dr["idestado"];
            if (dr.Table.Columns.Contains("descripcionestado")) DescripcionEstado = dr["descripcionestado"].ToString();
            if (dr.Table.Columns.Contains("nombrecentroservicio")) NombreCentroServicio = dr["nombrecentroservicio"].ToString();
            if (dr.Table.Columns.Contains("nombreproveedor")) NombreProveedor = dr["nombreproveedor"].ToString();
            if (dr.Table.Columns.Contains("periodo")) Periodo = (int)dr["periodo"];
            if (dr.Table.Columns.Contains("cartera")) Cartera = dr["cartera"].ToString();
            if (dr.Table.Columns.Contains("indicaascendente")) IndicaAscendente = (short)dr["indicaascendente"];
            //
            if (dr.Table.Columns.Contains("NombreRutaReparto")) NombreRutaReparto = dr["NombreRutaReparto"].ToString();
            if (dr.Table.Columns.Contains("NombreUnidadNegocio")) NombreUUNN = dr["NombreUnidadNegocio"].ToString();
            if (dr.Table.Columns.Contains("NombreCiclo")) NombreCiclo = dr["NombreCiclo"].ToString();
            if (dr.Table.Columns.Contains("IdEstrato")) IdEstrato = (short)dr["IdEstrato"];
            if (dr.Table.Columns.Contains("Normal")) Normal = (int)dr["Normal"];
            if (dr.Table.Columns.Contains("Postal")) Postal = (int)dr["Postal"];
            if (dr.Table.Columns.Contains("GrupoPostal")) GrupoPostal = (int)dr["GrupoPostal"];
            //
            Sel = true;
        }

        #endregion

    }

    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    [Serializable()]
    public class ClsListaSuministrosSectorRecibo
    {
        #region Campos

        private List<ClsSuministrosSectorRecibo> _objelementos = new List<ClsSuministrosSectorRecibo>();
        private List<ClsSuministrosSectorRecibo> _objsuminitros = new List<ClsSuministrosSectorRecibo>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>   
        [CampoLista()]
        public List<ClsSuministrosSectorRecibo> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        /// <summary>
        /// 
        /// </summary>        
        public List<ClsSuministrosSectorRecibo> ListaSuministrosRecibo
        {
            get { return _objsuminitros; }
            set { _objsuminitros = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public ClsListaSuministrosSectorRecibo()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public ClsListaSuministrosSectorRecibo(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new ClsSuministrosSectorRecibo(_drw));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entidad"></param>
        public void CargarSuministros(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow item in entidad.Rows)
            {
                ListaSuministrosRecibo.Add(new ClsSuministrosSectorRecibo(item));
            }
        }

        #endregion

    }

    [Serializable()]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class ClsSuministrosSectorRecibo
    {
        #region Campos

        private Int16 _intidsector;
        private String _strnombresector;
        private Int32 _intidrutareparto;
        private String _strnombreruta;
        private Int32 _intnroservicio;
        private String _strnombrenroservicio;
        private String _strdireccion;
        private Int32 _intcantidadtotal;
        private Int32 _intcantidademitidos;
        private Int32 _intcantidadimprimidos;
        private Int16 _intidestado;
        private String _strdescripcionestado;
        private String _strnombrecentroservicio;
        private String _strnombreproveedor;
        private String _strcartera;
        private Int32 _intperiodo;
        private Int16 _intindicaascendente;
        private Boolean _bolsel;
        private Int16 _intespostal;
        private Int32 _intidciclo;
        //ini
        private String _strnombrerutareparto;
        private String _strnombreuunn;
        private String _strnombreciclo;
        private Int16 _intidestrato;
        private Int32 _intnormal;
        private Int32 _intpostal;
        private Int32 _intgrupopostal;
        private DateTime? _dtfechaimpresion;
        private Int32 _intnroservicioimpresos;
        private Int16 _intiduunn;
        private clsListaSuministrosRutaRecibo _objlistarutas;
        //
        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        [Campo("EsPostal")]
        public Int16 EsPostal
        {
            get { return _intespostal; }
            set { _intespostal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IndicaAscendente")]
        public Int16 IndicaAscendente
        {
            get { return _intindicaascendente; }
            set { _intindicaascendente = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Sel")]
        public Boolean Sel
        {
            get { return _bolsel; }
            set { _bolsel = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Sel")]
        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdSector")]
        public Int16 IdSector
        {
            get { return _intidsector; }
            set { _intidsector = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreSector")]
        public String NombreSector
        {
            get { return _strnombresector; }
            set { _strnombresector = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreProveedor")]
        public String NombreProveedor
        {
            get { return _strnombreproveedor; }
            set { _strnombreproveedor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdRutaReparto")]
        public Int32 IdRutaReparto
        {
            get { return _intidrutareparto; }
            set { _intidrutareparto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreRuta")]
        public String NombreRuta
        {
            get { return _strnombreruta; }
            set { _strnombreruta = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("IdNroServicio")]
        public Int32 IdNroServicio
        {
            get { return _intnroservicio; }
            set { _intnroservicio = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreNroServicio")]
        public String NombreNroServicio
        {
            get { return _strnombrenroservicio; }
            set { _strnombrenroservicio = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("Direccion")]
        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("CantidadTotal")]
        public Int32 CantidadTotal
        {
            get { return _intcantidadtotal; }
            set { _intcantidadtotal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("CantidadEmitidos")]
        public Int32 CantidadEmitidos
        {
            get { return _intcantidademitidos; }
            set { _intcantidademitidos = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("CantidadImprimidos")]
        public Int32 CantidadImprimidos
        {
            get { return _intcantidadimprimidos; }
            set { _intcantidadimprimidos = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("IdEstado")]
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [Campo("DescripcionEstado")]
        public String DescripcionEstado
        {
            get { return _strdescripcionestado; }
            set { _strdescripcionestado = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreCentroServicio")]
        public String NombreCentroServicio
        {
            get { return _strnombrecentroservicio; }
            set { _strnombrecentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Cartera")]
        public String Cartera
        {
            get { return _strcartera; }
            set { _strcartera = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdCiclo")]
        public Int32 IdCiclo
        {
            get { return _intidciclo; }
            set { _intidciclo = value; }
        }

        //ini
        /// <summary>
        /// 
        /// </summary>
        [Campo("NombreRutaReparto")]
        public String NombreRutaReparto
        {
            get { return _strnombrerutareparto; }
            set { _strnombrerutareparto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("UnidadNegocio")]
        public String NombreUUNN
        {
            get { return _strnombreuunn; }
            set { _strnombreuunn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Ciclo")]
        public String NombreCiclo
        {
            get { return _strnombreciclo; }
            set { _strnombreciclo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("IdEstrato")]
        public Int16 IdEstrato
        {
            get { return _intidestrato; }
            set { _intidestrato = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("Normal")]
        public Int32 Normal
        {
            get { return _intnormal; }
            set { _intnormal = value; }
        }

        /// <summary>
        /// Postal
        /// </summary>
        [Campo("Postal")]
        public Int32 Postal
        {
            get { return _intpostal; }
            set { _intpostal = value; }
        }

        [Campo("IdUUNN")]
        public Int16 IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Campo("GrupoPostal")]
        public Int32 GrupoPostal
        {
            get { return _intgrupopostal; }
            set { _intgrupopostal = value; }
        }
        public DateTime? FechaImpresion
        {
            get { return _dtfechaimpresion; }
            set { _dtfechaimpresion = value; }
        }

        public Int32 NroServiciosImpresos
        {
            get { return _intnroservicioimpresos; }
            set { _intnroservicioimpresos = value; }
        }

        public clsListaSuministrosRutaRecibo ListaRutas
        {
            get { return _objlistarutas; }
            set { _objlistarutas = value; }
        }

        //

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public ClsSuministrosSectorRecibo()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public ClsSuministrosSectorRecibo(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idsector")) IdSector = (Int16)dr["idsector"];
            if (dr.Table.Columns.Contains("idciclo")) IdCiclo = (Int16)dr["idciclo"];
            if (dr.Table.Columns.Contains("nombresector")) NombreSector = dr["nombresector"].ToString();
            if (dr.Table.Columns.Contains("idrutareparto")) IdRutaReparto = (Int32)dr["idrutareparto"];
            if (dr.Table.Columns.Contains("nombreruta")) NombreRuta = dr["nombreruta"].ToString();
            if (dr.Table.Columns.Contains("nombrecentroservicio")) NombreCentroServicio = dr["nombrecentroservicio"].ToString();
            if (dr.Table.Columns.Contains("idnroservicio")) IdNroServicio = (Int32)dr["idnroservicio"];
            if (dr.Table.Columns.Contains("nombrenroservicio")) NombreNroServicio = dr["nombrenroservicio"].ToString();
            if (dr.Table.Columns.Contains("direccion")) Direccion = dr["direccion"].ToString();
            if (dr.Table.Columns.Contains("cantidadtotal")) CantidadTotal = (Int32)dr["cantidadtotal"];
            if (dr.Table.Columns.Contains("cantidademitidos")) CantidadEmitidos = (Int32)dr["cantidademitidos"];
            if (dr.Table.Columns.Contains("cantidadimprimidos")) CantidadImprimidos = (Int32)dr["cantidadimprimidos"];
            if (dr.Table.Columns.Contains("idestado")) IdEstado = (Int16)dr["idestado"];
            if (dr.Table.Columns.Contains("descripcionestado")) DescripcionEstado = dr["descripcionestado"].ToString();
            if (dr.Table.Columns.Contains("nombrecentroservicio")) NombreCentroServicio = dr["nombrecentroservicio"].ToString();
            if (dr.Table.Columns.Contains("nombreproveedor")) NombreProveedor = dr["nombreproveedor"].ToString();
            if (dr.Table.Columns.Contains("periodo")) Periodo = (Int32)dr["periodo"];
            if (dr.Table.Columns.Contains("cartera")) Cartera = dr["cartera"].ToString();
            if (dr.Table.Columns.Contains("indicaascendente")) IndicaAscendente = (Int16)dr["indicaascendente"];
            //
            if (dr.Table.Columns.Contains("NombreRutaReparto")) NombreRutaReparto = dr["NombreRutaReparto"].ToString();
            if (dr.Table.Columns.Contains("NombreUnidadNegocio")) NombreUUNN = dr["NombreUnidadNegocio"].ToString();
            if (dr.Table.Columns.Contains("NombreCiclo")) NombreCiclo = dr["NombreCiclo"].ToString();
            if (dr.Table.Columns.Contains("IdEstrato")) IdEstrato = (Int16)dr["IdEstrato"];
            if (dr.Table.Columns.Contains("Normal")) Normal = (Int32)dr["Normal"];
            if (dr.Table.Columns.Contains("Postal")) Postal = (Int32)dr["Postal"];
            if (dr.Table.Columns.Contains("GrupoPostal")) GrupoPostal = (Int32)dr["GrupoPostal"];

            FechaImpresion = Convert.IsDBNull(dr["fechaimpresion"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechaimpresion"]);
            if (dr.Table.Columns.Contains("NroServiciosImpresos")) NroServiciosImpresos = Convert.IsDBNull(dr["nroserviciosimpresos"]) ? (Int32)0 : Convert.ToInt32(dr["nroserviciosimpresos"]);
            if (dr.Table.Columns.Contains("IdUUNN")) IdUUNN = Convert.IsDBNull(dr["IdUUNN"]) ? (Int16)0 : Convert.ToInt16(dr["IdUUNN"]);
            //
            Sel = true;
        }

        #endregion

    }

    [XmlInclude(typeof(clsParametroTarea))]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class ClsParametroImpresionRecibosNotificar : clsParametroTarea
    {
        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public int IdSolicitudImpresion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TipoNotificacionEnum EnumTipoNotificacion { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int IdUsuario { get; set; }

        /// <summary>
        ///
        /// </summary>
        public short IdEmpresa { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public short IdUUNNPadre { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public short IdCentroServicioPadre { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Periodo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Cartera { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string ListaIdsSuministros { get; set; }

        /// <summary>
        /// Guarda información teniendo en cuenta la siguiente estructura:
        /// clave:valor
        /// por ejemplo: se desea enviar el código de un suministro y su correo electrónico:
        /// idnroservicio:46118192;correo:alguien@ejemplo.com
        /// En la Clase Utilidades existe el método ObtenerValorDatosVarios() para descubrir cada dato.
        /// </summary>
        public string DatosVarios { get; set; }

        #endregion Propiedades

        /// <summary>
        ///
        /// </summary>
        public ClsParametroImpresionRecibosNotificar()
        {
            base.ClaseComando = "OptimusNG.ReglasNegocio.Procesos.ClsGestionarNotificacionImpresionRecibo";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [XmlInclude(typeof(clsParametroTarea))]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class ClsParametroRegistroTransaccionPagoVisa : clsParametroTarea
    {
        #region Campos

        private int _intIdNumeroOrden;
        private int _intIdUsuario;
        private short _intIdEmpresa;
        private string _strDatosVarios;

        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public int IdNumeroOrden
        {
            get { return _intIdNumeroOrden; }
            set { _intIdNumeroOrden = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int IdUsuario
        {
            get { return _intIdUsuario; }
            set { _intIdUsuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public short IdEmpresa
        {
            get { return _intIdEmpresa; }
            set { _intIdEmpresa = value; }
        }

        /// <summary>
        /// Guarda información teniendo en cuenta la siguiente estructura:
        /// clave:valor
        /// por ejemplo: se desea enviar el código de un suministro y su correo electrónico:
        /// idnroservicio:46118192;correo:alguien@ejemplo.com
        /// En la Clase Utilidades existe el método ObtenerValorDatosVarios() para descubrir cada dato.
        /// </summary>
        public string DatosVarios
        {
            get { return _strDatosVarios; }
            set { _strDatosVarios = value; }
        }

        #endregion Propiedades

        /// <summary>
        ///
        /// </summary>
        public ClsParametroRegistroTransaccionPagoVisa()
        {
            base.ClaseComando = "OptimusNG.ReglasNegocio.Procesos.ClsGestionarRegistroTransaccionPagoVisa";
        }

    }

    //DISTRILUZ-003 Requerimiento 14
    [Serializable()]
    public enum enumOperacionDepositoBancario
    {
        Conciliacion,
        Listar
    }


    #region Parametros para tarea de Deposito Bancario
    [XmlInclude(typeof(clsParametroTarea))]
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsParametroTareaDepositoBancarioClienteEmpresa : clsParametroTarea
    {
        #region Campos
        private int _idnumeroorden;

        private enumOperacionDepositoBancario _enumtipooperacion;

        #endregion

        #region Propiedades
        public int IdNumeroOrden
        {
            get { return _idnumeroorden; }
            set { _idnumeroorden = value; }
        }

        public enumOperacionDepositoBancario EnumTipoOperacion
        {
            get { return _enumtipooperacion; }
            set { _enumtipooperacion = value; }
        }


        #endregion

        #region Constructor
        public clsParametroTareaDepositoBancarioClienteEmpresa()
        {
            base.ClaseComando = "OptimusNG.ReglasNegocio.Procesos.clsGestionarTareaDepositoBancarioClienteEmpresa";
        }
        #endregion
    }
    #endregion




    #endregion

    #region Constantes y Enumeraciones

    public class ClsConstantesImpresionRecibos
    {
        public enum ParametroImpresionReciboEnum
        {
            NumeracionImpresion = 1,
            AplicacionesNotificacionPush = 2,
            ProcesosDisponiblesNotificacion = 3,
            IdTipoNotificacionSMS = 4,
            IdTipoNotificacionPush = 5,
            CantidadFilasPaginacion = 6,
            TiempoConsultaLogTarea = 7,
            RutaBaseArchivosPDFRecibos = 8, //CanviaDev_2020-05-27
            NivelRegistroTarea = 9 //CanviaDev_2020-05-27
        }

        public enum TipoTarea
        {
            NotificacionCorreo = 1,
            NotificacionSMS = 2,
            NotificacionPush = 3,
            ProcesamientoArchivoimpresion = 4
        }


        public const string ConstNomParamNumeracionImpresion = "NumeracionImpresion";
        public const string ConstNomParamAplicacNotifPush = "AplicacionesNotificacionPush";
        public const string ConstNomParamProcsDisponiblesNotif = "ProcesosDisponiblesNotificaciones";
        public const string ConstNomParamCantFilasPaginacion = "CantidadFilasPaginacion";
        public const string ConstNomParamTiempoConsLogTarea = "TiempoConsultaLogTarea";
        public const string ConstNomParamRutaBaseArchPDFRecibos = "RutaBaseArchivosPDFRecibos"; //CanviaDev_2020-05-27
        public const string ConstNomParamNivelRegistroTarea = "NivelRegistroTarea"; //CanviaDev_2020-05-27
    }

    /// <summary>
    /// Enumerador de los distintos procesos del Sistema Optimus NGC 
    /// que son controlados por la Tarea.
    /// </summary>
    public enum ProcesoNGEnum
    {
        /// <summary>
        ///
        /// </summary>
        ServicioDeIntercambio = 1,

        /// <summary>
        ///
        /// </summary>
        GeneracionDePadronDeLectura = 3,

        /// <summary>
        ///
        /// </summary>
        RecibirLecturasDeEnergíaElectrica = 4,

        /// <summary>
        ///
        /// </summary>
        Valorizacion = 5,

        /// <summary>
        ///
        /// </summary>
        EmisiondeRecibosDeEnergia = 6,

        /// <summary>
        ///
        /// </summary>
        EnviodePadronDeLectura = 7,

        /// <summary>
        ///
        /// </summary>
        CompensacionInterrupcionesNTCSEPostpagoMensual = 8,

        /// <summary>
        ///
        /// </summary>
        GenerarOTDerivadaDeInconsistencia = 9,

        /// <summary>
        ///
        /// </summary>
        CargarPliegos = 10,

        /// <summary>
        ///
        /// </summary>
        AplicarValorizado = 12,

        /// <summary>
        ///
        /// </summary>
        GenerarCuotasDeConvenio = 13,

        /// <summary>
        ///
        /// </summary>
        GeneraDATAparacortes = 14,

        /// <summary>
        ///
        /// </summary>
        GeneracionArchivoLecturas = 15,

        /// <summary>
        ///
        /// </summary>
        InterfaseOptimusNGC_Optimus = 16,

        /// <summary>
        /// Importa desde el Optimus los otros cargos a facturar en el OptimusNGC.
        /// </summary>
        ImportarCargosFacturacion = 17,

        /// <summary>
        /// Actualiza el atributo Suministro.Corte = 1 en el Optimus.
        /// </summary>
        InterfaseActualizarSuministroCorte = 18,

        /// <summary>
        /// Actualiza el atributo Suministro.Corte = 0 en el Optimus.
        /// </summary>
        InterfaseActualizarSuministroReconexion = 19,

        /// <summary>
        /// Actualiza varios atributos de la tabla suministro en el Optimus (1 solo registro x vez).
        /// </summary>
        InterfaseActualizacionSuministroIndividual = 20,

        /// <summary>
        /// Actualiza los atributos Suministro.UPFactura y Suministro.Corte en el Optimus (n registros x vez).
        /// </summary>
        InterfaseActualizacionSuministroMasiva = 21,

        /// <summary>
        /// Actualiza un registro de atención por Interrupción para que sea
        /// descargado por el módulo de NTCSE en Optimus.
        /// </summary>
        InterfaseActualizarAtencion = 22,

        /// <summary>
        /// Para el proceso de recibir archivo con cobranza de terceros.
        /// </summary>
        RecibirCobranzaExterna = 23,

        /// <summary>
        /// Proceso de ponderación de pliegos.
        /// </summary>
        PonderacionPliegos = 24,

        /// <summary>
        /// Actualiza en forma MASIVA el atributo Suministro.Corte = 1 en el Optimus.
        /// </summary>
        InterfaseActualizarSuministroCorteMasivo = 25,

        /// <summary>
        /// Actualiza en forma INDIVIDUAL un contrato en Optimus.
        /// </summary>
        InterfaseActualizacionContratoIndividual = 26,

        /// <summary>
        /// Actualiza en forma MASIVA un grupo de contratos en Optimus.
        /// </summary>
        InterfaseActualizacionContratoMasivo = 27,

        /// <summary>
        /// Genera Lote de Valorizacion
        /// </summary>
        GeneraLoteValorizacion = 28,

        /// <summary>
        /// Actualiza la tabla SumModeloConc de Optimus.
        /// </summary>
        InterfaseActualizacionSumModeloConc = 29,

        /// <summary>
        /// Actualiza la tabla Feriado de Optimus.
        /// </summary>
        InterfaseActualizacionFeriados = 30,

        /// <summary>
        /// Para importar los movimientos realizados en almacén desde los archivos generados en SAP
        /// </summary>
        ImportarMovimientosAlmacenSAP = 32,

        /// <summary>
        /// Actualiza los días hábiles, días de apelación y termina atenciones pendientes.
        /// </summary>
        ActualizacionAtenciones = 33,

        /// <summary>
        /// Genera los documentos provisionados
        /// </summary>
        GenerarDocumentosProvisionados = 34,

        /// <summary>
        /// Genera los documentos castigados
        /// </summary>
        GenerarDocumentosCastigados = 35,

        /// <summary>
        /// Registra las interrupciones y su descargo en Optimus
        /// </summary>
        InterfaseRegistroInterrupciones = 36,

        /// <summary>
        /// Proceso que genera los asientos contables.
        /// </summary>
        GenerarAsientosContables = 37,

        /// <summary>
        /// Proceso que importa los suministros (postpago y que tienen facturación mensual) con interrupciones x LCE.
        /// </summary>
        ImportarInteLCESumiPostpagoMensual = 42,

        /// <summary>
        /// Proceso que importa los suministros (postpago y que tienen facturación semestral) con interrupciones x LCE.
        /// </summary>
        ImportarInteLCESumiPostpagoSemestral = 43,

        /// <summary>
        /// Realizar los Previos a la Facturación
        /// </summary>
        RealizarPreviosFacturación = 44,

        /// <summary>
        /// Proceso que importa los suministros (postpago y que tienen facturación semestral) con interrupciones x NTCSE.
        /// </summary>
        ImportarInteNTCSESumiPostpagoMensual = 45,

        /// <summary>
        /// Notificación de cumplimientos
        /// </summary>
        NotificarCumplimientos = 46,

        /// <summary>
        /// Notificación de cumplimientos
        /// </summary>
        RevertirImportacionDataInterrupciones = 47,

        /// <summary>
        /// Importa archivo Excel que contiene facturación de clientes libres.
        /// </summary>
        ImportacionClientesLibres = 48,

        /// <summary>
        /// Importa los suministros dispuestos a ser compensados por Calidad del Producto.
        /// </summary>
        ImportarCompensaProducto = 49,

        GenerarArchivoIntercambio = 50,

        /// <summary>
        /// Reporte de Atenciones Vencidas y por Vencer (envio de reporte al correo)
        /// </summary>
        AtencionReporteVencerVencidos = 51,

        /// <summary>
        /// Aplicacion de Documentos a favor
        /// </summary>
        AplicarDocumentosAFavor = 52,

        /// <summary>
        /// Generar Lista Morosidad
        /// </summary>
        GenerarListaMorosidad = 53,
        /// <summary>
        /// 
        /// </summary>
        GenerarArchivoRecibos = 54,

        /// <summary>
        ///
        /// </summary>
        GenerarOTNotificacionMorosidad = 61,

        /// <summary>
        /// Consulta Rutas para el Padron.
        /// </summary>
        ConsultaRutasPadron = 60,

        /// <summary>
        ///
        /// </summary>
        RecepcionAutomaticaOTMorosidad = 62,

        /// <summary>
        ///
        /// </summary>
        ListadoSuministroTemporal = 63,

        /// <summary>
        ///
        /// </summary>
        ExportarSuministrosValorizados = 64,

        /// <summary>
        ///
        /// </summary>
        ExportacionCobranzaEncargada = 65,

        /// <summary>
        ///
        /// </summary>
        LiquidarConsumosSemestrales = 66,

        /// <summary>
        ///
        /// </summary>
        ExportarVentaExtraordinaria = 67,

        /// <summary>
        ///
        /// </summary>
        AplicarConsumosSemestrales = 68,

        /// <summary>
        ///
        /// </summary>
        AtualizarExcelProvision = 69,

        /// <summary>
        ///
        /// </summary>
        GenerarDataMedicionesNTCSE = 70,

        /// <summary>
        ///
        /// </summary>
        GenerarParqueMedidores = 71,

        /// <summary>
        ///
        /// </summary>
        CargarArchivoMedidores = 72,

        /// <summary>
        ///
        /// </summary>
        CompensacionNTCSE = 73,

        /// <summary>
        ///
        /// </summary>
        EmisionPadronLecturaElementoElectrico = 74,

        /// <summary>
        ///
        /// </summary>
        ImportarPadronLecturaElementoElectrico = 75,

        /// <summary>
        ///
        /// </summary>
        GenerarReportesReintegrosRecuperos = 76,

        /// <summary>
        ///
        /// </summary>
        ProcesarRegistroVentas = 78,

        /// <summary>
        ///
        /// </summary>
        ExportarRegistroVentas = 80,

        /// <summary>
        ///
        /// </summary>
        ExportarConsumosPorElementoElectrico = 82,

        /// <summary>
        ///
        /// </summary>
        GuardarConsumosPorElementoElectrico = 84,

        /// <summary>
        ///
        /// </summary>
        ExportarBalancePorPuntoMedicion = 85,

        /// <summary>
        ///
        /// </summary>
        GuardarBalancePorPuntoMedicion = 86,

        /// <summary>
        ///
        /// </summary>
        ExportarHistoricoConsumos = 87,

        /// <summary>
        ///
        /// </summary>
        ProcesoProyectoSolicitudes = 88,

        /// <summary>
        ///
        /// </summary>
        NotificacionesSolicitudesAtenciones = 89,

        /// <summary>
        ///
        /// </summary>
        VisualizarDataGenerada = 90,

        /// <summary>
        ///
        /// </summary>
        ConfirmarDataGenerada = 91,

        /// <summary>
        ///
        /// </summary>
        ExportarDataOsinergmin = 92,

        NotificacionCajasNoCerradas = 93,

        NotificaciónRemesasTránsito = 94,

        /// <summary>
        ///
        /// </summary>
        ExportarAtencionReclamo = 95,

        /// <summary>
        ///
        /// </summary>
        ExportarAtencionNoReclamo = 96,

        ImportarCobranzaEncargada = 97,

        ExportarLocalidades = 98,

        ObtenerBalanceSED = 100,

        ExportarConsumoAnual = 101,

        ObtenerBalanceMediaAlta = 102,

        ConsolidarDataAnexo01 = 103,

        TablasAnexo01 = 104,

        ExportarBalanceMediaAlta = 105,

        GuardarBalanceMediaAlta = 106,

        ExportarReportesNormaTecnica = 107,

        ExportaSuministrosXInterrupcion = 108,

        ExportarReportesNormaTecnicaCP = 109,

        SimulacionInterrupcionNTCSELey = 111,

        SimulacionInterrupcionNTCSENorma = 116,

        /// <summary>
        ///
        /// </summary>
        GenerarDataMedicionesNTCSER = 117,

        /// <summary>
        ///
        /// </summary>
        ProcesoCompensacionNTCSER = 118,

        /// <summary>
        /// Proceso de generación de data SAIFI SAIDI.
        /// </summary>
        GenerarDataSAIFISAIDI = 120,

        /// <summary>
        /// Proceso registro de compensación
        /// </summary>
        RegistroCompensacionNORMA = 122,

        /// <summary>
        /// Proceso de generación Reportes Contraste NTCSE.
        /// </summary>

        GenerarReportesContrasteNTCSE = 123,

        /// <summary>
        /// Proceso para generar el reporte de historicos de AP.
        /// </summary>
        ExportarBalanceHistoricoAP = 124,

        /// <summary>
        ///
        /// </summary>
        NotifcarSoporteMaximusII = 125,

        /// <summary>
        ///
        /// </summary>
        ActualizarDataAnexo01 = 126,

        /// <summary>
        /// Proceso que permite generar la lista de documentos emitidos.
        /// </summary>
        ReporteVectroDocumentosEmitidos = 127,

        /// <summary>
        /// Proceso que permite generar datos de suministros por lista.
        /// </summary>
        ReporteVectroPorListaSuministros = 128,

        /// <summary>
        /// Proceso que permite exportar las atenciones a excel.
        /// </summary>
        ExportarGestionClientes = 129,

        /// <summary>
        /// Proceso que permite generar el detalle de ventas
        /// </summary>
        ReporteVectroDetalleVentas = 132,

        /// <summary>
        /// Proceso que permite generar el detalle de materiales
        /// </summary>
        ReporteVectroDetalleMateriales = 133,

        /// <summary>
        /// Permite procesar las interrupciones
        /// </summary>
        CompensarInterrupciones = 130,

        /// <summary>
        /// Permite exportar Total Interrupciones
        /// </summary>
        ExportarTotalInterrupciones = 131,

        ProcesarAjusteTarifario = 141,

        GenerarOTInspeccionFISE = 143,

        /// <summary>
        ///
        /// </summary>
        AnularAsientoContable = 147,

        /// <summary>
        ///
        /// </summary>
        ProcesarReporteCobranzaResumen = 148,

        /// <summary>
        ///
        /// </summary>
        ProcesarReporteDocumentoConcepto = 149,

        /// <summary>
        ///
        /// </summary>
        ProcesarDescargaMasivaOTFise = 150,

        /// <summary>
        ///
        /// </summary>
        GenerarSuministrosSeleccionadosFise = 151,

        /// <summary>
        ///
        /// </summary>
        ReporteSuminDeudaCortes = 152,

        //obtenido y mod 19/11/2012
        ProcesarImportacionAportesReembosables = 153,

        /// <summary>
        /// Correspondiente al proceso de tarea de compensación Rural
        /// </summary>
        SimulacionInterrupcionNTCSER = 154,

        /// <summary>
        /// Proceso para la generación de los indicadores NIC-DIC.
        /// </summary>
        GeneracionIndicadoresNICDIC = 155,

        /// <summary>
        /// Proceso para reporte de indicadores NIC-DIC.
        /// </summary>
        GeneracionRepIndicadoresNICDIC = 156,

        /// <summary>
        ///
        /// </summary>
        GenerarOTInspeccionMorosidad = 157,

        /// <summary>
        /// Proceso para la compensación por rechazo de carga.
        /// </summary>
        CompensacionRechazoCarga = 158,

        /// <summary>
        ///
        /// </summary>
        DerivacionMasivaRecuperos = 159,

        /// <summary>
        /// 
        /// </summary>
        ReportesFISE = 161,

        /// <summary>
        /// Permite gestionar diversos reportes del modulo de calidad de suministro.
        /// </summary>
        ReporteVariosCalidadSuministro = 162,

        /// <summary>
        /// 
        /// </summary>
        ImprimirValeFISE = 163,

        GenerarValeFISE = 171,

        //Para procesar reportes de seguridad , como roles usuarios y opciones de menu
        ReportesSeguridad = 169,

        //Para procesar reportes detallado de Totales OT fact 
        ReportesDetalladoTotalesOTFact = 170,

        //Para generar las O.T. de A.P. Automatizado.
        AutomatizarOTAlumbradoPublico = 168,

        //Para procesar reportes Atenciones a Clientes
        ReportesGestionAtencionClientes = 172,

        EjecutarExportacionRegistroVentas = 177,

        ReporteTransaccionesCobranza = 178,

        /// <summary>
        /// Proceso que permite Simular o Compensar Interupciones por LEY
        /// </summary>
        CompensarInterrupcionesLCEMCS = 179,

        /// <summary>
        /// Proceso que permite Simular o Compensar Interupciones por LEY
        /// </summary>
        RevertirCompInterrupcionesLCEMCS = 180,

        /// <summary>
        /// Permite generar los reportes contables.
        /// </summary>
        ReportesContable = 181,

        /// <summary>
        /// Permite generar los reportes por Solicitudes de Servicio.
        /// </summary>
        ReportesSolicitudesServicio = 182,

        //Para procesar reportes de sectores
        ReportesGestionConfiguracionFacturacion = 183,

        /// <summary>
        /// Proceso que permite realizar la simulación de recompensación NTCSE.
        /// </summary>
        SimulacionRecompensacionNTCSENorma = 184,

        /// <summary>
        /// Proceso que permite importar el archivo para descargo masivo de actas.
        /// </summary>
        ImportarArchivoDescargoMasivoActas = 186,

        /// <summary>
        /// Proceso que registra en la BD el descargo masivo de las actas.
        /// </summary>
        GuardarDatosDescargoMasivoActas = 187,

        /// <summary>
        /// Proceso que permite automatizar la generación de O.T. de Lecturas.
        /// </summary>
        AutomatizacionOTLecturas = 188,

        /// <summary>
        /// Proceso de Medidores por obsolescencia de vida util
        /// </summary>
        ProcesarMedidoresObsolescencia = 191,

        /// <summary>
        /// Proceso que obtiene los medidores desaprobados por semana.
        /// </summary>
        ProcesarReporteMedidoresDesaprobadosporsemana = 192,

        //ProcesarSubidaBeneficiarioSinLuzFise = 194,
        ProcesarSubidaFise = 194,

        /// <summary>
        /// Proceso que permite generar los reportes de Tecnicom - Laboratorio.
        /// </summary>
        GenerarReportesTecnicomLaboratorio = 195,

        //Proceso para Importar, Guardar y Reportar
        ImportarReportarPliegoCosto = 199,

        AperturarCerrarPeriodoInterrupcion = 200,

        ConsolidaInterrupcionSuministro = 201,

        EnviarSMS = 202,

        GenerarDataPagoMorosidad = 213,

        ExportarAjusteTarifario = 214,

        //Para procesar Documentos Al credito
        ListadoDocumentoAlCredito = 251,

        //Para las OTs fact en Campo
        ConsultaRutasPadronFactCampo = 252,

        ExportarSuministrosAfectadosxCantSumi = 253,

        TrasladarDataContrasteRR = 254,

        CopiarItemInterrupcion = 262,

        ExportarRecompensacionNTCSE = 267,

        RevertirRecompensacionNTCSE = 268,

        ImportarArchivoBaseSemSuminsNoValidados = 269,

        RegistrarArchivoBaseSemSuminsNoValidados = 270,

        TransferenciaComprobanteSUNAT = 276,

        RegistrarCargaSuministrosConvenioMunicipalidades = 278,

        NotificarRecibos = 279,

        ImportarArchivoCostoActividadesComerciales = 319,

        TransferenciaAsientoContableSAP = 327,

        ImportarArchivoProgramacionSemestralContraste = 337,

        NotificarEmisionRecibosClientes = 342,

        GenerarArchivoImpresionRecibos = 350,

        GenerarArchivosPDFRecibosSolicitudImpresion = 352 //CanviaDev_2020-05-27
    }

    /// <summary>
    /// Enumerador para indicar el tipo de impresion.
    /// </summary>
    public enum TipoImpresionEnum
    {
        ImpresionRuta = 1,
        ImpresionTodosMayores = 2,
        ImpresionArchivo = 3,
        ImpresionRangoSuministros = 4,
        ImpresionGenerarArchivo = 5,
        ImpresionNotificar = 6,
        ImpresionRutaJSON = 8 //Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
    }

    public enum TipoNotificacionEnum
    {
        NotificacionCorreo = 1,
        NotificacionSMS = 2,
        NotificacionPush = 3,
        GeneracionArchivosPDF = 5 //CanviaDev_2020-05-27
    }


    public enum ClienteEmpresa
    {
        TransaccionPagoMasivoVisa = 343,
        TransaccionDepositoBancario = 345,

    }


    #endregion

    #endregion
}
