using SISAC.Entidad.Maestro;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml.Serialization;

namespace SISAC.Entidad.Facturacion
{
    #region EsquemaIntercambioPerfil

    /// <summary>
    ///
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsEsquemaIntercambioPerfilMaestro
    {
        #region Campos/Propiedades

        private Int16 _intid;
        private String _strnombre;
        private DateTime _datfechacreacion;
        private DateTime? _datfechavigencia;
        private DateTime? _datfechacaduca;
        private Int16 _intidEstadoNGC;

        /// <summary>
        ///
        /// </summary>
        public Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaCreacion
        {
            get { return _datfechacreacion; }
            set { _datfechacreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaVigencia
        {
            get { return _datfechavigencia; }
            set { _datfechavigencia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaCaduca
        {
            get { return _datfechacaduca; }
            set { _datfechacaduca = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstadoNGC
        {
            get { return _intidEstadoNGC; }
            set { _intidEstadoNGC = value; }
        }

        #endregion Campos/Propiedades

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        public clsEsquemaIntercambioPerfilMaestro()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dr"></param>
        public clsEsquemaIntercambioPerfilMaestro(DataRow dr)
        {
            if (dr == null) { return; }

            this.Id = Convert.ToInt16(dr["id"]);
            this.Nombre = dr["nombre"].ToString();
            this.FechaCreacion = (DateTime)dr["fechacreacion"];
            this.FechaVigencia = (DateTime?)dr["fechavigencia"];
            this.FechaCaduca = Convert.IsDBNull(dr["fechacaduca"]) ? (DateTime?)null : (DateTime?)dr["fechacaduca"];
            this.IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
        }

        #endregion Constructor
    }

    /// <summary>
    ///
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsEsquemaIntercambioPerfil : clsEsquemaIntercambioPerfilMaestro
    {
        #region Campos/Propiedades

        private String _strdescripcion = "";
        private Int16 _intorigenintercambio = 0;
        private Int16 _inttipoflujodato = 10;
        private Int32? _intidesquemaintercambioformatoenvio = (Int16?)null;
        private Int32? _intidesquemaintercambioformatodevolucion = (Int16?)null;
        private Int16? _intformatoarchivo = (Int16?)null;

        private String _strnombrearchivoprefijoenvio = "";
        private Int16? _intnombrearchivomascaraenvio = (Int16?)null;
        private String _strnombrearchivosufijoenvio = "";
        private String _strextensionarchivoenvio = "";
        private String _strrutaenvio = "";
        private String _strnombrearchivoprefijodevolucion = "";
        private Int16? _intnombrearchivomascaradevolucion = (Int16?)null;
        private String _strnombrearchivosufijodevolucion = "";
        private String _strextensionarchivodevolucion = "";
        private String _strrutadevolucion = "";

        private clsEsquemaIntercambioFormato oFormatoEnvio;
        private clsEsquemaIntercambioFormato oFormatoDevolucion;
        private clsListaEntidadMaestra oListaColumnas;

        /// <summary>
        /// Entidad del Formato de Env�o.
        /// </summary>
        public clsEsquemaIntercambioFormato FormatoEnvio
        {
            get { return oFormatoEnvio; }
            set { oFormatoEnvio = value; }
        }

        /// <summary>
        /// Entidad del Formato de Devoluci�n.
        /// </summary>
        public clsEsquemaIntercambioFormato FormatoDevolucion
        {
            get { return oFormatoDevolucion; }
            set { oFormatoDevolucion = value; }
        }

        /// <summary>
        /// Entidad que contiene la lista de las Columnas posibles a procesar.
        /// </summary>
        public clsListaEntidadMaestra ListaColumnas
        {
            get { return oListaColumnas; }
            set { oListaColumnas = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 OrigenIntercambio
        {
            get { return _intorigenintercambio; }
            set { _intorigenintercambio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 TipoFlujoDato
        {
            get { return _inttipoflujodato; }
            set { _inttipoflujodato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? IdEsquemaIntercambioFormatoEnvio
        {
            get { return _intidesquemaintercambioformatoenvio; }
            set { _intidesquemaintercambioformatoenvio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? IdEsquemaIntercambioFormatoDevolucion
        {
            get { return _intidesquemaintercambioformatodevolucion; }
            set { _intidesquemaintercambioformatodevolucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? FormatoArchivo
        {
            get { return _intformatoarchivo; }
            set { _intformatoarchivo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreArchivoPrefijoEnvio
        {
            get { return _strnombrearchivoprefijoenvio; }
            set { _strnombrearchivoprefijoenvio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? NombreArchivoMascaraEnvio
        {
            get { return _intnombrearchivomascaraenvio; }
            set { _intnombrearchivomascaraenvio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreArchivoSufijoEnvio
        {
            get { return _strnombrearchivosufijoenvio; }
            set { _strnombrearchivosufijoenvio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ExtensionArchivoEnvio
        {
            get { return _strextensionarchivoenvio; }
            set { _strextensionarchivoenvio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String RutaEnvio
        {
            get { return _strrutaenvio; }
            set { _strrutaenvio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreArchivoPrefijoDevolucion
        {
            get { return _strnombrearchivoprefijodevolucion; }
            set { _strnombrearchivoprefijodevolucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? NombreArchivoMascaraDevolucion
        {
            get { return _intnombrearchivomascaradevolucion; }
            set { _intnombrearchivomascaradevolucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreArchivoSufijoDevolucion
        {
            get { return _strnombrearchivosufijodevolucion; }
            set { _strnombrearchivosufijodevolucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ExtensionArchivoDevolucion
        {
            get { return _strextensionarchivodevolucion; }
            set { _strextensionarchivodevolucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String RutaDevolucion
        {
            get { return _strrutadevolucion; }
            set { _strrutadevolucion = value; }
        }

        #endregion Campos/Propiedades

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        public clsEsquemaIntercambioPerfil()
        {
            Id = 0;
            Nombre = "";
            FechaCreacion = DateTime.Now;
            FechaVigencia = (DateTime)FechaCreacion;
            FechaCaduca = (DateTime?)null;
            IdEstadoNGC = 0;

            _strdescripcion = "";
            _intorigenintercambio = 0;
            _inttipoflujodato = 10;
            _intidesquemaintercambioformatoenvio = (Int16?)null;
            _intidesquemaintercambioformatodevolucion = (Int16?)null;
            _intformatoarchivo = (Int16?)null;

            _strnombrearchivoprefijoenvio = "";
            _intnombrearchivomascaraenvio = (Int16?)null;
            _strnombrearchivosufijoenvio = "";
            _strextensionarchivoenvio = "";
            _strrutaenvio = "";
            _strnombrearchivoprefijodevolucion = "";
            _intnombrearchivomascaradevolucion = (Int16?)null;
            _strnombrearchivosufijodevolucion = "";
            _strextensionarchivodevolucion = "";
            _strrutadevolucion = "";
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dr"></param>
        public clsEsquemaIntercambioPerfil(DataRow dr)
        {
            if (dr == null) { return; }

            this.Id = Convert.ToInt16(dr["id"]);
            this.Nombre = dr["nombre"].ToString();
            this.Descripcion = dr["descripcion"].ToString();
            this.OrigenIntercambio = Convert.ToInt16(dr["origenintercambio"]);
            this.TipoFlujoDato = Convert.ToInt16(dr["tipoflujodato"]);
            this.IdEsquemaIntercambioFormatoEnvio = Convert.IsDBNull(dr["idesquemaintercambioformatoenvio"]) ? (Int32?)null : (Int32?)dr["idesquemaintercambioformatoenvio"];
            this.IdEsquemaIntercambioFormatoDevolucion = Convert.IsDBNull(dr["idesquemaintercambioformatodevolucion"]) ? (Int32?)null : (Int32?)dr["idesquemaintercambioformatodevolucion"];
            this.FormatoArchivo = (Int16?)dr["formatoarchivo"];
            this.FechaCreacion = (DateTime)dr["fechacreacion"];
            this.FechaVigencia = (DateTime?)dr["fechavigencia"];
            this.FechaCaduca = Convert.IsDBNull(dr["fechacaduca"]) ? (DateTime?)null : (DateTime?)dr["fechacaduca"];
            this.IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);

            NombreArchivoPrefijoEnvio = dr["nombrearchivoprefijoenvio"].ToString();
            NombreArchivoMascaraEnvio = Convert.IsDBNull(dr["nombrearchivomascaraenvio"]) ? (Int16?)null : Convert.ToInt16(dr["nombrearchivomascaraenvio"]);
            NombreArchivoSufijoEnvio = dr["nombrearchivosufijoenvio"].ToString();
            ExtensionArchivoEnvio = dr["extensionarchivoenvio"].ToString();
            RutaEnvio = dr["rutaenvio"].ToString();
            NombreArchivoPrefijoDevolucion = dr["nombrearchivoprefijodevolucion"].ToString();
            NombreArchivoMascaraDevolucion = Convert.IsDBNull(dr["nombrearchivomascaradevolucion"]) ? (Int16?)null : Convert.ToInt16(dr["nombrearchivomascaradevolucion"]);
            NombreArchivoSufijoDevolucion = dr["nombrearchivosufijodevolucion"].ToString();
            ExtensionArchivoDevolucion = dr["extensionarchivodevolucion"].ToString();
            RutaDevolucion = dr["rutadevolucion"].ToString();
        }

        #endregion Constructor
    }

    /// <summary>
    ///
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaEsquemaIntercambioPerfilMaestro
    {
        #region Campo/Propiedades

        System.ComponentModel.BindingList<clsEsquemaIntercambioPerfilMaestro> _lstelemento = new System.ComponentModel.BindingList<clsEsquemaIntercambioPerfilMaestro>();

        public System.ComponentModel.BindingList<clsEsquemaIntercambioPerfilMaestro> Elemento
        {
            get { return _lstelemento; }
            set { _lstelemento = value; }
        }

        #endregion Campo/Propiedades

        #region Constructor

        public clsListaEsquemaIntercambioPerfilMaestro()
        {
        }

        public clsListaEsquemaIntercambioPerfilMaestro(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0) { return; }

            foreach (DataRow dr in dt.Rows)
            {
                Elemento.Add(new clsEsquemaIntercambioPerfilMaestro(dr));
            }
        }

        #endregion Constructor
    }

    #endregion EsquemaIntercambioPerfil

    #region EsquemaIntercambioFormato/EsquemaIntercambioEstructura

    /// <summary>
    ///
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsEsquemaIntercambioFormato
    {
        #region Campos/Propiedades

        private Int32 _intid;
        private String _strnombre;
        private Int16 _intlongitudregistro;
        private DateTime _datfechacreacion;
        private DateTime? _datfechavigencia;
        private DateTime? _datfechacaduca;
        private Int16 _intposicioniniciodinamica;
        private Int16 _intidEstadoNGC;
        private Int32 _intnumerolineainiciodetalle;
        //add
        private Int32 _intidtiposeparadorcolumna;
        //add end
        private clsListaEsquemaIntercambioEstructura oEstructuraCabecera1;
        private clsListaEsquemaIntercambioEstructura oEstructuraCabecera2;
        private clsListaEsquemaIntercambioEstructura oEstructuraDetalle;
        private clsListaEsquemaIntercambioEstructura oEstructuraPie;
        private clsListaEsquemaIntercambioConversion oListaConversion;
        private clsListaEntidadMaestra oListaColumnas;
        //add
        /// <summary>
        /// Id del separador de columna
        /// </summary>
        public Int32 IdTipoSeparadorColumna
        {
            get { return _intidtiposeparadorcolumna; }
            set { _intidtiposeparadorcolumna = value; }
        }
        //add end      
        /// <summary>
        /// Estructura de la primera cabecera del formato.
        /// </summary>
        public clsListaEsquemaIntercambioEstructura EstructuraCabecera1
        {
            get { return oEstructuraCabecera1; }
            set { oEstructuraCabecera1 = value; }
        }

        /// <summary>
        /// Estructura de la segunda cabecera del formato.
        /// </summary>
        public clsListaEsquemaIntercambioEstructura EstructuraCabecera2
        {
            get { return oEstructuraCabecera2; }
            set { oEstructuraCabecera2 = value; }
        }

        /// <summary>
        /// Estructura del detalle del formato.
        /// </summary>
        public clsListaEsquemaIntercambioEstructura EstructuraDetalle
        {
            get { return oEstructuraDetalle; }
            set { oEstructuraDetalle = value; }
        }

        /// <summary>
        /// Estructura del pie del formato.
        /// </summary>
        public clsListaEsquemaIntercambioEstructura EstructuraPie
        {
            get { return oEstructuraPie; }
            set { oEstructuraPie = value; }
        }

        /// <summary>
        /// Lista que contiene los valores equivalentes a convertir.
        /// </summary>
        public clsListaEsquemaIntercambioConversion ListaConversion
        {
            get { return oListaConversion; }
            set { oListaConversion = value; }
        }

        /// <summary>
        /// Lista completa de las columnas definidas para el intercambio de archivos.
        /// </summary>
        public clsListaEntidadMaestra ListaColumnas
        {
            get { return oListaColumnas; }
            set { oListaColumnas = value; }
        }

        /// <summary>
        /// N�mero de linea a partir del cual empieza el detalle del formato.
        /// </summary>
        public Int32 NumeroLineaInicioDetalle
        {
            get { return _intnumerolineainiciodetalle; }
            set { _intnumerolineainiciodetalle = value; }
        }

        public Int32 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        public DateTime FechaCreacion
        {
            get { return _datfechacreacion; }
            set { _datfechacreacion = value; }
        }

        public DateTime? FechaVigencia
        {
            get { return _datfechavigencia; }
            set { _datfechavigencia = value; }
        }

        public DateTime? FechaCaduca
        {
            get { return _datfechacaduca; }
            set { _datfechacaduca = value; }
        }

        public Int16 IdEstadoNGC
        {
            get { return _intidEstadoNGC; }
            set { _intidEstadoNGC = value; }
        }

        public Int16 LongitudRegistro
        {
            get { return _intlongitudregistro; }
            set { _intlongitudregistro = value; }
        }

        public Int16 PosicionInicioDinamica
        {
            get { return _intposicioniniciodinamica; }
            set { _intposicioniniciodinamica = value; }
        }

        #endregion Campos/Propiedades

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        public clsEsquemaIntercambioFormato()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dr"></param>
        public clsEsquemaIntercambioFormato(DataRow dr)
        {
            if (dr == null) { return; }

            this.Id = Convert.ToInt32(dr["id"]);
            this.Nombre = dr["nombre"].ToString();
            this.LongitudRegistro = Convert.ToInt16(dr["longitudregistro"]);
            this.FechaCreacion = Convert.ToDateTime(dr["fechacreacion"]);
            this.FechaVigencia = Convert.IsDBNull(dr["fechavigencia"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechavigencia"]);
            this.FechaCaduca = Convert.IsDBNull(dr["fechacaduca"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechacaduca"]);
            this.PosicionInicioDinamica = Convert.ToInt16(dr["posicioniniciodinamica"]);
            this.IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
            //ADD          
            //this.IdTipoSeparadorColumna = Convert.IsDBNull(dr["IdTipoSerparadorColumna"]) ? 0 : Convert.ToInt32(dr["IdTipoSerparadorColumna"]);
            if (dr.Table.Columns.Contains("IdTipoSerparadorColumna")) this.IdTipoSeparadorColumna = dr.IsNull("IdTipoSerparadorColumna") ? 0 : Convert.ToInt32(dr["IdTipoSerparadorColumna"]);
            //ADD END
        }

        public clsEsquemaIntercambioFormato
            (DataRow dr
            , DataTable dtcabecera1
            , DataTable dtcabecera2
            , DataTable dtdetalle
            , DataTable dtpie
            , DataTable dtcolumnas
            , DataTable dtconversion)
        {
            if (dr == null) { return; }

            this.Id = Convert.ToInt32(dr["id"]);
            this.Nombre = dr["nombre"].ToString();
            this.LongitudRegistro = Convert.ToInt16(dr["longitudregistro"]);
            this.FechaCreacion = Convert.ToDateTime(dr["fechacreacion"]);
            this.FechaVigencia = Convert.IsDBNull(dr["fechavigencia"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechavigencia"]);
            this.FechaCaduca = Convert.IsDBNull(dr["fechacaduca"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechacaduca"]);
            this.PosicionInicioDinamica = Convert.ToInt16(dr["posicioniniciodinamica"]);
            this.IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
            //ADD          
            //this.IdTipoSeparadorColumna = Convert.IsDBNull(dr["IdTipoSerparadorColumna"]) ? 0 : Convert.ToInt32(dr["IdTipoSerparadorColumna"]);
            if (dr.Table.Columns.Contains("IdTipoSerparadorColumna")) this.IdTipoSeparadorColumna = dr.IsNull("IdTipoSerparadorColumna") ? 0 : Convert.ToInt32(dr["IdTipoSerparadorColumna"]);
            //ADD END

            this.oEstructuraCabecera1 = new clsListaEsquemaIntercambioEstructura(dtcabecera1);
            this.oEstructuraCabecera2 = new clsListaEsquemaIntercambioEstructura(dtcabecera2);
            this.oEstructuraDetalle = new clsListaEsquemaIntercambioEstructura(dtdetalle);
            this.oEstructuraPie = new clsListaEsquemaIntercambioEstructura(dtpie);
            this.oListaColumnas = new clsListaEntidadMaestra(dtcolumnas);
            this.oListaConversion = new clsListaEsquemaIntercambioConversion(dtconversion);

            // Indica a partir de que linea del archivo empieza los registros
            // que contienen el detalle de las transacciones.

            this._intnumerolineainiciodetalle = this.EstructuraCabecera2.Elementos.Count > 0 ? 3
                : this.EstructuraCabecera1.Elementos.Count > 0 ? 2
                : 1;
        }

        #endregion Constructor
    }

    /// <summary>
    ///
    /// </summary>
    ///
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsEsquemaIntercambioEstructura
    {
        #region Campos/Propiedades

        private Int32 _intid;
        private Int32 _intidformato;
        private Int16 _inttiporegistro;
        private Int16 _intidcolumna;
        private String _strnombrecolumna;
        private Int16 _strcolumnatipo;
        private Int16 _strcolumnaformato;
        private Int16 _intcolumnaposicioninicio;
        private Int16 _intcolumnalongitud;
        private Int16 _intcolumnaprecision;
        private Int16 _intesvalorconstante;
        private String _strvalorconstante;
        private Int16 _intseparadordecimales;
        private Int16 _intorden;
        private String _chrcompletarcaracter;
        private Int16 _intcompletaralineacion;
        private Int16 _intidEstadoNGC;
        private Int16 _inttienevalorequivalente;

        #endregion Campos/Propiedades

        #region Propiedades

        public Int32 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        public Int32 IdFormato
        {
            get { return _intidformato; }
            set { _intidformato = value; }
        }

        public Int16 TipoRegistro
        {
            get { return _inttiporegistro; }
            set { _inttiporegistro = value; }
        }

        public Int16 IdColumna
        {
            get { return _intidcolumna; }
            set { _intidcolumna = value; }
        }

        public String NombreColumna
        {
            get { return _strnombrecolumna; }
            set { _strnombrecolumna = value; }
        }

        public Int16 ColumnaTipo
        {
            get { return _strcolumnatipo; }
            set { _strcolumnatipo = value; }
        }

        public Int16 ColumnaFormato
        {
            get { return _strcolumnaformato; }
            set { _strcolumnaformato = value; }
        }

        public Int16 ColumnaPosicionInicio
        {
            get { return _intcolumnaposicioninicio; }
            set { _intcolumnaposicioninicio = value; }
        }

        public Int16 ColumnaLongitud
        {
            get { return _intcolumnalongitud; }
            set { _intcolumnalongitud = value; }
        }

        public Int16 ColumnaPrecision
        {
            get { return _intcolumnaprecision; }
            set { _intcolumnaprecision = value; }
        }

        public Int16 EsValorConstante
        {
            get { return _intesvalorconstante; }
            set { _intesvalorconstante = value; }
        }

        public String ValorConstante
        {
            get { return _strvalorconstante; }
            set { _strvalorconstante = value; }
        }

        public Int16 SeparadorDecimales
        {
            get { return _intseparadordecimales; }
            set { _intseparadordecimales = value; }
        }

        public Int16 Orden
        {
            get { return _intorden; }
            set { _intorden = value; }
        }

        public String CompletarCaracter
        {
            get { return _chrcompletarcaracter; }
            set { _chrcompletarcaracter = value; }
        }

        public Int16 CompletarAlineacion
        {
            get { return _intcompletaralineacion; }
            set { _intcompletaralineacion = value; }
        }

        public Int16 IdEstadoNGC
        {
            get { return _intidEstadoNGC; }
            set { _intidEstadoNGC = value; }
        }

        public Int16 TieneValorEquivalente
        {
            get { return _inttienevalorequivalente; }
            set { _inttienevalorequivalente = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        public clsEsquemaIntercambioEstructura()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dr"></param>
        public clsEsquemaIntercambioEstructura(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt32(dr["id"]);
            IdFormato = Convert.ToInt32(dr["idformato"]);
            TipoRegistro = Convert.ToInt16(dr["tiporegistro"]);
            IdColumna = Convert.ToInt16(dr["idcolumna"]);
            NombreColumna = dr["nombrecolumna"].ToString().Trim();
            ColumnaTipo = Convert.ToInt16(dr["columnatipo"]);
            ColumnaFormato = Convert.ToInt16(dr["columnaformato"]);
            ColumnaPosicionInicio = Convert.ToInt16(dr["columnaposicioninicio"]);
            ColumnaLongitud = Convert.ToInt16(dr["columnalongitud"]);
            ColumnaPrecision = Convert.ToInt16(dr["columnaprecision"]);
            EsValorConstante = Convert.ToInt16(dr["esvalorconstante"]);
            ValorConstante = dr["valorconstante"].ToString().Trim();
            SeparadorDecimales = Convert.ToInt16(dr["separadordecimales"]);
            Orden = Convert.ToInt16(dr["orden"]);
            CompletarCaracter = dr["completarcaracter"].ToString();
            CompletarAlineacion = Convert.ToInt16(dr["completaralineacion"]);
            IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
            TieneValorEquivalente = Convert.ToInt16(dr["tienevalorequivalente"]);
        }

        #endregion Constructor
    }

    /// <summary>
    ///
    /// </summary>
    ///
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaEsquemaIntercambioEstructura
    {
        private System.ComponentModel.BindingList<clsEsquemaIntercambioEstructura> _lstestructura = new System.ComponentModel.BindingList<clsEsquemaIntercambioEstructura>();

        public System.ComponentModel.BindingList<clsEsquemaIntercambioEstructura> Elementos
        {
            get { return _lstestructura; }
            set { _lstestructura = value; }
        }

        public clsListaEsquemaIntercambioEstructura()
        {
        }

        public clsListaEsquemaIntercambioEstructura(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0) { return; }

            foreach (DataRow dr in dt.Rows)
            {
                _lstestructura.Add(new clsEsquemaIntercambioEstructura(dr));
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaEsquemaIntercambioFormato
    {
        private System.ComponentModel.BindingList<clsEsquemaIntercambioFormato> _lstmaestro = new System.ComponentModel.BindingList<clsEsquemaIntercambioFormato>();

        /// <summary>
        /// Los elementos completo de la estructura del formato.
        /// </summary>
        public System.ComponentModel.BindingList<clsEsquemaIntercambioFormato> Elementos
        {
            get { return _lstmaestro; }
            set { _lstmaestro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public clsListaEsquemaIntercambioFormato()
        {
        }

        /// <summary>
        /// Constructor para establecer el formato completo.
        /// </summary>
        /// <param name="dt"></param>
        public clsListaEsquemaIntercambioFormato(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0) { return; }

            foreach (DataRow dr in dt.Rows)
            {
                _lstmaestro.Add(new clsEsquemaIntercambioFormato(dr));
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    ///
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaEsquemaIntercambioFormatoEstructura
    {
        private clsEsquemaIntercambioFormato _objformato;
        private clsListaEsquemaIntercambioEstructura _objestructura;

        /// <summary>
        ///
        /// </summary>
        public clsEsquemaIntercambioFormato Formato
        {
            get { return _objformato; }
            set { _objformato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public clsListaEsquemaIntercambioEstructura Estructura
        {
            get { return _objestructura; }
            set { _objestructura = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public clsListaEsquemaIntercambioFormatoEstructura()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="drformato"></param>
        /// <param name="dtestructura"></param>
        public clsListaEsquemaIntercambioFormatoEstructura(DataRow drformato, DataTable dtestructura)
        {
            _objformato = new clsEsquemaIntercambioFormato(drformato);
            _objestructura = new clsListaEsquemaIntercambioEstructura(dtestructura);
        }
    }

    #endregion EsquemaIntercambioFormato/EsquemaIntercambioEstructura

    #region EsquemaIntercambioConversion

    /// <summary>
    ///
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsEsquemaIntercambioConversion
    {
        #region Campos

        private Int16 _intid;
        private Int32 _intidestructura;
        private String _stridvalororigen;
        private String _stridvalordestino;
        private Int16 _intidEstadoNGC;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdEstructura
        {
            get { return _intidestructura; }
            set { _intidestructura = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String idValorOrigen
        {
            get { return _stridvalororigen; }
            set { _stridvalororigen = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String idValorDestino
        {
            get { return _stridvalordestino; }
            set { _stridvalordestino = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstadoNGC
        {
            get { return _intidEstadoNGC; }
            set { _intidEstadoNGC = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsEsquemaIntercambioConversion()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsEsquemaIntercambioConversion(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt16(dr["id"]);
            IdEstructura = Convert.ToInt32(dr["idestructura"]);
            idValorOrigen = dr["idvalororigen"].ToString();
            idValorDestino = dr["idvalordestino"].ToString();
            IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista EsquemaIntercambioConversion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaEsquemaIntercambioConversion
    {
        private System.ComponentModel.BindingList<clsEsquemaIntercambioConversion> _objelementos = new System.ComponentModel.BindingList<clsEsquemaIntercambioConversion>();

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public System.ComponentModel.BindingList<clsEsquemaIntercambioConversion> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaEsquemaIntercambioConversion()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaEsquemaIntercambioConversion(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsEsquemaIntercambioConversion(_drw));
            }
        }
    }

    #endregion EsquemaIntercambioConversion

    #region EsquemaIntercambioColumnas

    /// <summary>
    /// Entidad EsquemaIntercambioColumnas
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsEsquemaIntercambioColumnas
    {
        #region Campos

        private Int16 _intid;
        private String _strdescripcion;
        private String _strnombre;
        private String _strtiporegistro;
        private Int16 _intidEstadoNGC;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String TipoRegistro
        {
            get { return _strtiporegistro; }
            set { _strtiporegistro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstadoNGC
        {
            get { return _intidEstadoNGC; }
            set { _intidEstadoNGC = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsEsquemaIntercambioColumnas()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsEsquemaIntercambioColumnas(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt16(dr["id"]);
            Descripcion = dr["descripcion"].ToString();
            Nombre = dr["nombre"].ToString();
            TipoRegistro = dr["tiporegistro"].ToString();
            IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista EsquemaIntercambioColumnas
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaEsquemaIntercambioColumnas
    {
        #region Campos

        private System.ComponentModel.BindingList<clsEsquemaIntercambioColumnas> _objelementos = new System.ComponentModel.BindingList<clsEsquemaIntercambioColumnas>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public System.ComponentModel.BindingList<clsEsquemaIntercambioColumnas> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaEsquemaIntercambioColumnas()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaEsquemaIntercambioColumnas(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsEsquemaIntercambioColumnas(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion EsquemaIntercambioColumnas

    #region ServicioConfiguracion

    /*
    /// <summary>
    /// Entidad ServicioConfiguracion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsServicioConfiguracion
    {
        #region Campos

        private Int16 _intid;
        private Int16 _intidempresa;
        private Int16 _intidservicio;
        private Int32 _intidnroservicio;
        private Int16 _intidesquemaintercambioperfil;
        private Int16 _intidtipodocumento;
        private Int16 _intcobrarvencidos;
        private String _strrutacargafile;
        private String _strftpcargaserver;
        private String _strftpcargausuario;
        private String _strftpcargaclave;
        private String _strftpcargafolder;
        private String _strrutadescargafile;
        private String _strftpdescargaserver;
        private String _strftpdescargausuario;
        private String _strftpdescargoclave;
        private String _strftpdescargafolder;
        private Int16 _intidmoneda;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEsquemaIntercambioPerfil
        {
            get { return _intidesquemaintercambioperfil; }
            set { _intidesquemaintercambioperfil = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 CobrarVencidos
        {
            get { return _intcobrarvencidos; }
            set { _intcobrarvencidos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String RutaCargaFile
        {
            get { return _strrutacargafile; }
            set { _strrutacargafile = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaServer
        {
            get { return _strftpcargaserver; }
            set { _strftpcargaserver = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaUsuario
        {
            get { return _strftpcargausuario; }
            set { _strftpcargausuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaClave
        {
            get { return _strftpcargaclave; }
            set { _strftpcargaclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaFolder
        {
            get { return _strftpcargafolder; }
            set { _strftpcargafolder = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String RutaDescargaFile
        {
            get { return _strrutadescargafile; }
            set { _strrutadescargafile = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargaServer
        {
            get { return _strftpdescargaserver; }
            set { _strftpdescargaserver = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargaUsuario
        {
            get { return _strftpdescargausuario; }
            set { _strftpdescargausuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargoClave
        {
            get { return _strftpdescargoclave; }
            set { _strftpdescargoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargaFolder
        {
            get { return _strftpdescargafolder; }
            set { _strftpdescargafolder = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsServicioConfiguracion()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsServicioConfiguracion(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt16(dr["id"]);
            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdServicio = Convert.ToInt16(dr["idservicio"]);
            IdNroServicio = Convert.ToInt32(dr["idnroservicio"]);
            IdEsquemaIntercambioPerfil = Convert.ToInt16(dr["idesquemaintercambioperfil"]);
            IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            CobrarVencidos = Convert.ToInt16(dr["cobrarvencidos"]);
            RutaCargaFile = dr["rutacargafile"].ToString();
            FTPCargaServer = dr["ftpcargaserver"].ToString();
            FTPCargaUsuario = dr["ftpcargausuario"].ToString();
            FTPCargaClave = dr["ftpcargaclave"].ToString();
            FTPCargaFolder = dr["ftpcargafolder"].ToString();
            RutaDescargaFile = dr["rutadescargafile"].ToString();
            FTPDescargaServer = dr["ftpdescargaserver"].ToString();
            FTPDescargaUsuario = dr["ftpdescargausuario"].ToString();
            FTPDescargoClave = dr["ftpdescargoclave"].ToString();
            FTPDescargaFolder = dr["ftpdescargafolder"].ToString();
            IdMoneda = Convert.ToInt16(dr["idmoneda"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista ServicioConfiguracion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaServicioConfiguracion
    {
        #region campos

        private System.ComponentModel.BindingList<clsServicioConfiguracion> _objelementos = new System.ComponentModel.BindingList<clsServicioConfiguracion>();

        #endregion campos

        #region propiedades

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public System.ComponentModel.BindingList<clsServicioConfiguracion> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion propiedades

        #region constructor

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaServicioConfiguracion()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaServicioConfiguracion(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsServicioConfiguracion(_drw));
            }
        }

        #endregion constructor
    }
    */

    /// <summary>
    /// Entidad clsServicioConfiguracion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsServicioConfiguracion
    {
        #region Campos

        private Int16 _intid;
        private Int16 _intidempresa;
        private Int16 _intidservicio;
        private Int16? _intidproveedoractividad;
        private Int32 _intidnroservicio;
        private Int16 _intidesquemaintercambioperfil;
        private Int16 _intidtipodocumento;
        private Int16 _intcobrarvencidos;
        private Int16 _intidmoneda;
        private String _strrutacargafile;
        private String _strftpcargaserver;
        private String _strftpcargausuario;
        private String _strftpcargaclave;
        private String _strftpcargafolder;
        private String _strrutadescargafile;
        private String _strftpdescargaserver;
        private String _strftpdescargausuario;
        private String _strftpdescargoclave;
        private String _strftpdescargafolder;
        private Int32 _intlongitudcodigobarra;
        private Int32 _intposicioninicionroservicio;
        private Int32 _intlongitudnroservicio;
        private Int16 _intidEstadoNGC;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? IdProveedorActividad
        {
            get { return _intidproveedoractividad; }
            set { _intidproveedoractividad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEsquemaIntercambioPerfil
        {
            get { return _intidesquemaintercambioperfil; }
            set { _intidesquemaintercambioperfil = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 CobrarVencidos
        {
            get { return _intcobrarvencidos; }
            set { _intcobrarvencidos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String RutaCargaFile
        {
            get { return _strrutacargafile; }
            set { _strrutacargafile = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaServer
        {
            get { return _strftpcargaserver; }
            set { _strftpcargaserver = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaUsuario
        {
            get { return _strftpcargausuario; }
            set { _strftpcargausuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaClave
        {
            get { return _strftpcargaclave; }
            set { _strftpcargaclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPCargaFolder
        {
            get { return _strftpcargafolder; }
            set { _strftpcargafolder = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String RutaDescargaFile
        {
            get { return _strrutadescargafile; }
            set { _strrutadescargafile = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargaServer
        {
            get { return _strftpdescargaserver; }
            set { _strftpdescargaserver = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargaUsuario
        {
            get { return _strftpdescargausuario; }
            set { _strftpdescargausuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargoClave
        {
            get { return _strftpdescargoclave; }
            set { _strftpdescargoclave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String FTPDescargaFolder
        {
            get { return _strftpdescargafolder; }
            set { _strftpdescargafolder = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 LongitudCodigoBarra
        {
            get { return _intlongitudcodigobarra; }
            set { _intlongitudcodigobarra = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 PosicionInicioNroServicio
        {
            get { return _intposicioninicionroservicio; }
            set { _intposicioninicionroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 LongitudNroServicio
        {
            get { return _intlongitudnroservicio; }
            set { _intlongitudnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstadoNGC
        {
            get { return _intidEstadoNGC; }
            set { _intidEstadoNGC = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsServicioConfiguracion()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsServicioConfiguracion(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt16(dr["id"]);
            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdServicio = Convert.ToInt16(dr["idservicio"]);
            IdProveedorActividad = Convert.IsDBNull(dr["idproveedoractividad"]) ? (Int16?)null : Convert.ToInt16(dr["idproveedoractividad"]);
            IdNroServicio = Convert.ToInt32(dr["idnroservicio"]);
            IdEsquemaIntercambioPerfil = Convert.ToInt16(dr["idesquemaintercambioperfil"]);
            IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            CobrarVencidos = Convert.ToInt16(dr["cobrarvencidos"]);
            IdMoneda = Convert.ToInt16(dr["idmoneda"]);
            RutaCargaFile = dr["rutacargafile"].ToString();
            FTPCargaServer = dr["ftpcargaserver"].ToString();
            FTPCargaUsuario = dr["ftpcargausuario"].ToString();
            FTPCargaClave = dr["ftpcargaclave"].ToString();
            FTPCargaFolder = dr["ftpcargafolder"].ToString();
            RutaDescargaFile = dr["rutadescargafile"].ToString();
            FTPDescargaServer = dr["ftpdescargaserver"].ToString();
            FTPDescargaUsuario = dr["ftpdescargausuario"].ToString();
            FTPDescargoClave = dr["ftpdescargoclave"].ToString();
            FTPDescargaFolder = dr["ftpdescargafolder"].ToString();
            LongitudCodigoBarra = Convert.ToInt32(dr["longitudcodigobarra"]);
            PosicionInicioNroServicio = Convert.ToInt32(dr["posicioninicionroservicio"]);
            LongitudNroServicio = Convert.ToInt32(dr["longitudnroservicio"]);
            IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista clsServicioConfiguracion
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaclsServicioConfiguracion
    {
        #region Campos

        private List<clsServicioConfiguracion> _objelementos = new List<clsServicioConfiguracion>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public List<clsServicioConfiguracion> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaclsServicioConfiguracion()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaclsServicioConfiguracion(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsServicioConfiguracion(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion ServicioConfiguracion

    #region ServicioIntercambioParametros

    /// <summary>
    /// Entidad ServicioIntercambioParametros
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsServicioIntercambioParametros
    {
        #region Campos

        private Int16 _intid;
        private Int16 _intidserviciointercambio;
        private Int16 _intidcentroservicio;
        private DateTime _datfechaproceso;
        private Int32 _intidusuario;
        private Int64 _inttotalprocesar;
        private Int64 _inttotalprocesando;
        private String _strestaciontrabajo;
        private Int16 _intidEstadoNGC;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdServicioIntercambio
        {
            get { return _intidserviciointercambio; }
            set { _intidserviciointercambio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaProceso
        {
            get { return _datfechaproceso; }
            set { _datfechaproceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 TotalProcesar
        {
            get { return _inttotalprocesar; }
            set { _inttotalprocesar = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 TotalProcesando
        {
            get { return _inttotalprocesando; }
            set { _inttotalprocesando = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String EstacionTrabajo
        {
            get { return _strestaciontrabajo; }
            set { _strestaciontrabajo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstadoNGC
        {
            get { return _intidEstadoNGC; }
            set { _intidEstadoNGC = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsServicioIntercambioParametros()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsServicioIntercambioParametros(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt16(dr["id"]);
            IdServicioIntercambio = Convert.ToInt16(dr["idserviciointercambio"]);
            IdCentroServicio = Convert.ToInt16(dr["idcentroservicio"]);
            FechaProceso = Convert.ToDateTime(dr["fechaproceso"]);
            IdUsuario = Convert.ToInt32(dr["idusuario"]);
            TotalProcesar = Convert.ToInt64(dr["totalprocesar"]);
            TotalProcesando = Convert.ToInt64(dr["totalprocesando"]);
            EstacionTrabajo = dr["estaciontrabajo"].ToString();
            IdEstadoNGC = Convert.ToInt16(dr["idEstadoNGC"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista ServicioIntercambioParametros
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaServicioIntercambioParametros
    {
        #region Campos

        private System.ComponentModel.BindingList<clsServicioIntercambioParametros> _objelementos = new System.ComponentModel.BindingList<clsServicioIntercambioParametros>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public System.ComponentModel.BindingList<clsServicioIntercambioParametros> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaServicioIntercambioParametros()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaServicioIntercambioParametros(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsServicioIntercambioParametros(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion ServicioIntercambioParametros

    #region ServicioIntercambioLog

    /// <summary>
    /// Entidad ServicioIntercambioLog
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsServicioIntercambioLog
    {
        #region Campos

        private Int16 _intidserviciointercambioparametro;
        private DateTime _datfechaproceso;
        private String _strdescripcionlog;
        private Int32 _inttamayoarchivoprocesado;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 IdServicioIntercambioParametro
        {
            get { return _intidserviciointercambioparametro; }
            set { _intidserviciointercambioparametro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaProceso
        {
            get { return _datfechaproceso; }
            set { _datfechaproceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String DescripcionLog
        {
            get { return _strdescripcionlog; }
            set { _strdescripcionlog = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 TamayoArchivoProcesado
        {
            get { return _inttamayoarchivoprocesado; }
            set { _inttamayoarchivoprocesado = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsServicioIntercambioLog()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsServicioIntercambioLog(DataRow dr)
        {
            if (dr == null) { return; }

            IdServicioIntercambioParametro = Convert.ToInt16(dr["idserviciointercambioparametro"]);
            FechaProceso = Convert.ToDateTime(dr["fechaproceso"]);
            DescripcionLog = dr["descripcionlog"].ToString();
            TamayoArchivoProcesado = Convert.ToInt32(dr["tamayoarchivoprocesado"]);
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista ServicioIntercambioLog
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaServicioIntercambioLog
    {
        #region Campos

        private System.ComponentModel.BindingList<clsServicioIntercambioLog> _objelementos = new System.ComponentModel.BindingList<clsServicioIntercambioLog>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public System.ComponentModel.BindingList<clsServicioIntercambioLog> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaServicioIntercambioLog()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaServicioIntercambioLog(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsServicioIntercambioLog(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion ServicioIntercambioLog

    #region ServicioIntercambioAlcance

    /// <summary>
    /// Entidad ServicioIntercambioAlcance
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsServicioIntercambioAlcance
    {
        #region Campos

        private Int16 _intid = 0;
        private Int16 _intidserviciointercambioparametro;
        private String _stridalcance;
        private String _strvalor;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdServicioIntercambioParametro
        {
            get { return _intidserviciointercambioparametro; }
            set { _intidserviciointercambioparametro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Alcance
        {
            get { return _stridalcance; }
            set { _stridalcance = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Valor
        {
            get { return _strvalor; }
            set { _strvalor = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsServicioIntercambioAlcance()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsServicioIntercambioAlcance(DataRow dr)
        {
            if (dr == null) { return; }

            Id = Convert.ToInt16(dr["id"]);
            IdServicioIntercambioParametro = Convert.ToInt16(dr["idserviciointercambioparametro"]);
            Alcance = dr["alcance"].ToString();
            Valor = dr["valor"].ToString();
        }

        /// <summary>
        /// Constructor pasando como parametros valores.
        /// </summary>
        /// <param name="alcance"></param>
        /// <param name="valor"></param>
        public clsServicioIntercambioAlcance(String alcance, Object valor)
        {
            Alcance = alcance;
            Valor = (valor == null ? null : valor.ToString());
            Id = 0;
            IdServicioIntercambioParametro = 0;
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista ServicioIntercambioAlcance
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsListaServicioIntercambioAlcance
    {
        #region Campos

        private System.ComponentModel.BindingList<clsServicioIntercambioAlcance> _objelementos = new System.ComponentModel.BindingList<clsServicioIntercambioAlcance>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public System.ComponentModel.BindingList<clsServicioIntercambioAlcance> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaServicioIntercambioAlcance()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaServicioIntercambioAlcance(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsServicioIntercambioAlcance(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion ServicioIntercambioAlcance

    #region ServicioIntercambioDebitoAutomatico

    #region Clase

    /// <summary>
    /// Entidad ServicioIntercambioAlcance
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsServicioIntercambioDebitoAutomatico
    {
        #region Campos

        private Int16 _intidempresa;
        private Int16 _intidproveedorbanco;
        private Int32 _periodocomercial;
        private Int16 _intidano;
        private Int16 _intidmes;
        private DateTime _fechageneracion;
        private DateTime _fechavigencia;
        private Int16 _intidproveedoractividad;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdProveedorBanco
        {
            get { return _intidproveedorbanco; }
            set { _intidproveedorbanco = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 PeriodoComercial
        {
            get { return _periodocomercial; }
            set { _periodocomercial = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdAno
        {
            get { return _intidano; }
            set { _intidano = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdMes
        {
            get { return _intidmes; }
            set { _intidmes = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaGeneracion
        {
            get { return _fechageneracion; }
            set { _fechageneracion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaVigencia
        {
            get { return _fechavigencia; }
            set { _fechavigencia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdProveedorActividad
        {
            get { return _intidproveedoractividad; }
            set { _intidproveedoractividad = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsServicioIntercambioDebitoAutomatico()
        {
        }

        #endregion Constructor
    }

    #endregion Clase

    #endregion ServicioIntercambioDebitoAutomatico

    #region INTERFASES

    /// <summary>
    ///
    /// </summary>
    public interface IIntercambioPerfil
    {
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        clsListaEsquemaIntercambioPerfilMaestro ObtenerListaMaestroEsquemaIntercambioPerfil();

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        clsEsquemaIntercambioPerfil ObtenerRegistroEsquemaIntercambioPerfil(Int16 id);

        /// <summary>
        ///
        /// </summary>
        /// <param name="objEIFormato"></param>
        /// <returns></returns>
        clsEsquemaIntercambioPerfil ObtenerRegistroEsquemaIntercambioPerfilxFormato(clsEsquemaIntercambioFormato objEIFormato);


        /// <summary>
        ///
        /// </summary>
        /// <param name="esquemaintercambioperfil"></param>
        /// <returns></returns>
        Boolean InsertarEsquemaIntercambioPerfil(clsEsquemaIntercambioPerfil esquemaintercambioperfil);

        /// <summary>
        ///
        /// </summary>
        /// <param name="esquemaintercambioperfil"></param>
        /// <returns></returns>
        Boolean ActualizarEsquemaIntercambioPerfil(clsEsquemaIntercambioPerfil esquemaintercambioperfil);

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nuevoEstadoNGC"></param>
        /// <returns></returns>
        Boolean CambiarEstadoNGCEsquemaIntercambioPerfil(Int16 id, EstadoNGC nuevoEstadoNGC);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IIntercambioFormato
    {
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        clsListaEsquemaIntercambioFormato ObtenerListaMaestroEsquemaIntercambioFormato();

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        clsListaEsquemaIntercambioFormatoEstructura ObtenerListaEsquemaIntercambioFormatoEstructura(Int32 idformato);

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        clsEsquemaIntercambioFormato ObtenerRegistroEsquemaIntercambioFormato(Int32 id);

        /// <summary>
        ///
        /// </summary>
        /// <param name="esquemaformato"></param>
        /// <returns></returns>
        Boolean InsertarEsquemaIntercambioFormato(clsEsquemaIntercambioFormato esquemaformato);

        /// <summary>
        ///
        /// </summary>
        /// <param name="esquemaformato"></param>
        /// <returns></returns>
        Boolean InsertarEsquemaIntercambioFormatoEstructura(clsListaEsquemaIntercambioFormatoEstructura esquemaformato);

        /// <summary>
        ///
        /// </summary>
        /// <param name="esquemaformato"></param>
        /// <returns></returns>
        Boolean ActualizarEsquemaIntercambioFormato(clsEsquemaIntercambioFormato esquemaformato);

        /// <summary>
        ///
        /// </summary>
        /// <param name="esquemaformato"></param>
        /// <returns></returns>
        Boolean ActualizarEsquemaIntercambioFormatoEstructura(clsListaEsquemaIntercambioFormatoEstructura esquemaformato);

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        XmlSchemas ObtenerEsquemaIntercambioFormatoEsquema(Int32 id);

        /// <summary>
        ///
        /// </summary>
        /// <param name="esquema"></param>
        /// <returns></returns>
        Boolean GuardarEsquemaIntercambioFormatoEsquema(XmlSchemas esquema);

        /// <summary>
        /// Eliminar registro de Intercambio Formato.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idEstadoNGC"></param>
        /// <returns></returns>
        Boolean EliminarEsquemaIntercambioFormato(Int16 id, EstadoNGC idEstadoNGC);

        /// <summary>
        /// Confirmar/Aceptar Formato.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Boolean AceptarEsquemaIntercambioFormato(Int16 id);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IIntercambioEstructura
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="idformato"></param>
        /// <returns></returns>
        clsListaEsquemaIntercambioEstructura ObtenerListaEsquemaIntercambioEstructura(Int32 idformato);

        /// <summary>
        ///
        /// </summary>
        /// <param name="idestructura"></param>
        /// <returns></returns>
        clsEsquemaIntercambioEstructura ObtenerRegistroEsquemaIntercambioEstructura(Int32 idestructura);

        /// <summary>
        ///
        /// </summary>
        /// <param name="objestructura"></param>
        /// <returns></returns>
        Boolean InsertarEsquemaIntercambioEstructura(clsEsquemaIntercambioEstructura objestructura);

        /// <summary>
        ///
        /// </summary>
        /// <param name="objestructura"></param>
        /// <returns></returns>
        Boolean ActualizarEsquemaIntercambioEstructura(clsEsquemaIntercambioEstructura objestructura);

        /// <summary>
        ///
        /// </summary>
        /// <param name="idestructura"></param>
        /// <returns></returns>
        Boolean EliminarEsquemaIntercambioEstructura(Int32 idestructura);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IIntercambioConvertir
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="idestructura"></param>
        clsListaEsquemaIntercambioConversion ObtenerListaEsquemaIntercambioConversion(Int16 idestructura);

        /// <summary>
        ///
        /// </summary>
        /// <param name="idformato"></param>
        /// <returns></returns>
        clsListaEsquemaIntercambioConversion ObtenerListaEsquemaIntercambioConversionFormato(Int16 idformato);

        /// <summary>
        ///
        /// </summary>
        /// <param name="entidad"></param>
        /// <returns></returns>
        Boolean InsertarEsquemaIntercambioConversion(clsEsquemaIntercambioConversion entidad);

        /// <summary>
        ///
        /// </summary>
        /// <param name="entidad"></param>
        /// <returns></returns>
        Boolean ActualizarEsquemaIntercambioConversion(clsEsquemaIntercambioConversion entidad);

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Boolean EliminarEsquemaIntercambioConversion(Int16 id, Int16 idestructura);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IServicioConfiguracion
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="idservicioconfiguracion"></param>
        /// <returns></returns>
        clsServicioConfiguracion ServicioConfiguracionObtenerRegistro(Int16 idservicioconfiguracion);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IServicioIntercambioParametros
    {
        /// <summary>
        /// Busca el proceso de intercambio pendiente y devuelve el registro.
        /// </summary>
        clsServicioIntercambioParametros ServicioIntercambioParametrosObtenerRegistroProcesoPendiente(Int16 idserviciointercambio, Int16 @idEstadoNGCpendiente);

        /// <summary>
        /// Devuelve el registro de intercambio pendiente.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        clsServicioIntercambioParametros ServicioIntercambioParametrosObtenerRegistro(Int16 id);

        /// <summary>
        ///
        /// </summary>
        /// <param name="_objentidad"></param>
        /// <returns></returns>
        Int16 ServicioIntercambioParametrosInsertar(clsServicioIntercambioParametros _objentidad);

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="EstadoNGC"></param>
        /// <returns></returns>
        Boolean ServicioIntercambioParametrosActualizarEstadoNGC(Int16 id, EstadoNGC EstadoNGC);

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tamayo"></param>
        /// <returns></returns>
        Boolean ServicioIntercambioParametrosActualizarTamayoProcesado(Int16 id, Int64 tamayo);

        /// <summary>
        ///
        /// </summary>
        /// <param name="idserviciointercambioparametro"></param>
        /// <returns></returns>
        Int64 ServicioIntercambioParametrosObtenerTamayoProcesado(Int16 idserviciointercambioparametro);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IServicioIntercambioLog
    {
        /// <summary>
        /// Obtiene lista del log de un proceso determinado.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        clsListaServicioIntercambioLog ServicioIntercambioLogObtenerLista(Int16 id);

        /// <summary>
        /// Inserta un registro en la tabla ServicioIntercambioLog
        /// </summary>
        /// <param name="objentidad"></param>
        /// <returns></returns>
        Boolean ServicioIntercambioLogInsertar(clsServicioIntercambioLog objentidad);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IServicioIntercambioAlcance
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="objentidad"></param>
        /// <returns></returns>
        Boolean ServicioIntercambioAlcanceInsertar(clsServicioIntercambioAlcance objentidad);

        /// <summary>
        ///
        /// </summary>
        /// <param name="idserviciointercambioparametro"></param>
        /// <returns></returns>
        clsListaServicioIntercambioAlcance ServicioIntercambioAlcanceObtenerLista(Int16 idserviciointercambioparametro);
    }

    /// <summary>
    ///
    /// </summary>
    public interface IIntercambioColumnas
    {
        /// <summary>
        /// Obtiene lista de la tabla EsquemaIntercambioColumnas
        /// </summary>
        /// <returns></returns>
        clsListaEsquemaIntercambioColumnas EsquemaIntercambioColumnasObtenerLista(Int16 tiporegistro);

        /// <summary>
        /// Obtiene lista maestra de la tabla EsquemaIntercambioColumnas
        /// </summary>
        /// <param name="tiporegistro"></param>
        /// <returns></returns>
        clsListaEntidadMaestra EsquemaIntercambioColumnasObtenerListaMaestra(Int16 tiporegistro);
    }

    /// <summary>
    /// Interface para generar el archivo de intercambio para debito automatico
    /// </summary>
    public interface IIntercambioDebitoAutomatico
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="objserviciointercambiodebauto"></param>
        /// <returns></returns>
        Boolean GenerarArchivoDebitoAutomatico(clsServicioIntercambioDebitoAutomatico objserviciointercambiodebauto);
    }

    #endregion INTERFASES

    #region Parametro para generaci�n del esquema de carpetas.

    /// <summary>
    /// Parametros para Gestionar Esquema de Carpetas.
    /// </summary>
    public class clsEsquemaCarpetaCliente : IEsquemaCarpetaCliente
    {
        private Int16 _intidproveedoractividad;
        private Int16 _intidunidadnegocio;
        private Int16 _intidcentroservicio;
        private String _strperiodo;
        private String _strrutadescargaarchivo;

        #region IEsquemaCarpetaCliente Members

        public short IdUnidadNegocio
        {
            get
            {
                return _intidunidadnegocio;
            }
            set
            {
                _intidunidadnegocio = value;
            }
        }

        public short IdCentroServicio
        {
            get
            {
                return _intidcentroservicio;
            }
            set
            {
                _intidcentroservicio = value;
            }
        }

        public string Periodo
        {
            get
            {
                return _strperiodo;
            }
            set
            {
                _strperiodo = value;
            }
        }

        #endregion IEsquemaCarpetaCliente Members

        public Int16 IdProveedorActividad
        {
            get { return _intidproveedoractividad; }
            set { _intidproveedoractividad = value; }
        }

        public String RutaDescargaArchivo
        {
            get { return _strrutadescargaarchivo; }
            set { _strrutadescargaarchivo = value; }
        }

        public clsEsquemaCarpetaCliente() { }

        /// <summary>
        /// Inicializa un objeto seg�n los par�metros.
        /// </summary>
        /// <param name="idproveedoractividad">ID del Proveedor Actividad.  El valor puede ser CERO (0).</param>
        /// <param name="idunidadnegocio">ID de la Unidad de Negocio.</param>
        /// <param name="idcentroservicio">ID del Centro de Servicio.</param>
        /// <param name="periodo">Periodo Comercial.</param>
        public clsEsquemaCarpetaCliente
            (Int16 idproveedoractividad
            , Int16 idunidadnegocio
            , Int16 idcentroservicio
            , String periodo)
        {
            _intidproveedoractividad = idproveedoractividad;
            _intidunidadnegocio = idunidadnegocio;
            _intidcentroservicio = idcentroservicio;
            _strperiodo = periodo;
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class clsEsquemaCarpetaBase : IEsquemaCarpetaBase
    {
        private String _strrutainicio;
        private Int16 _intidservicio;
        private Int16 _intidflujo;
        private Int16 _intidactividad;
        private Int16 _intidempresa;
        private enumTipoIntercambio _enumtipointercambio;

        #region IEsquemaCarpetaBase Members

        public string RutaInicio
        {
            get
            {
                return _strrutainicio;
            }
            set
            {
                _strrutainicio = value;
            }
        }

        public short IdServicio
        {
            get
            {
                return _intidservicio;
            }
            set
            {
                _intidservicio = value;
            }
        }

        public short IdFlujo
        {
            get
            {
                return _intidflujo;
            }
            set
            {
                _intidflujo = value;
            }
        }

        public short IdActividad
        {
            get
            {
                return _intidactividad;
            }
            set
            {
                _intidactividad = value;
            }
        }

        public short IdEmpresa
        {
            get
            {
                return _intidempresa;
            }
            set
            {
                _intidempresa = value;
            }
        }

        public enumTipoIntercambio TipoIntercambio
        {
            get
            {
                return _enumtipointercambio;
            }
            set
            {
                _enumtipointercambio = value;
            }
        }

        #endregion IEsquemaCarpetaBase Members

        public clsEsquemaCarpetaBase() { }

        public clsEsquemaCarpetaBase(DataRow registro)
        {
            if (registro == null) { return; }

            _strrutainicio = registro["claveinicio"].ToString();
            _intidservicio = Convert.ToInt16(registro["idservicio"]);
            _intidflujo = Convert.ToInt16(registro["idflujo"]);
            _intidactividad = Convert.ToInt16(registro["idactividad"]);
            _intidempresa = Convert.ToInt16(registro["idempresa"]);
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class clsEsquemaCarpeta : IEsquemaCarpetaBase, IEsquemaCarpetaCliente
    {
        private String _strrutainicio;
        private Int16 _intidservicio;
        private Int16 _intidflujo;
        private Int16 _intidactividad;
        private Int16 _intidempresa;
        private enumTipoIntercambio _enumtipointercambio;
        private Int16 _intidunidadnegocio;
        private Int16 _intidcentroservicio;
        private String _strperiodo;

        #region IEsquemaCarpetaBase Members

        public string RutaInicio
        {
            get
            {
                return _strrutainicio;
            }
            set
            {
                _strrutainicio = value;
            }
        }

        public short IdServicio
        {
            get
            {
                return _intidservicio;
            }
            set
            {
                _intidservicio = value;
            }
        }

        public short IdFlujo
        {
            get
            {
                return _intidflujo;
            }
            set
            {
                _intidflujo = value;
            }
        }

        public short IdActividad
        {
            get
            {
                return _intidactividad;
            }
            set
            {
                _intidactividad = value;
            }
        }

        public short IdEmpresa
        {
            get
            {
                return _intidempresa;
            }
            set
            {
                _intidempresa = value;
            }
        }

        public enumTipoIntercambio TipoIntercambio
        {
            get
            {
                return _enumtipointercambio;
            }
            set
            {
                _enumtipointercambio = value;
            }
        }

        #endregion IEsquemaCarpetaBase Members

        #region IEsquemaCarpetaCliente Members

        public short IdUnidadNegocio
        {
            get
            {
                return _intidunidadnegocio;
            }
            set
            {
                _intidunidadnegocio = value;
            }
        }

        public short IdCentroServicio
        {
            get
            {
                return _intidcentroservicio;
            }
            set
            {
                _intidcentroservicio = value;
            }
        }

        public string Periodo
        {
            get
            {
                return _strperiodo;
            }
            set
            {
                _strperiodo = value;
            }
        }

        #endregion IEsquemaCarpetaCliente Members
    }

    public interface IEsquemaCarpetaBase
    {
        String RutaInicio
        {
            get;
            set;
        }

        Int16 IdServicio
        {
            get;
            set;
        }

        Int16 IdFlujo
        {
            get;
            set;
        }

        Int16 IdActividad
        {
            get;
            set;
        }

        Int16 IdEmpresa
        {
            get;
            set;
        }

        enumTipoIntercambio TipoIntercambio
        {
            get;
            set;
        }
    }

    public interface IEsquemaCarpetaCliente
    {
        Int16 IdUnidadNegocio
        {
            get;
            set;
        }

        Int16 IdCentroServicio
        {
            get;
            set;
        }

        String Periodo
        {
            get;
            set;
        }
    }

    #endregion Parametro para generaci�n del esquema de carpetas.

    #region EncargoTerceros

    /// <summary>
    /// Entidad clsEncargo_Terceros
    /// </summary>
    [Serializable()]
    public class clsEncargoTerceros
    {
        #region Campos

        private Int32 _intidnroservicio;
        private Int16 _intidservicio;
        private Int16 _intidtiporecibo;
        private String _strnrorecibo;
        private DateTime _datfechaemision;
        private Int32 _intidencargocontrol;
        private String _strnroservicio;
        private DateTime? _datfechavencimiento;
        private DateTime _datfechacarga;
        private String _strnombreserviciotercero;
        private Decimal _decimporteservicios;
        private Decimal _decimporterecargos;
        private Decimal _decimporteimpuestos;
        private Decimal _decimportetotal;
        private String _stridtag;
        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private Int16 _intidmoneda;
        private Int16 _intEstadoNGC;
        private String _strcentroserviciotercero;
        private String _strcodigo3tag;

        private String _strcodigo4tag;

        private String _strnroreciboregularizado;

        private String _strcodigo2tag;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoRecibo
        {
            get { return _intidtiporecibo; }
            set { _intidtiporecibo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NroRecibo
        {
            get { return _strnrorecibo; }
            set { _strnrorecibo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaEmision
        {
            get { return _datfechaemision; }
            set { _datfechaemision = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdEncargoControl
        {
            get { return _intidencargocontrol; }
            set { _intidencargocontrol = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NroServicio
        {
            get { return _strnroservicio; }
            set { _strnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaCarga
        {
            get { return _datfechacarga; }
            set { _datfechacarga = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreServicioTercero
        {
            get { return _strnombreserviciotercero; }
            set { _strnombreserviciotercero = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteServicios
        {
            get { return _decimporteservicios; }
            set { _decimporteservicios = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteRecargos
        {
            get { return _decimporterecargos; }
            set { _decimporterecargos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteImpuestos
        {
            get { return _decimporteimpuestos; }
            set { _decimporteimpuestos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteTotal
        {
            get { return _decimportetotal; }
            set { _decimportetotal = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String IdTag
        {
            get { return _stridtag; }
            set { _stridtag = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 EstadoNGC
        {
            get { return _intEstadoNGC; }
            set { _intEstadoNGC = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String CentroServicioTercero
        {
            get { return _strcentroserviciotercero; }
            set { _strcentroserviciotercero = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Codigo3Tag
        {
            get { return _strcodigo3tag; }
            set { _strcodigo3tag = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Codigo4Tag
        {
            get { return _strcodigo4tag; }
            set { _strcodigo4tag = value; }
        }

        /// <summary>
        /// /
        /// </summary>
        public String NroReciboRegularizado
        {
            get { return _strnroreciboregularizado; }
            set { _strnroreciboregularizado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Codigo2Tag
        {
            get { return _strcodigo2tag; }
            set { _strcodigo2tag = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o por defecto.
        /// </summary>
        public clsEncargoTerceros()
        {
        }

        /// <summary>
        /// Constructor pasando como par�metro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsEncargoTerceros(DataRow dr)
        {
            if (dr == null) { return; }

            IdNroServicio = Convert.ToInt32(dr["idnroservicio"]);
            IdServicio = Convert.ToInt16(dr["idservicio"]);
            IdTipoRecibo = Convert.ToInt16(dr["idtiporecibo"]);
            NroRecibo = dr["nrorecibo"].ToString();
            FechaEmision = Convert.ToDateTime(dr["fechaemision"]);
            IdEncargoControl = Convert.ToInt32(dr["idencargocontrol"]);
            NroServicio = dr["nroservicio"].ToString();
            FechaVencimiento = Convert.IsDBNull(dr["fechavencimiento"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechavencimiento"]);
            FechaCarga = Convert.ToDateTime(dr["fechacarga"]);
            NombreServicioTercero = dr["nombreserviciotercero"].ToString();
            ImporteServicios = Convert.ToDecimal(dr["importeservicios"]);
            ImporteRecargos = Convert.ToDecimal(dr["importerecargos"]);
            ImporteImpuestos = Convert.ToDecimal(dr["importeimpuestos"]);
            ImporteTotal = Convert.ToDecimal(dr["importetotal"]);
            IdTag = dr["idtag"].ToString();
            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            NroDocumento = dr["nrodocumento"].ToString();
            IdMoneda = Convert.ToInt16(dr["idmoneda"]);
            EstadoNGC = Convert.ToInt16(dr["EstadoNGC"]);
            CentroServicioTercero = dr["centroserviciotercero"].ToString();
            Codigo3Tag = dr["codigo3tag"].ToString();
            Codigo4Tag = dr["codigo4tag"].ToString();
            NroReciboRegularizado = dr["nroreciboregularizado"].ToString();
            Codigo2Tag = dr["codigo2tag"].ToString();
        }

        #endregion Constructor
    }

    /// <summary>
    /// Entidad Lista clsEncargo_Terceros
    /// </summary>
    [Serializable()]
    public class clsListaclsEncargoTerceros
    {
        #region Campos

        private List<clsEncargoTerceros> _objelementos = new List<clsEncargoTerceros>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definici�n de la Propiedad "Elementos"
        /// </summary>
        public List<clsEncargoTerceros> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vac�o
        /// </summary>
        public clsListaclsEncargoTerceros()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaclsEncargoTerceros(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsEncargoTerceros(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion EncargoTerceros
}