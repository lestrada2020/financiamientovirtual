﻿namespace SISAC.Entidad.Facturacion.ImpresionRecibo
{
    //No es necesario pasar esta Clase al control de versiones
    public class ClsParametroImpresionRecibo
    {
        public short IdParametroImpresionRecibo { get; set; }
        public short IdEmpresa { get; set; }
        public short IdParametro { get; set; }
        public string NombreParametro { get; set; }
        public string ValorParametro { get; set; }
        public short IdEstado { get; set; }
    }
}
