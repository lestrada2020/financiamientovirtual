﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Entidad.Facturacion.ImpresionRecibo
{
    public class ClsListaControlGenerarArchivoImpresion
    {
        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<ClsControlGenerarArchivoImpresion> Elementos { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public ClsListaControlGenerarArchivoImpresion()
        {
            Elementos = new List<ClsControlGenerarArchivoImpresion>();
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public ClsListaControlGenerarArchivoImpresion(DataTable entidad)
        {
            Elementos = new List<ClsControlGenerarArchivoImpresion>();

            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new ClsControlGenerarArchivoImpresion(_drw));
            }
        }

        #endregion

    }
}
