﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Entidad.Facturacion.ImpresionRecibo
{
    public class ClsControlGenerarArchivoImpresion
    {
        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public short IdControlImpresion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NombreArchivo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdUsuarioGenera { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaGenera { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ResumenArchivo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public short IdSector { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IdTarea { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public short IdEstrato { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Cartera { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Periodo { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public ClsControlGenerarArchivoImpresion()
        {
        }

        public ClsControlGenerarArchivoImpresion(string resumenarchivo)
        {
            ResumenArchivo = resumenarchivo;
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public ClsControlGenerarArchivoImpresion(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idcontrolimpresion")) IdControlImpresion = Convert.ToInt16(dr["idcontrolimpresion"]);
            if (dr.Table.Columns.Contains("nombrearchivo")) NombreArchivo = dr["nombrearchivo"].ToString();
            if (dr.Table.Columns.Contains("idusuariogenera")) IdUsuarioGenera = Convert.ToInt32(dr["idusuariogenera"]);
            if (dr.Table.Columns.Contains("fechagenera")) FechaGenera = Convert.ToDateTime(dr["fechagenera"]);
            if (dr.Table.Columns.Contains("resumenarchivo")) ResumenArchivo = dr["resumenarchivo"].ToString();
            if (dr.Table.Columns.Contains("idsector")) IdSector = Convert.ToInt16(dr["idsector"]);
            if (dr.Table.Columns.Contains("idestrato")) IdEstrato = Convert.ToInt16(dr["idestrato"]);
            if (dr.Table.Columns.Contains("cartera")) Cartera = dr["cartera"].ToString();
            if (dr.Table.Columns.Contains("periodo")) Periodo = Convert.ToInt32(dr["periodo"]);
            if (dr.Table.Columns.Contains("idtarea")) IdTarea = Convert.ToInt32(dr["idtarea"]);
        }

        #endregion
    }
}
