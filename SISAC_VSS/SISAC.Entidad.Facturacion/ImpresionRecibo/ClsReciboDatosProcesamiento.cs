﻿using Newtonsoft.Json;
using System.Collections.Generic;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Entidad.Facturacion.ImpresionRecibo
{
    /// <summary>
    /// Clase que permite deserializar la informacion del recibo de los suministros 
    /// guardada en formato JSON por el procasamiento de archivo de impresion
    /// </summary>
    public class ClsReciboDatosProcesamiento
    {
        #region Propiedades

        #region ReciboMenor

        [JsonProperty("clsReciboMenorCabecera")]
        public List<clsReciboMenorCabecera> ReciboMenorCabecera { get; set; }

        [JsonProperty("clsReciboMenorConcepto")]
        public List<clsReciboMenorConcepto> ReciboMenorConcepto { get; set; }

        [JsonProperty("clsReciboMenorConsumo")]
        public List<clsReciboMenorConsumo> ReciboMenorConsumo { get; set; }

        [JsonProperty("clsReciboMenorLectura")]
        public List<clsReciboMenorLectura> ReciboMenorLectura { get; set; }

        #endregion

        #region ReciboMayor

        [JsonProperty("clsReciboMayorCabecera")]
        public List<clsReciboMayorCabecera> ReciboMayorCabecera { get; set; }

        [JsonProperty("clsReciboMayorConcepto")]
        public List<clsReciboMayorConcepto> ReciboMayorConcepto { get; set; }

        [JsonProperty("clsReciboMayorConsumo")]
        public List<clsReciboMayorConsumo> ReciboMayorConsumo { get; set; }

        [JsonProperty("clsReciboMayorLectura")]
        public List<clsReciboMayorLectura> ReciboMayorLectura { get; set; }

        #endregion

        #endregion

        #region Constructor

        public ClsReciboDatosProcesamiento()
        {
            ReciboMenorCabecera = new List<clsReciboMenorCabecera>();
            ReciboMenorConcepto = new List<clsReciboMenorConcepto>();
            ReciboMenorConsumo = new List<clsReciboMenorConsumo>();
            ReciboMenorLectura = new List<clsReciboMenorLectura>();

            ReciboMayorCabecera = new List<clsReciboMayorCabecera>();
            ReciboMayorConcepto = new List<clsReciboMayorConcepto>();
            ReciboMayorConsumo = new List<clsReciboMayorConsumo>();
            ReciboMayorLectura = new List<clsReciboMayorLectura>();
        }

        #endregion
    }
}
