﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion
{

    public class clsFacturacionAdministrativaRespuesta
    {
        public Int16 IdEmpresa { get; set; }
        public Int16 IdTipoDocumento { get; set; }
        public String TipoDocumento { get; set; }

        public Boolean RegistroComprobante { get; set; }
        public String MensajeRegistro { get; set; }

        public Boolean SeGeneroXML { get; set; }
        public String RutaXML { get; set; }

        public Boolean SeGeneroPDF { get; set; }
        public String RutaPDF { get; set; }

        public Boolean SeEnvioCorreo { get; set; }

        public Boolean SeEnvioSUNAT { get; set; }
        public String MensajeSUNAT { get; set; }
        public String CodigoSUNAT { get; set; }
        public String RutaRespuestaSUNAT { get; set; }
        public String TicketSUNAT { get; set; }

        public Boolean RegistroSAP { get; set; }
        public String NroVoucherSAP { get; set; }


        public String NroDocumento { get; set; }
        public Int32 IdNroServicio { get; set; }

        public String NroConvenio { get; set; }

        public Int32 IdPresupuesto { get; set; }
        public Int64? IdOrdenTrabajo { get; set; }

        public clsFacturacionAdministrativaRespuesta()
        { }
    }

}
