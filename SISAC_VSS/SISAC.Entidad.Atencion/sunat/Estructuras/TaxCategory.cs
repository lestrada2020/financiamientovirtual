﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion.sunat.Estructuras
{
    [Serializable]
    public class TaxCategory
    {
        public string TaxExemptionReasonCode { get; set; }
        public string TierRange { get; set; }
        public TaxScheme TaxScheme { get; set; }
        public string ID { get; set; }

        public TaxCategory()
        {
            TaxScheme = new TaxScheme();
        }
    }
}