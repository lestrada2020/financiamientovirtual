﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion.sunat.Estructuras
{
    [Serializable]
    public class PartyIdentificationID
    {
        public string schemeID { get; set; }
        public string value { get; set; }
    }
}