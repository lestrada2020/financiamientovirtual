﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion.sunat.Model
{
    public class GrupoResumen : DocumentoResumenDetalle
    {
        public int CorrelativoInicio { get; set; }
        public int CorrelativoFin { get; set; }

        public string EstadoItem { get; set; }

        public string SerieNumeroComprobante { get; set; }

        public string TipoDocumentoModificado { get; set; }
        public string SerieNumeroComprobanteModificado { get; set; }

        public string TipoIdentidadCliente { get; set; }
        public string NroIdentidadCliente { get; set; }

        public string Moneda { get; set; }
        public decimal TotalVenta { get; set; }
        public decimal TotalDescuentos { get; set; }

        public decimal TotalIgv { get; set; }
        public string CodigoTributoIgv { get; set; }
        public string NombreTributoIgv { get; set; }
        public string CodigoInternacionalIgv { get; set; }

        public decimal TotalIsc { get; set; }
        public string CodigoTributoIsc { get; set; }
        public string NombreTributoIsc { get; set; }
        public string CodigoInternacionalIsc { get; set; }

        public decimal TotalOtrosImpuestos { get; set; }
        public string CodigoTributoOtr { get; set; }
        public string NombreTributoOtr { get; set; }
        public string CodigoInternacionalOtr { get; set; }

        public decimal Gravadas { get; set; }
        public string CodigoTipoGravadas { get; set; }

        public decimal Exoneradas { get; set; }
        public string CodigoTipoExoneradas { get; set; }

        public decimal Inafectas { get; set; }
        public string CodigoTipoInafectas { get; set; }

        public decimal Gratuitas { get; set; }
        public string CodigoTipoGratuitas { get; set; }

        public decimal Exportacion { get; set; }
        public string CodigoTipoExportacion { get; set; }

        public string IndicadorCargo { get; set; }
        public decimal ImporteOtrosCargos { get; set; }
    }

    //public class GrupoResumen : DocumentoResumenDetalle
    //{
    //    public int CorrelativoInicio { get; set; }
    //    public int CorrelativoFin { get; set; }
    //    public string Moneda { get; set; }
    //    public decimal TotalVenta { get; set; }
    //    public decimal TotalDescuentos { get; set; }
    //    public decimal TotalIgv { get; set; }
    //    public decimal TotalIsc { get; set; }
    //    public decimal TotalOtrosImpuestos { get; set; }
    //    public decimal Gravadas { get; set; }
    //    public decimal Exoneradas { get; set; }
    //    public decimal Inafectas { get; set; }
    //    public decimal Exportacion { get; set; }
    //    public decimal Gratuitas { get; set; }
    //}
}