﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion.sunat.Model
{
    public class FirmadoResponse : RespuestaComun
    {
        public string TramaXmlFirmado { get; set; }
        public string ResumenFirma { get; set; }
        public string ValorFirma { get; set; }
    }
}