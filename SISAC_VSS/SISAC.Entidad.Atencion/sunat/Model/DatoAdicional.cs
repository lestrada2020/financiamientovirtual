﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion.sunat.Model
{
    public class DatoAdicional
    {
        public string Codigo { get; set; }
        public string Contenido { get; set; }
    }
}
