﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml.Serialization;


namespace SISAC.Entidad.Atencion
{
    // Autor: Juan Carlos Díaz Urtecho
    // Ultima Modificación : 02/08/07

    /// <summary>
    /// Clase implementada para el manejo de comprobantes de pago : boletas, 
    /// facturas, notas de crédito, notas de débito
    /// </summary>
    /// 
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsComprobantePago
    {
        #region Parametros
        private Int16 _intidservicioprincipal;
        private Int32 _intidnroservicioprincipal;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private String _strnombretipodocumento;
        private String _strnombrecliente;
        private String _strdireccioncliente;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private DateTime _datfecharegistro;
        private Int16 _intidmoneda;
        private Int32 _intidusuario;
        private String _strnombreusuario;
        private Int16 _intcaja;
        private String _strnombremoneda;
        private String _strsimbolomoneda;
        private String _strmensajetotal;
        private String _strnombrepuntoatencion;
        private List<clsComprobantePagoDetalle> _objcomprobantepagodetalle = new List<clsComprobantePagoDetalle>();
        private clsComprobantePagoConvenio _objcomprobantepagoconvenio;
        private clsListaComprobantePagoDetalle _objcomprobantepagodetalleimpresion = new clsListaComprobantePagoDetalle();

        private DateTime _datfechavencimiento;

        private Decimal _decsubtotal;
        private Decimal _decigv;
        private Decimal _decimportetotal;

        private String _strdireccioncomplementariacentroservicio;

        private Decimal _decoperacionesgravadas;
        private Decimal _decoperacionesgratuitas;
        private Decimal _decoperacionesexoneradas;
        private Decimal _decoperacionesinafectas;
        private Decimal _decdescuentostotales;
        private Decimal _decpercepcion;
        private Decimal _decmontoconpercepcion;
        private String _strobservacion;
        private String _strmensajerepresentacionelectronica;

        private Byte[] _obimagenempresa;

        private String _strdireccioncentroservicio;
        private String _strdireccionempresa;
        private String _strdireccioncomplementariaempresa;
        private String _strsiglamoneda;

        private String _strcodigobarra;
        private Int16 _intesespecial;
        private String _strdocumentoidentidad;

        private String _strnombreempresacorto;
        private String _strrucempresa;
        private String _strimportelectras;
        private Int16 _inttieneconvenio;
        private String _strrutaimagenempresa;
        private String _strrutaimagencodigobarra;
        private Int16 _intidempresa;

        #endregion

        #region Propiedades
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public String DocumentoIdentidad
        {
            get { return _strdocumentoidentidad; }
            set { _strdocumentoidentidad = value; }
        }

        public Int16 EsEspecial
        {
            get { return _intesespecial; }
            set { _intesespecial = value; }
        }

        public String CodigoBarra
        {
            get { return _strcodigobarra; }
            set { _strcodigobarra = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionCentroServicio
        {
            get { return _strdireccioncentroservicio; }
            set { _strdireccioncentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionEmpresa
        {
            get { return _strdireccionempresa; }
            set { _strdireccionempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionComplementariaEmpresa
        {
            get { return _strdireccioncomplementariaempresa; }
            set { _strdireccioncomplementariaempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombrePuntoAtencion
        {
            get { return _strnombrepuntoatencion; }
            set { _strnombrepuntoatencion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String SimboloMoneda
        {
            get { return _strsimbolomoneda; }
            set { _strsimbolomoneda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombreMoneda
        {
            get { return _strnombremoneda; }
            set { _strnombremoneda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdServicioPrincipal
        {
            get { return _intidservicioprincipal; }
            set { _intidservicioprincipal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicioprincipal; }
            set { _intidnroservicioprincipal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String DireccionCliente
        {
            get { return _strdireccioncliente; }
            set { _strdireccioncliente = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreUsuario
        {
            get { return _strnombreusuario; }
            set { _strnombreusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdCaja
        {
            get { return _intcaja; }
            set { _intcaja = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MensajeTotal
        {
            get { return _strmensajetotal; }
            set { _strmensajetotal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<clsComprobantePagoDetalle> DetalleComprobantePago
        {
            get { return _objcomprobantepagodetalle; }
            set { _objcomprobantepagodetalle = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public clsComprobantePagoConvenio ConvenioComprobantePago
        {
            get { return _objcomprobantepagoconvenio; }
            set { _objcomprobantepagoconvenio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal SubTotal
        {
            get { return _decsubtotal; }
            set { _decsubtotal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal IGV
        {
            get { return _decigv; }
            set { _decigv = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ImporteTotal
        {
            get { return _decimportetotal; }
            set { _decimportetotal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionComplementariaCentroServicio
        {
            get { return _strdireccioncomplementariacentroservicio; }
            set { _strdireccioncomplementariacentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal MontoConPercepcion
        {
            get { return _decmontoconpercepcion; }
            set { _decmontoconpercepcion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Percepcion
        {
            get { return _decpercepcion; }
            set { _decpercepcion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesDescuentos
        {
            get { return _decdescuentostotales; }
            set { _decdescuentostotales = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesInafectas
        {
            get { return _decoperacionesinafectas; }
            set { _decoperacionesinafectas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesExoneradas
        {
            get { return _decoperacionesexoneradas; }
            set { _decoperacionesexoneradas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesGratuitas
        {
            get { return _decoperacionesgratuitas; }
            set { _decoperacionesgratuitas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesGravadas
        {
            get { return _decoperacionesgravadas; }
            set { _decoperacionesgravadas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Observacion
        {
            get { return _strobservacion; }
            set { _strobservacion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MensajeRepresentacionElectronica
        {
            get { return _strmensajerepresentacionelectronica; }
            set { _strmensajerepresentacionelectronica = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Byte[] ImagenEmpresa
        {
            get { return _obimagenempresa; }
            set { _obimagenempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String SiglaMoneda
        {
            get { return _strsiglamoneda; }
            set { _strsiglamoneda = value; }
        }

        public String RutaImagenCodigoBarra
        {
            get { return _strrutaimagencodigobarra; }
            set { _strrutaimagencodigobarra = value; }
        }

        public String RutaImagenEmpresa
        {
            get { return _strrutaimagenempresa; }
            set { _strrutaimagenempresa = value; }
        }

        public String NombreEmpresaCorto
        {
            get { return _strnombreempresacorto; }
            set { _strnombreempresacorto = value; }
        }

        public String RUCEmpresa
        {
            get { return _strrucempresa; }
            set { _strrucempresa = value; }
        }

        public String ImporteLetras
        {
            get { return _strimportelectras; }
            set { _strimportelectras = value; }
        }

        public Int16 TieneConvenio
        {
            get { return _inttieneconvenio; }
            set { _inttieneconvenio = value; }
        }

        public clsListaComprobantePagoDetalle DetalleComprobantePagoImpresion
        {
            get { return _objcomprobantepagodetalleimpresion; }
            set { _objcomprobantepagodetalleimpresion = value; }
        }


        #endregion

        #region Constructores
        /// <summary>
        /// 
        /// </summary>
        public clsComprobantePago()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        public clsComprobantePago(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idservicioprincipal")) this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            if (registro.Table.Columns.Contains("idnroservicio")) this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            if (registro.Table.Columns.Contains("nrodocumento")) this.NroDocumento = (String)registro["nrodocumento"];
            if (registro.Table.Columns.Contains("nombredocumento")) this.NombreTipoDocumento = (String)registro["nombredocumento"];
            //if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = Convert.IsDBNull(registro["fechacaja"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechacaja"]);
            if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = (String)registro["nombrecliente"];
            if (registro.Table.Columns.Contains("direccioncliente")) this.DireccionCliente = (String)registro["direccioncliente"];
            if (registro.Table.Columns.Contains("idtipoidentidadpropietario ")) this.IdTipoIdentidad = (Int16)registro["idtipoidentidadpropietario"];
            if (registro.Table.Columns.Contains("nroidentidadpropietario")) this.NroIdentidad = (String)registro["nroidentidadpropietario"];
            if (registro.Table.Columns.Contains("fecharegistro")) this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            if (registro.Table.Columns.Contains("idmoneda")) this.IdMoneda = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (registro.Table.Columns.Contains("nombreusuario")) this.NombreUsuario = (String)registro["nombreusuario"];
            if (registro.Table.Columns.Contains("caja")) this.IdCaja = (Int16)registro["caja"];
            if (registro.Table.Columns.Contains("nombremoneda")) this.NombreMoneda = registro["nombremoneda"].ToString().Trim();
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("mensajetotal")) this.MensajeTotal = registro["mensajetotal"].ToString();
            if (registro.Table.Columns.Contains("nombrepuntoatencion")) this.NombrePuntoAtencion = registro["nombrepuntoatencion"].ToString().Trim();
            if (registro.Table.Columns.Contains("subtotal")) this.SubTotal = Convert.ToDecimal(registro["subtotal"]);
            if (registro.Table.Columns.Contains("igv")) this.IGV = Convert.ToDecimal(registro["igv"]);
            if (registro.Table.Columns.Contains("importetotal")) this.ImporteTotal = Convert.ToDecimal(registro["importetotal"]);

            if (registro.Table.Columns.Contains("montoconpercepcion")) this.MontoConPercepcion = Convert.ToDecimal(registro["montoconpercepcion"]);
            if (registro.Table.Columns.Contains("percepcion")) this.Percepcion = Convert.ToDecimal(registro["percepcion"]);
            if (registro.Table.Columns.Contains("operacionesdescuentos")) this.OperacionesDescuentos = Convert.ToDecimal(registro["operacionesdescuentos"]);
            if (registro.Table.Columns.Contains("operacionesinafectas")) this.OperacionesInafectas = Convert.ToDecimal(registro["operacionesinafectas"]);
            if (registro.Table.Columns.Contains("operacionesexoneradas")) this.OperacionesExoneradas = Convert.ToDecimal(registro["operacionesexoneradas"]);
            if (registro.Table.Columns.Contains("operacionesgratuitas")) this.OperacionesGratuitas = Convert.ToDecimal(registro["operacionesgratuitas"]);
            if (registro.Table.Columns.Contains("operacionesgravadas")) this.OperacionesGravadas = Convert.ToDecimal(registro["operacionesgravadas"]);

            if (registro.Table.Columns.Contains("observacion")) this.Observacion = registro["observacion"].ToString();
            if (registro.Table.Columns.Contains("mensajerepresentacionelectronica")) this.MensajeRepresentacionElectronica = registro["mensajerepresentacionelectronica"].ToString();

            if (registro.Table.Columns.Contains("imagenempresa"))
                ImagenEmpresa = Convert.IsDBNull(registro["imagenempresa"]) ? (Byte[])null : (Byte[])registro["imagenempresa"];

            if (registro.Table.Columns.Contains("direccioncentroservicio")) this.DireccionCentroServicio = registro["direccioncentroservicio"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariacentroservicio")) this.DireccionComplementariaCentroServicio = registro["direccioncomplementariacentroservicio"].ToString();

            if (registro.Table.Columns.Contains("direccionempresa")) this.DireccionEmpresa = registro["direccionempresa"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariaempresa")) this.DireccionComplementariaEmpresa = registro["direccioncomplementariaempresa"].ToString();

            if (registro.Table.Columns.Contains("siglamoneda")) this.SiglaMoneda = registro["siglamoneda"].ToString();
            if (registro.Table.Columns.Contains("codigobarra")) this.CodigoBarra = registro["codigobarra"].ToString();

            if (registro.Table.Columns.Contains("esepecial")) this.EsEspecial = Convert.ToInt16(registro["esepecial"]);

            if (registro.Table.Columns.Contains("documentoidentidad")) this.DocumentoIdentidad = registro["documentoidentidad"].ToString();

            if (registro.Table.Columns.Contains("rutaimagencodigobarra")) this.RutaImagenCodigoBarra = registro["rutaimagencodigobarra"].ToString();
            if (registro.Table.Columns.Contains("rutaimagenempresa")) this.RutaImagenEmpresa = registro["rutaimagenempresa"].ToString();
            if (registro.Table.Columns.Contains("nombreempresacorto")) this.NombreEmpresaCorto = registro["nombreempresacorto"].ToString();
            if (registro.Table.Columns.Contains("rucempresa")) this.RUCEmpresa = registro["rucempresa"].ToString();
            if (registro.Table.Columns.Contains("importeletras")) this.ImporteLetras = registro["importeletras"].ToString();
            if (registro.Table.Columns.Contains("tieneconvenio")) this.TieneConvenio = Convert.ToInt16(registro["tieneconvenio"]);

            if (registro.Table.Columns.Contains("idempresa")) this.IdEmpresa = Convert.ToInt16(registro["idempresa"]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        /// <param name="detalle"></param>
        /// <param name="convenio"></param>
        public clsComprobantePago(DataRow registro, DataTable detalle, DataRow convenio)
        {
            if (registro == null) { return; }

            AsignarRegistro(registro);

            if (detalle != null)
            {
                foreach (DataRow _drw in detalle.Rows)
                {
                    _objcomprobantepagodetalle.Add(new clsComprobantePagoDetalle(_drw));
                }
            }

            if (convenio == null)
                _objcomprobantepagoconvenio = null;
            else
                _objcomprobantepagoconvenio = new clsComprobantePagoConvenio(convenio);
        }

        #endregion

        #region Metodos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        private void AsignarRegistro(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idservicioprincipal")) this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            if (registro.Table.Columns.Contains("idnroservicio")) this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            if (registro.Table.Columns.Contains("nrodocumento")) this.NroDocumento = (String)registro["nrodocumento"];
            if (registro.Table.Columns.Contains("nombredocumento")) this.NombreTipoDocumento = (String)registro["nombredocumento"];
            //if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = Convert.IsDBNull(registro["fechacaja"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechacaja"]);
            if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = (String)registro["nombrecliente"];
            if (registro.Table.Columns.Contains("direccioncliente")) this.DireccionCliente = (String)registro["direccioncliente"];
            if (registro.Table.Columns.Contains("idtipoidentidadpropietario ")) this.IdTipoIdentidad = (Int16)registro["idtipoidentidadpropietario"];
            if (registro.Table.Columns.Contains("nroidentidadpropietario")) this.NroIdentidad = (String)registro["nroidentidadpropietario"];
            if (registro.Table.Columns.Contains("fecharegistro")) this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            if (registro.Table.Columns.Contains("idmoneda")) this.IdMoneda = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (registro.Table.Columns.Contains("nombreusuario")) this.NombreUsuario = (String)registro["nombreusuario"];
            if (registro.Table.Columns.Contains("caja")) this.IdCaja = (Int16)registro["caja"];
            if (registro.Table.Columns.Contains("nombremoneda")) this.NombreMoneda = registro["nombremoneda"].ToString().Trim();
            if (registro.Table.Columns.Contains("simbolomoneda")) this.SimboloMoneda = registro["simbolomoneda"].ToString().Trim();
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("mensajetotal")) this.MensajeTotal = registro["mensajetotal"].ToString();
            if (registro.Table.Columns.Contains("nombrepuntoatencion")) this.NombrePuntoAtencion = registro["nombrepuntoatencion"].ToString().Trim();
            if (registro.Table.Columns.Contains("subtotal")) this.SubTotal = Convert.ToDecimal(registro["subtotal"]);
            if (registro.Table.Columns.Contains("igv")) this.IGV = Convert.ToDecimal(registro["igv"]);
            if (registro.Table.Columns.Contains("importetotal")) this.ImporteTotal = Convert.ToDecimal(registro["importetotal"]);

            if (registro.Table.Columns.Contains("montoconpercepcion")) this.MontoConPercepcion = Convert.ToDecimal(registro["montoconpercepcion"]);
            if (registro.Table.Columns.Contains("percepcion")) this.Percepcion = Convert.ToDecimal(registro["percepcion"]);
            if (registro.Table.Columns.Contains("operacionesdescuentos")) this.OperacionesDescuentos = Convert.ToDecimal(registro["operacionesdescuentos"]);
            if (registro.Table.Columns.Contains("operacionesinafectas")) this.OperacionesInafectas = Convert.ToDecimal(registro["operacionesinafectas"]);
            if (registro.Table.Columns.Contains("operacionesexoneradas")) this.OperacionesExoneradas = Convert.ToDecimal(registro["operacionesexoneradas"]);
            if (registro.Table.Columns.Contains("operacionesgratuitas")) this.OperacionesGratuitas = Convert.ToDecimal(registro["operacionesgratuitas"]);
            if (registro.Table.Columns.Contains("operacionesgravadas")) this.OperacionesGravadas = Convert.ToDecimal(registro["operacionesgravadas"]);

            if (registro.Table.Columns.Contains("observacion")) this.Observacion = registro["observacion"].ToString();
            if (registro.Table.Columns.Contains("mensajerepresentacionelectronica")) this.MensajeRepresentacionElectronica = registro["mensajerepresentacionelectronica"].ToString();

            if (registro.Table.Columns.Contains("imagenempresa"))
                ImagenEmpresa = Convert.IsDBNull(registro["imagenempresa"]) ? (Byte[])null : (Byte[])registro["imagenempresa"];

            if (registro.Table.Columns.Contains("direccioncentroservicio")) this.DireccionCentroServicio = registro["direccioncentroservicio"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariacentroservicio")) this.DireccionComplementariaCentroServicio = registro["direccioncomplementariacentroservicio"].ToString();

            if (registro.Table.Columns.Contains("direccionempresa")) this.DireccionEmpresa = registro["direccionempresa"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariaempresa")) this.DireccionComplementariaEmpresa = registro["direccioncomplementariaempresa"].ToString();

            if (registro.Table.Columns.Contains("siglamoneda")) this.SiglaMoneda = registro["siglamoneda"].ToString();
            if (registro.Table.Columns.Contains("codigobarra")) this.CodigoBarra = registro["codigobarra"].ToString();
            if (registro.Table.Columns.Contains("esespecial")) this.EsEspecial = Convert.ToInt16(registro["esespecial"]);
            if (registro.Table.Columns.Contains("documentoidentidad")) this.DocumentoIdentidad = registro["documentoidentidad"].ToString();
        }


        #endregion

    }
    /// <summary>
    /// 
    /// </summary>
    public class clsListaComprobantePago
    {
        private List<clsComprobantePago> _lstcomprobantepago = new List<clsComprobantePago>();

        #region Constructor

        public clsListaComprobantePago()
        { }

        public clsListaComprobantePago(DataTable comprobantepago)
        {
            if (comprobantepago == null || comprobantepago.Rows.Count == 0) { return; }

            foreach (DataRow fila in comprobantepago.Rows)
            {
                Elementos.Add(new clsComprobantePago(fila));
            }

        }

        //public clsListaComprobantePago(DataTable comprobantepago, DataTable comprobantepagodocumentos)
        //{
        //    if (comprobantepago == null || comprobantepago.Rows.Count == 0) { return; }

        //    AsignarIngresoProvision(ingresoprovision, ingresoprovisiondocumentos);
        //}
        #endregion

        #region Propiedades
        public List<clsComprobantePago> Elementos
        {
            get { return _lstcomprobantepago; }
            set { _lstcomprobantepago = value; }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsComprobantePagoDetalle
    {
        #region Atributos

        private Int16 _intidconcepto;
        private String _stretiquetaconcepto;
        private String _strabreviaturaconcepto;
        private Decimal? _deccantidad;
        private String _strunidad;
        private String _strdescripcion;
        private Decimal? _decpreciounitario;
        private Decimal _importetotal;
        private Int16 _indicadorigv;
        private Decimal _decvalorigv;
        private Int16 _esvisible = 1;

        private Decimal _decvalorventa;

        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;

        #endregion

        #region Propiedades

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsVisible
        {
            get { return _esvisible; }
            set { _esvisible = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ValorIGV
        {
            get { return _decvalorigv; }
            set { _decvalorigv = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdConcepto
        {
            get { return _intidconcepto; }
            set { _intidconcepto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String EtiquetaConcepto
        {
            get { return _stretiquetaconcepto; }
            set { _stretiquetaconcepto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String AbreviaturaConcepto
        {
            get { return _strabreviaturaconcepto; }
            set { _strabreviaturaconcepto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Cantidad
        {
            get { return _deccantidad; }
            set { _deccantidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Unidad
        {
            get { return _strunidad; }
            set { _strunidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? PrecioUnitario
        {
            get { return _decpreciounitario; }
            set { _decpreciounitario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal Importe
        {
            get { return _importetotal; }
            set { _importetotal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IndicadorIGV
        {
            get { return _indicadorigv; }
            set { _indicadorigv = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ValorVenta
        {
            get { return _decvalorventa; }
            set { _decvalorventa = value; }
        }
        #endregion

        #region Constructores

        /// <summary>
        /// 
        /// </summary>
        public clsComprobantePagoDetalle()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        public clsComprobantePagoDetalle(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idconcepto")) this.IdConcepto = (Int16)registro["idconcepto"];
            if (registro.Table.Columns.Contains("etiquetaconcepto")) this.EtiquetaConcepto = (String)registro["etiquetaconcepto"];
            if (registro.Table.Columns.Contains("abreviaturaconcepto")) this.AbreviaturaConcepto = (String)registro["abreviaturaconcepto"];
            if (registro.Table.Columns.Contains("cantidad")) this.Cantidad = Convert.IsDBNull(registro["cantidad"]) ? (Decimal?)null : (Decimal)registro["cantidad"];
            if (registro.Table.Columns.Contains("unidad")) this.Unidad = (String)registro["unidad"];
            if (registro.Table.Columns.Contains("descripcion")) this.Descripcion = (String)registro["descripcion"];
            if (registro.Table.Columns.Contains("preciounitario")) this.PrecioUnitario = Convert.IsDBNull(registro["preciounitario"]) ? (Decimal?)null : (Decimal)registro["preciounitario"];
            if (registro.Table.Columns.Contains("importe")) this.Importe = (Decimal)registro["importe"];
            if (registro.Table.Columns.Contains("indicadorigv")) this.IndicadorIGV = (Int16)registro["indicadorigv"];
            if (registro.Table.Columns.Contains("igv")) this.ValorIGV = Convert.ToDecimal(registro["igv"]);
            if (registro.Table.Columns.Contains("esvisible")) this.EsVisible = Convert.ToInt16(registro["esvisible"]);
            if (registro.Table.Columns.Contains("valorventa")) this.ValorVenta = Convert.ToDecimal(registro["valorventa"]);

            if (registro.Table.Columns.Contains("idempresa")) this.IdEmpresa = Convert.ToInt16(registro["idempresa"]);
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = Convert.ToInt16(registro["idtipodocumento"]);
            if (registro.Table.Columns.Contains("nrodocumento")) this.NroDocumento = registro["nrodocumento"].ToString();
        }

        #endregion
    }

    public class clsListaComprobantePagoDetalle
    {
        private List<clsComprobantePagoDetalle> _lstcomprobantepago = new List<clsComprobantePagoDetalle>();

        #region Constructor

        public clsListaComprobantePagoDetalle()
        { }

        public clsListaComprobantePagoDetalle(DataTable comprobantepago)
        {
            if (comprobantepago == null || comprobantepago.Rows.Count == 0) { return; }

            foreach (DataRow fila in comprobantepago.Rows)
            {
                Elementos.Add(new clsComprobantePagoDetalle(fila));
            }
        }

        #endregion

        #region Propiedades
        public List<clsComprobantePagoDetalle> Elementos
        {
            get { return _lstcomprobantepago; }
            set { _lstcomprobantepago = value; }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsComprobantePagoConvenio
    {
        #region Campos
        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;

        private String _strnombreconvenio;
        private String _strnroconvenio;
        private Decimal _decpagoinicial;
        private Int16 _intnrocuotas;
        private Decimal _decvalorcuota;

        #endregion

        #region Propiedades

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombreConvenio
        {
            get { return _strnombreconvenio; }
            set { _strnombreconvenio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroConvenio
        {
            get { return _strnroconvenio; }
            set { _strnroconvenio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal PagoInicial
        {
            get { return _decpagoinicial; }
            set { _decpagoinicial = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 NroCuotas
        {
            get { return _intnrocuotas; }
            set { _intnrocuotas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ValorCuota
        {
            get { return _decvalorcuota; }
            set { _decvalorcuota = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public clsComprobantePagoConvenio()
        {
        }

        public clsComprobantePagoConvenio(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("nombreconvenio"))
                NombreConvenio = Convert.IsDBNull(dr["nombreconvenio"]) ? "" : dr["nombreconvenio"].ToString().Trim();
            if (dr.Table.Columns.Contains("nroconvenio"))
                NroConvenio = Convert.IsDBNull(dr["nroconvenio"]) ? "" : dr["nroconvenio"].ToString().Trim();
            if (dr.Table.Columns.Contains("pagoinicial"))
                PagoInicial = Convert.IsDBNull(dr["pagoinicial"]) ? Decimal.Zero : Convert.ToDecimal(dr["pagoinicial"]);
            if (dr.Table.Columns.Contains("valorcuota"))
                ValorCuota = Convert.IsDBNull(dr["valorcuota"]) ? Decimal.Zero : Convert.ToDecimal(dr["valorcuota"]);
            if (dr.Table.Columns.Contains("nrocuotas"))
                NroCuotas = Convert.IsDBNull(dr["nrocuotas"]) ? (Int16)0 : Convert.ToInt16(dr["nrocuotas"]);

            if (dr.Table.Columns.Contains("idempresa")) this.IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            if (dr.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            if (dr.Table.Columns.Contains("nrodocumento")) this.NroDocumento = dr["nrodocumento"].ToString();
        }

        #endregion
    }
    /// <summary>
    /// 
    /// </summary>
    public class clsListaComprobantePagoConvenio
    {
        private List<clsComprobantePagoConvenio> _lstcomprobantepago = new List<clsComprobantePagoConvenio>();

        #region Constructor

        public clsListaComprobantePagoConvenio()
        { }

        public clsListaComprobantePagoConvenio(DataTable comprobantepago)
        {
            if (comprobantepago == null || comprobantepago.Rows.Count == 0) { return; }

            foreach (DataRow fila in comprobantepago.Rows)
            {
                Elementos.Add(new clsComprobantePagoConvenio(fila));
            }
        }

        #endregion

        #region Propiedades
        public List<clsComprobantePagoConvenio> Elementos
        {
            get { return _lstcomprobantepago; }
            set { _lstcomprobantepago = value; }
        }
        #endregion
    }
}