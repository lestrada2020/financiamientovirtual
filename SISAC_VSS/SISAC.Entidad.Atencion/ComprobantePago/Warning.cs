﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion.ComprobantePago
{
    public sealed class Warning
    {
        public string Code { get; }
        public string Message { get; }
        public string ObjectName { get; }
        public string ObjectType { get; }
        public Severity Severity { get; }
    }

    public enum Severity
    {
        Warning = 0,
        Error = 1
    }
}
