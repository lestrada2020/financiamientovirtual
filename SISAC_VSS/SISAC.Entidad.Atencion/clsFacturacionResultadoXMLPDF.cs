﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion
{
    public class clsFacturacionResultadoXMLPDF
    {
        public Boolean GeneroXMLPDF { get; set; }

        public String RutaXMLPDF { get; set; }

        public clsFacturacionResultadoXMLPDF()
        { }
    }
}
