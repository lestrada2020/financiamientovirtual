﻿using SISAC.Entidad.Maestro;

namespace SISAC.Entidad.Atencion
{
    #region clsConsultaGeneralFiltro

    public sealed class clsConsultaGeneralFiltro : clsParametro
    {
        #region Property

        public TypeConsultaGeneral TipoConsulta { get; set; }

        #endregion Property
    }

    #endregion clsConsultaGeneralFiltro
}