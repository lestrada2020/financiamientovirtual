﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Data;
using Microsoft.Office.Interop.Word;
using System.IO;

namespace SISAC.Entidad.Atencion
{
    public partial class GeneradorPlantillaUI: Component
    {
        #region Campos

        private clsPlantilla _objplantilla;

        private String _strrutadocumento = "";
        private static object missing = Missing.Value;
        private static object ofalse = false;
        private Word.Application Word_App;
        private Word.Document Word_doc;
        private Int16 _intindicebusqueda;

        #endregion Campos

        #region Propiedades

        public String RutaDocumento
        {
            get { return _strrutadocumento; }
            set { _strrutadocumento = value; }
        }

        #endregion Propiedades

        #region Constructor

        public GeneradorPlantillaUI()
        {
            Word_App = new Word.Application();
            Word_doc = new Word.Document();

            //InitializeComponent();
        }

        public GeneradorPlantillaUI(IContainer container)
        {
            container.Add(this);

            //InitializeComponent();
        }

        #endregion Constructor

        #region Public

        #region Utilitarios Transaccion Extrajudicial

        public static string GenerarNombreFichero()
        {
            int ultimoTick = 0;

            while (ultimoTick == Environment.TickCount)
            { System.Threading.Thread.Sleep(1); }

            ultimoTick = Environment.TickCount;

            return DateTime.Now.ToString("yyyyMMddhhmmss") + "." +
                ultimoTick.ToString();
        }

        /// <summary>
        /// MODIFICAR´PARA GUARDAR LA RUTA EN UNA CARPETA COMPARTIDA
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static String ConvertirArrayBytes_Fichero(Byte[] array, string rutaArchvio, string nroServicio, string nroConvenio)
        {
            //Creamos un array de bytes que contiene los bytes almacenados
            //en el campo Documento de la tabla
            byte[] bits = ((byte[])(array));
            //Vamos a guardar ese array de bytes como un fichero en el
            //disco duro, un fichero temporal que después se podrá descartar.
            //Para evitar problemas de concurrencia de usuarios,
            //generamos un nombre único para el mismo
            string sFile = string.Concat(nroServicio,"_",nroConvenio.Trim(), ".doc");
            //Creamos un nuevo FileStream, que esta vez servirá para
            //crear un fichero con el nombre especificado
            //string sRuta = Server.MapPath(".") + @"\" + sFile;
            //string sRuta = System.Environment.SpecialFolder.CurrentDirectory + @"\" + sFile;
            string sRuta = string.Concat(rutaArchvio,sFile);


            if(File.Exists(sRuta))
            {
                return sRuta;
            }

            FileStream fs = new FileStream(sRuta, FileMode.Create);

            //Y escribimos en disco el array de bytes que conforman
            //el fichero Word
            fs.Write(bits, 0, Convert.ToInt32(bits.Length));
            fs.Close();

            //sRuta = Path.GetFileName(sRuta);

            return sRuta;
        }

        public static String ConvertirArrayBytesImagenJPG(Byte[] array)
        {
            //Creamos un array de bytes que contiene los bytes almacenados
            //en el campo Documento de la tabla
            byte[] bits = ((byte[])(array));
            //Vamos a guardar ese array de bytes como un fichero en el
            //disco duro, un fichero temporal que después se podrá descartar.
            //Para evitar problemas de concurrencia de usuarios,
            //generamos un nombre único para el mismo
            string sFile = "tmp" + GenerarNombreFichero() + ".jpg";
            //Creamos un nuevo FileStream, que esta vez servirá para
            //crear un fichero con el nombre especificado
            string sRuta = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + sFile;
            FileStream fs = new FileStream(sRuta, FileMode.Create);
            //Y escribimos en disco el array de bytes que conforman
            //el fichero JPG
            fs.Write(bits, 0, Convert.ToInt32(bits.Length));
            fs.Close();

            return sRuta;
        }

        #endregion Utilitarios Transaccion Extrajudicial
        /// <summary>
        /// ImprimirDocumentoPlantillaDosFirmas
        /// </summary>
        /// <param name="IdTipoDocumento"></param>
        /// <param name="IdServicio"></param>
        /// <param name="IdFlujo"></param>
        /// <param name="argListaParametros"></param>
        /// <param name="Imprimir"></param>
        /// <returns></returns>
        public string ImprimirDocumentoPlantillaDosFirmas
            (
             clsListaCampoDato argListaParametros
            , Boolean Imprimir
            , Byte[] _Plantilla
            , string rutaArchivo
            ,string nroServicio
            , string nroConvenio
            )
        {
            //Byte[] _Plantilla;
            Boolean _DocumentoOK = false;
            string RutaDocumentoPdf="";

            try
            {
                object rute = ConvertirArrayBytes_Fichero(_Plantilla,rutaArchivo,nroServicio,nroConvenio);
                RutaDocumento = (String)rute;

                /// Abrimos el fichero generado.
                Word_App.Documents.Open(ref rute, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing);
                Word_App.Visible = false;
                Word_doc = Word_App.ActiveDocument;

                

                Int16 intindicetabla = 0;

                for (Int16 k = 1; k < Word_doc.Tables.Count; k++)
                {
                    if (Word_doc.Tables[k].Columns.Count == 2)
                        intindicetabla = k;
                }

                foreach (clsCampoDato campodato in argListaParametros.Elementos)
                {
                    object textobuscar = "[" + campodato.Campo.Trim() + "]";
                    object textoreemplazar = campodato.Dato.Trim();

                    object wrap = Word.WdFindWrap.wdFindContinue;
                    object replace = Word.WdReplace.wdReplaceAll;

                    /// bucle que ejecuta busqueda y reemplazo. Encuentra un valor y reemplaza
                    /// todas sus instancias por el parametro definido.
                    while (Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref missing,
                        ref missing, ref missing, ref missing, ref missing, ref missing))
                    {
                        if (campodato.Campo == "Firma")
                        {
                            /*
                            String[] datosFirma = new String[campodato.Dato.Split('/').Length];
                            datosFirma = campodato.Dato.Split('/');

                            if (datosFirma[0].ToString().Trim() == "FIRMA")
                            {
                                for (int i = 1; i < datosFirma.Length; i++)
                                {
                            */
                                    //Obtener archivo de firma
                                    Object _rutafirma = ConvertirArrayBytesImagenJPG(campodato.Imagen);

                                    Object vacio = "";
                                    Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                                        ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref vacio,
                                        ref replace, ref missing, ref missing, ref missing, ref missing);

                                    Range imagen = Word_doc.Tables[1].Range;
                                    imagen.Cells[1].Range.InlineShapes.AddPicture(_rutafirma.ToString(), ref missing, ref missing, ref missing);
                            /*    
                                }
                            }
                            */
                        }
                        else if (campodato.Campo == "FirmaA")
                        {
                            /*
                            String[] datosFirma = new String[campodato.Dato.Split('/').Length];
                            datosFirma = campodato.Dato.Split('/');

                            if (datosFirma[0].ToString().Trim() == "FIRMAA")
                            {
                                for (int i = 1; i < datosFirma.Length; i++)
                                {
                            */
                                    //Obtener archivo de firma
                                    Object _rutafirma = ConvertirArrayBytesImagenJPG(campodato.Imagen);

                                    Object vacio = "";
                                    Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                                        ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref vacio,
                                        ref replace, ref missing, ref missing, ref missing, ref missing);

                                    Range imagen = Word_doc.Tables[intindicetabla].Range;
                                    imagen.Cells[1].Range.InlineShapes.AddPicture(_rutafirma.ToString(), ref missing, ref missing, ref missing);
                        /*        
                                }
                            }
                        */
                        }
                        else if (campodato.Campo == "FirmaB")
                        {
                            /*
                            String[] datosFirma = new String[campodato.Dato.Split('/').Length];
                            datosFirma = campodato.Dato.Split('/');

                            if (datosFirma[0].ToString().Trim() == "FIRMAB")
                            {
                                for (int i = 1; i < datosFirma.Length; i++)
                                {
                            */
                                    //Obtener archivo de firma
                                    Object _rutafirma = ConvertirArrayBytesImagenJPG(campodato.Imagen);

                                    Object vacio = "";
                                    Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                                        ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref vacio,
                                        ref replace, ref missing, ref missing, ref missing, ref missing);

                                    Range imagen = Word_doc.Tables[intindicetabla].Range;
                                    imagen.Cells[2].Range.InlineShapes.AddPicture(_rutafirma.ToString(), ref missing, ref missing, ref missing);
                            /*
                                }
                            }
                            */
                        }
                        else
                        {
                            if (textoreemplazar.ToString().Length > 100)
                            {
                                Decimal total = Convert.ToDecimal((Decimal)textoreemplazar.ToString().Length / (Decimal)100);
                                Int16 bloques;
                                bloques = Convert.ToInt16(total.ToString().Split('.')[0]);
                                if (Convert.ToInt16(total.ToString().Split('.')[1]) > 0)
                                {
                                    bloques++;
                                }

                                for (int i = 0; i < bloques; i++)
                                {
                                    Object texto = new Object();

                                    if (i == bloques - 1)
                                    {
                                        texto = textoreemplazar.ToString().Substring(i * 100);
                                    }
                                    else
                                    {
                                        texto = textoreemplazar.ToString().Substring(i * 100, 100)
                                                                        + textobuscar.ToString();
                                    }
                                    Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                                        ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref texto,
                                        ref replace, ref missing, ref missing, ref missing, ref missing);
                                }
                            }
                            else
                            {
                                Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                                    ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref textoreemplazar,
                                    ref replace, ref missing, ref missing, ref missing, ref missing);
                            }
                        }
                    }
                }

                /// guardamos los cambios.
                object indice = 1;
                object myBool = Word.WdSaveOptions.wdDoNotSaveChanges;

                Word_doc.Content.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;
                Word_doc.Content.PageSetup.LeftMargin = (float)57.00;
                Word_doc.Content.PageSetup.RightMargin = (float)57.00;
                Word_doc.Content.PageSetup.TopMargin = (float)57.00;
                Word_doc.Content.PageSetup.BottomMargin = (float)57.00;

                if (Imprimir) // siempre envia falso NO ENTRA
                {
                    Word_App.Documents.get_Item(ref indice).PrintOut(ref missing, ref missing, ref missing, ref missing, ref missing,
                                                                    ref missing, ref missing, ref missing, ref missing, ref missing,
                                                                    ref missing, ref missing, ref missing, ref missing, ref missing,
                                                                    ref missing, ref missing, ref missing);

                    Word_App.ActiveDocument.Close(ref myBool, ref missing, ref missing);
                }
                else
                {
                    Word_App.Documents.get_Item(ref indice).Save();

                    /// visualizamos el documento.

                }
               
                
                Object oMissing = System.Reflection.Missing.Value;

                Word_doc.Save();

                RutaDocumentoPdf = RutaDocumento.Replace(".doc", ".pdf");

                Word_doc.ExportAsFixedFormat(RutaDocumentoPdf, WdExportFormat.wdExportFormatPDF, false, WdExportOptimizeFor.wdExportOptimizeForOnScreen,
                    WdExportRange.wdExportAllDocument, 1, 1, WdExportItem.wdExportDocumentContent, true, true,
                    WdExportCreateBookmarks.wdExportCreateHeadingBookmarks, true, true, false, ref oMissing);
                Word_doc.Close();

                File.Delete(RutaDocumento);

                //ruta documento a mostar
                //RutaDocumento
            }
            catch (Exception ex)
            {
                if (!String.IsNullOrEmpty(RutaDocumento) && !Imprimir && _DocumentoOK == true)
                {
                    File.Delete(RutaDocumento);
                }

                throw;
            }

            finally
            {
                if (!String.IsNullOrEmpty(RutaDocumento) && Imprimir == true && _DocumentoOK == true)
                {
                    File.Delete(RutaDocumento);
                }
            }

            RutaDocumentoPdf = Path.GetFileName(RutaDocumentoPdf);
            return RutaDocumentoPdf;
        }
      
        #endregion Public

        #region Private

        //Agregar campo a plantilla
        private void AgregarCampo(string campo, string dato)
        {
            object textobuscar = "[" + campo.Trim() + "]";
            object textoreemplazar = dato.Trim();

            object wrap = Word.WdFindWrap.wdFindContinue;
            object replace = Word.WdReplace.wdReplaceAll;

            /// bucle que ejecuta busqueda y reemplazo. Encuentra un valor y reemplaza
            /// todas sus instancias por el parametro definido.
            while (Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref missing))
            {
                Word_doc.Content.Find.Execute(ref textobuscar, ref missing, ref missing,
                ref missing, ref missing, ref missing, ref missing, ref wrap, ref missing, ref textoreemplazar,
                ref replace, ref missing, ref missing, ref missing, ref missing);
            }
        }

        #endregion Private

        #region Protected

        protected Object ObtenerFinRango(Range dataRange)
        {
            Object inicio = dataRange.End - 1;
            Object fin = dataRange.End - 1;
            Range rng = Word_doc.Range(ref inicio, ref fin);
            Object rango = rng;
            return rango;
        }

        protected void CrearMarcaPagina(String nombreMarcaPagina, Object rangoMarca)
        {
            Word_doc.Bookmarks.Add(nombreMarcaPagina, ref rangoMarca);
        }

        #endregion Protected
    }

    public class clsPlantilla : clsEntidadMaestro
    {
        #region Miembros Privados - Campos

        private Int16 _intidplantilla;
        private Int16 _intidservicioflujo;
        private Int16 _intidservicio;
        private Int16 _intidflujo;
        private String _strnombreplantilla;
        private String _strnombreservicio;
        private String _strnombreflujo;
        private String _strabreviaplantilla;
        private DateTime _datfecharegistro;
        private String _strrutaplantilla;
        private Byte[] _strplantilla;
        private Int16 _intidestado;
        //private HybridDictionary _listabuscarreemplazar = new HybridDictionary();
        private Int16 _intidtipodocumento;
        private String _strnombredocumento;

        #endregion

        #region Miembros Públicos - Propiedades

        /// <summary>
        /// 
        /// </summary>
        /// 
        public Int16 IdServicioFlujo
        {
            get { return _intidservicioflujo; }
            set { _intidservicioflujo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public Int16 IdFlujo
        {
            get { return _intidflujo; }
            set { _intidflujo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public String NombreServicio
        {
            get { return _strnombreservicio; }
            set { _strnombreservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public String NombreFlujo
        {
            get { return _strnombreflujo; }
            set { _strnombreflujo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public String AbreviaPlantilla
        {
            get { return _strabreviaplantilla; }
            set { _strabreviaplantilla = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public Byte[] Plantilla
        {
            get { return _strplantilla; }
            set { _strplantilla = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }
        ///// <summary>
        ///// 
        ///// </summary>
        //public HybridDictionary ListaBuscarReemplazar
        //{
        //    get { return _listabuscarreemplazar; }
        //    set { _listabuscarreemplazar = value; }
        //}

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NombreDocumento
        {
            get { return _strnombredocumento; }
            set { _strnombredocumento = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsPlantilla()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsPlantilla(DataRow dr)
        {
            if (dr == null) { return; }

            DataColumnCollection col = dr.Table.Columns;

            if (col.Contains("idplantilla")) { Id = Convert.ToInt16(dr["idplantilla"]); }
            if (col.Contains("idservicioflujo")) { IdServicioFlujo = Convert.ToInt16(dr["idservicioflujo"]); }
            if (col.Contains("idservicio")) { IdServicio = Convert.ToInt16(dr["idservicio"]); }
            if (col.Contains("idflujo")) { IdFlujo = Convert.ToInt16(dr["idflujo"]); }
            if (col.Contains("nombreplantilla")) { Nombre = dr["nombreplantilla"].ToString(); }
            if (col.Contains("nombreservicio")) { NombreServicio = dr["nombreservicio"].ToString(); }
            if (col.Contains("nombreflujo")) { NombreFlujo = dr["nombreflujo"].ToString(); }
            if (col.Contains("abreviaplantilla")) { AbreviaPlantilla = dr["abreviaplantilla"].ToString(); }
            if (col.Contains("fecharegistro")) { FechaRegistro = Convert.ToDateTime(dr["fecharegistro"]); }
            if (col.Contains("plantilla")) { Plantilla = (Byte[])(dr["plantilla"]); }
            if (col.Contains("idestado")) { IdEstado = Convert.ToInt16(dr["idestado"]); }
            if (col.Contains("idtipodocumento")) { IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]); }
            if (col.Contains("nombredocumento")) { NombreDocumento = dr["nombredocumento"].ToString(); }
        }
        #endregion
    }


}
