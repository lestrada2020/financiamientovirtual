﻿using SISAC.Entidad.Maestro;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion
{
    //Entidades para el orden cobro
    public class enteDatoSuministro
    {
        #region Properties
        public Int16 IdNivelTension { get; set; }
        public Int16 Estado { get; set; }
        public Int16 EstadoFacturacion { get; set; }
        public String NombreNroservicio { get; set; }
        public String Direccion { get; set; }
        public String NombreEstado { get; set; }
        public Int16 IdTipoIdentidad { get; set; }
        public String NroIdentidad { get; set; }
        public Int16 IdTipoIdentidadPropietario { get; set; }
        public String NroIdentidadPropietario { get; set; }
        public String NombreCartera { get; set; }
        public String Tarifa { get; set; }

        #endregion

        #region Constructors
        public enteDatoSuministro()
        {

        }
        public enteDatoSuministro(DataRow row)
        {
            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdNivelTension") && !row.IsNull("IdNivelTension")) IdNivelTension = Convert.ToInt16(row["IdNivelTension"]);
            if (columns.Contains("estado") && !row.IsNull("estado")) Estado = Convert.ToInt16(row["estado"]);
            if (columns.Contains("estadofacturacion") && !row.IsNull("estadofacturacion")) EstadoFacturacion = Convert.ToInt16(row["estadofacturacion"]);
            if (columns.Contains("nombrenroservicio") && !row.IsNull("nombrenroservicio")) NombreNroservicio = Convert.ToString(row["nombrenroservicio"]);
            if (columns.Contains("direccion") && !row.IsNull("direccion")) Direccion = Convert.ToString(row["direccion"]);
            if (columns.Contains("nombreestado") && !row.IsNull("nombreestado")) NombreEstado = Convert.ToString(row["nombreestado"]);
            if (columns.Contains("idtipoidentidad") && !row.IsNull("idtipoidentidad")) IdTipoIdentidad = Convert.ToInt16(row["idtipoidentidad"]);
            if (columns.Contains("nroidentidad") && !row.IsNull("nroidentidad")) NroIdentidad = Convert.ToString(row["nroidentidad"]);
            if (columns.Contains("idtipoidentidadpropietario") && !row.IsNull("idtipoidentidadpropietario")) IdTipoIdentidadPropietario = Convert.ToInt16(row["idtipoidentidadpropietario"]);
            if (columns.Contains("nroidentidadpropietario") && !row.IsNull("nroidentidadpropietario")) NroIdentidadPropietario = Convert.ToString(row["nroidentidadpropietario"]);
            if (columns.Contains("nombrecartera") && !row.IsNull("nombrecartera")) NombreCartera = Convert.ToString(row["nombrecartera"]);
            if (columns.Contains("tarifa") && !row.IsNull("tarifa")) Tarifa = Convert.ToString(row["tarifa"]);
        }

        #endregion

    }


    public class enteOrdenDeCobro
    {
        #region Campos

        private Int32 _intidordencobro;
        private Int32 _intidusuarioautoriza;
        private DateTime _datfechaautoriza;
        private Int32? _intidusuariocaja;
        private DateTime? _datfechacaja;
        private enumMovimientoComercial _intidmovimientocomercial;
        private String _stridnromovimientocomercial;
        private Int32 _intidnroservicio;
        private Decimal _decimporte;
        private Int16 _intidmoneda;
        private Int32? _intidsolicitud;
        private String _stridcortereconexion;
        private EstadoGenerales _intidestado;
        private String _stroslastlogin;
        private DateTime _datoslastdate;
        private String _stroslastapp;
        private String _strosfirstlogin;
        private DateTime _datosfirstdate;
        private String _strosfirstapp;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 IdUsuarioAutoriza
        {
            get { return _intidusuarioautoriza; }
            set { _intidusuarioautoriza = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaAutoriza
        {
            get { return _datfechaautoriza; }
            set { _datfechaautoriza = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32? IdUsuarioCaja
        {
            get { return _intidusuariocaja; }
            set { _intidusuariocaja = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? FechaCaja
        {
            get { return _datfechacaja; }
            set { _datfechacaja = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public enumMovimientoComercial IdMovimientoComercial
        {
            get { return _intidmovimientocomercial; }
            set { _intidmovimientocomercial = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String IdNroMovimientoComercial
        {
            get { return _stridnromovimientocomercial; }
            set { _stridnromovimientocomercial = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32? IdSolicitud
        {
            get { return _intidsolicitud; }
            set { _intidsolicitud = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String IdCorteReconexion
        {
            get { return _stridcortereconexion; }
            set { _stridcortereconexion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public EstadoGenerales IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osLastLogin
        {
            get { return _stroslastlogin; }
            set { _stroslastlogin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime osLastDate
        {
            get { return _datoslastdate; }
            set { _datoslastdate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osLastApp
        {
            get { return _stroslastapp; }
            set { _stroslastapp = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osFirstLogin
        {
            get { return _strosfirstlogin; }
            set { _strosfirstlogin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime osFirstDate
        {
            get { return _datosfirstdate; }
            set { _datosfirstdate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osFirstApp
        {
            get { return _strosfirstapp; }
            set { _strosfirstapp = value; }
        }


        #endregion

        #region Propiedades de Asociación

        private enteListaOrdenCobroDocumento _listaOrdenCobroDocumento;

        public enteListaOrdenCobroDocumento ListaOrdenCobroDocumento
        {
            get { return _listaOrdenCobroDocumento; }
            set { _listaOrdenCobroDocumento = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public enteOrdenDeCobro()
        {
            ListaOrdenCobroDocumento = new enteListaOrdenCobroDocumento();
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public enteOrdenDeCobro(DataRow dr)
        {
            if (dr == null) { return; }

            IdOrdenCobro = Convert.ToInt32(dr["idordencobro"]);
            IdUsuarioAutoriza = Convert.ToInt32(dr["idusuarioautoriza"]);
            FechaAutoriza = Convert.ToDateTime(dr["fechaautoriza"]);
            IdUsuarioCaja = Convert.IsDBNull(dr["idusuariocaja"]) ? (Int32?)null : Convert.ToInt32(dr["idusuariocaja"]);
            FechaCaja = Convert.IsDBNull(dr["fechacaja"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechacaja"]);
            IdMovimientoComercial = (enumMovimientoComercial)Convert.ToInt16(dr["idmovimientocomercial"]);
            IdNroMovimientoComercial = dr["idnromovimientocomercial"].ToString();
            IdNroServicio = Convert.ToInt32(dr["idnroservicio"]);
            Importe = Convert.ToDecimal(dr["importe"]);
            IdMoneda = Convert.ToInt16(dr["idmoneda"]);
            IdSolicitud = Convert.IsDBNull(dr["idsolicitud"]) ? (Int32?)null : Convert.ToInt32(dr["idsolicitud"]);
            IdCorteReconexion = dr["idcortereconexion"].ToString();
            IdEstado = (EstadoGenerales)Convert.ToInt16(dr["idestado"]);
            osLastLogin = dr["oslastlogin"].ToString();
            osLastDate = Convert.ToDateTime(dr["oslastdate"]);
            osLastApp = dr["oslastapp"].ToString();
            osFirstLogin = dr["osfirstlogin"].ToString();
            osFirstDate = Convert.ToDateTime(dr["osfirstdate"]);
            osFirstApp = dr["osfirstapp"].ToString();
        }

        #endregion

    }

    public class enteListaOrdenDeCobro
    {
        #region Campos

        private List<enteOrdenDeCobro> _objelementos = new List<enteOrdenDeCobro>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<enteOrdenDeCobro> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public enteListaOrdenDeCobro()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public enteListaOrdenDeCobro(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new enteOrdenDeCobro(_drw));
            }
        }

        #endregion

    }

    public class enteOrdenCobroDocumento
    {
        #region Campos

        private Int32 _intidordencobrodocumento;
        private Int32 _intidordencobro;
        private String _strdescripcion;
        private Int16 _intidempresa;
        private TipoDocumentoComercial _intidtipodocumento;
        private String _strnrodocumento;
        private Decimal _decimporte;
        private Int16 _intidmoneda;
        private Int16 _intescobrable;
        private String _stroslastlogin;
        private DateTime _datoslastdate;
        private String _stroslastapp;
        private String _strosfirstlogin;
        private DateTime _datosfirstdate;
        private String _strosfirstapp;
        private Int16 _intesdocumentofavor;

        private enteListaOrdenCobroDocumentoDetalle _listaDetalleDocumento;

        public enteListaOrdenCobroDocumentoDetalle ListaDetalleDocumento
        {
            get { return _listaDetalleDocumento; }
            set { _listaDetalleDocumento = value; }
        }

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobroDocumento
        {
            get { return _intidordencobrodocumento; }
            set { _intidordencobrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public TipoDocumentoComercial IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsCobrable
        {
            get { return _intescobrable; }
            set { _intescobrable = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osLastLogin
        {
            get { return _stroslastlogin; }
            set { _stroslastlogin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime osLastDate
        {
            get { return _datoslastdate; }
            set { _datoslastdate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osLastApp
        {
            get { return _stroslastapp; }
            set { _stroslastapp = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osFirstLogin
        {
            get { return _strosfirstlogin; }
            set { _strosfirstlogin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime osFirstDate
        {
            get { return _datosfirstdate; }
            set { _datosfirstdate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osFirstApp
        {
            get { return _strosfirstapp; }
            set { _strosfirstapp = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsDocumentoFavor
        {
            get { return _intesdocumentofavor; }
            set { _intesdocumentofavor = value; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public enteOrdenCobroDocumento()
        {
            ListaDetalleDocumento = new enteListaOrdenCobroDocumentoDetalle();
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public enteOrdenCobroDocumento(DataRow dr)
        {
            if (dr == null) { return; }

            IdOrdenCobroDocumento = Convert.ToInt32(dr["idordencobrodocumento"]);
            IdOrdenCobro = Convert.ToInt32(dr["idordencobro"]);
            Descripcion = dr["descripcion"].ToString();
            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdTipoDocumento = (TipoDocumentoComercial)Convert.ToInt16(dr["idtipodocumento"]);
            NroDocumento = dr["nrodocumento"].ToString();
            Importe = Convert.ToDecimal(dr["importe"]);
            IdMoneda = Convert.ToInt16(dr["idmoneda"]);
            EsCobrable = Convert.ToInt16(dr["escobrable"]);
            osLastLogin = dr["oslastlogin"].ToString();
            osLastDate = Convert.ToDateTime(dr["oslastdate"]);
            osLastApp = dr["oslastapp"].ToString();
            osFirstLogin = dr["osfirstlogin"].ToString();
            osFirstDate = Convert.ToDateTime(dr["osfirstdate"]);
            osFirstApp = dr["osfirstapp"].ToString();
            EsDocumentoFavor = Convert.ToInt16(dr["esdocumentofavor"]);
        }

        #endregion

    }


    public class enteListaOrdenCobroDocumento
    {
        #region Campos

        private List<enteOrdenCobroDocumento> _objelementos = new List<enteOrdenCobroDocumento>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<enteOrdenCobroDocumento> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public enteListaOrdenCobroDocumento()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public enteListaOrdenCobroDocumento(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new enteOrdenCobroDocumento(_drw));
            }
        }

        #endregion

    }

    public class enteOrdenCobroDocumentoDetalle
    {
        #region Campos

        private Int32 _intidordencobrodocumento;
        private ConceptosFacturar _intidconcepto;
        private TipoDocumentoComercial _intidtipodocumentosustento;
        private Decimal _decimporte;
        private Decimal _decpreciounitario;
        private Decimal _deccantidad;
        private Boolean _intindicadorigv;
        private String _strdescripcionconcepto;
        private String _stroslastlogin;
        private DateTime _datoslastdate;
        private String _stroslastapp;
        private String _strosfirstlogin;
        private DateTime _datosfirstdate;
        private String _strosfirstapp;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobroDocumento
        {
            get { return _intidordencobrodocumento; }
            set { _intidordencobrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ConceptosFacturar IdConcepto
        {
            get { return _intidconcepto; }
            set { _intidconcepto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public TipoDocumentoComercial IdTipoDocumentoSustento
        {
            get { return _intidtipodocumentosustento; }
            set { _intidtipodocumentosustento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal PrecioUnitario
        {
            get { return _decpreciounitario; }
            set { _decpreciounitario = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Cantidad
        {
            get { return _deccantidad; }
            set { _deccantidad = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IndicadorIGV
        {
            get { return _intindicadorigv; }
            set { _intindicadorigv = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DescripcionConcepto
        {
            get { return _strdescripcionconcepto; }
            set { _strdescripcionconcepto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osLastLogin
        {
            get { return _stroslastlogin; }
            set { _stroslastlogin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime osLastDate
        {
            get { return _datoslastdate; }
            set { _datoslastdate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osLastApp
        {
            get { return _stroslastapp; }
            set { _stroslastapp = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osFirstLogin
        {
            get { return _strosfirstlogin; }
            set { _strosfirstlogin = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime osFirstDate
        {
            get { return _datosfirstdate; }
            set { _datosfirstdate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String osFirstApp
        {
            get { return _strosfirstapp; }
            set { _strosfirstapp = value; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public enteOrdenCobroDocumentoDetalle()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public enteOrdenCobroDocumentoDetalle(DataRow dr)
        {
            if (dr == null) { return; }

            IdOrdenCobroDocumento = Convert.ToInt32(dr["idordencobrodocumento"]);
            IdConcepto = (ConceptosFacturar)Convert.ToInt16(dr["idconcepto"]);
            IdTipoDocumentoSustento = (TipoDocumentoComercial)Convert.ToInt16(dr["idtipodocumentosustento"]);
            Importe = Convert.ToDecimal(dr["importe"]);
            PrecioUnitario = Convert.ToDecimal(dr["preciounitario"]);
            Cantidad = Convert.ToDecimal(dr["cantidad"]);
            IndicadorIGV = Convert.ToBoolean(dr["indicadorigv"]);
            DescripcionConcepto = dr["descripcionconcepto"].ToString();
            osLastLogin = dr["oslastlogin"].ToString();
            osLastDate = Convert.ToDateTime(dr["oslastdate"]);
            osLastApp = dr["oslastapp"].ToString();
            osFirstLogin = dr["osfirstlogin"].ToString();
            osFirstDate = Convert.ToDateTime(dr["osfirstdate"]);
            osFirstApp = dr["osfirstapp"].ToString();
        }

        #endregion

    }

    public class enteListaOrdenCobroDocumentoDetalle
    {
        #region Campos

        private List<enteOrdenCobroDocumentoDetalle> _objelementos = new List<enteOrdenCobroDocumentoDetalle>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<enteOrdenCobroDocumentoDetalle> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public enteListaOrdenCobroDocumentoDetalle()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public enteListaOrdenCobroDocumentoDetalle(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new enteOrdenCobroDocumentoDetalle(_drw));
            }
        }

        #endregion

    }

    public class enteNroServicioIntereses
    {
        private short _idConcepto;
        private string _nombreConcepto;
        private decimal _importe;
        private bool _esTributable;
        private decimal _tasaImpuesto;

        public decimal TasaImpuesto
        {
            get { return _tasaImpuesto; }
            set { _tasaImpuesto = value; }
        }

        public short IdConcepto
        {
            get { return _idConcepto; }
            set { _idConcepto = value; }
        }

        public string NombreConcepto
        {
            get { return _nombreConcepto; }
            set { _nombreConcepto = value; }
        }

        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; }
        }

        public bool EsTributable
        {
            get { return _esTributable; }
            set { _esTributable = value; }
        }

        public enteNroServicioIntereses() { }

        public enteNroServicioIntereses(System.Data.DataRow drrregistro)
        {
            if (drrregistro == null) return;

            this._idConcepto = Convert.ToInt16(drrregistro["idconcepto"]);
            this._importe = Convert.ToDecimal(drrregistro["importe"]);
            this._nombreConcepto = drrregistro["Nombre"].ToString();
            this._esTributable = Convert.ToBoolean(drrregistro["estributable"]);
            this._tasaImpuesto = Convert.ToDecimal(drrregistro["tasaimpuesto"]);
        }

        public enteNroServicioIntereses(ConceptosFacturar concepto, decimal importe)
        {
            this._idConcepto = Convert.ToInt16(concepto);
            this._nombreConcepto = concepto.ToString();
            this._importe = importe;
        }

    }

    public class enteListaNroServicioIntereses
    {
        private List<enteNroServicioIntereses> _elementos = new List<enteNroServicioIntereses>();
        private decimal _totalIntereses = 0;

        public decimal TotalIntereses
        {
            get { return _totalIntereses; }
            set { _totalIntereses = value; }
        }

        public List<enteNroServicioIntereses> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        public enteListaNroServicioIntereses() { }

        public enteListaNroServicioIntereses(System.Data.DataTable dtlistaentidad)
        {
            if (dtlistaentidad == null || dtlistaentidad.Rows.Count == 0) return;

            enteNroServicioIntereses ointeres;
            enteNroServicioIntereses oimpuesto;

            foreach (DataRow drregistro in dtlistaentidad.Rows)
            {
                ointeres = new enteNroServicioIntereses(drregistro);

                _elementos.Add(ointeres);

                _totalIntereses += ointeres.Importe;

                // Agregar Impuesto.
                if (ointeres.EsTributable)
                {
                    oimpuesto = new enteNroServicioIntereses
                        (ConceptosFacturar.IGV
                        , Math.Round((ointeres.Importe * ointeres.TasaImpuesto / Convert.ToDecimal(100)), 2));

                    _elementos.Add(oimpuesto);
                    _totalIntereses += oimpuesto.Importe;
                }
            }
        }

        public enteListaNroServicioIntereses(System.Data.DataTable dtlistaentidad, int argAfectoIGV)
        {
            if (dtlistaentidad == null || dtlistaentidad.Rows.Count == 0) return;

            enteNroServicioIntereses ointeres;
            enteNroServicioIntereses oimpuesto;

            foreach (DataRow drregistro in dtlistaentidad.Rows)
            {
                ointeres = new enteNroServicioIntereses(drregistro);

                _elementos.Add(ointeres);

                _totalIntereses += ointeres.Importe;

                // Agregar Impuesto.
                if (ointeres.EsTributable & argAfectoIGV == 1)
                {
                    oimpuesto = new enteNroServicioIntereses
                        (ConceptosFacturar.IGV
                        , Math.Round((ointeres.Importe * ointeres.TasaImpuesto / Convert.ToDecimal(100)), 2));

                    _elementos.Add(oimpuesto);
                    _totalIntereses += oimpuesto.Importe;
                }
            }
        }
    }
    public class EOrdenCobroResultadoVE
    {
        #region Campos
        private Int32 _intidordencobro;
        private Int32 _intidnroservicio;
        private String _strnroconvenio;
        private Int32? _intidregistroconvenio;
        private Int32? _intidordencobropresupuesto;
        private Int16? _intidtipoconvenio;

        private Int32? _intidpresupuesto;
        private String _strnropresupuesto;
        #endregion

        #region Propiedades 

        public Int32 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }


        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }


        public String NroConvenio
        {
            get { return _strnroconvenio; }
            set { _strnroconvenio = value; }
        }


        public Int32? IdRegistroConvenio
        {
            get { return _intidregistroconvenio; }
            set { _intidregistroconvenio = value; }
        }


        public Int32? IdOrdenCobroPresupuesto
        {
            get { return _intidordencobropresupuesto; }
            set { _intidordencobropresupuesto = value; }
        }


        public Int16? IdTipoConvenio
        {
            get { return _intidtipoconvenio; }
            set { _intidtipoconvenio = value; }
        }


        public Int32? IdPresupuesto
        {
            get { return _intidpresupuesto; }
            set { _intidpresupuesto = value; }
        }


        public String NroPresupuesto
        {
            get { return _strnropresupuesto; }
            set { _strnropresupuesto = value; }
        }
        #endregion

        #region Constructor
        public EOrdenCobroResultadoVE()
        { }

        public EOrdenCobroResultadoVE(DataRow dr)
        {
            if (dr == null) return;

            IdOrdenCobro = Convert.ToInt32(dr["idordencobro"]);
            IdNroServicio = Convert.ToInt32(dr["idnroservicio"]);
            if (dr.Table.Columns.Contains("nroconvenio"))
                NroConvenio = dr["nroconvenio"] == null ? "" : dr["nroconvenio"].ToString();
            if (dr.Table.Columns.Contains("idregistroconvenio"))
                IdRegistroConvenio = Convert.IsDBNull(dr["idregistroconvenio"]) ? (Int32?)null : Convert.ToInt32(dr["idregistroconvenio"]);
            if (dr.Table.Columns.Contains("idordencobropresupuesto"))
                IdOrdenCobroPresupuesto = Convert.IsDBNull(dr["idordencobropresupuesto"]) ? (Int32?)null : Convert.ToInt32(dr["idordencobropresupuesto"]);
            if (dr.Table.Columns.Contains("idtipoconvenio"))
                IdTipoConvenio = Convert.IsDBNull(dr["idtipoconvenio"]) ? (Int16?)null : Convert.ToInt16(dr["idtipoconvenio"]);
            if (dr.Table.Columns.Contains("idpresupuesto"))
                IdPresupuesto = Convert.IsDBNull(dr["idpresupuesto"]) ? (Int32?)null : Convert.ToInt32(dr["idpresupuesto"]);
            if (dr.Table.Columns.Contains("nropresupuesto"))
                NroPresupuesto = Convert.IsDBNull(dr["nropresupuesto"]) ? "" : dr["nropresupuesto"].ToString();
        }
        #endregion

    }

    public class EListaOrdenCobroResultadoVE
    {
        private List<EOrdenCobroResultadoVE> objElementos = new List<EOrdenCobroResultadoVE>();

        public List<EOrdenCobroResultadoVE> Elementos
        {
            get { return objElementos; }
            set { objElementos = value; }
        }

        public EListaOrdenCobroResultadoVE()
        { }
    }


    #region NroServicioDeuda

    public class enteNroServicioDeuda
    {
        #region Campos

        private int _idNroServicio;
        private string _nombre;
        private string _direccion;
        private EstadoGenerales _idEstado;
        private short _idNivelTension;
        private string _mensajeResultado;
        private short _idTipoIdentidad;
        private string _nroIdentidad;
        private short _idTipoIdentidadPropietario;
        private string _nroIdentidadPropietario;
        private string _nombreCartera;
        private string _tarifa;

        #endregion

        #region campos y propiedades de asociacion

        private enteListaNroServicioDeudaFacturada _listaDeudaFacturada = new enteListaNroServicioDeudaFacturada();
        private enteListaNroServicioDeudaPorFacturar _listaDeudaPorFacturar = new enteListaNroServicioDeudaPorFacturar();
        private enteListaNroServicioDocumentoAFavor _listaDocumentosAFavor = new enteListaNroServicioDocumentoAFavor();
        private enteListaNroServicioIntereses _listaIntereses = new enteListaNroServicioIntereses();
        private enteListaNroServicioAComercial _listaAComercial = new enteListaNroServicioAComercial();
        private clsListaConvenioAutorizado _listaAutorizado;

        public enteListaNroServicioDeudaFacturada ListaDeudaFacturada
        {
            get { return _listaDeudaFacturada; }
            set { _listaDeudaFacturada = value; }
        }

        public enteListaNroServicioDeudaPorFacturar ListaDeudaPorFacturar
        {
            get { return _listaDeudaPorFacturar; }
            set { _listaDeudaPorFacturar = value; }
        }

        public enteListaNroServicioDocumentoAFavor ListaDocumentosAFavor
        {
            get { return _listaDocumentosAFavor; }
            set { _listaDocumentosAFavor = value; }
        }

        public enteListaNroServicioIntereses ListaIntereses
        {
            get { return _listaIntereses; }
            set { _listaIntereses = value; }
        }

        public enteListaNroServicioAComercial ListaAComercial
        {
            get { return _listaAComercial; }
            set { _listaAComercial = value; }
        }

        public clsListaConvenioAutorizado ListaAutorizado
        {
            get { return _listaAutorizado; }
            set { _listaAutorizado = value; }
        }
        #endregion

        #region Propiedades

        public int IdNroServicio
        {
            get { return _idNroServicio; }
            set { _idNroServicio = value; }
        }

        public short IdTipoIdentidad
        {
            get { return _idTipoIdentidad; }
            set { _idTipoIdentidad = value; }
        }

        public string NroIdentidad
        {
            get { return _nroIdentidad; }
            set { _nroIdentidad = value; }
        }

        public short IdTipoIdentidadPropietario
        {
            get { return _idTipoIdentidadPropietario; }
            set { _idTipoIdentidadPropietario = value; }
        }

        public string NroIdentidadPropietario
        {
            get { return _nroIdentidadPropietario; }
            set { _nroIdentidadPropietario = value; }
        }

        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        public EstadoGenerales IdEstado
        {
            get { return _idEstado; }
            set { _idEstado = value; }
        }

        public string NombreSituacion
        {
            get { return this._idEstado.ToString(); }
        }

        public int NroMesesDeuda
        {
            get { return ListaDeudaFacturada.Elementos.Count; }
        }

        public decimal _deudaFacturada;
        public decimal DeudaFacturada
        {
            get { return ListaDeudaFacturada.TotalDeudaFinanciar; }
            set { _deudaFacturada = value; }
        }

        public decimal DeudaFacturadaTerceros
        {
            get { return ListaDeudaFacturada.TotalDeudaTerceros; }
        }

        public decimal DeudaPorFacturar
        {
            get { return ListaDeudaPorFacturar.TotalDeudaPorFacturar; }
        }

        public decimal InteresesCalculados
        {
            get { return ListaIntereses.TotalIntereses; }
        }

        public decimal TotaldocumentosAFavor
        {
            get { return ListaDocumentosAFavor.TotalDocumentoAFavor; }
        }

        public decimal TotalDeuda
        {
            get
            {
                return ListaDeudaPorFacturar.TotalDeudaPorFacturar
              + ListaDeudaFacturada.TotalDeudaFinanciar
              + ListaIntereses.TotalIntereses;
            }
        }

        public bool TieneProvision
        {
            get { return this.ListaDeudaFacturada.TieneProvision; }
        }

        public bool TieneCastigo
        {
            get { return this.ListaDeudaFacturada.TieneCastigo; }
        }

        public bool TieneReclamo
        {
            get { return this.ListaDeudaFacturada.TieneReclamo; }
        }

        public decimal TotalDeudaCastigada
        {
            get { return this.ListaDeudaFacturada.TotalDeudaCastigada; }
        }

        public decimal TotalDeudaProvisionada
        {
            get { return this.ListaDeudaFacturada.TotalDeudaProvisionada; }
        }

        public short IdNivelTension
        {
            get { return _idNivelTension; }
            set { _idNivelTension = value; }
        }

        public int UltimoPeriodoFacturado
        {
            get
            {
                int upf = 0;

                if (this.ListaDeudaFacturada == null
                    || this.ListaDeudaFacturada.Elementos == null
                    || this.ListaDeudaFacturada.Elementos.Count == 0)
                    upf = 0;
                else
                    this.ListaDeudaFacturada.Elementos.ForEach(delegate (enteNroServicioDeudaFacturada p)
                    { if (p.Periodo > upf) upf = p.Periodo; });

                return upf;
            }
        }

        public int AntiguedadMesesDeuda
        {
            get
            {
                int antiguedad = 0;
                int mesesNoFacturados = 0;
                int upf = this.UltimoPeriodoFacturado;

                if (upf > 0)
                {
                    int ayoUPF = Convert.ToInt32(upf.ToString().Substring(0, 4));
                    int mesUPF = Convert.ToInt32(upf.ToString().Substring(4, 2));

                    DateTime periodoUPF = new DateTime(ayoUPF, mesUPF, 1);
                    DateTime periodoActual = DateTime.Now.Date.AddMonths(-1);

                    TimeSpan calculoFecha = periodoActual - periodoUPF;
                    DateTime nuevaFecha = DateTime.MinValue + calculoFecha;

                    mesesNoFacturados = nuevaFecha.Month - 1;

                    antiguedad = this.ListaDeudaFacturada.Elementos.Count + mesesNoFacturados - 1;
                }

                return antiguedad;
            }
        }

        public string MensajeResultado
        {
            get { return _mensajeResultado; }
            set { _mensajeResultado = value; }
        }

        public string NombreCartera
        {
            get { return _nombreCartera; }
            set { _nombreCartera = value; }
        }

        public string Tarifa
        {
            get { return _tarifa; }
            set { _tarifa = value; }
        }

        #endregion

        #region Constructor

        public enteNroServicioDeuda() { }

        #endregion

        /// <summary>
        /// Realiza la validación si existe alguna autorización para realizar convenio.
        /// </summary>
        /// <param name="convenioResumen">objeto con la información del convenio a financiar.</param>
        /// <param name="mensajeValidacion">mensaje de validación.  Si es correcto el valor es vacío</param>
        /// <returns>Devuelve Verdadero si existe autorización para realizar convenio.</returns>
        public clsConvenioAutorizado ValidarAutorizacion(enteConvenioResumen convenioResumen, ref string mensajeValidacion)
        {
            return _listaAutorizado.Elementos.Find(delegate (clsConvenioAutorizado p)
            {
                return p.PeriodoValidez == convenioResumen.PeriodoPago;
            });
        }


    }

    public class enteListaNroServicioDeuda
    {
        private List<enteNroServicioDeuda> _elementos = new List<enteNroServicioDeuda>();
        private List<enteNroServicioDeuda> _elementosGrilla = new List<enteNroServicioDeuda>();

        private decimal _totalDeudaFacturadaFinanciar = 0;

        public decimal TotalDeudaFacturadaFinanciar
        {
            get { return _totalDeudaFacturadaFinanciar; }
            set { _totalDeudaFacturadaFinanciar = value; }
        }
        private decimal _totalDeudaPorFacturarFinanciar = 0;

        public decimal TotalDeudaPorFacturarFinanciar
        {
            get { return _totalDeudaPorFacturarFinanciar; }
            set { _totalDeudaPorFacturarFinanciar = value; }
        }
        private decimal _totalDocumentosAFavor = 0;

        public decimal TotalDocumentosAFavor
        {
            get { return _totalDocumentosAFavor; }
            set { _totalDocumentosAFavor = value; }
        }
        private decimal _totalIntereses = 0;
        public decimal TotalIntereses
        {
            get { return _totalIntereses; }
            set { _totalIntereses = value; }
        }

        private decimal _totalDeudaFacturadaTerceros = 0;

        public decimal TotalDeudaFacturadaTerceros
        {
            get { return _totalDeudaFacturadaTerceros; }
            set { _totalDeudaFacturadaTerceros = value; }
        }

        public List<enteNroServicioDeuda> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        public List<enteNroServicioDeuda> ElementosGrilla
        {
            get { return _elementosGrilla; }
            set { _elementosGrilla = value; }
        }

        decimal _totalDeudaCastigada;
        public decimal TotalDeudaCastigada
        {
            get { return _totalDeudaCastigada; }
            set { _totalDeudaCastigada = value; }
        }

        decimal _totalDeudaProvisionada;
        public decimal TotalDeudaProvisionada
        {
            get { return _totalDeudaProvisionada; }
            set { _totalDeudaProvisionada = value; }
        }

        public enteListaNroServicioDeuda()
        {
            _elementos = new List<enteNroServicioDeuda>();
            _elementosGrilla = new List<enteNroServicioDeuda>();
        }

        public void AgregarElemento(enteNroServicioDeuda onroservicio)
        {
            _elementos.Add(onroservicio);
            _elementosGrilla.Add(onroservicio);

            _totalDeudaFacturadaFinanciar += onroservicio.DeudaFacturada;
            _totalDeudaPorFacturarFinanciar += onroservicio.DeudaPorFacturar;
            _totalDocumentosAFavor += onroservicio.TotaldocumentosAFavor;
            _totalIntereses += onroservicio.InteresesCalculados;
            _totalDeudaFacturadaTerceros += onroservicio.DeudaFacturadaTerceros;
            _totalDeudaCastigada += onroservicio.TotalDeudaCastigada;
            _totalDeudaProvisionada += onroservicio.TotalDeudaProvisionada;
        }

        public void AgregarElementoRango(enteListaNroServicioDeuda olistanroservicio)
        {
            foreach (enteNroServicioDeuda oregistro in olistanroservicio.Elementos)
                this.AgregarElemento(oregistro);
        }

        public void EliminarElemento(int indiceEliminar)
        {
            enteNroServicioDeuda onroservicio = _elementos[indiceEliminar] as enteNroServicioDeuda;

            _totalDeudaFacturadaFinanciar -= onroservicio.DeudaFacturada;
            _totalDeudaPorFacturarFinanciar -= onroservicio.DeudaPorFacturar;
            _totalDocumentosAFavor -= onroservicio.TotaldocumentosAFavor;
            _totalIntereses -= onroservicio.InteresesCalculados;
            _totalDeudaFacturadaTerceros -= onroservicio.DeudaFacturadaTerceros;
            _totalDeudaCastigada -= onroservicio.TotalDeudaCastigada;
            _totalDeudaProvisionada -= onroservicio.TotalDeudaProvisionada;

            _elementos.RemoveAt(indiceEliminar);
            _elementosGrilla.RemoveAt(indiceEliminar);
        }
    }

    public class enteNroServicioDeudaFacturada
    {
        #region campos

        private short _idEmpresa;
        private TipoDocumentoComercial _idTipoDocumento;
        private string _nroDocumento;
        private int _periodo;
        private DateTime _fechaVencimiento;
        private decimal _totalRecibo;
        private decimal _saldo;
        private decimal _saldoTerceros;
        private decimal _saldoFinanciar;
        private decimal _montoPagado;
        private EstadoGenerales _idEstadoDeuda;
        private EstadoGenerales _idEstadoDeudaNuevo;
        private enteListaNroServicioDeudaFacturadaSustento _elementosSustento = new enteListaNroServicioDeudaFacturadaSustento();

        private string _nombreEstadoDeuda;
        private string _nombreDocumento;
        private string _mensajeResultado;

        #endregion

        #region propiedades

        /// <summary>
        /// Mensaje de error encontrado para este recibo.
        /// </summary>
        public string MensajeResultado
        {
            get { return _mensajeResultado; }
            set { _mensajeResultado = value; }
        }

        /// <summary>
        /// Indicador si tiene algún mensaje de Error en el Resultado.
        /// </summary>
        public bool TieneError
        {
            get { return !string.IsNullOrEmpty(_mensajeResultado); }
        }

        /// <summary>
        /// ID de la empresa del documento.
        /// </summary>
        public short IdEmpresa
        {
            get { return _idEmpresa; }
            set { _idEmpresa = value; }
        }

        /// <summary>
        /// ID del tipo de documento.
        /// </summary>
        public TipoDocumentoComercial IdTipoDocumento
        {
            get { return _idTipoDocumento; }
            set { _idTipoDocumento = value; }
        }

        /// <summary>
        /// Nombre del documento.
        /// </summary>
        public string NombreDocumento
        {
            get { return _nombreDocumento; }
            set { _nombreDocumento = value; }
        }

        /// <summary>
        /// Número del documento.
        /// </summary>
        public string NroDocumento
        {
            get { return _nroDocumento; }
            set { _nroDocumento = value; }
        }

        /// <summary>
        /// Periodo de emisión del documento.
        /// </summary>
        public int Periodo
        {
            get { return _periodo; }
            set { _periodo = value; }
        }

        /// <summary>
        /// Fecha de Vencimiento del documento.
        /// </summary>
        public DateTime FechaVencimiento
        {
            get { return _fechaVencimiento; }
            set { _fechaVencimiento = value; }
        }

        /// <summary>
        /// Total importe del documento.
        /// </summary>
        public decimal TotalRecibo
        {
            get { return _totalRecibo; }
            set { _totalRecibo = value; }
        }

        /// <summary>
        /// Saldo de deuda total del recibo.  Incluye saldo por Servicios de Terceros.
        /// </summary>
        public decimal Saldo
        {
            get { return _saldo; }
            set { _saldo = value; }
        }

        /// <summary>
        /// Saldo de documentos sustentos por teceros.
        /// </summary>
        public decimal SaldoTerceros
        {
            get { return _saldoTerceros; }
            set { _saldoTerceros = value; }
        }

        /// <summary>
        /// Saldo del recibo.  No se consideran los saldos por terceros.
        /// </summary>
        public decimal SaldoFinanciar
        {
            get { return _saldoFinanciar; }
            set { _saldoFinanciar = value; }
        }

        /// <summary>
        /// Monto Pagado en ventanilla a descargar en la ctacte.
        /// </summary>
        public decimal MontoPagado
        {
            get { return _montoPagado; }
            set { _montoPagado = value; }
        }

        /// <summary>
        /// Estado Actual de la deuda.  Puede ser:
        /// 52.Pagada
        /// 50.No Pagada
        /// 51.Por Conciliar
        /// 2.Anulada
        /// 54.Financiada
        /// </summary>
        public EstadoGenerales IdEstadoDeudaOrigen
        {
            get { return _idEstadoDeuda; }
            set { _idEstadoDeuda = value; }
        }

        /// <summary>
        /// Nuevo Estado de la deuda a cambiar despues de alguna transacción de pago.
        /// 52.Pagada
        /// 50.No Pagada
        /// 54.Financiada
        /// </summary>
        public EstadoGenerales IdEstadoDeudaNuevo
        {
            get { return _idEstadoDeudaNuevo; }
            set { _idEstadoDeudaNuevo = value; }
        }

        /// <summary>
        /// Nombre del Estado de la Deuda.
        /// </summary>
        public string NombreEstadoDeuda
        {
            get { return _nombreEstadoDeuda; }
            set { _nombreEstadoDeuda = value; }
        }

        /// <summary>
        /// Indicador que permite saber si algún documento sustento está Provisionado.
        /// </summary>
        public bool TieneProvision
        {
            get { return this.ListaDocumentosSustentos.TieneProvision; }
        }

        /// <summary>
        /// Indicador que permite saber si algún documento sustento está Castigado.
        /// </summary>
        public bool TieneCastigo
        {
            get { return this.ListaDocumentosSustentos.TieneCastigo; }
        }

        public decimal TotalDeudaCastigada
        {
            get { return this.ListaDocumentosSustentos.TotalDeudaCastigada; }
        }

        public decimal TotalDeudaProvisionada
        {
            get { return this.ListaDocumentosSustentos.TotalDeudaProvisionada; }
        }

        /// <summary>
        /// Lista de documentos sustentos que componen la deuda.
        /// </summary>
        public enteListaNroServicioDeudaFacturadaSustento ListaDocumentosSustentos
        {
            get { return _elementosSustento; }
            set { _elementosSustento = value; }
        }

        public bool TieneReclamo
        {
            get { return this.ListaDocumentosSustentos.TieneReclamo; }
        }

        #endregion

        #region constructor

        public enteNroServicioDeudaFacturada() { }

        public enteNroServicioDeudaFacturada(System.Data.DataRow drregistro)
        {
            if (drregistro == null) return;

            this._fechaVencimiento = Convert.ToDateTime(drregistro["fechavencimiento"]);
            this._idEmpresa = Convert.ToInt16(drregistro["idempresa"]);
            this._idTipoDocumento = (TipoDocumentoComercial)Convert.ToInt16(drregistro["idtipodocumento"]);
            this._nroDocumento = drregistro["nrodocumento"].ToString();
            this._idNroServicio = Convert.ToInt32(drregistro["idnroservicioprincipal"]);
            this._periodo = Convert.ToInt32(drregistro["periodo"]);
            this._saldo = Convert.ToDecimal(drregistro["saldo"]);
            this._totalRecibo = Convert.ToDecimal(drregistro["importetotal"]);
            this._idEstadoDeuda = (EstadoGenerales)Convert.ToInt16(drregistro["estadodeuda"]);
            this._montoPagado = Convert.ToDecimal(drregistro["montodescargado"]);

            this._nombreDocumento = this._idTipoDocumento.ToString();
            this._nombreEstadoDeuda = this._idEstadoDeuda.ToString();
        }

        #endregion

        #region métodos publicos

        public void AgregarSustento(enteListaNroServicioDeudaFacturadaSustento osustento)
        {
            _elementosSustento = osustento;
            _saldoFinanciar += osustento.TotalDeudaSustentoFinanciar;
            _saldoTerceros += osustento.TotalDeudaTerceros;
        }

        #endregion

        private decimal _nuevoSaldo;
        public decimal NuevoSaldo
        {
            get
            {
                return _nuevoSaldo;
            }
            set
            {
                _nuevoSaldo = value;
            }
        }

        private int _idNroServicio;
        public int IdNroServicio
        {
            get { return _idNroServicio; }
            set { _idNroServicio = value; }
        }

        private int _periodoPago;
        public int PeriodoPago
        {
            get
            {
                return _periodoPago;
            }
            set
            {
                _periodoPago = value;
            }
        }

        private DateTime _fechaPago;
        public DateTime FechaPago
        {
            get
            {
                return _fechaPago;
            }
            set
            {
                _fechaPago = value;
            }
        }

        private short _idServicio;
        public short IdServicio
        {
            get
            {
                return _idServicio;
            }
            set
            {
                _idServicio = value;
            }
        }
    }

    public class enteListaNroServicioDeudaFacturada
    {
        private List<enteNroServicioDeudaFacturada> _elementos = new List<enteNroServicioDeudaFacturada>();
        private List<enteNroServicioDeudaFacturada> _elementosGrilla = new List<enteNroServicioDeudaFacturada>();

        private decimal _totalDeudaProvisionada = 0;
        private decimal _totalDeudaCastigada = 0;
        private decimal _totalDeuda = 0;
        private decimal _totalDeudaTerceros;

        /// <summary>
        /// Lista de recibos pendientes de pago.
        /// </summary>
        public List<enteNroServicioDeudaFacturada> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        public List<enteNroServicioDeudaFacturada> ElementosGrilla
        {
            get
            {
                return _elementosGrilla;
            }
        }

        /// <summary>
        /// Total deuda a financiar.
        /// </summary>
        public decimal TotalDeudaFinanciar
        {
            get { return _totalDeuda; }
            set { _totalDeuda = value; }
        }

        /// <summary>
        /// Total deuda de servicios de terceros.
        /// </summary>
        public decimal TotalDeudaTerceros
        {
            get { return _totalDeudaTerceros; }
            set { _totalDeudaTerceros = value; }
        }

        /// <summary>
        /// Indica si existen recibos provisionados.
        /// </summary>
        public bool TieneProvision
        {
            get { return this.TotalDeudaProvisionada > 0; }
        }

        /// <summary>
        /// Indica si existen recibos castigados.
        /// </summary>
        public bool TieneCastigo
        {
            get { return this.TotalDeudaCastigada > 0; }
        }

        public decimal TotalDeudaProvisionada
        {
            get { return _totalDeudaProvisionada; }
            set { _totalDeudaProvisionada = value; }
        }

        public decimal TotalDeudaCastigada
        {
            get { return _totalDeudaCastigada; }
            set { _totalDeudaCastigada = value; }
        }

        private bool _tieneReclamo;
        public bool TieneReclamo
        {
            get { return _tieneReclamo; }
            set { _tieneReclamo = value; }
        }

        /// <summary>
        /// Constructor predeterminado.
        /// </summary>
        public enteListaNroServicioDeudaFacturada()
        {

        }

        /// <summary>
        /// Agrega un nuevo recibo (objeto de tipo DataTable) a la lista.
        /// </summary>
        /// <param name="dtlistaentidad"></param>
        public enteListaNroServicioDeudaFacturada(System.Data.DataTable dtlistaentidad)
        {
            if (dtlistaentidad == null || dtlistaentidad.Rows.Count == 0) return;

            enteNroServicioDeudaFacturada odeuda;

            foreach (DataRow drregistro in dtlistaentidad.Rows)
            {
                odeuda = new enteNroServicioDeudaFacturada(drregistro);

                _elementos.Add(odeuda);
                _elementosGrilla.Add(odeuda);

                _totalDeuda += odeuda.SaldoFinanciar;
                _totalDeudaTerceros += odeuda.SaldoTerceros;

                _totalDeudaCastigada += odeuda.TotalDeudaCastigada;
                _totalDeudaProvisionada += odeuda.TotalDeudaProvisionada;

                if (odeuda.TieneReclamo) this._tieneReclamo = true;
            }
        }

        /// <summary>
        /// Agrega un nuevo recibo (objeto de tipo enteNroServicioDeudaFacturada) a la lista.
        /// </summary>
        /// <param name="orecibo"></param>
        public void AgregarRecibo(enteNroServicioDeudaFacturada orecibo)
        {
            this._elementos.Add(orecibo);
            this._elementosGrilla.Add(orecibo);

            this._totalDeuda += orecibo.SaldoFinanciar;
            this._totalDeudaTerceros += orecibo.SaldoTerceros;
            this._totalDeudaCastigada += orecibo.TotalDeudaCastigada;
            this._totalDeudaProvisionada += orecibo.TotalDeudaProvisionada;

            if (orecibo.TieneReclamo) this._tieneReclamo = true;
        }

    }

    public class enteNroServicioDeudaPorFacturar
    {
        private bool _seleccionar;
        private TipoDocumentoComercial _idTipoDocumento;
        private string _numeroDocumento;
        private ServicioDistribucionEnergia _idServicio;
        private string _fechaConvenio;
        private decimal _totalFinanciado;
        private decimal _saldoFinanciar;
        private int _cuotasPendientes;
        private EstadoGenerales _idEstado;
        private string _nombreEstado;
        private string _nombreDocumento;
        private string _nombreTipoServicio;
        private string _fechaVencimiento;
        private short _idConvenioTipo;
        private decimal _valorCuota;

        public bool Seleccionar
        {
            get { return _seleccionar; }
            set { _seleccionar = value; }
        }

        public string NombreDocumento
        {
            get { return _nombreDocumento; }
            set { _nombreDocumento = value; }
        }

        public string NombreTipoServicio
        {
            get { return _nombreTipoServicio; }
            set { _nombreTipoServicio = value; }
        }

        public string FechaVencimiento
        {
            get { return _fechaVencimiento; }
            set { _fechaVencimiento = value; }
        }

        public short IdConvenioTipo
        {
            get { return _idConvenioTipo; }
            set { _idConvenioTipo = value; }
        }

        public EstadoGenerales IdEstado
        {
            get { return _idEstado; }
            set { _idEstado = value; }
        }

        public TipoDocumentoComercial IdTipoDocumento
        {
            get { return _idTipoDocumento; }
            set { _idTipoDocumento = value; }
        }

        public string NumeroDocumento
        {
            get { return _numeroDocumento; }
            set { _numeroDocumento = value; }
        }

        public ServicioDistribucionEnergia IdServicio
        {
            get { return _idServicio; }
            set { _idServicio = value; }
        }

        public string FechaConvenio
        {
            get { return _fechaConvenio; }
            set { _fechaConvenio = value; }
        }

        public decimal TotalFinanciado
        {
            get { return _totalFinanciado; }
            set { _totalFinanciado = value; }
        }

        public decimal SaldoFinanciar
        {
            get { return _saldoFinanciar; }
            set { _saldoFinanciar = value; }
        }

        public int CuotasPendientes
        {
            get { return _cuotasPendientes; }
            set { _cuotasPendientes = value; }
        }

        public string NombreEstado
        {
            get { return _nombreEstado; }
            set { _nombreEstado = value; }
        }

        public decimal ValorCuota
        {
            get { return _valorCuota; }
            set { _valorCuota = value; }
        }

        public enteNroServicioDeudaPorFacturar() { }

        public enteNroServicioDeudaPorFacturar(System.Data.DataRow drregistro)
        {
            if (drregistro == null) return;

            this._idTipoDocumento = (TipoDocumentoComercial)Convert.ToInt16(drregistro["IdDocumentoComercial"]);
            this._numeroDocumento = drregistro["nrodocumento"].ToString();
            this._nombreTipoServicio = drregistro["NombreTipoServicio"].ToString();
            this._fechaConvenio = Convert.ToDateTime(drregistro["fechaconvenio"]).ToString("yyyyMMdd");
            this._fechaVencimiento = Convert.ToDateTime(drregistro["FechaVencimiento"]).ToString("yyyyMMdd");
            this._totalFinanciado = Convert.ToDecimal(drregistro["ImporteTotal"]);
            this._saldoFinanciar = Convert.ToDecimal(drregistro["ImporteFinanciar"]);
            this._cuotasPendientes = Convert.ToInt32(drregistro["cuotaspendientes"]);
            this._idEstado = (EstadoGenerales)Convert.ToInt16(drregistro["idestado"]);
            this._idServicio = (ServicioDistribucionEnergia)Convert.ToInt16(drregistro["idservicio"]);
            this._idConvenioTipo = Convert.ToInt16(drregistro["IdConvenioTipo"]);
            this._valorCuota = Convert.ToDecimal(drregistro["valorcuota"]);

            this._nombreEstado = this._idEstado.ToString();
            this._nombreDocumento = this._idTipoDocumento.ToString();
        }

    }

    public class enteListaNroServicioDeudaPorFacturar
    {
        private List<enteNroServicioDeudaPorFacturar> _elementos = new List<enteNroServicioDeudaPorFacturar>();
        private decimal _totalDeudaPorFacturar = 0;

        public decimal TotalDeudaPorFacturar
        {
            get { return _totalDeudaPorFacturar; }
            set { _totalDeudaPorFacturar = value; }
        }

        public List<enteNroServicioDeudaPorFacturar> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        public enteListaNroServicioDeudaPorFacturar() { }

        public enteListaNroServicioDeudaPorFacturar(System.Data.DataTable dtlistaentidad)
        {
            if (dtlistaentidad == null || dtlistaentidad.Rows.Count == 0) return;

            enteNroServicioDeudaPorFacturar oporfacturar;

            foreach (DataRow oregistro in dtlistaentidad.Rows)
            {
                oporfacturar = new enteNroServicioDeudaPorFacturar(oregistro);

                _elementos.Add(oporfacturar);

                _totalDeudaPorFacturar += oporfacturar.SaldoFinanciar;
            }
        }
    }

    public class enteNroServicioDeudaFacturadaSustento
    {
        #region campos

        private short _idEmpresa;
        private TipoDocumentoComercial _idTipoDocumento;
        private string _nroDocumento;
        private short _idTipoDocumentoSustento;
        private string _nroDocumentoSustento;
        private short _idServicio;
        private bool _esPropio;
        private decimal _totalSustento;
        private decimal _saldoSustento;
        private EstadoGenerales _idEstadoDocumento;
        private EstadoGenerales _idEstadoDeuda;
        private bool _indicadorReclamo;

        private string _nombreDocumento;
        private string _nombreDocumentoSustento;
        private string _nombreEstadoDocumento;
        private string _nombreEstadoDeuda;

        #endregion

        #region propiedades

        public short IdTipoDocumentoSustento
        {
            get { return _idTipoDocumentoSustento; }
            set { _idTipoDocumentoSustento = value; }
        }

        public string NroDocumentoSustento
        {
            get { return _nroDocumentoSustento; }
            set { _nroDocumentoSustento = value; }
        }

        public string NombreDocumentoSustento
        {
            get { return _nombreDocumentoSustento; }
            set { _nombreDocumentoSustento = value; }
        }

        public short IdServicio
        {
            get { return _idServicio; }
            set { _idServicio = value; }
        }

        public bool EsPropio
        {
            get { return _esPropio; }
            set { _esPropio = value; }
        }

        public decimal TotalSustento
        {
            get { return _totalSustento; }
            set { _totalSustento = value; }
        }

        public decimal SaldoSustento
        {
            get { return _saldoSustento; }
            set { _saldoSustento = value; }
        }

        public EstadoGenerales IdEstadoDocumento
        {
            get { return _idEstadoDocumento; }
            set { _idEstadoDocumento = value; }
        }

        public EstadoGenerales IdEstadoDeuda
        {
            get { return _idEstadoDeuda; }
            set { _idEstadoDeuda = value; }
        }

        public short IdEmpresa
        {
            get { return _idEmpresa; }
            set { _idEmpresa = value; }
        }

        public TipoDocumentoComercial IdTipoDocumento
        {
            get { return _idTipoDocumento; }
            set { _idTipoDocumento = value; }
        }

        public string NombreDocumento
        {
            get { return _nombreDocumento; }
            set { _nombreDocumento = value; }
        }

        public string NroDocumento
        {
            get { return _nroDocumento; }
            set { _nroDocumento = value; }
        }

        public string NombreEstadoDocumento
        {
            get { return _nombreEstadoDocumento; }
            set { _nombreEstadoDocumento = value; }
        }

        private decimal _totalDeudaProvisionada;
        public decimal TotalDeudaProvisionada
        {
            get { return _totalDeudaProvisionada; }
            set { _totalDeudaProvisionada = value; }
        }

        private decimal _totalDeudaCastigada;
        public decimal TotalDeudaCastigada
        {
            get { return _totalDeudaCastigada; }
            set { _totalDeudaCastigada = value; }
        }

        public bool IndicadorReclamo
        {
            get { return _indicadorReclamo; }
            set { _indicadorReclamo = value; }
        }
        #endregion

        #region constructor

        public enteNroServicioDeudaFacturadaSustento() { }

        public enteNroServicioDeudaFacturadaSustento(System.Data.DataRow drregistro)
        {
            if (drregistro == null) return;

            this._idEmpresa = Convert.ToInt16(drregistro["idempresa"]);
            this._idTipoDocumento = (TipoDocumentoComercial)Convert.ToInt16(drregistro["idtipodocumento"]);
            this._nroDocumento = drregistro["nrodocumento"].ToString();
            this._idTipoDocumentoSustento = Convert.ToInt16(drregistro["IdTipoDocumentoSustento"]);
            this._nroDocumentoSustento = drregistro["NroDocumentoSustento"].ToString();
            this._idServicio = Convert.ToInt16(drregistro["IdServicio"]);
            this._totalSustento = Convert.ToDecimal(drregistro["ImporteTotal"]);
            this._saldoSustento = Convert.ToDecimal(drregistro["Saldo"]);
            this._idEstadoDeuda = (EstadoGenerales)Convert.ToInt16(drregistro["EstadoDeuda"]);
            this._idEstadoDocumento = (EstadoGenerales)Convert.ToInt16(drregistro["EstadoDocumento"]);
            this._indicadorReclamo = Convert.ToBoolean(drregistro["indicadorreclamo"]);
            this._esPropio = Convert.ToBoolean(drregistro["espropio"]);

            this._totalDeudaCastigada = this._idEstadoDocumento == EstadoGenerales.Castigado ? this.SaldoSustento : 0;
            this._totalDeudaProvisionada = this._idEstadoDocumento == EstadoGenerales.Provisionado ? this.SaldoSustento : 0;

            this._nombreEstadoDocumento = this._idEstadoDocumento.ToString();
            this._nombreEstadoDeuda = this._idEstadoDeuda.ToString();
            this._nombreDocumento = this._idTipoDocumento.ToString();
            this._nombreDocumentoSustento = this._idTipoDocumentoSustento.ToString();

        }

        #endregion

        private decimal _montoPagado;
        public decimal MontoPagado
        {
            get
            {
                return _montoPagado;
            }
            set
            {
                _montoPagado = value;
            }
        }

        private EstadoGenerales _idEstadoDeudaNuevo;
        public EstadoGenerales IdEstadoDeudaNuevo
        {
            get
            {
                return _idEstadoDeudaNuevo;
            }
            set
            {
                _idEstadoDeudaNuevo = value;
            }
        }

        public decimal NuevoSaldo
        {
            get { return this._saldoSustento - this._montoPagado; }
        }

        private enteNroServicioDeudaFacturada _parent;
        public enteNroServicioDeudaFacturada Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;
            }
        }

        private short _ordenPago;
        public short OrdenPago
        {
            get { return _ordenPago; }
            set { _ordenPago = value; }
        }

    }

    public class enteListaNroServicioDeudaFacturadaSustento
    {
        private List<enteNroServicioDeudaFacturadaSustento> _elementos = new List<enteNroServicioDeudaFacturadaSustento>();
        private decimal _totalDeudaSustento = 0;
        private decimal _totalDeudaSustentoFinanciar = 0;
        private decimal _totalDeudaTerceros = 0;
        private bool _tieneProvision;
        private bool _tieneCastigo;

        public List<enteNroServicioDeudaFacturadaSustento> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        public decimal TotalDeudaSustentoFinanciar
        {
            get { return _totalDeudaSustentoFinanciar; }
            set { _totalDeudaSustentoFinanciar = value; }
        }

        public decimal TotalDeudaSustento
        {
            get { return _totalDeudaSustento; }
            set { _totalDeudaSustento = value; }
        }

        public decimal TotalDeudaTerceros
        {
            get { return _totalDeudaTerceros; }
            set { _totalDeudaTerceros = value; }
        }

        public bool TieneProvision
        {
            get { return _tieneProvision; }
            set { _tieneProvision = value; }
        }

        public bool TieneCastigo
        {
            get { return _tieneCastigo; }
            set { _tieneCastigo = value; }
        }

        private decimal _totalDeudaCastigada;
        public decimal TotalDeudaCastigada
        {
            get { return _totalDeudaCastigada; }
            set { _totalDeudaCastigada = value; }
        }

        private decimal _totalDeudaProvisionada;
        public decimal TotalDeudaProvisionada
        {
            get { return _totalDeudaProvisionada; }
            set { _totalDeudaProvisionada = value; }
        }

        private bool _tieneReclamo;
        public bool TieneReclamo
        {
            get { return _tieneReclamo; }
            set { _tieneReclamo = value; }
        }

        public enteListaNroServicioDeudaFacturadaSustento() { }

        public enteListaNroServicioDeudaFacturadaSustento(System.Data.DataTable dtlistaentidad)
        {
            if (dtlistaentidad == null || dtlistaentidad.Rows.Count == 0) return;

            enteNroServicioDeudaFacturadaSustento osustento;

            foreach (DataRow drregistro in dtlistaentidad.Rows)
            {
                osustento = new enteNroServicioDeudaFacturadaSustento(drregistro);

                this._elementos.Add(osustento);

                this._totalDeudaSustento += osustento.SaldoSustento;

                this._totalDeudaCastigada += osustento.TotalDeudaCastigada;
                this._totalDeudaProvisionada += osustento.TotalDeudaProvisionada;

                if (osustento.EsPropio)
                    this._totalDeudaSustentoFinanciar += osustento.SaldoSustento;
                else
                    this._totalDeudaTerceros += osustento.SaldoSustento;

                if (osustento.IdEstadoDocumento == EstadoGenerales.Provisionado)
                    this._tieneProvision = true;

                if (osustento.IdEstadoDocumento == EstadoGenerales.Castigado)
                    this._tieneCastigo = true;

                if (osustento.IndicadorReclamo)
                    this._tieneReclamo = osustento.IndicadorReclamo;
            }
        }
    }

    public class enteNroServicioDocumentoAFavor
    {
        private short _idEmpresa;
        private TipoDocumentoComercial _idTipoDocumento;
        private string _numeroDocumento;
        private string _nombreDocumento;
        private decimal _saldo;
        private string _fechaEmision;
        private string _fechaVencimiento;
        private decimal _importe;

        public decimal Importe
        {
            get { return _importe; }
            set { _importe = value; }
        }

        public string FechaEmision
        {
            get { return _fechaEmision; }
            set { _fechaEmision = value; }
        }

        public string FechaVencimiento
        {
            get { return _fechaVencimiento; }
            set { _fechaVencimiento = value; }
        }

        public short IdEmpresa
        {
            get { return _idEmpresa; }
            set { _idEmpresa = value; }
        }

        public TipoDocumentoComercial IdTipoDocumento
        {
            get { return _idTipoDocumento; }
            set { _idTipoDocumento = value; }
        }

        public string NumeroDocumento
        {
            get { return _numeroDocumento; }
            set { _numeroDocumento = value; }
        }

        public string NombreDocumento
        {
            get { return _nombreDocumento; }
            set { _nombreDocumento = value; }
        }

        public decimal Saldo
        {
            get { return _saldo; }
            set { _saldo = value; }
        }

        public enteNroServicioDocumentoAFavor() { }

        public enteNroServicioDocumentoAFavor(System.Data.DataRow drregistro)
        {
            if (drregistro == null) return;

            this._idEmpresa = Convert.ToInt16(drregistro["idempresa"]);
            this._idTipoDocumento = (TipoDocumentoComercial)Convert.ToInt16(drregistro["idtipodocumentoafavor"]);
            this._numeroDocumento = drregistro["nrodocumentoafavor"].ToString();
            this._fechaEmision = Convert.ToDateTime(drregistro["fecharegistro"]).ToString("dd/MM/yyyy");
            this._fechaVencimiento = Convert.ToDateTime(drregistro["fechavencimiento"]).ToString("dd/MM/yyyy");
            this._importe = Convert.ToDecimal(drregistro["importe"]);
            this._saldo = Convert.ToDecimal(drregistro["saldoafavor"]);

            this._nombreDocumento = this._idTipoDocumento.ToString();
        }
    }

    public class enteListaNroServicioDocumentoAFavor
    {
        private List<enteNroServicioDocumentoAFavor> _elementos = new List<enteNroServicioDocumentoAFavor>();

        private decimal _totalDocumentoAFavor = 0;

        public decimal TotalDocumentoAFavor
        {
            get { return _totalDocumentoAFavor; }
            set { _totalDocumentoAFavor = value; }
        }

        public List<enteNroServicioDocumentoAFavor> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        public enteListaNroServicioDocumentoAFavor() { }

        public enteListaNroServicioDocumentoAFavor(System.Data.DataTable dtlistaentidad)
        {
            if (dtlistaentidad == null || dtlistaentidad.Rows.Count == 0) return;

            enteNroServicioDocumentoAFavor odocfavor;

            foreach (DataRow drregistro in dtlistaentidad.Rows)
            {
                odocfavor = new enteNroServicioDocumentoAFavor(drregistro);

                _elementos.Add(odocfavor);

                this._totalDocumentoAFavor += odocfavor.Saldo;
            }
        }

        public void SetearDocumentosAFavor(enteListaNroServicioDocumentoAFavor documentoFavor)
        {
            this._totalDocumentoAFavor = 0;
            this._elementos = new List<enteNroServicioDocumentoAFavor>();

            foreach (enteNroServicioDocumentoAFavor documento in documentoFavor.Elementos)
            {
                this._elementos.Add(documento);
                this._totalDocumentoAFavor += documento.Saldo;
            }
        }
    }
    public class enteNroServicioAComercial
    {
        private string _nombreActividad;
        private string _nroOrdenTrabajo;
        private string _nroActa;
        private DateTime _fechaOrdenTrabajo;
        private DateTime _fechaEjecucion;
        private string _nombreEstado;

        public string NombreActividad
        {
            get { return _nombreActividad; }
            set { _nombreActividad = value; }
        }

        public string NroOrdenTrabajo
        {
            get { return _nroOrdenTrabajo; }
            set { _nroOrdenTrabajo = value; }
        }

        public string NroActa
        {
            get { return _nroActa; }
            set { _nroActa = value; }
        }

        public DateTime FechaOrdenTrabajo
        {
            get { return _fechaOrdenTrabajo; }
            set { _fechaOrdenTrabajo = value; }
        }

        public DateTime FechaEjecucion
        {
            get { return _fechaEjecucion; }
            set { _fechaEjecucion = value; }
        }

        public string NombreEstado
        {
            get { return _nombreEstado; }
            set { _nombreEstado = value; }
        }

        public enteNroServicioAComercial() { }

        public enteNroServicioAComercial(System.Data.DataRow registro)
        {
            this._fechaEjecucion = Convert.ToDateTime(registro["fechaejecucion"]);
            this._fechaOrdenTrabajo = Convert.ToDateTime(registro["fechaordentrabajo"]);
            this._nombreEstado = ((Estado)Convert.ToInt32(registro["idestado"])).ToString();
            this._nroActa = registro["nroacta"].ToString();
            this._nroOrdenTrabajo = registro["ordentrabajo"].ToString();
        }
    }

    public class enteListaNroServicioAComercial
    {
        private List<enteNroServicioAComercial> _elementos = new List<enteNroServicioAComercial>();

        public List<enteNroServicioAComercial> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        public enteListaNroServicioAComercial() { }

        public enteListaNroServicioAComercial(System.Data.DataTable tabla)
        {
            if (tabla == null || tabla.Rows.Count == 0) return;

            foreach (System.Data.DataRow registro in tabla.Rows)
                _elementos.Add(new enteNroServicioAComercial(registro));
        }
    }

    public class clsConvenioAutorizado
    {
        #region Campos
        private Decimal? _decdeudafacturada;
        private Int32? _intnromesesdeuda;
        private Decimal? _decintereses;
        private Decimal? _decdeudaporfacturar;
        private Decimal? _decpagoinicialexigido;
        private Decimal? _decnrocuotasmaximo;
        private String _strobservacionesaut;
        private Int32 _intidconvenioautorizado;
        private Int16 _intidunidadnegocio;
        private String _nombreunidadnegocio;
        private Int32 _intidusuariosolicita;
        private Int16 _intidconveniotipo;
        private Int32 _intnroservicio;
        private String _strnroconvenio;
        private Decimal _decdeuda;
        private DateTime _datfechasolicitud;
        private Decimal _decdescuento;
        private Decimal _decpagoinicial;
        private Int16 _intnrocuotas;
        private Int16 _intidtasainteres;
        private Decimal? _dectasainteres;
        private DateTime? _datfechaautorizacion;
        private DateTime? _datfechavalidez;
        private Int32? _intidautorizado;
        private Decimal? _decdescuentoaut;
        private Decimal? _decpagoinicialaut;
        private Int16? _intnrocuotasaut;
        private Int16? _intidtasainteresaut;
        private Decimal? _dectasainteresaut;
        private Int16 _intidestado;
        private String _strobservaciones;
        private Boolean _bolactualizaintereshoy;
        private clsListaConveniosPorFinanciar _lstconveniosfinanciar;

        private String _nombresuministro;
        private String _direccion;
        private String _cartera;
        private String _nombretarifa;
        private String _estado;
        private Int32 _periodovalidez;
        private Int16 _setearfechavalidez;
        private Int32 _idConvenioConfiguraAutorizacion;
        #endregion

        #region Propiedades
        /// <summary>
        /// IdConvenioAutorizado.
        /// </summary>
        public Int32 IdConvenioAutorizado
        {
            get { return _intidconvenioautorizado; }
            set { _intidconvenioautorizado = value; }
        }
        /// <summary>
        /// IdUnidadNegocio
        /// </summary>
        public Int16 IdUnidadNegocio
        {
            get { return _intidunidadnegocio; }
            set { _intidunidadnegocio = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreUnidadNegocio
        {
            get { return _nombreunidadnegocio; }
            set { _nombreunidadnegocio = value; }
        }
        /// <summary>
        /// IdUsuarioSolicita
        /// </summary>
        public Int32 IdUsuarioSolicita
        {
            get { return _intidusuariosolicita; }
            set { _intidusuariosolicita = value; }
        }
        /// <summary>
        /// IdConvenioTipo.
        /// </summary>
        public Int16 IdConvenioTipo
        {
            get { return _intidconveniotipo; }
            set { _intidconveniotipo = value; }
        }
        /// <summary>
        /// NroServicio.
        /// </summary>
        public Int32 NroServicio
        {
            get { return _intnroservicio; }
            set { _intnroservicio = value; }
        }
        /// <summary>
        /// NroConvenio.
        /// </summary>
        public String NroConvenio
        {
            get { return _strnroconvenio; }
            set { _strnroconvenio = value; }
        }
        /// <summary>
        /// Deuda.
        /// </summary>
        public Decimal Deuda
        {
            get { return _decdeuda; }
            set { _decdeuda = value; }
        }
        /// <summary>
        /// FechaSolicitud.
        /// </summary>
        public DateTime FechaSolicitud
        {
            get { return _datfechasolicitud; }
            set { _datfechasolicitud = value; }
        }
        /// <summary>
        /// Descuento.
        /// </summary>
        public Decimal Descuento
        {
            get { return _decdescuento; }
            set { _decdescuento = value; }
        }
        /// <summary>
        /// PagoInicial.
        /// </summary>
        public Decimal PagoInicial
        {
            get { return _decpagoinicial; }
            set { _decpagoinicial = value; }
        }
        /// <summary>
        /// NroCuotas.
        /// </summary>
        public Int16 NroCuotas
        {
            get { return _intnrocuotas; }
            set { _intnrocuotas = value; }
        }
        /// <summary>
        /// IdTasaInteres.
        /// </summary>
        public Int16 IdTasaInteres
        {
            get { return _intidtasainteres; }
            set { _intidtasainteres = value; }
        }
        /// <summary>
        /// TasaInteres.
        /// </summary>
        public Decimal? TasaInteres
        {
            get { return _dectasainteres; }
            set { _dectasainteres = value; }
        }
        /// <summary>
        /// FechaAutorizacion.
        /// </summary>
        public DateTime? FechaAutorizacion
        {
            get { return _datfechaautorizacion; }
            set { _datfechaautorizacion = value; }
        }
        /// <summary>
        /// FechaValidez.
        /// </summary>
        public DateTime? FechaValidez
        {
            get { return _datfechavalidez; }
            set { _datfechavalidez = value; }
        }
        /// <summary>
        /// IdAutorizado.
        /// </summary>
        public Int32? IdAutorizado
        {
            get { return _intidautorizado; }
            set { _intidautorizado = value; }
        }
        /// <summary>
        /// NroMesesDeuda.
        /// </summary>
        public Int32? NroMesesDeuda
        {
            get { return _intnromesesdeuda; }
            set { _intnromesesdeuda = value; }
        }
        /// <summary>
        /// DeudaFacturada
        /// </summary>
        public Decimal? DeudaFacturada
        {
            get { return _decdeudafacturada; }
            set { _decdeudafacturada = value; }
        }
        /// <summary>
		/// 
		/// </summary>
		public Decimal? DeudaPorFacturar
        {
            get { return _decdeudaporfacturar; }
            set { _decdeudaporfacturar = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Intereses
        {
            get { return _decintereses; }
            set { _decintereses = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? PagoInicialExigido
        {
            get { return _decpagoinicialexigido; }
            set { _decpagoinicialexigido = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? NroCuotasMaximo
        {
            get { return _decnrocuotasmaximo; }
            set { _decnrocuotasmaximo = value; }
        }
        /// <summary>
        /// ObservacionesAuto.
        /// </summary>
        public String ObservacionesAuto
        {
            get { return _strobservacionesaut; }
            set { _strobservacionesaut = value; }
        }
        /// <summary>
        /// DescuentoAut.
        /// </summary>
        public Decimal? DescuentoAut
        {
            get { return _decdescuentoaut; }
            set { _decdescuentoaut = value; }
        }
        /// <summary>
        /// PagoInicialAut.
        /// </summary>
        public Decimal? PagoInicialAut
        {
            get { return _decpagoinicialaut; }
            set { _decpagoinicialaut = value; }
        }
        /// <summary>
        /// NroCuotasAut.
        /// </summary>
        public Int16? NroCuotasAut
        {
            get { return _intnrocuotasaut; }
            set { _intnrocuotasaut = value; }
        }
        /// <summary>
        /// IdTasaInteresAut.
        /// </summary>
        public Int16? IdTasaInteresAut
        {
            get { return _intidtasainteresaut; }
            set { _intidtasainteresaut = value; }
        }
        /// <summary>
        /// TasaInteresAut.
        /// </summary>
        public Decimal? TasaInteresAut
        {
            get { return _dectasainteresaut; }
            set { _dectasainteresaut = value; }
        }
        /// <summary>
        /// IdEstado.
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }
        /// <summary>
        /// Observaciones.
        /// </summary>
        public String Observaciones
        {
            get { return _strobservaciones; }
            set { _strobservaciones = value; }
        }
        /// <summary>
        /// ActualizaInteresHoy.
        /// </summary>
        public Boolean ActualizaInteresHoy
        {
            get { return _bolactualizaintereshoy; }
            set { _bolactualizaintereshoy = value; }
        }
        /// <summary>
        /// LstConveniosPorFinanciar.
        /// </summary>
        public clsListaConveniosPorFinanciar LstConveniosPorFinanciar
        {
            get { return _lstconveniosfinanciar; }
            set { _lstconveniosfinanciar = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreSuministro
        {
            get { return _nombresuministro; }
            set { _nombresuministro = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Cartera
        {
            get { return _cartera; }
            set { _cartera = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreTarifa
        {
            get { return _nombretarifa; }
            set { _nombretarifa = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 PeriodoValidez
        {
            get { return _periodovalidez; }
            set { _periodovalidez = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 SetearFechaValidez
        {
            get { return _setearfechavalidez; }
            set { _setearfechavalidez = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdConvenioConfiguraAutorizacion
        {
            get { return _idConvenioConfiguraAutorizacion; }
            set { _idConvenioConfiguraAutorizacion = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// clsConvenioAutorizado.
        /// </summary>
        public clsConvenioAutorizado()
        {
            LstConveniosPorFinanciar = new clsListaConveniosPorFinanciar();
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsConvenioAutorizado(DataRow dr)
        {
            if (dr == null)
                return;

            AsignarValores(dr);
        }

        /// <summary>
        /// Constructor pasandole datarow y datatable.
        /// </summary>
        /// <param name="convenioautorizado"></param>
        /// <param name="conveniosporfinanciar"></param>
        public clsConvenioAutorizado(DataRow convenioautorizado,
                                     DataTable conveniosporfinanciar)
        {
            //Invocar al contructor con datarow para llenar sus propiedades.
            AsignarValores(convenioautorizado);

            ////Llenar Lista ConveniosPorFinanciar.
            _lstconveniosfinanciar = new clsListaConveniosPorFinanciar(conveniosporfinanciar);
        }

        /// <summary>
        /// AsignarValores.
        /// </summary>
        /// <param name="dr"></param>
        private void AsignarValores(DataRow dr)
        {
            IdConvenioAutorizado = dr.Table.Columns.Contains("idconvenioautorizado") ? Convert.ToInt32(dr["idconvenioautorizado"]) : (Int32)0;
            IdConvenioTipo = dr.Table.Columns.Contains("idconveniotipo") ? Convert.ToInt16(dr["idconveniotipo"]) : (Int16)0;
            NroServicio = dr.Table.Columns.Contains("nroservicio") ? Convert.ToInt32(dr["nroservicio"]) : (Int32)0;
            NroConvenio = dr.Table.Columns.Contains("nroconvenio") ? Convert.IsDBNull(dr["nroconvenio"]) ? "" : dr["nroconvenio"].ToString() : String.Empty;
            Descuento = dr.Table.Columns.Contains("descuento") ? Convert.ToDecimal(dr["descuento"]) : (Decimal)0;
            IdTasaInteres = dr.Table.Columns.Contains("idtasainteres") ? Convert.ToInt16(dr["idtasainteres"]) : (Int16)0;
            TasaInteres = dr.Table.Columns.Contains("tasainteres") ? Convert.IsDBNull(dr["tasainteres"]) ? (Decimal?)null : Convert.ToDecimal(dr["tasainteres"]) : (Decimal?)null;
            FechaAutorizacion = dr.Table.Columns.Contains("fechaautorizacion") ? Convert.IsDBNull(dr["fechaautorizacion"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechaautorizacion"]) : (DateTime?)null;
            FechaValidez = dr.Table.Columns.Contains("fechavalidez") ? Convert.IsDBNull(dr["fechavalidez"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechavalidez"]) : (DateTime?)null;
            IdAutorizado = dr.Table.Columns.Contains("idautorizado") ? Convert.IsDBNull(dr["idautorizado"]) ? (Int32?)null : Convert.ToInt32(dr["idautorizado"]) : (Int32?)null;
            DescuentoAut = dr.Table.Columns.Contains("descuentoaut") ? Convert.IsDBNull(dr["descuentoaut"]) ? (Decimal?)null : Convert.ToDecimal(dr["descuentoaut"]) : (Decimal?)null;
            IdTasaInteresAut = dr.Table.Columns.Contains("idtasainteresaut") ? Convert.IsDBNull(dr["idtasainteresaut"]) ? (Int16?)null : Convert.ToInt16(dr["idtasainteresaut"]) : (Int16?)null;
            TasaInteresAut = dr.Table.Columns.Contains("tasainteresaut") ? Convert.IsDBNull(dr["tasainteresaut"]) ? (Decimal?)null : Convert.ToDecimal(dr["tasainteresaut"]) : (Decimal?)null;
            Observaciones = dr.Table.Columns.Contains("observaciones") ? dr["observaciones"].ToString() : String.Empty;
            ActualizaInteresHoy = dr.Table.Columns.Contains("actualizarintereshoy") ? Convert.ToBoolean(dr["actualizarintereshoy"]) : false;

            IdUnidadNegocio = dr.Table.Columns.Contains("iduunn") ? Convert.ToInt16(dr["iduunn"]) : (Int16)0;
            NombreSuministro = dr.Table.Columns.Contains("nombresuministro") ? dr["nombresuministro"].ToString() : String.Empty;
            NombreUnidadNegocio = dr.Table.Columns.Contains("nombreuunn") ? dr["nombreuunn"].ToString() : String.Empty;
            Direccion = dr.Table.Columns.Contains("direccion") ? dr["direccion"].ToString() : String.Empty;
            Cartera = dr.Table.Columns.Contains("cartera") ? dr["cartera"].ToString() : String.Empty;
            NombreTarifa = dr.Table.Columns.Contains("nombretarifa") ? dr["nombretarifa"].ToString() : String.Empty;
            NroMesesDeuda = dr.Table.Columns.Contains("nromesesdeuda") ? Convert.ToInt32(dr["nromesesdeuda"]) : (Int32)0;
            FechaSolicitud = Convert.ToDateTime(dr["fechasolicitud"]);
            Deuda = Convert.ToDecimal(dr["deuda"]);
            PagoInicialExigido = dr.Table.Columns.Contains("pagoinicialexigido") ? Convert.ToDecimal(dr["pagoinicialexigido"]) : (Decimal?)0;
            NroCuotasMaximo = dr.Table.Columns.Contains("nrocuotasmaximo") ? Convert.ToDecimal(dr["nrocuotasmaximo"]) : (Decimal?)0;
            PagoInicial = Convert.ToDecimal(dr["pagoinicial"]);
            NroCuotas = Convert.ToInt16(dr["nrocuotas"]);
            PagoInicialAut = Convert.IsDBNull(dr["pagoinicialaut"]) ? (Decimal?)null : Convert.ToDecimal(dr["pagoinicialaut"]);
            NroCuotasAut = Convert.IsDBNull(dr["nrocuotasaut"]) ? (Int16?)null : Convert.ToInt16(dr["nrocuotasaut"]);
            IdEstado = dr.Table.Columns.Contains("idestado") ? Convert.ToInt16(dr["idestado"]) : (Int16)0;
            Estado = dr.Table.Columns.Contains("estado") ? dr["estado"].ToString() : String.Empty;
            PeriodoValidez = dr.Table.Columns.Contains("periodovalidez") ? Convert.IsDBNull(dr["periodovalidez"]) ? (Int32)0 : Convert.ToInt32(dr["periodovalidez"]) : (Int32)0;
            IdConvenioConfiguraAutorizacion = dr.Table.Columns.Contains("idconvenioconfiguraautorizacion") ? Convert.ToInt32(dr["idconvenioconfiguraautorizacion"]) : (Int32)0;
        }
        #endregion
    }

    public class clsListaConvenioAutorizado
    {
        #region Campos
        private List<clsConvenioAutorizado> _objelementos = new List<clsConvenioAutorizado>();
        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public List<clsConvenioAutorizado> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor vacio.
        /// </summary>
        public clsListaConvenioAutorizado()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaConvenioAutorizado(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsConvenioAutorizado(_drw));
            }
        }
        #endregion
    }
    public class clsConveniosPorFinanciar
    {
        #region Campos

        private Int32 _intidconvenioautorizado;
        private String _strnroconvenio;

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// IdConvenioAutorizado.
        /// </summary>
        public Int32 IdConvenioAutorizado
        {
            get { return _intidconvenioautorizado; }
            set { _intidconvenioautorizado = value; }
        }

        /// <summary>
        /// NroConvenio
        /// </summary>
        public String NroConvenio
        {
            get { return _strnroconvenio; }
            set { _strnroconvenio = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacio.
        /// </summary>
        public clsConveniosPorFinanciar()
        {
            IdConvenioAutorizado = Convert.ToInt32(0);
            NroConvenio = "";
        }

        /// <summary>
        /// Constructor con parametros.
        /// </summary>
        /// <param name="idconvenioautorizado"></param>
        /// <param name="nroconvenio"></param>
        public clsConveniosPorFinanciar(Int32 idconvenioautorizado
                                      , String nroconvenio)
        {
            IdConvenioAutorizado = idconvenioautorizado;
            NroConvenio = nroconvenio;
        }

        /// <summary>
        /// Constructor con DataRow.
        /// </summary>
        /// <param name="dr"></param>
        public clsConveniosPorFinanciar(DataRow dr)
        {
            if (dr == null)
                return;

            IdConvenioAutorizado = Convert.ToInt16(dr["idconvenioautorizado"]);
            NroConvenio = dr["nroconvenio"].ToString();
        }

        #endregion Constructor
    }

    public class clsListaConveniosPorFinanciar
    {
        #region Campos

        private System.ComponentModel.BindingList<clsConveniosPorFinanciar> _objelementos = new System.ComponentModel.BindingList<clsConveniosPorFinanciar>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Elementos.
        /// </summary>
        public System.ComponentModel.BindingList<clsConveniosPorFinanciar> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// clsListaConveniosPorFinanciar.
        /// </summary>
        public clsListaConveniosPorFinanciar()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaConveniosPorFinanciar(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsConveniosPorFinanciar(_drw));
            }
        }

        #endregion Constructor
    }

    public class enteConvenioResumen
    {
        #region campos

        private short _idTipoDocumentoDescuento;
        private decimal _montoADescontar;
        private decimal _montoInicial;
        private short _numeroCuotas;
        private decimal _montoCuota;
        private string _mensajeResultado;
        private decimal _totalDeudaFacturada = 0;
        private decimal _totalDeudaPorFacturar = 0;
        private decimal _totalIntereses = 0;
        private decimal _saldoAFavor = 0;
        private bool _descontarDeudaFacturada;
        private bool _descontarDeudaPorFacturar;
        private bool _descontarIntereses;
        private decimal _interesFinancierosGenerados = 0;
        private enteNroServicioDeuda _nroServicioPrincipal;
        private int _idUsuario;
        private short _idConvenioTipo;
        private short _idPuntoAtencion;
        private decimal _totalDeudaFacturadaTerceros;

        private int _periodoPago;

        private short _idEmpresa;
        public short IdEmpresa
        {
            get { return _idEmpresa; }
            set { _idEmpresa = value; }
        }

        private decimal _tasaImpuesto;
        public decimal TasaImpuesto
        {
            get { return _tasaImpuesto; }
            set { _tasaImpuesto = value; }
        }

        // Para las Garantías.

        private Int16 _esFirmaSoloTitular;
        public Int16 EsFirmaSoloTitular
        {
            get { return _esFirmaSoloTitular; }
            set { _esFirmaSoloTitular = value; }
        }

        private Int16 _idTipoIdentidad;
        public Int16 IdTipoIdentidad
        {
            get { return _idTipoIdentidad; }
            set { _idTipoIdentidad = value; }
        }

        private String _nroIdentidad;
        public String NroIdentidad
        {
            get { return _nroIdentidad; }
            set { _nroIdentidad = value; }
        }

        private clsConvenioCoParticipantes _participantes;
        public clsConvenioCoParticipantes Participantes
        {
            get { return _participantes; }
            set { _participantes = value; }
        }

        private clsConvenioCoParticipantes _garantes;
        public clsConvenioCoParticipantes Garantes
        {
            get { return _garantes; }
            set { _garantes = value; }
        }

        private clsConvenioGarantia _garantia;
        public clsConvenioGarantia Garantia
        {
            get { return _garantia; }
            set { _garantia = value; }
        }

        private enteListaNroServicioDeuda _listaNroServicio = new enteListaNroServicioDeuda();

        private decimal _inicialExigida;

        public decimal InicialExigida
        {
            get { return _inicialExigida; }
            set { _inicialExigida = value; }
        }

        private short _idTipoInteres;

        public short IdTipoInteres
        {
            get { return _idTipoInteres; }
            set { _idTipoInteres = value; }
        }

        private DateTime _fechaInicioNotificar = DateTime.Now.Date;

        public DateTime FechaInicioNotificar
        {
            get { return _fechaInicioNotificar; }
            set { _fechaInicioNotificar = value; }
        }

        private string _observacion;

        public string Observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }

        private short _idCentroServicio;

        public short IdCentroServicio
        {
            get { return _idCentroServicio; }
            set { _idCentroServicio = value; }
        }

        private short _idUnidadNegocio;

        public short IdUnidadNegocio
        {
            get { return _idUnidadNegocio; }
            set { _idUnidadNegocio = value; }
        }

        private bool _esUUNNSede;

        public bool EsUUNNSede
        {
            get { return _esUUNNSede; }
            set { _esUUNNSede = value; }
        }

        private short _idTipoAtencion;

        public short IdTipoAtencion
        {
            get { return _idTipoAtencion; }
            set { _idTipoAtencion = value; }
        }

        private clsConvenioTipoParametros _parametroConvenio;

        public clsConvenioTipoParametros ParametroConvenio
        {
            get { return _parametroConvenio; }
            set { _parametroConvenio = value; }
        }

        //private EParametroVentasExtraordinarias _parametroreapertura;

        private int _intafectoigv;
        public int AfectoIGV
        {
            get { return _intafectoigv; }
            set { _intafectoigv = value; }
        }

        #endregion

        #region propiedades

        /// <summary>
        /// ID del NroServicio donde se realizará el convenio.
        /// </summary>
        public enteNroServicioDeuda NroServicioPrincipal
        {
            get { return _nroServicioPrincipal; }
            set { _nroServicioPrincipal = value; }
        }

        /// <summary>
        /// Total de intereses calculados a la fecha.
        /// </summary>
        public decimal InteresFinancierosGenerados
        {
            get { return _interesFinancierosGenerados; }
        }

        /// <summary>
        /// Total de la deuda facturada a financiar.
        /// </summary>
        public decimal TotalDeudaFacturada
        {
            get { return this._totalDeudaFacturada; }
            set { this._totalDeudaFacturada = value; }
        }

        public decimal TotalDeudaFacturadaFinanciar
        {
            get { return (this.DescontarDeudaFacturada ? 0 : this.TotalDeudaFacturada); }
        }

        /// <summary>
        /// Total de la deuda por facturar a financiar.
        /// </summary>
        public decimal TotalDeudaPorFacturar
        {
            get { return this._totalDeudaPorFacturar; }
            set { this._totalDeudaPorFacturar = value; }
        }

        public decimal TotalDeudaPorFacturarFinanciar
        {
            get { return this.DescontarDeudaPorFacturar ? 0 : this.TotalDeudaPorFacturar; }
        }

        /// <summary>
        /// Total de intereses a financiar.
        /// </summary>
        public decimal TotalIntereses
        {
            get { return this._totalIntereses; }
            set { this._totalIntereses = value; }
        }

        public decimal TotalInteresesFinanciar
        {
            get { return (this.DescontarIntereses ? 0 : this.TotalIntereses); }
        }

        private decimal _totalDeuda;

        /// <summary>
        /// Total deuda del suministro (deuda facturada + deuda por facturar + intereses).
        /// </summary>
        public decimal TotalDeuda
        {
            get
            {
                return this.TotalDeudaFacturadaFinanciar
                    + this.TotalDeudaPorFacturarFinanciar
                    + this.TotalInteresesFinanciar;
            }
        }

        /// <summary>
        /// Total de Saldo a favor que tienen los suministros a financiar.
        /// </summary>
        public decimal SaldoAFavor
        {
            get { return _saldoAFavor; }
            set { _saldoAFavor = value; }
        }

        public decimal SaldoAFavorUsar
        {
            get { return (SaldoAFavor + MontoADescontar) > TotalDeuda ? TotalDeuda : SaldoAFavor + MontoADescontar; }
        }

        /// <summary>
        /// Tipo de Documento del Descuento.
        /// Puede ser Nota de Abono por Provisión o por Castigo.
        /// </summary>
        public short IdTipoDocumentoDescuento
        {
            get { return _idTipoDocumentoDescuento; }
            set { _idTipoDocumentoDescuento = value; }
        }

        /// <summary>
        /// Monto a descontar a la deuda.
        /// Se genera una NOTA DE ABONO por el monto a descontar.
        /// </summary>
        public decimal MontoADescontar
        {
            get { return _montoADescontar; }
            set { _montoADescontar = value; }
        }

        /// <summary>
        /// Monto Inicial a pagar para el financiamiento.
        /// </summary>
        public decimal MontoInicial
        {
            get
            {
                return _montoInicial;
            }
            set
            {
                _montoInicial = value;
            }
        }

        private decimal _montoFinanciar;

        /// <summary>
        /// Monto Total a Financiar.  Se calcula de la siguiente forma:
        /// Sumatoria: Deuda Facturada + Deuda por Facturar + Intereses.
        /// Resta: Saldo a Favor, Monto a Descontar, Monto Inicial.
        /// </summary>
        public decimal MontoFinanciar
        {
            get
            {
                return (this.TotalDeuda - (this.SaldoAFavorUsar + this.MontoInicial));
            }
            set
            {
                _montoFinanciar = value;
            }
        }

        /// <summary>
        /// Total deuda facturada por servicio de terceros.
        /// </summary>
        public decimal TotalDeudaFacturadaTerceros
        {
            get { return _totalDeudaFacturadaTerceros; }
        }

        /// <summary>
        /// Numero de cuotas convenidas para el financiamiento.
        /// </summary>
        public short NumeroCuotas
        {
            get { return _numeroCuotas; }
            set { _numeroCuotas = value; }
        }

        /// <summary>
        /// Monto de cada cuota a financiar.
        /// </summary>
        public decimal MontoCuota
        {
            get { return _montoCuota; }
            set { _montoCuota = value; }
        }

        /// <summary>
        /// Mensaje de error del financiamiento.
        /// </summary>
        public string MensajeResultado
        {
            get { return _mensajeResultado; }
            set { _mensajeResultado = value; }
        }

        /// <summary>
        /// Lista de Nro de Servicios a financiar.
        /// </summary>
        public enteListaNroServicioDeuda ListaNroServicio
        {
            get { return _listaNroServicio; }
            set { _listaNroServicio = value; }
        }

        /// <summary>
        /// Indica si se considera o no la deuda facturada para el financiamiento.
        /// </summary>
        public bool DescontarDeudaFacturada
        {
            get { return _descontarDeudaFacturada; }
            set { _descontarDeudaFacturada = value; }
        }

        /// <summary>
        /// Indica si se considera o no la deuda por facturar para el financiamiento.
        /// </summary>
        public bool DescontarDeudaPorFacturar
        {
            get { return _descontarDeudaPorFacturar; }
            set { _descontarDeudaPorFacturar = value; }
        }

        /// <summary>
        /// Indica si se descuentan los intereses calculados a la fecha.
        /// </summary>
        public bool DescontarIntereses
        {
            get { return _descontarIntereses; }
            set { _descontarIntereses = value; }
        }

        /// <summary>
        /// ID del usuario que realiza el financiamiento.
        /// </summary>
        public int IdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        /// <summary>
        /// ID del Tipo de Convenio a financiar.
        /// </summary>
        public short IdConvenioTipo
        {
            get { return _idConvenioTipo; }
            set { _idConvenioTipo = value; }
        }

        /// <summary>
        /// Periodo Comercial del pago.
        /// </summary>
        public int PeriodoPago
        {
            get { return _periodoPago; }
            set { _periodoPago = value; }
        }

        /// <summary>
        /// ID del Punto de Atención que genera el convenio.
        /// </summary>
        public short IdPuntoAtencion
        {
            get { return _idPuntoAtencion; }
            set { _idPuntoAtencion = value; }
        }

        /// <summary>
        /// Total Deuda Provisionada.
        /// </summary>
        public decimal TotalDeudaCastigada
        {
            get { return this.ListaNroServicio.TotalDeudaCastigada; }
        }

        /// <summary>
        /// Total Deuda Castigada.
        /// </summary>
        public decimal TotalDeudaProvisionada
        {
            get { return this.ListaNroServicio.TotalDeudaProvisionada; }
        }

        /// <summary>
        /// Parámetro que permite generar los registros para la Solicitud, Presupuesto, Orden de Cobro 
        /// de Reapertura y Actualizar el Contrato.
        /// </summary>
        //public EParametroVentasExtraordinarias ParametroGenerarReapertura
        //{
        //    set { this._parametroreapertura = value; }
        //    get { return this._parametroreapertura; }
        //}

        #endregion

        #region constructor

        /// <summary>
        /// constructor predeterminado.
        /// </summary>
        public enteConvenioResumen()
        {
            _participantes = new clsConvenioCoParticipantes();
            _garantes = new clsConvenioCoParticipantes();
            _garantia = new clsConvenioGarantia();
            _parametroConvenio = new clsConvenioTipoParametros();
            _listaNroServicio = new enteListaNroServicioDeuda();
        }

        /// <summary>
        /// Agregar suministros con su deuda a financiar.
        /// </summary>
        /// <param name="oresumendeuda"></param>
        public void AgregarDeuda(enteListaNroServicioDeuda olistaNroServicioDeuda)
        {
            if (olistaNroServicioDeuda == null) return;

            this._listaNroServicio.AgregarElementoRango(olistaNroServicioDeuda);

            this._totalDeudaFacturada += olistaNroServicioDeuda.TotalDeudaFacturadaFinanciar;
            this._totalDeudaPorFacturar += olistaNroServicioDeuda.TotalDeudaPorFacturarFinanciar;
            this._totalIntereses += olistaNroServicioDeuda.TotalIntereses;
            this._totalDeudaFacturadaTerceros += olistaNroServicioDeuda.TotalDeudaFacturadaTerceros;

            this._totalDeuda = (this._totalIntereses + this._totalDeudaFacturada + this._totalDeudaPorFacturar);

            this._nroServicioPrincipal = this._listaNroServicio.Elementos[0] as enteNroServicioDeuda;
            this._saldoAFavor = this._nroServicioPrincipal.TotaldocumentosAFavor;
        }

        public void AgregarDeudaNroServicio(enteNroServicioDeuda oNroServicioDeuda)
        {
            if (oNroServicioDeuda == null) return;

            this._listaNroServicio.AgregarElemento(oNroServicioDeuda);

            this._totalDeudaFacturada += oNroServicioDeuda.ListaDeudaFacturada.TotalDeudaFinanciar;
            this._totalDeudaPorFacturar += oNroServicioDeuda.ListaDeudaPorFacturar.TotalDeudaPorFacturar;
            this._totalIntereses += oNroServicioDeuda.ListaIntereses.TotalIntereses;
            this._totalDeudaFacturadaTerceros += oNroServicioDeuda.ListaDeudaFacturada.TotalDeudaTerceros;

            this._totalDeuda = (this._totalIntereses + this._totalDeudaFacturada + this._totalDeudaPorFacturar);

            this._nroServicioPrincipal = this._listaNroServicio.Elementos[0] as enteNroServicioDeuda;
            this._saldoAFavor = this._nroServicioPrincipal.TotaldocumentosAFavor;
        }

        public void AgregarDeudaResumen(enteConvenioResumen oconvenioresumen)
        {
            if (oconvenioresumen == null) return;

            this._listaNroServicio.AgregarElementoRango(oconvenioresumen.ListaNroServicio);

            this._totalDeudaFacturada += oconvenioresumen.ListaNroServicio.TotalDeudaFacturadaFinanciar;
            this._totalDeudaPorFacturar += oconvenioresumen.ListaNroServicio.TotalDeudaPorFacturarFinanciar;
            this._totalIntereses += oconvenioresumen.ListaNroServicio.TotalIntereses;
            this._totalDeudaFacturadaTerceros += oconvenioresumen.ListaNroServicio.TotalDeudaFacturadaTerceros;

            this._totalDeuda = (this._totalIntereses + this._totalDeudaFacturada + this._totalDeudaPorFacturar);

            this._nroServicioPrincipal = this._listaNroServicio.Elementos[0] as enteNroServicioDeuda;
            this._saldoAFavor = this._nroServicioPrincipal.TotaldocumentosAFavor;
        }

        public void QuitarRegistroDeuda(int indiceNroServicioDeudaEliminar)
        {
            if (indiceNroServicioDeudaEliminar < 0) return;

            enteNroServicioDeuda oresumendeuda = this._listaNroServicio.ElementosGrilla[indiceNroServicioDeudaEliminar] as enteNroServicioDeuda;

            this._totalDeudaFacturada -= oresumendeuda.DeudaFacturada;
            this._totalDeudaPorFacturar -= oresumendeuda.DeudaPorFacturar;
            this._totalIntereses -= oresumendeuda.InteresesCalculados;
            this._totalDeudaFacturadaTerceros -= oresumendeuda.DeudaFacturadaTerceros;

            this._listaNroServicio.EliminarElemento(indiceNroServicioDeudaEliminar);

            if (this._listaNroServicio == null
                || this._listaNroServicio.Elementos == null
                || this._listaNroServicio.Elementos.Count == 0)
            {
                this._nroServicioPrincipal = null;
                this._saldoAFavor = 0;
            }
            else
            {
                this._nroServicioPrincipal = this._listaNroServicio.Elementos[0] as enteNroServicioDeuda;
                this._saldoAFavor = this._nroServicioPrincipal.TotaldocumentosAFavor;
            }
        }

        public void ActualizarDocumentoAFavor(int idNroServicioDeuda, enteListaNroServicioDocumentoAFavor listaDocAFavor)
        {
            // Actualizar Saldo a Favor.
            foreach (enteNroServicioDeuda onroservicio in _listaNroServicio.Elementos)
            {
                if (onroservicio.IdNroServicio.Equals(idNroServicioDeuda))
                {
                    onroservicio.ListaDocumentosAFavor.SetearDocumentosAFavor(listaDocAFavor);
                }
            }

            this._nroServicioPrincipal = this._listaNroServicio.Elementos[0] as enteNroServicioDeuda;
            this._saldoAFavor = this._nroServicioPrincipal.TotaldocumentosAFavor;
        }

        #endregion

    }
    public class clsConvenioTipoParametros
    {
        #region Campos
        private Int16 _intidmoneda;
        private Int16 _intiddocumentocomercial;
        private Int16 _intplazomaximo;
        private Decimal _decporcinicial;
        private Decimal _decvalortasa;
        private Int16 _intidtasainteres;
        private Int16 _intidrol;
        private String _strnombrerol;
        private Int16 _intgeneracorte;
        private List<Int16> _lstidtipogarantia;

        private Int16? _intidtipointeres;
        #endregion

        #region Propiedades
        /// <summary>
        /// IdMoneda
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }
        /// <summary>
        /// IdDocumentoComercial.
        /// </summary>
        public Int16 IdDocumentoComercial
        {
            get { return _intiddocumentocomercial; }
            set { _intiddocumentocomercial = value; }
        }
        /// <summary>
        /// PlazoMaximo.
        /// </summary>
        public Int16 PlazoMaximo
        {
            get { return _intplazomaximo; }
            set { _intplazomaximo = value; }
        }
        /// <summary>
        /// PorcentajeInicial.
        /// </summary>
        public Decimal PorcentajeInicial
        {
            get { return _decporcinicial; }
            set { _decporcinicial = value; }
        }

        private Decimal _tamMensual;
        private Decimal _tipMensual;

        /// <summary>
        /// Tasa TAM Mensual
        /// </summary>
        public Decimal TamMensual
        {
            get { return _tamMensual; }
            set { _tamMensual = value; }
        }

        /// <summary>
        /// Tasa TIP Mensual
        /// </summary>
        public Decimal TipMensual
        {
            get { return _tipMensual; }
            set { _tipMensual = value; }
        }

        /// <summary>
        /// ValorTasa a aplicar en convenio.
        /// </summary>
        public Decimal ValorTasa
        {
            get { return _decvalortasa; }
            set { _decvalortasa = value; }
        }
        /// <summary>
        /// IdTasaInteres.
        /// </summary>
        public Int16 IdTasaInteres
        {
            get { return _intidtasainteres; }
            set { _intidtasainteres = value; }
        }
        /// <summary>
        /// IdRol.
        /// </summary>
        public Int16 IdRol
        {
            get { return _intidrol; }
            set { _intidrol = value; }
        }
        /// <summary>
        /// NombreRol.
        /// </summary>
        public String NombreRol
        {
            get { return _strnombrerol; }
            set { _strnombrerol = value; }
        }

        /// <summary>
        /// GeneraCorte.
        /// </summary>
        public Int16 GeneraCorte
        {
            get { return _intgeneracorte; }
            set { _intgeneracorte = value; }
        }
        /// <summary>
        /// LstIdTipoGarantia.
        /// </summary>
        public List<Int16> LstIdTipoGarantia
        {
            get { return _lstidtipogarantia; }
            set { _lstidtipogarantia = value; }
        }

        /// <summary>
        /// GeneraCorte.
        /// </summary>
        public Int16? IdTipoInteres
        {
            get { return _intidtipointeres; }
            set { _intidtipointeres = value; }
        }
        #endregion

        private int _numeroDiasMinimoFacturacion;
        private int _numeroDiasMaximoFacturacion;

        /// <summary>
        /// Número de días Minimo para inicio de la facturación.
        /// </summary>
        public int NumeroDiasMinimoFacturacion
        {
            get { return _numeroDiasMinimoFacturacion; }
            set { _numeroDiasMinimoFacturacion = value; }
        }

        /// <summary>
        /// Número de dias máximo para facturar.
        /// </summary>
        public int NumeroDiasMaximoFacturacion
        {
            get { return _numeroDiasMaximoFacturacion; }
            set { _numeroDiasMaximoFacturacion = value; }
        }

        private decimal _valorUIT;

        public decimal ValorUIT
        {
            get { return _valorUIT; }
            set { _valorUIT = value; }
        }


        #region Constructor
        /// <summary>
        /// Constructor Vacio
        /// </summary>
        public clsConvenioTipoParametros()
        {
            _lstidtipogarantia = new List<Int16>();
        }

        /// <summary>
        /// clsConvenioTipoParametros.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="dtLista"></param>
        public clsConvenioTipoParametros(DataRow dr, DataTable dtLista)
        {
            //if (dr == null || dtLista == null || dtLista.Rows.Count == 0) { return; }
            if (dr == null || dtLista == null) { return; }

            this._tamMensual = Convert.ToDecimal(dr["tammensual"]);
            this._tipMensual = Convert.ToDecimal(dr["tipmensual"]);
            this.IdMoneda = Convert.ToInt16(dr["idmoneda"]);
            this.IdDocumentoComercial = Convert.ToInt16(dr["iddocumentocomercial"]);
            this.PlazoMaximo = Convert.ToInt16(dr["plazomaximo"]);
            this.PorcentajeInicial = Convert.ToDecimal(dr["porcentajeinicial"]);
            this.ValorTasa = Convert.ToDecimal(dr["valortasa"]);
            this.IdTasaInteres = Convert.ToInt16(dr["idtasainteres"]);
            this.IdRol = Convert.ToInt16(dr["idrol"]);
            this.NombreRol = dr["nombrerol"].ToString();
            this.GeneraCorte = Convert.ToInt16(dr["generacorte"]);
            this.NumeroDiasMinimoFacturacion = Convert.ToInt32(dr["NumeroDiasMinimoFacturacion"]);
            this.NumeroDiasMaximoFacturacion = Convert.ToInt32(dr["NumeroDiasMaximoFacturacion"]);

            if (dr.Table.Columns.Contains("idtipointeres"))
                this.IdTipoInteres = Convert.IsDBNull(dr["idtipointeres"]) ? (Int16?)null : Convert.ToInt16(dr["idtipointeres"]);

            this._lstidtipogarantia = new List<Int16>();

            // Llenar Lista
            foreach (DataRow drgarantias in dtLista.Rows)
            {
                LstIdTipoGarantia.Add(Convert.ToInt16(drgarantias[0]));
            }
        }
        /// <summary>
        /// Agregado por: Carlos López.
        /// </summary>
        /// <param name="dr"></param>
        public clsConvenioTipoParametros(DataRow dr)
        {
            if (dr == null) { return; }

            this.IdMoneda = Convert.IsDBNull(dr["moneda"]) ? Convert.ToInt16(null) : Convert.ToInt16(dr["moneda"]);
            this.IdDocumentoComercial = Convert.IsDBNull(dr["idtipodocumentocomercial"]) ? Convert.ToInt16(null) : Convert.ToInt16(dr["idtipodocumentocomercial"]);
            this.PlazoMaximo = (dr.Table.Columns.Contains("plazomaximo")) ? Convert.IsDBNull(dr["plazomaximo"]) ? Convert.ToInt16(null) : Convert.ToInt16(dr["plazomaximo"]) : Convert.ToInt16(0);
            this.PorcentajeInicial = (dr.Table.Columns.Contains("porcentajeinicial")) ? Convert.IsDBNull(dr["porcentajeinicial"]) ? Convert.ToDecimal(null) : Convert.ToDecimal(dr["porcentajeinicial"]) : Convert.ToDecimal(0);
            this.IdTasaInteres = Convert.IsDBNull(dr["idtasainteres"]) ? Convert.ToInt16(null) : Convert.ToInt16(dr["idtasainteres"]);
            this.ValorTasa = Convert.IsDBNull(dr["valortasainteres"]) ? Convert.ToDecimal(null) : Convert.ToDecimal(dr["valortasainteres"]);

            if (dr.Table.Columns.Contains("generacorte")) this.GeneraCorte = Convert.ToInt16(dr["generacorte"]);
            if (dr.Table.Columns.Contains("valoruit")) this._valorUIT = Convert.ToDecimal(dr["valoruit"]);
        }

        #endregion
    }
    public class clsConvenioCoParticipantes
    {
        #region Campos
        private String _strnroconvenio;
        private Int16 _intidtipoparticipante;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private String _strnombreparticipante;
        private String _strdireccionparticipante;
        private Int32 _intidregistroconvenio;
        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdRegistroConvenio
        {
            get { return _intidregistroconvenio; }
            set { _intidregistroconvenio = value; }
        }

        /// <summary>
        /// NroConvenio.
        /// </summary>
        public String NroConvenio
        {
            get { return _strnroconvenio; }
            set { _strnroconvenio = value; }
        }
        /// <summary>
        /// IdTipoParticipante.
        /// </summary>
        public Int16 IdTipoParticipante
        {
            get { return _intidtipoparticipante; }
            set { _intidtipoparticipante = value; }
        }
        /// <summary>
        /// IdTipoIdentidad.
        /// </summary>
        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }
        /// <summary>
        /// NroIdentidad.
        /// </summary>
        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }
        /// <summary>
        /// NombreParticipante
        /// </summary>
        public String NombreParticipante
        {
            get { return _strnombreparticipante; }
            set { _strnombreparticipante = value; }
        }
        /// <summary>
        /// DireccionParticipante.
        /// </summary>
        public String DireccionParticipante
        {
            get { return _strdireccionparticipante; }
            set { _strdireccionparticipante = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// clsConvenioCoParticipantes.
        /// </summary>
        public clsConvenioCoParticipantes()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        public clsConvenioCoParticipantes(DataRow dr)
        {
            if (dr != null)
            {
                NroConvenio = dr["nroconvenio"].ToString();
                IdTipoParticipante = Convert.ToInt16(dr["idtipoparticipante"]);
                IdTipoIdentidad = Convert.ToInt16(dr["nroidentidad"]);
                NroIdentidad = dr["idtipoidentidad"].ToString();
                NombreParticipante = dr["nombreparticipante"].ToString();
                DireccionParticipante = dr["direccionparticipante"].ToString();
            }
        }
        #endregion
    }
    public class clsConvenioGarantia
    {
        #region Campos
        private String _strnroconvenio;
        private Int16 _intidtipogarantia;
        private String _strnrogarantia;
        private DateTime _datfechavencimiento;
        private Decimal _decimportegarantia;
        private String _strtipogarantia;
        private Int32 _intidregistroconvenio;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>    
        public Int32 IdRegistroConvenio
        {
            get { return _intidregistroconvenio; }
            set { _intidregistroconvenio = value; }
        }
        /// <summary>
        /// NroConvenio.
        /// </summary>
        public String NroConvenio
        {
            get { return _strnroconvenio; }
            set { _strnroconvenio = value; }
        }
        /// <summary>
        /// IdTipoGarantia.
        /// </summary>
        public Int16 IdTipoGarantia
        {
            get { return _intidtipogarantia; }
            set { _intidtipogarantia = value; }
        }
        /// <summary>
        /// NroGarantia.
        /// </summary>
        public String NroGarantia
        {
            get { return _strnrogarantia; }
            set { _strnrogarantia = value; }
        }
        /// <summary>
        /// FechaVencimiento.
        /// </summary>
        public DateTime FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }
        /// <summary>
        /// ImporteGarantia.
        /// </summary>
        public Decimal ImporteGarantia
        {
            get { return _decimportegarantia; }
            set { _decimportegarantia = value; }
        }
        /// <summary>
        /// NroConvenio.
        /// </summary>
        public String TipoGarantia
        {
            get { return _strtipogarantia; }
            set { _strtipogarantia = value; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// clsConvenioGarantia.
        /// </summary>
        public clsConvenioGarantia()
        {
        }

        public clsConvenioGarantia(DataRow dr)
        {
            if (dr == null) { return; }
            DataColumnCollection col = dr.Table.Columns;

            if (col.Contains("nroconvenio")) { NroConvenio = dr["nroconvenio"].ToString(); }
            if (col.Contains("idtipogarantia")) { IdTipoGarantia = Convert.ToInt16(dr["idtipogarantia"]); }
            if (col.Contains("nrogarantia")) { NroGarantia = dr["nrogarantia"].ToString(); }
            if (col.Contains("fechavencimiento")) { FechaVencimiento = Convert.ToDateTime(dr["fechavencimiento"]); }
            if (col.Contains("importegarantizado")) { ImporteGarantia = Convert.ToDecimal(dr["importegarantizado"]); }
            if (col.Contains("tipogarantia")) { TipoGarantia = dr["tipogarantia"].ToString(); }

        }

        #endregion
    }
    #endregion NroServicioDeuda
    #region Enumeradores
    public enum EstadoGenerales
    {
        /// Estados generales para los objetos de negocio
        /// Rango: 0 a 19
        /// ---------------------------------------------
        /// <summary>
        /// Registro "inactivo" para la entidad
        /// </summary>
        Inactivo = 0,

        /// <summary>
        /// Registro "activo" de la entidad
        /// </summary>
        Activo = 1,

        /// <summary>
        /// Registro "anulado" de la entidad
        /// </summary>
        Anulado = 2,

        /// <summary>
        /// Registro "borrado" de la entidad
        /// </summary>
        Borrado = 3,

        /// <summary>
        /// Registro "eventual" de la entidad
        /// </summary>
        Eventual = 4,

        /// <summary>
        ///
        /// </summary>
        Pendiente = 5,

        /// ------------------------------------------------------------
        /// Estados para los objetos de negocio de Modulo de Facturación
        /// Rango: 20 a 49
        /// <summary>
        /// Registro cortado para la entidad
        /// </summary>
        Cortado = 20,

        /// <summary>
        /// Registro "retirado" de la entidad
        /// </summary>
        Retirado = 22,

        /// <summary>
        /// Facturacion en proceso.
        /// </summary>
        EnProceso = 21,

        /// <summary>
        /// Facturacion Vigente o Corriente o En Cobranza.
        /// </summary>
        Vigente = 23,

        NoVigente = 204,

        /// <summary>
        /// Registro retirado definitivo.
        /// </summary>
        RetiradoDefinitivo = 25,

        /// <summary>
        ///
        /// </summary>
        Desestimado = 26,

        /// <summary>
        /// Estado incial del NroServicio, al momento de insertar
        /// a partir de un cobro de presupuesto por solicitud del
        /// cliente por instalación de nuevo suministro.
        /// </summary>
        NuevoSuministro = 27,

        ///Estados para Cronograma
        /// <summary>
        /// Estado para Cronograma
        /// </summary>
        EnRevision = 30,

        /// <summary>
        ///
        /// </summary>
        PorAprobar = 31,

        /// <summary>
        ///
        /// </summary>
        Aprobado = 32,

        /// <summary>
        ///
        /// </summary>
        Incompleto = 203,

        // ---------------------------------------------------------------------------
        // A partir de aqui es para los estados de la compensación por Interrupciones.
        //
        /// <summary>
        /// Estado para cuando ya se ha realizado la compensación por interrupciones LCE.
        /// </summary>
        Compensado = 40,

        /// <summary>
        /// Estado para cuando ya se a incluido en la facturación el registro compensado.
        /// </summary>
        Facturado = 41,

        /// <summary>
        ///
        /// </summary>
        Rechazado = 166,

        /// ------------------------------------------------------------
        /// Estados para los objetos de negocios de Modulo de Cobranzas
        /// Rango: 50a 79
        /// <summary>
        /// Registro "no pagado" de la entidad
        /// </summary>
        NoPagado = 50,

        /// <summary>
        /// Registro "pagado" de la entidad
        /// </summary>
        Pagado = 52,

        /// <summary>
        /// Registro "financiado" de la entidad
        /// </summary>
        Financiado = 54,

        /// <summary>
        /// Registro "vencido" de la entidad
        /// </summary>
        Vencido = 62,

        /// <summary>
        /// Registro "moroso" de la entidad
        /// </summary>
        Moroso = 64,

        /// <summary>
        ///
        /// </summary>
        PorProvisionar = 69,

        /// <summary>
        /// Registro "provisionado" de la entidad
        /// </summary>
        Provisionado = 70,

        /// <summary>
        ///
        /// </summary>
        PorCastigar = 71,

        /// <summary>
        /// Registro "Castigado" de la entidad
        /// </summary>
        Castigado = 72,

        /// <summary>
        /// Registro "Por Confirmar" de la entidad
        /// </summary>
        PorConfirmar = 74,

        /// <summary>
        /// Registro "Pendiente Por Cuota Inicial" de la entidad
        /// </summary>
        PendientePorCI = 76,

        /// ------------------------------------------------------------
        /// Estados para los objetos de negocios de Modulo Contable
        /// Rango: 80 al 99
        /// <summary>
        /// Registro "Registrado" de la entidad
        /// </summary>
        Registrado = 80,

        /// <summary>
        /// Registro "Contabilizado" de la entidad
        /// </summary>
        Contabilizado = 82,

        /// <summary>
        /// Registro "extornado" de la entidad
        /// </summary>
        Extornado = 84,

        /// <summary>
        /// Registro "extorno parcial" de la entidad
        /// </summary>
        ExtornoParcial = 85,

        /// <summary>
        ///
        /// </summary>
        Abierto = 90,

        /// <summary>
        ///
        /// </summary>
        Abrir = 91,

        /// <summary>
        ///
        /// </summary>
        Suspendido = 92,

        /// <summary>
        ///
        /// </summary>
        Suspender = 93,

        /// <summary>
        ///
        /// </summary>
        QuitarSuspension,

        /// <summary>
        ///
        /// </summary>
        CerrarCajero = 95,

        /// <summary>
        ///
        /// </summary>
        EnCierre = 96,

        /// <summary>
        ///
        /// </summary>
        Bloqueado = 97,

        /// <summary>
        ///
        /// </summary>
        Cerrado = 98,

        /// Estado para el ENVIO DE DINERO.
        /// <summary>
        ///
        /// </summary>
        ABoveda = 100,

        /// <summary>
        ///
        /// </summary>
        EnCustodia = 101,

        /// <summary>
        ///
        /// </summary>
        Remitido = 102,

        /// <summary>
        /// Estado para el INTERCAMBIO.
        /// </summary>
        Creado = 110,

        /// <summary>
        ///
        /// </summary>
        Preparando = 111,

        /// <summary>
        ///
        /// </summary>
        Configurado = 112,

        /// <summary>
        ///
        /// </summary>
        Asignado = 113,

        /// <summary>
        /// Tambien se usa para cuando la fecha de validez de la solicitid de autorización para hacer
        /// un convenio fuera de los parametros se vence y no se hace el convenio. (NCA).
        /// </summary>
        Caducado = 114,

        /// <summary>
        ///
        /// </summary>
        Enviado = 115,

        /// <summary>
        ///
        /// </summary>
        Recepcionado = 116,

        /// <summary>
        ///
        /// </summary>
        Procesando = 117,

        /// <summary>
        ///
        /// </summary>
        Procesado = 118,

        /// <summary>
        ///
        /// </summary>
        Abortado = 119,

        /// <summary>
        ///
        /// </summary>
        EnDefinicion = 150,

        /// <summary>
        /// Tarea sin Iniciar o en cola
        /// </summary>
        EnCola = 160,

        /// <summary>
        /// Tarea Iniciada
        /// </summary>
        Ejecutando = 161,

        /// <summary>
        /// Tarea Pausada
        /// </summary>
        Pausado = 162,

        /// <summary>
        /// Tarea en proceso de cancelación
        /// </summary>
        Cancelando = 163,

        /// <summary>
        /// Tarea en proceso de reanudación
        /// </summary>
        Reanudar = 164,

        /// <summary>
        /// Tarea Finalizada
        /// </summary>
        Terminado = 165,

        /// <summary>
        /// Tarea en proceso de Reinicio
        /// </summary>
        Reiniciando = 167,

        /// <summary>
        /// Tarea Finalizada con Cancelación
        /// </summary>
        Cancelado = 168,

        /// <summary>
        /// Tarea Finalizada con Errores
        /// </summary>
        Fallado = 169,

        /// <summary>
        /// Orden de Trabajo Generado (cabecera).
        /// También se usa para la data que se genera en Cortes para poder crear las O.T.
        /// </summary>
        Generado = 170,

        /// <summary>
        /// Orden de Trabajo entregado a Contratista
        /// </summary>
        Entregado = 171,

        /// <summary>
        /// La OT esta procesandose
        /// </summary>
        EnEjecucion = 172,

        /// <summary>
        /// Se Recibio la totalidad de la OT.
        /// </summary>
        Recibido = 173,

        /// <summary>
        /// Se proceso totalmente la OT
        /// </summary>
        Ejecutado = 174,

        /// <summary>
        ///
        /// </summary>
        ParaInspeccion = 176,

        /// <summary>
        ///
        /// </summary>
        EnInspeccion = 178,

        /// <summary>
        ///
        /// </summary>
        ParaCalculo = 175,

        /// <summary>
        ///
        /// </summary>
        Validado = 177,

        /// <summary>
        ///
        /// </summary>
        Errado = 179,

        /// <summary>
        ///
        /// </summary>
        Valorizado = 191,

        /// <summary>
        ///
        /// </summary>
        ParaValidar = 192,

        /// <summary>
        ///
        /// </summary>
        Aplicado = 202,

        /// <summary>
        /// Estado Emitido de un Lote de Facturacion
        /// </summary>
        Emitido = 190,

        /// <summary>
        /// Estado para representar un Error en la Emisión.
        /// </summary>
        ErrorEmision = 193,

        /// <summary>
        /// Estado para representar que faltan sectores por Emitir.
        /// </summary>
        EmisionIncompleta = 194,

        /// <summary>
        /// Estado EnLote del Suministro
        /// </summary>
        EnLote = 207,

        /// <summary>
        /// Estado Normal de Suministro luego que ha sido impreso
        /// </summary>
        Normal = 201,

        /// <summary>
        /// Estado del Suministro en Lote cuando va a pasar a Retiro
        /// </summary>
        PorRetirar = 280,

        /// <summary>
        /// -----------------------------------------------------------------------------------------
        /// Estados para el convenio.
        /// Rango del 140 - 149
        /// --
        /// Todo convenio que no necesita autorización nace con el estado: PorFirmar.
        /// </summary>
        PorFirmar = 140,

        /// <summary>
        /// Todo convenio que necesita autorización nace con el estado: PorAutorizar.
        /// </summary>
        PorAutorizar = 141,

        /// <summary>
        /// Cuando el saldo de un convenio es nuevamente financiado.
        /// </summary>
        Refinanciado = 142,

        /// <summary>
        /// Cuando se ha calculado las cuotas del convenio y se esta a la espera del OK de facturación.
        /// </summary>
        EnFacturacion = 143,

        /// <summary>
        /// Cuando se va a retirar al NroServicio por acumulación de 8 meses de deuda.
        /// </summary>
        VencidoPorIncumplimiento = 144,

        /// <summary>
        /// Cuando la autorización para hacer un convenio fuera de los parametros fue dada.
        /// </summary>
        Autorizado = 145,

        Robado = 216,

        /// <summary>
        /// --------------------------------------------
        /// Estados para La Solicitud Servicio
        /// Los valores no son correlativos por que estan en distintos grupos
        ///
        /// Primer estado de la Solicitud
        /// </summary>
        Solicitado = 206,

        /// <summary>
        /// Estado de atendido de la solicitud
        /// </summary>
        Atendido = 221,

        /// <summary>
        /// Estado en que el Servicio Nuevo Suministro pasa a Factible, despues de la evaluacion tecnica
        /// </summary>
        Factible = 222,

        /// <summary>
        /// Estado en que el Servicio Nuevo Suministro para a no factible cuando no es aprobada la evaluacion tecnica
        /// </summary>
        NoFactible = 223,

        /// <summary>
        /// Estado de la solicitud que se asigna despues de determinado la factibilidad
        /// </summary>
        Presupuestado = 224,

        /// <summary>
        /// Estado de la solicitud que se asigna despues que el cliente paga su presupuesto
        /// </summary>
        EnInstalacion = 225,

        /// <summary>
        /// Estado de la solicitud que se asigna cuando ya instalaron el suministro
        /// </summary>
        Instalado = 226,

        /// <summary>
        /// Estado de solicitud que se asigna cuando de la orden de factibilidad no se especifica si
        /// es factible o no la instalación.
        /// </summary>
        ConObservacion = 227,

        /// <summary>
        /// Estado que adopta la solicitud luego de imprimir el contrato.
        /// </summary>
        ConContrato = 228,

        /// <summary>
        /// Estado que adopta la solicitud se nuevo suministro luego de cancelar la orden de cobro.
        /// </summary>
        CanceladoConContrato = 229,

        /// <summary>
        /// Estado de la solicitud antes de generar el presupuesto.
        /// </summary>
        PorPresupuesto = 262,

        /// <summary>
        /// --------------------------------------------
        /// Estados para Atención a Clientes y Gestión
        /// de reclamos.
        /// Los valores no son correlativos por que estan en distintos grupos
        Finalizado = 220,

        /// <summary>
        /// Incida que el solicitante presento parte de los documentos
        /// necesarios para admitir la atención
        /// </summary>
        PreAdmisible = 282,

        /// <summary>
        /// Indica que el solicitante presento todos los documentos necesarios
        /// para admitir la atencion.
        /// </summary>
        Admisible = 251,

        /// <summary>
        /// Indica que el solicitante no presento todos los documentos necesarios
        /// para admitir la atencion.
        /// </summary>
        Inadmisible = 252,

        /// <summary>
        /// Indica que la atencion se ha declarado procedente para la atención.
        /// </summary>
        Procedente = 253,

        /// <summary>
        /// Indica que la atencion se ha declarado improcedente para la atención.
        /// </summary>
        Improcedente = 254,

        /// <summary>
        /// Se concilio un acuerdo entre las partes tras el proceso de reclamo.
        /// </summary>
        Conciliado = 255,

        /// <summary>
        /// No se concilio un acuerdo entre las partes tras el proceso de reclamo.
        /// </summary>
        NoConciliado = 256,

        /// <summary>
        /// Se concilio un acuerdo parcial entre las partes tras el proceso de reclamo.
        /// </summary>
        ConciliadoParcial = 257,

        /// <summary>
        /// Se declaro fundado el reclamo en la resolución determinada por la concesionaria.
        /// </summary>
        Fundado = 258,

        /// <summary>
        /// Se declaro infundado el reclamo en la resolución determinada por la concesionaria.
        /// </summary>
        Infundado = 259,

        /// <summary>
        /// Se declaro fundado en parte el reclamo en la resolución determinada por la concesionaria.
        /// </summary>
        FundadoenParte = 260,

        /// <summary>
        /// El reclamo fue reconsiderado por la concesionario a petición del solicitante.
        /// </summary>
        Reconsiderado = 261,

        /// <summary>
        /// La solución del motivo es justificada.
        /// </summary>
        Justificado = 263,

        /// <summary>
        /// La solución del motivo es insjustificada.
        /// </summary>
        Injustificado = 264,

        /// <summary>
        /// La solucion del motivo es consultado.
        /// </summary>
        Consultado = 265,

        /// <summary>
        /// Sin Acta de Solución
        /// </summary>
        SinActaSolución = 266,

        /// <summary>
        /// Resolucion emitida por osinerg
        /// </summary>
        Nula = 272,

        /// <summary>
        /// No Asistió a Cita.
        /// </summary>
        NoAsistio = 277,

        /// <summary>
        /// --------------------------------------------
        /// Estados para Ordenes de Trabajo.
        /// Los valores no son correlativos por que estan en distintos grupos
        /// </summary>
        NoEjecutado = 240,

        /* Estado Pronunciamiento Ente Regulador */

        /// <summary>
        ///
        /// </summary>
        Revocar = 273,

        /// <summary>
        ///
        /// </summary>
        RevocarEnParte = 274,

        /// <summary>
        ///
        /// </summary>
        CareceDeObjeto = 275,

        /// <summary>
        ///
        /// </summary>
        Rescindido = 281,

        /// <summary>
        ///
        /// </summary>
        Acumulado = 283,

        /// <summary>
        ///
        /// </summary>
        EnTransicion = 284,

        /// <summary>
        ///
        /// </summary>
        Usado = 211,

        /// <summary>
        ///
        /// </summary>
        NoDisponible = 217,

        /// <summary>
        ///
        /// </summary>
        Ponderado = 205,

        /// <summary>
        ///
        /// </summary>
        ///

        ConfirmarParte = 276,

        /// <summary>
        ///
        /// </summary>
        PCPropuesta = 287,

        /// <summary>
        ///
        /// </summary>
        PCPreAutorizado = 288,

        /// <summary>
        ///
        /// </summary>
        PCAutorizado = 289,

        /// <summary>
        ///
        /// </summary>
        Grabando = 290,

        /// <summary>
        ///
        /// </summary>
        EnLlamada = 291,

        /// <summary>
        /// Estado para los lotes de Facturación que cuando se encuentran
        /// preparados para valorizar.
        /// </summary>
        ParaValorizar = 295,

        /// <summary>
        /// Estado para los lotes de Facturación para cuando se
        /// encuentran en proceso de Valorización.
        /// </summary>
        EnValorizacion = 296,

        /// <summary>
        /// Estado para los lotes de Facturación para cuando se
        /// encuentran en proceso de Emisión.
        /// </summary>
        EnEmision = 297,

        /// <summary>
        ///
        /// </summary>
        Desistido = 301,

        /// <summary>
        ///
        /// </summary>
        Inspeccionado = 320,

        /// <summary>
        ///
        /// </summary>
        EnIntervención = 321,

        /// <summary>
        ///
        /// </summary>
        Intervenido = 322,

        /// <summary>
        ///
        /// </summary>
        ConCodigoAutogenerado = 323,

        /// <summary>
        ///
        /// </summary>
        Pactado = 324,

        /// <summary>
        ///
        /// </summary>
        Calculado = 326,

        /// <summary>
        ///
        /// </summary>
        PagadoParcial = 330,

        /// <summary>
        ///
        /// </summary>
        PagoDeudaFacturada = 331,

        /// <summary>
        ///
        /// </summary>
        Distribuido = 332,

        /// <summary>
        ///
        /// </summary>
        Sustentado = 333,

        /// <summary>
        ///
        /// </summary>
        CargadoSAP = 334,

        // Estado de llamadas al CallCenter

        NoAtendida = 336,

        NoContestada = 337,
        NoCompensa = 42,
        SiCompensa = 43,
        NoDefinido = 44,
        EnTramite = 342,

        // Medidor

        Nuevo = 210,
        Adecuado = 212,
        Reciclado = 213,
        Decomisado = 214,
        DadoDeBaja = 215,
        Calidad = 310,
        MalaCalidad = 311,

        ConFiltro = 344,
        Confirmado = 345,

        EnReclamo = 335,

        PorNotifPreviaIntervencion = 346,
        NotifPreviaIntervencion = 347,
        PorNotifContraste = 348,
        NotifContraste = 349,
        PorContraste = 350,
        Contrastado = 351,
        PorNotifReintegroRecupero = 352,
        NotifReintegroRecupero = 353,
        Publicado = 338,
        PorConciliar = 51,

        PorConciliarFormaPago = 355,
        ConciliadoFormaPago = 356,

        NoSolicitado = 354,
        PorGenerarOC = 361,
        PorTransferir = 365,
        ConInforme = 371,
        Programado = 357,
        Confirmar = 267,
        Excluido = 380,
        AnalisisPruebas = 382,
        Personal = 383,
        Impreso = 384,
        Guardado = 385,
        Notificado = 396 //DISTRILUZ
    }
    public enum TipoDocumentoIdentidad
    {
        /// <summary>
        /// DNI = 1
        /// </summary>
        DNI = 1,

        /// <summary>
        /// RUC = 3
        /// </summary>
        RUC = 3
    }
    public enum ServicioDistribucionEnergia
    {
        EnergiaPostPago = 1,
        EnergíaLibre = 10,
        Prepago = 11,
        Temporal = 12,
        Provisional = 14,
    }
}
#endregion Enumeradores


