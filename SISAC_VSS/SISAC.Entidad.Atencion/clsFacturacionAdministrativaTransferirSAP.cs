﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Entidad.Atencion
{

    public class clsFacturacionAdministrativaTransferirSAP
    {
        public Int16 IdEmpresa { get; set; }
        public Int16 IdTipoDocumento { get; set; }
        public String NroDocumento { get; set; }
        public Int16 IdCentroServicio { get; set; }
        public Int16 IdMovimientoComercial { get; set; }
        public String RutaArchivo { get; set; }
        public Int16 GenerarXML { get; set; }
        public Int16 GenerarPDF { get; set; }
        public Int16 IdUUNN { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }

        public Int16 ProcesarComunicacionBaja { get; set; }
        public Int16 ProcesarResumenDiario { get; set; }

        public String RUC { get; set; }

        public Int16 TransferirSUNAT { get; set; }

        public Boolean AplicaAsientoProvision { get; set; }

        public Boolean AplicaAsientoReversaProvision { get; set; }

        public String TextoGeneralAsientoProvision { get; set; }

        public String NroVoucherRegistro { get; set; }

        public String NombreProveedor { get; set; }

        public Int16 AnioEmision { get; set; }

        public String NroVoucherProvision { get; set; }

        public DateTime FechaContabilizacion { get; set; }

        public clsFacturacionAdministrativaTransferirSAP()
        { }
    }

}
