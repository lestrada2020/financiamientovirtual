﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SISAC.Entidad.Atencion
{
    #region clsOrdenVisaNet

    [Serializable()]
    public class clsOrdenVisaNet
    {
        #region Properties

        public Int32 IdNumeroOrden { get; set; }

        public Int16 IdEmpresa { get; set; }

        public Int32 IdNroServicio { get; set; }

        public DateTime FechaGeneracion { get; set; }

        public Int32 IdUsuarioWEB { get; set; }

        public String eTicket { get; set; }

        public String MensajeVisaNet { get; set; }

        public String Trama { get; set; }

        public Decimal? Importe { get; set; }

        public Int32? CodAccion { get; set; }

        public Int32? IdCobranzaExterna { get; set; }

        public String NroTarjeta { get; set; }

        public Int32? IdTarea { get; set; }

        public Int16 IdEstado { get; set; }

        public Int32? NroTransaccion { get; set; }

        public String MensajeExcepcion { get; set; }

        public Int16? IdOrigen { get; set; }

        public Int16? TipoImporte { get; set; }

        public String SessionTokenVisa { get; set; }

        public Int16? IdPaso { get; set; }

        public Int16? IdEstadoPaso { get; set; }

        public String JsonPagoResumen { get; set; }

        //Extra

        public Int16 IdUUNN { get; set; }

        public Int32 IdCentroServicio { get; set; }

        public Int16 IdTipoAtencion { get; set; }

        public Int32 IdPuntoAtencion { get; set; }

        public Int32 IdUsuario { get; set; }

        public String CodigoComercio { get; set; }

        #endregion Properties

        #region Constructors

        public clsOrdenVisaNet()
        {
        }

        public clsOrdenVisaNet(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdNumeroOrden") && !row.IsNull("IdNumeroOrden")) IdNumeroOrden = Convert.ToInt32(row["IdNumeroOrden"]);
            if (columns.Contains("IdEmpresa") && !row.IsNull("IdEmpresa")) IdEmpresa = Convert.ToInt16(row["IdEmpresa"]);
            if (columns.Contains("IdNroServicio") && !row.IsNull("IdNroServicio")) IdNroServicio = Convert.ToInt32(row["IdNroServicio"]);
            if (columns.Contains("FechaGeneracion") && !row.IsNull("FechaGeneracion")) FechaGeneracion = Convert.ToDateTime(row["FechaGeneracion"]);
            if (columns.Contains("IdUsuarioWEB") && !row.IsNull("IdUsuarioWEB")) IdUsuarioWEB = Convert.ToInt32(row["IdUsuarioWEB"]);
            if (columns.Contains("eTicket") && !row.IsNull("eTicket")) eTicket = Convert.ToString(row["eTicket"]);
            if (columns.Contains("MensajeVisaNet") && !row.IsNull("MensajeVisaNet")) MensajeVisaNet = Convert.ToString(row["MensajeVisaNet"]);
            if (columns.Contains("Trama") && !row.IsNull("Trama")) Trama = Convert.ToString(row["Trama"]);
            if (columns.Contains("Importe") && !row.IsNull("Importe")) Importe = Convert.ToDecimal(row["Importe"]);
            if (columns.Contains("CodAccion") && !row.IsNull("CodAccion")) CodAccion = Convert.ToInt32(row["CodAccion"]);
            if (columns.Contains("IdCobranzaExterna") && !row.IsNull("IdCobranzaExterna")) IdCobranzaExterna = Convert.ToInt32(row["IdCobranzaExterna"]);
            if (columns.Contains("NroTarjeta") && !row.IsNull("NroTarjeta")) NroTarjeta = Convert.ToString(row["NroTarjeta"]);
            if (columns.Contains("IdTarea") && !row.IsNull("IdTarea")) IdTarea = Convert.ToInt32(row["IdTarea"]);
            if (columns.Contains("IdEstado") && !row.IsNull("IdEstado")) IdEstado = Convert.ToInt16(row["IdEstado"]);
            if (columns.Contains("NroTransaccion") && !row.IsNull("NroTransaccion")) NroTransaccion = Convert.ToInt32(row["NroTransaccion"]);
            if (columns.Contains("MensajeExcepcion") && !row.IsNull("MensajeExcepcion")) MensajeExcepcion = Convert.ToString(row["MensajeExcepcion"]);
            if (columns.Contains("IdOrigen") && !row.IsNull("IdOrigen")) IdOrigen = Convert.ToInt16(row["IdOrigen"]);
            if (columns.Contains("TipoImporte") && !row.IsNull("TipoImporte")) TipoImporte = Convert.ToInt16(row["TipoImporte"]);
            if (columns.Contains("SessionTokenVisa") && !row.IsNull("SessionTokenVisa")) SessionTokenVisa = Convert.ToString(row["SessionTokenVisa"]);
            if (columns.Contains("IdPaso") && !row.IsNull("IdPaso")) IdPaso = Convert.ToInt16(row["IdPaso"]);
            if (columns.Contains("IdEstadoPaso") && !row.IsNull("IdEstadoPaso")) IdEstadoPaso = Convert.ToInt16(row["IdEstadoPaso"]);
            if (columns.Contains("JsonPagoResumen") && !row.IsNull("JsonPagoResumen")) JsonPagoResumen = Convert.ToString(row["JsonPagoResumen"]);

            if (columns.Contains("iduunn")) IdUUNN = row["iduunn"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(row["iduunn"]);
            if (columns.Contains("IdTipoAtencion")) IdTipoAtencion = row["IdTipoAtencion"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(row["IdTipoAtencion"]);
            if (columns.Contains("IdCentroServicio")) IdCentroServicio = row["IdCentroServicio"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(row["IdCentroServicio"]);
            if (columns.Contains("IdPuntoAtencion")) IdPuntoAtencion = row["IdPuntoAtencion"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(row["IdPuntoAtencion"]);
            if (columns.Contains("idusuario")) IdUsuario = row["idusuario"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(row["idusuario"]);
            if (columns.Contains("CodigoComercio")) CodigoComercio = row["CodigoComercio"] == DBNull.Value ? String.Empty : row["CodigoComercio"].ToString();
        }

        #endregion Constructors
    }

    #endregion clsOrdenVisaNet

    #region clsListaOrdenVisaNet

    [Serializable()]
    public class clsListaOrdenVisaNet
    {
        #region Properties

        public List<clsOrdenVisaNet> Elementos { get; set; }

        #endregion Properties

        #region Constructors

        public clsListaOrdenVisaNet()
        {
            Elementos = new List<clsOrdenVisaNet>();
        }

        public clsListaOrdenVisaNet(DataTable table)
        {
            Elementos = new List<clsOrdenVisaNet>();

            if (table == null) return;

            foreach (DataRow row in table.Rows) Elementos.Add(new clsOrdenVisaNet(row));
        }

        #endregion Constructors
    }

    #endregion clsListaOrdenVisaNet

    #region clsOrdenVisaNetDetalle

    [Serializable()]
    public class clsOrdenVisaNetDetalle
    {
        #region Properties

        public Int32 IdDetalleOrden { get; set; }

        public Int32 IdNumeroOrden { get; set; }

        public Int16 IdTipoDocumento { get; set; }

        public String NroDocumento { get; set; }

        public Int32 Periodo { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public Decimal Saldo { get; set; }

        public Int16? IdEmpresa { get; set; }

        #endregion Properties

        #region Constructors

        public clsOrdenVisaNetDetalle()
        {
        }

        public clsOrdenVisaNetDetalle(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdDetalleOrden") && !row.IsNull("IdDetalleOrden")) IdDetalleOrden = Convert.ToInt32(row["IdDetalleOrden"]);
            if (columns.Contains("IdNumeroOrden") && !row.IsNull("IdNumeroOrden")) IdNumeroOrden = Convert.ToInt32(row["IdNumeroOrden"]);
            if (columns.Contains("IdTipoDocumento") && !row.IsNull("IdTipoDocumento")) IdTipoDocumento = Convert.ToInt16(row["IdTipoDocumento"]);
            if (columns.Contains("NroDocumento") && !row.IsNull("NroDocumento")) NroDocumento = Convert.ToString(row["NroDocumento"]);
            if (columns.Contains("Periodo") && !row.IsNull("Periodo")) Periodo = Convert.ToInt32(row["Periodo"]);
            if (columns.Contains("FechaVencimiento") && !row.IsNull("FechaVencimiento")) FechaVencimiento = Convert.ToDateTime(row["FechaVencimiento"]);
            if (columns.Contains("Saldo") && !row.IsNull("Saldo")) Saldo = Convert.ToDecimal(row["Saldo"]);
            if (columns.Contains("IdEmpresa") && !row.IsNull("IdEmpresa")) IdEmpresa = Convert.ToInt16(row["IdEmpresa"]);
        }

        #endregion Constructors
    }

    #endregion clsOrdenVisaNetDetalle

    #region clsListaOrdenVisaNetDetalle

    [Serializable()]
    public class clsListaOrdenVisaNetDetalle
    {
        #region Properties

        public List<clsOrdenVisaNetDetalle> Elementos { get; set; }

        #endregion Properties

        #region Constructors

        public clsListaOrdenVisaNetDetalle()
        {
            Elementos = new List<clsOrdenVisaNetDetalle>();
        }

        public clsListaOrdenVisaNetDetalle(DataTable table)
        {
            Elementos = new List<clsOrdenVisaNetDetalle>();

            if (table == null) return;

            foreach (DataRow row in table.Rows) Elementos.Add(new clsOrdenVisaNetDetalle(row));
        }

        #endregion Constructors
    }

    #endregion clsListaOrdenVisaNetDetalle

    #region NGC

    #region Enum

    #region TipoMoneda

    public enum Moneda
    {
        Soles = 1,
        Dolares = 2,
    }

    #endregion TipoMoneda

    #region TipoDocumentoComercial

    public enum TipoDocumentoComercial
    {
        Efectivo = 1,
        BoletaVenta = 2,
        Factura = 3,
        Ticket = 4,
        NotaCredito = 5,
        NotaDebito = 6,
        ReciboEnergia = 7,
        Cheque = 8,
        OrdenTrabajo = 29,
        ConvenioDePago = 11,
        TarjetaCredito = 12,
        OrdenPago = 16,
        DepositoBancario = 18,
        NotaIngresoCaja = 22,
        ReciboAgua = 23,
        ReciboTelefono = 24,
        ReciboCableMagico = 25,
        DepositoGarangia = 33,
        CuotaInicialConvenio = 38,
        InteresDeudaEnergia = 45,
        VoucherContable = 102,

        //ActaInstNuevoSuministro = 104,
        ActaTrabajo = 110,

        RegistroInterrupcion = 142,

        //ActaCambioMedidor = 125,
        //ActaRetiroMedidor = 127,
        //ActaDenunciaAP = 128
        VoucherDsctoPlanilla = 148,

        DocumentoDescuento = 156,
        SobranteFaltante = 157,
        ContratoSuministroEnergiaMayores = 158,
        RegularizaciónFacturacionNegativo = 159,
        NotaAbono = 160,
        CorteReconexion = 161,
        EncargoTerceros = 162,
        ReciboPrepago = 163,
        Redondeo = 101,
        TransaccionExtrajudicial = 175,
        RegistroInterrupcionSimulacion = 182
    }

    #endregion TipoDocumentoComercial

    #region Estado

    public enum Estado
    {
        Activo = 1,
        EnProceso = 21,
        Aprobado = 32,
        NoPagado = 50,
        Pagado = 52,
        Fallado = 169,
        Generado = 170,
        Confirmar = 271,
        Normal = 201,
        Cerrado = 98,
        ErrorAplicacion = 9999,
        Procesando = 117,
        Errado = 179,
        //DISTRILUZ 036 INICIO FINANCIAMIENTO
        Inactivo = 0,
        Pendiente = 5,
        PorConciliar = 51,
        Abierto = 90
        //DISTRILUZ 036 FIN FINANCIAMIENTO
    }

    #endregion Estado

    #region TipoOperacionCobranza

    public enum TipoOperacionCobranza
    {
        Recaudacion = 1,
        Extorno = 2,
        Remito = 3,
        Derivado = 4
    }

    #endregion TipoOperacionCobranza

    #region enumMovimientoComercial

    public enum enumMovimientoComercial
    {
        FacturacionMasiva = 1,
        Facturacion = 2,
        CobranzaTerceros = 3,
        AjusteFacturacion = 4,
        Devoluciones = 5,
        PagosExtornados = 6,
        DepositoEnGarantia = 7,
        Convenios = 11,
        Refacturacion = 12,
        Reclamo = 13,
        Cobranza = 14,
        VentaPrePago = 15,
        NuevoSuministro = 16,
        VentasExtraordinarias = 17,
        ConveniosSolicitudServicio = 18,
        FacturacionTemporal = 19,
        Recupero = 20,
        Reintegro = 21,
        ConveniosRecupero = 22,
        ConvenioVentaExtraordinaria = 23,
    }

    #endregion enumMovimientoComercial

    #region EstadoRegistro

    public enum EstadoRegistro
    {
        Ninguno = -1,
        Consulta = 0,
        Nuevo = 1,
        Modificado = 2,
        Eliminado = 3,
    }

    #endregion EstadoRegistro
    #endregion Enum

    #region Documentos Descargo

    #region IDocumentosDescargo - V2

    public interface IDocumentosDescargo
    {
        Int32 IdNroServicio { get; set; }

        Int32 OrdenPagoServicio { get; set; }

        Int16 IdServicio { get; set; }

        Int16 IdEmpresa { get; set; }

        Int16 IdTipoDocumento { get; set; }

        String NroDocumento { get; set; }

        DateTime FechaEmision { get; set; }

        Int16 IdMonedaOrigen { get; set; }

        Decimal ImporteOrigenDeuda { get; set; }

        Decimal ImporteOrigenCobrar { get; set; }

        Decimal ImporteOrigenDescargo { get; set; }

        Int16 IdMonedaCambio { get; set; }

        Decimal ImporteCambioDeuda { get; set; }

        Decimal ImporteCambioCobrar { get; set; }

        Decimal ImporteCambioDescargo { get; set; }
    }

    #endregion IDocumentosDescargo - V2

    #region DocumentosDescargoBase - V2

    public abstract class clsDocumentosDescargoBase : IDocumentosDescargo, IComparable
    {
        #region Campos

        private Int32 _intidtransaccionlog;
        private Int32 _intordenpagoservicio;
        private Int32 _intordenpagodocumento;
        private Int32 _intidnroservicio;
        private String _strcodservicio;
        private Int16 _intidservicio;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private String _strnombretipodocumento;
        private String _strabreviatipodocumento;
        private DateTime _datfechaemision;

        private Int16 _intidmonedaorigen;
        private Decimal _decimporteorigendeuda;
        private Decimal _decimporteorigencobrar;
        private Decimal _decimporteorigendescargo;

        private Decimal _dectipocambio;
        private Int16 _intidmonedacambio;
        private Decimal _decimportecambiodeuda;
        private Decimal _decimportecambiocobrar;
        private Decimal _decimportecambiodescargo;

        protected Boolean _boopermitepagoparcial;
        protected Boolean _booesterceros;

        private Int16 _intidemresaservicio;

        private Int32 _intnrotransaccion;

        #endregion Campos

        #region Propiedades

        public Int32 NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int32 OrdenPagoServicio
        {
            get { return _intordenpagoservicio; }
            set { _intordenpagoservicio = value; }
        }

        public Int32 OrdenPagoDocumento
        {
            get { return _intordenpagodocumento; }
            set { _intordenpagodocumento = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public String CodServicio
        {
            get { return _strcodservicio; }
            set { _strcodservicio = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }

        public String AbreviaTipoDocumento
        {
            get { return _strabreviatipodocumento; }
            set { _strabreviatipodocumento = value; }
        }

        public DateTime FechaEmision
        {
            get { return _datfechaemision; }
            set { _datfechaemision = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        public Decimal ImporteOrigenDeuda
        {
            //get { return Math.Round(_decimporteorigendeuda, 2); }
            get { return _decimporteorigendeuda; }
            set { _decimporteorigendeuda = value; }
        }

        public Decimal ImporteOrigenCobrar
        {
            //get { return Math.Round(_decimporteorigencobrar,2); }
            get { return _decimporteorigencobrar; }
            set { _decimporteorigencobrar = value; }
        }

        public Decimal ImporteOrigenDescargo
        {
            //get { return Math.Round(_decimporteorigendescargo,2); }
            get { return _decimporteorigendescargo; }
            set { _decimporteorigendescargo = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmonedacambio; }
            set { _intidmonedacambio = value; }
        }

        public Decimal ImporteCambioDeuda
        {
            get { return _decimportecambiodeuda; }
            set { _decimportecambiodeuda = value; }
        }

        public Decimal ImporteCambioCobrar
        {
            get { return _decimportecambiocobrar; }
            set { _decimportecambiocobrar = value; }
        }

        public Decimal ImporteCambioDescargo
        {
            get { return _decimportecambiodescargo; }
            set { _decimportecambiodescargo = value; }
        }

        public Boolean EsTerceros
        {
            get { return _booesterceros; }
            set { _booesterceros = value; }
        }

        public Boolean PermitePagoParcial
        {
            get { return _boopermitepagoparcial; }
            set { _boopermitepagoparcial = value; }
        }

        public Int16 IdEmpresaServicio
        {
            get { return _intidemresaservicio; }
            set { _intidemresaservicio = value; }
        }

        #endregion Propiedades

        #region Metodos

        public virtual clsDocumentosDescargoBase Copiar()
        { return null; }

        #endregion Metodos

        #region IComparable Members

        public virtual Int32 CompareTo(object obj)
        {
            clsDocumentosDescargoBase _objdocumentodescargo = (clsDocumentosDescargoBase)obj;
            String _strcomparacion1;
            String _strcomparacion2;

            _strcomparacion1 = OrdenPagoServicio.ToString() + FechaEmision.ToString("yyyyMMdd")
                             + OrdenPagoDocumento.ToString() + NroDocumento;

            _strcomparacion2 = _objdocumentodescargo.OrdenPagoServicio.ToString() + _objdocumentodescargo.FechaEmision.ToString("yyyyMMdd")
                             + _objdocumentodescargo.OrdenPagoDocumento.ToString() + _objdocumentodescargo.NroDocumento;

            return _strcomparacion1.CompareTo(_strcomparacion2);
        }

        #endregion IComparable Members

        #region IDocumentosDescargo Members

        private Int16 _intidempresa;

        public short IdEmpresa
        {
            get
            {
                return _intidempresa;
            }
            set
            {
                _intidempresa = value;
            }
        }

        #endregion IDocumentosDescargo Members
    }

    #endregion DocumentosDescargoBase - V2

    #region DocumentosDescargoPropios - V2

    public class clsDocumentosDescargoPropio : clsDocumentosDescargoBase
    {
        #region Campos

        private Int16 _intidtipodocumentosustento;
        private String _strnrodocumentosustento;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private Int32 _intperiodo;

        private String _strnombrecliente;

        #endregion Campos

        #region Constructor

        //****************************************************************
        /// <summary>
        /// clsDocumentosDescargoPropio
        /// </summary>
        /// <param name="registro"></param>

        public clsDocumentosDescargoPropio(DataRow registro)
        {
            this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            this.NombreTipoDocumento = registro["nombredocumento"].ToString();
            this.AbreviaTipoDocumento = registro["abreviadocumento"].ToString();
            this.NroDocumento = registro["nrodocumento"].ToString();
            this.IdTipoDocumentoSustento = (Int16)registro["idtipodocumentosustento"];
            this.NroDocumentoSustento = registro["nrodocumentosustento"].ToString();
            this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("tipocambio")) { this.TipoCambio = (Decimal)registro["tipocambio"]; }
            this.ImporteOrigenDeuda = (Decimal)registro["importedocumento"];
            this.ImporteOrigenDescargo = (Decimal)registro["importepagado"];
            this.IdServicio = (Int16)registro["idservicio"];
            this.FechaEmision = (DateTime)registro["fechaemision"];
            this.IdTipoIdentidad = (Int16)registro["idtipoidentidad"];
            this.NroIdentidad = registro["nroidentidad"].ToString();
            this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("codservicio"))
                this.CodServicio = registro["codservicio"].ToString().Trim();
            if (registro.Table.Columns.Contains("periodo"))
                this.Periodo = Convert.IsDBNull(registro["periodo"]) ? (Int32)0 : Convert.ToInt32(registro["periodo"]);
        }

        //*****************************************************************

        public clsDocumentosDescargoPropio
        (Int32 idtransaccionlog, Int16 idtipodocumento, String nombretipodocumento
        , String nrodocumento, Int16 idtipodocumentosustento, String nrodocumentosustento
        , Decimal importedeuda, Decimal importecobrar, Int16 idservicio, Int16 idmoneda
        , DateTime fechaemision, Int16 idtipoidentidad, String nroidentidad, Int32 idnroservicio
        )
        {
            //this.IdTransaccionLog =  idtransaccionlog;
            this.IdTipoDocumento = idtipodocumento;
            this.NombreTipoDocumento = nombretipodocumento;
            this.NroDocumento = nrodocumento;
            this.IdTipoDocumentoSustento = idtipodocumentosustento;
            this.NroDocumentoSustento = nrodocumentosustento;

            this.IdMonedaOrigen = idmoneda;
            this.ImporteOrigenDeuda = importedeuda;
            this.ImporteOrigenDescargo = 0;
            this.ImporteOrigenCobrar = importecobrar;

            this.IdServicio = idservicio;
            this.FechaEmision = fechaemision;
            this.IdTipoIdentidad = idtipoidentidad;
            this.NroIdentidad = nroidentidad;
            this.IdNroServicio = idnroservicio;
        }

        public clsDocumentosDescargoPropio()
        {
        }

        #endregion Constructor

        #region Propiedades

        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        public Int16 IdTipoDocumentoSustento
        {
            get { return _intidtipodocumentosustento; }
            set { _intidtipodocumentosustento = value; }
        }

        public String NroDocumentoSustento
        {
            get { return _strnrodocumentosustento; }
            set { _strnrodocumentosustento = value; }
        }

        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }

        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }

        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }

        #endregion Propiedades

        #region Metodos

        public override clsDocumentosDescargoBase Copiar()
        {
            clsDocumentosDescargoPropio _objdocumentodescargopropio = new clsDocumentosDescargoPropio();
            _objdocumentodescargopropio.EsTerceros = this.EsTerceros;
            _objdocumentodescargopropio.FechaEmision = this.FechaEmision;
            _objdocumentodescargopropio.IdMonedaCambio = this.IdMonedaCambio;
            _objdocumentodescargopropio.IdMonedaOrigen = this.IdMonedaOrigen;
            _objdocumentodescargopropio.IdNroServicio = this.IdNroServicio;
            _objdocumentodescargopropio.IdServicio = this.IdServicio;
            _objdocumentodescargopropio.IdTipoDocumento = this.IdTipoDocumento;
            _objdocumentodescargopropio.IdTipoDocumentoSustento = this.IdTipoDocumentoSustento;
            _objdocumentodescargopropio.IdTipoIdentidad = this.IdTipoIdentidad;
            _objdocumentodescargopropio.IdTransaccionLog = this.IdTransaccionLog;
            _objdocumentodescargopropio.ImporteCambioCobrar = this.ImporteCambioCobrar;
            _objdocumentodescargopropio.ImporteCambioDescargo = this.ImporteCambioDescargo;
            _objdocumentodescargopropio.ImporteCambioDeuda = this.ImporteCambioDeuda;
            _objdocumentodescargopropio.ImporteOrigenCobrar = this.ImporteOrigenCobrar;
            _objdocumentodescargopropio.ImporteOrigenDescargo = this.ImporteOrigenDescargo;
            _objdocumentodescargopropio.ImporteOrigenDeuda = this.ImporteOrigenDeuda;
            _objdocumentodescargopropio.NombreTipoDocumento = this.NombreTipoDocumento;
            _objdocumentodescargopropio.NroDocumento = this.NroDocumento;
            _objdocumentodescargopropio.NroDocumentoSustento = this.NroDocumentoSustento;
            _objdocumentodescargopropio.NroIdentidad = this.NroIdentidad;
            _objdocumentodescargopropio.OrdenPagoServicio = this.OrdenPagoServicio;
            _objdocumentodescargopropio.PermitePagoParcial = this.PermitePagoParcial;
            _objdocumentodescargopropio.TipoCambio = this.TipoCambio;
            _objdocumentodescargopropio.Periodo = this.Periodo;

            return _objdocumentodescargopropio;
        }

        #endregion Metodos
    }

    #endregion DocumentosDescargoPropios - V2

    #region DocumentosDescargoTerceros - V2

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsDocumentosDescargoTerceros : clsDocumentosDescargoBase
    {
        #region Campos

        private String _strnroservicio;
        private Int16 _intidtiporecibo;
        private String _strnrorecibo;
        private clsDocumentosDescargoPropio _objdocumentodescargopropio;

        #endregion Campos

        #region Constructor

        public clsDocumentosDescargoTerceros()
        {
            _booesterceros = true;
        }

        #endregion Constructor

        #region Propiedades

        public String NroServicio
        {
            get { return _strnroservicio; }
            set { _strnroservicio = value; }
        }

        public Int16 IdTipoRecibo
        {
            get { return _intidtiporecibo; }
            set { _intidtiporecibo = value; }
        }

        public String NroRecibo
        {
            get { return _strnrorecibo; }
            set { _strnrorecibo = value; }
        }

        public clsDocumentosDescargoPropio DocumentoDescargoPropio
        {
            get { return _objdocumentodescargopropio; }
            set { _objdocumentodescargopropio = value; }
        }

        #endregion Propiedades

        #region Metodos

        public override clsDocumentosDescargoBase Copiar()
        {
            clsDocumentosDescargoTerceros _objdocumentodescargotercero = new clsDocumentosDescargoTerceros();
            _objdocumentodescargotercero.EsTerceros = this.EsTerceros;
            _objdocumentodescargotercero.FechaEmision = this.FechaEmision;
            _objdocumentodescargotercero.IdMonedaCambio = this.IdMonedaCambio;
            _objdocumentodescargotercero.IdMonedaOrigen = this.IdMonedaOrigen;
            _objdocumentodescargotercero.IdNroServicio = this.IdNroServicio;
            _objdocumentodescargotercero.IdServicio = this.IdServicio;
            _objdocumentodescargotercero.IdTipoDocumento = this.IdTipoDocumento;
            _objdocumentodescargotercero.IdTransaccionLog = this.IdTransaccionLog;
            _objdocumentodescargotercero.ImporteCambioCobrar = this.ImporteCambioCobrar;
            _objdocumentodescargotercero.ImporteCambioDescargo = this.ImporteCambioDescargo;
            _objdocumentodescargotercero.ImporteCambioDeuda = this.ImporteCambioDeuda;
            _objdocumentodescargotercero.ImporteOrigenCobrar = this.ImporteOrigenCobrar;
            _objdocumentodescargotercero.ImporteOrigenDescargo = this.ImporteOrigenDescargo;
            _objdocumentodescargotercero.ImporteOrigenDeuda = this.ImporteOrigenDeuda;
            _objdocumentodescargotercero.NombreTipoDocumento = this.NombreTipoDocumento;
            _objdocumentodescargotercero.NroDocumento = this.NroDocumento;
            _objdocumentodescargotercero.OrdenPagoServicio = this.OrdenPagoServicio;
            _objdocumentodescargotercero.PermitePagoParcial = this.PermitePagoParcial;
            _objdocumentodescargotercero.TipoCambio = this.TipoCambio;

            _objdocumentodescargotercero.NroServicio = this.NroServicio;
            _objdocumentodescargotercero.IdTipoRecibo = this.IdTipoRecibo;
            _objdocumentodescargotercero.NroRecibo = this.NroRecibo;
            _objdocumentodescargotercero.DocumentoDescargoPropio = this.DocumentoDescargoPropio;

            return _objdocumentodescargotercero;
        }

        #endregion Metodos
    }

    #endregion DocumentosDescargoTerceros - V2

    #endregion Documentos Descargo

    #region clsMediosPagoExterno

    public class clsMediosPagoExterno : clsMediosPagoBase
    {
        private Boolean _boogeneravuelto;
        private Boolean _booesefectivo;
        private Boolean _booescheque;
        private Int16 _intesingreso;

        private Int16? _intidtipotarjeta;
        private String _strnrotajeta;
        private Int16 _intidcuentabancaria;
        private Int16 _intidcuentacontable;
        private String _strcuentabancaria;
        private DateTime? _datfechadocumento;
        private Int32 _intidnrocaja;

        //VAS
        private String _strnombrebanco;

        //EGM
        private Boolean _booenviaaboveda;

        public clsMediosPagoExterno(Int16 idtipodocumento, String nombretipodocumento, String nrodocumento, Int16 idbanco, Int16 idmonedapago, Decimal importepago, Int16 idmonedacambio, Decimal importecambio, Decimal tipocambio, Boolean generavuelto, Int16 idtipotarjeta, String nrotarjeta, String nrocuentabancaria, String monedapagosimbolo, Boolean efectivo)
        {
            this.IdTipoDocumento = idtipodocumento;
            this.NombreTipoDocumento = nombretipodocumento;
            this.NroDocumento = nrodocumento;
            this.IdBanco = idbanco;
            this.MonedaOrigenSimbolo = monedapagosimbolo;
            this.IdMonedaOrigen = idmonedapago;
            this.ImporteOrigenTotal = importepago;

            this.IdMonedaCambio = idmonedacambio;
            this.ImporteCambioTotal = importecambio;
            this.TipoCambio = tipocambio;
            this.GeneraVuelto = generavuelto;
            this.IdTipoTarjeta = idtipotarjeta;
            this.NroTarjeta = nrotarjeta;
            this.NroCuentaBancaria = nrocuentabancaria;
        }

        public clsMediosPagoExterno(Int16 idtipodocumento, String nombretipodocumento, String nrodocumento, Int16 idbanco, Int16 idmonedapago, Decimal importepago, Int16 idmonedacambio, Decimal importecambio, Decimal tipocambio, Boolean generavuelto, Int16 idtipotarjeta, String nrotarjeta, String nrocuentabancaria, String monedapagosimbolo, Boolean efectivo, Int16 intidservicio)
        {
            this.IdTipoDocumento = idtipodocumento;
            this.NombreTipoDocumento = nombretipodocumento;
            this.NroDocumento = nrodocumento;
            this.IdBanco = idbanco;
            this.MonedaOrigenSimbolo = monedapagosimbolo;
            this.IdMonedaOrigen = idmonedapago;
            this.ImporteOrigenTotal = importepago;

            this.IdMonedaCambio = idmonedacambio;
            this.ImporteCambioTotal = importecambio;
            this.TipoCambio = tipocambio;
            this.GeneraVuelto = generavuelto;
            this.IdTipoTarjeta = idtipotarjeta;
            this.NroTarjeta = nrotarjeta;
            this.NroCuentaBancaria = nrocuentabancaria;
            this.IdServicio = intidservicio;
        }

        public clsMediosPagoExterno(DataRow registro)
        {
            this.Sel = Convert.ToBoolean(registro["Sel"]);
            this.EsEfectivo = Convert.ToBoolean(registro["EsEfectivo"]);

            this.EsIngreso = Convert.ToInt16(registro["EsIngreso"]);

            this.GeneraVuelto = Convert.ToBoolean(registro["GeneraVuelto"]);
            this.IdMonedaCambio = Convert.ToInt16(registro["IdMonedaCambio"]);
            this.IdMonedaOrigen = Convert.ToInt16(registro["IdMoneda"]);
            this.MonedaOrigenSimbolo = Convert.ToString(registro["monedaorigensimbolo"]);
            this.IdTipoDocumento = Convert.ToInt16(registro["IdTipoDocumento"]);
            this.NroDocumento = registro["NroDocumento"].ToString();
            this.NombreTipoDocumento = registro["NombreDocumento"].ToString();
            this.ImporteOrigenTotal = Convert.ToDecimal(registro["Importe"]);
            this.ImporteCambioTotal = Convert.ToDecimal(registro["ImporteCambio"]);
            this.TipoCambio = Convert.ToDecimal(registro["TipoCambio"]);
            if (registro.Table.Columns.Contains("EsEfectivo")) { this.EsEfectivo = Convert.ToBoolean(registro["EsEfectivo"]); }
            if (this.IdTipoDocumento == (Int16)TipoDocumentoComercial.Efectivo) { return; }

            if (registro.Table.Columns.Contains("EsCheque")) { this.EsCheque = Convert.ToBoolean(registro["EsCheque"]); }
            this.NroCuentaBancaria = registro["CuentaBancaria"].ToString();
            this.IdBanco = Convert.IsDBNull(registro["IdBanco"]) ? (Int16?)null : Convert.ToInt16(registro["IdBanco"]);
            this.NroTarjeta = registro["NroTarjeta"].ToString();
            if (IdTipoTarjeta != null) { this.IdTipoTarjeta = Convert.ToInt16(registro["IdTipoTarjeta"]); }

            this.DescripcionTarjeta = registro.Table.Columns.Contains("descripciontarjeta") ? registro["descripciontarjeta"].ToString() : "";

            //VAS
            this.NombreBanco = registro.Table.Columns.Contains("NombreBanco") ? registro["NombreBanco"].ToString() : "";
            //this.OrdenDePago = Convert.ToInt16(registro["OrdenDePago"]);
            //this.MonedaOrigenSimbolo = registro["MonedaSimbolo"].ToString();
            //this.NombreMonedaOrigen = registro["NombreMoneda"].ToString();
            //this.FechaDocumento = Convert.ToDateTime(registro["fechadeposito"]);
            //this.IdTransaccionLog = Convert.ToInt32(registro["IdTransaccionLog"]);
        }

        public clsMediosPagoExterno()
        {
        }

        public Boolean EnviaABoveda
        {
            get { return _booenviaaboveda; }
            set { _booenviaaboveda = value; }
        }

        public Boolean GeneraVuelto
        {
            get { return _boogeneravuelto; }
            set { _boogeneravuelto = value; }
        }

        public Int16 EsIngreso
        {
            get { return _intesingreso; }
            set { _intesingreso = value; }
        }

        public Int16? IdTipoTarjeta
        {
            get { return _intidtipotarjeta; }
            set { _intidtipotarjeta = value; }
        }

        public String NroTarjeta
        {
            get { return _strnrotajeta; }
            set { _strnrotajeta = value; }
        }

        public Int16 IdCuentaBancaria
        {
            get { return _intidcuentabancaria; }
            set { _intidcuentabancaria = value; }
        }

        public Int16 IdCuentaContable
        {
            get { return _intidcuentacontable; }
            set { _intidcuentacontable = value; }
        }

        public String NroCuentaBancaria
        {
            get { return _strcuentabancaria; }
            set { _strcuentabancaria = value; }
        }

        public Boolean EsEfectivo
        {
            get { return _booesefectivo; }
            set { _booesefectivo = value; }
        }

        public Boolean EsCheque
        {
            get { return _booescheque; }
            set { _booescheque = value; }
        }

        public DateTime? FechaDocumento
        {
            get { return _datfechadocumento; }
            set { _datfechadocumento = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public String NombreBanco
        {
            get { return _strnombrebanco; }
            set { _strnombrebanco = value; }
        }
    }

    #endregion clsMediosPagoExterno

    #region clsMediosPagoBase

    public class clsMediosPagoBase : IComparable
    {
        #region Campos

        private Boolean _boosel;
        private Int32 _intidtransaccionlog;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private String _strnombretipodocumento;
        private String _strnombremonedaorigen;
        private String _strsimbolomonedaorigen;
        private Int16 _intidmonedaorigen;
        private Decimal _decimporteorigentotal;
        private Decimal _decimporteorigendescargo;

        private Decimal _dectipocambio;
        private Int16 _intidmonedacambio;
        private Decimal _decimportecambiototal;
        private Decimal _decimportecambiodescargo;

        protected Boolean _booesmediopagointerno;
        private Int16 _intordendepago;
        private Int16? _intidbanco;

        private String _descripciontarjeta;

        private Int32 _intnrotransaccion;

        //CLS
        private Int16 _intidservicio;

        #endregion Campos

        #region Propiedades IMediosPago

        /// <summary>
        ///
        /// </summary>
        public Int32 NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String DescripcionTarjeta
        {
            get { return _descripciontarjeta; }
            set { _descripciontarjeta = value; }
        }

        public Boolean Sel
        {
            get { return _boosel; }
            set { _boosel = value; }
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }

        public String NombreMonedaOrigen
        {
            get { return _strnombremonedaorigen; }
            set { _strnombremonedaorigen = value; }
        }

        public String MonedaOrigenSimbolo
        {
            get { return _strsimbolomonedaorigen; }
            set { _strsimbolomonedaorigen = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        public Decimal ImporteOrigenTotal
        {
            get { return _decimporteorigentotal; }
            set { _decimporteorigentotal = value; }
        }

        public Decimal ImporteOrigenDescargo
        {
            get { return Math.Round(_decimporteorigendescargo, 2); }
            set { _decimporteorigendescargo = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmonedacambio; }
            set { _intidmonedacambio = value; }
        }

        public Decimal ImporteCambioTotal
        {
            get { return _decimportecambiototal; }
            set { _decimportecambiototal = value; }
        }

        public Decimal ImporteCambioDescargo
        {
            get { return _decimportecambiodescargo; }
            set { _decimportecambiodescargo = value; }
        }

        public Boolean EsMedioPagoInterno
        {
            get { return _booesmediopagointerno; }
        }

        public Int16 OrdenDePago
        {
            get { return _intordendepago; }
            set { _intordendepago = value; }
        }

        public Int16? IdBanco
        {
            get { return _intidbanco; }
            set { _intidbanco = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        #endregion Propiedades IMediosPago

        #region IComparable Members

        public virtual Int32 CompareTo(object obj)
        {
            clsMediosPagoBase _objmediopago = (clsMediosPagoBase)obj;
            String _strcomparacion1 = (EsMedioPagoInterno ? "0" : "1") + OrdenDePago.ToString();
            String _strcomparacion2 = (_objmediopago.EsMedioPagoInterno ? "0" : "1") + _objmediopago.OrdenDePago.ToString();

            return _strcomparacion1.CompareTo(_strcomparacion2);
        }

        #endregion IComparable Members

        #region IMediosPago Members

        private Int16 _intidempresa;

        public short IdEmpresa
        {
            get
            {
                return _intidempresa;
            }
            set
            {
                _intidempresa = value;
            }
        }

        #endregion IMediosPago Members
    }

    #endregion clsMediosPagoBase

    #region clsUsuarioPuntoAtencion

    public class clsUsuarioPuntoAtencion
    {
        #region campos

        private Int32 _intidusuario;
        private Int16 _intidtipoatencion;
        private Int16 _intidpuntoatencion;
        private Int16 _intidcentroservicio;
        private Int16 _intidunidadnegocio;
        private Int16 _intidempresa;
        private Int16 _intidestadocobranza;
        private Int16 _intidestadocaja;
        private Int32 _intidnrocajaultimo;

        private String _strnombrecentroservicio;
        private String _strnombreunidadnegocio;
        private String _strnombreempresa;
        private String _strnombretipopunto;
        private String _strnombrepuntoatencion;
        private String _texto; // EGM
        private DateTime _datfechapago; // EGM
        private DateTime _datfechaapertura;

        //Datos para mostrar Cantidades
        private Decimal _decTotalRecaudado;

        private Decimal _decimporteventanillamn;
        private Decimal _decimportecheque;
        private Decimal _decpendienteabovedamn;
        private Decimal _decimporteenbovedamn;
        private Decimal _decimportedocumentosmn;
        private Int32 _intcantidadpendienteaboveda;
        private String _strestado;

        #endregion campos

        #region constructor

        /// <summary>
        ///
        /// </summary>
        /// <param name="idusuario"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idunidadnegocio"></param>
        /// <param name="idestadocobranza"></param>
        /// <param name="idestadocaja"></param>
        /// <param name="idnrocajaultimo"></param>
        /// <param name="idempresa"></param>
        public clsUsuarioPuntoAtencion(Int32 idusuario, Int16 idtipoatencion, Int16 idpuntoatencion, Int16 idcentroservicio, Int16 idunidadnegocio, Int16 idestadocobranza, Int16 idestadocaja, Int32 idnrocajaultimo, Int16 idempresa)
        {
            this.IdUsuario = idusuario;
            this.IdTipoAtencion = idtipoatencion;
            this.IdPuntoAtencion = idpuntoatencion;
            this.IdCentroServicio = idcentroservicio;
            this.IdEstadoCobranza = idestadocobranza;
            this.IdEstadoCaja = idestadocaja;
            this.IdNroCajaUltimo = idnrocajaultimo;
            this.IdUnidadNegocio = idunidadnegocio;
            this.IdEmpresa = idempresa;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        public clsUsuarioPuntoAtencion(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = Convert.ToInt32(registro["idusuario"]);
            if (registro.Table.Columns.Contains("idtipoatencion")) this.IdTipoAtencion = Convert.ToInt16(registro["idtipoatencion"]);
            if (registro.Table.Columns.Contains("idpuntoatencion")) this.IdPuntoAtencion = Convert.ToInt16(registro["idpuntoatencion"]);
            if (registro.Table.Columns.Contains("idcentroservicio")) this.IdCentroServicio = Convert.ToInt16(registro["idcentroservicio"]);
            if (registro.Table.Columns.Contains("IdUUNN")) this.IdUnidadNegocio = Convert.ToInt16(registro["IdUUNN"]);
            if (registro.Table.Columns.Contains("idestadocobranza")) this.IdEstadoCobranza = Convert.ToInt16(registro["idestadocobranza"]);
            if (registro.Table.Columns.Contains("idestadocaja")) this.IdEstadoCaja = Convert.ToInt16(registro["idestadocaja"]);
            if (registro.Table.Columns.Contains("idnrocajaultimo")) this.IdNroCajaUltimo = Convert.ToInt32(registro["idnrocajaultimo"]);

            if (registro.Table.Columns.Contains("nombrecentroservicio"))
                this.NombreCentroServicio = registro["nombrecentroservicio"].ToString();

            if (registro.Table.Columns.Contains("nombretipoatencion"))
                this.NombreTipoAtencion = registro["nombretipoatencion"].ToString();

            if (registro.Table.Columns.Contains("nombreatencion"))
                this.NombrePuntoAtencion = registro["nombreatencion"].ToString();

            // EGM
            if (registro.Table.Columns.Contains("texto"))
                this.Texto = registro["texto"].ToString();

            if (registro.Table.Columns.Contains("fechapago")) FechaPago = Convert.ToDateTime(registro["fechapago"].ToString());
            if (registro.Table.Columns.Contains("fechaapertura")) FechaApertura = Convert.ToDateTime(registro["fechaapertura"].ToString());

            if (registro.Table.Columns.Contains("totalrecaudado")) TotalRecaudado = Convert.ToDecimal(registro["totalrecaudado"].ToString());

            if (registro.Table.Columns.Contains("enventanillamn")) ImporteVentanillaMN = Convert.ToDecimal(registro["enventanillamn"]);

            if (registro.Table.Columns.Contains("ImporteenChequesMN")) ImporteChequeMN = Convert.IsDBNull(registro["ImporteenChequesMN"]) ? 0 : Convert.ToDecimal(registro["ImporteenChequesMN"]);

            if (registro.Table.Columns.Contains("EnDocumentosmn")) ImporteDocumentosMN = Convert.ToDecimal(registro["EnDocumentosmn"]);

            if (registro.Table.Columns.Contains("PendienteaBovedaMN")) PendienteabovedaMN = Convert.IsDBNull(registro["PendienteaBovedaMN"]) ? 0 : Convert.ToDecimal(registro["PendienteaBovedaMN"]);

            if (registro.Table.Columns.Contains("ImporteEnbovedamn")) ImporteenbovedaMN = Convert.ToDecimal(registro["ImporteEnbovedamn"]);

            if (registro.Table.Columns.Contains("DescripcionEstado")) DescripcionEstado = (registro["DescripcionEstado"]).ToString();
        }

        /// <summary>
        ///
        /// </summary>
        public clsUsuarioPuntoAtencion()
        {
        }

        #endregion constructor

        #region propiedades

        public DateTime FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        public Int16 IdEstadoCobranza
        {
            get { return _intidestadocobranza; }
            set { _intidestadocobranza = value; }
        }

        public Int16 IdUnidadNegocio
        {
            get { return _intidunidadnegocio; }
            set { _intidunidadnegocio = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdEstadoCaja
        {
            get { return _intidestadocaja; }
            set { _intidestadocaja = value; }
        }

        public Int32 IdNroCajaUltimo
        {
            get { return _intidnrocajaultimo; }
            set { _intidnrocajaultimo = value; }
        }

        public String NombreCentroServicio
        {
            get { return _strnombrecentroservicio; }
            set { _strnombrecentroservicio = value; }
        }

        public String NombreUnidadNegocio
        {
            get { return _strnombreunidadnegocio; }
            set { _strnombreunidadnegocio = value; }
        }

        public String NombreEmpresa
        {
            get { return _strnombreempresa; }
            set { _strnombreempresa = value; }
        }

        public String NombreTipoAtencion
        {
            get { return _strnombretipopunto; }
            set { _strnombretipopunto = value; }
        }

        public String NombrePuntoAtencion
        {
            get { return _strnombrepuntoatencion; }
            set { _strnombrepuntoatencion = value; }
        }

        public String Texto // EGM
        {
            get { return _texto; }
            set { _texto = value; }
        }

        public Decimal TotalRecaudado
        {
            get { return _decTotalRecaudado; }
            set { _decTotalRecaudado = value; }
        }

        public Decimal ImporteVentanillaMN
        {
            get { return _decimporteventanillamn; }
            set { _decimporteventanillamn = value; }
        }

        public Decimal ImporteChequeMN
        {
            get { return _decimportecheque; }
            set { _decimportecheque = value; }
        }

        public Decimal PendienteabovedaMN
        {
            get { return _decpendienteabovedamn; }
            set { _decpendienteabovedamn = value; }
        }

        public Decimal ImporteenbovedaMN
        {
            get { return _decimporteenbovedamn; }
            set { _decimporteenbovedamn = value; }
        }

        public Decimal ImporteDocumentosMN
        {
            get { return _decimportedocumentosmn; }
            set { _decimportedocumentosmn = value; }
        }

        public Int32 CantidadPendienteABoveda
        {
            get { return _intcantidadpendienteaboveda; }
            set { _intcantidadpendienteaboveda = value; }
        }

        public String DescripcionEstado
        {
            get { return _strestado; }
            set { _strestado = value; }
        }

        public DateTime FechaApertura
        {
            get { return _datfechaapertura; }
            set { _datfechaapertura = value; }
        }

        #endregion propiedades
    }

    #endregion clsUsuarioPuntoAtencion

    #region clsCajaEstadisticaDiarioMoneda

    public class clsCajaEstadisticaDiarioMoneda : clsCajaDiarioMoneda
    {
        private Int32 _intidtransaccionlog;
        private Int32? _intnrotransaccion;
        private Decimal _decimporteboveda;
        private Decimal _decimportebovedacambio;

        /// <summary>
        ///
        /// </summary>
        public clsCajaEstadisticaDiarioMoneda()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        public clsCajaEstadisticaDiarioMoneda(DataRow registro)
        {
            IdNroCaja = (Int32)registro["IdNroCaja"];
            IdMoneda = (Int16)registro["IdMoneda"];
            IdMonedaCambio = (Int16)registro["IdMonedaCambio"];
            Importe = (Decimal)registro["ImporteRetiro"];
            ImporteCambio = (Decimal)registro["ImporteRetiroCambio"];
            TipoCambio = (Decimal)registro["TipoCambio"];
            IdTipoDocumentoComercial = (Int16)registro["idtipodocumentocomercial"];
            if (registro.Table.Columns.Contains("NroMovimiento")) { NroTransaccion = registro.IsNull("NroMovimiento") ? (Int32?)0 : Convert.ToInt32(registro["NroMovimiento"]); }
            if (registro.Table.Columns.Contains("IdBanco")) { IdBanco = registro.IsNull("IdBanco") ? (Int16?)0 : Convert.ToInt16(registro["IdBanco"]); }
            if (registro.Table.Columns.Contains("NroDocumento")) { NroDocumentoComercial = registro.IsNull("NroDocumento") ? "" : (String)registro["NroDocumento"]; }
            if (registro.Table.Columns.Contains("ImporteEnBovedaPago")) { ImporteBoveda = (Decimal)registro["ImporteEnBovedaPago"]; }
            if (registro.Table.Columns.Contains("ImporteEnBovedaCambio")) { ImporteBovedaCambio = (Decimal)registro["ImporteEnBovedaCambio"]; }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        public clsCajaEstadisticaDiarioMoneda(clsCajaEstadisticaDiarioMoneda item)
        {
            IdTransaccionLog = item.IdTransaccionLog;
            IdNroCaja = item.IdNroCaja;
            IdMoneda = item.IdMoneda;
            IdMonedaCambio = item.IdMonedaCambio;
            Importe = item.Importe;
            ImporteCambio = item.ImporteCambio;
            TipoCambio = item.TipoCambio;
            IdTipoDocumentoComercial = item.IdTipoDocumentoComercial;
            ImporteDescargo = item.ImporteDescargo;
            ImporteBoveda = item.ImporteBoveda;
            ImporteBovedaCambio = item.ImporteBovedaCambio;
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int32? NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        public Decimal ImporteBoveda
        {
            get { return _decimporteboveda; }
            set { _decimporteboveda = value; }
        }

        public Decimal ImporteBovedaCambio
        {
            get { return _decimportebovedacambio; }
            set { _decimportebovedacambio = value; }
        }
    }

    #endregion clsCajaEstadisticaDiarioMoneda

    #region clsCajaDiarioMoneda

    public abstract class clsCajaDiarioMoneda
    {
        private Int32 _intidnrocaja;
        private Int16 _intidmoneda;
        private Int16 _intidmonedacambio;
        private Decimal _decimporte;
        private Decimal _decimportecambio;
        private Decimal _dectipocambio;
        private Int16 _intidtipodocumentocomercial;
        private String _strnrodocumentocomercial;
        private Int16? _intidbanco;
        private Decimal _decimportedescargo;

        private Boolean _bolseleccionar = true;

        /// <summary>
        ///
        /// </summary>
        public Boolean Seleccionar
        {
            get { return _bolseleccionar; }
            set { _bolseleccionar = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmonedacambio; }
            set { _intidmonedacambio = value; }
        }

        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        public Decimal ImporteDescargo
        {
            get { return _decimportedescargo; }
            set { _decimportedescargo = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Decimal ImporteCambio
        {
            get { return _decimportecambio; }
            set { _decimportecambio = value; }
        }

        public Int16 IdTipoDocumentoComercial
        {
            get { return _intidtipodocumentocomercial; }
            set { _intidtipodocumentocomercial = value; }
        }

        public Int16? IdBanco
        {
            get { return _intidbanco; }
            set { _intidbanco = value; }
        }

        public String NroDocumentoComercial
        {
            get { return _strnrodocumentocomercial; }
            set { _strnrodocumentocomercial = value; }
        }
    }

    #endregion clsCajaDiarioMoneda

    #region Transaccion - V2

    public class clsTransaccion
    {
        #region Campos
        private Int32 _intnrotransaccion;
        private Int32 _intidtransaccionlog;
        private Int16 _intidtipodocumento;
        private Decimal _decimporteneto;
        private DateTime _datfecharegistro;
        private Int16 _intidestado;
        private Int32 _intidnroservicio;
        private Decimal _decimporterecibido;
        private Decimal _decimportetotalvuelto;
        private Decimal _decimportetotalvueltosindevolver;

        private Decimal _decimporteefectivo;
        private Decimal _decimportevalores;
        private Decimal _decimportecheques = 0; //mod
        private DateTime _datfechapago;
        private Int32 _intperiodopago;

        private Int16 _intidmonedanacional;
        private TipoOperacionCobranza _enumtipooperacion;
        private Int32 _decsegundosatencionusuario;
        private DateTime _datfechainiciocobranzalog;
        private Int32 _intidchecksum;
        private Int16 _intidmovcomercial = 14;

        private clsUsuarioPuntoAtencion _objusuariopuntoatencion;

        private List<clsDocumentosDescargoBase> _elementosdocumentodescargo = new List<clsDocumentosDescargoBase>();
        private List<clsMediosPagoBase> _elementosmediopago = new List<clsMediosPagoBase>();
        private List<clsCajaEstadisticaDiarioMoneda> _elementoscajamoneda = new List<clsCajaEstadisticaDiarioMoneda>();
        private List<clsCajaEstadisticaServicio> _elementocajaestadisticaservicio = new List<clsCajaEstadisticaServicio>();
        private clsListaDocumentosAFavor _listadocumentoafavor = new clsListaDocumentosAFavor();
        private clsListaIngresoProvision _listaingresoprovision = new clsListaIngresoProvision();

        //Agregado por Carlos López.
        //Se agregó este campo para generar el convenio en la tabla del esquema DBO, considerando la Orden de Cobro
        //Se agregó el campo IdSolicitud para actualizar los datos en la tabla Sol.SolicitudServicio, sin embargo
        //no siempre este dato tendra valor.
        private Int32? _intidordencobro;

        private Int32? _intidsolicitud;

        // EGM. Para el compromiso de la cuota inicial.
        private Int32 _intidcompromiso = 0;
        
        private Int16 _inteshmologado; //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL

        #endregion Campos

        #region Propiedades

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        public Int16 EsHomologado
        {
            get { return _inteshmologado; }
            set { _inteshmologado = value; }
        }
        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        /// <summary>
        /// Nro de Compromiso de la Cuota Inicial.
        /// </summary>
        public Int32 IdCompromiso
        {
            get { return _intidcompromiso; }
            set { _intidcompromiso = value; }
        }

        public Int32 NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        public DateTime FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int32 PeriodoPago
        {
            get { return _intperiodopago; }
            set { _intperiodopago = value; }
        }

        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public Int16 IdMovComercial
        {
            get { return _intidmovcomercial; }
            set { _intidmovcomercial = value; }
        }

        public Decimal ImporteRecibido
        {
            get { return _decimporterecibido; }
            set { _decimporterecibido = value; }
        }

        public Decimal ImporteNeto
        {
            get { return _decimporteneto; }
            set { _decimporteneto = value; }
        }

        public Decimal ImporteTotalVuelto
        {
            get { return _decimportetotalvuelto; }
            set { _decimportetotalvuelto = value; }
        }

        public Decimal ImporteTotalVueltoSinDevolver
        {
            get { return _decimportetotalvueltosindevolver; }
            set { _decimportetotalvueltosindevolver = value; }
        }

        public Decimal ImporteEfectivo
        {
            get { return _decimporteefectivo; }
            set { _decimporteefectivo = value; }
        }

        public Decimal ImporteValores
        {
            get { return _decimportevalores; }
            set { _decimportevalores = value; }
        }

        public Decimal ImporteCheques //mod
        {
            get { return _decimportecheques; }
            set { _decimportecheques = value; }
        }

        public DateTime FechaInicioCobranzaLog
        {
            get { return _datfechainiciocobranzalog; }
            set { _datfechainiciocobranzalog = value; }
        }

        public Int32 SegundosAtencionUsuario
        {
            get { return _decsegundosatencionusuario; }
            set { _decsegundosatencionusuario = value; }
        }

        public Int16 IdMonedaNacional
        {
            get { return _intidmonedanacional; }
            set { _intidmonedanacional = value; }
        }

        public clsUsuarioPuntoAtencion UsuarioPuntoAtencion
        {
            get { return _objusuariopuntoatencion; }
            set { _objusuariopuntoatencion = value; }
        }

        public Int32 IdCheckSum
        {
            get { return _intidchecksum; }
            set { _intidchecksum = value; }
        }

        /// <summary>
        /// Determina si es Operacion de Cobranza o Operacion de Extorno.
        /// Los Identificadores obtengalos de clsRecurso. Por ejemplo clsRecurso.IdOperacionCobranza
        /// </summary>
        public TipoOperacionCobranza TipoOperacion
        {
            get { return _enumtipooperacion; }
            set { _enumtipooperacion = value; }
        }

        /// <summary>
        /// Propiedad agregado para generar el convenio en el esquema DBO.
        /// </summary>
        public Int32? IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        /// <summary>
        /// Propiedad agregado para actualizar los datos del documento generado en la Solicitud.
        /// </summary>
        public Int32? IdSolicitud
        {
            get { return _intidsolicitud; }
            set { _intidsolicitud = value; }
        }
        public Int32 NroTransaccionOriginal { get; set; } //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL
        #endregion Propiedades

        #region Elementos

        public List<clsDocumentosDescargoBase> ElementosDocumentoDescargo
        {
            get { return _elementosdocumentodescargo; }
            set { _elementosdocumentodescargo = value; }
        }

        public List<clsMediosPagoBase> ElementosMedioPago
        {
            get { return _elementosmediopago; }
            set { _elementosmediopago = value; }
        }

        public List<clsCajaEstadisticaDiarioMoneda> ElementosCajaServicioMoneda
        {
            get { return _elementoscajamoneda; }
            set { _elementoscajamoneda = value; }
        }

        public List<clsCajaEstadisticaServicio> ElementoCajaServicio
        {
            get { return _elementocajaestadisticaservicio; }
            set { _elementocajaestadisticaservicio = value; }
        }

        /// <summary>
        /// Todos estos datos seran Creado en la Tabla Documentos_a_favor.
        /// </summary>
        public clsListaDocumentosAFavor ListaDocumentoAfavor
        {
            get { return _listadocumentoafavor; }
            set { _listadocumentoafavor = value; }
        }

        /// <summary>
        /// Todos estos datos seran Creado en la Tabla IngresoProvision, cuando se llame a la cobranza.
        /// Por Ejemplo: Crear Deposito Garantia
        /// </summary>
        public clsListaIngresoProvision ListaIngresoProvision
        {
            get { return _listaingresoprovision; }
            set { _listaingresoprovision = value; }
        }

        #endregion Elementos

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        public clsTransaccion()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tipooperacion"></param>
        public clsTransaccion(TipoOperacionCobranza tipooperacion)
        {
            this.TipoOperacion = tipooperacion;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        /// <param name="tipooperacion"></param>
        public clsTransaccion(DataRow registro, TipoOperacionCobranza tipooperacion)
        {
            this.NroTransaccion = Convert.ToInt32(registro["nrotransaccion"]);
            this.FechaPago = Convert.ToDateTime(registro["fechapago"]);
            this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            this.ImporteNeto = Convert.ToDecimal(registro["importe"]);
            this.ImporteRecibido = Convert.ToDecimal(registro["importerecibido"]);
            this.ImporteTotalVuelto = Convert.ToDecimal(registro["vuelto"]);
            if (registro.Table.Columns.Contains("idchecksum")) { IdCheckSum = Convert.ToInt32(registro["idchecksum"]); }
            if (registro.Table.Columns.Contains("fecharegistro"))
                this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            this.TipoOperacion = tipooperacion;
        }

        #endregion Constructor
    }

    #endregion Transaccion - V2

    #region CajaEstadisticaServicio

    public class clsCajaEstadisticaServicio
    {
        private Int32 _intidtransaccionlog;
        private Int32 _intidnrocaja;
        private Int16 _intidservicio;
        private Int16 _intcantidadnroservicio;
        private Int16 _intcantidaddocumentos;

        public clsCajaEstadisticaServicio(Int32 idtransaccionlog, Int32 idnrocaja, Int16 idservicio, Int16 cantidadnroservicio, Int16 cantidaddocumentos)
        {
            this.IdTransaccionLog = idtransaccionlog;
            this.IdNroCaja = idnrocaja;
            this.IdServicio = idservicio;
            this.CantidadNroServicio = cantidadnroservicio;
            this.CantidadDocumentos = cantidaddocumentos;
        }

        public clsCajaEstadisticaServicio()
        {
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Int16 CantidadNroServicio
        {
            get { return _intcantidadnroservicio; }
            set { _intcantidadnroservicio = value; }
        }

        public Int16 CantidadDocumentos
        {
            get { return _intcantidaddocumentos; }
            set { _intcantidaddocumentos = value; }
        }
    }

    #endregion CajaEstadisticaServicio

    #region clsIngresoProvisionDetalle

    public class clsIngresoProvisionDetalle
    {
        private int intidConcepto;
        private decimal decImporte;
        private int intCRC;
        private decimal decCantidad;
        private decimal decPrecioUnitario;
        private decimal? decPrecioUnitarioSinFose;
        private Int16 _intidempresa;
        private short intidTipoDocumento;
        private short intidTipoDocumentoSustento;
        private string strNroDocumento;
        private string strNroDocumentoSustento;
        private Int32 _intidnroservicio;
        private Int16 _intindicadorigv;
        private Int16 _intcodigoenlace;

        private clsIngresoProvisionDetalleComplemento _oingresoprovisiondetallecomplemento; //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL

        public clsIngresoProvisionDetalle(Int16 idempresa, short _idTipoDocumento
            , string _NroDocumento, short _idTipoDocumentoSustento
            , string _NroDocumentoSustento, int _idConcepto
            , decimal _Importe, int _CRC, decimal _Cantidad
            , decimal _PrecioUnitario, decimal? _PrecioUnitarioSinFose
            , Int32 _IdNroServicio, Int16 _IndicadorIGV)
        {
            this.IdEmpresa = idempresa;
            this.idTipoDocumento = _idTipoDocumento;
            this.NroDocumento = _NroDocumento;
            this.idTipoDocumentoSustento = _idTipoDocumentoSustento;
            this.NroDocumentoSustento = _NroDocumentoSustento;
            this.idConcepto = _idConcepto;
            this.Importe = _Importe;
            this.CRC = _CRC;
            this.Cantidad = _Cantidad;
            this.PrecioUnitario = _PrecioUnitario;
            this.PrecioUnitarioSinFose = _PrecioUnitarioSinFose;
            this.IdNroServicio = _IdNroServicio;
            this.IndicadorIGV = _IndicadorIGV;
        }
        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        public clsIngresoProvisionDetalle(Int16 idempresa, short _idTipoDocumento
            , string _NroDocumento, short _idTipoDocumentoSustento
            , string _NroDocumentoSustento, int _idConcepto
            , decimal _Importe, int _CRC, decimal _Cantidad
            , decimal _PrecioUnitario, decimal? _PrecioUnitarioSinFose
            , Int32 _IdNroServicio, Int16 _IndicadorIGV, clsIngresoProvisionDetalleComplemento oComplemento)
        {
            this.IdEmpresa = idempresa;
            this.idTipoDocumento = _idTipoDocumento;
            this.NroDocumento = _NroDocumento;
            this.idTipoDocumentoSustento = _idTipoDocumentoSustento;
            this.NroDocumentoSustento = _NroDocumentoSustento;
            this.idConcepto = _idConcepto;
            this.Importe = _Importe;
            this.CRC = _CRC;
            this.Cantidad = _Cantidad;
            this.PrecioUnitario = _PrecioUnitario;
            this.PrecioUnitarioSinFose = _PrecioUnitarioSinFose;
            this.IdNroServicio = _IdNroServicio;
            this.IndicadorIGV = _IndicadorIGV;
            this.oIngresoProvisionDetalleComplemento = oComplemento;
        }
        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        public clsIngresoProvisionDetalle(DataRow drregistro)
        {
            this.IdEmpresa = Convert.ToInt16(drregistro["idempresa"]);
            this.idTipoDocumento = Convert.ToInt16(drregistro["idtipodocumento"]);
            this.NroDocumento = drregistro["nrodocumento"].ToString();
            this.idTipoDocumentoSustento = Convert.ToInt16(drregistro["idtipodocumentosustento"]);
            this.NroDocumentoSustento = drregistro["nrodocumentosustento"].ToString();
            this.idConcepto = Convert.ToInt32(drregistro["idconcepto"]);
            this.Importe = Convert.ToDecimal(drregistro["importe"]);
            this.CRC = Convert.ToInt32(drregistro["crc"]);
            this.Cantidad = Convert.ToDecimal(drregistro["cantidad"]);
            this.PrecioUnitario = Convert.ToDecimal(drregistro["preciounitario"]);
            this.PrecioUnitarioSinFose = drregistro.Table.Columns.Contains("preciounitariosinfose") ? (Convert.IsDBNull(drregistro["preciounitariosinfose"]) ? (Decimal?)null : Convert.ToDecimal(drregistro["preciounitariosinfose"])) : (Decimal?)null;
            this.IdNroServicio = Convert.ToInt32(drregistro["idnroservicio"]);
            this.IndicadorIGV = Convert.ToInt16(drregistro["indicadorigv"]);
        }

        public clsIngresoProvisionDetalle()
        {
        }

        public int idConcepto
        {
            get { return intidConcepto; }
            set { intidConcepto = value; }
        }

        public decimal Importe
        {
            get { return decImporte; }
            set { decImporte = value; }
        }

        public int CRC
        {
            get { return intCRC; }
            set { intCRC = value; }
        }

        public decimal Cantidad
        {
            get { return decCantidad; }
            set { decCantidad = value; }
        }

        public decimal PrecioUnitario
        {
            get { return decPrecioUnitario; }
            set { decPrecioUnitario = value; }
        }

        public decimal? PrecioUnitarioSinFose
        {
            get { return decPrecioUnitarioSinFose; }
            set { decPrecioUnitarioSinFose = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public short idTipoDocumento
        {
            get { return intidTipoDocumento; }
            set { intidTipoDocumento = value; }
        }

        public short idTipoDocumentoSustento
        {
            get { return intidTipoDocumentoSustento; }
            set { intidTipoDocumentoSustento = value; }
        }

        public string NroDocumento
        {
            get { return strNroDocumento; }
            set { strNroDocumento = value; }
        }

        public string NroDocumentoSustento
        {
            get { return strNroDocumentoSustento; }
            set { strNroDocumentoSustento = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public Int16 IndicadorIGV
        {
            get { return _intindicadorigv; }
            set { _intindicadorigv = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }
        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        public clsIngresoProvisionDetalleComplemento oIngresoProvisionDetalleComplemento
        {
            get { return _oingresoprovisiondetallecomplemento; }
            set { _oingresoprovisiondetallecomplemento = value; }
        }
        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
    }
    #endregion clsIngresoProvisionDetalle

    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
    #region clsIngresoProvisionDetalleComplemento
    public class clsIngresoProvisionDetalleComplemento
    {
        #region Campos

        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private Int16 _intidtipodocumentosustento;
        private String _strnrodocumentosustento;
        private Int32 _intidnroservicio;
        private Int16 _intidconcepto;
        private Int16 _intesexonerada;
        private Int16 _intesretiropremio;
        private Int16 _intesretirotrabajador;
        private Int16 _intidunidadmedida;
        private String _strobservacion;

        private Int16 _intaplicadescuento;
        private Decimal _decimportedescuento;
        private Decimal _decpreciounitario;

        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 idTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 idTipoDocumentoSustento
        {
            get { return _intidtipodocumentosustento; }
            set { _intidtipodocumentosustento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroDocumentoSustento
        {
            get { return _strnrodocumentosustento; }
            set { _strnrodocumentosustento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 idNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 idConcepto
        {
            get { return _intidconcepto; }
            set { _intidconcepto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsExonerada
        {
            get { return _intesexonerada; }
            set { _intesexonerada = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsRetiroPremio
        {
            get { return _intesretiropremio; }
            set { _intesretiropremio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsRetiroTrabajador
        {
            get { return _intesretirotrabajador; }
            set { _intesretirotrabajador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdUnidadMedida
        {
            get { return _intidunidadmedida; }
            set { _intidunidadmedida = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Observacion
        {
            get { return _strobservacion; }
            set { _strobservacion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 AplicaDescuento
        {
            get { return _intaplicadescuento; }
            set { _intaplicadescuento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ImporteDescuento
        {
            get { return _decimportedescuento; }
            set { _decimportedescuento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal PrecioUnitario
        {
            get { return _decpreciounitario; }
            set { _decpreciounitario = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsIngresoProvisionDetalleComplemento()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsIngresoProvisionDetalleComplemento(DataRow dr)
        {
            if (dr == null) { return; }

            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            idTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            NroDocumento = dr["nrodocumento"].ToString();
            idTipoDocumentoSustento = Convert.ToInt16(dr["idtipodocumentosustento"]);
            NroDocumentoSustento = dr["nrodocumentosustento"].ToString();
            idNroServicio = Convert.ToInt32(dr["idnroservicio"]);
            idConcepto = Convert.ToInt16(dr["idconcepto"]);
            EsExonerada = Convert.ToInt16(dr["esexonerada"]);
            EsRetiroPremio = Convert.ToInt16(dr["esretiropremio"]);
            EsRetiroTrabajador = Convert.ToInt16(dr["esretirotrabajador"]);
            IdUnidadMedida = Convert.ToInt16(dr["idunidadmedida"]);
            Observacion = dr["observacion"].ToString();

            AplicaDescuento = Convert.ToInt16(dr["aplicadescuento"]);
            ImporteDescuento = Convert.ToDecimal(dr["importedeuda"]);
            if (dr.Table.Columns.Contains("preciounitario")) PrecioUnitario = Convert.ToDecimal(dr["importedeuda"]);
        }

        #endregion

    }
    #endregion clsIngresoProvisionDetalleComplemento

    #region clsIngreProvisionComplemento
    public class clsIngresoProvisionComplemento
    {
        #region Campos

        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private String _strserie;
        private String _strcorrelativo;
        private DateTime _datfechacontabilizacion;
        private Decimal _dectipocambio;
        private String _strdetalle;
        private Int16 _intaplicadetraccion;
        private String _strnombre;
        private String _strdireccion;
        private String _strcodigosap;
        private String _stremail;

        private String _strrutaarchivoxml;
        private String _strrutaarchivopdf;
        private String _strrutarespuestasunat;
        private String _strcodigorespuestasunat;
        private String _strmensajerespuestasunat;
        private String _strticket;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Serie
        {
            get { return _strserie; }
            set { _strserie = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Correlativo
        {
            get { return _strcorrelativo; }
            set { _strcorrelativo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaContabilizacion
        {
            get { return _datfechacontabilizacion; }
            set { _datfechacontabilizacion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Detalle
        {
            get { return _strdetalle; }
            set { _strdetalle = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 AplicaDetraccion
        {
            get { return _intaplicadetraccion; }
            set { _intaplicadetraccion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String CodigoSAP
        {
            get { return _strcodigosap; }
            set { _strcodigosap = value; }
        }

        public String EMail
        {
            get { return _stremail; }
            set { _stremail = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String RutaArchivoPDF
        {
            get { return _strrutaarchivopdf; }
            set { _strrutaarchivopdf = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String RutaArchivoXML
        {
            get { return _strrutaarchivoxml; }
            set { _strrutaarchivoxml = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        public String MensajeRespuestaSUNAT
        {
            get { return _strmensajerespuestasunat; }
            set { _strmensajerespuestasunat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String CodigoRespuestaSUNAT
        {
            get { return _strcodigorespuestasunat; }
            set { _strcodigorespuestasunat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String RutaRespuestaSUNAT
        {
            get { return _strrutarespuestasunat; }
            set { _strrutarespuestasunat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Ticket
        {
            get { return _strticket; }
            set { _strticket = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsIngresoProvisionComplemento()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsIngresoProvisionComplemento(DataRow dr)
        {
            if (dr == null) { return; }

            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            NroDocumento = dr["nrodocumento"].ToString();
            Serie = dr["serie"].ToString();
            Correlativo = dr["correlativo"].ToString();
            FechaContabilizacion = Convert.ToDateTime(dr["fechacontabilizacion"]);
            TipoCambio = Convert.ToDecimal(dr["tipocambio"]);
            Detalle = dr["detalle"].ToString();
            AplicaDetraccion = Convert.ToInt16(dr["aplicadetraccion"]);
            Nombre = dr["nombre"].ToString();
            Direccion = dr["direccion"].ToString();
            CodigoSAP = dr["codigosap"].ToString();
            EMail = dr["email"].ToString();

            RutaArchivoPDF = dr["rutarchivopdf"].ToString();
            RutaArchivoXML = dr["rutaarchivoxml"].ToString();

            CodigoRespuestaSUNAT = dr["codigorespuestasunat"].ToString();
            MensajeRespuestaSUNAT = dr["mensajerespuestasunat"].ToString();
            RutaRespuestaSUNAT = dr["rutarespuestasunat"].ToString();

            if (dr.Table.Columns.Contains("ticket"))
                Ticket = dr["ticket"].ToString();
        }

        #endregion

    }

    #endregion clsIngresoProvisionComplemento
    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
    #region clsIngresoProvisionReferencias

    public class clsIngresoProvisionReferencias
    {
        #region Campos

        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private Int16 _intidempresareferenciado;
        private Int16 _intidtipodocumentoreferenciado;
        private String _strnrodocumentoreferenciado;
        private DateTime _datfechareferencia;
        private Int16 _intidestado;
        private Int16 _intcodigoenlace;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresaReferenciado
        {
            get { return _intidempresareferenciado; }
            set { _intidempresareferenciado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoDocumentoReferenciado
        {
            get { return _intidtipodocumentoreferenciado; }
            set { _intidtipodocumentoreferenciado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NroDocumentoReferenciado
        {
            get { return _strnrodocumentoreferenciado; }
            set { _strnrodocumentoreferenciado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaReferencia
        {
            get { return _datfechareferencia; }
            set { _datfechareferencia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsIngresoProvisionReferencias()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsIngresoProvisionReferencias(DataRow dr)
        {
            if (dr == null) { return; }

            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            NroDocumento = dr["nrodocumento"].ToString();
            IdEmpresaReferenciado = Convert.ToInt16(dr["idempresareferenciado"]);
            IdTipoDocumentoReferenciado = Convert.ToInt16(dr["idtipodocumentoreferenciado"]);
            NroDocumentoReferenciado = dr["nrodocumentoreferenciado"].ToString();
            FechaReferencia = Convert.ToDateTime(dr["fechareferencia"]);
            IdEstado = Convert.ToInt16(dr["idestado"]);
        }

        #endregion Constructor
    }

    #endregion clsIngresoProvisionReferencias

    #region clsIngresoProvision

    public class clsIngresoProvision : clsIngresoProvisionBase
    {
        #region Campos

        private String _strserie;
        private Decimal _decimportetotal;
        private Decimal _decmontodescargado;
        private Decimal _decsaldo;

        //private Decimal _decimportecobrar;
        private Int32 _intlotefacturacion;

        private Int16 _intidtipoatencion;
        private Int16 _intidpuntoatencion;
        private Int16 _intnromesesdeuda;
        private Decimal _decdeudaanteriorinicial;
        private Int16 _intidcentroservicio;
        private Int16 _intidmovimientocomercial;

        private Decimal _decimportedescargadoorigen;
        private Decimal _decimportecontrolsaldoorigen;
        private Boolean _booexcluirvalidacioninterna;

        private Int32? _intperiodopago;
        private DateTime? _datfechapago;

        private System.ComponentModel.BindingList<clsIngresoProvisionDocumentos> _objlistaingresoprovisiondocumentos = new System.ComponentModel.BindingList<clsIngresoProvisionDocumentos>();
        private System.ComponentModel.BindingList<clsIngresoProvisionDetalle> _objlistaingresoprovisiondetalles = new System.ComponentModel.BindingList<clsIngresoProvisionDetalle>();
        private System.ComponentModel.BindingList<clsIngresoProvisionReferencias> _objlistaingresoprovisionreferencias = new System.ComponentModel.BindingList<clsIngresoProvisionReferencias>();

        private clsListaAsientoDocumentoValida _objlistaasientodocumentovalida;
        private Int32 _intidreintegro;
        private Int16 _intidformapagocobro; //Para el proceso de Reintegro y Recuperos
        private Int16 _intcodigoenlace;

        private clsIngresoProvisionComplemento _oIngresoProvisionComplemento; //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL

        #endregion Campos

        #region Contructor

        public clsIngresoProvision(DataRow registro)
        {
            if (registro == null) { return; }
            AsignarRegistro(registro);
        }

        public clsIngresoProvision(DataRow registro, DataRow[] detalle)
        {
            if (registro == null) { return; }
            _booexcluirvalidacioninterna = true;

            AsignarRegistro(registro);

            if (detalle != null)
            {
                foreach (DataRow _drw in detalle)
                {
                    if (Convert.ToInt16(_drw["indicadorreclamo"]) == 1) { IndicadorReclamo = true; }
                    _objlistaingresoprovisiondocumentos.Add(new clsIngresoProvisionDocumentos(_drw));
                }
            }

            _booexcluirvalidacioninterna = false;
        }

        private void AsignarRegistro(DataRow registro)
        {
            this.Sel = (Boolean)registro["sel"];
            this.IdEmpresa = (Int16)registro["idempresa"];
            this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            this.NroDocumento = registro["nrodocumento"].ToString();
            this.Serie = registro["serie"].ToString();
            this.NombreCliente = registro["nombrecliente"].ToString();
            this.Direccion = registro["direccion"].ToString();
            this.IdTipoIdentidad = (Int16)registro["idtipoidentidad"];
            this.NroIdentidad = registro["nroidentidad"].ToString();

            this.FechaRegistro = (DateTime)registro["fecharegistro"];
            this.FechaEmision = (DateTime)registro["fechaemision"];
            this.FechaVencimiento = (DateTime)registro["fechavencimiento"];
            //VAS
            this.LoteFacturacion = Convert.IsDBNull(registro["lotefacturacion"]) ? 0 : (Int32)registro["lotefacturacion"];

            this.IdEstadoDeuda = (Int16)registro["estadodeuda"];
            this.IdTipoAtencion = (Int16)registro["idtipoatencion"];
            this.IdPuntoAtencion = (Int16)registro["idpuntoatencion"];
            this.IdUsuario = (Int32)registro["idusuario"];
            this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            this.IdNroServicioPrincipal = (Int32)registro["idnroservicioprincipal"];

            this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            this.ImporteOrigenTotal = (Decimal)registro["importetotal"];
            this.ImporteOrigenDescargo = (Decimal)registro["montodescargado"];
            this.ImporteOrigenSaldo = (Decimal)registro["saldo"];
            this.ImporteOrigenCobrar = (Decimal)registro["saldo"];
            //VAS
            this.ImporteReclamo = (Decimal)registro["importeenreclamo"];
            this.IndicadorReclamo = Convert.ToBoolean(registro["indicadorreclamo"]);

            if (registro.Table.Columns.Contains("estadosuministro"))
                this.EstadoSuministro = Convert.ToInt16(registro["estadosuministro"]);
            if (registro.Table.Columns.Contains("idanocomercial"))
                this.IdAnoComercial = Convert.ToInt16(registro["idanocomercial"]);
            if (registro.Table.Columns.Contains("idmescomercial"))
                this.IdMesComercial = Convert.ToInt16(registro["idmescomercial"]);
            if (this.IdAnoComercial > 0 && this.IdMesComercial > 0)
                this.Periodo = (IdAnoComercial * 100) + IdMesComercial;

            if (registro.Table.Columns.Contains("periodopago"))
                this.PeriodoPago = Convert.IsDBNull(registro["periodopago"]) ? (Int32?)null : Convert.ToInt32(registro["periodopago"]);
            if (registro.Table.Columns.Contains("fechapago"))
                this.FechaPago = Convert.IsDBNull(registro["fechapago"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechapago"]);

            if (registro.Table.Columns.Contains("importepagofijo"))
                this.ImportePagoFijo = Convert.IsDBNull(registro["importepagofijo"]) ? (Decimal)0 : Convert.ToDecimal(registro["importepagofijo"]);
        }

        public clsIngresoProvision(Int16 idempresa, Int16 idtipodocumento, String nrodocumento, String serie, String nombrecliente, Int16 idtipoidentidad
                                 , String nroidentidad, Int16 idmoneda, Decimal importetotal, Decimal montodescargado, Decimal saldo
                                 , DateTime fecharegistro, DateTime fechaemision, DateTime fechavencimiento, Int32 lotefacturacion, Int16 idestadodeuda
                                 , Int16 idtipoatencion, Int16 idpuntoatencion, Int32 idusuario, Int16 idservicioprincipal, Int32 idnroservicioprincipal
                                 , Boolean? escobranzaterceros)
        {
            this.IdEmpresa = idempresa;
            this.IdTipoDocumento = idtipodocumento;
            this.NroDocumento = nrodocumento;
            this.Serie = serie;
            this.NombreCliente = nombrecliente;
            this.IdTipoIdentidad = idtipoidentidad;
            this.NroIdentidad = nroidentidad;
            this.IdMonedaCambio = idmoneda;
            this.ImporteOrigenTotal = importetotal;
            this.ImporteOrigenDescargo = montodescargado;
            this.ImporteOrigenSaldo = saldo;
            this.ImporteOrigenCobrar = saldo;
            this.FechaRegistro = fecharegistro;
            this.FechaEmision = fechaemision;
            this.FechaVencimiento = fechavencimiento;
            this.LoteFacturacion = lotefacturacion;
            this.IdEstadoDeuda = idestadodeuda;
            this.IdTipoAtencion = idtipoatencion;
            this.IdPuntoAtencion = idpuntoatencion;
            this.IdUsuario = idusuario;
            this.IdServicioPrincipal = idservicioprincipal;
            this.IdNroServicioPrincipal = idnroservicioprincipal;
        }

        public clsIngresoProvision()
        {
        }

        #endregion Contructor

        #region Propiedades - Generales
        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        public clsIngresoProvisionComplemento oIngresoProvisionComplemento
        {
            get { return _oIngresoProvisionComplemento; }
            set { _oIngresoProvisionComplemento = value; }
        }
        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
        public String Serie
        {
            get { return _strserie; }
            set { _strserie = value; }
        }

        public Int32 LoteFacturacion
        {
            get { return _intlotefacturacion; }
            set { _intlotefacturacion = value; }
        }

        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        public Int16 NroMesesDeuda
        {
            get { return _intnromesesdeuda; }
            set { _intnromesesdeuda = value; }
        }

        public Decimal DeudaAnteriorInicial
        {
            get { return _decdeudaanteriorinicial; }
            set { _decdeudaanteriorinicial = value; }
        }

        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        public Int16 IdMovimientoComercial
        {
            get { return _intidmovimientocomercial; }
            set { _intidmovimientocomercial = value; }
        }

        public Int32? PeriodoPago
        {
            get { return _intperiodopago; }
            set { _intperiodopago = value; }
        }

        public DateTime? FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int32 IdReintegro
        {
            get { return _intidreintegro; }
            set { _intidreintegro = value; }
        }

        public Int16 IdFormaPagoCobro
        {
            get { return _intidformapagocobro; }
            set { _intidformapagocobro = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades - Generales

        #region Propiedades - Redefinidas por Herencia

        public Boolean Sel
        {
            get { return _boosel; }
            set
            {
                //Si esta Pagado NO Seleccionar
                if ((Estado)IdEstadoDeuda == Estado.Pagado) { _boosel = false; return; }

                //Si Deselecciono y Tipo Cambio y ImporteCambioSaldo es Mayor que Cero
                if ((!value) || (TipoCambio > 0 && ImporteCambioSaldo > 0))
                { _boosel = value; return; }
                //Si Excluye validación interna
                if (_booexcluirvalidacioninterna || IdMonedaOrigen == 0) { _boosel = value; return; }

                _boosel = false;
                StringBuilder _sbmensaje = new StringBuilder();

                if (TipoCambio == 0)
                {
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("!Ingrese Tipo Cambio!");
                }

                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("No puede seleccionar documento = " + _intidtipodocumento.ToString() + " - " + _strnrodocumento);
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("Importe de Tipo Cambio  = " + TipoCambio.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Importe Saldo de Cambio = " + ImporteCambioSaldo.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Moneda de cambio        = " + IdMonedaCambio.ToString());
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("Importe Total Origen  = " + ImporteOrigenTotal.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Importe Saldo Origen  = " + ImporteOrigenSaldo.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Moneda Origen         = " + IdMonedaOrigen.ToString());
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("");

                throw new Exception(_sbmensaje.ToString());
            }
        }

        public override Decimal ImporteOrigenCobrar
        {
            get { return _decimportecobrar; }
            set
            {
                Decimal _decimporteacobrar = value;
                if (_decimporteacobrar > ImporteCambioSaldo && ImporteCambioSaldo != 0)
                {
                    StringBuilder _sbmensaje = new StringBuilder();
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    _sbmensaje.Append("Saldo a Cobrar ");
                    _sbmensaje.Append(_decimporteacobrar.ToString());
                    _sbmensaje.Append(" no puede ser mayor a Saldo Original de ");
                    _sbmensaje.Append(ImporteCambioSaldo.ToString());
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    throw new Exception(_sbmensaje.ToString());
                }
                Decimal _decimportecobratmp = _decimporteacobrar;
                Boolean _booCobroTotal = (_decimporteacobrar == ImporteCambioSaldo);

                _decimportecobrar = _decimporteacobrar;
                if (!AplicarEnHijos) { return; }

                //VAS
                Boolean _boolReclamo = false;
                foreach (clsIngresoProvisionDocumentos itemingresoprovisiondocumentos in ListaIngresoProvisionDocumentos)
                {
                    //VAS - si el indicador de reclamo es 1 no se toma encuenta este documento en la deuda y no se
                    // incluye en la distribucion del monto a cobrar
                    if (itemingresoprovisiondocumentos.FechaLimitePago != null)
                        _boolReclamo = (itemingresoprovisiondocumentos.IndicadorReclamo == 1 &&
                            itemingresoprovisiondocumentos.FechaLimitePago > this.FechaServidor);

                    if (itemingresoprovisiondocumentos.IndicadorReclamo == 0 || _boolReclamo)
                    {
                        //Si se distribuye saldo entonces marcar por defecto la selección.
                        //ya que si es cero, no se tomara en cuenta para la cobranza
                        itemingresoprovisiondocumentos.Sel = true;
                        //Si se Cobra Todo El Importe Entonces restaurar los saldos
                        if (_booCobroTotal)
                        {
                            itemingresoprovisiondocumentos.Saldo = itemingresoprovisiondocumentos.SaldoOriginal;
                            continue;
                        }
                        //Si el importe a cobrar es menor entonces se distribuye hasta alcanzar el tope de cobro parcial
                        //1. Si importe a cobrar llego a cero entonces asignar cobro cero a todos los demás items
                        if (_decimportecobratmp == 0 || itemingresoprovisiondocumentos.SaldoOriginal < 0)
                        {
                            itemingresoprovisiondocumentos.Saldo = 0;
                            continue;
                        }
                        //2. Si el importe a cobrar es mayor a saldo original
                        if (_decimportecobratmp > itemingresoprovisiondocumentos.SaldoOriginal)
                        {
                            itemingresoprovisiondocumentos.Saldo = itemingresoprovisiondocumentos.SaldoOriginal;
                            _decimportecobratmp -= itemingresoprovisiondocumentos.SaldoOriginal; //_decimportecobratmp - itemingresoprovisiondocumentos.SaldoOriginal;
                        }
                        else
                        {
                            itemingresoprovisiondocumentos.Saldo = _decimportecobratmp;
                            _decimportecobratmp = 0;// _decimportecobratmp - _decimportecobratmp;
                        }
                    }
                }
            }
        }

        public override Int32 NroItems
        {
            get { return ListaIngresoProvisionDocumentos.Count; }
        }

        #endregion Propiedades - Redefinidas por Herencia

        #region Propiedades Listas

        public System.ComponentModel.BindingList<clsIngresoProvisionDocumentos> ListaIngresoProvisionDocumentos
        {
            get { return _objlistaingresoprovisiondocumentos; }
            set { _objlistaingresoprovisiondocumentos = value; }
        }

        public System.ComponentModel.BindingList<clsIngresoProvisionDetalle> ListaIngresoProvisionDetalles
        {
            get { return _objlistaingresoprovisiondetalles; }
            set { _objlistaingresoprovisiondetalles = value; }
        }

        public System.ComponentModel.BindingList<clsIngresoProvisionReferencias> ListaIngresoProvisionReferencias
        {
            get { return _objlistaingresoprovisionreferencias; }
            set { _objlistaingresoprovisionreferencias = value; }
        }

        public clsListaAsientoDocumentoValida ListaAsientoDocumentoValida
        {
            get { return _objlistaasientodocumentovalida; }
            set { _objlistaasientodocumentovalida = value; }
        }

        #endregion Propiedades Listas
    }

    #endregion clsIngresoProvision

    #region clsDocumentosAFavor

    public class clsDocumentosAFavor
    {
        #region Campos
        private Int16 _intidempresa;
        private String _strnrodocumentoafavor;
        private Int16 _intidtipodocumentoafavor;
        private Int16 _intidmoneda;
        private Decimal _decimporte;
        private Decimal _decsaldoafavor;
        private DateTime _datfecharegistro;
        private DateTime? _datfechavencimiento;
        private String _strdocumentoreferencia;
        private Int16 _intidpuntoatencion;
        private Int16 _intidtipoatencion;
        private Int32 _stridusuario;
        private Int32 _intidusuarioautoriza;
        private Int32 _intidnroservicio;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private Int32 _intidnrocaja;
        private Int16 _intmotivo;
        private String _strdescripcion;
        private Int16 _intestadoafavor;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private String _strmonedasimbolo;
        private String _strnombrecliente;
        private String _strdireccioncliente;
        private Int16 _intidmovimientocomercial;
        private Int16 _intidservicio;
        private Decimal _dectipocambio;
        private String _strnombredocumento;
        private String _strNombreCentroServicio;
        private String _strNombreUnidadNegocio;
        private String _strNombreEmpresa;
        private Decimal _dectasaigv;

        //add 07/03/2013
        private Int16 _intdevolvercliente;

        private Int16 _intcodigoenlace;

        #endregion Campos

        #region Constructor

        public clsDocumentosAFavor
            (String nrodocumentoafavor
            , Int16 idtipodocumentoafavor
            , Decimal saldoafavor
            , Decimal importe
            , Int16 idestadoafavor
            , DateTime fecharegistro
            , String documentoreferencia
            , Int16 idmoneda
            , Int16 idpuntoatencion
            , Int16 idtipodocumento
            , String nrodocumento
            , Int16 idtipoatencion
            , Int32 idusuario
            , Int16 idtipoidentidad
            , String nroidentidad
            , Int32 idnroservicio
            , String monedasimbolo
            , Int16 idempresa)
        {
            this.NroDocumentoAFavor = nrodocumentoafavor;
            this.IdTipoDocumentoAFavor = idtipodocumentoafavor;
            this.SaldoaFavor = saldoafavor;
            this.Importe = importe;
            this.IdEstadoaFavor = idestadoafavor;
            this.FechaRegistro = fecharegistro;
            this.DocumentoReferencia = documentoreferencia;
            this.IdMonedaOrigen = idmoneda;
            this.IdPuntoAtencion = idpuntoatencion;
            this.IdTipoDocumento = idtipodocumento;
            this.NroDocumento = nrodocumento;
            this.IdTipoAtencion = idtipoatencion;
            this.IdUsuario = idusuario;
            this.IdTipoIdentidad = idtipoidentidad;
            this.NroIdentidad = nroidentidad;
            this.IdNroServicio = idnroservicio;
            this.MonedaSimbolo = monedasimbolo;
            this.IdEmpresa = idempresa;
        }

        public clsDocumentosAFavor(DataRow registro)
        {
            this.IdEmpresa = (Int16)registro["idempresa"];
            this.NroDocumentoAFavor = registro["nrodocumentoafavor"].ToString();
            this.IdTipoDocumentoAFavor = (Int16)registro["idtipodocumentoafavor"];
            this.SaldoaFavor = (Decimal)registro["saldoafavor"];
            this.Importe = (Decimal)registro["importe"];
            this.IdEstadoaFavor = (Int16)registro["estadoafavor"];
            this.FechaRegistro = (DateTime)registro["fecharegistro"];
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.IsDBNull(registro["fechavencimiento"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("documentoreferencia")) this.DocumentoReferencia = registro["documentoreferencia"].ToString();
            if (registro.Table.Columns.Contains("idmoneda")) this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("idpuntoatencion")) this.IdPuntoAtencion = (Int16)registro["idpuntoatencion"];
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            if (registro.Table.Columns.Contains("idtipoatencion")) this.IdTipoAtencion = (Int16)registro["idtipoatencion"];
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (registro.Table.Columns.Contains("idtipoidentidad")) this.IdTipoIdentidad = (Int16)registro["idtipoidentidad"];
            if (registro.Table.Columns.Contains("nroidentidad")) this.NroIdentidad = registro["nroidentidad"].ToString();
            if (registro.Table.Columns.Contains("idmotivo")) this.IdMotivoConstitucion = (Int16)registro["idmotivo"];
            if (registro.Table.Columns.Contains("descripcion")) this.Descripcion = registro["descripcion"].ToString().Trim();
            if (registro.Table.Columns.Contains("simbolomoneda")) this.MonedaSimbolo = registro["simbolomoneda"].ToString();
            if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = registro["nombrecliente"].ToString().Trim();
            if (registro.Table.Columns.Contains("direccion")) this.DireccionCliente = registro["direccion"].ToString().Trim();
            if (registro.Table.Columns.Contains("nombretipodocumento")) this.NombreTipoDocumento = registro["nombretipodocumento"].ToString().Trim();
            this.IdNroServicio = registro.IsNull("idnroservicio") ? 0 : (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("NombreEmpresa")) this.NombreEmpresa = registro["NombreEmpresa"].ToString().Trim();
            if (registro.Table.Columns.Contains("NombreUnidadNegocio")) this.NombreUnidadNegocio = registro["NombreUnidadNegocio"].ToString().Trim();
            if (registro.Table.Columns.Contains("NombreCentroServicio")) this.NombreCentroServicio = registro["NombreCentroServicio"].ToString().Trim();

            if (registro.Table.Columns.Contains("devolvercliente")) this.DevolverCliente = (Int16)registro["devolvercliente"];
            //this.IdNroServicio = (Int32)registro["idnroservicio"];
        }

        public clsDocumentosAFavor()
        {
        }

        #endregion Constructor

        #region Propiedades

        public String NroDocumentoAFavor
        {
            get { return _strnrodocumentoafavor; }
            set { _strnrodocumentoafavor = value; }
        }

        public Int16 IdTipoDocumentoAFavor
        {
            get { return _intidtipodocumentoafavor; }
            set { _intidtipodocumentoafavor = value; }
        }

        public Decimal SaldoaFavor
        {
            get { return _decsaldoafavor; }
            set { _decsaldoafavor = value; }
        }

        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        public Int16 IdEstadoaFavor
        {
            get { return _intestadoafavor; }
            set { _intestadoafavor = value; }
        }

        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        public DateTime? FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        public String DocumentoReferencia
        {
            get { return _strdocumentoreferencia; }
            set { _strdocumentoreferencia = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public String MonedaSimbolo
        {
            get { return _strmonedasimbolo; }
            set { _strmonedasimbolo = value; }
        }

        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        public Int32 IdUsuario
        {
            get { return _stridusuario; }
            set { _stridusuario = value; }
        }

        public Int32 IdUsuarioAutoriza
        {
            get { return _intidusuarioautoriza; }
            set { _intidusuarioautoriza = value; }
        }

        public Int16 IdMotivoConstitucion
        {
            get { return _intmotivo; }
            set { _intmotivo = value; }
        }

        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }

        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }

        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public Boolean Interno
        { get { return true; } }

        public Int16 IdBanco
        { get { return 0; } }

        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }

        public String DireccionCliente
        {
            get { return _strdireccioncliente; }
            set { _strdireccioncliente = value; }
        }

        public Int16 IdMovimientoComercial
        {
            get { return _intidmovimientocomercial; }
            set { _intidmovimientocomercial = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombredocumento; }
            set { _strnombredocumento = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public String NombreEmpresa
        {
            get { return _strNombreEmpresa; }
            set { _strNombreEmpresa = value; }
        }

        public String NombreUnidadNegocio
        {
            get { return _strNombreUnidadNegocio; }
            set { _strNombreUnidadNegocio = value; }
        }

        public String NombreCentroServicio
        {
            get { return _strNombreCentroServicio; }
            set { _strNombreCentroServicio = value; }
        }

        public Decimal TasaIGV
        {
            get { return _dectasaigv; }
            set { _dectasaigv = value; }
        }

        public Int16 DevolverCliente
        {
            get { return _intdevolvercliente; }
            set { _intdevolvercliente = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades

        #region ObtenerIngresoProvision

        public clsIngresoProvision ObtenerIngresoProvision(Int16 idanocomercial, Int16 idmescomercial, Int16 idcentroservicio)
        {
            clsIngresoProvision _objingresoprovision = new clsIngresoProvision();

            _objingresoprovision.Sel = true;
            _objingresoprovision.FechaEmision = this.FechaRegistro;
            _objingresoprovision.FechaRegistro = this.FechaRegistro;
            _objingresoprovision.FechaVencimiento = this.FechaRegistro;

            _objingresoprovision.IdEstadoDeuda = (Int16)Estado.NoPagado;
            _objingresoprovision.IdTipoDocumento = this.IdTipoDocumentoAFavor;
            _objingresoprovision.NroMesesDeuda = 0;

            _objingresoprovision.IdMonedaOrigen = this.IdMonedaOrigen;
            _objingresoprovision.IdTipoAtencion = this.IdTipoAtencion;
            _objingresoprovision.IdPuntoAtencion = this.IdPuntoAtencion;

            _objingresoprovision.IdTipoIdentidad = this.IdTipoIdentidad;
            _objingresoprovision.IdUsuario = this.IdUsuario;
            _objingresoprovision.ImporteOrigenTotal = this.Importe;
            _objingresoprovision.TipoCambio = this.TipoCambio;
            _objingresoprovision.NroIdentidad = this.NroIdentidad;
            _objingresoprovision.ImporteOrigenSaldo = this.Importe;
            _objingresoprovision.ImporteOrigenCobrar = this.Importe;

            _objingresoprovision.NroDocumento = this.NroDocumentoAFavor;
            String _strnrodocumento = this.NroDocumentoAFavor + "   ";
            _objingresoprovision.Serie = _strnrodocumento.Substring(0, 3).Trim();
            _objingresoprovision.IdServicioPrincipal = this.IdServicio;
            _objingresoprovision.IdNroServicioPrincipal = this.IdNroServicio;

            _objingresoprovision.IdAnoComercial = idanocomercial;
            _objingresoprovision.IdCentroServicio = idcentroservicio;
            _objingresoprovision.IdMesComercial = idmescomercial;
            _objingresoprovision.IdMovimientoComercial = this.IdMovimientoComercial;
            _objingresoprovision.LoteFacturacion = 0;
            _objingresoprovision.NombreTipoDocumento = this.NombreTipoDocumento;

            _objingresoprovision.IdEmpresa = this.IdEmpresa;

            clsIngresoProvisionDocumentos _objingresoprovisiondocumentos;
            // _objingresoprovisiondocumentos = new clsIngresoProvisionDocumentos(this.IdEmpresa, this.IdTipoDocumentoAFavor, this.NroDocumentoAFavor, _objingresoprovision.Serie, this.IdTipoDocumentoAFavor, this.NroDocumentoAFavor, this.IdMonedaOrigen, this.TipoCambio, this.Importe, this.Importe, this.Importe, (Int16)Estado.NoPagado, this.IdServicio, this.IdNroServicio, 0, 1, (Int16)Estado.Pendiente, 0, true);
            _objingresoprovisiondocumentos = new clsIngresoProvisionDocumentos(this.IdEmpresa, this.IdTipoDocumentoAFavor, this.NroDocumentoAFavor, _objingresoprovision.Serie, this.IdTipoDocumentoAFavor, this.NroDocumentoAFavor, this.IdMonedaOrigen, this.TipoCambio, this.Importe, this.Importe, this.Importe, (Int16)Estado.NoPagado, this.IdServicio, this.IdNroServicio, 0, 1, (Int16)Estado.Normal, 0, true);
            _objingresoprovision.ListaIngresoProvisionDocumentos.Add(_objingresoprovisiondocumentos);

            return _objingresoprovision;
        }

        #endregion ObtenerIngresoProvision
    }

    #endregion clsDocumentosAFavor

    #region clsIngresoProvisionDocumentos

    public class clsIngresoProvisionDocumentos
    {
        #region Campos

        private String _strserie;
        private String _strnrodocumentosustento;

        private Int16 _intidestadodeuda;
        private Int16 _intidservicio;
        private Int32 _intidnroservicio;
        private Decimal _decimpuesto;
        private Int16 _intindicadordeuda;
        private Int16 _intidestadodocumento;
        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private Int16 _intidtipodocumentosustento;
        private Int16 _intindicadorreclamo;
        private Boolean _boosel;

        private Decimal _decimportetotal;
        private Decimal _decmontodescargado;
        private Decimal _decsaldo;
        private Decimal _decsaldooriginal;
        private Int16 _intidmoneda;

        private Decimal _decimportetotalorigen;
        private Decimal _decimportedescargadoorigen;
        private Decimal _decimportesaldoorigen;
        private Decimal _dectipocambio;
        private Int16 _intidmonedaorigen;
        private Int16 _intidescobrable;

        //VAS - Para reclamos
        private DateTime? _fecLimitePago;

        private Int32? _intperiodopago;
        private DateTime? _datfechapago;
        private Int16 _intcodigoenlace;

        #endregion Campos

        #region Constructor

        public clsIngresoProvisionDocumentos(Int16 idempresa, Int16 idtipodocumento, String nrodocumento, String serie, Int16 idtipodocumentosustento, String nrodocumentosustento, Int16 idmoneda, Decimal tipocambio, Decimal importetotal, Decimal montodescargado, Decimal saldo, Int16 idestadodeuda, Int16 idservicio, Int32 idnroservicio, Decimal impuesto, Int16 indicadordeuda, Int16 idestadodocumento, Int16 indicadorreclamo, Boolean sel)
        {
            this.IdEmpresa = idempresa;
            this.IdTipoDocumento = idtipodocumento;
            this.NroDocumento = nrodocumento;
            this.Serie = serie;
            this.IdTipoDocumentoSustento = idtipodocumentosustento;
            this.NroDocumentoSustento = nrodocumentosustento;

            this.ImporteTotal = importetotal;
            this.ImporteTotalOrigen = importetotal;
            this.ImporteDescargadoOrigen = montodescargado;
            this.SaldoOriginal = saldo;
            this.Saldo = saldo;
            this.IdMoneda = idmoneda;
            this.TipoCambio = tipocambio;
            this.IdEstadoDeuda = idestadodeuda;
            this.IdServicio = idservicio;
            this.IdNroServicio = idnroservicio;
            this.Impuesto = impuesto;
            this.IndicadorDeuda = indicadordeuda;
            this.IdEstadoDocumento = idestadodocumento;
            this.IndicadorReclamo = indicadorreclamo;
            this.Sel = sel;
        }

        public clsIngresoProvisionDocumentos(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idempresa"))
            {
                this.IdEmpresa = (Int16)registro["idempresa"];
            }
            if (registro.Table.Columns.Contains("idtipodocumento"))
            {
                this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            }
            if (registro.Table.Columns.Contains("nrodocumento"))
            {
                this.NroDocumento = registro["nrodocumento"].ToString();
            }
            if (registro.Table.Columns.Contains("serie"))
            {
                this.Serie = registro["serie"].ToString();
            }
            if (registro.Table.Columns.Contains("idtipodocumentosustento"))
            {
                this.IdTipoDocumentoSustento = (Int16)registro["idtipodocumentosustento"];
            }
            if (registro.Table.Columns.Contains("nrodocumentosustento"))
            {
                this.NroDocumentoSustento = registro["nrodocumentosustento"].ToString();
            }
            if (registro.Table.Columns.Contains("estadodeuda"))
            {
                this.IdEstadoDeuda = (Int16)registro["estadodeuda"];
            }
            if (registro.Table.Columns.Contains("idservicio"))
            {
                this.IdServicio = (Int16)registro["idservicio"];
            }
            if (registro.Table.Columns.Contains("idnroservicio"))
            {
                this.IdNroServicio = (Int32)registro["idnroservicio"];
            }
            if (registro.Table.Columns.Contains("impuesto"))
            {
                this.Impuesto = (Decimal)registro["impuesto"];
            }
            if (registro.Table.Columns.Contains("indicadordeuda"))
            {
                this.IndicadorDeuda = (Int16)registro["indicadordeuda"];
            }
            if (registro.Table.Columns.Contains("estadodocumento"))
            {
                this.IdEstadoDocumento = (Int16)registro["estadodocumento"];
            }
            if (registro.Table.Columns.Contains("indicadorreclamo"))
            {
                this.IndicadorReclamo = (Int16)registro["indicadorreclamo"];
            }
            if (registro.Table.Columns.Contains("Sel"))
            {
                this.Sel = (Boolean)registro["Sel"];
            }
            if (registro.Table.Columns.Contains("FechaLimitePago"))
            {
                this.FechaLimitePago = registro["FechaLimitePago"] == DBNull.Value ? null : (DateTime?)registro["FechaLimitePago"];
            }
            if (registro.Table.Columns.Contains("importetotal"))
            {
                this.ImporteTotal = (Decimal)registro["importetotal"];
            }
            if (registro.Table.Columns.Contains("montodescargado"))
            {
                this.MontoDescargado = (Decimal)registro["montodescargado"];
            }
            if (registro.Table.Columns.Contains("saldo"))
            {
                this.SaldoOriginal = (Decimal)registro["saldo"];
                this.Saldo = (Decimal)registro["saldo"];
            }

            if (registro.Table.Columns.Contains("idmoneda"))
            { this.IdMoneda = Convert.ToInt16(registro["idmoneda"]); }

            if (registro.Table.Columns.Contains("importetotalorigen"))
            { this.ImporteTotalOrigen = Convert.ToDecimal(registro["importetotalorigen"]); }
            if (registro.Table.Columns.Contains("importedescargadoorigen"))
            { this.ImporteDescargadoOrigen = Convert.ToDecimal(registro["importedescargadoorigen"]); }
            if (registro.Table.Columns.Contains("importesaldoorigen"))
            { this.ImporteSaldoOrigen = Convert.ToDecimal(registro["importesaldoorigen"]); }
            if (registro.Table.Columns.Contains("idmonedaorigen"))
            { this.IdMonedaOrigen = Convert.ToInt16(registro["idmonedaorigen"]); }
            if (registro.Table.Columns.Contains("tipocambio"))
            { this.TipoCambio = Convert.ToDecimal(registro["tipocambio"]); }
            if (registro.Table.Columns.Contains("periodopago"))
            { this.PeriodoPago = Convert.IsDBNull(registro["periodopago"]) ? (Int32?)null : Convert.ToInt32(registro["periodopago"]); }
            if (registro.Table.Columns.Contains("fechapago"))
            { this.FechaPago = Convert.IsDBNull(registro["fechapago"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechapago"]); }
        }

        public clsIngresoProvisionDocumentos()
        {
        }

        #endregion Constructor

        #region Propiedades - Generales

        public String Serie
        {
            get { return _strserie; }
            set { _strserie = value; }
        }

        public String NroDocumentoSustento
        {
            get { return _strnrodocumentosustento; }
            set { _strnrodocumentosustento = value; }
        }

        public Int16 IdEstadoDeuda
        {
            get { return _intidestadodeuda; }
            set { _intidestadodeuda = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public Decimal Impuesto
        {
            get { return _decimpuesto; }
            set { _decimpuesto = value; }
        }

        public Int16 IndicadorDeuda
        {
            get { return _intindicadordeuda; }
            set { _intindicadordeuda = value; }
        }

        public Int16 IdEstadoDocumento
        {
            get { return _intidestadodocumento; }
            set { _intidestadodocumento = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        public Int16 IdTipoDocumentoSustento
        {
            get { return _intidtipodocumentosustento; }
            set { _intidtipodocumentosustento = value; }
        }

        public Int16 IndicadorReclamo
        {
            get { return _intindicadorreclamo; }
            set { _intindicadorreclamo = value; }
        }

        public Boolean Sel
        {
            get { return _boosel; }
            set { _boosel = value; }
        }

        //VAS - Para reclamos
        public DateTime? FechaLimitePago
        {
            get { return _fecLimitePago; }
            set { _fecLimitePago = value; }
        }

        public Int32? PeriodoPago
        {
            get { return _intperiodopago; }
            set { _intperiodopago = value; }
        }

        public DateTime? FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades - Generales

        #region Propiedades - Importes al TipoCambio MonedaNacional

        public Decimal ImporteTotal
        {
            get { return _decimportetotal; }
            set { _decimportetotal = value; }
        }

        public Decimal MontoDescargado
        {
            get { return _decmontodescargado; }
            set { _decmontodescargado = value; }
        }

        public Decimal Saldo
        {
            get { return _decsaldo; }
            set
            {
                //  EGM. 26/08/2009. Se modifico el IF para los casos como del Documento Rendondeo (101)
                //  que tiene importe negativo cuando se quiere realizar Pago Parcial.
                //  Por defecto Saldo toma valor 0(cero) y siempre saldra error al comparar con estos
                //  tipos de documento
                //if (value > SaldoOriginal && SaldoOriginal != 0)
                if (value > SaldoOriginal && SaldoOriginal > 0)
                {
                    StringBuilder _sbmensaje = new StringBuilder();
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    _sbmensaje.Append("Saldo a Cobrar ");
                    _sbmensaje.Append(value.ToString());
                    _sbmensaje.Append(" no puede ser mayor a Saldo Original de ");
                    _sbmensaje.Append(SaldoOriginal.ToString());
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    throw new Exception(_sbmensaje.ToString());
                }

                _decsaldo = value;
            }
        }

        public Decimal SaldoOriginal
        {
            get { return _decsaldooriginal; }
            set { _decsaldooriginal = value; }
        }

        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        #endregion Propiedades - Importes al TipoCambio MonedaNacional

        #region Propiedades - Importes Origen - creadas por necesidad de cobro de diferentes monedas.

        public Decimal ImporteTotalOrigen
        {
            get { return _decimportetotalorigen; }
            set { _decimportetotalorigen = value; }
        }

        public Decimal ImporteDescargadoOrigen
        {
            get { return _decimportedescargadoorigen; }
            set { _decimportedescargadoorigen = value; }
        }

        public Decimal ImporteSaldoOrigen
        {
            get { return _decimportesaldoorigen; }
            set { _decimportesaldoorigen = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        #endregion Propiedades - Importes Origen - creadas por necesidad de cobro de diferentes monedas.

        #region

        public clsDocumentosDescargoPropio ObtenerDocumentoDescargo(Boolean estercero, DateTime fechaemision, Int16 idtipoidentidad, String nroidentidad, Int16 ordenpago)
        {
            clsDocumentosDescargoPropio documentodescargopropio = new clsDocumentosDescargoPropio();
            documentodescargopropio.EsTerceros = false;
            documentodescargopropio.FechaEmision = fechaemision;
            documentodescargopropio.IdMonedaCambio = this.IdMoneda;
            documentodescargopropio.IdMonedaOrigen = this.IdMonedaOrigen;
            documentodescargopropio.IdNroServicio = this.IdNroServicio;
            documentodescargopropio.IdServicio = this.IdServicio;
            documentodescargopropio.IdTipoDocumento = this.IdTipoDocumento;
            documentodescargopropio.IdTipoDocumentoSustento = this.IdTipoDocumentoSustento;
            documentodescargopropio.IdTipoIdentidad = idtipoidentidad;
            documentodescargopropio.IdTransaccionLog = 0;
            documentodescargopropio.ImporteCambioCobrar = this.TipoCambio * this.ImporteTotalOrigen;
            documentodescargopropio.ImporteOrigenDescargo = 0;
            documentodescargopropio.ImporteOrigenDeuda = this.ImporteTotalOrigen;
            documentodescargopropio.ImporteOrigenCobrar = this.ImporteTotalOrigen;
            documentodescargopropio.NroDocumento = this.NroDocumento;
            documentodescargopropio.NroDocumentoSustento = this.NroDocumentoSustento;
            documentodescargopropio.NroIdentidad = nroidentidad;
            documentodescargopropio.OrdenPagoServicio = ordenpago;
            documentodescargopropio.PermitePagoParcial = false;
            documentodescargopropio.TipoCambio = this.TipoCambio;

            return documentodescargopropio;
        }

        #endregion clsIngresoProvisionDocumentos
    }

    #endregion NGC

    #region clsListaDocumentosAFavor

    public class clsListaDocumentosAFavor
    {
        private List<clsDocumentosAFavor> _objlistadocumentosafavor = new List<clsDocumentosAFavor>();

        public clsListaDocumentosAFavor()
        { }

        public clsListaDocumentosAFavor(DataTable entidad)
        {
            if (entidad == null) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsDocumentosAFavor(_drw));
            }
        }

        public List<clsDocumentosAFavor> Elementos
        {
            get { return _objlistadocumentosafavor; }
            set { _objlistadocumentosafavor = value; }
        }

        #region Metodos

        public clsListaIngresoProvision ObtenerListaIngresoProvision(Int16 idanocomercial, Int16 idmescomercial, Int16 idcentroservicio)
        {
            clsListaIngresoProvision _objlistaingresoprovision = new clsListaIngresoProvision();

            foreach (clsDocumentosAFavor item in Elementos)
            {
                if (item.Importe == 0) { continue; }
                _objlistaingresoprovision.Elementos.Add(item.ObtenerIngresoProvision(idanocomercial, idmescomercial, idcentroservicio));
            }
            return _objlistaingresoprovision;
        }

        #endregion Metodos
    }

    #endregion clsListaDocumentosAFavor

    #region clsEncargo_Terceros

    public class clsEncargo_Terceros : clsIngresoProvisionBase
    {
        #region Campos

        private DateTime _datfechacarga;
        private String _stridtag;
        private Decimal _decimporteservicios;
        private Decimal _decimporterecargos;
        private Decimal _decimporteimpuestos;
        private Decimal _decimportetotal;
        private String _strnroservicio;
        private Int16 _intidtipodocumentoip;
        private String _strnrodocumentoip;
        private clsIngresoProvision _objingresoprovision = new clsIngresoProvision();

        #endregion Campos

        #region Constructor

        public clsEncargo_Terceros(DataRow registro, clsIngresoProvision ingresoprovision)
        {
            AsignarDatos(registro);
            this.IdEmpresa = ingresoprovision.IdEmpresa;
            this.IngresoProvision = ingresoprovision;
        }

        public clsEncargo_Terceros(DataRow registro)
        {
            AsignarDatos(registro);
        }

        private void AsignarDatos(DataRow registro)
        {
            _booesterceros = true;
            this.Sel = (Boolean)registro["sel"];
            this.IdNroServicioPrincipal = (Int32)registro["idnroservicio"];
            this.IdServicioPrincipal = (Int16)registro["idservicio"];
            this.IdTipoDocumento = (Int16)registro["idtiporecibo"];
            this.NroDocumento = registro["nrorecibo"].ToString();
            this.IdTipoDocumentoIP = (Int16)registro["IdTipoDocumento"];
            this.NroDocumentoIP = registro["NroDocumento"].ToString();
            //this.IdUsuario = (Int32)registro["idusuario"];
            this.FechaEmision = (DateTime)registro["fechaemision"];
            this.FechaVencimiento = (DateTime)registro["fechavencimiento"];
            this.FechaCarga = (DateTime)registro["fechacarga"];
            this.IdTag = registro["idtag"].ToString();
            this.NombreCliente = registro["nombreserviciotercero"].ToString();
            if (registro.Table.Columns.Contains("Direccion"))
            { this.Direccion = registro["Direccion"].ToString(); }
            this.IdEstadoDeuda = (Int16)registro["estado"];
            this.NroServicio = registro["nroservicio"].ToString();
            this.NroServicioTercero = registro["nroservicio"].ToString();
            this.IdTipoDocumentoIP = (Int16)registro["idtipodocumento"];
            this.NroDocumentoIP = registro["nrodocumento"].ToString();

            this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            this.ImporteServicios = (Decimal)registro["importeservicios"];
            this.ImporteRecargos = (Decimal)registro["importerecargos"];
            this.ImporteImpuestos = (Decimal)registro["importeimpuestos"];
            this.ImporteOrigenTotal = (Decimal)registro["importetotal"];
            if (registro.Table.Columns.Contains("TipoCambio"))
            { this.TipoCambio = (Decimal)registro["tipocambio"]; }

            if ((Estado)this.IdEstadoDeuda == Estado.Pagado)
            {
                this.ImporteOrigenDescargo = (Decimal)registro["importetotal"];
            }
            else
            {
                this.ImporteOrigenSaldo = (Decimal)registro["importetotal"];
                this.ImporteOrigenCobrar = (Decimal)registro["importetotal"];
            }

            if (registro.Table.Columns.Contains("idanocomercial"))
                this.IdAnoComercial = Convert.ToInt16(registro["idanocomercial"]);
            if (registro.Table.Columns.Contains("idmescomercial"))
                this.IdMesComercial = Convert.ToInt16(registro["idmescomercial"]);
            if (this.IdAnoComercial > 0 && this.IdMesComercial > 0)
                this.Periodo = (IdAnoComercial * 100) + IdMesComercial;
        }

        public clsEncargo_Terceros()
        {
        }

        #endregion Constructor

        #region Propiedades

        public DateTime FechaCarga
        {
            get { return _datfechacarga; }
            set { _datfechacarga = value; }
        }

        public String IdTag
        {
            get { return _stridtag; }
            set { _stridtag = value; }
        }

        public Decimal ImporteServicios
        {
            get { return _decimporteservicios; }
            set { _decimporteservicios = value; }
        }

        public Decimal ImporteRecargos
        {
            get { return _decimporterecargos; }
            set { _decimporterecargos = value; }
        }

        public Decimal ImporteImpuestos
        {
            get { return _decimporteimpuestos; }
            set { _decimporteimpuestos = value; }
        }

        public String NroServicio
        {
            get { return _strnroservicio; }
            set { _strnroservicio = value; }
        }

        public Int16 IdTipoDocumentoIP
        {
            get { return _intidtipodocumentoip; }
            set { _intidtipodocumentoip = value; }
        }

        public String NroDocumentoIP
        {
            get { return _strnrodocumentoip; }
            set { _strnrodocumentoip = value; }
        }

        public clsIngresoProvision IngresoProvision
        {
            get { return _objingresoprovision; }
            set { _objingresoprovision = value; }
        }

        #endregion Propiedades

        #region Propiedades Redefinidas por Herencia

        public Int16 IdTipoIdentidad
        {
            get
            {
                return IngresoProvision.IdTipoIdentidad;
            }
            set
            {
                IngresoProvision.IdTipoIdentidad = value;
            }
        }

        public String NroIdentidad
        {
            get
            {
                return IngresoProvision.NroIdentidad;
            }
            set
            {
                IngresoProvision.NroIdentidad = value;
            }
        }

        public override Int32 NroItems
        {
            get { return 1; }
        }

        #endregion Propiedades Redefinidas por Herencia
    }

    #endregion clsEncargo_Terceros

    #region clsListaIngresoProvision

    public class clsListaIngresoProvision
    {
        private List<clsIngresoProvisionBase> _lstingresoprovision = new List<clsIngresoProvisionBase>();

        #region Constructor

        public clsListaIngresoProvision(DataTable ingresoprovision)
        {
            if (ingresoprovision == null || ingresoprovision.Rows.Count == 0) { return; }

            foreach (DataRow fila in ingresoprovision.Rows)
            {
                Elementos.Add(new clsIngresoProvision(fila));
            }
        }

        public clsListaIngresoProvision(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos)
        {
            if (ingresoprovision == null || ingresoprovision.Rows.Count == 0) { return; }

            AsignarIngresoProvision(ingresoprovision, ingresoprovisiondocumentos);
        }

        public clsListaIngresoProvision(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos, DataTable encargoterceros)
        {
            if (ingresoprovision == null || ingresoprovision.Rows.Count == 0) { return; }
            Boolean _booesterceros = (encargoterceros != null);

            if (_booesterceros)
            { AsignarEncargoTerceros(ingresoprovision, ingresoprovisiondocumentos, encargoterceros); }
            else
            { AsignarIngresoProvision(ingresoprovision, ingresoprovisiondocumentos); }
        }

        public clsListaIngresoProvision()
        { }

        #endregion Constructor

        #region Propiedades

        public List<clsIngresoProvisionBase> Elementos
        {
            get { return _lstingresoprovision; }
            set { _lstingresoprovision = value; }
        }

        #endregion Propiedades

        #region Metodos

        public System.ComponentModel.BindingList<clsDocumentosDescargoBase> ObtenerListaDocumentosDescargo(Int16 ordenpago)
        {
            System.ComponentModel.BindingList<clsDocumentosDescargoBase> _objelementosdocumentosdescargo = new System.ComponentModel.BindingList<clsDocumentosDescargoBase>();
            foreach (clsIngresoProvisionBase item in Elementos)
            {
                clsIngresoProvision _objingresoprovision = null;
                if (item.EsTerceros)
                {
                    clsEncargo_Terceros itemterceros = (clsEncargo_Terceros)item;
                    _objingresoprovision = itemterceros.IngresoProvision;
                }
                else
                {
                    _objingresoprovision = (clsIngresoProvision)item;
                }

                foreach (clsIngresoProvisionDocumentos itemdocumentos in _objingresoprovision.ListaIngresoProvisionDocumentos)
                {
                    ordenpago += 1;
                    _objelementosdocumentosdescargo.Add(itemdocumentos.ObtenerDocumentoDescargo(false, _objingresoprovision.FechaEmision, _objingresoprovision.IdTipoIdentidad, _objingresoprovision.NroIdentidad, ordenpago));
                }
            }
            return _objelementosdocumentosdescargo;
        }

        private void AsignarIngresoProvision(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos)
        {
            String _strfiltro;
            foreach (DataRow drcabecera in ingresoprovision.Rows)
            {
                DataRow[] _lstrowdetalle, _lstrowteceros;
                _lstrowteceros = null;

                _strfiltro = "IdTipoDocumento = " + drcabecera["IdTipoDocumento"].ToString();
                _strfiltro = _strfiltro + " AND NroDocumento = '" + drcabecera["NroDocumento"].ToString() + "'";
                _lstrowdetalle = ingresoprovisiondocumentos.Select(_strfiltro.ToString());
                clsIngresoProvision _objingresoprovision = new clsIngresoProvision(drcabecera, _lstrowdetalle);
                Elementos.Add(_objingresoprovision);//, _lstrowteceros));
            }
        }

        private void AsignarEncargoTerceros(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos, DataTable encargoterceros)
        {
            String _strfiltro;
            foreach (DataRow drterceros in encargoterceros.Rows)
            {
                DataRow[] _lstrowdetalle, _lstrowcabecera;
                _strfiltro = "IdTipoDocumento = " + drterceros["IdTipoDocumento"].ToString();
                _strfiltro = _strfiltro + " AND NroDocumento = '" + drterceros["NroDocumento"].ToString() + "'";
                _lstrowcabecera = ingresoprovision.Select(_strfiltro.ToString());
                _lstrowdetalle = ingresoprovisiondocumentos.Select(_strfiltro.ToString());
                clsIngresoProvision _objingresoprovision = new clsIngresoProvision(_lstrowcabecera[0], _lstrowdetalle);
                Elementos.Add(new clsEncargo_Terceros(drterceros, _objingresoprovision));
            }
        }

        #endregion Metodos

        #region MétodosBusqueda

        private Predicate<clsIngresoProvisionBase> PredicadoBuscar;
        private Int16 _intidtipodocumentotmp;
        private String _strnrodocumentotmp;
        private Int32 _intidserviciotmp;
        private Int32 _intidnroserviciotmp;
        private DateTime _datfechaemisiontmp;
        private Boolean _boolseleccionadostmp;

        public clsIngresoProvisionBase BuscarPorDocumentoSinNumero(Int16 idtipodocumento, Int32 idnroservicio)
        {
            _strnrodocumentotmp = "";
            _intidtipodocumentotmp = idtipodocumento;
            _intidnroserviciotmp = idnroservicio;
            clsIngresoProvisionBase _objIngresoprovision;
            PredicadoBuscar = new Predicate<clsIngresoProvisionBase>(BuscarDocumentoNroServicio);
            _objIngresoprovision = Elementos.Find(PredicadoBuscar);
            return _objIngresoprovision;
        }

        private Boolean BuscarDocumentoNroServicio(clsIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.IdTipoDocumento == _intidtipodocumentotmp && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp && ingresoprovision.NroDocumento.Trim() == _strnrodocumentotmp);
        }

        public List<clsIngresoProvisionBase> BuscarPorNroServicio(Int16 idservicio, Int32 idnroservicio)
        {
            _intidserviciotmp = idservicio;
            _intidnroserviciotmp = idnroservicio;
            List<clsIngresoProvisionBase> _objlistaingresoprovision;
            PredicadoBuscar = new Predicate<clsIngresoProvisionBase>(BuscarNroServicio);
            _objlistaingresoprovision = Elementos.FindAll(PredicadoBuscar);
            return _objlistaingresoprovision;
        }

        private Boolean BuscarNroServicio(clsIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.IdServicioPrincipal == _intidserviciotmp && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp);
        }

        public Boolean EsMasRecienteOrMasAntiguo(Int16 idservicio, Int32 idnroservicio, DateTime fechaemision, Boolean EsMasAntiguo)
        {
            _intidserviciotmp = idservicio;
            _intidnroserviciotmp = idnroservicio;
            _datfechaemisiontmp = fechaemision;
            _boolseleccionadostmp = EsMasAntiguo;
            if (EsMasAntiguo)
            { PredicadoBuscar = new Predicate<clsIngresoProvisionBase>(BuscarMasAntiguo); }
            else
            { PredicadoBuscar = new Predicate<clsIngresoProvisionBase>(BuscarMasReciente); }
            return Elementos.Exists(PredicadoBuscar);
        }

        private Boolean BuscarMasReciente(clsIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.Sel == !_boolseleccionadostmp
                  && ingresoprovision.IdServicioPrincipal == _intidserviciotmp
                  && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp
                  && ingresoprovision.FechaEmision.CompareTo(_datfechaemisiontmp) > 0 //es mayor
                   );
        }

        private Boolean BuscarMasAntiguo(clsIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.Sel == !_boolseleccionadostmp
                  && ingresoprovision.IdServicioPrincipal == _intidserviciotmp
                  && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp
                  && ingresoprovision.FechaEmision.CompareTo(_datfechaemisiontmp) < 0 //es menor
                   );
        }

        public clsListaIngresoProvision ObtenerListaPropios()
        {
            clsListaIngresoProvision _objlistapropios = new clsListaIngresoProvision();
            PredicadoBuscar = new Predicate<clsIngresoProvisionBase>(BuscarPropios);
            _objlistapropios.Elementos.AddRange(Elementos.FindAll(PredicadoBuscar));

            return _objlistapropios;
        }

        public List<Int32> ListaSuministros()
        {
            List<Int32> objListaSuministroTemp = new List<Int32>();

            Int32 intIdNroServicio = Elementos[0].IdNroServicioPrincipal;

            foreach (clsIngresoProvisionBase objItem in Elementos)
            {
                if (!objListaSuministroTemp.Contains(objItem.IdNroServicioPrincipal))
                    objListaSuministroTemp.Add(objItem.IdNroServicioPrincipal);
            }

            return objListaSuministroTemp;
        }

        public Boolean BuscarPropios(clsIngresoProvisionBase ingresoprovision)
        {
            return (!ingresoprovision.EsTerceros);
        }

        public Int32 CantidadServicios()
        {
            Int32 intResultado = 0;

            if (Elementos != null && Elementos.Count > 0)
            {
                Int32 intIdNroServicioTemp = Elementos[0].IdNroServicioPrincipal;
                Int32 intContador = 1;

                foreach (clsIngresoProvisionBase objItem in Elementos)
                {
                    if (objItem.IdNroServicioPrincipal != intIdNroServicioTemp)
                    {
                        intIdNroServicioTemp = objItem.IdNroServicioPrincipal;
                        intContador += 1;
                    }
                }

                intResultado = intContador;
            }

            return intResultado;
        }

        #endregion MétodosBusqueda
    }

    #endregion clsListaIngresoProvision

    #region clsAsientoDocumentoValida

    public class clsAsientoDocumentoValida
    {
        #region Campos
        private Int16 _intiduunn;
        private String _strcentroservicio;
        private String _strprocesoasiento;
        private DateTime? _datfecharegistro;
        private DateTime? _datfechaprocesodesde;
        private DateTime? _datfechaprocesohasta;
        private Int32? _intperiodo;
        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String CentroServicio
        {
            get { return _strcentroservicio; }
            set { _strcentroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ProcesoAsiento
        {
            get { return _strprocesoasiento; }
            set { _strprocesoasiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaProcesoDesde
        {
            get { return _datfechaprocesodesde; }
            set { _datfechaprocesodesde = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaProcesoHasta
        {
            get { return _datfechaprocesohasta; }
            set { _datfechaprocesohasta = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsAsientoDocumentoValida()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsAsientoDocumentoValida(DataRow dr)
        {
            if (dr == null) { return; }

            IdUUNN = Convert.ToInt16(dr["iduunn"]);
            CentroServicio = dr["centroservicio"].ToString();
            ProcesoAsiento = dr["procesoasiento"].ToString();
            FechaRegistro = Convert.IsDBNull(dr["fecharegistro"]) ? (DateTime?)null : Convert.ToDateTime(dr["fecharegistro"]);
            FechaProcesoDesde = Convert.IsDBNull(dr["fechaprocesodesde"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechaprocesodesde"]);
            FechaProcesoHasta = Convert.IsDBNull(dr["fechaprocesohasta"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechaprocesohasta"]);
            Periodo = Convert.IsDBNull(dr["periodo"]) ? (Int32?)null : Convert.ToInt32(dr["periodo"]);
        }

        #endregion Constructor
    }

    #endregion clsAsientoDocumentoValida

    #region clsListaAsientoDocumentoValida

    public class clsListaAsientoDocumentoValida
    {
        #region Campos
        private List<clsAsientoDocumentoValida> _objelementos = new List<clsAsientoDocumentoValida>();
        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsAsientoDocumentoValida> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaAsientoDocumentoValida()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaAsientoDocumentoValida(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsAsientoDocumentoValida(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion clsListaAsientoDocumentoValida

    #region IIngresoProvision

    public interface IIngresoProvision
    {
        Boolean Sel { get; set; }
        Int32 IdNroServicioPrincipal { get; set; }
        Int16 IdServicioPrincipal { get; set; }
        Int16 IdMonedaOrigen { get; set; }
        Int16 IdTipoDocumento { get; set; }
        Int16 IdTipoIdentidad { get; set; }
        String NroDocumento { get; set; }
        String NroIdentidad { get; set; }
        String NombreCliente { get; set; }

        Decimal ImporteOrigenTotal { get; set; }
        Decimal ImporteOrigenSaldo { get; set; }
        Decimal ImporteOrigenCobrar { get; set; }
        Decimal ImporteOrigenDescargo { get; set; }

        Decimal ImporteCambioTotal { get; set; }
        Decimal ImporteCambioSaldo { get; set; }
        Decimal ImporteCambioCobrar { get; set; }
        Decimal ImporteCambioDescargo { get; set; }

        DateTime FechaVencimiento { get; set; }
        DateTime FechaEmision { get; set; }
    }

    #endregion IIngresoProvision

    #region clsIngresoProvisionBase

    public abstract class clsIngresoProvisionBase : IIngresoProvision
    {
        #region Campos

        protected Boolean _boosel;
        private Int16 _intidservicioprincipal;
        private Int32 _intidnroservicioprincipal;
        private String _strnroserviciotercero;
        protected Int16 _intidempresa;
        protected Int16 _intidtipodocumento;
        protected String _strnrodocumento;
        private DateTime _datfecharegistro;
        private DateTime _datfechaemision;
        private DateTime _datfechavencimiento;
        private Decimal _decimportetotalorigen;
        private Decimal _decimportesaldoorigen;
        private Int16 _intidmonedaorigen;
        private Int16 _intidmoneda;
        private Int16 _intidestadodeuda;
        protected Decimal _decimportecobrar;
        private String _strnombrecliente;
        private String _strdireccion;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private EstadoRegistro _enuestadoregistro;
        protected Boolean _booesterceros;
        private String _strnombretipodocumento;
        private Boolean _booexcluirvalidacioninterna;
        private Decimal _decsaldo;
        private Boolean _booaplicarenipd;
        private Decimal _dectipocambio;
        private Int32 _intidusuario;
        private Int32 _intperiodo;
        private Int16 _intidanocomercial;
        private Int16 _intidmescomercial;
        private Int16 _intestadosuministro;
        private Int16 _intidccss;
        private Int16 _intiduunn;

        private Decimal _decimportecambiototal;
        private Decimal _decimportecambiodescargo;
        private Decimal _decimportecambiocobrar;
        private Decimal _decimporteorigendescargo;

        private Boolean _boopermitepagoparcial;
        private Boolean _intindicadorreclamo = false;
        private Int32 _intordenpagodocumento;
        private Int32 _intordenpagoservicio;
        private Boolean _bolindicadordeuda = false;

        //Se agregó el campo para poder generar el convenio asociado a la solicitud.

        //Se agrega campo pra efecto de leer el campo ImporteNoReclamado de la tabla IP para los casos
        //en que se registra un reclamo y el usuario indica lo que solia pagar, requerimiento solicitado por Jorge Vargas
        //10/06/2014
        private Decimal _decimportepaga;

        #endregion Campos

        #region Propiedades - Generales

        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }

        public String NroServicioTercero
        {
            get { return _strnroserviciotercero; }
            set { _strnroserviciotercero = value; }
        }

        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }

        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }

        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        public DateTime FechaEmision
        {
            get { return _datfechaemision; }
            set { _datfechaemision = value; }
        }

        public Int16 IdCCSS
        {
            get { return _intidccss; }
            set { _intidccss = value; }
        }

        public Int16 IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        public DateTime FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        public Int16 IdEstadoDeuda
        {
            get { return _intidestadodeuda; }
            set { _intidestadodeuda = value; }
        }

        public Int16 IdServicioPrincipal
        {
            get { return _intidservicioprincipal; }
            set { _intidservicioprincipal = value; }
        }

        public Int32 IdNroServicioPrincipal
        {
            get { return _intidnroservicioprincipal; }
            set { _intidnroservicioprincipal = value; }
        }

        public Boolean EsTerceros
        {
            get { return _booesterceros; }
            set { _booesterceros = value; }
        }

        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        public Int16 EstadoSuministro
        {
            get { return _intestadosuministro; }
            set { _intestadosuministro = value; }
        }

        public Int16 IdAnoComercial
        {
            get { return _intidanocomercial; }
            set { _intidanocomercial = value; }
        }

        public Int16 IdMesComercial
        {
            get { return _intidmescomercial; }
            set { _intidmescomercial = value; }
        }

        public Boolean Sel
        {
            get { return _boosel; }
            set { _boosel = value; }
            //{
            //    if ((Estado)IdEstadoDeuda == Estado.Pagado) { _boosel = false; return; }

            //    if ((!value) || (TipoCambio > 0 && ImporteCambioSaldo > 0))
            //    { _boosel = value; return; }

            //    if (_booexcluirvalidacioninterna || IdMonedaOrigen == 0) { _boosel = value; return; }

            //    _boosel = false;
            //    StringBuilder _sbmensaje = new StringBuilder();

            //    if (TipoCambio == 0)
            //    {
            //        _sbmensaje.AppendLine("");
            //        _sbmensaje.AppendLine("");
            //        _sbmensaje.AppendLine("!Ingrese Tipo Cambio!");
            //    }

            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("No puede seleccionar documento = " + _intidtipodocumento.ToString() + " - " + _strnrodocumento);
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("Importe de Tipo Cambio  = " + TipoCambio.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Importe Saldo de Cambio = " + ImporteCambioSaldo.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Moneda de cambio        = " + IdMonedaCambio.ToString());
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("Importe Total Origen  = " + ImporteOrigenTotal.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Importe Saldo Origen  = " + ImporteOrigenSaldo.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Moneda Origen         = " + IdMonedaOrigen.ToString());
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("");

            //    throw new Exception(_sbmensaje.ToString());
            //}
        }

        public EstadoRegistro EstadoRegistro
        {
            get { return _enuestadoregistro; }
            set { _enuestadoregistro = value; }
        }

        public Boolean AplicarEnHijos
        {
            get { return _booaplicarenipd; }
            set { _booaplicarenipd = value; }
        }

        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        //Importes Origen
        public Decimal ImporteOrigenTotal
        {
            get { return _decimportetotalorigen; }
            set { _decimportetotalorigen = value; }
        }

        public Decimal ImporteOrigenSaldo
        {
            get { return _decimportesaldoorigen; }
            set { _decimportesaldoorigen = value; }
        }

        public virtual Decimal ImporteOrigenCobrar
        {
            get { return _decimportecobrar; }
            set { _decimportecobrar = value; }
        }

        public Decimal ImporteOrigenDescargo
        {
            get { return _decimporteorigendescargo; }
            set { _decimporteorigendescargo = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        //Importes Cambio
        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Decimal ImporteCambioTotal
        {
            get { return _decimportecambiototal; }
            set { _decimportecambiototal = value; }
        }

        public Decimal ImporteCambioSaldo
        {
            get { return _decsaldo; }
            set { _decsaldo = value; }
        }

        public Decimal ImporteCambioCobrar
        {
            get { return _decimportecambiocobrar; }
            set { _decimportecambiocobrar = value; }
        }

        public Decimal ImporteCambioDescargo
        {
            get { return _decimportecambiodescargo; }
            set { _decimportecambiodescargo = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public Boolean PermitePagoParcial
        {
            get { return _boopermitepagoparcial; }
            set { _boopermitepagoparcial = value; }
        }

        public Int32 OrdenPagoServicio
        {
            get { return _intordenpagoservicio; }
            set { _intordenpagoservicio = value; }
        }

        public Int32 OrdenPagoDocumento
        {
            get { return _intordenpagodocumento; }
            set { _intordenpagodocumento = value; }
        }

        public Boolean IndicadorReclamo
        {
            get { return _intindicadorreclamo; }
            set { _intindicadorreclamo = value; }
        }

        public abstract Int32 NroItems { get; }

        public Boolean IndicadorDeuda
        {
            get { return _bolindicadordeuda; }
            set { _bolindicadordeuda = value; }
        }

        //VAS
        private Decimal decImporteReclamo;

        public Decimal ImporteReclamo
        {
            get { return decImporteReclamo; }
            set { decImporteReclamo = value; }
        }

        private DateTime _decFechaServidor;

        //VAS - Definida para implementacion de reclamos
        public DateTime FechaServidor
        {
            get { return _decFechaServidor; }
            set { _decFechaServidor = value; }
        }

        public Decimal ImportePagoFijo
        {
            get { return _decimportepaga; }
            set { _decimportepaga = value; }
        }

        #endregion Propiedades - Generales
    }

    #endregion clsIngresoProvisionBase

    #region clsCajaEstadisticaDiario

    public class clsCajaEstadisticaDiario
    {
        #region Campos

        private clsUsuarioPuntoAtencion _objusuariopuntoatencion;
        private DateTime _datfechapago;
        private Int16 _intidempresacajero;
        private Int16 _intidunidadnegocio;
        private Int16 _intidcentroservicio;
        private Int16 _intidtipoatencion;
        private Int16 _intidpuntoatencion;
        private String _strnombrepuntoatencion;
        private Int32 _intnrotransacciones;
        private Int32 _intnroservicios;
        private Int32 _intnrodocumentos;
        private Int32 intServiciosPagados;
        private Decimal _decdineromn;
        private Decimal _decimporteventanillamn;
        private Decimal _decpendienteabovedamn;
        private Decimal _decimporteenbovedamn;
        private Decimal _decimportedocumentosmn;
        private Decimal _decimportedocumentosexternomn;
        private Decimal _decimportedocumentosinternomn;
        private Decimal _decimporteencustodiamn;
        private Decimal _decimporteenbancosmn;
        private Decimal _decimportecheque;
        private Int32 _intidnrocaja;
        private Int32 _intidUsuario;
        private DateTime _datfechaapertura;
        private DateTime? _datfechacierre;
        private Int16 _intidEstadoCobranza;
        private DateTime _datultimomovimiento;
        private Decimal _decmovefectivoingresomn;
        private Decimal _decmovefectivoegresomn;
        private Decimal _decmovdocumentoingresomn;
        private Decimal _decmovdocumentoegresomn;
        private Decimal _decrecchequeingresocalculadomn;
        private Decimal _decrecefectivoingresomn;
        private Decimal _decrecefectivoegresomn;
        private Decimal _decrecdocumentoingresomn;
        private Decimal _decrecdocumentoegresomn;

        //VAS Campos para el manejo de montos en cheque
        private Decimal _decrecchequeingresomn;

        private Decimal _decrecchequeegresomn;
        private Decimal _decmovchequeingresomn;
        private Decimal _decmovchequeegresomn;

        private Decimal _decTotalRecaudado;
        private String _strnombreusuario = "";
        private Int32 _intcantidadpendienteaboveda;
        private Int32 _idProveedorPersonal;
        private String _nombreProveedor;
        private String _nombreCortoProveedor;
        private String _nombreRefrendadora;

        private Int16 _intesintercativo;
        private String _strremitos;

        #endregion Campos

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        /// <param name="fechapago"></param>
        /// <param name="idempresa"></param>
        /// <param name="idunidadnegocio"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <param name="transaccionespagadas"></param>
        /// <param name="dineromn"></param>
        /// <param name="importeventanillamn"></param>
        /// <param name="importeenbovedamn"></param>
        /// <param name="importedocumentosmn"></param>
        /// <param name="idnrocaja"></param>
        /// <param name="idusuario"></param>
        /// <param name="fechaapertura"></param>
        /// <param name="fechacierre"></param>
        /// <param name="idestadocobranza"></param>
        /// <param name="ultimomovimiento"></param>
        /// <param name="movefectivoingresomn"></param>
        /// <param name="movefectivoegresomn"></param>
        /// <param name="movdocumentoingresomn"></param>
        /// <param name="movdocumentoegresomn"></param>
        /// <param name="recefectivoingresomn"></param>
        /// <param name="recefectivoegresomn"></param>
        /// <param name="recdocumentoingresomn"></param>
        /// <param name="recdocumentoegresomn"></param>
        public clsCajaEstadisticaDiario(DateTime fechapago
            , Int16 idempresa, Int16 idunidadnegocio, Int16 idcentroservicio, Int16 idtipoatencion
            , Int16 idpuntoatencion, Int32 transaccionespagadas, Decimal dineromn, Decimal importeventanillamn
            , Decimal importeenbovedamn, Decimal importedocumentosmn, Int32 idnrocaja, Int32 idusuario
            , DateTime fechaapertura, DateTime? fechacierre, Int16 idestadocobranza, DateTime ultimomovimiento
            , Decimal movefectivoingresomn, Decimal movefectivoegresomn, Decimal movdocumentoingresomn
            , Decimal movdocumentoegresomn, Decimal recefectivoingresomn, Decimal recefectivoegresomn
            , Decimal recdocumentoingresomn, Decimal recdocumentoegresomn)
        {
            this.UsuarioPuntoAtencion =
                new clsUsuarioPuntoAtencion(idusuario, idtipoatencion, idpuntoatencion, idcentroservicio
                                          , idunidadnegocio, idestadocobranza, 1, idnrocaja, idempresa);

            this.IdNroCaja = idnrocaja;
            this.FechaPago = fechapago;
            this.NroTransacciones = 0;
            this.NroServicios = 0;
            this.NroDocumentos = 0;
            this.DineroMN = dineromn;
            this.ImporteVentanillaMN = importeventanillamn;
            this.ImporteenbovedaMN = importeenbovedamn;
            this.ImporteDocumentosMN = importedocumentosmn;
            this.FechaApertura = fechaapertura;
            this.FechaCierre = fechacierre;
            this.UltimoMovimiento = ultimomovimiento;
            this.MovEfectivoIngresoMN = movefectivoingresomn;
            this.MovEfectivoEgresoMN = movefectivoegresomn;
            this.MovDocumentoIngresoMN = movdocumentoingresomn;
            this.MovDocumentoEgresoMN = movdocumentoegresomn;
            this.RecEfectivoIngresoMN = recefectivoingresomn;
            this.RecEfectivoEgresoMN = recefectivoegresomn;
            this.RecDocumentoIngresoMN = recdocumentoingresomn;
            this.RecDocumentoEgresoMN = recdocumentoegresomn;
            this.TotalRecaudado = RecEfectivoIngresoMN + MovEfectivoIngresoMN + MovDocumentoIngresoMN + RecDocumentoIngresoMN + ImporteenbovedaMN;
            this.IdTipoAtencion = idtipoatencion;
            this.IdPuntoAtencion = idpuntoatencion;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        public clsCajaEstadisticaDiario(DataRow registro)
        {
            if (registro == null) { return; }

            this.UsuarioPuntoAtencion = new clsUsuarioPuntoAtencion
                                        (Convert.ToInt32(registro["idusuario"])
                                        , Convert.ToInt16(registro["idtipoatencion"])
                                        , Convert.ToInt16(registro["idpuntoatencion"])
                                        , Convert.ToInt16(registro["idCentroServicioCajero"])
                                        , Convert.ToInt16(registro["idUUNNCajero"])
                                        , Convert.ToInt16(registro["idestadocobranza"])
                                        , 1
                                        , Convert.ToInt32(registro["idnrocaja"])
                                        , Convert.ToInt16(registro["idempresacajero"]));

            this.IdNroCaja = Convert.ToInt32(registro["idnrocaja"]);
            this.FechaPago = Convert.ToDateTime(registro["fechapago"]);
            this.UsuarioPuntoAtencion.FechaPago = Convert.ToDateTime(registro["fechapago"]);
            this.NroTransacciones = Convert.ToInt32(registro["nrotransacciones"]);
            this.NroServicios = Convert.ToInt32(registro["nroservicios"]);
            this.NroDocumentos = Convert.ToInt32(registro["nrodocumentos"]);

            this.DineroMN = Convert.ToDecimal(registro["dineromn"]);
            this.ImporteVentanillaMN = Convert.ToDecimal(registro["EnVentanillamn"]);
            if (registro.Table.Columns.Contains("PendienteaBovedaMN")) { this.PendienteabovedaMN = Convert.IsDBNull(registro["PendienteaBovedaMN"]) ? 0 : Convert.ToDecimal(registro["PendienteaBovedaMN"]); }
            if (registro.Table.Columns.Contains("ImporteenChequesMN")) { this.ImporteChequeMN = Convert.IsDBNull(registro["ImporteenChequesMN"]) ? 0 : Convert.ToDecimal(registro["ImporteenChequesMN"]); }
            if (registro.Table.Columns.Contains("EnCustodiaMN")) { this.EnCustodiaMN = Convert.IsDBNull(registro["EnCustodiaMN"]) ? 0 : Convert.ToDecimal(registro["EnCustodiaMN"]); }
            if (registro.Table.Columns.Contains("RemitidoMN")) { this.EnBancoMN = Convert.IsDBNull(registro["RemitidoMN"]) ? 0 : Convert.ToDecimal(registro["RemitidoMN"]); }
            this.ImporteenbovedaMN = Convert.ToDecimal(registro["ImporteEnbovedamn"]);
            this.ImporteDocumentosMN = Convert.ToDecimal(registro["EnDocumentosmn"]);
            if (registro.Table.Columns.Contains("importedocinterno"))
            {
                this.ImporteDocumentosInternoMN = Convert.IsDBNull(registro["importedocinterno"]) ? 0 : Convert.ToDecimal(registro["importedocinterno"]);
                this.ImporteDocumentosExternoMN = this.ImporteDocumentosMN - this.ImporteDocumentosInternoMN;
            }
            this.FechaApertura = Convert.ToDateTime(registro["fechaapertura"]);
            this.FechaCierre = registro.IsNull("fechacierre") ? (DateTime?)null : Convert.ToDateTime(registro["fechacierre"]);
            this.UltimoMovimiento = Convert.ToDateTime(registro["ultimomovimiento"]);

            this.MovEfectivoIngresoMN = Convert.ToDecimal(registro["MovEfectivoIngresoMN"]);
            this.MovEfectivoEgresoMN = Convert.ToDecimal(registro["MovEfectivoEgresoMN"]);
            this.MovDocumentoIngresoMN = Convert.ToDecimal(registro["MovDocumentoIngresoMN"]);
            this.MovDocumentoEgresoMN = Convert.ToDecimal(registro["MovDocumentoEgresoMN"]);

            //VAS Cheques..
            if (registro.Table.Columns.Contains("MovChequeIngresoMN")) { this.MovChequeIngresoMN = Convert.ToDecimal(registro["MovChequeIngresoMN"]); }
            if (registro.Table.Columns.Contains("MovChequeEgresoMN")) { this.MovChequeEgresoMN = Convert.ToDecimal(registro["MovChequeEgresoMN"]); }
            if (registro.Table.Columns.Contains("RecChequeIngresoMN")) { this.RecChequeIngresoMN = Convert.ToDecimal(registro["RecChequeIngresoMN"]); }
            if (registro.Table.Columns.Contains("RecChequeEgresoMN")) { this.RecChequeEgresoMN = Convert.ToDecimal(registro["RecChequeEgresoMN"]); }

            this.RecEfectivoIngresoMN = Convert.ToDecimal(registro["RecEfectivoIngresoMN"]);
            this.RecEfectivoEgresoMN = Convert.ToDecimal(registro["RecEfectivoEgresoMN"]);
            this.RecDocumentoIngresoMN = Convert.ToDecimal(registro["RecDocumentoIngresoMN"]);
            this.RecDocumentoEgresoMN = Convert.ToDecimal(registro["RecDocumentoEgresoMN"]);

            this.IdTipoAtencion = Convert.ToInt16(registro["idtipoatencion"]);
            this.IdPuntoAtencion = Convert.ToInt16(registro["idpuntoatencion"]);

            if (registro.Table.Columns.Contains("RecChequeIngresoCalculadoMN"))
            {
                this.RecChequeIngresoCalculadoMN = Convert.IsDBNull(registro["RecChequeIngresoMN"]) ? 0 : Convert.ToDecimal(registro["RecChequeIngresoMN"]);
            }

            this.TotalRecaudado = (this.RecChequeIngresoMN
                                    + this.RecEfectivoIngresoMN
                                    + this.RecDocumentoIngresoMN) -
                                    (this.RecChequeEgresoMN
                                    + this.RecDocumentoEgresoMN
                                    + this.RecEfectivoEgresoMN);

            if (registro.Table.Columns.Contains("nombreusuario"))
            { this.NombreUsuario = registro["nombreusuario"].ToString(); }

            if (registro.Table.Columns.Contains("idproveedorpersonal"))
            { this.IdProveedorPersonal = Convert.ToInt32(registro["idproveedorpersonal"]); }

            if (registro.Table.Columns.Contains("nombreproveedor"))
            { this.NombreProveedor = registro["nombreproveedor"].ToString(); }

            if (registro.Table.Columns.Contains("nombrecortoproveedor"))
            { this.NombreCortoProveedor = registro["nombrecortoproveedor"].ToString(); }

            if (registro.Table.Columns.Contains("nombrerefrendadora"))
                this.NombreRefrendadora = Convert.IsDBNull(registro["nombrerefrendadora"]) ? null : registro["nombrerefrendadora"].ToString().Trim();

            if (registro.Table.Columns.Contains("nombrepuntoatencion"))
            {
                this.NombrePuntoAtencion = Convert.IsDBNull(registro["nombrepuntoatencion"]) ? "" : registro["nombrepuntoatencion"].ToString().Trim();
                this.UsuarioPuntoAtencion.NombrePuntoAtencion = Convert.IsDBNull(registro["nombrepuntoatencion"]) ? "" : registro["nombrepuntoatencion"].ToString().Trim();
            }

            if (registro.Table.Columns.Contains("nombretipoatencion"))
                this.UsuarioPuntoAtencion.NombreTipoAtencion = Convert.IsDBNull(registro["nombretipoatencion"]) ? "" : registro["nombretipoatencion"].ToString().Trim();

            if (registro.Table.Columns.Contains("interactivo"))
                this.EsInteractivo = Convert.IsDBNull(registro["interactivo"]) ? (Int16)0 : Convert.ToInt16(registro["interactivo"]);

            if (registro.Table.Columns.Contains("remitos"))
                this.Remitos = Convert.IsDBNull(registro["remitos"]) ? String.Empty : registro["remitos"].ToString();
        }

        /// <summary>
        ///
        /// </summary>
        public clsCajaEstadisticaDiario()
        {
            UsuarioPuntoAtencion = new clsUsuarioPuntoAtencion();
        }

        #endregion Constructor

        #region Propiedades

        /// <summary>
        /// NombrePuntoAtencion.
        /// </summary>
        public String NombrePuntoAtencion
        {
            get { return _strnombrepuntoatencion; }
            set { _strnombrepuntoatencion = value; }
        }

        /// <summary>
        /// NombreUsuario.
        /// </summary>
        public String NombreUsuario
        {
            get { return _strnombreusuario; }
            set { _strnombreusuario = value; }
        }

        /// <summary>
        /// FechaApertura.
        /// </summary>
        public DateTime FechaApertura
        {
            get { return _datfechaapertura; }
            set { _datfechaapertura = value; }
        }

        /// <summary>
        /// FechaPago.
        /// </summary>
        public DateTime FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        /// <summary>
        /// NroTransacciones.
        /// </summary>
        public Int32 NroTransacciones
        {
            get { return _intnrotransacciones; }
            set { _intnrotransacciones = value; }
        }

        /// <summary>
        /// NroServicios.
        /// </summary>
        public Int32 NroServicios
        {
            get { return _intnroservicios; }
            set { _intnroservicios = value; }
        }

        /// <summary>
        /// ImporteVentanillaMN.
        /// </summary>
        public Decimal ImporteVentanillaMN
        {
            get { return _decimporteventanillamn; }
            set { _decimporteventanillamn = value; }
        }

        /// <summary>
        /// ImporteChequeMN.
        /// </summary>
        public Decimal ImporteChequeMN
        {
            get { return _decimportecheque; }
            set { _decimportecheque = value; }
        }

        /// <summary>
        /// ImporteDocumentosMN.
        /// </summary>
        public Decimal ImporteDocumentosMN
        {
            get { return _decimportedocumentosmn; }
            set { _decimportedocumentosmn = value; }
        }

        /// <summary>
        /// PendienteabovedaMN.
        /// </summary>
        public Decimal PendienteabovedaMN
        {
            get { return _decpendienteabovedamn; }
            set { _decpendienteabovedamn = value; }
        }

        /// <summary>
        /// ImporteenbovedaMN.
        /// </summary>
        public Decimal ImporteenbovedaMN
        {
            get { return _decimporteenbovedamn; }
            set { _decimporteenbovedamn = value; }
        }

        /// <summary>
        /// EnCustodiaMN.
        /// </summary>
        public Decimal EnCustodiaMN
        {
            get { return _decimporteencustodiamn; }
            set { _decimporteencustodiamn = value; }
        }

        /// <summary>
        /// EnBancoMN.
        /// </summary>
        public Decimal EnBancoMN
        {
            get { return _decimporteenbancosmn; }
            set { _decimporteenbancosmn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreRefrendadora
        {
            get { return _nombreRefrendadora; }
            set { _nombreRefrendadora = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdProveedorPersonal
        {
            get { return _idProveedorPersonal; }
            set { _idProveedorPersonal = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreProveedor
        {
            get { return _nombreProveedor; }
            set { _nombreProveedor = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreCortoProveedor
        {
            get { return _nombreCortoProveedor; }
            set { _nombreCortoProveedor = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public clsUsuarioPuntoAtencion UsuarioPuntoAtencion
        {
            get { return _objusuariopuntoatencion; }
            set { _objusuariopuntoatencion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 NroDocumentos
        {
            get { return _intnrodocumentos; }
            set { _intnrodocumentos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal DineroMN
        {
            get { return _decdineromn; }
            set { _decdineromn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteDocumentosExternoMN
        {
            get { return _decimportedocumentosexternomn; }
            set { _decimportedocumentosexternomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteDocumentosInternoMN
        {
            get { return _decimportedocumentosinternomn; }
            set { _decimportedocumentosinternomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaCierre
        {
            get { return _datfechacierre; }
            set { _datfechacierre = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime UltimoMovimiento
        {
            get { return _datultimomovimiento; }
            set { _datultimomovimiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovDocumentoEgresoMN
        {
            get { return _decmovdocumentoegresomn; }
            set { _decmovdocumentoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovDocumentoIngresoMN
        {
            get { return _decmovdocumentoingresomn; }
            set { _decmovdocumentoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovEfectivoEgresoMN
        {
            get { return _decmovefectivoegresomn; }
            set { _decmovefectivoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovEfectivoIngresoMN
        {
            get { return _decmovefectivoingresomn; }
            set { _decmovefectivoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecChequeIngresoCalculadoMN
        {
            get { return _decrecchequeingresocalculadomn; }
            set { _decrecchequeingresocalculadomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecDocumentoEgresoMN
        {
            get { return _decrecdocumentoegresomn; }
            set { _decrecdocumentoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecDocumentoIngresoMN
        {
            get { return _decrecdocumentoingresomn; }
            set { _decrecdocumentoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecEfectivoEgresoMN
        {
            get { return _decrecefectivoegresomn; }
            set { _decrecefectivoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecEfectivoIngresoMN
        {
            get { return _decrecefectivoingresomn; }
            set { _decrecefectivoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 CantidadPendienteABoveda
        {
            get { return _intcantidadpendienteaboveda; }
            set { _intcantidadpendienteaboveda = value; }
        }

        //VAS  Total recaudado.
        /// <summary>
        ///
        /// </summary>
        public Decimal TotalRecaudado
        {
            get { return _decTotalRecaudado; }
            set { _decTotalRecaudado = value; }
        }

        //VAS Creados para el manejo de chques - 02/02/2009
        /// <summary>
        ///
        /// </summary>
        public Decimal RecChequeEgresoMN
        {
            get { return _decrecchequeegresomn; }
            set { _decrecchequeegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecChequeIngresoMN
        {
            get { return _decrecchequeingresomn; }
            set { _decrecchequeingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovChequeEgresoMN
        {
            get { return _decmovchequeegresomn; }
            set { _decmovchequeegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovChequeIngresoMN
        {
            get { return _decmovchequeingresomn; }
            set { _decmovchequeingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 EsInteractivo
        {
            get { return _intesintercativo; }
            set { _intesintercativo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Remitos
        {
            get { return _strremitos; }
            set { _strremitos = value; }
        }

        #endregion Propiedades

        #region Propiedades Interactuan con UsuarioPuntoAtencion

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set
            {
                _intidnrocaja = value;
                UsuarioPuntoAtencion.IdNroCajaUltimo = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstadoCobranza
        {
            get { return UsuarioPuntoAtencion.IdEstadoCobranza; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdUnidadNegocio
        {
            get { return UsuarioPuntoAtencion.IdUnidadNegocio; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdCentroServicio
        {
            get { return UsuarioPuntoAtencion.IdCentroServicio; }
        }

        #endregion Propiedades Interactuan con UsuarioPuntoAtencion
    }

    #endregion clsCajaEstadisticaDiario

    #region clsListaDocumentoDescargo
    public class clsListaDocumentoDescargo
    {
        private List<clsDocumentosDescargoBase> _elementos = new List<clsDocumentosDescargoBase>();

        public List<clsDocumentosDescargoBase> Elementos
        {
            get { return _elementos; }
            set { _elementos = value; }
        }

        //Constructor *******************************************************

        public clsListaDocumentoDescargo()
        {
        }

        public clsListaDocumentoDescargo(DataTable entidaddocumentodescargo)
        {
            if (entidaddocumentodescargo != null)
            {
                foreach (DataRow registro in entidaddocumentodescargo.Rows)
                {
                    Elementos.Add(new clsDocumentosDescargoPropio(registro));
                }
            }
        }
        //*******************************************************************

        #region MétodosBusqueda
        Predicate<clsDocumentosDescargoBase> PredicadoBuscarDocumento;
        Int16 _intidtipodocumentotmp;
        Int32 _intidnroserviciotmp;
        String _strnrodocumentotmp;
        public List<clsDocumentosDescargoBase> BuscarPorDocumentosSinNumero(Int16 idtipodocumento, Int32 idnroservicio)
        {
            _strnrodocumentotmp = "";
            _intidtipodocumentotmp = idtipodocumento;
            _intidnroserviciotmp = idnroservicio;
            List<clsDocumentosDescargoBase> _objlistadocumentodescargo;
            PredicadoBuscarDocumento = new Predicate<clsDocumentosDescargoBase>(BuscarDocumentoNroServicio);
            _objlistadocumentodescargo = Elementos.FindAll(PredicadoBuscarDocumento);
            return _objlistadocumentodescargo;
        }
        private Boolean BuscarDocumentoNroServicio(clsDocumentosDescargoBase documentodescargo)
        {
            return (documentodescargo.IdTipoDocumento == _intidtipodocumentotmp && documentodescargo.IdNroServicio == _intidnroserviciotmp && documentodescargo.NroDocumento.Trim() == _strnrodocumentotmp);
        }
        #endregion

    }
    #endregion clsListaDocumentoDescargo
    #endregion NGC
}