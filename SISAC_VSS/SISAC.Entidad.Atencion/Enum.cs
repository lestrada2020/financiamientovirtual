﻿namespace SISAC.Entidad.Atencion
{
    #region TypeConsultaGeneral

    public enum TypeConsultaGeneral
    {
        AyudaLista,
        BuscarBySuministro,
        BuscarByNroDocumento,
        BuscarByNombre,
        BuscarByRecibo,
        DatosPago,
        DatosRecibo,
        DatosReclamo,
        DatosMedidor,
        DatosRefacfurado,
        DatosFacturacion,
        DatosTecnico,
        DatosConsumo,
        DatosSuministro,
        DatosContrato,
        DatosDeuda,
        DatosComercial
    }

    #endregion TypeConsultaGeneral
}