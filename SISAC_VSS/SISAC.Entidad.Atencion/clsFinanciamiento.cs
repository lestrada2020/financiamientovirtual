﻿using SISAC.Entidad.Maestro;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

using System.Collections;
using System.Reflection;
using System;
using System.Data;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;

namespace SISAC.Entidad.Atencion
{

    #region clsFinanciamientoOrdenVisaNet

    [Serializable()]
    public class clsFinanciamientoOrdenVisaNet
    {
        #region Properties

        public Int32 IdNumeroOrden { get; set; }

        public Int16 IdEmpresa { get; set; }

        public Int32 IdNroServicio { get; set; }

        public DateTime FechaGeneracion { get; set; }

        public Int32 IdUsuarioWEB { get; set; }

        public String eTicket { get; set; }

        public String MensajeVisaNet { get; set; }

        public String Trama { get; set; }

        public Decimal? Importe { get; set; }

        public Int32? CodAccion { get; set; }

        public Int32? IdCobranzaExterna { get; set; }

        public String NroTarjeta { get; set; }

        public Int32? IdTarea { get; set; }

        public Int16 IdEstado { get; set; }

        public Int32? NroTransaccion { get; set; }

        public String MensajeExcepcion { get; set; }

        public Int16? IdOrigen { get; set; }

        public Int16? TipoImporte { get; set; }

        public String SessionTokenVisa { get; set; }

        public Int16? IdPaso { get; set; }

        public Int16? IdEstadoPaso { get; set; }

        public String JsonPagoResumen { get; set; }

        //Extra

        public Int16 IdUUNN { get; set; }

        public Int32 IdCentroServicio { get; set; }

        public Int16 IdTipoAtencion { get; set; }

        public Int32 IdPuntoAtencion { get; set; }

        public Int32 IdUsuario { get; set; }

        public String CodigoComercio { get; set; }

        #endregion Properties

        #region Constructors

        public clsFinanciamientoOrdenVisaNet()
        {
        }

        public clsFinanciamientoOrdenVisaNet(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdNumeroOrden") && !row.IsNull("IdNumeroOrden")) IdNumeroOrden = Convert.ToInt32(row["IdNumeroOrden"]);
            if (columns.Contains("IdEmpresa") && !row.IsNull("IdEmpresa")) IdEmpresa = Convert.ToInt16(row["IdEmpresa"]);
            if (columns.Contains("IdNroServicio") && !row.IsNull("IdNroServicio")) IdNroServicio = Convert.ToInt32(row["IdNroServicio"]);
            if (columns.Contains("FechaGeneracion") && !row.IsNull("FechaGeneracion")) FechaGeneracion = Convert.ToDateTime(row["FechaGeneracion"]);
            if (columns.Contains("IdUsuarioWEB") && !row.IsNull("IdUsuarioWEB")) IdUsuarioWEB = Convert.ToInt32(row["IdUsuarioWEB"]);
            if (columns.Contains("eTicket") && !row.IsNull("eTicket")) eTicket = Convert.ToString(row["eTicket"]);
            if (columns.Contains("MensajeVisaNet") && !row.IsNull("MensajeVisaNet")) MensajeVisaNet = Convert.ToString(row["MensajeVisaNet"]);
            if (columns.Contains("Trama") && !row.IsNull("Trama")) Trama = Convert.ToString(row["Trama"]);
            if (columns.Contains("Importe") && !row.IsNull("Importe")) Importe = Convert.ToDecimal(row["Importe"]);
            if (columns.Contains("CodAccion") && !row.IsNull("CodAccion")) CodAccion = Convert.ToInt32(row["CodAccion"]);
            if (columns.Contains("IdCobranzaExterna") && !row.IsNull("IdCobranzaExterna")) IdCobranzaExterna = Convert.ToInt32(row["IdCobranzaExterna"]);
            if (columns.Contains("NroTarjeta") && !row.IsNull("NroTarjeta")) NroTarjeta = Convert.ToString(row["NroTarjeta"]);
            if (columns.Contains("IdTarea") && !row.IsNull("IdTarea")) IdTarea = Convert.ToInt32(row["IdTarea"]);
            if (columns.Contains("IdEstado") && !row.IsNull("IdEstado")) IdEstado = Convert.ToInt16(row["IdEstado"]);
            if (columns.Contains("NroTransaccion") && !row.IsNull("NroTransaccion")) NroTransaccion = Convert.ToInt32(row["NroTransaccion"]);
            if (columns.Contains("MensajeExcepcion") && !row.IsNull("MensajeExcepcion")) MensajeExcepcion = Convert.ToString(row["MensajeExcepcion"]);
            if (columns.Contains("IdOrigen") && !row.IsNull("IdOrigen")) IdOrigen = Convert.ToInt16(row["IdOrigen"]);
            if (columns.Contains("TipoImporte") && !row.IsNull("TipoImporte")) TipoImporte = Convert.ToInt16(row["TipoImporte"]);
            if (columns.Contains("SessionTokenVisa") && !row.IsNull("SessionTokenVisa")) SessionTokenVisa = Convert.ToString(row["SessionTokenVisa"]);
            if (columns.Contains("IdPaso") && !row.IsNull("IdPaso")) IdPaso = Convert.ToInt16(row["IdPaso"]);
            if (columns.Contains("IdEstadoPaso") && !row.IsNull("IdEstadoPaso")) IdEstadoPaso = Convert.ToInt16(row["IdEstadoPaso"]);
            if (columns.Contains("JsonPagoResumen") && !row.IsNull("JsonPagoResumen")) JsonPagoResumen = Convert.ToString(row["JsonPagoResumen"]);

            if (columns.Contains("iduunn")) IdUUNN = row["iduunn"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(row["iduunn"]);
            if (columns.Contains("IdTipoAtencion")) IdTipoAtencion = row["IdTipoAtencion"] == DBNull.Value ? (Int16)0 : Convert.ToInt16(row["IdTipoAtencion"]);
            if (columns.Contains("IdCentroServicio")) IdCentroServicio = row["IdCentroServicio"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(row["IdCentroServicio"]);
            if (columns.Contains("IdPuntoAtencion")) IdPuntoAtencion = row["IdPuntoAtencion"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(row["IdPuntoAtencion"]);
            if (columns.Contains("idusuario")) IdUsuario = row["idusuario"] == DBNull.Value ? (Int32)0 : Convert.ToInt32(row["idusuario"]);
            if (columns.Contains("CodigoComercio")) CodigoComercio = row["CodigoComercio"] == DBNull.Value ? String.Empty : row["CodigoComercio"].ToString();
        }

        #endregion Constructors
    }

    #endregion clsFinanciamientoOrdenVisaNet

    #region clsListaFinanciamientoOrdenVisaNet

    [Serializable()]
    public class clsListaFinanciamientoOrdenVisaNet
    {
        #region Properties

        public List<clsFinanciamientoOrdenVisaNet> Elementos { get; set; }

        #endregion Properties

        #region Constructors

        public clsListaFinanciamientoOrdenVisaNet()
        {
            Elementos = new List<clsFinanciamientoOrdenVisaNet>();
        }

        public clsListaFinanciamientoOrdenVisaNet(DataTable table)
        {
            Elementos = new List<clsFinanciamientoOrdenVisaNet>();

            if (table == null) return;

            foreach (DataRow row in table.Rows) Elementos.Add(new clsFinanciamientoOrdenVisaNet(row));
        }

        #endregion Constructors
    }

    #endregion clsListaFinanciamientoOrdenVisaNet

    #region clsFinanciamientoOrdenVisaNetDetalle

    [Serializable()]
    public class clsFinanciamientoOrdenVisaNetDetalle
    {
        #region Properties

        public Int32 IdDetalleOrden { get; set; }

        public Int32 IdNumeroOrden { get; set; }

        public Int16 IdTipoDocumento { get; set; }

        public string NroDocumento { get; set; }

        public Int32 IdOrdenCobro { get; set; }

        public String IdOrdenCobroDocumento { get; set; }

        public Int32 Periodo { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public Decimal Saldo { get; set; }

        public Int16? IdEmpresa { get; set; }

        #endregion Properties

        #region Constructors

        public clsFinanciamientoOrdenVisaNetDetalle()
        {
        }

        public clsFinanciamientoOrdenVisaNetDetalle(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdDetalleOrden") && !row.IsNull("IdDetalleOrden")) IdDetalleOrden = Convert.ToInt32(row["IdDetalleOrden"]);
            if (columns.Contains("IdNumeroOrden") && !row.IsNull("IdNumeroOrden")) IdNumeroOrden = Convert.ToInt32(row["IdNumeroOrden"]);
            if (columns.Contains("IdOrdenCobro") && !row.IsNull("IdOrdenCobro")) IdOrdenCobro = Convert.ToInt32(row["IdOrdenCobro"]);
            if (columns.Contains("IdOrdenCobroDocumento") && !row.IsNull("IdOrdenCobroDocumento")) IdOrdenCobroDocumento = Convert.ToString(row["IdOrdenCobroDocumento"]);
            if (columns.Contains("Periodo") && !row.IsNull("Periodo")) Periodo = Convert.ToInt32(row["Periodo"]);
            if (columns.Contains("FechaVencimiento") && !row.IsNull("FechaVencimiento")) FechaVencimiento = Convert.ToDateTime(row["FechaVencimiento"]);
            if (columns.Contains("Saldo") && !row.IsNull("Saldo")) Saldo = Convert.ToDecimal(row["Saldo"]);
            if (columns.Contains("NroDocumento") && !row.IsNull("NroDocumento")) NroDocumento = row["NroDocumento"].ToString();
            if (columns.Contains("IdTipoDocumento") && !row.IsNull("IdTipoDocumento")) IdTipoDocumento = Convert.ToInt16(row["IdTipoDocumento"].ToString());
        }

        #endregion Constructors
    }

    #endregion clsOrdenVisaNetDetalle

    #region clsListaOrdenVisaNetDetalle

    [Serializable()]
    public class clsListaFinanciamientoOrdenVisaNetDetalle
    {
        #region Properties

        public List<clsFinanciamientoOrdenVisaNetDetalle> Elementos { get; set; }

        #endregion Properties

        #region Constructors

        public clsListaFinanciamientoOrdenVisaNetDetalle()
        {
            Elementos = new List<clsFinanciamientoOrdenVisaNetDetalle>();
        }

        public clsListaFinanciamientoOrdenVisaNetDetalle(DataTable table)
        {
            Elementos = new List<clsFinanciamientoOrdenVisaNetDetalle>();

            if (table == null) return;

            foreach (DataRow row in table.Rows) Elementos.Add(new clsFinanciamientoOrdenVisaNetDetalle(row));
        }

        #endregion Constructors
    }

    #endregion clsListaOrdenVisaNetDetalle

    #region NGC

    #region Enum

    #region TipoMoneda

    public enum FinanciamientoMoneda
    {
        Soles = 1,
        Dolares = 2,
    }

    #endregion TipoMoneda

    #region TipoDocumentoComercial

    public enum FinanciamientoTipoDocumentoComercial
    {
        Efectivo = 1,
        BoletaVenta = 2,
        Factura = 3,
        Ticket = 4,
        NotaCredito = 5,
        NotaDebito = 6,
        ReciboEnergia = 7,
        Cheque = 8,
        OrdenTrabajo = 29,
        ConvenioDePago = 11,
        TarjetaCredito = 12,
        OrdenPago = 16,
        DepositoBancario = 18,
        NotaIngresoCaja = 22,
        ReciboAgua = 23,
        ReciboTelefono = 24,
        ReciboCableMagico = 25,
        DepositoGarangia = 33,
        CuotaInicialConvenio = 38,
        InteresDeudaEnergia = 45,
        VoucherContable = 102,

        //ActaInstNuevoSuministro = 104,
        ActaTrabajo = 110,

        RegistroInterrupcion = 142,

        //ActaCambioMedidor = 125,
        //ActaRetiroMedidor = 127,
        //ActaDenunciaAP = 128
        VoucherDsctoPlanilla = 148,

        DocumentoDescuento = 156,
        SobranteFaltante = 157,
        ContratoSuministroEnergiaMayores = 158,
        RegularizaciónFacturacionNegativo = 159,
        NotaAbono = 160,
        CorteReconexion = 161,
        EncargoTerceros = 162,
        ReciboPrepago = 163,
        Redondeo = 101,
        TransaccionExtrajudicial = 175,
        RegistroInterrupcionSimulacion = 182
    }

    #endregion TipoDocumentoComercial

    #region Estado

    public enum FinanciamientoEstado
    {
        Activo = 1,
        EnProceso = 21,
        Aprobado = 32,
        NoPagado = 50,
        Pagado = 52,
        Fallado = 169,
        Generado = 170,
        Confirmar = 271,
        Normal = 201,
        Cerrado = 98,
        ErrorAplicacion = 9999,
        Procesando = 117,
        Errado = 179
    }

    #endregion Estado

    #region TipoOperacionCobranza

    public enum FinanciamientoTipoOperacionCobranza
    {
        Recaudacion = 1,
        Extorno = 2,
        Remito = 3,
        Derivado = 4
    }

    #endregion FinanciamientoTipoOperacionCobranza

    #region enumMovimientoComercial

    public enum enumFinanciamientoMovimientoComercial
    {
        FacturacionMasiva = 1,
        Facturacion = 2,
        CobranzaTerceros = 3,
        AjusteFacturacion = 4,
        Devoluciones = 5,
        PagosExtornados = 6,
        DepositoEnGarantia = 7,
        Convenios = 11,
        Refacturacion = 12,
        Reclamo = 13,
        Cobranza = 14,
        VentaPrePago = 15,
        NuevoSuministro = 16,
        VentasExtraordinarias = 17,
        ConveniosSolicitudServicio = 18,
        FacturacionTemporal = 19,
        Recupero = 20,
        Reintegro = 21,
        ConveniosRecupero = 22,
        ConvenioVentaExtraordinaria = 23,
    }

    #endregion enumMovimientoComercial

    #region EstadoRegistro

    public enum FinanciamientoEstadoRegistro
    {
        Ninguno = -1,
        Consulta = 0,
        Nuevo = 1,
        Modificado = 2,
        Eliminado = 3,
    }

    #endregion EstadoRegistro
    #endregion Enum

    #region Documentos Descargo

    #region IDocumentosDescargo - V2

    public interface IFinanciamientoDocumentosDescargo
    {
        Int32 IdNroServicio { get; set; }

        Int32 OrdenPagoServicio { get; set; }

        Int16 IdServicio { get; set; }

        Int16 IdEmpresa { get; set; }

        Int32 IdOrdenCobro { get; set; }

        Int32 IdOrdenCobroDocumento { get; set; }

        DateTime FechaEmision { get; set; }

        Int16 IdMonedaOrigen { get; set; }

        Decimal ImporteOrigenDeuda { get; set; }

        Decimal ImporteOrigenCobrar { get; set; }

        Decimal ImporteOrigenDescargo { get; set; }

        Int16 IdMonedaCambio { get; set; }

        Decimal ImporteCambioDeuda { get; set; }

        Decimal ImporteCambioCobrar { get; set; }

        Decimal ImporteCambioDescargo { get; set; }
    }

    #endregion IDocumentosDescargo - V2

    #region DocumentosDescargoBase - V2

    public abstract class clsFinanciamientoDocumentosDescargoBase : IFinanciamientoDocumentosDescargo, IComparable
    {
        #region Campos

        private Int32 _intidtransaccionlog;
        private Int32 _intordenpagoservicio;
        private Int32 _intordenpagodocumento;
        private Int32 _intidnroservicio;
        private String _strcodservicio;
        private Int16 _intidservicio;
        private Int32 _intidordencobro;
        private Int32 _stridordencobrodocumento;
        private String _strnombretipodocumento;
        private String _strabreviatipodocumento;
        private DateTime _datfechaemision;

        private Int16 _intidmonedaorigen;
        private Decimal _decimporteorigendeuda;
        private Decimal _decimporteorigencobrar;
        private Decimal _decimporteorigendescargo;

        private Decimal _dectipocambio;
        private Int16 _intidmonedacambio;
        private Decimal _decimportecambiodeuda;
        private Decimal _decimportecambiocobrar;
        private Decimal _decimportecambiodescargo;

        protected Boolean _boopermitepagoparcial;
        protected Boolean _booesterceros;

        private Int16 _intidemresaservicio;

        private Int32 _intnrotransaccion;

        #endregion Campos

        #region Propiedades

        public Int32 NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int32 OrdenPagoServicio
        {
            get { return _intordenpagoservicio; }
            set { _intordenpagoservicio = value; }
        }

        public Int32 OrdenPagoDocumento
        {
            get { return _intordenpagodocumento; }
            set { _intordenpagodocumento = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public String CodServicio
        {
            get { return _strcodservicio; }
            set { _strcodservicio = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Int32 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        public Int32 IdOrdenCobroDocumento
        {
            get { return _stridordencobrodocumento; }
            set { _stridordencobrodocumento = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }

        public String AbreviaTipoDocumento
        {
            get { return _strabreviatipodocumento; }
            set { _strabreviatipodocumento = value; }
        }

        public DateTime FechaEmision
        {
            get { return _datfechaemision; }
            set { _datfechaemision = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        public Decimal ImporteOrigenDeuda
        {
            //get { return Math.Round(_decimporteorigendeuda, 2); }
            get { return _decimporteorigendeuda; }
            set { _decimporteorigendeuda = value; }
        }

        public Decimal ImporteOrigenCobrar
        {
            //get { return Math.Round(_decimporteorigencobrar,2); }
            get { return _decimporteorigencobrar; }
            set { _decimporteorigencobrar = value; }
        }

        public Decimal ImporteOrigenDescargo
        {
            //get { return Math.Round(_decimporteorigendescargo,2); }
            get { return _decimporteorigendescargo; }
            set { _decimporteorigendescargo = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmonedacambio; }
            set { _intidmonedacambio = value; }
        }

        public Decimal ImporteCambioDeuda
        {
            get { return _decimportecambiodeuda; }
            set { _decimportecambiodeuda = value; }
        }

        public Decimal ImporteCambioCobrar
        {
            get { return _decimportecambiocobrar; }
            set { _decimportecambiocobrar = value; }
        }

        public Decimal ImporteCambioDescargo
        {
            get { return _decimportecambiodescargo; }
            set { _decimportecambiodescargo = value; }
        }

        public Boolean EsTerceros
        {
            get { return _booesterceros; }
            set { _booesterceros = value; }
        }

        public Boolean PermitePagoParcial
        {
            get { return _boopermitepagoparcial; }
            set { _boopermitepagoparcial = value; }
        }

        public Int16 IdEmpresaServicio
        {
            get { return _intidemresaservicio; }
            set { _intidemresaservicio = value; }
        }

        #endregion Propiedades

        #region Metodos

        public virtual clsFinanciamientoDocumentosDescargoBase Copiar()
        { return null; }

        #endregion Metodos

        #region IComparable Members

        public virtual Int32 CompareTo(object obj)
        {
            clsFinanciamientoDocumentosDescargoBase _objdocumentodescargo = (clsFinanciamientoDocumentosDescargoBase)obj;
            String _strcomparacion1;
            String _strcomparacion2;

            _strcomparacion1 = OrdenPagoServicio.ToString() + FechaEmision.ToString("yyyyMMdd")
                             + OrdenPagoDocumento.ToString() + IdOrdenCobroDocumento;

            _strcomparacion2 = _objdocumentodescargo.OrdenPagoServicio.ToString() + _objdocumentodescargo.FechaEmision.ToString("yyyyMMdd")
                             + _objdocumentodescargo.OrdenPagoDocumento.ToString() + _objdocumentodescargo.IdOrdenCobroDocumento;

            return _strcomparacion1.CompareTo(_strcomparacion2);
        }

        #endregion IComparable Members

        #region IDocumentosDescargo Members

        private Int16 _intidempresa;

        public short IdEmpresa
        {
            get
            {
                return _intidempresa;
            }
            set
            {
                _intidempresa = value;
            }
        }

        #endregion IDocumentosDescargo Members
    }

    #endregion DocumentosDescargoBase - V2

    #region DocumentosDescargoPropios - V2

    public class clsFinanciamientoDocumentosDescargoPropio : clsFinanciamientoDocumentosDescargoBase
    {
        #region Campos

        private Int16 _intidordencobrosustento;
        private String _stridordencobrodocumentosustento;
        private Int16 _stridtipodocumentosustento;
        private String _strnumerodocumentosustento;

        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private Int32 _intperiodo;

        private String _strnombrecliente;

        #endregion Campos

        #region Constructor

        //****************************************************************
        /// <summary>
        /// clsDocumentosDescargoPropio
        /// </summary>
        /// <param name="registro"></param>

        public clsFinanciamientoDocumentosDescargoPropio(DataRow registro)
        {
            this.IdOrdenCobro = (Int16)registro["idordencobro"];
            this.NombreTipoDocumento = registro["nombredocumento"].ToString();
            this.AbreviaTipoDocumento = registro["abreviadocumento"].ToString();
            this.IdOrdenCobroDocumento = int.Parse(registro["idordencobrodocumento"].ToString());

            this.IdOrdenCobroSustento = (Int16)registro["idordencobrosustento"];
            this.IdOrdenCobroDocumentoSustento = registro["idordencobrodocumentosustento"].ToString();
            this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("tipocambio")) { this.TipoCambio = (Decimal)registro["tipocambio"]; }
            this.ImporteOrigenDeuda = (Decimal)registro["importedocumento"];
            this.ImporteOrigenDescargo = (Decimal)registro["importepagado"];
            this.IdServicio = (Int16)registro["idservicio"];
            this.FechaEmision = (DateTime)registro["fechaemision"];
            this.IdTipoIdentidad = (Int16)registro["idtipoidentidad"];
            this.NroIdentidad = registro["nroidentidad"].ToString();
            this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("codservicio"))
                this.CodServicio = registro["codservicio"].ToString().Trim();
            if (registro.Table.Columns.Contains("periodo"))
                this.Periodo = Convert.IsDBNull(registro["periodo"]) ? (Int32)0 : Convert.ToInt32(registro["periodo"]);
        }

        //*****************************************************************

        public clsFinanciamientoDocumentosDescargoPropio
        (Int32 idtransaccionlog, Int16 idordencobro, String nombretipodocumento
        , String idordencobrodocumento, Int16 idordencobrosustento, String idordencobrodocumentosustento
        , Decimal importedeuda, Decimal importecobrar, Int16 idservicio, Int16 idmoneda
        , DateTime fechaemision, Int16 idtipoidentidad, String nroidentidad, Int32 idnroservicio
        )
        {
            //this.IdTransaccionLog =  idtransaccionlog;
            this.IdOrdenCobro = idordencobro;
            this.NombreTipoDocumento = nombretipodocumento;
            this.IdOrdenCobroDocumento = int.Parse(idordencobrodocumento);

            this.IdOrdenCobroSustento = idordencobrosustento;
            this.IdOrdenCobroDocumentoSustento = idordencobrodocumentosustento;

            this.IdMonedaOrigen = idmoneda;
            this.ImporteOrigenDeuda = importedeuda;
            this.ImporteOrigenDescargo = 0;
            this.ImporteOrigenCobrar = importecobrar;

            this.IdServicio = idservicio;
            this.FechaEmision = fechaemision;
            this.IdTipoIdentidad = idtipoidentidad;
            this.NroIdentidad = nroidentidad;
            this.IdNroServicio = idnroservicio;
        }

        public clsFinanciamientoDocumentosDescargoPropio()
        {
        }

        #endregion Constructor

        #region Propiedades

        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        public Int16 IdOrdenCobroSustento
        {
            get { return _intidordencobrosustento; }
            set { _intidordencobrosustento = value; }
        }

        public Int16 IdTipoDocumentoSustento
        {
            get { return _stridtipodocumentosustento; }
            set { _stridtipodocumentosustento = value; }
        }
        public String NroDocumentoSustento
        {
            get { return _strnumerodocumentosustento; }
            set { _strnumerodocumentosustento = value; }
        }

        public String IdOrdenCobroDocumentoSustento
        {
            get { return _stridordencobrodocumentosustento; }
            set { _stridordencobrodocumentosustento = value; }
        }

        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }

        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }

        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }

        #endregion Propiedades

        #region Metodos

        public override clsFinanciamientoDocumentosDescargoBase Copiar()
        {
            clsFinanciamientoDocumentosDescargoPropio _objdocumentodescargopropio = new clsFinanciamientoDocumentosDescargoPropio();
            _objdocumentodescargopropio.EsTerceros = this.EsTerceros;
            _objdocumentodescargopropio.FechaEmision = this.FechaEmision;
            _objdocumentodescargopropio.IdMonedaCambio = this.IdMonedaCambio;
            _objdocumentodescargopropio.IdMonedaOrigen = this.IdMonedaOrigen;
            _objdocumentodescargopropio.IdNroServicio = this.IdNroServicio;
            _objdocumentodescargopropio.IdServicio = this.IdServicio;
            _objdocumentodescargopropio.IdOrdenCobro = this.IdOrdenCobro;
            _objdocumentodescargopropio.IdOrdenCobroSustento = this.IdOrdenCobroSustento;

            _objdocumentodescargopropio.IdTipoIdentidad = this.IdTipoIdentidad;
            _objdocumentodescargopropio.IdTransaccionLog = this.IdTransaccionLog;
            _objdocumentodescargopropio.ImporteCambioCobrar = this.ImporteCambioCobrar;
            _objdocumentodescargopropio.ImporteCambioDescargo = this.ImporteCambioDescargo;
            _objdocumentodescargopropio.ImporteCambioDeuda = this.ImporteCambioDeuda;
            _objdocumentodescargopropio.ImporteOrigenCobrar = this.ImporteOrigenCobrar;
            _objdocumentodescargopropio.ImporteOrigenDescargo = this.ImporteOrigenDescargo;
            _objdocumentodescargopropio.ImporteOrigenDeuda = this.ImporteOrigenDeuda;
            _objdocumentodescargopropio.NombreTipoDocumento = this.NombreTipoDocumento;
            _objdocumentodescargopropio.IdOrdenCobroDocumento = this.IdOrdenCobroDocumento;
            _objdocumentodescargopropio.IdOrdenCobroDocumentoSustento = this.IdOrdenCobroDocumentoSustento;
            _objdocumentodescargopropio.NroIdentidad = this.NroIdentidad;
            _objdocumentodescargopropio.OrdenPagoServicio = this.OrdenPagoServicio;
            _objdocumentodescargopropio.PermitePagoParcial = this.PermitePagoParcial;
            _objdocumentodescargopropio.TipoCambio = this.TipoCambio;
            _objdocumentodescargopropio.Periodo = this.Periodo;

            return _objdocumentodescargopropio;
        }

        #endregion Metodos
    }

    #endregion DocumentosDescargoPropios - V2

    #region DocumentosDescargoTerceros - V2

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.distriluz.com.pe/optimusng/serviciosweb")]
    public class clsFinanciamientoDocumentosDescargoTerceros : clsFinanciamientoDocumentosDescargoBase
    {
        #region Campos

        private String _strnroservicio;
        private Int16 _intidtiporecibo;
        private String _strnrorecibo;
        private clsFinanciamientoDocumentosDescargoPropio _objdocumentodescargopropio;

        #endregion Campos

        #region Constructor

        public clsFinanciamientoDocumentosDescargoTerceros()
        {
            _booesterceros = true;
        }

        #endregion Constructor

        #region Propiedades

        public String NroServicio
        {
            get { return _strnroservicio; }
            set { _strnroservicio = value; }
        }

        public Int16 IdTipoRecibo
        {
            get { return _intidtiporecibo; }
            set { _intidtiporecibo = value; }
        }

        public String NroRecibo
        {
            get { return _strnrorecibo; }
            set { _strnrorecibo = value; }
        }

        public clsFinanciamientoDocumentosDescargoPropio DocumentoDescargoPropio
        {
            get { return _objdocumentodescargopropio; }
            set { _objdocumentodescargopropio = value; }
        }

        #endregion Propiedades

        #region Metodos

        public override clsFinanciamientoDocumentosDescargoBase Copiar()
        {
            clsFinanciamientoDocumentosDescargoTerceros _objdocumentodescargotercero = new clsFinanciamientoDocumentosDescargoTerceros();
            _objdocumentodescargotercero.EsTerceros = this.EsTerceros;
            _objdocumentodescargotercero.FechaEmision = this.FechaEmision;
            _objdocumentodescargotercero.IdMonedaCambio = this.IdMonedaCambio;
            _objdocumentodescargotercero.IdMonedaOrigen = this.IdMonedaOrigen;
            _objdocumentodescargotercero.IdNroServicio = this.IdNroServicio;
            _objdocumentodescargotercero.IdServicio = this.IdServicio;
            _objdocumentodescargotercero.IdOrdenCobro = this.IdOrdenCobro;
            _objdocumentodescargotercero.IdTransaccionLog = this.IdTransaccionLog;
            _objdocumentodescargotercero.ImporteCambioCobrar = this.ImporteCambioCobrar;
            _objdocumentodescargotercero.ImporteCambioDescargo = this.ImporteCambioDescargo;
            _objdocumentodescargotercero.ImporteCambioDeuda = this.ImporteCambioDeuda;
            _objdocumentodescargotercero.ImporteOrigenCobrar = this.ImporteOrigenCobrar;
            _objdocumentodescargotercero.ImporteOrigenDescargo = this.ImporteOrigenDescargo;
            _objdocumentodescargotercero.ImporteOrigenDeuda = this.ImporteOrigenDeuda;
            _objdocumentodescargotercero.NombreTipoDocumento = this.NombreTipoDocumento;
            _objdocumentodescargotercero.IdOrdenCobroDocumento = this.IdOrdenCobroDocumento;
            _objdocumentodescargotercero.OrdenPagoServicio = this.OrdenPagoServicio;
            _objdocumentodescargotercero.PermitePagoParcial = this.PermitePagoParcial;
            _objdocumentodescargotercero.TipoCambio = this.TipoCambio;

            _objdocumentodescargotercero.NroServicio = this.NroServicio;
            _objdocumentodescargotercero.IdTipoRecibo = this.IdTipoRecibo;
            _objdocumentodescargotercero.NroRecibo = this.NroRecibo;
            _objdocumentodescargotercero.DocumentoDescargoPropio = this.DocumentoDescargoPropio;

            return _objdocumentodescargotercero;
        }

        #endregion Metodos
    }

    #endregion DocumentosDescargoTerceros - V2

    #endregion Documentos Descargo

    #region clsMediosPagoExterno

    public class clsFinanciamientoMediosPagoExterno : clsFinanciamientoMediosPagoBase
    {
        private Boolean _boogeneravuelto;
        private Boolean _booesefectivo;
        private Boolean _booescheque;
        private Int16 _intesingreso;

        private Int16? _intidtipotarjeta;
        private String _strnrotajeta;
        private Int16 _intidcuentabancaria;
        private Int16 _intidcuentacontable;
        private String _strcuentabancaria;
        private DateTime? _datfechadocumento;
        private Int32 _intidnrocaja;

        //VAS
        private String _strnombrebanco;

        //EGM
        private Boolean _booenviaaboveda;

        public clsFinanciamientoMediosPagoExterno(
            Int16 idordencobro, String nombretipodocumento, String idordencobrodocumento,
            Int16 idbanco, Int16 idmonedapago, Decimal importepago, Int16 idmonedacambio,
            Decimal importecambio, Decimal tipocambio, Boolean generavuelto,
            Int16 idtipotarjeta, String nrotarjeta, String nrocuentabancaria,
            String monedapagosimbolo, Boolean efectivo)
        {
            this.IdOrdenCobro = idordencobro;
            this.NombreTipoDocumento = nombretipodocumento;
            this.IdOrdenCobroDocumento = idordencobrodocumento;
            this.IdBanco = idbanco;
            this.MonedaOrigenSimbolo = monedapagosimbolo;
            this.IdMonedaOrigen = idmonedapago;
            this.ImporteOrigenTotal = importepago;

            this.IdMonedaCambio = idmonedacambio;
            this.ImporteCambioTotal = importecambio;
            this.TipoCambio = tipocambio;
            this.GeneraVuelto = generavuelto;
            this.IdTipoTarjeta = idtipotarjeta;
            this.NroTarjeta = nrotarjeta;
            this.NroCuentaBancaria = nrocuentabancaria;
        }

        public clsFinanciamientoMediosPagoExterno(Int16 idordencobro, String nombretipodocumento, String idordencobrodocumento, Int16 idbanco, Int16 idmonedapago, Decimal importepago, Int16 idmonedacambio, Decimal importecambio, Decimal tipocambio, Boolean generavuelto, Int16 idtipotarjeta, String nrotarjeta, String nrocuentabancaria, String monedapagosimbolo, Boolean efectivo, Int16 intidservicio)
        {
            this.IdOrdenCobro = idordencobro;
            this.NombreTipoDocumento = nombretipodocumento;
            this.IdOrdenCobroDocumento = idordencobrodocumento;
            this.IdBanco = idbanco;
            this.MonedaOrigenSimbolo = monedapagosimbolo;
            this.IdMonedaOrigen = idmonedapago;
            this.ImporteOrigenTotal = importepago;

            this.IdMonedaCambio = idmonedacambio;
            this.ImporteCambioTotal = importecambio;
            this.TipoCambio = tipocambio;
            this.GeneraVuelto = generavuelto;
            this.IdTipoTarjeta = idtipotarjeta;
            this.NroTarjeta = nrotarjeta;
            this.NroCuentaBancaria = nrocuentabancaria;
            this.IdServicio = intidservicio;
        }

        public clsFinanciamientoMediosPagoExterno(DataRow registro)
        {
            this.Sel = Convert.ToBoolean(registro["Sel"]);
            this.EsEfectivo = Convert.ToBoolean(registro["EsEfectivo"]);

            this.EsIngreso = Convert.ToInt16(registro["EsIngreso"]);

            this.GeneraVuelto = Convert.ToBoolean(registro["GeneraVuelto"]);
            this.IdMonedaCambio = Convert.ToInt16(registro["IdMonedaCambio"]);
            this.IdMonedaOrigen = Convert.ToInt16(registro["IdMoneda"]);
            this.MonedaOrigenSimbolo = Convert.ToString(registro["monedaorigensimbolo"]);
            this.IdOrdenCobro = Convert.ToInt16(registro["IdOrdenCobro"]);
            this.IdOrdenCobroDocumento = registro["IdOrdenCobroDocumento"].ToString();
            this.NombreTipoDocumento = registro["NombreDocumento"].ToString();
            this.ImporteOrigenTotal = Convert.ToDecimal(registro["Importe"]);
            this.ImporteCambioTotal = Convert.ToDecimal(registro["ImporteCambio"]);
            this.TipoCambio = Convert.ToDecimal(registro["TipoCambio"]);
            if (registro.Table.Columns.Contains("EsEfectivo")) { this.EsEfectivo = Convert.ToBoolean(registro["EsEfectivo"]); }
            if (this.IdOrdenCobro == (Int16)TipoDocumentoComercial.Efectivo) { return; }

            if (registro.Table.Columns.Contains("EsCheque")) { this.EsCheque = Convert.ToBoolean(registro["EsCheque"]); }
            this.NroCuentaBancaria = registro["CuentaBancaria"].ToString();
            this.IdBanco = Convert.IsDBNull(registro["IdBanco"]) ? (Int16?)null : Convert.ToInt16(registro["IdBanco"]);
            this.NroTarjeta = registro["NroTarjeta"].ToString();
            if (IdTipoTarjeta != null) { this.IdTipoTarjeta = Convert.ToInt16(registro["IdTipoTarjeta"]); }

            this.DescripcionTarjeta = registro.Table.Columns.Contains("descripciontarjeta") ? registro["descripciontarjeta"].ToString() : "";

            //VAS
            this.NombreBanco = registro.Table.Columns.Contains("NombreBanco") ? registro["NombreBanco"].ToString() : "";
            //this.OrdenDePago = Convert.ToInt16(registro["OrdenDePago"]);
            //this.MonedaOrigenSimbolo = registro["MonedaSimbolo"].ToString();
            //this.NombreMonedaOrigen = registro["NombreMoneda"].ToString();
            //this.FechaDocumento = Convert.ToDateTime(registro["fechadeposito"]);
            //this.IdTransaccionLog = Convert.ToInt32(registro["IdTransaccionLog"]);
        }

        public clsFinanciamientoMediosPagoExterno()
        {
        }

        public Boolean EnviaABoveda
        {
            get { return _booenviaaboveda; }
            set { _booenviaaboveda = value; }
        }

        public Boolean GeneraVuelto
        {
            get { return _boogeneravuelto; }
            set { _boogeneravuelto = value; }
        }

        public Int16 EsIngreso
        {
            get { return _intesingreso; }
            set { _intesingreso = value; }
        }

        public Int16? IdTipoTarjeta
        {
            get { return _intidtipotarjeta; }
            set { _intidtipotarjeta = value; }
        }

        public String NroTarjeta
        {
            get { return _strnrotajeta; }
            set { _strnrotajeta = value; }
        }

        public Int16 IdCuentaBancaria
        {
            get { return _intidcuentabancaria; }
            set { _intidcuentabancaria = value; }
        }

        public Int16 IdCuentaContable
        {
            get { return _intidcuentacontable; }
            set { _intidcuentacontable = value; }
        }

        public String NroCuentaBancaria
        {
            get { return _strcuentabancaria; }
            set { _strcuentabancaria = value; }
        }

        public Boolean EsEfectivo
        {
            get { return _booesefectivo; }
            set { _booesefectivo = value; }
        }

        public Boolean EsCheque
        {
            get { return _booescheque; }
            set { _booescheque = value; }
        }

        public DateTime? FechaDocumento
        {
            get { return _datfechadocumento; }
            set { _datfechadocumento = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public String NombreBanco
        {
            get { return _strnombrebanco; }
            set { _strnombrebanco = value; }
        }
    }

    #endregion clsMediosPagoExterno

    #region clsMediosPagoBase

    public class clsFinanciamientoMediosPagoBase : IComparable
    {
        #region Campos

        private Boolean _boosel;
        private Int32 _intidtransaccionlog;
        private Int16 _intidordencobro;
        private String _stridordencobrodocumento;
        private String _strnombretipodocumento;
        private String _strnombremonedaorigen;
        private String _strsimbolomonedaorigen;
        private Int16 _intidmonedaorigen;
        private Decimal _decimporteorigentotal;
        private Decimal _decimporteorigendescargo;

        private Decimal _dectipocambio;
        private Int16 _intidmonedacambio;
        private Decimal _decimportecambiototal;
        private Decimal _decimportecambiodescargo;

        protected Boolean _booesmediopagointerno;
        private Int16 _intordendepago;
        private Int16? _intidbanco;

        private String _descripciontarjeta;

        private Int32 _intnrotransaccion;

        //CLS
        private Int16 _intidservicio;

        // 
        public int idTipoDocumento { get; set; }
        public string NroDocumento { get; set; }

        #endregion Campos

        #region Propiedades IMediosPago

        /// <summary>
        ///
        /// </summary>
        public Int32 NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String DescripcionTarjeta
        {
            get { return _descripciontarjeta; }
            set { _descripciontarjeta = value; }
        }

        public Boolean Sel
        {
            get { return _boosel; }
            set { _boosel = value; }
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int16 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        public String IdOrdenCobroDocumento
        {
            get { return _stridordencobrodocumento; }
            set { _stridordencobrodocumento = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }

        public String NombreMonedaOrigen
        {
            get { return _strnombremonedaorigen; }
            set { _strnombremonedaorigen = value; }
        }

        public String MonedaOrigenSimbolo
        {
            get { return _strsimbolomonedaorigen; }
            set { _strsimbolomonedaorigen = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        public Decimal ImporteOrigenTotal
        {
            get { return _decimporteorigentotal; }
            set { _decimporteorigentotal = value; }
        }

        public Decimal ImporteOrigenDescargo
        {
            get { return Math.Round(_decimporteorigendescargo, 2); }
            set { _decimporteorigendescargo = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmonedacambio; }
            set { _intidmonedacambio = value; }
        }

        public Decimal ImporteCambioTotal
        {
            get { return _decimportecambiototal; }
            set { _decimportecambiototal = value; }
        }

        public Decimal ImporteCambioDescargo
        {
            get { return _decimportecambiodescargo; }
            set { _decimportecambiodescargo = value; }
        }

        public Boolean EsMedioPagoInterno
        {
            get { return _booesmediopagointerno; }
        }

        public Int16 OrdenDePago
        {
            get { return _intordendepago; }
            set { _intordendepago = value; }
        }

        public Int16? IdBanco
        {
            get { return _intidbanco; }
            set { _intidbanco = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        #endregion Propiedades IMediosPago

        #region IComparable Members

        public virtual Int32 CompareTo(object obj)
        {
            clsFinanciamientoMediosPagoBase _objmediopago = (clsFinanciamientoMediosPagoBase)obj;
            String _strcomparacion1 = (EsMedioPagoInterno ? "0" : "1") + OrdenDePago.ToString();
            String _strcomparacion2 = (_objmediopago.EsMedioPagoInterno ? "0" : "1") + _objmediopago.OrdenDePago.ToString();

            return _strcomparacion1.CompareTo(_strcomparacion2);
        }

        #endregion IComparable Members

        #region IMediosPago Members

        private Int16 _intidempresa;

        public short IdEmpresa
        {
            get
            {
                return _intidempresa;
            }
            set
            {
                _intidempresa = value;
            }
        }

        #endregion IMediosPago Members
    }

    #endregion clsMediosPagoBase

    #region clsUsuarioPuntoAtencion

    public class clsFinanciamientoUsuarioPuntoAtencion
    {
        #region campos

        private Int32 _intidusuario;
        private Int16 _intidtipoatencion;
        private Int16 _intidpuntoatencion;
        private Int16 _intidcentroservicio;
        private Int16 _intidunidadnegocio;
        private Int16 _intidempresa;
        private Int16 _intidestadocobranza;
        private Int16 _intidestadocaja;
        private Int32 _intidnrocajaultimo;

        private String _strnombrecentroservicio;
        private String _strnombreunidadnegocio;
        private String _strnombreempresa;
        private String _strnombretipopunto;
        private String _strnombrepuntoatencion;
        private String _texto; // EGM
        private DateTime _datfechapago; // EGM
        private DateTime _datfechaapertura;

        //Datos para mostrar Cantidades
        private Decimal _decTotalRecaudado;

        private Decimal _decimporteventanillamn;
        private Decimal _decimportecheque;
        private Decimal _decpendienteabovedamn;
        private Decimal _decimporteenbovedamn;
        private Decimal _decimportedocumentosmn;
        private Int32 _intcantidadpendienteaboveda;
        private String _strestado;

        #endregion campos

        #region constructor

        /// <summary>
        ///
        /// </summary>
        /// <param name="idusuario"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idunidadnegocio"></param>
        /// <param name="idestadocobranza"></param>
        /// <param name="idestadocaja"></param>
        /// <param name="idnrocajaultimo"></param>
        /// <param name="idempresa"></param>
        public clsFinanciamientoUsuarioPuntoAtencion(Int32 idusuario, Int16 idtipoatencion, Int16 idpuntoatencion, Int16 idcentroservicio, Int16 idunidadnegocio, Int16 idestadocobranza, Int16 idestadocaja, Int32 idnrocajaultimo, Int16 idempresa)
        {
            this.IdUsuario = idusuario;
            this.IdTipoAtencion = idtipoatencion;
            this.IdPuntoAtencion = idpuntoatencion;
            this.IdCentroServicio = idcentroservicio;
            this.IdEstadoCobranza = idestadocobranza;
            this.IdEstadoCaja = idestadocaja;
            this.IdNroCajaUltimo = idnrocajaultimo;
            this.IdUnidadNegocio = idunidadnegocio;
            this.IdEmpresa = idempresa;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        public clsFinanciamientoUsuarioPuntoAtencion(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = Convert.ToInt32(registro["idusuario"]);
            if (registro.Table.Columns.Contains("idtipoatencion")) this.IdTipoAtencion = Convert.ToInt16(registro["idtipoatencion"]);
            if (registro.Table.Columns.Contains("idpuntoatencion")) this.IdPuntoAtencion = Convert.ToInt16(registro["idpuntoatencion"]);
            if (registro.Table.Columns.Contains("idcentroservicio")) this.IdCentroServicio = Convert.ToInt16(registro["idcentroservicio"]);
            if (registro.Table.Columns.Contains("IdUUNN")) this.IdUnidadNegocio = Convert.ToInt16(registro["IdUUNN"]);
            if (registro.Table.Columns.Contains("idestadocobranza")) this.IdEstadoCobranza = Convert.ToInt16(registro["idestadocobranza"]);
            if (registro.Table.Columns.Contains("idestadocaja")) this.IdEstadoCaja = Convert.ToInt16(registro["idestadocaja"]);
            if (registro.Table.Columns.Contains("idnrocajaultimo")) this.IdNroCajaUltimo = Convert.ToInt32(registro["idnrocajaultimo"]);

            if (registro.Table.Columns.Contains("nombrecentroservicio"))
                this.NombreCentroServicio = registro["nombrecentroservicio"].ToString();

            if (registro.Table.Columns.Contains("nombretipoatencion"))
                this.NombreTipoAtencion = registro["nombretipoatencion"].ToString();

            if (registro.Table.Columns.Contains("nombreatencion"))
                this.NombrePuntoAtencion = registro["nombreatencion"].ToString();

            // EGM
            if (registro.Table.Columns.Contains("texto"))
                this.Texto = registro["texto"].ToString();

            if (registro.Table.Columns.Contains("fechapago")) FechaPago = Convert.ToDateTime(registro["fechapago"].ToString());
            if (registro.Table.Columns.Contains("fechaapertura")) FechaApertura = Convert.ToDateTime(registro["fechaapertura"].ToString());

            if (registro.Table.Columns.Contains("totalrecaudado")) TotalRecaudado = Convert.ToDecimal(registro["totalrecaudado"].ToString());

            if (registro.Table.Columns.Contains("enventanillamn")) ImporteVentanillaMN = Convert.ToDecimal(registro["enventanillamn"]);

            if (registro.Table.Columns.Contains("ImporteenChequesMN")) ImporteChequeMN = Convert.IsDBNull(registro["ImporteenChequesMN"]) ? 0 : Convert.ToDecimal(registro["ImporteenChequesMN"]);

            if (registro.Table.Columns.Contains("EnDocumentosmn")) ImporteDocumentosMN = Convert.ToDecimal(registro["EnDocumentosmn"]);

            if (registro.Table.Columns.Contains("PendienteaBovedaMN")) PendienteabovedaMN = Convert.IsDBNull(registro["PendienteaBovedaMN"]) ? 0 : Convert.ToDecimal(registro["PendienteaBovedaMN"]);

            if (registro.Table.Columns.Contains("ImporteEnbovedamn")) ImporteenbovedaMN = Convert.ToDecimal(registro["ImporteEnbovedamn"]);

            if (registro.Table.Columns.Contains("DescripcionEstado")) DescripcionEstado = (registro["DescripcionEstado"]).ToString();
        }

        /// <summary>
        ///
        /// </summary>
        public clsFinanciamientoUsuarioPuntoAtencion()
        {
        }

        #endregion constructor

        #region propiedades

        public DateTime FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        public Int16 IdEstadoCobranza
        {
            get { return _intidestadocobranza; }
            set { _intidestadocobranza = value; }
        }

        public Int16 IdUnidadNegocio
        {
            get { return _intidunidadnegocio; }
            set { _intidunidadnegocio = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdEstadoCaja
        {
            get { return _intidestadocaja; }
            set { _intidestadocaja = value; }
        }

        public Int32 IdNroCajaUltimo
        {
            get { return _intidnrocajaultimo; }
            set { _intidnrocajaultimo = value; }
        }

        public String NombreCentroServicio
        {
            get { return _strnombrecentroservicio; }
            set { _strnombrecentroservicio = value; }
        }

        public String NombreUnidadNegocio
        {
            get { return _strnombreunidadnegocio; }
            set { _strnombreunidadnegocio = value; }
        }

        public String NombreEmpresa
        {
            get { return _strnombreempresa; }
            set { _strnombreempresa = value; }
        }

        public String NombreTipoAtencion
        {
            get { return _strnombretipopunto; }
            set { _strnombretipopunto = value; }
        }

        public String NombrePuntoAtencion
        {
            get { return _strnombrepuntoatencion; }
            set { _strnombrepuntoatencion = value; }
        }

        public String Texto // EGM
        {
            get { return _texto; }
            set { _texto = value; }
        }

        public Decimal TotalRecaudado
        {
            get { return _decTotalRecaudado; }
            set { _decTotalRecaudado = value; }
        }

        public Decimal ImporteVentanillaMN
        {
            get { return _decimporteventanillamn; }
            set { _decimporteventanillamn = value; }
        }

        public Decimal ImporteChequeMN
        {
            get { return _decimportecheque; }
            set { _decimportecheque = value; }
        }

        public Decimal PendienteabovedaMN
        {
            get { return _decpendienteabovedamn; }
            set { _decpendienteabovedamn = value; }
        }

        public Decimal ImporteenbovedaMN
        {
            get { return _decimporteenbovedamn; }
            set { _decimporteenbovedamn = value; }
        }

        public Decimal ImporteDocumentosMN
        {
            get { return _decimportedocumentosmn; }
            set { _decimportedocumentosmn = value; }
        }

        public Int32 CantidadPendienteABoveda
        {
            get { return _intcantidadpendienteaboveda; }
            set { _intcantidadpendienteaboveda = value; }
        }

        public String DescripcionEstado
        {
            get { return _strestado; }
            set { _strestado = value; }
        }

        public DateTime FechaApertura
        {
            get { return _datfechaapertura; }
            set { _datfechaapertura = value; }
        }

        #endregion propiedades
    }

    #endregion clsUsuarioPuntoAtencion

    #region clsFinanciamientoCajaEstadisticaDiarioMoneda

    public class clsFinanciamientoCajaEstadisticaDiarioMoneda : clsFinanciamientoCajaDiarioMoneda
    {
        private Int32 _intidtransaccionlog;
        private Int32? _intnrotransaccion;
        private Decimal _decimporteboveda;
        private Decimal _decimportebovedacambio;

        /// <summary>
        ///
        /// </summary>
        public clsFinanciamientoCajaEstadisticaDiarioMoneda()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        public clsFinanciamientoCajaEstadisticaDiarioMoneda(DataRow registro)
        {
            IdNroCaja = (Int32)registro["IdNroCaja"];
            IdMoneda = (Int16)registro["IdMoneda"];
            IdMonedaCambio = (Int16)registro["IdMonedaCambio"];
            Importe = (Decimal)registro["ImporteRetiro"];
            ImporteCambio = (Decimal)registro["ImporteRetiroCambio"];
            TipoCambio = (Decimal)registro["TipoCambio"];
            IdTipoDocumentoComercial = (Int16)registro["idtipodocumentocomercial"];
            if (registro.Table.Columns.Contains("NroMovimiento")) { NroTransaccion = registro.IsNull("NroMovimiento") ? (Int32?)0 : Convert.ToInt32(registro["NroMovimiento"]); }
            if (registro.Table.Columns.Contains("IdBanco")) { IdBanco = registro.IsNull("IdBanco") ? (Int16?)0 : Convert.ToInt16(registro["IdBanco"]); }
            if (registro.Table.Columns.Contains("IdOrdenCobroDocumento")) { NroDocumentoComercial = registro.IsNull("IdOrdenCobroDocumento") ? "" : (String)registro["IdOrdenCobroDocumento"]; }
            if (registro.Table.Columns.Contains("ImporteEnBovedaPago")) { ImporteBoveda = (Decimal)registro["ImporteEnBovedaPago"]; }
            if (registro.Table.Columns.Contains("ImporteEnBovedaCambio")) { ImporteBovedaCambio = (Decimal)registro["ImporteEnBovedaCambio"]; }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        public clsFinanciamientoCajaEstadisticaDiarioMoneda(clsFinanciamientoCajaEstadisticaDiarioMoneda item)
        {
            IdTransaccionLog = item.IdTransaccionLog;
            IdNroCaja = item.IdNroCaja;
            IdMoneda = item.IdMoneda;
            IdMonedaCambio = item.IdMonedaCambio;
            Importe = item.Importe;
            ImporteCambio = item.ImporteCambio;
            TipoCambio = item.TipoCambio;
            IdTipoDocumentoComercial = item.IdTipoDocumentoComercial;
            ImporteDescargo = item.ImporteDescargo;
            ImporteBoveda = item.ImporteBoveda;
            ImporteBovedaCambio = item.ImporteBovedaCambio;
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int32? NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        public Decimal ImporteBoveda
        {
            get { return _decimporteboveda; }
            set { _decimporteboveda = value; }
        }

        public Decimal ImporteBovedaCambio
        {
            get { return _decimportebovedacambio; }
            set { _decimportebovedacambio = value; }
        }
    }

    #endregion clsCajaEstadisticaDiarioMoneda

    #region clsCajaDiarioMoneda

    public abstract class clsFinanciamientoCajaDiarioMoneda
    {
        private Int32 _intidnrocaja;
        private Int16 _intidmoneda;
        private Int16 _intidmonedacambio;
        private Decimal _decimporte;
        private Decimal _decimportecambio;
        private Decimal _dectipocambio;
        private Int16 _intidtipodocumentocomercial;
        private String _strnrodocumentocomercial;
        private Int16? _intidbanco;
        private Decimal _decimportedescargo;


        private Boolean _bolseleccionar = true;

        /// <summary>
        ///
        /// </summary>
        public Boolean Seleccionar
        {
            get { return _bolseleccionar; }
            set { _bolseleccionar = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmonedacambio; }
            set { _intidmonedacambio = value; }
        }

        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        public Decimal ImporteDescargo
        {
            get { return _decimportedescargo; }
            set { _decimportedescargo = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Decimal ImporteCambio
        {
            get { return _decimportecambio; }
            set { _decimportecambio = value; }
        }

        public Int16 IdTipoDocumentoComercial
        {
            get { return _intidtipodocumentocomercial; }
            set { _intidtipodocumentocomercial = value; }
        }

        public Int16? IdBanco
        {
            get { return _intidbanco; }
            set { _intidbanco = value; }
        }

        public String NroDocumentoComercial
        {
            get { return _strnrodocumentocomercial; }
            set { _strnrodocumentocomercial = value; }
        }
    }

    #endregion clsCajaDiarioMoneda

    #region Transaccion - V2

    public class clsFinanciamientoTransaccion
    {
        #region Campos
        private Int32 _intnrotransaccion;
        private Int32 _intidtransaccionlog;
        private Int32 _intidordencobro;
        private Decimal _decimporteneto;
        private DateTime _datfecharegistro;
        private Int16 _intidestado;
        private Int32 _intidnroservicio;
        private Decimal _decimporterecibido;
        private Decimal _decimportetotalvuelto;
        private Decimal _decimportetotalvueltosindevolver;

        private Decimal _decimporteefectivo;
        private Decimal _decimportevalores;
        private Decimal _decimportecheques = 0; //mod
        private DateTime _datfechapago;
        private Int32 _intperiodopago;

        private Int16 _intidmonedanacional;
        private FinanciamientoTipoOperacionCobranza _enumtipooperacion;
        private Int32 _decsegundosatencionusuario;
        private DateTime _datfechainiciocobranzalog;
        private Int32 _intidchecksum;
        private Int16 _intidtipodocumento;
        private Int16 _intidmovcomercial = 14;

        private clsFinanciamientoUsuarioPuntoAtencion _objusuariopuntoatencion;

        private List<clsFinanciamientoDocumentosDescargoBase> _elementosdocumentodescargo = new List<clsFinanciamientoDocumentosDescargoBase>();
        private List<clsFinanciamientoMediosPagoBase> _elementosmediopago = new List<clsFinanciamientoMediosPagoBase>();
        private List<clsFinanciamientoCajaEstadisticaDiarioMoneda> _elementoscajamoneda = new List<clsFinanciamientoCajaEstadisticaDiarioMoneda>();
        private List<clsFinanciamientoCajaEstadisticaServicio> _elementocajaestadisticaservicio = new List<clsFinanciamientoCajaEstadisticaServicio>();
        private clsListaFinanciamientoDocumentosAFavor _listadocumentoafavor = new clsListaFinanciamientoDocumentosAFavor();
        private clsListaFinanciamientoIngresoProvision _listaingresoprovision = new clsListaFinanciamientoIngresoProvision();

        //Agregado por Carlos López.
        //Se agregó este campo para generar el convenio en la tabla del esquema DBO, considerando la Orden de Cobro
        //Se agregó el campo IdSolicitud para actualizar los datos en la tabla Sol.SolicitudServicio, sin embargo
        //no siempre este dato tendra valor.
        //private Int32? _intidordencobro;

        private Int32? _intidsolicitud;

        // EGM. Para el compromiso de la cuota inicial.
        private Int32 _intidcompromiso = 0;

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Nro de Compromiso de la Cuota Inicial.
        /// </summary>
        public Int32 IdCompromiso
        {
            get { return _intidcompromiso; }
            set { _intidcompromiso = value; }
        }

        public Int32 NroTransaccion
        {
            get { return _intnrotransaccion; }
            set { _intnrotransaccion = value; }
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int32 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        public DateTime FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int32 PeriodoPago
        {
            get { return _intperiodopago; }
            set { _intperiodopago = value; }
        }

        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public Int16 IdMovComercial
        {
            get { return _intidmovcomercial; }
            set { _intidmovcomercial = value; }
        }

        public Decimal ImporteRecibido
        {
            get { return _decimporterecibido; }
            set { _decimporterecibido = value; }
        }

        public Decimal ImporteNeto
        {
            get { return _decimporteneto; }
            set { _decimporteneto = value; }
        }

        public Decimal ImporteTotalVuelto
        {
            get { return _decimportetotalvuelto; }
            set { _decimportetotalvuelto = value; }
        }

        public Decimal ImporteTotalVueltoSinDevolver
        {
            get { return _decimportetotalvueltosindevolver; }
            set { _decimportetotalvueltosindevolver = value; }
        }

        public Decimal ImporteEfectivo
        {
            get { return _decimporteefectivo; }
            set { _decimporteefectivo = value; }
        }

        public Decimal ImporteValores
        {
            get { return _decimportevalores; }
            set { _decimportevalores = value; }
        }

        public Decimal ImporteCheques //mod
        {
            get { return _decimportecheques; }
            set { _decimportecheques = value; }
        }

        public DateTime FechaInicioCobranzaLog
        {
            get { return _datfechainiciocobranzalog; }
            set { _datfechainiciocobranzalog = value; }
        }

        public Int32 SegundosAtencionUsuario
        {
            get { return _decsegundosatencionusuario; }
            set { _decsegundosatencionusuario = value; }
        }

        public Int16 IdMonedaNacional
        {
            get { return _intidmonedanacional; }
            set { _intidmonedanacional = value; }
        }

        public clsFinanciamientoUsuarioPuntoAtencion UsuarioPuntoAtencion
        {
            get { return _objusuariopuntoatencion; }
            set { _objusuariopuntoatencion = value; }
        }

        public Int32 IdCheckSum
        {
            get { return _intidchecksum; }
            set { _intidchecksum = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }
        /// <summary>
        /// Determina si es Operacion de Cobranza o Operacion de Extorno.
        /// Los Identificadores obtengalos de clsRecurso. Por ejemplo clsRecurso.IdOperacionCobranza
        /// </summary>
        public FinanciamientoTipoOperacionCobranza TipoOperacion
        {
            get { return _enumtipooperacion; }
            set { _enumtipooperacion = value; }
        }

        /// <summary>
        /// Propiedad agregado para generar el convenio en el esquema DBO.
        /// </summary>
        //public Int32? IdOrdenCobro
        //{
        //    get { return _intidordencobro; }
        //    set { _intidordencobro = value; }
        //}

        /// <summary>
        /// Propiedad agregado para actualizar los datos del documento generado en la Solicitud.
        /// </summary>
        public Int32? IdSolicitud
        {
            get { return _intidsolicitud; }
            set { _intidsolicitud = value; }
        }

        #endregion Propiedades

        #region Elementos

        public List<clsFinanciamientoDocumentosDescargoBase> ElementosDocumentoDescargo
        {
            get { return _elementosdocumentodescargo; }
            set { _elementosdocumentodescargo = value; }
        }

        public List<clsFinanciamientoMediosPagoBase> ElementosMedioPago
        {
            get { return _elementosmediopago; }
            set { _elementosmediopago = value; }
        }

        public List<clsFinanciamientoCajaEstadisticaDiarioMoneda> ElementosCajaServicioMoneda
        {
            get { return _elementoscajamoneda; }
            set { _elementoscajamoneda = value; }
        }

        public List<clsFinanciamientoCajaEstadisticaServicio> ElementoCajaServicio
        {
            get { return _elementocajaestadisticaservicio; }
            set { _elementocajaestadisticaservicio = value; }
        }

        /// <summary>
        /// Todos estos datos seran Creado en la Tabla Documentos_a_favor.
        /// </summary>
        public clsListaFinanciamientoDocumentosAFavor ListaDocumentoAfavor
        {
            get { return _listadocumentoafavor; }
            set { _listadocumentoafavor = value; }
        }

        /// <summary>
        /// Todos estos datos seran Creado en la Tabla IngresoProvision, cuando se llame a la cobranza.
        /// Por Ejemplo: Crear Deposito Garantia
        /// </summary>
        public clsListaFinanciamientoIngresoProvision ListaIngresoProvision
        {
            get { return _listaingresoprovision; }
            set { _listaingresoprovision = value; }
        }
        public clsListaIngresoProvision ListaFinanciamientoIngresoProvision { get; set; }

        #endregion Elementos

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        public clsFinanciamientoTransaccion()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tipooperacion"></param>
        public clsFinanciamientoTransaccion(FinanciamientoTipoOperacionCobranza tipooperacion)
        {
            this.TipoOperacion = tipooperacion;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        /// <param name="tipooperacion"></param>
        public clsFinanciamientoTransaccion(DataRow registro, FinanciamientoTipoOperacionCobranza tipooperacion)
        {
            this.NroTransaccion = Convert.ToInt32(registro["nrotransaccion"]);
            this.FechaPago = Convert.ToDateTime(registro["fechapago"]);
            this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            this.ImporteNeto = Convert.ToDecimal(registro["importe"]);
            this.ImporteRecibido = Convert.ToDecimal(registro["importerecibido"]);
            this.ImporteTotalVuelto = Convert.ToDecimal(registro["vuelto"]);
            if (registro.Table.Columns.Contains("idchecksum")) { IdCheckSum = Convert.ToInt32(registro["idchecksum"]); }
            if (registro.Table.Columns.Contains("fecharegistro"))
                this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            this.TipoOperacion = tipooperacion;
        }

        #endregion Constructor
    }

    #endregion Transaccion - V2

    #region CajaEstadisticaServicio

    public class clsFinanciamientoCajaEstadisticaServicio
    {
        private Int32 _intidtransaccionlog;
        private Int32 _intidnrocaja;
        private Int16 _intidservicio;
        private Int16 _intcantidadnroservicio;
        private Int16 _intcantidaddocumentos;

        public clsFinanciamientoCajaEstadisticaServicio(Int32 idtransaccionlog, Int32 idnrocaja, Int16 idservicio, Int16 cantidadnroservicio, Int16 cantidaddocumentos)
        {
            this.IdTransaccionLog = idtransaccionlog;
            this.IdNroCaja = idnrocaja;
            this.IdServicio = idservicio;
            this.CantidadNroServicio = cantidadnroservicio;
            this.CantidadDocumentos = cantidaddocumentos;
        }

        public clsFinanciamientoCajaEstadisticaServicio()
        {
        }

        public Int32 IdTransaccionLog
        {
            get { return _intidtransaccionlog; }
            set { _intidtransaccionlog = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Int16 CantidadNroServicio
        {
            get { return _intcantidadnroservicio; }
            set { _intcantidadnroservicio = value; }
        }

        public Int16 CantidadDocumentos
        {
            get { return _intcantidaddocumentos; }
            set { _intcantidaddocumentos = value; }
        }
    }

    #endregion CajaEstadisticaServicio

    #region clsIngresoProvisionDetalle

    public class clsFinanciamientoIngresoProvisionDetalle
    {
        private int intidConcepto;
        private decimal decImporte;
        private int intCRC;
        private decimal decCantidad;
        private decimal decPrecioUnitario;
        private decimal? decPrecioUnitarioSinFose;
        private Int16 _intidempresa;
        private short intidOrdenCobro;
        private short intidOrdenCobroSustento;
        private string stridOrdenCobroDocumento;
        private string stridOrdenCobroDocumentoSustento;
        private Int32 _intidnroservicio;
        private Int16 _intindicadorigv;
        private Int16 _intcodigoenlace;

        public clsFinanciamientoIngresoProvisionDetalle(Int16 idempresa, short _idOrdenCobro
            , string _IdOrdenCobroDocumento, short _idOrdenCobroSustento
            , string _IdOrdenCobroDocumentoSustento, int _idConcepto
            , decimal _Importe, int _CRC, decimal _Cantidad
            , decimal _PrecioUnitario, decimal? _PrecioUnitarioSinFose
            , Int32 _IdNroServicio, Int16 _IndicadorIGV)
        {
            this.IdEmpresa = idempresa;
            this.IdOrdenCobro = _idOrdenCobro;
            this.IdOrdenCobroDocumento = _IdOrdenCobroDocumento;
            this.IdOrdenCobroSustento = _idOrdenCobroSustento;
            this.IdOrdenCobroDocumentoSustento = _IdOrdenCobroDocumentoSustento;
            this.idConcepto = _idConcepto;
            this.Importe = _Importe;
            this.CRC = _CRC;
            this.Cantidad = _Cantidad;
            this.PrecioUnitario = _PrecioUnitario;
            this.PrecioUnitarioSinFose = _PrecioUnitarioSinFose;
            this.IdNroServicio = _IdNroServicio;
            this.IndicadorIGV = _IndicadorIGV;
        }

        public clsFinanciamientoIngresoProvisionDetalle(DataRow drregistro)
        {
            this.IdEmpresa = Convert.ToInt16(drregistro["idempresa"]);
            this.IdOrdenCobro = Convert.ToInt16(drregistro["idordencobro"]);
            this.IdOrdenCobroDocumento = drregistro["idordencobrodocumento"].ToString();
            this.IdOrdenCobroSustento = Convert.ToInt16(drregistro["idordendecobrosustento"]);
            this.IdOrdenCobroDocumentoSustento = drregistro["idordendecobrodocumentosustento"].ToString();
            this.idConcepto = Convert.ToInt32(drregistro["idconcepto"]);
            this.Importe = Convert.ToDecimal(drregistro["importe"]);
            this.CRC = Convert.ToInt32(drregistro["crc"]);
            this.Cantidad = Convert.ToDecimal(drregistro["cantidad"]);
            this.PrecioUnitario = Convert.ToDecimal(drregistro["preciounitario"]);
            this.PrecioUnitarioSinFose = drregistro.Table.Columns.Contains("preciounitariosinfose") ? (Convert.IsDBNull(drregistro["preciounitariosinfose"]) ? (Decimal?)null : Convert.ToDecimal(drregistro["preciounitariosinfose"])) : (Decimal?)null;
            this.IdNroServicio = Convert.ToInt32(drregistro["idnroservicio"]);
            this.IndicadorIGV = Convert.ToInt16(drregistro["indicadorigv"]);
        }

        public clsFinanciamientoIngresoProvisionDetalle()
        {
        }

        public int idConcepto
        {
            get { return intidConcepto; }
            set { intidConcepto = value; }
        }

        public decimal Importe
        {
            get { return decImporte; }
            set { decImporte = value; }
        }

        public int CRC
        {
            get { return intCRC; }
            set { intCRC = value; }
        }

        public decimal Cantidad
        {
            get { return decCantidad; }
            set { decCantidad = value; }
        }

        public decimal PrecioUnitario
        {
            get { return decPrecioUnitario; }
            set { decPrecioUnitario = value; }
        }

        public decimal? PrecioUnitarioSinFose
        {
            get { return decPrecioUnitarioSinFose; }
            set { decPrecioUnitarioSinFose = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public short IdOrdenCobro
        {
            get { return intidOrdenCobro; }
            set { intidOrdenCobro = value; }
        }

        public short IdOrdenCobroSustento
        {
            get { return intidOrdenCobroSustento; }
            set { intidOrdenCobroSustento = value; }
        }

        public string IdOrdenCobroDocumento
        {
            get { return stridOrdenCobroDocumento; }
            set { stridOrdenCobroDocumento = value; }
        }

        public string IdOrdenCobroDocumentoSustento
        {
            get { return stridOrdenCobroDocumentoSustento; }
            set { stridOrdenCobroDocumentoSustento = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public Int16 IndicadorIGV
        {
            get { return _intindicadorigv; }
            set { _intindicadorigv = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }
    }

    #endregion clsIngresoProvisionDetalle

    #region clsIngresoProvisionReferencias

    public class clsFinanciamientoIngresoProvisionReferencias
    {
        #region Campos

        private Int16 _intidempresa;
        private Int16 _intidordencobro;
        private String _stridordencobrodocumento;
        private Int16 _intidempresareferenciado;
        private Int16 _intidordencobroreferenciado;
        private String _stridordencobrodocumentoreferenciado;
        private DateTime _datfechareferencia;
        private Int16 _intidestado;
        private Int16 _intcodigoenlace;

        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String IdOrdenCobroDocumento
        {
            get { return _stridordencobrodocumento; }
            set { _stridordencobrodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresaReferenciado
        {
            get { return _intidempresareferenciado; }
            set { _intidempresareferenciado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdOrdenCobroReferenciado
        {
            get { return _intidordencobroreferenciado; }
            set { _intidordencobroreferenciado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String IdOrdenCobroDocumentoReferenciado
        {
            get { return _stridordencobrodocumentoreferenciado; }
            set { _stridordencobrodocumentoreferenciado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaReferencia
        {
            get { return _datfechareferencia; }
            set { _datfechareferencia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsFinanciamientoIngresoProvisionReferencias()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsFinanciamientoIngresoProvisionReferencias(DataRow dr)
        {
            if (dr == null) { return; }

            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdOrdenCobro = Convert.ToInt16(dr["idordencobro"]);
            IdOrdenCobroDocumento = dr["idordencobrodocumento"].ToString();
            IdEmpresaReferenciado = Convert.ToInt16(dr["idempresareferenciado"]);
            IdOrdenCobroReferenciado = Convert.ToInt16(dr["idordencobroreferenciado"]);
            IdOrdenCobroDocumentoReferenciado = dr["idordencobrodocumentoreferenciado"].ToString();
            FechaReferencia = Convert.ToDateTime(dr["fechareferencia"]);
            IdEstado = Convert.ToInt16(dr["idestado"]);
        }

        #endregion Constructor
    }

    #endregion clsIngresoProvisionReferencias

    #region clsIngresoProvision

    public class clsFinanciamientoIngresoProvision : clsFinanciamientoIngresoProvisionBase
    {
        #region Campos

        private String _strserie;
        private Decimal _decimportetotal;
        private Decimal _decmontodescargado;
        private Decimal _decsaldo;

        //private Decimal _decimportecobrar;
        private Int32 _intlotefacturacion;

        private Int16 _intidtipoatencion;
        private Int16 _intidpuntoatencion;
        private Int16 _intnromesesdeuda;
        private Decimal _decdeudaanteriorinicial;
        private Int16 _intidcentroservicio;
        private Int16 _intidmovimientocomercial;

        private Decimal _decimportedescargadoorigen;
        private Decimal _decimportecontrolsaldoorigen;
        private Boolean _booexcluirvalidacioninterna;

        private Int32? _intperiodopago;
        private DateTime? _datfechapago;

        private System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionDocumentos> _objlistaingresoprovisiondocumentos = new System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionDocumentos>();
        private System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionDetalle> _objlistaingresoprovisiondetalles = new System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionDetalle>();
        private System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionReferencias> _objlistaingresoprovisionreferencias = new System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionReferencias>();

        private clsListaFinanciamientoAsientoDocumentoValida _objlistaasientodocumentovalida;
        private Int32 _intidreintegro;
        private Int16 _intidformapagocobro; //Para el proceso de Reintegro y Recuperos
        private Int16 _intcodigoenlace;

        #endregion Campos

        #region Contructor

        public clsFinanciamientoIngresoProvision(DataRow registro)
        {
            if (registro == null) { return; }
            AsignarRegistro(registro);
        }

        public clsFinanciamientoIngresoProvision(DataRow registro, DataRow[] detalle)
        {
            if (registro == null) { return; }
            _booexcluirvalidacioninterna = true;

            AsignarRegistro(registro);

            if (detalle != null)
            {
                foreach (DataRow _drw in detalle)
                {
                    if (Convert.ToInt16(_drw["indicadorreclamo"]) == 1) { IndicadorReclamo = true; }
                    _objlistaingresoprovisiondocumentos.Add(new clsFinanciamientoIngresoProvisionDocumentos(_drw));
                }
            }

            _booexcluirvalidacioninterna = false;
        }

        private void AsignarRegistro(DataRow registro)
        {
            this.Sel = (Boolean)registro["sel"];
            this.IdEmpresa = (Int16)registro["idempresa"];
            this.IdOrdenCobro = (Int16)registro["idordencobro"];
            this.IdOrdenCobroDocumento = registro["idordencobrodocumento"].ToString();
            this.Serie = registro["serie"].ToString();
            this.NombreCliente = registro["nombrecliente"].ToString();
            this.Direccion = registro["direccion"].ToString();
            this.IdTipoIdentidad = (Int16)registro["idtipoidentidad"];
            this.NroIdentidad = registro["nroidentidad"].ToString();

            this.FechaRegistro = (DateTime)registro["fecharegistro"];
            this.FechaEmision = (DateTime)registro["fechaemision"];
            this.FechaVencimiento = (DateTime)registro["fechavencimiento"];
            //VAS
            this.LoteFacturacion = Convert.IsDBNull(registro["lotefacturacion"]) ? 0 : (Int32)registro["lotefacturacion"];

            this.IdEstadoDeuda = (Int16)registro["estadodeuda"];
            this.IdTipoAtencion = (Int16)registro["idtipoatencion"];
            this.IdPuntoAtencion = (Int16)registro["idpuntoatencion"];
            this.IdUsuario = (Int32)registro["idusuario"];
            this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            this.IdNroServicioPrincipal = (Int32)registro["idnroservicioprincipal"];

            this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            this.ImporteOrigenTotal = (Decimal)registro["importetotal"];
            this.ImporteOrigenDescargo = (Decimal)registro["montodescargado"];
            this.ImporteOrigenSaldo = (Decimal)registro["saldo"];
            this.ImporteOrigenCobrar = (Decimal)registro["saldo"];
            //VAS
            this.ImporteReclamo = (Decimal)registro["importeenreclamo"];
            this.IndicadorReclamo = Convert.ToBoolean(registro["indicadorreclamo"]);

            if (registro.Table.Columns.Contains("estadosuministro"))
                this.EstadoSuministro = Convert.ToInt16(registro["estadosuministro"]);
            if (registro.Table.Columns.Contains("idanocomercial"))
                this.IdAnoComercial = Convert.ToInt16(registro["idanocomercial"]);
            if (registro.Table.Columns.Contains("idmescomercial"))
                this.IdMesComercial = Convert.ToInt16(registro["idmescomercial"]);
            if (this.IdAnoComercial > 0 && this.IdMesComercial > 0)
                this.Periodo = (IdAnoComercial * 100) + IdMesComercial;

            if (registro.Table.Columns.Contains("periodopago"))
                this.PeriodoPago = Convert.IsDBNull(registro["periodopago"]) ? (Int32?)null : Convert.ToInt32(registro["periodopago"]);
            if (registro.Table.Columns.Contains("fechapago"))
                this.FechaPago = Convert.IsDBNull(registro["fechapago"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechapago"]);

            if (registro.Table.Columns.Contains("importepagofijo"))
                this.ImportePagoFijo = Convert.IsDBNull(registro["importepagofijo"]) ? (Decimal)0 : Convert.ToDecimal(registro["importepagofijo"]);
        }

        public clsFinanciamientoIngresoProvision(Int16 idempresa, Int16 idordencobro, String idordencobrodocumento, String serie, String nombrecliente, Int16 idtipoidentidad
                                 , String nroidentidad, Int16 idmoneda, Decimal importetotal, Decimal montodescargado, Decimal saldo
                                 , DateTime fecharegistro, DateTime fechaemision, DateTime fechavencimiento, Int32 lotefacturacion, Int16 idestadodeuda
                                 , Int16 idtipoatencion, Int16 idpuntoatencion, Int32 idusuario, Int16 idservicioprincipal, Int32 idnroservicioprincipal
                                 , Boolean? escobranzaterceros)
        {
            this.IdEmpresa = idempresa;
            this.IdOrdenCobro = idordencobro;
            this.IdOrdenCobroDocumento = idordencobrodocumento;
            this.Serie = serie;
            this.NombreCliente = nombrecliente;
            this.IdTipoIdentidad = idtipoidentidad;
            this.NroIdentidad = nroidentidad;
            this.IdMonedaCambio = idmoneda;
            this.ImporteOrigenTotal = importetotal;
            this.ImporteOrigenDescargo = montodescargado;
            this.ImporteOrigenSaldo = saldo;
            this.ImporteOrigenCobrar = saldo;
            this.FechaRegistro = fecharegistro;
            this.FechaEmision = fechaemision;
            this.FechaVencimiento = fechavencimiento;
            this.LoteFacturacion = lotefacturacion;
            this.IdEstadoDeuda = idestadodeuda;
            this.IdTipoAtencion = idtipoatencion;
            this.IdPuntoAtencion = idpuntoatencion;
            this.IdUsuario = idusuario;
            this.IdServicioPrincipal = idservicioprincipal;
            this.IdNroServicioPrincipal = idnroservicioprincipal;
        }

        public clsFinanciamientoIngresoProvision()
        {
        }

        #endregion Contructor

        #region Propiedades - Generales

        public String Serie
        {
            get { return _strserie; }
            set { _strserie = value; }
        }

        public Int32 LoteFacturacion
        {
            get { return _intlotefacturacion; }
            set { _intlotefacturacion = value; }
        }

        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        public Int16 NroMesesDeuda
        {
            get { return _intnromesesdeuda; }
            set { _intnromesesdeuda = value; }
        }

        public Decimal DeudaAnteriorInicial
        {
            get { return _decdeudaanteriorinicial; }
            set { _decdeudaanteriorinicial = value; }
        }

        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        public Int16 IdMovimientoComercial
        {
            get { return _intidmovimientocomercial; }
            set { _intidmovimientocomercial = value; }
        }

        public Int32? PeriodoPago
        {
            get { return _intperiodopago; }
            set { _intperiodopago = value; }
        }

        public DateTime? FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int32 IdReintegro
        {
            get { return _intidreintegro; }
            set { _intidreintegro = value; }
        }

        public Int16 IdFormaPagoCobro
        {
            get { return _intidformapagocobro; }
            set { _intidformapagocobro = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades - Generales

        #region Propiedades - Redefinidas por Herencia

        public Boolean Sel
        {
            get { return _boosel; }
            set
            {
                //Si esta Pagado NO Seleccionar
                if ((Estado)IdEstadoDeuda == Estado.Pagado) { _boosel = false; return; }

                //Si Deselecciono y Tipo Cambio y ImporteCambioSaldo es Mayor que Cero
                if ((!value) || (TipoCambio > 0 && ImporteCambioSaldo > 0))
                { _boosel = value; return; }
                //Si Excluye validación interna
                if (_booexcluirvalidacioninterna || IdMonedaOrigen == 0) { _boosel = value; return; }

                _boosel = false;
                StringBuilder _sbmensaje = new StringBuilder();

                if (TipoCambio == 0)
                {
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("!Ingrese Tipo Cambio!");
                }

                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("No puede seleccionar documento = " + _intidordencobro.ToString() + " - " + _stridordencobrodocumento);
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("Importe de Tipo Cambio  = " + TipoCambio.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Importe Saldo de Cambio = " + ImporteCambioSaldo.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Moneda de cambio        = " + IdMonedaCambio.ToString());
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("Importe Total Origen  = " + ImporteOrigenTotal.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Importe Saldo Origen  = " + ImporteOrigenSaldo.ToString("###,###,##0.00"));
                _sbmensaje.AppendLine("Moneda Origen         = " + IdMonedaOrigen.ToString());
                _sbmensaje.AppendLine("");
                _sbmensaje.AppendLine("");

                throw new Exception(_sbmensaje.ToString());
            }
        }

        public override Decimal ImporteOrigenCobrar
        {
            get { return _decimportecobrar; }
            set
            {
                Decimal _decimporteacobrar = value;
                if (_decimporteacobrar > ImporteCambioSaldo && ImporteCambioSaldo != 0)
                {
                    StringBuilder _sbmensaje = new StringBuilder();
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    _sbmensaje.Append("Saldo a Cobrar ");
                    _sbmensaje.Append(_decimporteacobrar.ToString());
                    _sbmensaje.Append(" no puede ser mayor a Saldo Original de ");
                    _sbmensaje.Append(ImporteCambioSaldo.ToString());
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    throw new Exception(_sbmensaje.ToString());
                }
                Decimal _decimportecobratmp = _decimporteacobrar;
                Boolean _booCobroTotal = (_decimporteacobrar == ImporteCambioSaldo);

                _decimportecobrar = _decimporteacobrar;
                if (!AplicarEnHijos) { return; }

                //VAS
                Boolean _boolReclamo = false;
                foreach (clsFinanciamientoIngresoProvisionDocumentos itemingresoprovisiondocumentos in ListaIngresoProvisionDocumentos)
                {
                    //VAS - si el indicador de reclamo es 1 no se toma encuenta este documento en la deuda y no se
                    // incluye en la distribucion del monto a cobrar
                    if (itemingresoprovisiondocumentos.FechaLimitePago != null)
                        _boolReclamo = (itemingresoprovisiondocumentos.IndicadorReclamo == 1 &&
                            itemingresoprovisiondocumentos.FechaLimitePago > this.FechaServidor);

                    if (itemingresoprovisiondocumentos.IndicadorReclamo == 0 || _boolReclamo)
                    {
                        //Si se distribuye saldo entonces marcar por defecto la selección.
                        //ya que si es cero, no se tomara en cuenta para la cobranza
                        itemingresoprovisiondocumentos.Sel = true;
                        //Si se Cobra Todo El Importe Entonces restaurar los saldos
                        if (_booCobroTotal)
                        {
                            itemingresoprovisiondocumentos.Saldo = itemingresoprovisiondocumentos.SaldoOriginal;
                            continue;
                        }
                        //Si el importe a cobrar es menor entonces se distribuye hasta alcanzar el tope de cobro parcial
                        //1. Si importe a cobrar llego a cero entonces asignar cobro cero a todos los demás items
                        if (_decimportecobratmp == 0 || itemingresoprovisiondocumentos.SaldoOriginal < 0)
                        {
                            itemingresoprovisiondocumentos.Saldo = 0;
                            continue;
                        }
                        //2. Si el importe a cobrar es mayor a saldo original
                        if (_decimportecobratmp > itemingresoprovisiondocumentos.SaldoOriginal)
                        {
                            itemingresoprovisiondocumentos.Saldo = itemingresoprovisiondocumentos.SaldoOriginal;
                            _decimportecobratmp -= itemingresoprovisiondocumentos.SaldoOriginal; //_decimportecobratmp - itemingresoprovisiondocumentos.SaldoOriginal;
                        }
                        else
                        {
                            itemingresoprovisiondocumentos.Saldo = _decimportecobratmp;
                            _decimportecobratmp = 0;// _decimportecobratmp - _decimportecobratmp;
                        }
                    }
                }
            }
        }

        public override Int32 NroItems
        {
            get { return ListaIngresoProvisionDocumentos.Count; }
        }

        #endregion Propiedades - Redefinidas por Herencia

        #region Propiedades Listas

        public System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionDocumentos> ListaIngresoProvisionDocumentos
        {
            get { return _objlistaingresoprovisiondocumentos; }
            set { _objlistaingresoprovisiondocumentos = value; }
        }

        public System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionDetalle> ListaIngresoProvisionDetalles
        {
            get { return _objlistaingresoprovisiondetalles; }
            set { _objlistaingresoprovisiondetalles = value; }
        }

        public System.ComponentModel.BindingList<clsFinanciamientoIngresoProvisionReferencias> ListaIngresoProvisionReferencias
        {
            get { return _objlistaingresoprovisionreferencias; }
            set { _objlistaingresoprovisionreferencias = value; }
        }

        public clsListaFinanciamientoAsientoDocumentoValida ListaAsientoDocumentoValida
        {
            get { return _objlistaasientodocumentovalida; }
            set { _objlistaasientodocumentovalida = value; }
        }

        #endregion Propiedades Listas
    }

    #endregion clsIngresoProvision

    #region clsDocumentosAFavor

    public class clsFinanciamientoDocumentosAFavor
    {
        #region Campos
        private Int16 _intidempresa;
        private String _stridordencobrodocumentoafavor;
        private Int16 _intidordencobroafavor;
        private Int16 _intidmoneda;
        private Decimal _decimporte;
        private Decimal _decsaldoafavor;
        private DateTime _datfecharegistro;
        private DateTime? _datfechavencimiento;
        private String _strdocumentoreferencia;
        private Int16 _intidpuntoatencion;
        private Int16 _intidtipoatencion;
        private Int32 _stridusuario;
        private Int32 _intidusuarioautoriza;
        private Int32 _intidnroservicio;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private Int32 _intidnrocaja;
        private Int16 _intmotivo;
        private String _strdescripcion;
        private Int16 _intestadoafavor;
        private Int16 _intidordencobro;
        private String _stridordencobrodocumento;
        private String _strmonedasimbolo;
        private String _strnombrecliente;
        private String _strdireccioncliente;
        private Int16 _intidmovimientocomercial;
        private Int16 _intidservicio;
        private Decimal _dectipocambio;
        private String _strnombredocumento;
        private String _strNombreCentroServicio;
        private String _strNombreUnidadNegocio;
        private String _strNombreEmpresa;
        private Decimal _dectasaigv;

        //add 07/03/2013
        private Int16 _intdevolvercliente;

        private Int16 _intcodigoenlace;

        #endregion Campos

        #region Constructor

        public clsFinanciamientoDocumentosAFavor
            (String idordencobrodocumentoafavor
            , Int16 idordencobroafavor
            , Decimal saldoafavor
            , Decimal importe
            , Int16 idestadoafavor
            , DateTime fecharegistro
            , String documentoreferencia
            , Int16 idmoneda
            , Int16 idpuntoatencion
            , Int16 idordencobro
            , String idordencobrodocumento
            , Int16 idtipoatencion
            , Int32 idusuario
            , Int16 idtipoidentidad
            , String nroidentidad
            , Int32 idnroservicio
            , String monedasimbolo
            , Int16 idempresa)
        {
            this.IdOrdenCobroDocumentoAFavor = idordencobrodocumentoafavor;
            this.IdOrdenCobroAFavor = idordencobroafavor;
            this.SaldoaFavor = saldoafavor;
            this.Importe = importe;
            this.IdEstadoaFavor = idestadoafavor;
            this.FechaRegistro = fecharegistro;
            this.DocumentoReferencia = documentoreferencia;
            this.IdMonedaOrigen = idmoneda;
            this.IdPuntoAtencion = idpuntoatencion;
            this.IdOrdenCobro = idordencobro;
            this.IdOrdenCobroDocumento = idordencobrodocumento;
            this.IdTipoAtencion = idtipoatencion;
            this.IdUsuario = idusuario;
            this.IdTipoIdentidad = idtipoidentidad;
            this.NroIdentidad = nroidentidad;
            this.IdNroServicio = idnroservicio;
            this.MonedaSimbolo = monedasimbolo;
            this.IdEmpresa = idempresa;
        }

        public clsFinanciamientoDocumentosAFavor(DataRow registro)
        {
            this.IdEmpresa = (Int16)registro["idempresa"];
            this.IdOrdenCobroDocumentoAFavor = registro["idordencobrodocumentoafavor"].ToString();
            this.IdOrdenCobroAFavor = (Int16)registro["idordencobroafavor"];
            this.SaldoaFavor = (Decimal)registro["saldoafavor"];
            this.Importe = (Decimal)registro["importe"];
            this.IdEstadoaFavor = (Int16)registro["estadoafavor"];
            this.FechaRegistro = (DateTime)registro["fecharegistro"];
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.IsDBNull(registro["fechavencimiento"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("documentoreferencia")) this.DocumentoReferencia = registro["documentoreferencia"].ToString();
            if (registro.Table.Columns.Contains("idmoneda")) this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("idpuntoatencion")) this.IdPuntoAtencion = (Int16)registro["idpuntoatencion"];
            if (registro.Table.Columns.Contains("idordencobro")) this.IdOrdenCobro = (Int16)registro["idordencobro"];
            if (registro.Table.Columns.Contains("idtipoatencion")) this.IdTipoAtencion = (Int16)registro["idtipoatencion"];
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (registro.Table.Columns.Contains("idtipoidentidad")) this.IdTipoIdentidad = (Int16)registro["idtipoidentidad"];
            if (registro.Table.Columns.Contains("nroidentidad")) this.NroIdentidad = registro["nroidentidad"].ToString();
            if (registro.Table.Columns.Contains("idmotivo")) this.IdMotivoConstitucion = (Int16)registro["idmotivo"];
            if (registro.Table.Columns.Contains("descripcion")) this.Descripcion = registro["descripcion"].ToString().Trim();
            if (registro.Table.Columns.Contains("simbolomoneda")) this.MonedaSimbolo = registro["simbolomoneda"].ToString();
            if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = registro["nombrecliente"].ToString().Trim();
            if (registro.Table.Columns.Contains("direccion")) this.DireccionCliente = registro["direccion"].ToString().Trim();
            if (registro.Table.Columns.Contains("nombretipodocumento")) this.NombreTipoDocumento = registro["nombretipodocumento"].ToString().Trim();
            this.IdNroServicio = registro.IsNull("idnroservicio") ? 0 : (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("NombreEmpresa")) this.NombreEmpresa = registro["NombreEmpresa"].ToString().Trim();
            if (registro.Table.Columns.Contains("NombreUnidadNegocio")) this.NombreUnidadNegocio = registro["NombreUnidadNegocio"].ToString().Trim();
            if (registro.Table.Columns.Contains("NombreCentroServicio")) this.NombreCentroServicio = registro["NombreCentroServicio"].ToString().Trim();

            if (registro.Table.Columns.Contains("devolvercliente")) this.DevolverCliente = (Int16)registro["devolvercliente"];
            //this.IdNroServicio = (Int32)registro["idnroservicio"];
        }

        public clsFinanciamientoDocumentosAFavor()
        {
        }

        #endregion Constructor

        #region Propiedades

        public String IdOrdenCobroDocumentoAFavor
        {
            get { return _stridordencobrodocumentoafavor; }
            set { _stridordencobrodocumentoafavor = value; }
        }

        public Int16 IdOrdenCobroAFavor
        {
            get { return _intidordencobroafavor; }
            set { _intidordencobroafavor = value; }
        }

        public Decimal SaldoaFavor
        {
            get { return _decsaldoafavor; }
            set { _decsaldoafavor = value; }
        }

        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        public Int16 IdEstadoaFavor
        {
            get { return _intestadoafavor; }
            set { _intestadoafavor = value; }
        }

        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        public DateTime? FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        public String DocumentoReferencia
        {
            get { return _strdocumentoreferencia; }
            set { _strdocumentoreferencia = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public String MonedaSimbolo
        {
            get { return _strmonedasimbolo; }
            set { _strmonedasimbolo = value; }
        }

        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        public Int16 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        public String IdOrdenCobroDocumento
        {
            get { return _stridordencobrodocumento; }
            set { _stridordencobrodocumento = value; }
        }

        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        public Int32 IdUsuario
        {
            get { return _stridusuario; }
            set { _stridusuario = value; }
        }

        public Int32 IdUsuarioAutoriza
        {
            get { return _intidusuarioautoriza; }
            set { _intidusuarioautoriza = value; }
        }

        public Int16 IdMotivoConstitucion
        {
            get { return _intmotivo; }
            set { _intmotivo = value; }
        }

        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }

        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }

        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set { _intidnrocaja = value; }
        }

        public Boolean Interno
        { get { return true; } }

        public Int16 IdBanco
        { get { return 0; } }

        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }

        public String DireccionCliente
        {
            get { return _strdireccioncliente; }
            set { _strdireccioncliente = value; }
        }

        public Int16 IdMovimientoComercial
        {
            get { return _intidmovimientocomercial; }
            set { _intidmovimientocomercial = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombredocumento; }
            set { _strnombredocumento = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public String NombreEmpresa
        {
            get { return _strNombreEmpresa; }
            set { _strNombreEmpresa = value; }
        }

        public String NombreUnidadNegocio
        {
            get { return _strNombreUnidadNegocio; }
            set { _strNombreUnidadNegocio = value; }
        }

        public String NombreCentroServicio
        {
            get { return _strNombreCentroServicio; }
            set { _strNombreCentroServicio = value; }
        }

        public Decimal TasaIGV
        {
            get { return _dectasaigv; }
            set { _dectasaigv = value; }
        }

        public Int16 DevolverCliente
        {
            get { return _intdevolvercliente; }
            set { _intdevolvercliente = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades

        #region ObtenerIngresoProvision

        public clsFinanciamientoIngresoProvision ObtenerIngresoProvision(Int16 idanocomercial, Int16 idmescomercial, Int16 idcentroservicio)
        {
            clsFinanciamientoIngresoProvision _objingresoprovision = new clsFinanciamientoIngresoProvision();

            _objingresoprovision.Sel = true;
            _objingresoprovision.FechaEmision = this.FechaRegistro;
            _objingresoprovision.FechaRegistro = this.FechaRegistro;
            _objingresoprovision.FechaVencimiento = this.FechaRegistro;

            _objingresoprovision.IdEstadoDeuda = (Int16)Estado.NoPagado;
            _objingresoprovision.IdOrdenCobro = this.IdOrdenCobroAFavor;
            _objingresoprovision.NroMesesDeuda = 0;

            _objingresoprovision.IdMonedaOrigen = this.IdMonedaOrigen;
            _objingresoprovision.IdTipoAtencion = this.IdTipoAtencion;
            _objingresoprovision.IdPuntoAtencion = this.IdPuntoAtencion;

            _objingresoprovision.IdTipoIdentidad = this.IdTipoIdentidad;
            _objingresoprovision.IdUsuario = this.IdUsuario;
            _objingresoprovision.ImporteOrigenTotal = this.Importe;
            _objingresoprovision.TipoCambio = this.TipoCambio;
            _objingresoprovision.NroIdentidad = this.NroIdentidad;
            _objingresoprovision.ImporteOrigenSaldo = this.Importe;
            _objingresoprovision.ImporteOrigenCobrar = this.Importe;

            _objingresoprovision.IdOrdenCobroDocumento = this.IdOrdenCobroDocumentoAFavor;
            String _stridordencobrodocumento = this.IdOrdenCobroDocumentoAFavor + "   ";
            _objingresoprovision.Serie = _stridordencobrodocumento.Substring(0, 3).Trim();
            _objingresoprovision.IdServicioPrincipal = this.IdServicio;
            _objingresoprovision.IdNroServicioPrincipal = this.IdNroServicio;

            _objingresoprovision.IdAnoComercial = idanocomercial;
            _objingresoprovision.IdCentroServicio = idcentroservicio;
            _objingresoprovision.IdMesComercial = idmescomercial;
            _objingresoprovision.IdMovimientoComercial = this.IdMovimientoComercial;
            _objingresoprovision.LoteFacturacion = 0;
            _objingresoprovision.NombreTipoDocumento = this.NombreTipoDocumento;

            _objingresoprovision.IdEmpresa = this.IdEmpresa;

            clsFinanciamientoIngresoProvisionDocumentos _objingresoprovisiondocumentos;
            // _objingresoprovisiondocumentos = new clsIngresoProvisionDocumentos(this.IdEmpresa, this.IdTipoDocumentoAFavor, this.NroDocumentoAFavor, _objingresoprovision.Serie, this.IdTipoDocumentoAFavor, this.NroDocumentoAFavor, this.IdMonedaOrigen, this.TipoCambio, this.Importe, this.Importe, this.Importe, (Int16)Estado.NoPagado, this.IdServicio, this.IdNroServicio, 0, 1, (Int16)Estado.Pendiente, 0, true);
            _objingresoprovisiondocumentos = new clsFinanciamientoIngresoProvisionDocumentos(this.IdEmpresa, this.IdOrdenCobroAFavor, this.IdOrdenCobroDocumentoAFavor, _objingresoprovision.Serie, this.IdOrdenCobroAFavor, this.IdOrdenCobroDocumentoAFavor, this.IdMonedaOrigen, this.TipoCambio, this.Importe, this.Importe, this.Importe, (Int16)Estado.NoPagado, this.IdServicio, this.IdNroServicio, 0, 1, (Int16)Estado.Normal, 0, true);
            _objingresoprovision.ListaIngresoProvisionDocumentos.Add(_objingresoprovisiondocumentos);

            return _objingresoprovision;
        }

        #endregion ObtenerIngresoProvision
    }

    #endregion clsDocumentosAFavor

    #region clsIngresoProvisionDocumentos

    public class clsFinanciamientoIngresoProvisionDocumentos
    {
        #region Campos

        private String _strserie;
        private String _stridordencobrodocumentosustento;

        private Int16 _intidestadodeuda;
        private Int16 _intidservicio;
        private Int32 _intidnroservicio;
        private Decimal _decimpuesto;
        private Int16 _intindicadordeuda;
        private Int16 _intidestadodocumento;
        private Int16 _intidempresa;
        private Int16 _intidordencobro;
        private String _stridordencobrodocumento;
        private Int16 _intidordencobrosustento;
        private Int16 _intindicadorreclamo;
        private Boolean _boosel;

        private Decimal _decimportetotal;
        private Decimal _decmontodescargado;
        private Decimal _decsaldo;
        private Decimal _decsaldooriginal;
        private Int16 _intidmoneda;

        private Decimal _decimportetotalorigen;
        private Decimal _decimportedescargadoorigen;
        private Decimal _decimportesaldoorigen;
        private Decimal _dectipocambio;
        private Int16 _intidmonedaorigen;
        private Int16 _intidescobrable;

        //VAS - Para reclamos
        private DateTime? _fecLimitePago;

        private Int32? _intperiodopago;
        private DateTime? _datfechapago;
        private Int16 _intcodigoenlace;

        #endregion Campos

        #region Constructor

        public clsFinanciamientoIngresoProvisionDocumentos(Int16 idempresa, Int16 idordencobro, String idordencobrodocumento, String serie, Int16 idordencobrosustento, String idordencobrodocumentosustento, Int16 idmoneda, Decimal tipocambio, Decimal importetotal, Decimal montodescargado, Decimal saldo, Int16 idestadodeuda, Int16 idservicio, Int32 idnroservicio, Decimal impuesto, Int16 indicadordeuda, Int16 idestadodocumento, Int16 indicadorreclamo, Boolean sel)
        {
            this.IdEmpresa = idempresa;
            this.IdOrdenCobro = idordencobro;
            this.IdOrdenCobroDocumento = idordencobrodocumento;
            this.Serie = serie;
            this.IdOrdenCobroSustento = idordencobrosustento;
            this.IdOrdenCobroDocumentoSustento = idordencobrodocumentosustento;

            this.ImporteTotal = importetotal;
            this.ImporteTotalOrigen = importetotal;
            this.ImporteDescargadoOrigen = montodescargado;
            this.SaldoOriginal = saldo;
            this.Saldo = saldo;
            this.IdMoneda = idmoneda;
            this.TipoCambio = tipocambio;
            this.IdEstadoDeuda = idestadodeuda;
            this.IdServicio = idservicio;
            this.IdNroServicio = idnroservicio;
            this.Impuesto = impuesto;
            this.IndicadorDeuda = indicadordeuda;
            this.IdEstadoDocumento = idestadodocumento;
            this.IndicadorReclamo = indicadorreclamo;
            this.Sel = sel;
        }

        public clsFinanciamientoIngresoProvisionDocumentos(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idempresa"))
            {
                this.IdEmpresa = (Int16)registro["idempresa"];
            }
            if (registro.Table.Columns.Contains("idordencobro"))
            {
                this.IdOrdenCobro = (Int16)registro["idordencobro"];
            }
            if (registro.Table.Columns.Contains("idordencobrodocumento"))
            {
                this.IdOrdenCobroDocumento = registro["idordencobrodocumento"].ToString();
            }
            if (registro.Table.Columns.Contains("serie"))
            {
                this.Serie = registro["serie"].ToString();
            }
            if (registro.Table.Columns.Contains("idordencobrosustento"))
            {
                this.IdOrdenCobroSustento = (Int16)registro["idordencobrosustento"];
            }
            if (registro.Table.Columns.Contains("nrodocumentosustento"))
            {
                this.IdOrdenCobroDocumentoSustento = registro["idordencobrodocumentosustento"].ToString();
            }
            if (registro.Table.Columns.Contains("estadodeuda"))
            {
                this.IdEstadoDeuda = (Int16)registro["estadodeuda"];
            }
            if (registro.Table.Columns.Contains("idservicio"))
            {
                this.IdServicio = (Int16)registro["idservicio"];
            }
            if (registro.Table.Columns.Contains("idnroservicio"))
            {
                this.IdNroServicio = (Int32)registro["idnroservicio"];
            }
            if (registro.Table.Columns.Contains("impuesto"))
            {
                this.Impuesto = (Decimal)registro["impuesto"];
            }
            if (registro.Table.Columns.Contains("indicadordeuda"))
            {
                this.IndicadorDeuda = (Int16)registro["indicadordeuda"];
            }
            if (registro.Table.Columns.Contains("estadodocumento"))
            {
                this.IdEstadoDocumento = (Int16)registro["estadodocumento"];
            }
            if (registro.Table.Columns.Contains("indicadorreclamo"))
            {
                this.IndicadorReclamo = (Int16)registro["indicadorreclamo"];
            }
            if (registro.Table.Columns.Contains("Sel"))
            {
                this.Sel = (Boolean)registro["Sel"];
            }
            if (registro.Table.Columns.Contains("FechaLimitePago"))
            {
                this.FechaLimitePago = registro["FechaLimitePago"] == DBNull.Value ? null : (DateTime?)registro["FechaLimitePago"];
            }
            if (registro.Table.Columns.Contains("importetotal"))
            {
                this.ImporteTotal = (Decimal)registro["importetotal"];
            }
            if (registro.Table.Columns.Contains("montodescargado"))
            {
                this.MontoDescargado = (Decimal)registro["montodescargado"];
            }
            if (registro.Table.Columns.Contains("saldo"))
            {
                this.SaldoOriginal = (Decimal)registro["saldo"];
                this.Saldo = (Decimal)registro["saldo"];
            }

            if (registro.Table.Columns.Contains("idmoneda"))
            { this.IdMoneda = Convert.ToInt16(registro["idmoneda"]); }

            if (registro.Table.Columns.Contains("importetotalorigen"))
            { this.ImporteTotalOrigen = Convert.ToDecimal(registro["importetotalorigen"]); }
            if (registro.Table.Columns.Contains("importedescargadoorigen"))
            { this.ImporteDescargadoOrigen = Convert.ToDecimal(registro["importedescargadoorigen"]); }
            if (registro.Table.Columns.Contains("importesaldoorigen"))
            { this.ImporteSaldoOrigen = Convert.ToDecimal(registro["importesaldoorigen"]); }
            if (registro.Table.Columns.Contains("idmonedaorigen"))
            { this.IdMonedaOrigen = Convert.ToInt16(registro["idmonedaorigen"]); }
            if (registro.Table.Columns.Contains("tipocambio"))
            { this.TipoCambio = Convert.ToDecimal(registro["tipocambio"]); }
            if (registro.Table.Columns.Contains("periodopago"))
            { this.PeriodoPago = Convert.IsDBNull(registro["periodopago"]) ? (Int32?)null : Convert.ToInt32(registro["periodopago"]); }
            if (registro.Table.Columns.Contains("fechapago"))
            { this.FechaPago = Convert.IsDBNull(registro["fechapago"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechapago"]); }
        }

        public clsFinanciamientoIngresoProvisionDocumentos()
        {
        }

        #endregion Constructor

        #region Propiedades - Generales

        public String Serie
        {
            get { return _strserie; }
            set { _strserie = value; }
        }

        public String IdOrdenCobroDocumentoSustento
        {
            get { return _stridordencobrodocumentosustento; }
            set { _stridordencobrodocumentosustento = value; }
        }

        public Int16 IdEstadoDeuda
        {
            get { return _intidestadodeuda; }
            set { _intidestadodeuda = value; }
        }

        public Int16 IdServicio
        {
            get { return _intidservicio; }
            set { _intidservicio = value; }
        }

        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        public Decimal Impuesto
        {
            get { return _decimpuesto; }
            set { _decimpuesto = value; }
        }

        public Int16 IndicadorDeuda
        {
            get { return _intindicadordeuda; }
            set { _intindicadordeuda = value; }
        }

        public Int16 IdEstadoDocumento
        {
            get { return _intidestadodocumento; }
            set { _intidestadodocumento = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        public String IdOrdenCobroDocumento
        {
            get { return _stridordencobrodocumento; }
            set { _stridordencobrodocumento = value; }
        }

        public Int16 IdOrdenCobroSustento
        {
            get { return _intidordencobrosustento; }
            set { _intidordencobrosustento = value; }
        }

        public Int16 IndicadorReclamo
        {
            get { return _intindicadorreclamo; }
            set { _intindicadorreclamo = value; }
        }

        public Boolean Sel
        {
            get { return _boosel; }
            set { _boosel = value; }
        }

        //VAS - Para reclamos
        public DateTime? FechaLimitePago
        {
            get { return _fecLimitePago; }
            set { _fecLimitePago = value; }
        }

        public Int32? PeriodoPago
        {
            get { return _intperiodopago; }
            set { _intperiodopago = value; }
        }

        public DateTime? FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodigoenlace; }
            set { _intcodigoenlace = value; }
        }

        #endregion Propiedades - Generales

        #region Propiedades - Importes al TipoCambio MonedaNacional

        public Decimal ImporteTotal
        {
            get { return _decimportetotal; }
            set { _decimportetotal = value; }
        }

        public Decimal MontoDescargado
        {
            get { return _decmontodescargado; }
            set { _decmontodescargado = value; }
        }

        public Decimal Saldo
        {
            get { return _decsaldo; }
            set
            {
                //  EGM. 26/08/2009. Se modifico el IF para los casos como del Documento Rendondeo (101)
                //  que tiene importe negativo cuando se quiere realizar Pago Parcial.
                //  Por defecto Saldo toma valor 0(cero) y siempre saldra error al comparar con estos
                //  tipos de documento
                //if (value > SaldoOriginal && SaldoOriginal != 0)
                if (value > SaldoOriginal && SaldoOriginal > 0)
                {
                    StringBuilder _sbmensaje = new StringBuilder();
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    _sbmensaje.Append("Saldo a Cobrar ");
                    _sbmensaje.Append(value.ToString());
                    _sbmensaje.Append(" no puede ser mayor a Saldo Original de ");
                    _sbmensaje.Append(SaldoOriginal.ToString());
                    _sbmensaje.AppendLine("");
                    _sbmensaje.AppendLine("");
                    throw new Exception(_sbmensaje.ToString());
                }

                _decsaldo = value;
            }
        }

        public Decimal SaldoOriginal
        {
            get { return _decsaldooriginal; }
            set { _decsaldooriginal = value; }
        }

        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        #endregion Propiedades - Importes al TipoCambio MonedaNacional

        #region Propiedades - Importes Origen - creadas por necesidad de cobro de diferentes monedas.

        public Decimal ImporteTotalOrigen
        {
            get { return _decimportetotalorigen; }
            set { _decimportetotalorigen = value; }
        }

        public Decimal ImporteDescargadoOrigen
        {
            get { return _decimportedescargadoorigen; }
            set { _decimportedescargadoorigen = value; }
        }

        public Decimal ImporteSaldoOrigen
        {
            get { return _decimportesaldoorigen; }
            set { _decimportesaldoorigen = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        #endregion Propiedades - Importes Origen - creadas por necesidad de cobro de diferentes monedas.

        #region

        public clsFinanciamientoDocumentosDescargoPropio ObtenerDocumentoDescargo(Boolean estercero, DateTime fechaemision, Int16 idtipoidentidad, String nroidentidad, Int16 ordenpago)
        {
            clsFinanciamientoDocumentosDescargoPropio documentodescargopropio = new clsFinanciamientoDocumentosDescargoPropio();
            documentodescargopropio.EsTerceros = false;
            documentodescargopropio.FechaEmision = fechaemision;
            documentodescargopropio.IdMonedaCambio = this.IdMoneda;
            documentodescargopropio.IdMonedaOrigen = this.IdMonedaOrigen;
            documentodescargopropio.IdNroServicio = this.IdNroServicio;
            documentodescargopropio.IdServicio = this.IdServicio;
            documentodescargopropio.IdOrdenCobro = this.IdOrdenCobro;
            documentodescargopropio.IdOrdenCobroSustento = this.IdOrdenCobroSustento;
            documentodescargopropio.IdTipoIdentidad = idtipoidentidad;
            documentodescargopropio.IdTransaccionLog = 0;
            documentodescargopropio.ImporteCambioCobrar = this.TipoCambio * this.ImporteTotalOrigen;
            documentodescargopropio.ImporteOrigenDescargo = 0;
            documentodescargopropio.ImporteOrigenDeuda = this.ImporteTotalOrigen;
            documentodescargopropio.ImporteOrigenCobrar = this.ImporteTotalOrigen;
            documentodescargopropio.IdOrdenCobroDocumento = int.Parse(this.IdOrdenCobroDocumento);
            documentodescargopropio.IdOrdenCobroDocumentoSustento = this.IdOrdenCobroDocumentoSustento;
            documentodescargopropio.NroIdentidad = nroidentidad;
            documentodescargopropio.OrdenPagoServicio = ordenpago;
            documentodescargopropio.PermitePagoParcial = false;
            documentodescargopropio.TipoCambio = this.TipoCambio;

            return documentodescargopropio;
        }

        #endregion clsIngresoProvisionDocumentos
    }

    #endregion NGC

    #region clsListaDocumentosAFavor

    public class clsListaFinanciamientoDocumentosAFavor
    {
        private List<clsFinanciamientoDocumentosAFavor> _objlistadocumentosafavor = new List<clsFinanciamientoDocumentosAFavor>();

        public clsListaFinanciamientoDocumentosAFavor()
        { }

        public clsListaFinanciamientoDocumentosAFavor(DataTable entidad)
        {
            if (entidad == null) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsFinanciamientoDocumentosAFavor(_drw));
            }
        }

        public List<clsFinanciamientoDocumentosAFavor> Elementos
        {
            get { return _objlistadocumentosafavor; }
            set { _objlistadocumentosafavor = value; }
        }

        #region Metodos

        public clsListaFinanciamientoIngresoProvision ObtenerListaIngresoProvision(Int16 idanocomercial, Int16 idmescomercial, Int16 idcentroservicio)
        {
            clsListaFinanciamientoIngresoProvision _objlistaingresoprovision = new clsListaFinanciamientoIngresoProvision();

            foreach (clsFinanciamientoDocumentosAFavor item in Elementos)
            {
                if (item.Importe == 0) { continue; }
                _objlistaingresoprovision.Elementos.Add(item.ObtenerIngresoProvision(idanocomercial, idmescomercial, idcentroservicio));
            }
            return _objlistaingresoprovision;
        }

        #endregion Metodos
    }

    #endregion clsListaDocumentosAFavor

    #region clsEncargo_Terceros

    public class clsFinanciamientoEncargo_Terceros : clsFinanciamientoIngresoProvisionBase
    {
        #region Campos

        private DateTime _datfechacarga;
        private String _stridtag;
        private Decimal _decimporteservicios;
        private Decimal _decimporterecargos;
        private Decimal _decimporteimpuestos;
        private Decimal _decimportetotal;
        private String _strnroservicio;
        private Int16 _intidordencobroip;
        private String _stridordencobrodocumentoip;
        private clsFinanciamientoIngresoProvision _objingresoprovision = new clsFinanciamientoIngresoProvision();

        #endregion Campos

        #region Constructor

        public clsFinanciamientoEncargo_Terceros(DataRow registro, clsFinanciamientoIngresoProvision ingresoprovision)
        {
            AsignarDatos(registro);
            this.IdEmpresa = ingresoprovision.IdEmpresa;
            this.IngresoProvision = ingresoprovision;
        }

        public clsFinanciamientoEncargo_Terceros(DataRow registro)
        {
            AsignarDatos(registro);
        }

        private void AsignarDatos(DataRow registro)
        {
            _booesterceros = true;
            this.Sel = (Boolean)registro["sel"];
            this.IdNroServicioPrincipal = (Int32)registro["idnroservicio"];
            this.IdServicioPrincipal = (Int16)registro["idservicio"];
            this.IdOrdenCobro = (Int16)registro["idtiporecibo"];
            this.IdOrdenCobroDocumento = registro["nrorecibo"].ToString();
            this.IdOrdenCobroIP = (Int16)registro["idordenCobro"];
            this.IdOrdenCobroDocumentoIP = registro["idordencobrodocumento"].ToString();
            //this.IdUsuario = (Int32)registro["idusuario"];
            this.FechaEmision = (DateTime)registro["fechaemision"];
            this.FechaVencimiento = (DateTime)registro["fechavencimiento"];
            this.FechaCarga = (DateTime)registro["fechacarga"];
            this.IdTag = registro["idtag"].ToString();
            this.NombreCliente = registro["nombreserviciotercero"].ToString();
            if (registro.Table.Columns.Contains("Direccion"))
            { this.Direccion = registro["Direccion"].ToString(); }
            this.IdEstadoDeuda = (Int16)registro["estado"];
            this.NroServicio = registro["nroservicio"].ToString();
            this.NroServicioTercero = registro["nroservicio"].ToString();
            this.IdOrdenCobroIP = (Int16)registro["idordencobro"];
            this.IdOrdenCobroDocumentoIP = registro["idordencobrodocumento"].ToString();

            this.IdMonedaOrigen = (Int16)registro["idmoneda"];
            this.ImporteServicios = (Decimal)registro["importeservicios"];
            this.ImporteRecargos = (Decimal)registro["importerecargos"];
            this.ImporteImpuestos = (Decimal)registro["importeimpuestos"];
            this.ImporteOrigenTotal = (Decimal)registro["importetotal"];
            if (registro.Table.Columns.Contains("TipoCambio"))
            { this.TipoCambio = (Decimal)registro["tipocambio"]; }

            if ((Estado)this.IdEstadoDeuda == Estado.Pagado)
            {
                this.ImporteOrigenDescargo = (Decimal)registro["importetotal"];
            }
            else
            {
                this.ImporteOrigenSaldo = (Decimal)registro["importetotal"];
                this.ImporteOrigenCobrar = (Decimal)registro["importetotal"];
            }

            if (registro.Table.Columns.Contains("idanocomercial"))
                this.IdAnoComercial = Convert.ToInt16(registro["idanocomercial"]);
            if (registro.Table.Columns.Contains("idmescomercial"))
                this.IdMesComercial = Convert.ToInt16(registro["idmescomercial"]);
            if (this.IdAnoComercial > 0 && this.IdMesComercial > 0)
                this.Periodo = (IdAnoComercial * 100) + IdMesComercial;
        }

        public clsFinanciamientoEncargo_Terceros()
        {
        }

        #endregion Constructor

        #region Propiedades

        public DateTime FechaCarga
        {
            get { return _datfechacarga; }
            set { _datfechacarga = value; }
        }

        public String IdTag
        {
            get { return _stridtag; }
            set { _stridtag = value; }
        }

        public Decimal ImporteServicios
        {
            get { return _decimporteservicios; }
            set { _decimporteservicios = value; }
        }

        public Decimal ImporteRecargos
        {
            get { return _decimporterecargos; }
            set { _decimporterecargos = value; }
        }

        public Decimal ImporteImpuestos
        {
            get { return _decimporteimpuestos; }
            set { _decimporteimpuestos = value; }
        }

        public String NroServicio
        {
            get { return _strnroservicio; }
            set { _strnroservicio = value; }
        }

        public Int16 IdOrdenCobroIP
        {
            get { return _intidordencobroip; }
            set { _intidordencobroip = value; }
        }

        public String IdOrdenCobroDocumentoIP
        {
            get { return _stridordencobrodocumentoip; }
            set { _stridordencobrodocumentoip = value; }
        }

        public clsFinanciamientoIngresoProvision IngresoProvision
        {
            get { return _objingresoprovision; }
            set { _objingresoprovision = value; }
        }

        #endregion Propiedades

        #region Propiedades Redefinidas por Herencia

        public Int16 IdTipoIdentidad
        {
            get
            {
                return IngresoProvision.IdTipoIdentidad;
            }
            set
            {
                IngresoProvision.IdTipoIdentidad = value;
            }
        }

        public String NroIdentidad
        {
            get
            {
                return IngresoProvision.NroIdentidad;
            }
            set
            {
                IngresoProvision.NroIdentidad = value;
            }
        }

        public override Int32 NroItems
        {
            get { return 1; }
        }

        #endregion Propiedades Redefinidas por Herencia
    }

    #endregion clsEncargo_Terceros

    #region clsListaIngresoProvision

    public class clsListaFinanciamientoIngresoProvision
    {
        private List<clsFinanciamientoIngresoProvisionBase> _lstingresoprovision = new List<clsFinanciamientoIngresoProvisionBase>();

        #region Constructor

        public clsListaFinanciamientoIngresoProvision(DataTable ingresoprovision)
        {
            if (ingresoprovision == null || ingresoprovision.Rows.Count == 0) { return; }

            foreach (DataRow fila in ingresoprovision.Rows)
            {
                Elementos.Add(new clsFinanciamientoIngresoProvision(fila));
            }
        }

        public clsListaFinanciamientoIngresoProvision(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos)
        {
            if (ingresoprovision == null || ingresoprovision.Rows.Count == 0) { return; }

            AsignarIngresoProvision(ingresoprovision, ingresoprovisiondocumentos);
        }

        public clsListaFinanciamientoIngresoProvision(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos, DataTable encargoterceros)
        {
            if (ingresoprovision == null || ingresoprovision.Rows.Count == 0) { return; }
            Boolean _booesterceros = (encargoterceros != null);

            if (_booesterceros)
            { AsignarEncargoTerceros(ingresoprovision, ingresoprovisiondocumentos, encargoterceros); }
            else
            { AsignarIngresoProvision(ingresoprovision, ingresoprovisiondocumentos); }
        }

        public clsListaFinanciamientoIngresoProvision()
        { }

        #endregion Constructor

        #region Propiedades

        public List<clsFinanciamientoIngresoProvisionBase> Elementos
        {
            get { return _lstingresoprovision; }
            set { _lstingresoprovision = value; }
        }

        #endregion Propiedades

        #region Metodos

        public System.ComponentModel.BindingList<clsFinanciamientoDocumentosDescargoBase> ObtenerListaDocumentosDescargo(Int16 ordenpago)
        {
            System.ComponentModel.BindingList<clsFinanciamientoDocumentosDescargoBase> _objelementosdocumentosdescargo = new System.ComponentModel.BindingList<clsFinanciamientoDocumentosDescargoBase>();
            foreach (clsFinanciamientoIngresoProvisionBase item in Elementos)
            {
                clsFinanciamientoIngresoProvision _objingresoprovision = null;
                if (item.EsTerceros)
                {
                    clsFinanciamientoEncargo_Terceros itemterceros = (clsFinanciamientoEncargo_Terceros)item;
                    _objingresoprovision = itemterceros.IngresoProvision;
                }
                else
                {
                    _objingresoprovision = (clsFinanciamientoIngresoProvision)item;
                }

                foreach (clsFinanciamientoIngresoProvisionDocumentos itemdocumentos in _objingresoprovision.ListaIngresoProvisionDocumentos)
                {
                    ordenpago += 1;
                    _objelementosdocumentosdescargo.Add(itemdocumentos.ObtenerDocumentoDescargo(false, _objingresoprovision.FechaEmision, _objingresoprovision.IdTipoIdentidad, _objingresoprovision.NroIdentidad, ordenpago));
                }
            }
            return _objelementosdocumentosdescargo;
        }

        private void AsignarIngresoProvision(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos)
        {
            String _strfiltro;
            foreach (DataRow drcabecera in ingresoprovision.Rows)
            {
                DataRow[] _lstrowdetalle, _lstrowteceros;
                _lstrowteceros = null;

                _strfiltro = "IdTipoDocumento = " + drcabecera["IdTipoDocumento"].ToString();
                _strfiltro = _strfiltro + " AND NroDocumento = '" + drcabecera["NroDocumento"].ToString() + "'";
                _lstrowdetalle = ingresoprovisiondocumentos.Select(_strfiltro.ToString());
                clsFinanciamientoIngresoProvision _objingresoprovision = new clsFinanciamientoIngresoProvision(drcabecera, _lstrowdetalle);
                //clsIngresoProvisionDetalle _objingresoprovision = new clsIngresoProvisionDetalle(drcabecera, _lstrowdetalle);

                Elementos.Add(_objingresoprovision);//, _lstrowteceros));
            }
        }

        private void AsignarEncargoTerceros(DataTable ingresoprovision, DataTable ingresoprovisiondocumentos, DataTable encargoterceros)
        {
            String _strfiltro;
            foreach (DataRow drterceros in encargoterceros.Rows)
            {
                DataRow[] _lstrowdetalle, _lstrowcabecera;
                _strfiltro = "IdOrdenCobro = " + drterceros["IdOrdenCobro"].ToString();
                _strfiltro = _strfiltro + " AND IdOrdenCobroDocumento = '" + drterceros["IdOrdenCobroDocumento"].ToString() + "'";
                _lstrowcabecera = ingresoprovision.Select(_strfiltro.ToString());
                _lstrowdetalle = ingresoprovisiondocumentos.Select(_strfiltro.ToString());
                clsFinanciamientoIngresoProvision _objingresoprovision = new clsFinanciamientoIngresoProvision(_lstrowcabecera[0], _lstrowdetalle);
                Elementos.Add(new clsFinanciamientoEncargo_Terceros(drterceros, _objingresoprovision));
            }
        }

        #endregion Metodos

        #region MétodosBusqueda

        private Predicate<clsFinanciamientoIngresoProvisionBase> PredicadoBuscar;
        private Int16 _intidordencobrotmp;
        private String _stridordencobrodocumentotmp;
        private Int32 _intidserviciotmp;
        private Int32 _intidnroserviciotmp;
        private DateTime _datfechaemisiontmp;
        private Boolean _boolseleccionadostmp;

        public clsFinanciamientoIngresoProvisionBase BuscarPorDocumentoSinNumero(Int16 idordencobro, Int32 idnroservicio)
        {
            _stridordencobrodocumentotmp = "";
            _intidordencobrotmp = idordencobro;
            _intidnroserviciotmp = idnroservicio;
            clsFinanciamientoIngresoProvisionBase _objIngresoprovision;
            PredicadoBuscar = new Predicate<clsFinanciamientoIngresoProvisionBase>(BuscarDocumentoNroServicio);
            _objIngresoprovision = Elementos.Find(PredicadoBuscar);
            return _objIngresoprovision;
        }

        private Boolean BuscarDocumentoNroServicio(clsFinanciamientoIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.IdOrdenCobro == _intidordencobrotmp && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp && ingresoprovision.IdOrdenCobroDocumento.Trim() == _stridordencobrodocumentotmp);
        }

        public List<clsFinanciamientoIngresoProvisionBase> BuscarPorNroServicio(Int16 idservicio, Int32 idnroservicio)
        {
            _intidserviciotmp = idservicio;
            _intidnroserviciotmp = idnroservicio;
            List<clsFinanciamientoIngresoProvisionBase> _objlistaingresoprovision;
            PredicadoBuscar = new Predicate<clsFinanciamientoIngresoProvisionBase>(BuscarNroServicio);
            _objlistaingresoprovision = Elementos.FindAll(PredicadoBuscar);
            return _objlistaingresoprovision;
        }

        private Boolean BuscarNroServicio(clsFinanciamientoIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.IdServicioPrincipal == _intidserviciotmp && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp);
        }

        public Boolean EsMasRecienteOrMasAntiguo(Int16 idservicio, Int32 idnroservicio, DateTime fechaemision, Boolean EsMasAntiguo)
        {
            _intidserviciotmp = idservicio;
            _intidnroserviciotmp = idnroservicio;
            _datfechaemisiontmp = fechaemision;
            _boolseleccionadostmp = EsMasAntiguo;
            if (EsMasAntiguo)
            { PredicadoBuscar = new Predicate<clsFinanciamientoIngresoProvisionBase>(BuscarMasAntiguo); }
            else
            { PredicadoBuscar = new Predicate<clsFinanciamientoIngresoProvisionBase>(BuscarMasReciente); }
            return Elementos.Exists(PredicadoBuscar);
        }

        private Boolean BuscarMasReciente(clsFinanciamientoIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.Sel == !_boolseleccionadostmp
                  && ingresoprovision.IdServicioPrincipal == _intidserviciotmp
                  && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp
                  && ingresoprovision.FechaEmision.CompareTo(_datfechaemisiontmp) > 0 //es mayor
                   );
        }

        private Boolean BuscarMasAntiguo(clsFinanciamientoIngresoProvisionBase ingresoprovision)
        {
            return (ingresoprovision.Sel == !_boolseleccionadostmp
                  && ingresoprovision.IdServicioPrincipal == _intidserviciotmp
                  && ingresoprovision.IdNroServicioPrincipal == _intidnroserviciotmp
                  && ingresoprovision.FechaEmision.CompareTo(_datfechaemisiontmp) < 0 //es menor
                   );
        }

        public clsListaFinanciamientoIngresoProvision ObtenerListaPropios()
        {
            clsListaFinanciamientoIngresoProvision _objlistapropios = new clsListaFinanciamientoIngresoProvision();
            PredicadoBuscar = new Predicate<clsFinanciamientoIngresoProvisionBase>(BuscarPropios);
            _objlistapropios.Elementos.AddRange(Elementos.FindAll(PredicadoBuscar));

            return _objlistapropios;
        }

        public List<Int32> ListaSuministros()
        {
            List<Int32> objListaSuministroTemp = new List<Int32>();

            Int32 intIdNroServicio = Elementos[0].IdNroServicioPrincipal;

            foreach (clsFinanciamientoIngresoProvisionBase objItem in Elementos)
            {
                if (!objListaSuministroTemp.Contains(objItem.IdNroServicioPrincipal))
                    objListaSuministroTemp.Add(objItem.IdNroServicioPrincipal);
            }

            return objListaSuministroTemp;
        }

        public Boolean BuscarPropios(clsFinanciamientoIngresoProvisionBase ingresoprovision)
        {
            return (!ingresoprovision.EsTerceros);
        }

        public Int32 CantidadServicios()
        {
            Int32 intResultado = 0;

            if (Elementos != null && Elementos.Count > 0)
            {
                Int32 intIdNroServicioTemp = Elementos[0].IdNroServicioPrincipal;
                Int32 intContador = 1;

                foreach (clsFinanciamientoIngresoProvisionBase objItem in Elementos)
                {
                    if (objItem.IdNroServicioPrincipal != intIdNroServicioTemp)
                    {
                        intIdNroServicioTemp = objItem.IdNroServicioPrincipal;
                        intContador += 1;
                    }
                }

                intResultado = intContador;
            }

            return intResultado;
        }

        #endregion MétodosBusqueda
    }

    #endregion clsListaIngresoProvision

    #region clsAsientoDocumentoValida

    public class clsFinanciamientoAsientoDocumentoValida
    {
        #region Campos
        private Int16 _intiduunn;
        private String _strcentroservicio;
        private String _strprocesoasiento;
        private DateTime? _datfecharegistro;
        private DateTime? _datfechaprocesodesde;
        private DateTime? _datfechaprocesohasta;
        private Int32? _intperiodo;
        #endregion Campos

        #region Propiedades

        /// <summary>
        ///
        /// </summary>
        public Int16 IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String CentroServicio
        {
            get { return _strcentroservicio; }
            set { _strcentroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ProcesoAsiento
        {
            get { return _strprocesoasiento; }
            set { _strprocesoasiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaProcesoDesde
        {
            get { return _datfechaprocesodesde; }
            set { _datfechaprocesodesde = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaProcesoHasta
        {
            get { return _datfechaprocesohasta; }
            set { _datfechaprocesohasta = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsFinanciamientoAsientoDocumentoValida()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsFinanciamientoAsientoDocumentoValida(DataRow dr)
        {
            if (dr == null) { return; }

            IdUUNN = Convert.ToInt16(dr["iduunn"]);
            CentroServicio = dr["centroservicio"].ToString();
            ProcesoAsiento = dr["procesoasiento"].ToString();
            FechaRegistro = Convert.IsDBNull(dr["fecharegistro"]) ? (DateTime?)null : Convert.ToDateTime(dr["fecharegistro"]);
            FechaProcesoDesde = Convert.IsDBNull(dr["fechaprocesodesde"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechaprocesodesde"]);
            FechaProcesoHasta = Convert.IsDBNull(dr["fechaprocesohasta"]) ? (DateTime?)null : Convert.ToDateTime(dr["fechaprocesohasta"]);
            Periodo = Convert.IsDBNull(dr["periodo"]) ? (Int32?)null : Convert.ToInt32(dr["periodo"]);
        }

        #endregion Constructor
    }

    #endregion clsAsientoDocumentoValida

    #region clsListaAsientoDocumentoValida

    public class clsListaFinanciamientoAsientoDocumentoValida
    {
        #region Campos
        private List<clsFinanciamientoAsientoDocumentoValida> _objelementos = new List<clsFinanciamientoAsientoDocumentoValida>();
        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<clsFinanciamientoAsientoDocumentoValida> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaFinanciamientoAsientoDocumentoValida()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaFinanciamientoAsientoDocumentoValida(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsFinanciamientoAsientoDocumentoValida(_drw));
            }
        }

        #endregion Constructor
    }

    #endregion clsListaAsientoDocumentoValida

    #region IIngresoProvision

    public interface IFinanciamientoIngresoProvision
    {
        Boolean Sel { get; set; }
        Int32 IdNroServicioPrincipal { get; set; }
        Int16 IdServicioPrincipal { get; set; }
        Int16 IdMonedaOrigen { get; set; }
        Int16 IdOrdenCobro { get; set; }
        Int16 IdTipoIdentidad { get; set; }
        String IdOrdenCobroDocumento { get; set; }
        String NroIdentidad { get; set; }
        String NombreCliente { get; set; }

        Decimal ImporteOrigenTotal { get; set; }
        Decimal ImporteOrigenSaldo { get; set; }
        Decimal ImporteOrigenCobrar { get; set; }
        Decimal ImporteOrigenDescargo { get; set; }

        Decimal ImporteCambioTotal { get; set; }
        Decimal ImporteCambioSaldo { get; set; }
        Decimal ImporteCambioCobrar { get; set; }
        Decimal ImporteCambioDescargo { get; set; }

        DateTime FechaVencimiento { get; set; }
        DateTime FechaEmision { get; set; }
    }

    #endregion IIngresoProvision

    #region clsIngresoProvisionBase

    public abstract class clsFinanciamientoIngresoProvisionBase : IFinanciamientoIngresoProvision
    {
        #region Campos

        protected Boolean _boosel;
        private Int16 _intidservicioprincipal;
        private Int32 _intidnroservicioprincipal;
        private String _strnroserviciotercero;
        protected Int16 _intidempresa;
        protected Int16 _intidordencobro;
        protected String _stridordencobrodocumento;
        private DateTime _datfecharegistro;
        private DateTime _datfechaemision;
        private DateTime _datfechavencimiento;
        private Decimal _decimportetotalorigen;
        private Decimal _decimportesaldoorigen;
        private Int16 _intidmonedaorigen;
        private Int16 _intidmoneda;
        private Int16 _intidestadodeuda;
        protected Decimal _decimportecobrar;
        private String _strnombrecliente;
        private String _strdireccion;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private EstadoRegistro _enuestadoregistro;
        protected Boolean _booesterceros;
        private String _strnombretipodocumento;
        private Boolean _booexcluirvalidacioninterna;
        private Decimal _decsaldo;
        private Boolean _booaplicarenipd;
        private Decimal _dectipocambio;
        private Int32 _intidusuario;
        private Int32 _intperiodo;
        private Int16 _intidanocomercial;
        private Int16 _intidmescomercial;
        private Int16 _intestadosuministro;
        private Int16 _intidccss;
        private Int16 _intiduunn;

        private Decimal _decimportecambiototal;
        private Decimal _decimportecambiodescargo;
        private Decimal _decimportecambiocobrar;
        private Decimal _decimporteorigendescargo;

        private Boolean _boopermitepagoparcial;
        private Boolean _intindicadorreclamo = false;
        private Int32 _intordenpagodocumento;
        private Int32 _intordenpagoservicio;
        private Boolean _bolindicadordeuda = false;

        //Se agregó el campo para poder generar el convenio asociado a la solicitud.

        //Se agrega campo pra efecto de leer el campo ImporteNoReclamado de la tabla IP para los casos
        //en que se registra un reclamo y el usuario indica lo que solia pagar, requerimiento solicitado por Jorge Vargas
        //10/06/2014
        private Decimal _decimportepaga;

        #endregion Campos

        #region Propiedades - Generales

        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }

        public String NroServicioTercero
        {
            get { return _strnroserviciotercero; }
            set { _strnroserviciotercero = value; }
        }

        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }

        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }

        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }

        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }

        public String IdOrdenCobroDocumento
        {
            get { return _stridordencobrodocumento; }
            set { _stridordencobrodocumento = value; }
        }

        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        public DateTime FechaEmision
        {
            get { return _datfechaemision; }
            set { _datfechaemision = value; }
        }

        public Int16 IdCCSS
        {
            get { return _intidccss; }
            set { _intidccss = value; }
        }

        public Int16 IdUUNN
        {
            get { return _intiduunn; }
            set { _intiduunn = value; }
        }

        public DateTime FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        public Int16 IdEstadoDeuda
        {
            get { return _intidestadodeuda; }
            set { _intidestadodeuda = value; }
        }

        public Int16 IdServicioPrincipal
        {
            get { return _intidservicioprincipal; }
            set { _intidservicioprincipal = value; }
        }

        public Int32 IdNroServicioPrincipal
        {
            get { return _intidnroservicioprincipal; }
            set { _intidnroservicioprincipal = value; }
        }

        public Boolean EsTerceros
        {
            get { return _booesterceros; }
            set { _booesterceros = value; }
        }

        public Int32 Periodo
        {
            get { return _intperiodo; }
            set { _intperiodo = value; }
        }

        public Int16 EstadoSuministro
        {
            get { return _intestadosuministro; }
            set { _intestadosuministro = value; }
        }

        public Int16 IdAnoComercial
        {
            get { return _intidanocomercial; }
            set { _intidanocomercial = value; }
        }

        public Int16 IdMesComercial
        {
            get { return _intidmescomercial; }
            set { _intidmescomercial = value; }
        }

        public Boolean Sel
        {
            get { return _boosel; }
            set { _boosel = value; }
            //{
            //    if ((Estado)IdEstadoDeuda == Estado.Pagado) { _boosel = false; return; }

            //    if ((!value) || (TipoCambio > 0 && ImporteCambioSaldo > 0))
            //    { _boosel = value; return; }

            //    if (_booexcluirvalidacioninterna || IdMonedaOrigen == 0) { _boosel = value; return; }

            //    _boosel = false;
            //    StringBuilder _sbmensaje = new StringBuilder();

            //    if (TipoCambio == 0)
            //    {
            //        _sbmensaje.AppendLine("");
            //        _sbmensaje.AppendLine("");
            //        _sbmensaje.AppendLine("!Ingrese Tipo Cambio!");
            //    }

            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("No puede seleccionar documento = " + _intidtipodocumento.ToString() + " - " + _strnrodocumento);
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("Importe de Tipo Cambio  = " + TipoCambio.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Importe Saldo de Cambio = " + ImporteCambioSaldo.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Moneda de cambio        = " + IdMonedaCambio.ToString());
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("Importe Total Origen  = " + ImporteOrigenTotal.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Importe Saldo Origen  = " + ImporteOrigenSaldo.ToString("###,###,##0.00"));
            //    _sbmensaje.AppendLine("Moneda Origen         = " + IdMonedaOrigen.ToString());
            //    _sbmensaje.AppendLine("");
            //    _sbmensaje.AppendLine("");

            //    throw new Exception(_sbmensaje.ToString());
            //}
        }

        public EstadoRegistro EstadoRegistro
        {
            get { return _enuestadoregistro; }
            set { _enuestadoregistro = value; }
        }

        public Boolean AplicarEnHijos
        {
            get { return _booaplicarenipd; }
            set { _booaplicarenipd = value; }
        }

        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        //Importes Origen
        public Decimal ImporteOrigenTotal
        {
            get { return _decimportetotalorigen; }
            set { _decimportetotalorigen = value; }
        }

        public Decimal ImporteOrigenSaldo
        {
            get { return _decimportesaldoorigen; }
            set { _decimportesaldoorigen = value; }
        }

        public virtual Decimal ImporteOrigenCobrar
        {
            get { return _decimportecobrar; }
            set { _decimportecobrar = value; }
        }

        public Decimal ImporteOrigenDescargo
        {
            get { return _decimporteorigendescargo; }
            set { _decimporteorigendescargo = value; }
        }

        public Int16 IdMonedaOrigen
        {
            get { return _intidmonedaorigen; }
            set { _intidmonedaorigen = value; }
        }

        //Importes Cambio
        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        public Decimal ImporteCambioTotal
        {
            get { return _decimportecambiototal; }
            set { _decimportecambiototal = value; }
        }

        public Decimal ImporteCambioSaldo
        {
            get { return _decsaldo; }
            set { _decsaldo = value; }
        }

        public Decimal ImporteCambioCobrar
        {
            get { return _decimportecambiocobrar; }
            set { _decimportecambiocobrar = value; }
        }

        public Decimal ImporteCambioDescargo
        {
            get { return _decimportecambiodescargo; }
            set { _decimportecambiodescargo = value; }
        }

        public Int16 IdMonedaCambio
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        public Boolean PermitePagoParcial
        {
            get { return _boopermitepagoparcial; }
            set { _boopermitepagoparcial = value; }
        }

        public Int32 OrdenPagoServicio
        {
            get { return _intordenpagoservicio; }
            set { _intordenpagoservicio = value; }
        }

        public Int32 OrdenPagoDocumento
        {
            get { return _intordenpagodocumento; }
            set { _intordenpagodocumento = value; }
        }

        public Boolean IndicadorReclamo
        {
            get { return _intindicadorreclamo; }
            set { _intindicadorreclamo = value; }
        }

        public abstract Int32 NroItems { get; }

        public Boolean IndicadorDeuda
        {
            get { return _bolindicadordeuda; }
            set { _bolindicadordeuda = value; }
        }

        //VAS
        private Decimal decImporteReclamo;

        public Decimal ImporteReclamo
        {
            get { return decImporteReclamo; }
            set { decImporteReclamo = value; }
        }

        private DateTime _decFechaServidor;

        //VAS - Definida para implementacion de reclamos
        public DateTime FechaServidor
        {
            get { return _decFechaServidor; }
            set { _decFechaServidor = value; }
        }

        public Decimal ImportePagoFijo
        {
            get { return _decimportepaga; }
            set { _decimportepaga = value; }
        }

        #endregion Propiedades - Generales



    }

    #endregion clsIngresoProvisionBase

    #region clsCajaEstadisticaDiario

    public class clsFinanciamientoCajaEstadisticaDiario
    {
        #region Campos

        private clsFinanciamientoUsuarioPuntoAtencion _objusuariopuntoatencion;
        private DateTime _datfechapago;
        private Int16 _intidempresacajero;
        private Int16 _intidunidadnegocio;
        private Int16 _intidcentroservicio;
        private Int16 _intidtipoatencion;
        private Int16 _intidpuntoatencion;
        private String _strnombrepuntoatencion;
        private Int32 _intnrotransacciones;
        private Int32 _intnroservicios;
        private Int32 _intnrodocumentos;
        private Int32 intServiciosPagados;
        private Decimal _decdineromn;
        private Decimal _decimporteventanillamn;
        private Decimal _decpendienteabovedamn;
        private Decimal _decimporteenbovedamn;
        private Decimal _decimportedocumentosmn;
        private Decimal _decimportedocumentosexternomn;
        private Decimal _decimportedocumentosinternomn;
        private Decimal _decimporteencustodiamn;
        private Decimal _decimporteenbancosmn;
        private Decimal _decimportecheque;
        private Int32 _intidnrocaja;
        private Int32 _intidUsuario;
        private DateTime _datfechaapertura;
        private DateTime? _datfechacierre;
        private Int16 _intidEstadoCobranza;
        private DateTime _datultimomovimiento;
        private Decimal _decmovefectivoingresomn;
        private Decimal _decmovefectivoegresomn;
        private Decimal _decmovdocumentoingresomn;
        private Decimal _decmovdocumentoegresomn;
        private Decimal _decrecchequeingresocalculadomn;
        private Decimal _decrecefectivoingresomn;
        private Decimal _decrecefectivoegresomn;
        private Decimal _decrecdocumentoingresomn;
        private Decimal _decrecdocumentoegresomn;

        //VAS Campos para el manejo de montos en cheque
        private Decimal _decrecchequeingresomn;

        private Decimal _decrecchequeegresomn;
        private Decimal _decmovchequeingresomn;
        private Decimal _decmovchequeegresomn;

        private Decimal _decTotalRecaudado;
        private String _strnombreusuario = "";
        private Int32 _intcantidadpendienteaboveda;
        private Int32 _idProveedorPersonal;
        private String _nombreProveedor;
        private String _nombreCortoProveedor;
        private String _nombreRefrendadora;

        private Int16 _intesintercativo;
        private String _strremitos;

        #endregion Campos

        #region Constructor

        /// <summary>
        ///
        /// </summary>
        /// <param name="fechapago"></param>
        /// <param name="idempresa"></param>
        /// <param name="idunidadnegocio"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <param name="transaccionespagadas"></param>
        /// <param name="dineromn"></param>
        /// <param name="importeventanillamn"></param>
        /// <param name="importeenbovedamn"></param>
        /// <param name="importedocumentosmn"></param>
        /// <param name="idnrocaja"></param>
        /// <param name="idusuario"></param>
        /// <param name="fechaapertura"></param>
        /// <param name="fechacierre"></param>
        /// <param name="idestadocobranza"></param>
        /// <param name="ultimomovimiento"></param>
        /// <param name="movefectivoingresomn"></param>
        /// <param name="movefectivoegresomn"></param>
        /// <param name="movdocumentoingresomn"></param>
        /// <param name="movdocumentoegresomn"></param>
        /// <param name="recefectivoingresomn"></param>
        /// <param name="recefectivoegresomn"></param>
        /// <param name="recdocumentoingresomn"></param>
        /// <param name="recdocumentoegresomn"></param>
        public clsFinanciamientoCajaEstadisticaDiario(DateTime fechapago
            , Int16 idempresa, Int16 idunidadnegocio, Int16 idcentroservicio, Int16 idtipoatencion
            , Int16 idpuntoatencion, Int32 transaccionespagadas, Decimal dineromn, Decimal importeventanillamn
            , Decimal importeenbovedamn, Decimal importedocumentosmn, Int32 idnrocaja, Int32 idusuario
            , DateTime fechaapertura, DateTime? fechacierre, Int16 idestadocobranza, DateTime ultimomovimiento
            , Decimal movefectivoingresomn, Decimal movefectivoegresomn, Decimal movdocumentoingresomn
            , Decimal movdocumentoegresomn, Decimal recefectivoingresomn, Decimal recefectivoegresomn
            , Decimal recdocumentoingresomn, Decimal recdocumentoegresomn)
        {
            this.UsuarioPuntoAtencion =
                new clsFinanciamientoUsuarioPuntoAtencion(idusuario, idtipoatencion, idpuntoatencion, idcentroservicio
                                          , idunidadnegocio, idestadocobranza, 1, idnrocaja, idempresa);

            this.IdNroCaja = idnrocaja;
            this.FechaPago = fechapago;
            this.NroTransacciones = 0;
            this.NroServicios = 0;
            this.NroDocumentos = 0;
            this.DineroMN = dineromn;
            this.ImporteVentanillaMN = importeventanillamn;
            this.ImporteenbovedaMN = importeenbovedamn;
            this.ImporteDocumentosMN = importedocumentosmn;
            this.FechaApertura = fechaapertura;
            this.FechaCierre = fechacierre;
            this.UltimoMovimiento = ultimomovimiento;
            this.MovEfectivoIngresoMN = movefectivoingresomn;
            this.MovEfectivoEgresoMN = movefectivoegresomn;
            this.MovDocumentoIngresoMN = movdocumentoingresomn;
            this.MovDocumentoEgresoMN = movdocumentoegresomn;
            this.RecEfectivoIngresoMN = recefectivoingresomn;
            this.RecEfectivoEgresoMN = recefectivoegresomn;
            this.RecDocumentoIngresoMN = recdocumentoingresomn;
            this.RecDocumentoEgresoMN = recdocumentoegresomn;
            this.TotalRecaudado = RecEfectivoIngresoMN + MovEfectivoIngresoMN + MovDocumentoIngresoMN + RecDocumentoIngresoMN + ImporteenbovedaMN;
            this.IdTipoAtencion = idtipoatencion;
            this.IdPuntoAtencion = idpuntoatencion;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="registro"></param>
        public clsFinanciamientoCajaEstadisticaDiario(DataRow registro)
        {
            if (registro == null) { return; }

            this.UsuarioPuntoAtencion = new clsFinanciamientoUsuarioPuntoAtencion
                                        (Convert.ToInt32(registro["idusuario"])
                                        , Convert.ToInt16(registro["idtipoatencion"])
                                        , Convert.ToInt16(registro["idpuntoatencion"])
                                        , Convert.ToInt16(registro["idCentroServicioCajero"])
                                        , Convert.ToInt16(registro["idUUNNCajero"])
                                        , Convert.ToInt16(registro["idestadocobranza"])
                                        , 1
                                        , Convert.ToInt32(registro["idnrocaja"])
                                        , Convert.ToInt16(registro["idempresacajero"]));

            this.IdNroCaja = Convert.ToInt32(registro["idnrocaja"]);
            this.FechaPago = Convert.ToDateTime(registro["fechapago"]);
            this.UsuarioPuntoAtencion.FechaPago = Convert.ToDateTime(registro["fechapago"]);
            this.NroTransacciones = Convert.ToInt32(registro["nrotransacciones"]);
            this.NroServicios = Convert.ToInt32(registro["nroservicios"]);
            this.NroDocumentos = Convert.ToInt32(registro["nrodocumentos"]);

            this.DineroMN = Convert.ToDecimal(registro["dineromn"]);
            this.ImporteVentanillaMN = Convert.ToDecimal(registro["EnVentanillamn"]);
            if (registro.Table.Columns.Contains("PendienteaBovedaMN")) { this.PendienteabovedaMN = Convert.IsDBNull(registro["PendienteaBovedaMN"]) ? 0 : Convert.ToDecimal(registro["PendienteaBovedaMN"]); }
            if (registro.Table.Columns.Contains("ImporteenChequesMN")) { this.ImporteChequeMN = Convert.IsDBNull(registro["ImporteenChequesMN"]) ? 0 : Convert.ToDecimal(registro["ImporteenChequesMN"]); }
            if (registro.Table.Columns.Contains("EnCustodiaMN")) { this.EnCustodiaMN = Convert.IsDBNull(registro["EnCustodiaMN"]) ? 0 : Convert.ToDecimal(registro["EnCustodiaMN"]); }
            if (registro.Table.Columns.Contains("RemitidoMN")) { this.EnBancoMN = Convert.IsDBNull(registro["RemitidoMN"]) ? 0 : Convert.ToDecimal(registro["RemitidoMN"]); }
            this.ImporteenbovedaMN = Convert.ToDecimal(registro["ImporteEnbovedamn"]);
            this.ImporteDocumentosMN = Convert.ToDecimal(registro["EnDocumentosmn"]);
            if (registro.Table.Columns.Contains("importedocinterno"))
            {
                this.ImporteDocumentosInternoMN = Convert.IsDBNull(registro["importedocinterno"]) ? 0 : Convert.ToDecimal(registro["importedocinterno"]);
                this.ImporteDocumentosExternoMN = this.ImporteDocumentosMN - this.ImporteDocumentosInternoMN;
            }
            this.FechaApertura = Convert.ToDateTime(registro["fechaapertura"]);
            this.FechaCierre = registro.IsNull("fechacierre") ? (DateTime?)null : Convert.ToDateTime(registro["fechacierre"]);
            this.UltimoMovimiento = Convert.ToDateTime(registro["ultimomovimiento"]);

            this.MovEfectivoIngresoMN = Convert.ToDecimal(registro["MovEfectivoIngresoMN"]);
            this.MovEfectivoEgresoMN = Convert.ToDecimal(registro["MovEfectivoEgresoMN"]);
            this.MovDocumentoIngresoMN = Convert.ToDecimal(registro["MovDocumentoIngresoMN"]);
            this.MovDocumentoEgresoMN = Convert.ToDecimal(registro["MovDocumentoEgresoMN"]);

            //VAS Cheques..
            if (registro.Table.Columns.Contains("MovChequeIngresoMN")) { this.MovChequeIngresoMN = Convert.ToDecimal(registro["MovChequeIngresoMN"]); }
            if (registro.Table.Columns.Contains("MovChequeEgresoMN")) { this.MovChequeEgresoMN = Convert.ToDecimal(registro["MovChequeEgresoMN"]); }
            if (registro.Table.Columns.Contains("RecChequeIngresoMN")) { this.RecChequeIngresoMN = Convert.ToDecimal(registro["RecChequeIngresoMN"]); }
            if (registro.Table.Columns.Contains("RecChequeEgresoMN")) { this.RecChequeEgresoMN = Convert.ToDecimal(registro["RecChequeEgresoMN"]); }

            this.RecEfectivoIngresoMN = Convert.ToDecimal(registro["RecEfectivoIngresoMN"]);
            this.RecEfectivoEgresoMN = Convert.ToDecimal(registro["RecEfectivoEgresoMN"]);
            this.RecDocumentoIngresoMN = Convert.ToDecimal(registro["RecDocumentoIngresoMN"]);
            this.RecDocumentoEgresoMN = Convert.ToDecimal(registro["RecDocumentoEgresoMN"]);

            this.IdTipoAtencion = Convert.ToInt16(registro["idtipoatencion"]);
            this.IdPuntoAtencion = Convert.ToInt16(registro["idpuntoatencion"]);

            if (registro.Table.Columns.Contains("RecChequeIngresoCalculadoMN"))
            {
                this.RecChequeIngresoCalculadoMN = Convert.IsDBNull(registro["RecChequeIngresoMN"]) ? 0 : Convert.ToDecimal(registro["RecChequeIngresoMN"]);
            }

            this.TotalRecaudado = (this.RecChequeIngresoMN
                                    + this.RecEfectivoIngresoMN
                                    + this.RecDocumentoIngresoMN) -
                                    (this.RecChequeEgresoMN
                                    + this.RecDocumentoEgresoMN
                                    + this.RecEfectivoEgresoMN);

            if (registro.Table.Columns.Contains("nombreusuario"))
            { this.NombreUsuario = registro["nombreusuario"].ToString(); }

            if (registro.Table.Columns.Contains("idproveedorpersonal"))
            { this.IdProveedorPersonal = Convert.ToInt32(registro["idproveedorpersonal"]); }

            if (registro.Table.Columns.Contains("nombreproveedor"))
            { this.NombreProveedor = registro["nombreproveedor"].ToString(); }

            if (registro.Table.Columns.Contains("nombrecortoproveedor"))
            { this.NombreCortoProveedor = registro["nombrecortoproveedor"].ToString(); }

            if (registro.Table.Columns.Contains("nombrerefrendadora"))
                this.NombreRefrendadora = Convert.IsDBNull(registro["nombrerefrendadora"]) ? null : registro["nombrerefrendadora"].ToString().Trim();

            if (registro.Table.Columns.Contains("nombrepuntoatencion"))
            {
                this.NombrePuntoAtencion = Convert.IsDBNull(registro["nombrepuntoatencion"]) ? "" : registro["nombrepuntoatencion"].ToString().Trim();
                this.UsuarioPuntoAtencion.NombrePuntoAtencion = Convert.IsDBNull(registro["nombrepuntoatencion"]) ? "" : registro["nombrepuntoatencion"].ToString().Trim();
            }

            if (registro.Table.Columns.Contains("nombretipoatencion"))
                this.UsuarioPuntoAtencion.NombreTipoAtencion = Convert.IsDBNull(registro["nombretipoatencion"]) ? "" : registro["nombretipoatencion"].ToString().Trim();

            if (registro.Table.Columns.Contains("interactivo"))
                this.EsInteractivo = Convert.IsDBNull(registro["interactivo"]) ? (Int16)0 : Convert.ToInt16(registro["interactivo"]);

            if (registro.Table.Columns.Contains("remitos"))
                this.Remitos = Convert.IsDBNull(registro["remitos"]) ? String.Empty : registro["remitos"].ToString();
        }

        /// <summary>
        ///
        /// </summary>
        public clsFinanciamientoCajaEstadisticaDiario()
        {
            UsuarioPuntoAtencion = new clsFinanciamientoUsuarioPuntoAtencion();
        }

        #endregion Constructor

        #region Propiedades

        /// <summary>
        /// NombrePuntoAtencion.
        /// </summary>
        public String NombrePuntoAtencion
        {
            get { return _strnombrepuntoatencion; }
            set { _strnombrepuntoatencion = value; }
        }

        /// <summary>
        /// NombreUsuario.
        /// </summary>
        public String NombreUsuario
        {
            get { return _strnombreusuario; }
            set { _strnombreusuario = value; }
        }

        /// <summary>
        /// FechaApertura.
        /// </summary>
        public DateTime FechaApertura
        {
            get { return _datfechaapertura; }
            set { _datfechaapertura = value; }
        }

        /// <summary>
        /// FechaPago.
        /// </summary>
        public DateTime FechaPago
        {
            get { return _datfechapago; }
            set { _datfechapago = value; }
        }

        /// <summary>
        /// NroTransacciones.
        /// </summary>
        public Int32 NroTransacciones
        {
            get { return _intnrotransacciones; }
            set { _intnrotransacciones = value; }
        }

        /// <summary>
        /// NroServicios.
        /// </summary>
        public Int32 NroServicios
        {
            get { return _intnroservicios; }
            set { _intnroservicios = value; }
        }

        /// <summary>
        /// ImporteVentanillaMN.
        /// </summary>
        public Decimal ImporteVentanillaMN
        {
            get { return _decimporteventanillamn; }
            set { _decimporteventanillamn = value; }
        }

        /// <summary>
        /// ImporteChequeMN.
        /// </summary>
        public Decimal ImporteChequeMN
        {
            get { return _decimportecheque; }
            set { _decimportecheque = value; }
        }

        /// <summary>
        /// ImporteDocumentosMN.
        /// </summary>
        public Decimal ImporteDocumentosMN
        {
            get { return _decimportedocumentosmn; }
            set { _decimportedocumentosmn = value; }
        }

        /// <summary>
        /// PendienteabovedaMN.
        /// </summary>
        public Decimal PendienteabovedaMN
        {
            get { return _decpendienteabovedamn; }
            set { _decpendienteabovedamn = value; }
        }

        /// <summary>
        /// ImporteenbovedaMN.
        /// </summary>
        public Decimal ImporteenbovedaMN
        {
            get { return _decimporteenbovedamn; }
            set { _decimporteenbovedamn = value; }
        }

        /// <summary>
        /// EnCustodiaMN.
        /// </summary>
        public Decimal EnCustodiaMN
        {
            get { return _decimporteencustodiamn; }
            set { _decimporteencustodiamn = value; }
        }

        /// <summary>
        /// EnBancoMN.
        /// </summary>
        public Decimal EnBancoMN
        {
            get { return _decimporteenbancosmn; }
            set { _decimporteenbancosmn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreRefrendadora
        {
            get { return _nombreRefrendadora; }
            set { _nombreRefrendadora = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdProveedorPersonal
        {
            get { return _idProveedorPersonal; }
            set { _idProveedorPersonal = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreProveedor
        {
            get { return _nombreProveedor; }
            set { _nombreProveedor = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreCortoProveedor
        {
            get { return _nombreCortoProveedor; }
            set { _nombreCortoProveedor = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public clsFinanciamientoUsuarioPuntoAtencion UsuarioPuntoAtencion
        {
            get { return _objusuariopuntoatencion; }
            set { _objusuariopuntoatencion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 NroDocumentos
        {
            get { return _intnrodocumentos; }
            set { _intnrodocumentos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal DineroMN
        {
            get { return _decdineromn; }
            set { _decdineromn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteDocumentosExternoMN
        {
            get { return _decimportedocumentosexternomn; }
            set { _decimportedocumentosexternomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal ImporteDocumentosInternoMN
        {
            get { return _decimportedocumentosinternomn; }
            set { _decimportedocumentosinternomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? FechaCierre
        {
            get { return _datfechacierre; }
            set { _datfechacierre = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime UltimoMovimiento
        {
            get { return _datultimomovimiento; }
            set { _datultimomovimiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovDocumentoEgresoMN
        {
            get { return _decmovdocumentoegresomn; }
            set { _decmovdocumentoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovDocumentoIngresoMN
        {
            get { return _decmovdocumentoingresomn; }
            set { _decmovdocumentoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovEfectivoEgresoMN
        {
            get { return _decmovefectivoegresomn; }
            set { _decmovefectivoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovEfectivoIngresoMN
        {
            get { return _decmovefectivoingresomn; }
            set { _decmovefectivoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecChequeIngresoCalculadoMN
        {
            get { return _decrecchequeingresocalculadomn; }
            set { _decrecchequeingresocalculadomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecDocumentoEgresoMN
        {
            get { return _decrecdocumentoegresomn; }
            set { _decrecdocumentoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecDocumentoIngresoMN
        {
            get { return _decrecdocumentoingresomn; }
            set { _decrecdocumentoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecEfectivoEgresoMN
        {
            get { return _decrecefectivoegresomn; }
            set { _decrecefectivoegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecEfectivoIngresoMN
        {
            get { return _decrecefectivoingresomn; }
            set { _decrecefectivoingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 CantidadPendienteABoveda
        {
            get { return _intcantidadpendienteaboveda; }
            set { _intcantidadpendienteaboveda = value; }
        }

        //VAS  Total recaudado.
        /// <summary>
        ///
        /// </summary>
        public Decimal TotalRecaudado
        {
            get { return _decTotalRecaudado; }
            set { _decTotalRecaudado = value; }
        }

        //VAS Creados para el manejo de chques - 02/02/2009
        /// <summary>
        ///
        /// </summary>
        public Decimal RecChequeEgresoMN
        {
            get { return _decrecchequeegresomn; }
            set { _decrecchequeegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal RecChequeIngresoMN
        {
            get { return _decrecchequeingresomn; }
            set { _decrecchequeingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovChequeEgresoMN
        {
            get { return _decmovchequeegresomn; }
            set { _decmovchequeegresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal MovChequeIngresoMN
        {
            get { return _decmovchequeingresomn; }
            set { _decmovchequeingresomn = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 EsInteractivo
        {
            get { return _intesintercativo; }
            set { _intesintercativo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Remitos
        {
            get { return _strremitos; }
            set { _strremitos = value; }
        }

        #endregion Propiedades

        #region Propiedades Interactuan con UsuarioPuntoAtencion

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroCaja
        {
            get { return _intidnrocaja; }
            set
            {
                _intidnrocaja = value;
                UsuarioPuntoAtencion.IdNroCajaUltimo = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEstadoCobranza
        {
            get { return UsuarioPuntoAtencion.IdEstadoCobranza; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdUnidadNegocio
        {
            get { return UsuarioPuntoAtencion.IdUnidadNegocio; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdCentroServicio
        {
            get { return UsuarioPuntoAtencion.IdCentroServicio; }
        }

        #endregion Propiedades Interactuan con UsuarioPuntoAtencion
    }

    #endregion clsCajaEstadisticaDiario

    #endregion NGC

    #region Entidades NGC Escritorio

    public class clsContadorDocumentoComercial
    {
        #region Campos

        private Int16 _intidcontador;
        private Int16 _intidtipodocumento;
        private String _strnombredocumento;
        private Int16 _intidcentroservicio;
        private String _strnombrecentroservicio;
        private String _strserie;
        private Int32 _intrangoinicial;
        private Int32 _intrangofinal;
        private Int32 _intnumeroactual;
        private DateTime _datfechavigencia;
        private Int16 _intidestado;
        private List<clsPuntoAtencionContador> _elementospuntosatencion = new List<clsPuntoAtencionContador>();
        private Int16 _nposiciones;
        private Boolean? _ReiniciarPorAyo;
        private Int16? _AyoUltimoNroDocumento;
        private Int16? _IdPuntoAtencion;
        private Int16 _intidorganizacion;
        private Int16 _intidempresa;
        private Int16 _intidunidadnegocio;
        private Int16 _intformagrabar;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdContador
        {
            get { return _intidcontador; }
            set { _intidcontador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombreDocumento
        {
            get { return _strnombredocumento; }
            set { _strnombredocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombreCentroServicio
        {
            get { return _strnombrecentroservicio; }
            set { _strnombrecentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Serie
        {
            get { return _strserie; }
            set { _strserie = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 RangoInicial
        {
            get { return _intrangoinicial; }
            set { _intrangoinicial = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 RangoFinal
        {
            get { return _intrangofinal; }
            set { _intrangofinal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 NumeroActual
        {
            get { return _intnumeroactual; }
            set { _intnumeroactual = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaVigencia
        {
            get { return _datfechavigencia; }
            set { _datfechavigencia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        /// <summary>
        /// Puntos de Atencion Asociados a un Contador.
        /// </summary>
        public List<clsPuntoAtencionContador> ElementosPuntoAtencion
        {
            get { return _elementospuntosatencion; }
            set { _elementospuntosatencion = value; }
        }

        /// <summary>
        /// Indica si el contador del documento debe reiniciarse por año.
        /// </summary>
        public Boolean? ReiniciarPorAyo
        {
            get { return _ReiniciarPorAyo; }
            set { _ReiniciarPorAyo = value; }
        }

        /// <summary>
        /// Año del ultimo documento generado.
        /// </summary>
        public Int16? AyoUltimoNroDocumento
        {
            get { return _AyoUltimoNroDocumento; }
            set { _AyoUltimoNroDocumento = value; }
        }

        /// <summary>
        /// Id del Punto de Atención.
        /// </summary>
        public Int16? IdPuntoAtencion
        {
            get { return _IdPuntoAtencion; }
            set { _IdPuntoAtencion = value; }
        }

        /// <summary>
        /// Indica el número de posiciones que tendrá el
        /// número de documento.
        /// </summary>
        public Int16 NPosiciones
        {
            get { return _nposiciones; }
            set { _nposiciones = value; }
        }

        public Int16 IdOrganizacion
        {
            get { return _intidorganizacion; }
            set { _intidorganizacion = value; }
        }

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdUnidadNegocio
        {
            get { return _intidunidadnegocio; }
            set { _intidunidadnegocio = value; }
        }

        /// <summary>
        /// Setea la forma de grabar de la Entidad
        /// 1.Graba Todo
        /// 2.Graba Solo Contador.
        /// 3.Graba Solo Puntos Atencion Asociados.
        /// </summary>
        public Int16 FormaGrabar
        {
            get { return _intformagrabar; }
            set { _intformagrabar = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsContadorDocumentoComercial()
        {
            _datfechavigencia = DateTime.Now;
            _strserie = "";
            _ReiniciarPorAyo = false;
            _intidestado = 1;
        }

        /// <summary>
        /// Inicializar Entidad con parámetros predeterminados.
        /// </summary>
        /// <param name="idOrganizacion"></param>
        /// <param name="idUnidadNegocio"></param>
        /// <param name="idCentroServicio"></param>
        /// <param name="fechaVigencia"></param>
        public clsContadorDocumentoComercial
            (Int16 idOrganizacion
            , Int16 idEmpresa
            , Int16 idUnidadNegocio
            , Int16 idCentroServicio
            , DateTime fechaVigencia)
        {
            _intidorganizacion = idOrganizacion;
            _intidempresa = idEmpresa;
            _intidunidadnegocio = idUnidadNegocio;
            _intidcentroservicio = idCentroServicio;
            _datfechavigencia = DateTime.Now;
            _strserie = "";
            _ReiniciarPorAyo = false;
            _intidestado = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        public clsContadorDocumentoComercial(DataRow registro)
        {
            if (registro == null) { return; }

            AsignarRegistro(registro);
        }

        /// <summary>
        /// Constructor que inicializa los parametros del contador y 
        /// puntos de atencion.
        /// </summary>
        /// <param name="registro">Información del contador.</param>
        /// <param name="detalle">Puntos de Atención Asignados.</param>
        public clsContadorDocumentoComercial(DataRow registro, DataTable detalle)
        {
            if (registro == null) { return; }

            AsignarRegistro(registro);

            if (detalle != null)
            {
                foreach (DataRow _drw in detalle.Rows)
                { _elementospuntosatencion.Add(new clsPuntoAtencionContador(_drw)); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        public void AsignarRegistro(DataRow registro)
        {
            DataColumnCollection col = registro.Table.Columns;

            if (col.Contains("idcontador")) { IdContador = Convert.ToInt16(registro["idcontador"]); }
            if (col.Contains("idtipodocumento")) { IdTipoDocumento = Convert.ToInt16(registro["idtipodocumento"]); }
            if (col.Contains("nombredocumento")) { NombreDocumento = registro["nombredocumento"].ToString(); }
            if (col.Contains("idcentroservicio")) { IdCentroServicio = Convert.ToInt16(registro["idcentroservicio"]); }
            if (col.Contains("nombrecentroservicio")) { NombreCentroServicio = registro["nombrecentroservicio"].ToString(); }
            if (col.Contains("serie")) { Serie = registro["serie"].ToString(); }
            if (col.Contains("rangoinicial")) { RangoInicial = Convert.ToInt32(registro["rangoinicial"]); }
            if (col.Contains("rangofinal")) { RangoFinal = Convert.ToInt32(registro["rangofinal"]); }
            if (col.Contains("numeroactual")) { NumeroActual = Convert.ToInt32(registro["numeroactual"]); }
            if (col.Contains("fechavigencia")) { FechaVigencia = Convert.ToDateTime(registro["fechavigencia"]); }
            if (col.Contains("idestado")) { IdEstado = Convert.ToInt16(registro["idestado"]); }
            if (col.Contains("idorganizacion")) { _intidorganizacion = Convert.ToInt16(registro["idorganizacion"]); }
            if (col.Contains("idempresa")) { _intidempresa = Convert.ToInt16(registro["idempresa"]); }
            if (col.Contains("iduunn")) { _intidunidadnegocio = Convert.ToInt16(registro["iduunn"]); }

            if (col.Contains("reiniciarporayo"))
            {
                _ReiniciarPorAyo = Convert.IsDBNull(registro["reiniciarporayo"])
                                    ? false
                                    : Convert.ToBoolean(registro["reiniciarporayo"]);
            }

            if (col.Contains("ayoultimonrodocumento"))
            {
                _AyoUltimoNroDocumento = Convert.IsDBNull(registro["ayoultimonrodocumento"])
                                        ? (Int16?)null
                                        : (Int16?)registro["ayoultimonrodocumento"];
            }

            if (col.Contains("idpuntoatencion"))
            {
                _IdPuntoAtencion = Convert.IsDBNull(registro["idpuntoatencion"])
                                    ? (Int16?)null
                                    : (Int16?)registro["idpuntoatencion"];
            }

            if (col.Contains("nposiciones"))
            {
                NPosiciones = Convert.IsDBNull(registro["nposiciones"])
                                    ? (Int16)0
                                    : Convert.ToInt16(registro["nposiciones"]);
            }
        }
        #endregion
    }

    public class clsPuntoAtencionContador
    {
        #region Campos

        private Int16 _intidpuntoatencion;
        private String _strnombrepuntoatencion;
        private Int16 _intidcontador;
        private Int16 _intidestado;

        #endregion
        private Int16 _intidtipoatencion;

        public Int16 IdTipoAtencion
        {
            get { return _intidtipoatencion; }
            set { _intidtipoatencion = value; }
        }

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdPuntoAtencion
        {
            get { return _intidpuntoatencion; }
            set { _intidpuntoatencion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombrePuntoAtencion
        {
            get { return _strnombrepuntoatencion; }
            set { _strnombrepuntoatencion = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        public Int16 IdContador
        {
            get { return _intidcontador; }
            set { _intidcontador = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsPuntoAtencionContador()
        {
        }

        /// <summary>
        /// Constructor pasando parámetros.
        /// </summary>
        public clsPuntoAtencionContador(Int16 idpuntoatencion, Int16 idcontador, String nombrepuntoatencion)
        {
            IdPuntoAtencion = idpuntoatencion;
            IdContador = idcontador;
            NombrePuntoAtencion = nombrepuntoatencion;
        }

        /// <summary>
        /// Constructor pasando parámetros.
        /// </summary>
        public clsPuntoAtencionContador(Int16 idpuntoatencion, Int16 idcontador, String nombrepuntoatencion, Int16 idestado)
        {
            IdPuntoAtencion = idpuntoatencion;
            IdContador = idcontador;
            NombrePuntoAtencion = nombrepuntoatencion;
            IdEstado = idestado;
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsPuntoAtencionContador(DataRow dr)
        {
            if (dr == null) { return; }

            if (dr.Table.Columns.Contains("idpuntoatencion")) { IdPuntoAtencion = Convert.ToInt16(dr["idpuntoatencion"]); }
            if (dr.Table.Columns.Contains("idcontador")) { IdContador = Convert.ToInt16(dr["idcontador"]); }
            if (dr.Table.Columns.Contains("nombreatencion")) { NombrePuntoAtencion = dr["nombreatencion"].ToString(); }
            if (dr.Table.Columns.Contains("idestado")) { IdEstado = Convert.ToInt16(dr["idestado"]); }
        }

        #endregion



        public class clsIngresoProvisionDetalle
        {
            private int intidConcepto;
            private decimal decImporte;
            private int intCRC;
            private decimal decCantidad;
            private decimal decPrecioUnitario;
            private decimal? decPrecioUnitarioSinFose;
            private Int16 _intidempresa;
            private short intidTipoDocumento;
            private short intidTipoDocumentoSustento;
            private string strNroDocumento;
            private string strNroDocumentoSustento;
            private Int32 _intidnroservicio;
            private Int16 _intindicadorigv;
            private Int16 _intcodigoenlace;

            private clsIngresoProvisionDetalleComplemento _oingresoprovisiondetallecomplemento;

            public clsIngresoProvisionDetalle(Int16 idempresa, short _idTipoDocumento
                , string _NroDocumento, short _idTipoDocumentoSustento
                , string _NroDocumentoSustento, int _idConcepto
                , decimal _Importe, int _CRC, decimal _Cantidad
                , decimal _PrecioUnitario, decimal? _PrecioUnitarioSinFose
                , Int32 _IdNroServicio, Int16 _IndicadorIGV)
            {
                this.IdEmpresa = idempresa;
                this.idTipoDocumento = _idTipoDocumento;
                this.NroDocumento = _NroDocumento;
                this.idTipoDocumentoSustento = _idTipoDocumentoSustento;
                this.NroDocumentoSustento = _NroDocumentoSustento;
                this.idConcepto = _idConcepto;
                this.Importe = _Importe;
                this.CRC = _CRC;
                this.Cantidad = _Cantidad;
                this.PrecioUnitario = _PrecioUnitario;
                this.PrecioUnitarioSinFose = _PrecioUnitarioSinFose;
                this.IdNroServicio = _IdNroServicio;
                this.IndicadorIGV = _IndicadorIGV;
            }

            public clsIngresoProvisionDetalle(Int16 idempresa, short _idTipoDocumento
                , string _NroDocumento, short _idTipoDocumentoSustento
                , string _NroDocumentoSustento, int _idConcepto
                , decimal _Importe, int _CRC, decimal _Cantidad
                , decimal _PrecioUnitario, decimal? _PrecioUnitarioSinFose
                , Int32 _IdNroServicio, Int16 _IndicadorIGV, clsIngresoProvisionDetalleComplemento oComplemento)
            {
                this.IdEmpresa = idempresa;
                this.idTipoDocumento = _idTipoDocumento;
                this.NroDocumento = _NroDocumento;
                this.idTipoDocumentoSustento = _idTipoDocumentoSustento;
                this.NroDocumentoSustento = _NroDocumentoSustento;
                this.idConcepto = _idConcepto;
                this.Importe = _Importe;
                this.CRC = _CRC;
                this.Cantidad = _Cantidad;
                this.PrecioUnitario = _PrecioUnitario;
                this.PrecioUnitarioSinFose = _PrecioUnitarioSinFose;
                this.IdNroServicio = _IdNroServicio;
                this.IndicadorIGV = _IndicadorIGV;
                this.oIngresoProvisionDetalleComplemento = oComplemento;
            }

            public clsIngresoProvisionDetalle(DataRow drregistro)
            {
                this.IdEmpresa = Convert.ToInt16(drregistro["idempresa"]);
                this.idTipoDocumento = Convert.ToInt16(drregistro["idtipodocumento"]);
                this.NroDocumento = drregistro["nrodocumento"].ToString();
                this.idTipoDocumentoSustento = Convert.ToInt16(drregistro["idtipodocumentosustento"]);
                this.NroDocumentoSustento = drregistro["nrodocumentosustento"].ToString();
                this.idConcepto = Convert.ToInt32(drregistro["idconcepto"]);
                this.Importe = Convert.ToDecimal(drregistro["importe"]);
                this.CRC = Convert.ToInt32(drregistro["crc"]);
                this.Cantidad = Convert.ToDecimal(drregistro["cantidad"]);
                this.PrecioUnitario = Convert.ToDecimal(drregistro["preciounitario"]);
                this.PrecioUnitarioSinFose = drregistro.Table.Columns.Contains("preciounitariosinfose") ? (Convert.IsDBNull(drregistro["preciounitariosinfose"]) ? (Decimal?)null : Convert.ToDecimal(drregistro["preciounitariosinfose"])) : (Decimal?)null;
                this.IdNroServicio = Convert.ToInt32(drregistro["idnroservicio"]);
                this.IndicadorIGV = Convert.ToInt16(drregistro["indicadorigv"]);
            }

            public clsIngresoProvisionDetalle()
            {
            }

            public int idConcepto
            {
                get { return intidConcepto; }
                set { intidConcepto = value; }
            }

            public decimal Importe
            {
                get { return decImporte; }
                set { decImporte = value; }
            }

            public int CRC
            {
                get { return intCRC; }
                set { intCRC = value; }
            }

            public decimal Cantidad
            {
                get { return decCantidad; }
                set { decCantidad = value; }
            }

            public decimal PrecioUnitario
            {
                get { return decPrecioUnitario; }
                set { decPrecioUnitario = value; }
            }

            public decimal? PrecioUnitarioSinFose
            {
                get { return decPrecioUnitarioSinFose; }
                set { decPrecioUnitarioSinFose = value; }
            }

            public Int16 IdEmpresa
            {
                get { return _intidempresa; }
                set { _intidempresa = value; }
            }

            public short idTipoDocumento
            {
                get { return intidTipoDocumento; }
                set { intidTipoDocumento = value; }
            }

            public short idTipoDocumentoSustento
            {
                get { return intidTipoDocumentoSustento; }
                set { intidTipoDocumentoSustento = value; }
            }

            public string NroDocumento
            {
                get { return strNroDocumento; }
                set { strNroDocumento = value; }
            }

            public string NroDocumentoSustento
            {
                get { return strNroDocumentoSustento; }
                set { strNroDocumentoSustento = value; }
            }

            public Int32 IdNroServicio
            {
                get { return _intidnroservicio; }
                set { _intidnroservicio = value; }
            }

            public Int16 IndicadorIGV
            {
                get { return _intindicadorigv; }
                set { _intindicadorigv = value; }
            }

            public Int16 CodigoEnlace
            {
                get { return _intcodigoenlace; }
                set { _intcodigoenlace = value; }
            }

            public clsIngresoProvisionDetalleComplemento oIngresoProvisionDetalleComplemento
            {
                get { return _oingresoprovisiondetallecomplemento; }
                set { _oingresoprovisiondetallecomplemento = value; }
            }
        }



    }

    public class clsListaOrdenCobroDocumento
    {
        private System.ComponentModel.BindingList<clsOrdenCobroDocumento> _objelementos = new System.ComponentModel.BindingList<clsOrdenCobroDocumento>();

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public System.ComponentModel.BindingList<clsOrdenCobroDocumento> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public clsListaOrdenCobroDocumento()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public clsListaOrdenCobroDocumento(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new clsOrdenCobroDocumento(_drw));
            }
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="ordencobrodocumentos"></param>
        /// <param name="ordencobrodocumentodetalle"></param>
        public clsListaOrdenCobroDocumento(DataTable ordencobrodocumentos, DataTable ordencobrodocumentodetalle)
        {
            if (ordencobrodocumentos == null || ordencobrodocumentos.Rows.Count == 0) { return; }
            AsignarOrdenCobroDocumento(ordencobrodocumentos, ordencobrodocumentodetalle);
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="ordencobrodocumentos"></param>
        /// <param name="ordencobrodocumentodetalle"></param>
        public clsListaOrdenCobroDocumento(DataTable ordencobrodocumentos, DataTable ordencobrodocumentodetalle, DataTable ordencobrodocumentoreferencia)
        {
            if (ordencobrodocumentos == null || ordencobrodocumentos.Rows.Count == 0) { return; }
            AsignarOrdenCobroDocumento(ordencobrodocumentos, ordencobrodocumentodetalle, ordencobrodocumentoreferencia);
        }

        #region Metodos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ordencobrodocumentos"></param>
        /// <param name="ordencobrodocumentodetalle"></param>
        private void AsignarOrdenCobroDocumento(DataTable ordencobrodocumentos, DataTable ordencobrodocumentodetalle)
        {
            String _strfiltro;
            foreach (DataRow drcabecera in ordencobrodocumentos.Rows)
            {
                DataRow[] _lstrowdetalle;
                _strfiltro = "IdOrdenCobroDocumento = " + drcabecera["IdOrdenCobroDoc"].ToString();
                _strfiltro = _strfiltro + " AND IdTipoDocumento = " + drcabecera["IdTipoDocumento"].ToString();
                _lstrowdetalle = ordencobrodocumentodetalle.Select(_strfiltro.ToString());
                clsOrdenCobroDocumento _objordencobrodocumento = new clsOrdenCobroDocumento(drcabecera, _lstrowdetalle);
                Elementos.Add(_objordencobrodocumento);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ordencobrodocumentos"></param>
        /// <param name="ordencobrodocumentodetalle"></param>
        /// <param name="ordencobrodocumentoreferencia"></param>
        private void AsignarOrdenCobroDocumento(DataTable ordencobrodocumentos, DataTable ordencobrodocumentodetalle, DataTable ordencobrodocumentoreferencia)
        {
            String _strfiltro;
            foreach (DataRow drcabecera in ordencobrodocumentos.Rows)
            {
                DataRow[] _lstrowdetalle;
                _strfiltro = "IdOrdenCobroDocumento = " + drcabecera["IdOrdenCobroDoc"].ToString();
                _strfiltro = _strfiltro + " AND IdTipoDocumento = " + drcabecera["IdTipoDocumento"].ToString();
                _lstrowdetalle = ordencobrodocumentodetalle.Select(_strfiltro.ToString());

                _strfiltro = String.Empty;
                _strfiltro = "IdOrdenCobroDocumento = " + drcabecera["IdOrdenCobroDoc"].ToString();
                DataRow[] _lstrowreferencia = ordencobrodocumentoreferencia.Select(_strfiltro);

                clsOrdenCobroDocumento _objordencobrodocumento = new clsOrdenCobroDocumento(drcabecera, _lstrowdetalle, _lstrowreferencia);

                Elementos.Add(_objordencobrodocumento);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idanocomercial"></param>
        /// <param name="idmescomercial"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <param name="idusuario"></param>
        /// <param name="fechapago"></param>
        /// <returns></returns>
        public clsListaIngresoProvision ObtenerListaIngresoProvision(Int16 idanocomercial, Int16 idmescomercial, Int16 idcentroservicio,
                                                                         Int16 idtipoatencion, Int16 idpuntoatencion, Int32 idusuario, DateTime fechapago)
        {
            clsListaIngresoProvision _objlistaingresoprovision = new clsListaIngresoProvision();

            foreach (clsOrdenCobroDocumento item in Elementos)
            {
                if (item.Importe == 0) { continue; }
                if (item.IdTipoDocumento == (Int16)TipoDocumentoComercial.ReciboEnergia) { continue; }
                _objlistaingresoprovision.Elementos.Add(item.ObtenerIngresoProvisionNuevo(idanocomercial, idmescomercial, idcentroservicio,
                                                        idtipoatencion, idpuntoatencion, idusuario, fechapago));
            }
            return _objlistaingresoprovision;
        }

        /// <summary>
        /// Obtiene la lista de documentos cobrables para su descargo
        /// </summary>
        /// <param name="idanocomercial"></param>
        /// <param name="idmescomercial"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <param name="idusuario"></param>
        /// <returns></returns>
        public clsListaIngresoProvision ObtenerListaCobrables(Int16 idanocomercial, Int16 idmescomercial, Int16 idcentroservicio,
                                                                 Int16 idtipoatencion, Int16 idpuntoatencion, Int32 idusuario, DateTime fechapago)
        {
            clsListaIngresoProvision _objlistaingresoprovision = new clsListaIngresoProvision();

            foreach (clsOrdenCobroDocumento item in Elementos)
            {
                if (item.Importe == 0 || item.EsCobrable == 0) { continue; }
                if (item.IdTipoDocumento == (Int16)TipoDocumentoComercial.ReciboEnergia) { continue; }
                _objlistaingresoprovision.Elementos.Add(item.ObtenerIngresoProvisionNuevo(idanocomercial, idmescomercial, idcentroservicio,
                                                         idtipoatencion, idpuntoatencion, idusuario, fechapago));
            }
            return _objlistaingresoprovision;
        }

        #endregion

    }

    public class clsOrdenCobroDocumento
    {
        #region Campos

        private Int32 _intidordencobrodoc;
        private Int32 _intidordencobro;
        private Int16? _intidempresa;                        //Cambio realizado por el IdEmpresa - 04/11/2008
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private Int16 _intidtipoidentidadpropietario;
        private String _strnroidentidadpropietario;
        private Int16 _intidmoneda;
        private Decimal _decimporte;
        private DateTime _datfecharegistro;
        private Int32 _intidusuario;
        private Int32 _intidnroservicio;
        private Int16 _intidservicioprincipal;
        private Int16 _intidmovimientocomercial;
        private String _stridnromovimientocomercial;
        private Decimal _dectipocambio;
        private String _strdescripcion;
        private Int16 _intescobrable;
        private List<clsOrdenCobroDocumentoDetalle> _elementosdocumentosdetalle = new List<clsOrdenCobroDocumentoDetalle>();
        private Int16 _intcontrolptoatencion;
        private Int16 _indcodenlace;
        private Int16 _intesdocumentofavor;

        /// <summary>
        /// Para OC de NC - ND
        /// </summary>
        private List<clsOrdenCobroDocumentoReferencia> _elementosdocumentosreferencia = new List<clsOrdenCobroDocumentoReferencia>();
        private Int16 _intidmotivoconstitucion;
        private String _strdescripciondocfavor;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private DateTime _datfechavencimiento;
        private Decimal _dectasaigv;


        #endregion

        #region Propiedades

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsDocumentoFavor
        {
            get { return _intesdocumentofavor; }
            set { _intesdocumentofavor = value; }
        }

        /// <summary>
        /// Indica si el Documento Comercial debe asignarse su numeración
        /// por Punto de Atención (1) o por Centro de Servicio (0)
        /// </summary>
        public Int16 ControlPtoAtencion
        {
            get { return _intcontrolptoatencion; }
            set { _intcontrolptoatencion = value; }
        }

        /// <summary>
        /// IdOrdenCobroDoc.
        /// </summary>
        public Int32 IdOrdenCobroDoc
        {
            get { return _intidordencobrodoc; }
            set { _intidordencobrodoc = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobro
        {
            get { return _intidordencobro; }
            set { _intidordencobro = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16? IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// IdTipoIdentidadPropietario.
        /// </summary>
        public Int16 IdTipoIdentidadPropietario
        {
            get { return _intidtipoidentidadpropietario; }
            set { _intidtipoidentidadpropietario = value; }
        }

        /// <summary>
        /// NroIdentidadPropietario.
        /// </summary>
        public String NroIdentidadPropietario
        {
            get { return _strnroidentidadpropietario; }
            set { _strnroidentidadpropietario = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdServicioPrincipal
        {
            get { return _intidservicioprincipal; }
            set { _intidservicioprincipal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdMovimientoComercial
        {
            get { return _intidmovimientocomercial; }
            set { _intidmovimientocomercial = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String IdNroMovimientoComercial
        {
            get { return _stridnromovimientocomercial; }
            set { _stridnromovimientocomercial = value; }
        }

        /// <summary>
        /// TipoCambio.
        /// </summary>
        public Decimal TipoCambio
        {
            get { return _dectipocambio; }
            set { _dectipocambio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsCobrable
        {
            get { return _intescobrable; }
            set { _intescobrable = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<clsOrdenCobroDocumentoDetalle> ElementosDocumentosDetalle
        {
            get { return _elementosdocumentosdetalle; }
            set { _elementosdocumentosdetalle = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 CodEnlace
        {
            get { return _indcodenlace; }
            set { _indcodenlace = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<clsOrdenCobroDocumentoReferencia> ElementosDocumentosReferencia
        {
            get { return _elementosdocumentosreferencia; }
            set { _elementosdocumentosreferencia = value; }
        }

        /// <summary>
        /// Campo que se usa para el proceso de emision de NC - ND
        /// Se obtiene de OrdenCobroDocumentoAFavor
        /// </summary>
        public Int16 IdMotivoConstitucion
        {
            get { return _intidmotivoconstitucion; }
            set { _intidmotivoconstitucion = value; }
        }

        /// <summary>
        /// Campo que se usa para el proceso de emision de NC - ND
        /// Se obtiene de OrdenCobroDocumentoAFavor
        /// </summary>
        public String DescripcionDocFavor
        {
            get { return _strdescripciondocfavor; }
            set { _strdescripciondocfavor = value; }
        }

        /// <summary>
        /// Campo que se usa para el proceso de emision de NC - ND
        /// Se obtiene de OrdenCobroDocumentoAFavor
        /// </summary>
        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }

        /// <summary>
        /// Campo que se usa para el proceso de emision de NC - ND
        /// Se obtiene de OrdenCobroDocumentoAFavor
        /// </summary>
        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }

        /// <summary>
        /// Campo que se usa para el proceso de emision de NC - ND
        /// Se obtiene de OrdenCobroDocumentoAFavor
        /// </summary>
        public DateTime FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        /// <summary>
        /// Campo que se usa para el proceso de emision de NC - ND
        /// Se obtiene de OrdenCobroDocumentoAFavor
        /// </summary>
        public Decimal TasaIGV
        {
            get { return _dectasaigv; }
            set { _dectasaigv = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsOrdenCobroDocumento()
        {
        }

        /// <summary>
        /// Constructor con parametros enviados.
        /// </summary>
        public clsOrdenCobroDocumento(Int32 idordencobrodoc
                                    , Int32 idordencobro
                                    , Int16 idtipodocumento
                                    , Int16 idempresa
                                    , Decimal importe
                                    , Int16 idmoneda
                                    , String descripcion
                                    , Int16 escobrable)
        {
            this.IdOrdenCobroDoc = idordencobrodoc;
            this.IdOrdenCobro = idordencobro;
            this.IdTipoDocumento = idtipodocumento;
            this.IdEmpresa = idempresa;
            this.Importe = importe;
            this.IdMoneda = idmoneda;
            this.Descripcion = descripcion;
            this.EsCobrable = escobrable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        /// <param name="detalle"></param>
        public clsOrdenCobroDocumento(DataRow registro, DataRow[] detalle)
        {
            if (registro == null) { return; }

            AsignarRegistro(registro);

            if (detalle != null)
            {
                foreach (DataRow _drw in detalle)
                {
                    _elementosdocumentosdetalle.Add(new clsOrdenCobroDocumentoDetalle(_drw));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        /// <param name="detalle"></param>
        public clsOrdenCobroDocumento(DataRow registro, DataRow[] detalle, DataRow[] referencia)
        {
            if (registro == null) { return; }

            AsignarRegistro(registro);

            if (detalle != null)
            {
                foreach (DataRow _drw in detalle)
                {
                    _elementosdocumentosdetalle.Add(new clsOrdenCobroDocumentoDetalle(_drw));
                }
            }

            if (referencia != null)
            {
                foreach (DataRow _drw in referencia)
                {
                    _elementosdocumentosreferencia.Add(new clsOrdenCobroDocumentoReferencia(_drw));
                }
            }
        }

        /// <summary>
        /// Constructor con datarow.
        /// </summary>
        public clsOrdenCobroDocumento(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idordencobrodoc")) this.IdOrdenCobroDoc = (Int32)registro["idordencobrodoc"];
            if (registro.Table.Columns.Contains("idordencobro")) this.IdOrdenCobro = (Int32)registro["idordencobro"];
            if (registro.Table.Columns.Contains("idempresa")) this.IdEmpresa = Convert.IsDBNull(registro["idempresa"]) ? (Int16?)null : Convert.ToInt16(registro["idempresa"]);
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            if (registro.Table.Columns.Contains("nrodocumento")) this.NroDocumento = Convert.IsDBNull(registro["nrodocumento"]) ? "" : (String)registro["nrodocumento"];
            if (registro.Table.Columns.Contains("idtipoidentidadpropietario")) this.IdTipoIdentidadPropietario = (Int16)registro["idtipoidentidadpropietario"];
            if (registro.Table.Columns.Contains("nroidentidadpropietario")) this.NroIdentidadPropietario = (String)registro["nroidentidadpropietario"];
            if (registro.Table.Columns.Contains("idmoneda")) this.IdMoneda = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("importe")) this.Importe = (Decimal)registro["importe"];
            if (registro.Table.Columns.Contains("fecharegistro")) this.FechaRegistro = (DateTime)registro["fecharegistro"];
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (registro.Table.Columns.Contains("idnroservicio")) this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("idservicioprincipal")) this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            if (registro.Table.Columns.Contains("idmovimientocomercial")) this.IdMovimientoComercial = (Int16)registro["idmovimientocomercial"];
            if (registro.Table.Columns.Contains("idnromovimientocomercial")) this.IdNroMovimientoComercial = (String)registro["idnromovimientocomercial"];
            if (registro.Table.Columns.Contains("tipocambio")) this.TipoCambio = (Decimal)registro["tipocambio"];
            if (registro.Table.Columns.Contains("descripcion")) this.Descripcion = (registro["descripcion"]).ToString();
            if (registro.Table.Columns.Contains("escobrable")) this.EsCobrable = (Int16)registro["escobrable"];
            if (registro.Table.Columns.Contains("control")) this.ControlPtoAtencion = Convert.ToInt16(registro["control"]);
            if (registro.Table.Columns.Contains("esdocumentofavor")) this.EsDocumentoFavor = Convert.ToInt16(registro["esdocumentofavor"]);

            if (registro.Table.Columns.Contains("idmotivoconstitucion")) this.IdMotivoConstitucion = Convert.IsDBNull(registro["idmotivoconstitucion"]) ? (Int16)0 : Convert.ToInt16(registro["idmotivoconstitucion"]);
            if (registro.Table.Columns.Contains("descripciondocfavor")) this.DescripcionDocFavor = Convert.IsDBNull(registro["descripciondocfavor"]) ? String.Empty : registro["descripciondocfavor"].ToString();
            if (registro.Table.Columns.Contains("idtipoidentidad")) this.IdTipoIdentidad = Convert.IsDBNull(registro["idtipoidentidad"]) ? (Int16)0 : Convert.ToInt16(registro["idtipoidentidad"]);
            if (registro.Table.Columns.Contains("nroidentidad")) this.NroIdentidad = Convert.IsDBNull(registro["nroidentidad"]) ? String.Empty : registro["nroidentidad"].ToString();
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.IsDBNull(registro["fechavencimiento"]) ? DateTime.Now : Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("tasaigv")) this.TasaIGV = Convert.IsDBNull(registro["tasaigv"]) ? (Decimal)0 : Convert.ToDecimal(registro["tasaigv"]);
        }

        #endregion

        #region Metodos

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        public void AsignarRegistro(DataRow registro)
        {
            DataColumnCollection col = registro.Table.Columns;

            if (col.Contains("idordencobrodoc")) this.IdOrdenCobroDoc = (Int32)registro["idordencobrodoc"];
            if (col.Contains("idordencobro")) this.IdOrdenCobro = (Int32)registro["idordencobro"];
            if (col.Contains("idempresa")) this.IdEmpresa = Convert.IsDBNull(registro["idempresa"]) ? (Int16?)null : Convert.ToInt16(registro["idempresa"]);
            if (col.Contains("idtipodocumento")) this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            if (col.Contains("nrodocumento")) this.NroDocumento = Convert.IsDBNull(registro["nrodocumento"]) ? "" : (String)registro["nrodocumento"];
            if (col.Contains("idtipoidentidadpropietario")) this.IdTipoIdentidadPropietario = (Int16)registro["idtipoidentidadpropietario"];
            if (col.Contains("nroidentidadpropietario")) this.NroIdentidadPropietario = (String)registro["nroidentidadpropietario"];
            if (col.Contains("idmoneda")) this.IdMoneda = (Int16)registro["idmoneda"];
            if (col.Contains("importe")) this.Importe = (Decimal)registro["importe"];
            if (col.Contains("fecharegistro")) this.FechaRegistro = (DateTime)registro["fecharegistro"];
            if (col.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (col.Contains("idnroservicio")) this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (col.Contains("idservicioprincipal")) this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            if (col.Contains("idmovimientocomercial")) this.IdMovimientoComercial = (Int16)registro["idmovimientocomercial"];
            if (col.Contains("idnromovimientocomercial")) this.IdNroMovimientoComercial = (String)registro["idnromovimientocomercial"];
            if (col.Contains("tipocambio")) this.TipoCambio = (Decimal)registro["tipocambio"];
            if (col.Contains("descripcion")) this.Descripcion = (registro["descripcion"]).ToString();
            if (col.Contains("escobrable")) this.EsCobrable = (Int16)registro["escobrable"];
            if (col.Contains("control")) this.ControlPtoAtencion = Convert.ToInt16(registro["control"]);
            if (col.Contains("esdocumentofavor")) this.EsDocumentoFavor = Convert.ToInt16(registro["esdocumentofavor"]);

            if (registro.Table.Columns.Contains("idmotivoconstitucion")) this.IdMotivoConstitucion = Convert.IsDBNull(registro["idmotivoconstitucion"]) ? (Int16)0 : Convert.ToInt16(registro["idmotivoconstitucion"]);
            if (registro.Table.Columns.Contains("descripciondocfavor")) this.DescripcionDocFavor = Convert.IsDBNull(registro["descripciondocfavor"]) ? String.Empty : registro["descripciondocfavor"].ToString();
            if (registro.Table.Columns.Contains("idtipoidentidad")) this.IdTipoIdentidad = Convert.IsDBNull(registro["idtipoidentidad"]) ? (Int16)0 : Convert.ToInt16(registro["idtipoidentidad"]);
            if (registro.Table.Columns.Contains("nroidentidad")) this.NroIdentidad = Convert.IsDBNull(registro["nroidentidad"]) ? String.Empty : registro["nroidentidad"].ToString();
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.IsDBNull(registro["fechavencimiento"]) ? DateTime.Now : Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("tasaigv")) this.TasaIGV = Convert.IsDBNull(registro["tasaigv"]) ? (Decimal)0 : Convert.ToDecimal(registro["tasaigv"]);
        }
        #endregion

        #region ObtenerIngresoProvision
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idanocomercial"></param>
        /// <param name="idmescomercial"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <returns></returns>
        public clsIngresoProvision ObtenerIngresoProvision(Int16 idanocomercial
                                                         , Int16 idmescomercial
                                                         , Int16 idcentroservicio
                                                         , Int16 idtipoatencion
                                                         , Int16 idpuntoatencion)
        {
            Int16 _intidempresac = this.IdEmpresa == null ? (Int16)0 : (Int16)this.IdEmpresa;

            clsIngresoProvision _objingresoprovision = new clsIngresoProvision();
            _objingresoprovision.Sel = true;
            _objingresoprovision.FechaEmision = this.FechaRegistro;
            _objingresoprovision.FechaRegistro = this.FechaRegistro;
            _objingresoprovision.FechaVencimiento = this.FechaRegistro;
            _objingresoprovision.IdEstadoDeuda = (Int16)Estado.NoPagado;
            _objingresoprovision.IdTipoDocumento = this.IdTipoDocumento;
            _objingresoprovision.NroMesesDeuda = 0;
            _objingresoprovision.IdMonedaOrigen = this.IdMoneda;
            _objingresoprovision.IdTipoAtencion = idtipoatencion;
            _objingresoprovision.IdPuntoAtencion = idpuntoatencion;
            _objingresoprovision.IdTipoIdentidad = this.IdTipoIdentidadPropietario;
            _objingresoprovision.IdUsuario = this.IdUsuario;
            _objingresoprovision.ImporteOrigenTotal = this.Importe;
            _objingresoprovision.TipoCambio = this.TipoCambio;
            _objingresoprovision.NroIdentidad = this.NroIdentidadPropietario;
            _objingresoprovision.ImporteOrigenSaldo = this.Importe;
            _objingresoprovision.ImporteOrigenCobrar = this.Importe;
            _objingresoprovision.IdEmpresa = _intidempresac;
            _objingresoprovision.NroDocumento = this.NroDocumento;
            String _strnrodocumento = this.NroDocumento + "   ";
            _objingresoprovision.Serie = _strnrodocumento.Substring(0, 3).Trim();
            _objingresoprovision.IdServicioPrincipal = this.IdServicioPrincipal;
            _objingresoprovision.IdNroServicioPrincipal = this.IdNroServicio;
            _objingresoprovision.IdAnoComercial = idanocomercial;
            _objingresoprovision.IdCentroServicio = idcentroservicio;
            _objingresoprovision.IdMesComercial = idmescomercial;
            _objingresoprovision.IdMovimientoComercial = this.IdMovimientoComercial;
            _objingresoprovision.LoteFacturacion = 0;

            // Generamos el documento para IngresoProvisionDocumentos
            clsIngresoProvisionDocumentos _objingresoprovisiondocumentos;
            _objingresoprovisiondocumentos = new clsIngresoProvisionDocumentos(_intidempresac
                                                                             , IdTipoDocumento
                                                                             , NroDocumento
                                                                             , _objingresoprovision.Serie
                                                                             , IdTipoDocumento
                                                                             , NroDocumento
                                                                             , IdMoneda
                                                                             , TipoCambio
                                                                             , Importe
                                                                             , Importe
                                                                             , Importe
                                                                             , (Int16)Estado.NoPagado
                                                                             , IdServicioPrincipal
                                                                             , IdNroServicio
                                                                             , 0
                                                                             , 1
                                                                             , (Int16)Estado.Normal //.Pendiente <-> EGM.09/11/2009.
                                                                             , 0
                                                                             , true);

            _objingresoprovision.ListaIngresoProvisionDocumentos.Add(_objingresoprovisiondocumentos);

            //Generamos el detalle del Documento para IngresoProvisionDetalle  
            clsOrdenCobroDocumentoDetalle _item;
            IEnumerator _enume = ElementosDocumentosDetalle.GetEnumerator();
            while (_enume.MoveNext())
            {
                _item = (clsOrdenCobroDocumentoDetalle)_enume.Current;
                _objingresoprovision.ListaIngresoProvisionDetalles.Add(new clsIngresoProvisionDetalle(_intidempresac
                                                                                  , IdTipoDocumento //_item.IdTipoDocumento
                                                                                  , NroDocumento
                                                                                  , IdTipoDocumento //_item.IdTipoDocumento
                                                                                  , NroDocumento
                                                                                  , _item.IdConcepto
                                                                                  , _item.Importe
                                                                                  , 0
                                                                                  , _item.Cantidad
                                                                                  , _item.PrecioUnitario
                                                                                  , _item.PrecioUnitarioSinFose
                                                                                  , IdNroServicio, _item.IndicadorIGV));
            }

            return _objingresoprovision;
        }
        #endregion

        #region ObtenerIngresoProvisionNuevo

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idanocomercial"></param>
        /// <param name="idmescomercial"></param>
        /// <param name="idcentroservicio"></param>
        /// <param name="idtipoatencion"></param>
        /// <param name="idpuntoatencion"></param>
        /// <returns></returns>
        public clsIngresoProvision ObtenerIngresoProvisionNuevo(Int16 idanocomercial
                                                              , Int16 idmescomercial
                                                              , Int16 idcentroservicio
                                                              , Int16 idtipoatencion
                                                              , Int16 idpuntoatencion
                                                              , Int32 idusuario
                                                              , DateTime fechapago)
        {
            Int16 _intidempresac = this.IdEmpresa == null ? (Int16)0 : (Int16)this.IdEmpresa;

            clsIngresoProvision _objingresoprovision = new clsIngresoProvision();
            _objingresoprovision.Sel = true;
            _objingresoprovision.FechaEmision = fechapago;
            _objingresoprovision.FechaRegistro = this.FechaRegistro;
            _objingresoprovision.FechaVencimiento = fechapago;
            _objingresoprovision.IdEstadoDeuda = (Int16)Estado.NoPagado;
            _objingresoprovision.IdTipoDocumento = this.IdTipoDocumento;

            _objingresoprovision.NroMesesDeuda = 0;
            _objingresoprovision.IdMonedaOrigen = this.IdMoneda;
            _objingresoprovision.IdTipoAtencion = idtipoatencion;
            _objingresoprovision.IdPuntoAtencion = idpuntoatencion;
            _objingresoprovision.IdTipoIdentidad = this.IdTipoIdentidadPropietario;
            _objingresoprovision.IdUsuario = idusuario; // this.IdUsuario;
            _objingresoprovision.ImporteOrigenTotal = this.Importe;
            _objingresoprovision.TipoCambio = this.TipoCambio;
            _objingresoprovision.NroIdentidad = this.NroIdentidadPropietario;
            _objingresoprovision.ImporteOrigenSaldo = this.Importe;
            _objingresoprovision.ImporteOrigenCobrar = this.Importe;
            _objingresoprovision.IdEmpresa = _intidempresac;
            _objingresoprovision.NroDocumento = this.NroDocumento;
            String _strnrodocumento = this.NroDocumento + "   ";
            _objingresoprovision.Serie = _strnrodocumento.Substring(0, 3).Trim();
            _objingresoprovision.IdServicioPrincipal = this.IdServicioPrincipal;
            _objingresoprovision.IdNroServicioPrincipal = this.IdNroServicio;
            _objingresoprovision.IdAnoComercial = idanocomercial;
            _objingresoprovision.IdCentroServicio = idcentroservicio;
            _objingresoprovision.IdMesComercial = idmescomercial;
            _objingresoprovision.IdMovimientoComercial = this.IdMovimientoComercial;
            _objingresoprovision.LoteFacturacion = 0;

            // Agrupar los Documentos Sustentos y calcular su Total.
            Hashtable htDocSustento = new Hashtable();

            //adicionado 18/01/2013
            //Boolean _paraventasprepago=false;
            //if (this.ElementosDocumentosDetalle !=null && this.ElementosDocumentosDetalle.Count > 0)
            //     _paraventasprepago=ElementosDocumentosDetalle[0].ParaVentaPrepago;

            foreach (clsOrdenCobroDocumentoDetalle detalle in this.ElementosDocumentosDetalle)
            {
                if (htDocSustento.ContainsKey(detalle.IdTipoDocumentoSustento))
                    //htDocSustento[detalle.IdTipoDocumentoSustento] = Convert.ToDecimal(htDocSustento[detalle.IdTipoDocumentoSustento]) + (_paraventasprepago?detalle.ImporteFuente : detalle.Importe);
                    htDocSustento[detalle.IdTipoDocumentoSustento] = Convert.ToDecimal(htDocSustento[detalle.IdTipoDocumentoSustento]) + (detalle.Importe);
                else
                    //htDocSustento.Add(detalle.IdTipoDocumentoSustento, (_paraventasprepago ? detalle.ImporteFuente : detalle.Importe));
                    htDocSustento.Add(detalle.IdTipoDocumentoSustento, (detalle.Importe));
            }

            clsIngresoProvisionDocumentos _objingresoprovisiondocumentos;

            if (htDocSustento.Count > 0)
            {
                // Generamos los documentos para IngresoProvisionDocumentos 
                // de acuerdo a la cantidad de elementos del HashTable.
                foreach (DictionaryEntry elemento in htDocSustento)
                {
                    _objingresoprovisiondocumentos = new clsIngresoProvisionDocumentos(_intidempresac
                                                                                     , IdTipoDocumento
                                                                                     , NroDocumento
                                                                                     , _objingresoprovision.Serie
                                                                                     , Convert.ToInt16(elemento.Key)
                                                                                     , NroDocumento
                                                                                     , IdMoneda
                                                                                     , TipoCambio
                                                                                     , Convert.ToDecimal(elemento.Value)
                                                                                     , Convert.ToDecimal(elemento.Value)
                                                                                     , Convert.ToDecimal(elemento.Value)
                                                                                     , (Int16)Estado.NoPagado
                                                                                     , IdServicioPrincipal
                                                                                     , IdNroServicio
                                                                                     , 0
                                                                                     , 1
                                                                                     , (Int16)Estado.Normal //.Pendiente <-> EGM.09/11/2009.
                                                                                     , 0
                                                                                     , true);

                    _objingresoprovision.ListaIngresoProvisionDocumentos.Add(_objingresoprovisiondocumentos);
                }
            }
            else
            {
                if ((this.EsDocumentoFavor == (Int16)1)
                    || (this.IdTipoDocumento == (Int16)TipoDocumentoComercial.ConvenioDePago)
                    || (this.IdTipoDocumento == (Int16)TipoDocumentoComercial.DocumentoDescuento))
                {
                    _objingresoprovisiondocumentos = new clsIngresoProvisionDocumentos(_intidempresac
                                                                                     , IdTipoDocumento
                                                                                     , NroDocumento
                                                                                     , _objingresoprovision.Serie
                                                                                     , IdTipoDocumento
                                                                                     , NroDocumento
                                                                                     , IdMoneda
                                                                                     , TipoCambio
                                                                                     , Importe
                                                                                     , Importe
                                                                                     , Importe
                                                                                     , (Int16)Estado.NoPagado
                                                                                     , IdServicioPrincipal
                                                                                     , IdNroServicio
                                                                                     , 0
                                                                                     , 1
                                                                                     , (Int16)Estado.Normal //.Pendiente <-> EGM.09/11/2009.
                                                                                     , 0
                                                                                     , true);

                    _objingresoprovision.ListaIngresoProvisionDocumentos.Add(_objingresoprovisiondocumentos);
                }
                else
                {
                    throw new Exception("<<<No existen conceptos para el Documento a crear.>>>");
                }
            }

            //Generamos el detalle del Documento para IngresoProvisionDetalle  
            clsOrdenCobroDocumentoDetalle _item;
            IEnumerator _enume = ElementosDocumentosDetalle.GetEnumerator();
            while (_enume.MoveNext())
            {
                _item = (clsOrdenCobroDocumentoDetalle)_enume.Current;

                clsIngresoProvisionDetalleComplemento oParametro = new clsIngresoProvisionDetalleComplemento();
                oParametro.IdEmpresa = _intidempresac;
                oParametro.NroDocumento = NroDocumento;

                oParametro.idTipoDocumento = IdTipoDocumento;
                oParametro.idTipoDocumentoSustento = _item.IdTipoDocumentoSustento;
                oParametro.NroDocumentoSustento = NroDocumento;
                oParametro.idConcepto = _item.IdConcepto;
                oParametro.IdUnidadMedida = 76;
                oParametro.AplicaDescuento = 0;
                oParametro.EsExonerada = 0;
                oParametro.EsRetiroPremio = 0;
                oParametro.EsRetiroTrabajador = 0;
                oParametro.idNroServicio = IdNroServicio;
                oParametro.ImporteDescuento = 0;
                oParametro.Observacion = String.Empty;

                _objingresoprovision.ListaIngresoProvisionDetalles.Add(new clsIngresoProvisionDetalle(_intidempresac
                                                                                  , IdTipoDocumento //_item.IdTipoDocumento
                                                                                  , NroDocumento
                                                                                  , _item.IdTipoDocumentoSustento // IdTipoDocumento //_item.IdTipoDocumento
                                                                                  , NroDocumento
                                                                                  , _item.IdConcepto
                                                                                  , _item.Importe
                                                                                  , 0
                                                                                  , _item.Cantidad
                                                                                  , _item.PrecioUnitario
                                                                                  , _item.PrecioUnitarioSinFose
                                                                                  , IdNroServicio
                                                                                  , _item.IndicadorIGV
                                                                                  , oParametro));

                ///oParametro
            }

            return _objingresoprovision;
        }

        #endregion
    }

    public class clsOrdenCobroDocumentoDetalle
    {
        #region Campos
        private Int32 _intidordencobrodoc;
        private Int16 _intidtipodocumentosustento;
        private Int16 _intidconcepto;
        private Decimal _decimporte;
        private Decimal _decpreciounitario;
        private Decimal? _decpreciounitariosinfose;
        private Decimal _deccantidad;
        private Int16 _intindicadorigv;

        //Campos agregados para la grilla que genera el convenio
        private String _strnombredocumento;
        private String _strnumerodocumento;
        private String _strnombreconcepto;

        private String _strdescripcionconcepto;
        private Int16 _intcodenlace;

        //modificado 18/01/2013, para correcion de pago parcial en ventas prepago
        private Decimal? _decimportefuente;
        private Boolean _bolparaventaprepago;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobroDoc
        {
            get { return _intidordencobrodoc; }
            set { _intidordencobrodoc = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoDocumentoSustento
        {
            get { return _intidtipodocumentosustento; }
            set { _intidtipodocumentosustento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdConcepto
        {
            get { return _intidconcepto; }
            set { _intidconcepto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal PrecioUnitario
        {
            get { return _decpreciounitario; }
            set { _decpreciounitario = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal? PrecioUnitarioSinFose
        {
            get { return _decpreciounitariosinfose; }
            set { _decpreciounitariosinfose = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Cantidad
        {
            get { return _deccantidad; }
            set { _deccantidad = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IndicadorIGV
        {
            get { return _intindicadorigv; }
            set { _intindicadorigv = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreDocumento
        {
            get { return _strnombredocumento; }
            set { _strnombredocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NumeroDocumento
        {
            get { return _strnumerodocumento; }
            set { _strnumerodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreConcepto
        {
            get { return _strnombreconcepto; }
            set { _strnombreconcepto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String DescripcionConcepto
        {
            get { return _strdescripcionconcepto; }
            set { _strdescripcionconcepto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 CodEnlace
        {
            get { return _intcodenlace; }
            set { _intcodenlace = value; }
        }

        //adicionado 18/01/2013 
        public Decimal? ImporteFuente
        {
            get { return _decimportefuente; }
            set { _decimportefuente = value; }
        }

        public Boolean ParaVentaPrepago
        {
            get { return _bolparaventaprepago; }
            set { _bolparaventaprepago = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsOrdenCobroDocumentoDetalle()
        {
        }

        /// <summary>
        /// Constructor con parametros enviados.
        /// </summary>
        public clsOrdenCobroDocumentoDetalle(Int32 idordencobrodoc
                                           , Int16 idconcepto
                                           , Decimal importe
                                           , Decimal preciounitario
                                           , Decimal cantidad
                                           , Int16 indicadorigv)
        {
            this.IdOrdenCobroDoc = idordencobrodoc;
            this.IdConcepto = idconcepto;
            this.Importe = importe;
            this.PrecioUnitario = preciounitario;
            this.Cantidad = cantidad;
            this.IndicadorIGV = indicadorigv;
        }

        /// <summary>
        /// Constructor con datarow.
        /// </summary>
        public clsOrdenCobroDocumentoDetalle(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idordencobrodoc")) this.IdOrdenCobroDoc = (Int32)registro["idordencobrodoc"];
            if (registro.Table.Columns.Contains("idtipodocumentosustento")) this.IdTipoDocumentoSustento = (Int16)registro["idtipodocumentosustento"];
            if (registro.Table.Columns.Contains("idconcepto")) this.IdConcepto = (Int16)registro["idconcepto"];
            if (registro.Table.Columns.Contains("importe")) this.Importe = (Decimal)registro["importe"];
            if (registro.Table.Columns.Contains("preciounitario")) this.PrecioUnitario = (Decimal)registro["preciounitario"];
            if (registro.Table.Columns.Contains("preciounitariosinfose")) this.PrecioUnitarioSinFose = Convert.IsDBNull(registro["preciounitariosinfose"]) ? (Decimal?)null : (Decimal)registro["preciounitariosinfose"];
            if (registro.Table.Columns.Contains("cantidad")) this.Cantidad = (Decimal)registro["cantidad"];
            if (registro.Table.Columns.Contains("indicadorigv")) this.IndicadorIGV = (Int16)registro["indicadorigv"];

            //Campos agregados para la grilla en convenio
            if (registro.Table.Columns.Contains("nombredocumento")) this.NombreDocumento = (String)registro["nombredocumento"];
            if (registro.Table.Columns.Contains("nrodocumento")) this.NumeroDocumento = (String)registro["nrodocumento"];
            if (registro.Table.Columns.Contains("nombreconcepto")) this.NombreConcepto = (String)registro["nombreconcepto"];
            if (registro.Table.Columns.Contains("descripcionconcepto")) this.DescripcionConcepto = (String)registro["descripcionconcepto"];

            //adicionado 18/01/2013
            ImporteFuente = registro.Table.Columns.Contains("importefuente") ? Convert.IsDBNull(registro["importefuente"]) ? (Decimal?)null : Convert.ToDecimal(registro["importefuente"]) : (Decimal?)null;
            ParaVentaPrepago = false;


        }

        #endregion
    }

    public class clsOrdenCobroDocumentoReferencia
    {
        #region Campos

        private Int32 _intidordencobrodocumentoreferencia;
        private Int32 _intidordencobrodocumento;
        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private Int16 _intidestado;
        private Int16 _intcodenlace;

        #endregion

        #region Propiedades
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobroDocumentoReferencia
        {
            get { return _intidordencobrodocumentoreferencia; }
            set { _intidordencobrodocumentoreferencia = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 IdOrdenCobroDocumento
        {
            get { return _intidordencobrodocumento; }
            set { _intidordencobrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEstado
        {
            get { return _intidestado; }
            set { _intidestado = value; }
        }

        public Int16 CodigoEnlace
        {
            get { return _intcodenlace; }
            set { _intcodenlace = value; }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public clsOrdenCobroDocumentoReferencia()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public clsOrdenCobroDocumentoReferencia(DataRow dr)
        {
            if (dr == null) { return; }

            IdOrdenCobroDocumentoReferencia = Convert.ToInt32(dr["idordencobrodocumentoreferencia"]);
            IdOrdenCobroDocumento = Convert.ToInt32(dr["idordencobrodocumento"]);
            IdEmpresa = Convert.ToInt16(dr["idempresa"]);
            IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
            NroDocumento = dr["nrodocumento"].ToString();
            IdEstado = Convert.ToInt16(dr["idestado"]);

            if (dr.Table.Columns.Contains("codigoenlace"))
                CodigoEnlace = Convert.ToInt16(dr["codigoenlace"]);
        }

        #endregion
    }

    #endregion Entidades NGC Escritorio

    #region Comprobante NGC Escritorio

    public class EListaNotaCreditoDebitoControlGestor
    {
        #region Campos

        private List<ENotaCreditoDebitoControlGestor> _objelementos = new List<ENotaCreditoDebitoControlGestor>();

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// Definición de la Propiedad "Elementos"
        /// </summary>
        public List<ENotaCreditoDebitoControlGestor> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío
        /// </summary>
        public EListaNotaCreditoDebitoControlGestor()
        {
        }

        /// <summary>
        /// Constructor para la lista
        /// </summary>
        /// <param name="entidad"></param>
        public EListaNotaCreditoDebitoControlGestor(DataTable entidad)
        {
            if (entidad == null || entidad.Rows.Count == 0) { return; }

            foreach (DataRow _drw in entidad.Rows)
            {
                Elementos.Add(new ENotaCreditoDebitoControlGestor(_drw));
            }
        }

        #endregion
    }

    public class ENotaCreditoDebitoControlGestor
    {
        #region Campos

        private Int32 _intidnroservicio;
        private Int16 _intidempresancnd;
        private Int16 _intidtipodocumentoncnd;
        private String _strabreviadocumentoncnd;
        private String _strnrodocumento;
        private Int16 _intidempresadoc;
        private Int16 _intidtipodocumentodoc;
        private String _strabreviadocumento;
        private String _strnrodocumentodoc;
        private DateTime _datfecharegistro;
        private Decimal _decimporte;
        private String _strnombreusuario;
        private String _strobservacion;
        private Int16 _intidmovimientocomercial;
        private String _strestadodocumento;
        private Int16 _intidestadodocumento;

        private Int16 _intescontabilizado;
        private String _strnrovouchersap;
        private Int16 _intidcentroservicio;

        private String _strrutaarchivopdf;
        private String _strrutaarchivoxml;
        private String _strcodigorespuestasunat;
        private String _strmensajerespuestasunat;
        private String _strrutarespuestasunat;

        private Int16 _inttienexml;
        private Int16 _inttienepdf;

        private String _strnombreempresa;
        private String _strnombreunidadnegocio;
        private String _strnombrecentroservicio;
        private Int16 _intesfacturadoradministrativo;
        private Int16 _inttransfieresunat;
        private Int16 _intaceptadosunat;
        private DateTime _datfechaemision;

        private String _strnombrecliente;
        private String _strdocumentoidentidad;
        private Boolean _bolaceptadosunat;
        private String _strfechaemision;
        private String _strimporte;
        #endregion

        #region Propiedades
        public String CadenaImporte
        {
            get { return _strimporte; }
            set { _strimporte = value; }
        }

        public String CadenaFechaEmision
        {
            get { return _strfechaemision; }
            set { _strfechaemision = value; }
        }

        public Boolean BolAceptadoSUNAT
        {
            get { return _bolaceptadosunat; }
            set { _bolaceptadosunat = value; }
        }

        public Int16 AceptadoSUNAT
        {
            get { return _intaceptadosunat; }
            set { _intaceptadosunat = value; }
        }

        public Int16 TransfiereSUNAT
        {
            get { return _inttransfieresunat; }
            set { _inttransfieresunat = value; }
        }

        public String NombreEmpresa
        {
            get { return _strnombreempresa; }
            set { _strnombreempresa = value; }
        }

        public String NombreUnidadNegocio
        {
            get { return _strnombreunidadnegocio; }
            set { _strnombreunidadnegocio = value; }
        }

        public String NombreCentroServicio
        {
            get { return _strnombrecentroservicio; }
            set { _strnombrecentroservicio = value; }
        }

        public Int16 EsFacturadorAdministrativo
        {
            get { return _intesfacturadoradministrativo; }
            set { _intesfacturadoradministrativo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicio; }
            set { _intidnroservicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresaNCND
        {
            get { return _intidempresancnd; }
            set { _intidempresancnd = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoDocumentoNCND
        {
            get { return _intidtipodocumentoncnd; }
            set { _intidtipodocumentoncnd = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String AbreviaDocumentoNCND
        {
            get { return _strabreviadocumentoncnd; }
            set { _strabreviadocumentoncnd = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdEmpresaDoc
        {
            get { return _intidempresadoc; }
            set { _intidempresadoc = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdTipoDocumentoDoc
        {
            get { return _intidtipodocumentodoc; }
            set { _intidtipodocumentodoc = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String AbreviaDocumento
        {
            get { return _strabreviadocumento; }
            set { _strabreviadocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NroDocumentoDoc
        {
            get { return _strnrodocumentodoc; }
            set { _strnrodocumentodoc = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Decimal Importe
        {
            get { return _decimporte; }
            set { _decimporte = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String NombreUsuario
        {
            get { return _strnombreusuario; }
            set { _strnombreusuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Observacion
        {
            get { return _strobservacion; }
            set { _strobservacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 IdMovimientoComercial
        {
            get { return _intidmovimientocomercial; }
            set { _intidmovimientocomercial = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String EstadoDocumento
        {
            get { return _strestadodocumento; }
            set { _strestadodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdEstadoDocumento
        {
            get { return _intidestadodocumento; }
            set { _intidestadodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NroVoucherSAP
        {
            get { return _strnrovouchersap; }
            set { _strnrovouchersap = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsContabilizado
        {
            get { return _intescontabilizado; }
            set { _intescontabilizado = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdCentroServicio
        {
            get { return _intidcentroservicio; }
            set { _intidcentroservicio = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        public String RutaRespuestaSUNAT
        {
            get { return _strrutarespuestasunat; }
            set { _strrutarespuestasunat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MensajeRespuestaSUNAT
        {
            get { return _strmensajerespuestasunat; }
            set { _strmensajerespuestasunat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String CodigoRespuestaSUNAT
        {
            get { return _strcodigorespuestasunat; }
            set { _strcodigorespuestasunat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String RutaArchivoXML
        {
            get { return _strrutaarchivoxml; }
            set { _strrutaarchivoxml = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String RutaArchivoPDF
        {
            get { return _strrutaarchivopdf; }
            set { _strrutaarchivopdf = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 TieneXML
        {
            get { return _inttienexml; }
            set { _inttienexml = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16 TienePDF
        {
            get { return _inttienepdf; }
            set { _inttienepdf = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime FechaEmision
        {
            get { return _datfechaemision; }
            set { _datfechaemision = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DocumentoIdentidad
        {
            get { return _strdocumentoidentidad; }
            set { _strdocumentoidentidad = value; }
        }

        public String Ticket { get; set; }
        public Boolean BolAceptadoBajaSUNAT { get; set; }
        public String TicketBaja { get; set; }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor vacío por defecto.
        /// </summary>
        public ENotaCreditoDebitoControlGestor()
        {
        }

        /// <summary>
        /// Constructor pasando como parámetro un DATAROW.
        /// </summary>
        /// <param name="dr"></param>
        public ENotaCreditoDebitoControlGestor(DataRow dr)
        {
            if (dr == null) { return; }

            IdNroServicio = Convert.ToInt32(dr["idnroservicio"]);
            IdEmpresaNCND = Convert.ToInt16(dr["idempresancnd"]);

            if (dr.Table.Columns.Contains("idtipodocumento"))
                IdTipoDocumentoNCND = Convert.ToInt16(dr["idtipodocumento"]);
            else if (dr.Table.Columns.Contains("idtipodocumentoncnd"))
                IdTipoDocumentoNCND = Convert.ToInt16(dr["idtipodocumentoncnd"]);

            AbreviaDocumentoNCND = dr["abreviadocumentoncnd"].ToString();
            NroDocumento = dr["nrodocumento"].ToString();
            IdEmpresaDoc = Convert.ToInt16(dr["idempresadoc"]);
            IdTipoDocumentoDoc = Convert.ToInt16(dr["idtipodocumentodoc"]);
            AbreviaDocumento = dr["abreviadocumento"].ToString();
            NroDocumentoDoc = dr["nrodocumentodoc"].ToString();
            FechaRegistro = Convert.ToDateTime(dr["fecharegistro"]);
            Importe = Convert.ToDecimal(dr["importe"]);
            NombreUsuario = dr["nombreusuario"].ToString();
            Observacion = dr["observacion"].ToString();
            IdMovimientoComercial = Convert.ToInt16(dr["idmovimientocomercial"]);
            EstadoDocumento = dr["estadodocumento"].ToString();
            IdEstadoDocumento = Convert.ToInt16(dr["idestadodocumento"]);

            if (dr.Table.Columns.Contains("escontabilizado"))
                EsContabilizado = Convert.ToInt16(dr["escontabilizado"]);

            if (dr.Table.Columns.Contains("nrovouchersap"))
                NroVoucherSAP = dr["nrovouchersap"].ToString();

            if (dr.Table.Columns.Contains("idcentroservicio"))
                IdCentroServicio = Convert.ToInt16(dr["idcentroservicio"]);

            if (dr.Table.Columns.Contains("rutarespuestasunat"))
                RutaRespuestaSUNAT = dr["rutarespuestasunat"].ToString();

            if (dr.Table.Columns.Contains("mensajerespuestasunat"))
                MensajeRespuestaSUNAT = dr["mensajerespuestasunat"].ToString();

            if (dr.Table.Columns.Contains("codigorespuestasunat"))
                CodigoRespuestaSUNAT = dr["codigorespuestasunat"].ToString();

            if (dr.Table.Columns.Contains("rutaarchivoxml"))
                RutaArchivoXML = dr["rutaarchivoxml"].ToString();

            if (dr.Table.Columns.Contains("rutaarchivopdf"))
                RutaArchivoPDF = dr["rutaarchivopdf"].ToString();

            if (dr.Table.Columns.Contains("tienexml"))
                TieneXML = Convert.ToInt16(dr["tienexml"]);

            if (dr.Table.Columns.Contains("tienepdf"))
                TienePDF = Convert.ToInt16(dr["tienepdf"]);

            if (dr.Table.Columns.Contains("nombreempresa"))
                NombreEmpresa = dr["nombreempresa"].ToString();

            if (dr.Table.Columns.Contains("nombreunidadnegocio"))
                NombreUnidadNegocio = dr["nombreunidadnegocio"].ToString();

            if (dr.Table.Columns.Contains("nombrecentroservicio"))
                NombreCentroServicio = dr["nombrecentroservicio"].ToString();

            if (dr.Table.Columns.Contains("esfacturadoradministrativo"))
                EsFacturadorAdministrativo = Convert.ToInt16(dr["esfacturadoradministrativo"]);

            if (dr.Table.Columns.Contains("transfieresunat"))
                TransfiereSUNAT = Convert.ToInt16(dr["transfieresunat"]);

            if (dr.Table.Columns.Contains("aceptadosunat"))
                AceptadoSUNAT = Convert.ToInt16(dr["aceptadosunat"]);

            FechaEmision = Convert.ToDateTime(dr["fechaemision"]);

            if (dr.Table.Columns.Contains("aceptadosunat"))
                AceptadoSUNAT = Convert.ToInt16(dr["aceptadosunat"]);

            if (dr.Table.Columns.Contains("nombrecliente"))
                NombreCliente = dr["nombrecliente"].ToString();

            if (dr.Table.Columns.Contains("documentoidentidad"))
                DocumentoIdentidad = dr["documentoidentidad"].ToString();

            if (dr.Table.Columns.Contains("aceptadosunat"))
                BolAceptadoSUNAT = Convert.ToBoolean(Convert.ToInt16(dr["aceptadosunat"]));
            else if (dr.Table.Columns.Contains("bolaceptadosunat"))
                BolAceptadoSUNAT = Convert.ToBoolean(dr["bolaceptadosunat"]);

            if (dr.Table.Columns.Contains("fechaemision"))
                CadenaFechaEmision = Convert.ToDateTime(dr["fechaemision"]).ToShortDateString();
            else if (dr.Table.Columns.Contains("CadenaFechaEmision"))
                CadenaFechaEmision = dr["fechaemision"].ToString();

            if (dr.Table.Columns.Contains("importe"))
                CadenaImporte = Convert.ToDecimal(dr["importe"]).ToString("N2", CultureInfo.InvariantCulture);
            else if (dr.Table.Columns.Contains("importe"))
                CadenaImporte = dr["importe"].ToString();

            if (dr.Table.Columns.Contains("aceptadobajasunat"))
                BolAceptadoBajaSUNAT = Convert.ToBoolean(dr["aceptadobajasunat"]);

            if (dr.Table.Columns.Contains("ticket"))
                Ticket = dr["ticket"].ToString();

            if (dr.Table.Columns.Contains("ticketbaja"))
                TicketBaja = dr["ticketbaja"].ToString();
        }

        #endregion
    }


    public interface ICombo
    {
        Int16 Id { get; set; }

        String Nombre { get; set; }
    }

    public abstract class clsEntidadMaestro : ICombo
    {
        protected Int16 _intid;
        protected String _strnombre;
        protected Boolean _bolseleccionado = false;

        #region ICombo Members

        /// <summary>
        /// Id
        /// </summary>
        ///
        //[Campo()]
        public virtual Int16 Id
        {
            get { return _intid; }
            set { _intid = value; }
        }

        /// <summary>
        /// Nombre
        /// </summary>
        ///
        //[Campo()]
        public String Nombre
        {
            get { return _strnombre; }
            set { _strnombre = value; }
        }

        public Boolean Seleccionado
        {
            get { return _bolseleccionado; }
            set { _bolseleccionado = value; }
        }

        #endregion ICombo Members
    }

    public class clsDireccionCodificada
    {
        #region Campos

        private Int32 _intiddireccioncodificada;
        private Int16 _intiddepartamento;
        private Int16 _intidprovincia;
        private Int16 _intiddistrito;
        private Int16 _intidtipogrupohabitacional;
        private Int32 _intidgrupohabitacional;
        private String _stretapa;
        private Int16 _intidtipovia;
        private Int32 _intidvia;
        private String _strnumero;
        private String _strlote;
        private Int16? _intidtipointerior;
        private String _strnrointerior;
        private Int16 _intesverificado;
        private String _strnombrevia;
        private String _strnombretipovia;
        private String _strnombregrupo;
        private String _strnombretipogrupo;

        private String _strdireccion;
        private String _strdireccioncomplementaria;
        //private Int16? _intidempresa;

        private String _strubigeo;

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// idDireccionCodificada.
        /// </summary>
        public Int32 idDireccionCodificada
        {
            get { return _intiddireccioncodificada; }
            set { _intiddireccioncodificada = value; }
        }

        /// <summary>
        /// idDepartamento.
        /// </summary>
        public Int16 idDepartamento
        {
            get { return _intiddepartamento; }
            set { _intiddepartamento = value; }
        }

        /// <summary>
        /// idProvincia.
        /// </summary>
        public Int16 idProvincia
        {
            get { return _intidprovincia; }
            set { _intidprovincia = value; }
        }

        /// <summary>
        /// idDistrito.
        /// </summary>
        public Int16 idDistrito
        {
            get { return _intiddistrito; }
            set { _intiddistrito = value; }
        }

        /// <summary>
        /// idTipoGrupoHabitacional.
        /// </summary>
        public Int16 idTipoGrupoHabitacional
        {
            get { return _intidtipogrupohabitacional; }
            set { _intidtipogrupohabitacional = value; }
        }

        /// <summary>
        /// idGrupoHabitacional.
        /// </summary>
        public Int32 idGrupoHabitacional
        {
            get { return _intidgrupohabitacional; }
            set { _intidgrupohabitacional = value; }
        }

        /// <summary>
        /// Etapa.
        /// </summary>
        public String Etapa
        {
            get { return _stretapa; }
            set { _stretapa = value; }
        }

        /// <summary>
        /// idTipoVia.
        /// </summary>
        public Int16 idTipoVia
        {
            get { return _intidtipovia; }
            set { _intidtipovia = value; }
        }

        /// <summary>
        /// idVia.
        /// </summary>
        public Int32 idVia
        {
            get { return _intidvia; }
            set { _intidvia = value; }
        }

        /// <summary>
        /// Numero
        /// </summary>
        public String Numero
        {
            get { return _strnumero; }
            set { _strnumero = value; }
        }

        /// <summary>
        /// Lote.
        /// </summary>
        public String Lote
        {
            get { return _strlote; }
            set { _strlote = value; }
        }

        /// <summary>
        /// IdTipoInterior.
        /// </summary>
        public Int16? IdTipoInterior
        {
            get { return _intidtipointerior; }
            set { _intidtipointerior = value; }
        }

        /// <summary>
        /// NroInterior.
        /// </summary>
        public String NroInterior
        {
            get { return _strnrointerior; }
            set { _strnrointerior = value; }
        }

        /// <summary>
        /// EsVerificado.
        /// </summary>
        public Int16 EsVerificado
        {
            get { return _intesverificado; }
            set { _intesverificado = value; }
        }

        /// <summary>
        /// NombreTipoGrupo.
        /// </summary>
        public String NombreTipoGrupo
        {
            get { return _strnombretipogrupo; }
            set { _strnombretipogrupo = value; }
        }

        /// <summary>
        /// NombreGrupo.
        /// </summary>
        public String NombreGrupo
        {
            get { return _strnombregrupo; }
            set { _strnombregrupo = value; }
        }

        /// <summary>
        /// NombreVia.
        /// </summary>
        public String NombreVia
        {
            get { return _strnombrevia; }
            set { _strnombrevia = value; }
        }

        /// <summary>
        /// NombreTipoVia.
        /// </summary>
        public String NombreTipoVia
        {
            get { return _strnombretipovia; }
            set { _strnombretipovia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String DireccionComplementaria
        {
            get { return _strdireccioncomplementaria; }
            set { _strdireccioncomplementaria = value; }
        }

        ///// <summary>
        /////
        ///// </summary>
        //public Int16? IdEmpresa
        //{
        //    get { return _intidempresa; }
        //    set { _intidempresa = value; }
        //}

        /// <summary>
        /// 
        /// </summary>
        public String Ubigeo
        {
            get { return _strubigeo; }
            set { _strubigeo = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacío.
        /// </summary>
        public clsDireccionCodificada()
        {
        }

        /// <summary>
        /// clsDireccionCodificada.- con el ID de la dirección codificada.
        /// </summary>
        /// <param name="iddireccioncodificada"></param>
        /// <param name="iddepartamento"></param>
        /// <param name="idprovincia"></param>
        /// <param name="iddistrito"></param>
        /// <param name="idtipogrupohabitacional"></param>
        /// <param name="idgrupohabitacional"></param>
        /// <param name="etapa"></param>
        /// <param name="idtipovia"></param>
        /// <param name="idvia"></param>
        /// <param name="numero"></param>
        /// <param name="lote"></param>
        /// <param name="idtipointerior"></param>
        /// <param name="nrointerior"></param>
        /// <param name="esverificado"></param>
        public clsDireccionCodificada(Int32 iddireccioncodificada
                                    , Int16 iddepartamento
                                    , Int16 idprovincia
                                    , Int16 iddistrito
                                    , Int16 idtipogrupohabitacional
                                    , Int32 idgrupohabitacional
                                    , String etapa
                                    , Int16 idtipovia
                                    , Int32 idvia
                                    , String numero
                                    , String lote
                                    , Int16? idtipointerior
                                    , String nrointerior
                                    , Int16 esverificado)
        {
            this.idDireccionCodificada = iddireccioncodificada;
            this.idDepartamento = iddepartamento;
            this.idProvincia = idprovincia;
            this.idDistrito = iddistrito;
            this.idTipoGrupoHabitacional = idtipogrupohabitacional;
            this.idGrupoHabitacional = idgrupohabitacional;
            this.Etapa = etapa;
            this.idTipoVia = idtipovia;
            this.idVia = idvia;
            this.Numero = numero;
            this.Lote = lote;
            this.IdTipoInterior = idtipointerior;
            this.NroInterior = nrointerior;
            this.EsVerificado = esverificado;
        }


        public clsDireccionCodificada(Int32 iddireccioncodificada
                                    , Int16 iddepartamento
                                    , Int16 idprovincia
                                    , Int16 iddistrito
                                    , Int16 idtipogrupohabitacional
                                    , Int32 idgrupohabitacional
                                    , String etapa
                                    , Int16 idtipovia
                                    , Int32 idvia
                                    , String numero
                                    , String lote
                                    , Int16? idtipointerior
                                    , String nrointerior
                                    , Int16 esverificado
                                    , String strDireccion
                                    , String strDireccionComplementaria
                                    , Boolean bolEsDireccionNueva)
        {
            this.idDireccionCodificada = !bolEsDireccionNueva ? iddireccioncodificada : (Int32)0;
            this.idDepartamento = iddepartamento;
            this.idProvincia = idprovincia;
            this.idDistrito = iddistrito;
            this.idTipoGrupoHabitacional = idtipogrupohabitacional;
            this.idGrupoHabitacional = idgrupohabitacional;
            this.Etapa = etapa;
            this.idTipoVia = idtipovia;
            this.idVia = idvia;
            this.Numero = numero;
            this.Lote = lote;
            this.IdTipoInterior = idtipointerior;
            this.NroInterior = nrointerior;
            this.EsVerificado = esverificado;
            this.Direccion = strDireccion;
            this.DireccionComplementaria = strDireccionComplementaria;
        }

        /// <summary>
        /// clsDireccionCodificada.- sin el ID de la dirección codificada.
        /// </summary>
        /// <param name="iddepartamento"></param>
        /// <param name="idprovincia"></param>
        /// <param name="iddistrito"></param>
        /// <param name="idtipogrupohabitacional"></param>
        /// <param name="idgrupohabitacional"></param>
        /// <param name="nombregrupohabitacional"></param>
        /// <param name="etapa"></param>
        /// <param name="idtipovia"></param>
        /// <param name="idvia"></param>
        /// <param name="nombrevia"></param>
        /// <param name="numero"></param>
        /// <param name="lote"></param>
        /// <param name="idtipointerior"></param>
        /// <param name="nrointerior"></param>
        /// <param name="esverificado"></param>
        public clsDireccionCodificada(Int16 iddepartamento
                                    , Int16 idprovincia
                                    , Int16 iddistrito
                                    , Int16 idtipogrupohabitacional
                                    , Int32 idgrupohabitacional
                                    , String nombregrupohabitacional
                                    , String etapa
                                    , Int16 idtipovia
                                    , Int32 idvia
                                    , String nombrevia
                                    , String numero
                                    , String lote
                                    , Int16? idtipointerior
                                    , String nrointerior
                                    , Int16 esverificado)
        {
            this.idDepartamento = iddepartamento;
            this.idProvincia = idprovincia;
            this.idDistrito = iddistrito;
            this.idTipoGrupoHabitacional = idtipogrupohabitacional;
            this.idGrupoHabitacional = idgrupohabitacional;
            this.NombreGrupo = nombregrupohabitacional;
            this.Etapa = etapa;
            this.idTipoVia = idtipovia;
            this.idVia = idvia;
            this.NombreVia = nombrevia;
            this.Numero = numero;
            this.Lote = lote;
            this.IdTipoInterior = idtipointerior;
            this.NroInterior = nrointerior;
            this.EsVerificado = esverificado;
        }

        /// <summary>
        /// Constructor considerando los campos Direccion y DireccionComplementaria
        /// para el uso en el user control ucDireccionEstructurada.
        /// </summary>
        /// <param name="iddepartamento"></param>
        /// <param name="idprovincia"></param>
        /// <param name="iddistrito"></param>
        /// <param name="idtipogrupohabitacional"></param>
        /// <param name="idgrupohabitacional"></param>
        /// <param name="nombregrupohabitacional"></param>
        /// <param name="etapa"></param>
        /// <param name="idtipovia"></param>
        /// <param name="idvia"></param>
        /// <param name="nombrevia"></param>
        /// <param name="numero"></param>
        /// <param name="lote"></param>
        /// <param name="idtipointerior"></param>
        /// <param name="nrointerior"></param>
        /// <param name="esverificado"></param>
        /// <param name="direccion"></param>
        /// <param name="direccioncomplementaria"></param>
        public clsDireccionCodificada(Int16 iddepartamento
                                    , Int16 idprovincia
                                    , Int16 iddistrito
                                    , Int16 idtipogrupohabitacional
                                    , Int32 idgrupohabitacional
                                    , String nombregrupohabitacional
                                    , String etapa
                                    , Int16 idtipovia
                                    , Int32 idvia
                                    , String nombrevia
                                    , String numero
                                    , String lote
                                    , Int16? idtipointerior
                                    , String nrointerior
                                    , Int16 esverificado
                                    , String direccion
                                    , String direccioncomplementaria
                                    , String ubigeo)
        //, Int16? idempresa)
        {
            this.idDepartamento = iddepartamento;
            this.idProvincia = idprovincia;
            this.idDistrito = iddistrito;
            this.idTipoGrupoHabitacional = idtipogrupohabitacional;
            this.idGrupoHabitacional = idgrupohabitacional;
            this.NombreGrupo = nombregrupohabitacional;
            this.Etapa = etapa;
            this.idTipoVia = idtipovia;
            this.idVia = idvia;
            this.NombreVia = nombrevia;
            this.Numero = numero;
            this.Lote = lote;
            this.IdTipoInterior = idtipointerior;
            this.NroInterior = nrointerior;
            this.EsVerificado = esverificado;
            this.Direccion = direccion;
            this.DireccionComplementaria = direccioncomplementaria;
            this.Ubigeo = ubigeo;
            //this.IdEmpresa = (Int16?)idempresa;
        }

        /// <summary>
        /// Todos los campos de la clase.
        /// </summary>
        /// <param name="iddireccioncodificada"></param>
        /// <param name="iddepartamento"></param>
        /// <param name="idprovincia"></param>
        /// <param name="iddistrito"></param>
        /// <param name="idtipogrupohabitacional"></param>
        /// <param name="idgrupohabitacional"></param>
        /// <param name="nombregrupohabitacional"></param>
        /// <param name="etapa"></param>
        /// <param name="idtipovia"></param>
        /// <param name="idvia"></param>
        /// <param name="nombrevia"></param>
        /// <param name="numero"></param>
        /// <param name="lote"></param>
        /// <param name="idtipointerior"></param>
        /// <param name="nrointerior"></param>
        /// <param name="esverificado"></param>
        /// <param name="direccion"></param>
        /// <param name="direccioncomplementaria"></param>
        public clsDireccionCodificada(Int32 iddireccioncodificada
                                    , Int16 iddepartamento
                                    , Int16 idprovincia
                                    , Int16 iddistrito
                                    , Int16 idtipogrupohabitacional
                                    , Int32 idgrupohabitacional
                                    , String nombregrupohabitacional
                                    , String etapa
                                    , Int16 idtipovia
                                    , Int32 idvia
                                    , String nombrevia
                                    , String numero
                                    , String lote
                                    , Int16? idtipointerior
                                    , String nrointerior
                                    , Int16 esverificado
                                    , String direccion
                                    , String direccioncomplementaria)
        {
            this.idDireccionCodificada = iddireccioncodificada;
            this.idDepartamento = iddepartamento;
            this.idProvincia = idprovincia;
            this.idDistrito = iddistrito;
            this.idTipoGrupoHabitacional = idtipogrupohabitacional;
            this.idGrupoHabitacional = idgrupohabitacional;
            this.NombreGrupo = nombregrupohabitacional;
            this.Etapa = etapa;
            this.idTipoVia = idtipovia;
            this.idVia = idvia;
            this.NombreVia = nombrevia;
            this.Numero = numero;
            this.Lote = lote;
            this.IdTipoInterior = idtipointerior;
            this.NroInterior = nrointerior;
            this.EsVerificado = esverificado;
            this.Direccion = direccion;
            this.DireccionComplementaria = direccioncomplementaria;
        }

        public clsDireccionCodificada(Int32 iddireccioncodificada
                                    , Int16 iddepartamento
                                    , Int16 idprovincia
                                    , Int16 iddistrito
                                    , Int16 idtipogrupohabitacional
                                    , Int32 idgrupohabitacional
                                    , String nombregrupohabitacional
                                    , String etapa
                                    , Int16 idtipovia
                                    , Int32 idvia
                                    , String nombrevia
                                    , String numero
                                    , String lote
                                    , Int16? idtipointerior
                                    , String nrointerior
                                    , Int16 esverificado
                                    , String direccion
                                    , String direccioncomplementaria
                                    , String ubigeo)
        {
            this.idDireccionCodificada = iddireccioncodificada;
            this.idDepartamento = iddepartamento;
            this.idProvincia = idprovincia;
            this.idDistrito = iddistrito;
            this.idTipoGrupoHabitacional = idtipogrupohabitacional;
            this.idGrupoHabitacional = idgrupohabitacional;
            this.NombreGrupo = nombregrupohabitacional;
            this.Etapa = etapa;
            this.idTipoVia = idtipovia;
            this.idVia = idvia;
            this.NombreVia = nombrevia;
            this.Numero = numero;
            this.Lote = lote;
            this.IdTipoInterior = idtipointerior;
            this.NroInterior = nrointerior;
            this.EsVerificado = esverificado;
            this.Direccion = direccion;
            this.DireccionComplementaria = direccioncomplementaria;
            this.Ubigeo = ubigeo;
        }

        /// <summary>
        /// clsDireccionCodificada.- con un datarow.
        /// </summary>
        /// <param name="registro"></param>
        public clsDireccionCodificada(DataRow registro)
        {
            if (registro == null) { return; }

            idDireccionCodificada = (Int32)registro["iddireccioncodificada"];
            idDepartamento = (Int16)registro["iddepartamento"];
            idProvincia = (Int16)registro["idprovincia"];
            idDistrito = (Int16)registro["iddistrito"];
            idTipoGrupoHabitacional = (Int16)registro["idtipogrupohabitacional"];
            NombreTipoGrupo = registro["nombretipogrupo"].ToString();
            idGrupoHabitacional = (Int32)registro["idgrupohabitacional"];
            NombreGrupo = registro["nombregrupohabitacional"].ToString();
            Etapa = (registro.IsNull("etapa") ? null : registro["etapa"].ToString());
            idTipoVia = (Int16)registro["idtipovia"];
            NombreTipoVia = registro["nombretipovia"].ToString();
            idVia = (Int32)registro["idvia"];
            NombreVia = registro["nombrevia"].ToString();
            Numero = (registro.IsNull("numero") ? null : registro["numero"].ToString());
            Lote = (registro.IsNull("lote") ? null : registro["lote"].ToString());
            IdTipoInterior = (registro.IsNull("idtipointerior") ? (Int16?)null : (Int16?)registro["idtipointerior"]);
            NroInterior = (registro.IsNull("nrointerior") ? null : registro["nrointerior"].ToString());
            EsVerificado = (Int16)registro["esverificado"];
            Direccion = registro["direccion"].ToString();
            DireccionComplementaria = registro["direccioncomplementaria"].ToString();
            //IdEmpresa = registro.IsNull("idempresa") ? (Int16?)null : (Int16?)registro["idempresa"];
        }

        public clsDireccionCodificada(IDataRecord registro)
        {
            if (registro == null || !(registro as SqlDataReader).HasRows) { return; }

            idDireccionCodificada = (Int32)registro["iddireccioncodificada"];
            idDepartamento = (Int16)registro["iddepartamento"];
            idProvincia = (Int16)registro["idprovincia"];
            idDistrito = (Int16)registro["iddistrito"];
            idTipoGrupoHabitacional = (Int16)registro["idtipogrupohabitacional"];
            NombreTipoGrupo = registro["nombretipogrupo"].ToString();
            idGrupoHabitacional = (Int32)registro["idgrupohabitacional"];
            NombreGrupo = registro["nombregrupohabitacional"].ToString();
            Etapa = (Convert.IsDBNull(registro["etapa"]) ? null : registro["etapa"].ToString());
            idTipoVia = (Int16)registro["idtipovia"];
            NombreTipoVia = registro["nombretipovia"].ToString();
            idVia = (Int32)registro["idvia"];
            NombreVia = registro["nombrevia"].ToString();
            Numero = (Convert.IsDBNull(registro["numero"]) ? null : registro["numero"].ToString());
            Lote = (Convert.IsDBNull(registro["lote"]) ? null : registro["lote"].ToString());
            IdTipoInterior = (Convert.IsDBNull(registro["idtipointerior"]) ? (Int16?)null : (Int16?)registro["idtipointerior"]);
            NroInterior = (Convert.IsDBNull(registro["nrointerior"]) ? null : registro["nrointerior"].ToString());
            EsVerificado = (Int16)registro["esverificado"];
            Direccion = registro["direccion"].ToString();
            DireccionComplementaria = registro["direccioncomplementaria"].ToString();
            //IdEmpresa = registro.IsNull("idempresa") ? (Int16?)null : (Int16?)registro["idempresa"];
        }

        #endregion Constructor
    }

    public class clsEmpresa : clsEntidadMaestro
    {
        #region Campos

        private String _strabreviaempresa;
        private String _strpartidaregistral;
        private Int16 _intestado;
        private Int16 _intidorganizacion;
        private String _strruc;

        private String _strnombrecompleto;
        private String _strdireccion;
        private String _strdireccioncomplementaria;
        private Int32 _intdireccioncodificada;
        private clsDireccionCodificada _objdireccioncodificada;

        private String _strcodigoidentificacion;
        private Byte[] _objbyteimagen;
        private Byte[] _objbytefirmalegal;
        private String _strtelefono;

        #endregion Campos

        #region Propiedades

        /// <summary>
        /// IdOrganizacion.
        /// </summary>
        public Int16 IdOrganizacion
        {
            get { return _intidorganizacion; }
            set { _intidorganizacion = value; }
        }

        /// <summary>
        /// AbreviaEmpresa.
        /// </summary>
        public String AbreviaEmpresa
        {
            get { return _strabreviaempresa; }
            set { _strabreviaempresa = value; }
        }

        /// <summary>
        /// Ruc.
        /// </summary>
        public String Ruc
        {
            get { return _strruc; }
            set { _strruc = value; }
        }

        /// <summary>
        /// Estado.
        /// </summary>
        public Int16 Estado
        {
            get { return _intestado; }
            set { _intestado = value; }
        }

        /// <summary>
        /// NombreCompleto.
        /// </summary>
        public String NombreCompleto
        {
            get { return _strnombrecompleto; }
            set { _strnombrecompleto = value; }
        }

        /// <summary>
        /// Direccion.
        /// </summary>
        public String Direccion
        {
            get { return _strdireccion; }
            set { _strdireccion = value; }
        }

        /// <summary>
        /// DireccionComplementaria.
        /// </summary>
        public String DireccionComplementaria
        {
            get { return _strdireccioncomplementaria; }
            set { _strdireccioncomplementaria = value; }
        }

        /// <summary>
        /// DireccionCodificada.
        /// </summary>
        public Int32 DireccionCodificada
        {
            get { return _intdireccioncodificada; }
            set { _intdireccioncodificada = value; }
        }

        /// <summary>
        /// clsDireccionCodificada.
        /// </summary>
        public clsDireccionCodificada ODireccionCodificada
        {
            get { return _objdireccioncodificada; }
            set { _objdireccioncodificada = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String CodigoIdentificacion
        {
            get { return _strcodigoidentificacion; }
            set { _strcodigoidentificacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Byte[] Imagen
        {
            get { return _objbyteimagen; }
            set { _objbyteimagen = value; }
        }

        public Byte[] FirmaLegal
        {
            get { return _objbytefirmalegal; }
            set { _objbytefirmalegal = value; }
        }

        public String Telefono
        {
            get { return _strtelefono; }
            set { _strtelefono = value; }
        }

        public String PartidaRegistral
        {
            get { return _strpartidaregistral; }
            set { _strpartidaregistral = value; }
        }

        #endregion Propiedades

        #region Constructor

        /// <summary>
        /// Constructor vacio.
        /// </summary>
        public clsEmpresa()
        {
        }

        /// <summary>
        /// Constructor pasandole todos los atributos de la entidad.
        /// </summary>
        /// <param name="idempresa"></param>
        /// <param name="nombreempresa"></param>
        /// <param name="abreviaempresa"></param>
        /// <param name="estado"></param>
        /// <param name="idorganizacion"></param>
        /// <param name="ruc"></param>
        /// <param name="nombrecompleto"></param>
        /// <param name="direccion"></param>
        /// <param name="direccioncomplementaria"></param>
        /// <param name="direccioncodificada"></param>
        public clsEmpresa(Int16 idempresa
                        , String nombreempresa
                        , String abreviaempresa
                        , Int16 estado
                        , Int16 idorganizacion
                        , String ruc
                        , String nombrecompleto
                        , String direccion
                        , String direccioncomplementaria
                        , Int32 direccioncodificada
                        , String strcodigoidentificacion)
        {
            this.Id = idempresa;
            this.Nombre = nombreempresa;
            this.AbreviaEmpresa = abreviaempresa;
            this.IdOrganizacion = idorganizacion;
            this.Ruc = ruc;
            this.Estado = estado;

            this.NombreCompleto = nombrecompleto;
            this.Direccion = direccion;
            this.DireccionComplementaria = direccioncomplementaria;
            this.DireccionCodificada = direccioncodificada;
            this.CodigoIdentificacion = strcodigoidentificacion;
        }

        /// <summary>
        /// Constructor pasandole un datarow.
        /// </summary>
        /// <param name="registro"></param>
        public clsEmpresa(DataRow registro)
        {
            if (registro == null) return;

            this.Id = (Int16)registro["idempresa"];
            this.Nombre = registro["NombreEmpresa"].ToString();
            this.AbreviaEmpresa = registro["abreviaempresa"].ToString();
            this.IdOrganizacion = (Int16)registro["idorganizacion"];
            this.Ruc = registro["Ruc"].ToString();
            this.Estado = (Int16)registro["estado"];

            this.NombreCompleto = registro["nombrecompleto"].ToString();
            this.Direccion = registro["direccion"].ToString();
            this.DireccionComplementaria = registro["direccioncomplementaria"].ToString();
            this.DireccionCodificada = Convert.IsDBNull(registro["iddireccioncodificada"]) ? (Int32)0 : (Int32)registro["iddireccioncodificada"];

            if (registro.Table.Columns.Contains("codigoidentificacion"))
                this.CodigoIdentificacion = registro["codigoidentificacion"] == null ? (String)null : registro["codigoidentificacion"].ToString();

            if (registro.Table.Columns.Contains("imagen"))
                this.Imagen = Convert.IsDBNull(registro["imagen"]) ? (Byte[])(null) : (Byte[])registro["imagen"];

            if (registro.Table.Columns.Contains("firmalegal"))
                this.FirmaLegal = Convert.IsDBNull(registro["firmalegal"]) ? (Byte[])(null) : (Byte[])registro["firmalegal"];

            if (registro.Table.Columns.Contains("PartidaRegistral")) PartidaRegistral = registro["PartidaRegistral"] == DBNull.Value ? String.Empty : registro["PartidaRegistral"].ToString();
        }

        #endregion Constructor
    }

    public class clsComprobantePagoNGC
    {
        #region Parametros
        private Int16 _intidservicioprincipal;
        private Int32 _intidnroservicioprincipal;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;
        private String _strnombretipodocumento;
        private String _strnombrecliente;
        private String _strdireccioncliente;
        private Int16 _intidtipoidentidad;
        private String _strnroidentidad;
        private DateTime _datfecharegistro;
        private Int16 _intidmoneda;
        private Int32 _intidusuario;
        private String _strnombreusuario;
        private Int16 _intcaja;
        private String _strnombremoneda;
        private String _strsimbolomoneda;
        private String _strmensajetotal;
        private String _strnombrepuntoatencion;
        private List<clsComprobantePagoDetalleNGC> _objcomprobantepagodetalle = new List<clsComprobantePagoDetalleNGC>();
        private clsComprobantePagoConvenio _objcomprobantepagoconvenio;
        private clsListaComprobantePagoDetalle _objcomprobantepagodetalleimpresion = new clsListaComprobantePagoDetalle();

        private DateTime _datfechavencimiento;

        private Decimal _decsubtotal;
        private Decimal _decigv;
        private Decimal _decimportetotal;

        private String _strdireccioncomplementariacentroservicio;

        private Decimal _decoperacionesgravadas;
        private Decimal _decoperacionesgratuitas;
        private Decimal _decoperacionesexoneradas;
        private Decimal _decoperacionesinafectas;
        private Decimal _decdescuentostotales;
        private Decimal _decpercepcion;
        private Decimal _decmontoconpercepcion;
        private String _strobservacion;
        private String _strmensajerepresentacionelectronica;

        private Byte[] _obimagenempresa;

        private String _strdireccioncentroservicio;
        private String _strdireccionempresa;
        private String _strdireccioncomplementariaempresa;
        private String _strsiglamoneda;

        private String _strcodigobarra;
        private Int16 _intesespecial;
        private String _strdocumentoidentidad;

        private String _strnombreempresacorto;
        private String _strrucempresa;
        private String _strimportelectras;
        private Int16 _inttieneconvenio;
        private String _strrutaimagenempresa;
        private String _strrutaimagencodigobarra;
        private Int16 _intidempresa;

        #endregion

        #region Propiedades
        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public String DocumentoIdentidad
        {
            get { return _strdocumentoidentidad; }
            set { _strdocumentoidentidad = value; }
        }

        public Int16 EsEspecial
        {
            get { return _intesespecial; }
            set { _intesespecial = value; }
        }

        public String CodigoBarra
        {
            get { return _strcodigobarra; }
            set { _strcodigobarra = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionCentroServicio
        {
            get { return _strdireccioncentroservicio; }
            set { _strdireccioncentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionEmpresa
        {
            get { return _strdireccionempresa; }
            set { _strdireccionempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionComplementariaEmpresa
        {
            get { return _strdireccioncomplementariaempresa; }
            set { _strdireccioncomplementariaempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombrePuntoAtencion
        {
            get { return _strnombrepuntoatencion; }
            set { _strnombrepuntoatencion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String SimboloMoneda
        {
            get { return _strsimbolomoneda; }
            set { _strsimbolomoneda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String NombreMoneda
        {
            get { return _strnombremoneda; }
            set { _strnombremoneda = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdServicioPrincipal
        {
            get { return _intidservicioprincipal; }
            set { _intidservicioprincipal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdNroServicio
        {
            get { return _intidnroservicioprincipal; }
            set { _intidnroservicioprincipal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreTipoDocumento
        {
            get { return _strnombretipodocumento; }
            set { _strnombretipodocumento = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreCliente
        {
            get { return _strnombrecliente; }
            set { _strnombrecliente = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String DireccionCliente
        {
            get { return _strdireccioncliente; }
            set { _strdireccioncliente = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdTipoIdentidad
        {
            get { return _intidtipoidentidad; }
            set { _intidtipoidentidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NroIdentidad
        {
            get { return _strnroidentidad; }
            set { _strnroidentidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaRegistro
        {
            get { return _datfecharegistro; }
            set { _datfecharegistro = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdMoneda
        {
            get { return _intidmoneda; }
            set { _intidmoneda = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 IdUsuario
        {
            get { return _intidusuario; }
            set { _intidusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String NombreUsuario
        {
            get { return _strnombreusuario; }
            set { _strnombreusuario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IdCaja
        {
            get { return _intcaja; }
            set { _intcaja = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MensajeTotal
        {
            get { return _strmensajetotal; }
            set { _strmensajetotal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<clsComprobantePagoDetalleNGC> DetalleComprobantePago
        {
            get { return _objcomprobantepagodetalle; }
            set { _objcomprobantepagodetalle = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public clsComprobantePagoConvenio ConvenioComprobantePago
        {
            get { return _objcomprobantepagoconvenio; }
            set { _objcomprobantepagoconvenio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime FechaVencimiento
        {
            get { return _datfechavencimiento; }
            set { _datfechavencimiento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal SubTotal
        {
            get { return _decsubtotal; }
            set { _decsubtotal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal IGV
        {
            get { return _decigv; }
            set { _decigv = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ImporteTotal
        {
            get { return _decimportetotal; }
            set { _decimportetotal = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DireccionComplementariaCentroServicio
        {
            get { return _strdireccioncomplementariacentroservicio; }
            set { _strdireccioncomplementariacentroservicio = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal MontoConPercepcion
        {
            get { return _decmontoconpercepcion; }
            set { _decmontoconpercepcion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal Percepcion
        {
            get { return _decpercepcion; }
            set { _decpercepcion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesDescuentos
        {
            get { return _decdescuentostotales; }
            set { _decdescuentostotales = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesInafectas
        {
            get { return _decoperacionesinafectas; }
            set { _decoperacionesinafectas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesExoneradas
        {
            get { return _decoperacionesexoneradas; }
            set { _decoperacionesexoneradas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesGratuitas
        {
            get { return _decoperacionesgratuitas; }
            set { _decoperacionesgratuitas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal OperacionesGravadas
        {
            get { return _decoperacionesgravadas; }
            set { _decoperacionesgravadas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Observacion
        {
            get { return _strobservacion; }
            set { _strobservacion = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MensajeRepresentacionElectronica
        {
            get { return _strmensajerepresentacionelectronica; }
            set { _strmensajerepresentacionelectronica = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Byte[] ImagenEmpresa
        {
            get { return _obimagenempresa; }
            set { _obimagenempresa = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String SiglaMoneda
        {
            get { return _strsiglamoneda; }
            set { _strsiglamoneda = value; }
        }

        public String RutaImagenCodigoBarra
        {
            get { return _strrutaimagencodigobarra; }
            set { _strrutaimagencodigobarra = value; }
        }

        public String RutaImagenEmpresa
        {
            get { return _strrutaimagenempresa; }
            set { _strrutaimagenempresa = value; }
        }

        public String NombreEmpresaCorto
        {
            get { return _strnombreempresacorto; }
            set { _strnombreempresacorto = value; }
        }

        public String RUCEmpresa
        {
            get { return _strrucempresa; }
            set { _strrucempresa = value; }
        }

        public String ImporteLetras
        {
            get { return _strimportelectras; }
            set { _strimportelectras = value; }
        }

        public Int16 TieneConvenio
        {
            get { return _inttieneconvenio; }
            set { _inttieneconvenio = value; }
        }

        public clsListaComprobantePagoDetalle DetalleComprobantePagoImpresion
        {
            get { return _objcomprobantepagodetalleimpresion; }
            set { _objcomprobantepagodetalleimpresion = value; }
        }
        #endregion

        #region Constructores
        /// <summary>
        /// 
        /// </summary>
        public clsComprobantePagoNGC()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        public clsComprobantePagoNGC(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idservicioprincipal")) this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            if (registro.Table.Columns.Contains("idnroservicio")) this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            if (registro.Table.Columns.Contains("nrodocumento")) this.NroDocumento = (String)registro["nrodocumento"];
            if (registro.Table.Columns.Contains("nombredocumento")) this.NombreTipoDocumento = (String)registro["nombredocumento"];
            //if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = Convert.IsDBNull(registro["fechacaja"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechacaja"]);
            if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = (String)registro["nombrecliente"];
            if (registro.Table.Columns.Contains("direccioncliente")) this.DireccionCliente = (String)registro["direccioncliente"];
            if (registro.Table.Columns.Contains("idtipoidentidadpropietario ")) this.IdTipoIdentidad = (Int16)registro["idtipoidentidadpropietario"];
            if (registro.Table.Columns.Contains("nroidentidadpropietario")) this.NroIdentidad = (String)registro["nroidentidadpropietario"];
            if (registro.Table.Columns.Contains("fecharegistro")) this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            if (registro.Table.Columns.Contains("idmoneda")) this.IdMoneda = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (registro.Table.Columns.Contains("nombreusuario")) this.NombreUsuario = (String)registro["nombreusuario"];
            if (registro.Table.Columns.Contains("caja")) this.IdCaja = (Int16)registro["caja"];
            if (registro.Table.Columns.Contains("nombremoneda")) this.NombreMoneda = registro["nombremoneda"].ToString().Trim();
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("mensajetotal")) this.MensajeTotal = registro["mensajetotal"].ToString();
            if (registro.Table.Columns.Contains("nombrepuntoatencion")) this.NombrePuntoAtencion = registro["nombrepuntoatencion"].ToString().Trim();
            if (registro.Table.Columns.Contains("subtotal")) this.SubTotal = Convert.ToDecimal(registro["subtotal"]);
            if (registro.Table.Columns.Contains("igv")) this.IGV = Convert.ToDecimal(registro["igv"]);
            if (registro.Table.Columns.Contains("importetotal")) this.ImporteTotal = Convert.ToDecimal(registro["importetotal"]);

            if (registro.Table.Columns.Contains("montoconpercepcion")) this.MontoConPercepcion = Convert.ToDecimal(registro["montoconpercepcion"]);
            if (registro.Table.Columns.Contains("percepcion")) this.Percepcion = Convert.ToDecimal(registro["percepcion"]);
            if (registro.Table.Columns.Contains("operacionesdescuentos")) this.OperacionesDescuentos = Convert.ToDecimal(registro["operacionesdescuentos"]);
            if (registro.Table.Columns.Contains("operacionesinafectas")) this.OperacionesInafectas = Convert.ToDecimal(registro["operacionesinafectas"]);
            if (registro.Table.Columns.Contains("operacionesexoneradas")) this.OperacionesExoneradas = Convert.ToDecimal(registro["operacionesexoneradas"]);
            if (registro.Table.Columns.Contains("operacionesgratuitas")) this.OperacionesGratuitas = Convert.ToDecimal(registro["operacionesgratuitas"]);
            if (registro.Table.Columns.Contains("operacionesgravadas")) this.OperacionesGravadas = Convert.ToDecimal(registro["operacionesgravadas"]);

            if (registro.Table.Columns.Contains("observacion")) this.Observacion = registro["observacion"].ToString();
            if (registro.Table.Columns.Contains("mensajerepresentacionelectronica")) this.MensajeRepresentacionElectronica = registro["mensajerepresentacionelectronica"].ToString();

            if (registro.Table.Columns.Contains("imagenempresa"))
                ImagenEmpresa = Convert.IsDBNull(registro["imagenempresa"]) ? (Byte[])null : (Byte[])registro["imagenempresa"];

            if (registro.Table.Columns.Contains("direccioncentroservicio")) this.DireccionCentroServicio = registro["direccioncentroservicio"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariacentroservicio")) this.DireccionComplementariaCentroServicio = registro["direccioncomplementariacentroservicio"].ToString();

            if (registro.Table.Columns.Contains("direccionempresa")) this.DireccionEmpresa = registro["direccionempresa"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariaempresa")) this.DireccionComplementariaEmpresa = registro["direccioncomplementariaempresa"].ToString();

            if (registro.Table.Columns.Contains("siglamoneda")) this.SiglaMoneda = registro["siglamoneda"].ToString();
            if (registro.Table.Columns.Contains("codigobarra")) this.CodigoBarra = registro["codigobarra"].ToString();

            if (registro.Table.Columns.Contains("esepecial")) this.EsEspecial = Convert.ToInt16(registro["esepecial"]);

            if (registro.Table.Columns.Contains("documentoidentidad")) this.DocumentoIdentidad = registro["documentoidentidad"].ToString();

            if (registro.Table.Columns.Contains("rutaimagencodigobarra")) this.RutaImagenCodigoBarra = registro["rutaimagencodigobarra"].ToString();
            if (registro.Table.Columns.Contains("rutaimagenempresa")) this.RutaImagenEmpresa = registro["rutaimagenempresa"].ToString();
            if (registro.Table.Columns.Contains("nombreempresacorto")) this.NombreEmpresaCorto = registro["nombreempresacorto"].ToString();
            if (registro.Table.Columns.Contains("rucempresa")) this.RUCEmpresa = registro["rucempresa"].ToString();
            if (registro.Table.Columns.Contains("importeletras")) this.ImporteLetras = registro["importeletras"].ToString();
            if (registro.Table.Columns.Contains("tieneconvenio")) this.TieneConvenio = Convert.ToInt16(registro["tieneconvenio"]);

            if (registro.Table.Columns.Contains("idempresa")) this.IdEmpresa = Convert.ToInt16(registro["idempresa"]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        /// <param name="detalle"></param>
        /// <param name="convenio"></param>
        public clsComprobantePagoNGC(DataRow registro, DataTable detalle, DataRow convenio)
        {
            if (registro == null) { return; }

            AsignarRegistro(registro);

            if (detalle != null)
            {
                foreach (DataRow _drw in detalle.Rows)
                {
                    _objcomprobantepagodetalle.Add(new clsComprobantePagoDetalleNGC(_drw));
                }
            }

            if (convenio == null)
                _objcomprobantepagoconvenio = null;
            else
                _objcomprobantepagoconvenio = new clsComprobantePagoConvenio(convenio);
        }

        #endregion

        #region Metodos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        private void AsignarRegistro(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idservicioprincipal")) this.IdServicioPrincipal = (Int16)registro["idservicioprincipal"];
            if (registro.Table.Columns.Contains("idnroservicio")) this.IdNroServicio = (Int32)registro["idnroservicio"];
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = (Int16)registro["idtipodocumento"];
            if (registro.Table.Columns.Contains("nrodocumento")) this.NroDocumento = (String)registro["nrodocumento"];
            if (registro.Table.Columns.Contains("nombredocumento")) this.NombreTipoDocumento = (String)registro["nombredocumento"];
            //if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = Convert.IsDBNull(registro["fechacaja"]) ? (DateTime?)null : Convert.ToDateTime(registro["fechacaja"]);
            if (registro.Table.Columns.Contains("nombrecliente")) this.NombreCliente = (String)registro["nombrecliente"];
            if (registro.Table.Columns.Contains("direccioncliente")) this.DireccionCliente = (String)registro["direccioncliente"];
            if (registro.Table.Columns.Contains("idtipoidentidadpropietario ")) this.IdTipoIdentidad = (Int16)registro["idtipoidentidadpropietario"];
            if (registro.Table.Columns.Contains("nroidentidadpropietario")) this.NroIdentidad = (String)registro["nroidentidadpropietario"];
            if (registro.Table.Columns.Contains("fecharegistro")) this.FechaRegistro = Convert.ToDateTime(registro["fecharegistro"]);
            if (registro.Table.Columns.Contains("idmoneda")) this.IdMoneda = (Int16)registro["idmoneda"];
            if (registro.Table.Columns.Contains("idusuario")) this.IdUsuario = (Int32)registro["idusuario"];
            if (registro.Table.Columns.Contains("nombreusuario")) this.NombreUsuario = (String)registro["nombreusuario"];
            if (registro.Table.Columns.Contains("caja")) this.IdCaja = (Int16)registro["caja"];
            if (registro.Table.Columns.Contains("nombremoneda")) this.NombreMoneda = registro["nombremoneda"].ToString().Trim();
            if (registro.Table.Columns.Contains("simbolomoneda")) this.SimboloMoneda = registro["simbolomoneda"].ToString().Trim();
            if (registro.Table.Columns.Contains("fechavencimiento")) this.FechaVencimiento = Convert.ToDateTime(registro["fechavencimiento"]);
            if (registro.Table.Columns.Contains("mensajetotal")) this.MensajeTotal = registro["mensajetotal"].ToString();
            if (registro.Table.Columns.Contains("nombrepuntoatencion")) this.NombrePuntoAtencion = registro["nombrepuntoatencion"].ToString().Trim();
            if (registro.Table.Columns.Contains("subtotal")) this.SubTotal = Convert.ToDecimal(registro["subtotal"]);
            if (registro.Table.Columns.Contains("igv")) this.IGV = Convert.ToDecimal(registro["igv"]);
            if (registro.Table.Columns.Contains("importetotal")) this.ImporteTotal = Convert.ToDecimal(registro["importetotal"]);

            if (registro.Table.Columns.Contains("montoconpercepcion")) this.MontoConPercepcion = Convert.ToDecimal(registro["montoconpercepcion"]);
            if (registro.Table.Columns.Contains("percepcion")) this.Percepcion = Convert.ToDecimal(registro["percepcion"]);
            if (registro.Table.Columns.Contains("operacionesdescuentos")) this.OperacionesDescuentos = Convert.ToDecimal(registro["operacionesdescuentos"]);
            if (registro.Table.Columns.Contains("operacionesinafectas")) this.OperacionesInafectas = Convert.ToDecimal(registro["operacionesinafectas"]);
            if (registro.Table.Columns.Contains("operacionesexoneradas")) this.OperacionesExoneradas = Convert.ToDecimal(registro["operacionesexoneradas"]);
            if (registro.Table.Columns.Contains("operacionesgratuitas")) this.OperacionesGratuitas = Convert.ToDecimal(registro["operacionesgratuitas"]);
            if (registro.Table.Columns.Contains("operacionesgravadas")) this.OperacionesGravadas = Convert.ToDecimal(registro["operacionesgravadas"]);

            if (registro.Table.Columns.Contains("observacion")) this.Observacion = registro["observacion"].ToString();
            if (registro.Table.Columns.Contains("mensajerepresentacionelectronica")) this.MensajeRepresentacionElectronica = registro["mensajerepresentacionelectronica"].ToString();

            if (registro.Table.Columns.Contains("imagenempresa"))
                ImagenEmpresa = Convert.IsDBNull(registro["imagenempresa"]) ? (Byte[])null : (Byte[])registro["imagenempresa"];

            if (registro.Table.Columns.Contains("direccioncentroservicio")) this.DireccionCentroServicio = registro["direccioncentroservicio"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariacentroservicio")) this.DireccionComplementariaCentroServicio = registro["direccioncomplementariacentroservicio"].ToString();

            if (registro.Table.Columns.Contains("direccionempresa")) this.DireccionEmpresa = registro["direccionempresa"].ToString();
            if (registro.Table.Columns.Contains("direccioncomplementariaempresa")) this.DireccionComplementariaEmpresa = registro["direccioncomplementariaempresa"].ToString();

            if (registro.Table.Columns.Contains("siglamoneda")) this.SiglaMoneda = registro["siglamoneda"].ToString();
            if (registro.Table.Columns.Contains("codigobarra")) this.CodigoBarra = registro["codigobarra"].ToString();
            if (registro.Table.Columns.Contains("esespecial")) this.EsEspecial = Convert.ToInt16(registro["esespecial"]);
            if (registro.Table.Columns.Contains("documentoidentidad")) this.DocumentoIdentidad = registro["documentoidentidad"].ToString();
        }


        #endregion

    }

    public class clsComprobantePagoDetalleNGC
    {
        #region Atributos

        private Int16 _intidconcepto;
        private String _stretiquetaconcepto;
        private String _strabreviaturaconcepto;
        private Decimal? _deccantidad;
        private String _strunidad;
        private String _strdescripcion;
        private Decimal? _decpreciounitario;
        private Decimal _importetotal;
        private Int16 _indicadorigv;
        private Decimal _decvalorigv;
        private Int16 _esvisible = 1;

        private Decimal _decvalorventa;

        private Int16 _intidempresa;
        private Int16 _intidtipodocumento;
        private String _strnrodocumento;

        #endregion

        #region Propiedades

        public Int16 IdEmpresa
        {
            get { return _intidempresa; }
            set { _intidempresa = value; }
        }

        public Int16 IdTipoDocumento
        {
            get { return _intidtipodocumento; }
            set { _intidtipodocumento = value; }
        }

        public String NroDocumento
        {
            get { return _strnrodocumento; }
            set { _strnrodocumento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 EsVisible
        {
            get { return _esvisible; }
            set { _esvisible = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ValorIGV
        {
            get { return _decvalorigv; }
            set { _decvalorigv = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int16 IdConcepto
        {
            get { return _intidconcepto; }
            set { _intidconcepto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String EtiquetaConcepto
        {
            get { return _stretiquetaconcepto; }
            set { _stretiquetaconcepto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String AbreviaturaConcepto
        {
            get { return _strabreviaturaconcepto; }
            set { _strabreviaturaconcepto = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? Cantidad
        {
            get { return _deccantidad; }
            set { _deccantidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Unidad
        {
            get { return _strunidad; }
            set { _strunidad = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String Descripcion
        {
            get { return _strdescripcion; }
            set { _strdescripcion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal? PrecioUnitario
        {
            get { return _decpreciounitario; }
            set { _decpreciounitario = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Decimal Importe
        {
            get { return _importetotal; }
            set { _importetotal = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int16 IndicadorIGV
        {
            get { return _indicadorigv; }
            set { _indicadorigv = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Decimal ValorVenta
        {
            get { return _decvalorventa; }
            set { _decvalorventa = value; }
        }
        #endregion

        #region Constructores

        /// <summary>
        /// 
        /// </summary>
        public clsComprobantePagoDetalleNGC()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registro"></param>
        public clsComprobantePagoDetalleNGC(DataRow registro)
        {
            if (registro.Table.Columns.Contains("idconcepto")) this.IdConcepto = (Int16)registro["idconcepto"];
            if (registro.Table.Columns.Contains("etiquetaconcepto")) this.EtiquetaConcepto = (String)registro["etiquetaconcepto"];
            if (registro.Table.Columns.Contains("abreviaturaconcepto")) this.AbreviaturaConcepto = (String)registro["abreviaturaconcepto"];
            if (registro.Table.Columns.Contains("cantidad")) this.Cantidad = Convert.IsDBNull(registro["cantidad"]) ? (Decimal?)null : (Decimal)registro["cantidad"];
            if (registro.Table.Columns.Contains("unidad")) this.Unidad = (String)registro["unidad"];
            if (registro.Table.Columns.Contains("descripcion")) this.Descripcion = (String)registro["descripcion"];
            if (registro.Table.Columns.Contains("preciounitario")) this.PrecioUnitario = Convert.IsDBNull(registro["preciounitario"]) ? (Decimal?)null : (Decimal)registro["preciounitario"];
            if (registro.Table.Columns.Contains("importe")) this.Importe = (Decimal)registro["importe"];
            if (registro.Table.Columns.Contains("indicadorigv")) this.IndicadorIGV = (Int16)registro["indicadorigv"];
            if (registro.Table.Columns.Contains("igv")) this.ValorIGV = Convert.ToDecimal(registro["igv"]);
            if (registro.Table.Columns.Contains("esvisible")) this.EsVisible = Convert.ToInt16(registro["esvisible"]);
            if (registro.Table.Columns.Contains("valorventa")) this.ValorVenta = Convert.ToDecimal(registro["valorventa"]);

            if (registro.Table.Columns.Contains("idempresa")) this.IdEmpresa = Convert.ToInt16(registro["idempresa"]);
            if (registro.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = Convert.ToInt16(registro["idtipodocumento"]);
            if (registro.Table.Columns.Contains("nrodocumento")) this.NroDocumento = registro["nrodocumento"].ToString();
        }

        #endregion
    }

    //public class clsListaComprobantePagoDetalle
    //{
    //    private List<clsComprobantePagoDetalleNGC> _lstcomprobantepago = new List<clsComprobantePagoDetalleNGC>();

    //    #region Constructor

    //    public clsListaComprobantePagoDetalle()
    //    { }

    //    public clsListaComprobantePagoDetalle(DataTable comprobantepago)
    //    {
    //        if (comprobantepago == null || comprobantepago.Rows.Count == 0) { return; }

    //        foreach (DataRow fila in comprobantepago.Rows)
    //        {
    //            Elementos.Add(new clsComprobantePagoDetalleNGC(fila));
    //        }
    //    }

    //    #endregion

    //    #region Propiedades
    //    public List<clsComprobantePagoDetalleNGC> Elementos
    //    {
    //        get { return _lstcomprobantepago; }
    //        set { _lstcomprobantepago = value; }
    //    }
    //    #endregion
    //}

    //public class clsComprobantePagoConvenio
    //{
    //    #region Campos
    //    private Int16 _intidempresa;
    //    private Int16 _intidtipodocumento;
    //    private String _strnrodocumento;

    //    private String _strnombreconvenio;
    //    private String _strnroconvenio;
    //    private Decimal _decpagoinicial;
    //    private Int16 _intnrocuotas;
    //    private Decimal _decvalorcuota;

    //    #endregion

    //    #region Propiedades

    //    public Int16 IdEmpresa
    //    {
    //        get { return _intidempresa; }
    //        set { _intidempresa = value; }
    //    }

    //    public Int16 IdTipoDocumento
    //    {
    //        get { return _intidtipodocumento; }
    //        set { _intidtipodocumento = value; }
    //    }

    //    public String NroDocumento
    //    {
    //        get { return _strnrodocumento; }
    //        set { _strnrodocumento = value; }
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public String NombreConvenio
    //    {
    //        get { return _strnombreconvenio; }
    //        set { _strnombreconvenio = value; }
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public String NroConvenio
    //    {
    //        get { return _strnroconvenio; }
    //        set { _strnroconvenio = value; }
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public Decimal PagoInicial
    //    {
    //        get { return _decpagoinicial; }
    //        set { _decpagoinicial = value; }
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public Int16 NroCuotas
    //    {
    //        get { return _intnrocuotas; }
    //        set { _intnrocuotas = value; }
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public Decimal ValorCuota
    //    {
    //        get { return _decvalorcuota; }
    //        set { _decvalorcuota = value; }
    //    }

    //    #endregion

    //    #region Constructor

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public clsComprobantePagoConvenio()
    //    {
    //    }

    //    public clsComprobantePagoConvenio(DataRow dr)
    //    {
    //        if (dr == null) { return; }

    //        if (dr.Table.Columns.Contains("nombreconvenio"))
    //            NombreConvenio = Convert.IsDBNull(dr["nombreconvenio"]) ? "" : dr["nombreconvenio"].ToString().Trim();
    //        if (dr.Table.Columns.Contains("nroconvenio"))
    //            NroConvenio = Convert.IsDBNull(dr["nroconvenio"]) ? "" : dr["nroconvenio"].ToString().Trim();
    //        if (dr.Table.Columns.Contains("pagoinicial"))
    //            PagoInicial = Convert.IsDBNull(dr["pagoinicial"]) ? Decimal.Zero : Convert.ToDecimal(dr["pagoinicial"]);
    //        if (dr.Table.Columns.Contains("valorcuota"))
    //            ValorCuota = Convert.IsDBNull(dr["valorcuota"]) ? Decimal.Zero : Convert.ToDecimal(dr["valorcuota"]);
    //        if (dr.Table.Columns.Contains("nrocuotas"))
    //            NroCuotas = Convert.IsDBNull(dr["nrocuotas"]) ? (Int16)0 : Convert.ToInt16(dr["nrocuotas"]);

    //        if (dr.Table.Columns.Contains("idempresa")) this.IdEmpresa = Convert.ToInt16(dr["idempresa"]);
    //        if (dr.Table.Columns.Contains("idtipodocumento")) this.IdTipoDocumento = Convert.ToInt16(dr["idtipodocumento"]);
    //        if (dr.Table.Columns.Contains("nrodocumento")) this.NroDocumento = dr["nrodocumento"].ToString();
    //    }

    //    #endregion
    //}


    #endregion Comprobante NGC Escritorio

    #region Transaccion Extrajudicial NGC Escritorio

    public class clsCampoDato
    {
        private String _strcampo;
        private String _strdato;
        private Byte[] _strimagen;
        private Int16 _intindice;

        public Byte[] Imagen
        {
            get { return _strimagen; }
            set { _strimagen = value; }
        }
        public String Campo
        {
            get { return _strcampo; }
            set { _strcampo = value; }
        }

        public String Dato
        {
            get { return _strdato; }
            set { _strdato = value; }
        }

        public Int16 Indice
        {
            get { return _intindice; }
            set { _intindice = value; }
        }

        public clsCampoDato() { }

        public clsCampoDato(String campo, String dato)
        {
            _strcampo = campo;
            _strdato = dato;
        }

        public clsCampoDato(String campo, String dato, Int16 indice)
        {
            _strcampo = campo;
            _strdato = dato;
            _intindice = indice;
        }
    }

    public class clsListaCampoDato
    {
        private List<clsCampoDato> _objelementos = new List<clsCampoDato>();
        public List<clsCampoDato> Elementos
        {
            get { return _objelementos; }
            set { _objelementos = value; }
        }

        private List<DataTable> _objtablasword = new List<DataTable>();
        public List<DataTable> TablasWord
        {
            get { return _objtablasword; }
            set { _objtablasword = value; }
        }

        /// <summary>
        /// Constructor predeterminado.
        /// </summary>
        public clsListaCampoDato() { }

        /// <summary>
        /// Constructor con DATAROW.
        /// </summary>
        /// <param name="registro"></param>
        public clsListaCampoDato(DataRow registro)
        {
            if (registro == null) { return; }

            Int32 nColumnas = registro.Table.Columns.Count;

            for (int i = 0; i < nColumnas; i++)
            {
                String nombreColumna = registro.Table.Columns[i].ColumnName;
                String valorColumna = registro[i].ToString().Trim();

                _objelementos.Add(new clsCampoDato(nombreColumna, valorColumna));
            }
        }
    }

    #endregion Transaccion Extrajudicial NGC Escritorio


}
