﻿using SISAC.Data.NTCSE;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Negocio.NTCSE
{
    #region clsNegocioConsultaInterrupcion

    public sealed class clsNegocioConsultaInterrupcion
    {
        #region Field

        private static readonly clsConsultaInterrupcionDAO _ConsultaInterrupDAO = clsConsultaInterrupcionDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly Lazy<clsNegocioConsultaInterrupcion> _Instancia = new Lazy<clsNegocioConsultaInterrupcion>(() => new clsNegocioConsultaInterrupcion(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioConsultaInterrupcion Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioConsultaInterrupcion()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado ObtenerDatosInterrupcion(clsParametro entidad)
        {
            try
            {
                var table = _ConsultaInterrupDAO.ObtenerDatosInterrupcion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerDatosReposicionParcialInterrupcion(clsParametro entidad)
        {
            try
            {
                var table = _ConsultaInterrupDAO.ObtenerDatosReposicionParcialInterrupcion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerDatosGPSSuministro(clsParametro entidad)
        {
            try
            {
                var table = _ConsultaInterrupDAO.ObtenerDatosGPSSuministro(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerDatosGPSSuministroLejano(clsParametro entidad)
        {
            try
            {
                var table = _ConsultaInterrupDAO.ObtenerDatosGPSSuministroLejano(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerSuministrosAfectadosGPS(clsParametro entidad)
        {
            try
            {
                var table = _ConsultaInterrupDAO.ObtenerSuministrosAfectadosGPS(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion Public

    }

    #endregion clsNegocioConsultaInterrupcion

}
