﻿using SISAC.Data.NTCSE;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Negocio.NTCSE
{
    #region clsNegocioIndicadorSaifiSaidi
    public sealed  class clsNegocioIndicadorSaifiSaidi
    {
        #region Field

        private static readonly clsIndicadorSaifiSaidiDAO _IndicadorSaifiSaidiDAO = clsIndicadorSaifiSaidiDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly Lazy<clsNegocioIndicadorSaifiSaidi> _Instancia = new Lazy<clsNegocioIndicadorSaifiSaidi>(() => new clsNegocioIndicadorSaifiSaidi(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioIndicadorSaifiSaidi Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioIndicadorSaifiSaidi()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado ObtenerTabularIndicadorAcumulado(clsParametro entidad)
        {
            try
            {
                var table = _IndicadorSaifiSaidiDAO.ObtenerTabularIndicadorAcumulado(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerIndicadorAcumulado(clsParametro entidad)
        {
            try
            {
                var table = _IndicadorSaifiSaidiDAO.ObtenerIndicadorAcumulado(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerEvolucionIndicadorAcumulado(clsParametro entidad)
        {
            try
            {
                var table = _IndicadorSaifiSaidiDAO.ObtenerEvolucionIndicadorAcumulado(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerProcesosxSimulacion(clsParametro entidad)
        {
            try
            {
                var table = _IndicadorSaifiSaidiDAO.ObtenerProcesosxSimulacion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }


        public clsResultado AyudaLista(clsParametro entidad)
        {
            try
            {
                var table = _IndicadorSaifiSaidiDAO.AyudaLista(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }


        public clsResultado ObtenerIndicadorAcumuladoMovil(clsParametro entidad)
        {
            try
            {
                var table = _IndicadorSaifiSaidiDAO.ObtenerIndicadorAcumuladoMovil(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion Public
    }

    #endregion clsNegocioIndicadorSaifiSaidi

}
