﻿using SISAC.Entidad.Maestro;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Interface.General
{
    public interface IProyectos
    {
        Task<IHttpActionResult> AyudaLista(clsParametro param);

        Task<IHttpActionResult> ListIniciativas(clsParametro param);

        Task<IHttpActionResult> ObtenerNivelesFase(clsParametro param);

        Task<IHttpActionResult> ObtenerUsuario(clsParametro param);

        Task<IHttpActionResult> ObtenerIniciativa(clsParametro param);

        Task<IHttpActionResult> RegistrarHito(clsParametro param);

        Task<IHttpActionResult> ObtenerHito(clsParametro param);

        Task<IHttpActionResult> ExportarIniciativa(clsParametro param);

        Task<IHttpActionResult> ExportarImporte(clsParametro param);

        Task<IHttpActionResult> RegistrarImporte(clsParametro param);

        Task<IHttpActionResult> ObtenerImporte(clsParametro param);

        Task<IHttpActionResult> EditarIniciativa(clsParametro param);
    }
}