﻿using SISAC.Data.General;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Negocio.General
{
    public sealed class clsNegocioProyectos
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;

        //private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly clsProyectosDAO _ProyDAO = clsProyectosDAO.Instancia;

        private static readonly Lazy<clsNegocioProyectos> _Instancia = new Lazy<clsNegocioProyectos>(() => new clsNegocioProyectos(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioProyectos Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioProyectos()
        {
        }

        #endregion Constructor

        #region Public

        public async Task<clsResultado> AyudaLista(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.AyudaLista(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ListIniciativas(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ListIniciativas(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ObtenerNivelesFase(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ObtenerNivelesFase(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ObtenerUsuario(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ObtenerUsuario(entidad);

                var result = new clsResultado();
                result.Datos = data.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ObtenerIniciativa(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ObtenerIniciativa(entidad);

                var result = new clsResultado();
                result.Datos = data.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> RegistrarHito(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.RegistrarHito(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ObtenerHito(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ObtenerHito(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ExportarIniciativa(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ExportarIniciativa(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ExportarImporte(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ExportarImporte(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> RegistrarImporte(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.RegistrarImporte(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ObtenerImporte(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.ObtenerImporte(entidad);

                var result = new clsResultado();
                result.Datos = data;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> EditarIniciativa(clsParametro entidad)
        {
            try
            {
                var data = await _ProyDAO.EditarIniciativa(entidad);

                var result = new clsResultado();
                result.Datos = data.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion Public
    }
}