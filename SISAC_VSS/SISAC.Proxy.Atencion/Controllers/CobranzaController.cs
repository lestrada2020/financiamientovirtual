﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Util.Tool;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace SISAC.Proxy.Atencion.Controllers
{
    public class CobranzaController : ApiController, ICobranza
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteAtencion";
                String controller = "Cobranza";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public IHttpActionResult ConsultarDeuda(clsParametro param)
        {
            var result = SendData<clsResultado>("ConsultarDeuda", param);

            return Ok(result);
        }

        public IHttpActionResult ConsultarDeudaDetalle(clsParametro param)
        {
            var result = SendData<clsResultado>("ConsultarDeudaDetalle", param);

            return Ok(result);
        }

        public IHttpActionResult PagarDeuda(clsParametro param)
        {
            var result = SendData<clsResultado>("PagarDeuda", param);

            return Ok(result);
        }

        public IHttpActionResult GrabarIntento(clsParametro param)
        {
            var result = SendData<clsResultado>("GrabarIntento", param);

            return Ok(result);
        }

        public IHttpActionResult ActualizarIntento(clsParametro param)
        {
            var result = SendData<clsResultado>("ActualizarIntento", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerPagoResumen(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerPagoResumen", param);

            return Ok(result);
        }

        public IHttpActionResult EnviarEmail(clsParametro param)
        {
            var result = SendData<clsResultado>("EnviarEmail", param);

            return Ok(result);
        }

        public IHttpActionResult GrabarIntentoEmpresa(clsParametroBulkData param)
        {
            var result = SendData<clsResultado>("GrabarIntentoEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult PagarDeudaEmpresa(clsParametro param)
        {
            var result = SendData<clsResultado>("PagarDeudaEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult ListarBancos(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarBancos", param);

            return Ok(result);
        }

        public IHttpActionResult ListarCuentas(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarCuentas", param);

            return Ok(result);
        }

        public IHttpActionResult GrabarDepositoBancarioIntento(clsParametroBulkData param)
        {
            var result = SendData<clsResultado>("GrabarDepositoBancarioIntento", param);

            return Ok(result);
        }

        public IHttpActionResult ActualizarDepositoBancarioEstado(clsParametro param)
        {
            var result = SendData<clsResultado>("ActualizarDepositoBancarioEstado", param);

            return Ok(result);
        }

        public IHttpActionResult CobranzaOrdenVisaNET(clsParametro param)
        {
            var result = SendData<clsResultado>("CobranzaOrdenVisaNET", param);

            return Ok(result);
        }
        #endregion Public
    }
}