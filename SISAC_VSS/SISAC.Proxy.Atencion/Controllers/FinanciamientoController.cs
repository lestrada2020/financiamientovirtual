﻿using SISAC.Interface.Atencion;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SISAC.Entidad.Maestro;
using System.Threading.Tasks;

namespace SISAC.Proxy.Atencion.Controllers
{
    public class FinanciamientoController : ApiController, IFinanciamiento
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly String _SiteAtencion = clsUtil.Instancia.GetValueOfConfig("SiteAtencion");


        #endregion Field
        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteAtencion";
                String controller = "Financiamiento";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        private async Task<clsResultado> SendAsync(String action, Object param)
        {
            try
            {
                var url = String.Format("{0}/Api/OficinaVirtual/{1}", _SiteAtencion, action);

                return await _Service.PostAsync<clsResultado>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }
        #endregion Private


        #region "CMASSA"
        public IHttpActionResult ObtenerDetalleFinanciamiento(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDetalleFinanciamiento", param);
            return Ok(result);
        }

        public IHttpActionResult GrabarIntento(clsParametro param)
        {
            var result = SendData<clsResultado>("GrabarIntento", param);
            return Ok(result);
        }

        #endregion  

        #region Public
        public IHttpActionResult ListarSuministroAsociado(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarSuministroAsociado", param);

            return Ok(result);
        }

        public IHttpActionResult GenerarOrdenCobro(clsParametro param)
        {
            var result = SendData<clsResultado>("GenerarOrdenCobro", param);

            return Ok(result);
        }
        #region LESTRADA

        public IHttpActionResult ObtenerParametrosConvenio(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerParametrosConvenio", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerPorcentajesConvenio(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerPorcentajesConvenio", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerConveniosPorEmpresa(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerConveniosPorEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerConvenioPorId(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerConvenioPorId", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleDeuda(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDetalleDeuda", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerConfiguracionGlobal(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerConfiguracionGlobal", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleOrdenCobro(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDetalleOrdenCobro", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerCabeceraOrdenCobro(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerCabeceraOrdenCobro", param);

            return Ok(result);
        }

        public IHttpActionResult GrabarIntentoDetalle(clsParametro param)
        {
            var result = SendData<clsResultado>("GrabarIntentoDetalle", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerListaPagoPorOrdenCobro(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerListaPagoPorOrdenCobro", param);

            return Ok(result);
        }

        public IHttpActionResult PagarDeuda(clsParametro param)
        {
            var result = SendData<clsResultado>("PagarDeuda", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerListaPorOrdenCobro(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerListaPorOrdenCobro", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDocumentoComprobantePago(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDocumentoComprobantePago", param);

            return Ok(result);
        }

        public IHttpActionResult GenerarDocumentoTransaccionExtrajudicial(clsParametro param)
        {
            var result = SendData<clsResultado>("GenerarDocumentoTransaccionExtrajudicial", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> VisaListFailed(clsParametro param)
        {
            var result = await SendAsync("VisaListFailed", param);

            return Ok(result);
        }

        public IHttpActionResult ActualizarMetodoPagoOrdenVisa(clsParametro param)
        {
            var result = SendData<clsResultado>("ActualizarMetodoPagoOrdenVisa", param);

            return Ok(result);
        }
        #endregion LESTRADA

        #region RBERROSPI
        public IHttpActionResult ObtenerMetodosPago(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerMetodosPago", param);

            return Ok(result);
        }

        public IHttpActionResult EnviarEmail(clsParametro param)
        {
            var result = SendData<clsResultado>("EnviarEmail", param);

            return Ok(result);
        }

        public IHttpActionResult FinanciamientoEnviarEmailError(clsParametro param)
        {
            var result = SendData<clsResultado>("FinanciamientoEnviarEmailError", param);
            return Ok(result);
        }

        public IHttpActionResult EstadoPagoDistriluz(clsParametro param)
        {
            var result = SendData<clsResultado>("EstadoPagoDistriluz", param);
            return Ok(result);
        }

        #endregion

        #endregion Public
    }
}
