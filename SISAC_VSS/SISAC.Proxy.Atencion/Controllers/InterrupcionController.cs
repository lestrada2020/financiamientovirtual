﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Util.Tool;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace SISAC.Proxy.Atencion.Controllers
{
    public class InterrupcionController : ApiController, IInterrupcion
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteAtencion";
                String controller = "Interrupcion";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public IHttpActionResult ObtenerConfiguracionReporte(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerConfiguracionReporte", param);

            return Ok(result);
        }

        public IHttpActionResult AyudaLista(clsParametro param)
        {
            var result = SendData<clsResultado>("AyudaLista", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerMotivoFomatoNotifica(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerMotivoFomatoNotifica", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNTiempo(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerResumenxUUNNTiempo", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNAreaTiempo(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerResumenxUUNNAreaTiempo", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNMotivoTiempo(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerResumenxUUNNMotivoTiempo", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerResumenxUUNNSEDTiempo(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerResumenxUUNNSEDTiempo", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleSuministros(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDetalleSuministros", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleSEDs(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDetalleSEDs", param);

            return Ok(result);
        }

        public IHttpActionResult GenerarTokenReporte(clsParametro param)
        {
            var result = SendData<clsResultado>("GenerarTokenReporte", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerAtencionesxRecursoInterrupcion(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerAtencionesxRecursoInterrupcion", param);

            return Ok(result);
        }

        public IHttpActionResult LimpiarTablaReporte(clsParametro param)
        {
            var result = SendData<clsResultado>("LimpiarTablaReporte", param);

            return Ok(result);
        }

        #endregion Public
    }
}