﻿using SISAC.Entidad.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Util.Tool;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace SISAC.Proxy.Atencion.Controllers
{
    public class ConsultaGeneralController : ApiController, IConsultaGeneral
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteAtencion";
                String controller = "ConsultaGeneral";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        #region Busqueda

        public IHttpActionResult BuscarBySuministro(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("BuscarBySuministro", param);

            return Ok(result);
        }

        public IHttpActionResult BuscarByNroDocumento(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("BuscarByNroDocumento", param);

            return Ok(result);
        }

        public IHttpActionResult BuscarByNombre(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("BuscarByNombre", param);

            return Ok(result);
        }

        public IHttpActionResult BuscarByRecibo(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("BuscarByRecibo", param);

            return Ok(result);
        }

        #endregion Busqueda

        #region Datos

        public IHttpActionResult DatosPago(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosPago", param);

            return Ok(result);
        }

        public IHttpActionResult DatosRecibo(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosRecibo", param);

            return Ok(result);
        }

        public IHttpActionResult DatosReclamo(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosReclamo", param);

            return Ok(result);
        }

        public IHttpActionResult DatosMedidor(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosMedidor", param);

            return Ok(result);
        }

        public IHttpActionResult DatosRefacfurado(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosRefacfurado", param);

            return Ok(result);
        }

        public IHttpActionResult DatosFacturacion(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosFacturacion", param);

            return Ok(result);
        }

        public IHttpActionResult DatosTecnico(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosTecnico", param);

            return Ok(result);
        }

        public IHttpActionResult DatosConsumo(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosConsumo", param);

            return Ok(result);
        }

        public IHttpActionResult DatosSuministro(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosSuministro", param);

            return Ok(result);
        }

        public IHttpActionResult DatosContrato(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosContrato", param);

            return Ok(result);
        }

        public IHttpActionResult DatosDeuda(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosDeuda", param);

            return Ok(result);
        }

        public IHttpActionResult DatosComercial(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("DatosComercial", param);

            return Ok(result);
        }

        #endregion Datos

        #region NroServicio

        public IHttpActionResult Resumen(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Resumen", param);

            return Ok(result);
        }

        public IHttpActionResult Basico(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Basico", param);

            return Ok(result);
        }

        public IHttpActionResult Restriccion(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Restriccion", param);

            return Ok(result);
        }

        public IHttpActionResult CtaCte(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("CtaCte", param);

            return Ok(result);
        }

        public IHttpActionResult Facturacion(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Facturacion", param);

            return Ok(result);
        }

        public IHttpActionResult FacturacionConcepto(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("FacturacionConcepto", param);

            return Ok(result);
        }

        public IHttpActionResult FacturacionDocumento(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("FacturacionDocumento", param);

            return Ok(result);
        }

        public IHttpActionResult ReciboBasico(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ReciboBasico", param);

            return Ok(result);
        }

        public IHttpActionResult ReciboMedidor(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ReciboMedidor", param);

            return Ok(result);
        }

        public IHttpActionResult ReciboLectura(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ReciboLectura", param);

            return Ok(result);
        }

        public IHttpActionResult ReciboConcepto(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ReciboConcepto", param);

            return Ok(result);
        }

        public IHttpActionResult Atencion(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Atencion", param);

            return Ok(result);
        }

        public IHttpActionResult FotoLectura(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("FotoLectura", param);

            return Ok(result);
        }

        public IHttpActionResult Lectura(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Lectura", param);

            return Ok(result);
        }

        public IHttpActionResult AFavor(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("AFavor", param);

            return Ok(result);
        }

        public IHttpActionResult Pagos(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Pagos", param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoBasico(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("TecnicoBasico", param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoMagnitud(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("TecnicoMagnitud", param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoPrecinto(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("TecnicoPrecinto", param);

            return Ok(result);
        }

        public IHttpActionResult TecnicoTransformador(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("TecnicoTransformador", param);

            return Ok(result);
        }

        public IHttpActionResult OrdenTrabajo(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("OrdenTrabajo", param);

            return Ok(result);
        }

        public IHttpActionResult FotoActividad(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("FotoActividad", param);

            return Ok(result);
        }

        public IHttpActionResult Foto(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Foto", param);

            return Ok(result);
        }

        public IHttpActionResult MedidorBasico(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("MedidorBasico", param);

            return Ok(result);
        }

        public IHttpActionResult MedidorLista(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("MedidorLista", param);

            return Ok(result);
        }

        public IHttpActionResult MedidorLecturas(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("MedidorLecturas", param);

            return Ok(result);
        }

        public IHttpActionResult Cambios(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("Cambios", param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioLista(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ConvenioLista", param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioRegistro(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ConvenioRegistro", param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioCreacion(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ConvenioCreacion", param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioDocumentos(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ConvenioDocumentos", param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioGarantias(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ConvenioGarantias", param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioAmortizaciones(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ConvenioAmortizaciones", param);

            return Ok(result);
        }

        public IHttpActionResult ConvenioLetra(clsConsultaGeneralFiltro param)
        {
            var result = SendData<clsResultado>("ConvenioLetra", param);

            return Ok(result);
        }

        #endregion NroServicio

        #endregion Public
    }
}