﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Util.Tool;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Atencion.Controllers
{
    public class SuministroController : ApiController, ISuministro
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly String _SiteAtencion = clsUtil.Instancia.GetValueOfConfig("SiteAtencion");
        private static readonly String _TokenChatBot = clsUtil.Instancia.GetValueOfConfig("TokenChatBot");

        #endregion Field

        #region Private

        private async Task<clsResultado> SendAsync(String action, Object param)
        {
            try
            {
                var url = String.Format("{0}/Api/Suministro/{1}", _SiteAtencion, action);

                return await _Service.PostAsync<clsResultado>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }

        private clsResultado ValidarToken(clsParametro param, String KeyToken)
        {
            var result = new clsResultado();

            if (param.Parametros["SessionToken"].Equals(KeyToken)) return result;

            result.IdError = 1;
            result.Mensaje = "Token no valido.";

            return result;
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> DeudaFechaVence(clsParametro param)
        {
            var result = ValidarToken(param, _TokenChatBot);
            if (result.IdError > 0) return Ok(result);

            result = await SendAsync("DeudaFechaVence", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> InterrupcionServicio(clsParametro param)
        {
            var result = ValidarToken(param, _TokenChatBot);
            if (result.IdError > 0) return Ok(result);

            result = await SendAsync("InterrupcionServicio", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> CortePorDeuda(clsParametro param)
        {
            var result = ValidarToken(param, _TokenChatBot);
            if (result.IdError > 0) return Ok(result);

            result = await SendAsync("CortePorDeuda", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConsultarDatos(clsParametro param)
        {
            var result = ValidarToken(param, _TokenChatBot);
            if (result.IdError > 0) return Ok(result);

            result = await SendAsync("ConsultarDatos", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistroInterrupcion(clsParametro param)
        {
            var result = ValidarToken(param, _TokenChatBot);
            if (result.IdError > 0) return Ok(result);

            result = await SendAsync("RegistroInterrupcion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConsultarRHD(clsParametro param)
        {
            var result = (clsResultado)null;

            result = await SendAsync("ConsultarRHD", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> UltimaLectura(clsParametro param)
        {
            var result = ValidarToken(param, _TokenChatBot);
            if (result.IdError > 0) return Ok(result);

            result = await SendAsync("UltimaLectura", param);

            return Ok(result);
        }

        #endregion Public
    }
}