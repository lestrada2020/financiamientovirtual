﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Atencion;
using SISAC.Util.Tool;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Atencion.Controllers
{
    public class OficinaVirtualController : ApiController, IOficinaVirtual
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly String _SiteAtencion = clsUtil.Instancia.GetValueOfConfig("SiteAtencion");

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                var url = String.Format("{0}/Api/OficinaVirtual/{1}", _SiteAtencion, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        private async Task<clsResultado> SendAsync(String action, Object param)
        {
            try
            {
                var url = String.Format("{0}/Api/OficinaVirtual/{1}", _SiteAtencion, action);

                return await _Service.PostAsync<clsResultado>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public async Task<IHttpActionResult> ConsultaAtencionesSuministro(clsParametro param)
        {
            var result = await SendAsync("ConsultaAtencionesSuministro", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConsultaAtencionNroAtencion(clsParametro param)
        {
            var result = await SendAsync("ConsultaAtencionNroAtencion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarContactanos(clsParametro param)
        {
            var result = await SendAsync("RegistrarContactanos", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListarInterrupciones(clsParametro param)
        {
            var result = await SendAsync("ListarInterrupciones", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConfirmarEmail(clsParametro param)
        {
            var result = await SendAsync("ConfirmarEmail", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarReclamo(clsParametro param)
        {
            var result = await SendAsync("RegistrarReclamo", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> InfoSuministro(clsParametro param)
        {
            var result = await SendAsync("InfoSuministro", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerObservacion(clsParametro param)
        {
            var result = await SendAsync("ObtenerObservacion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> DesasociarSuministro(clsParametro param)
        {
            var result = await SendAsync("DesasociarSuministro", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> CambiarPassword(clsParametro param)
        {
            var result = await SendAsync("CambiarPassword", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListarByNroAtencion(clsParametro param)
        {
            var result = await SendAsync("ListarByNroAtencion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarFotoLec(clsParametro param)
        {
            var result = await SendAsync("RegistrarFotoLec", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> VisaListFailed(clsParametro param)
        {
            var result = await SendAsync("VisaListFailed", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> VisaSendMailPayFailed(clsParametro param)
        {
            var result = await SendAsync("VisaSendMailPayFailed", param);

            return Ok(result);
        }

        #region Cliente Empresa

        public IHttpActionResult ValidaAccesoClienteEmpresa(clsParametro param)
        {
            var result = SendData<clsResultado>("ValidaAccesoClienteEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult RegistrarClienteEmpresaAdmin(clsParametro param)
        {
            var result = SendData<clsResultado>("RegistrarClienteEmpresaAdmin", param);

            return Ok(result);
        }

        public IHttpActionResult BuscarPorIdentidad(clsParametro param)
        {
            var result = SendData<clsResultado>("BuscarPorIdentidad", param);

            return Ok(result);
        }

        public IHttpActionResult ListarClienteEmpresa(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarClienteEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult ListarRoles(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarRoles", param);

            return Ok(result);
        }

        public IHttpActionResult TipoRelacion(clsParametro param)
        {
            var result = SendData<clsResultado>("TipoRelacion", param);

            return Ok(result);
        }

        public IHttpActionResult RegistrarClienteEmpresa(clsParametro param)
        {
            var result = SendData<clsResultado>("RegistrarClienteEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult EditarClienteEmpresa(clsParametro param)
        {
            var result = SendData<clsResultado>("EditarClienteEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult EliminarClienteEmpresa(clsParametro param)
        {
            var result = SendData<clsResultado>("EliminarClienteEmpresa", param);

            return Ok(result);
        }

        public IHttpActionResult ListarTarifaActiva(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarTarifaActiva", param);

            return Ok(result);
        }

        public IHttpActionResult ListarSuministros(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarSuministros", param);

            return Ok(result);
        }

        public IHttpActionResult ListarSuministrosDetallePago(clsParametro param)
        {
            var result = SendData<clsResultado>("ListarSuministrosDetallePago", param);

            return Ok(result);
        }

        public IHttpActionResult EliminarSuministroAsociado(clsParametro param)
        {
            var result = SendData<clsResultado>("EliminarSuministroAsociado", param);

            return Ok(result);
        }

        public IHttpActionResult BuscarSuministroAsociar(clsParametro param)
        {
            var result = SendData<clsResultado>("BuscarSuministroAsociar", param);

            return Ok(result);
        }

        public IHttpActionResult RegistrarSuministroAsociar(clsParametro param)
        {
            var result = SendData<clsResultado>("RegistrarSuministroAsociar", param);

            return Ok(result);
        }

        public IHttpActionResult ListarSuministrosCarga(clsParametroBulkData param)
        {
            var result = SendData<clsResultado>("ListarSuministrosCarga", param);

            return Ok(result);
        }

        #endregion Cliente Empresa

        public async Task<IHttpActionResult> ObtenerFile(clsParametro param)
        {
            var result = await SendAsync("ObtenerFile", param);

            return Ok(result);
        }

        #region Beneficiario

        public async Task<IHttpActionResult> ObtenerBeneficiario(clsParametro param)
        {
            var result = await SendAsync("ObtenerBeneficiario", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarBeneficiario(clsParametro param)
        {
            var result = await SendAsync("RegistrarBeneficiario", param);

            return Ok(result);
        }

        #endregion Beneficiario

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<IHttpActionResult> RegistrarUsuario(clsParametro param)
        {
            var result = await SendAsync("RegistrarUsuario", param);

            return Ok(result);
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<IHttpActionResult> ActivarCuentaXsms(clsParametro param)
        {
            var result = await SendAsync("ActivarCuentaXsms", param);

            return Ok(result);
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<IHttpActionResult> ReenviarSMS(clsParametro param)
        {
            var result = await SendAsync("ReenviarSMS", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerDatosConfirmacion(clsParametro param)
        {
            var result = await SendAsync("ObtenerDatosConfirmacion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> SolicitarNewPassword(clsParametro param)
        {
            var result = await SendAsync("SolicitarNewPassword", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> DatosNroServicio(clsParametro param)
        {
            var result = await SendAsync("DatosNroServicio", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListarNroServicio(clsParametro param)
        {
            var result = await SendAsync("ListarNroServicio", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> LecturaRegistrar(clsParametro param)
        {
            var result = await SendAsync("LecturaRegistrar", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> AyudaLista(clsParametro param)
        {
            var result = await SendAsync("AyudaLista", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListAppConfig(clsParametro param)
        {
            var result = await SendAsync("ListAppConfig", param);

            return Ok(result);
        }

        #region Cobranza

        public async Task<IHttpActionResult> ConsultarDeudaDetalle(clsParametro param)
        {
            var result = await SendAsync("ConsultarDeudaDetalle", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> GrabarIntentoDetalle(clsParametro param)
        {
            var result = await SendAsync("GrabarIntentoDetalle", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerIntento(clsParametro param)
        {
            var result = await SendAsync("ObtenerIntento", param);

            return Ok(result);
        }

        #endregion Cobranza

        #region BonoElectrico

        public async Task<IHttpActionResult> ObtenerBonoElectrico(clsParametro param)
        {
            var result = await SendAsync("ObtenerBonoElectrico", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerBonoElectricoDNI(clsParametro param)
        {
            var result = await SendAsync("ObtenerBonoElectricoDNI", param);

            return Ok(result);
        }

        #endregion BonoElectrico

        #endregion Public
    }
}