﻿using SISAC.Negocio.Maestro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SISAC.Site.Maestro.Controllers
{
    public class OrganizacionController : ApiController
    {
        #region Field

        private static readonly ClsNegocioOrganizacion _Organizacion = ClsNegocioOrganizacion.Instancia;

        #endregion Field

        #region Public

        public IHttpActionResult ListarUUNNxIdEmpresa(String id)
        {
            var result = _Organizacion.ListarUUNNxIdEmpresa(Convert.ToInt16(id));            

            return Ok(result);
        }

        public IHttpActionResult ListarCCSSxIdUUNN(String id)
        {
            var result = _Organizacion.ListarCCSSxIdUUNN(Convert.ToInt16(id));

            return Ok(result);
        }

        public IHttpActionResult ListarSectoresxIdCCSS(String id)
        {
            var result = _Organizacion.ListarSectoresxIdCCSS(Convert.ToInt16(id));

            return Ok(result);
        }

        public IHttpActionResult ListarRutasxIdSector(String id)
        {
            var result = _Organizacion.ListarRutasxIdSector(Convert.ToInt16(id));

            return Ok(result);
        }

        #endregion
    }
}