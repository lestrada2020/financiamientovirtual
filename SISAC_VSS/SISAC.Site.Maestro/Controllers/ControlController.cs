﻿using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using System.Web.Http;

namespace SISAC.Site.Maestro.Controllers
{
    public class ControlController : ApiController
    {
        #region Field

        private static readonly clsNegocioControl _Control = clsNegocioControl.Instancia;

        #endregion Field

        #region Public

        #region Cache

        public IHttpActionResult CacheList(clsParametro param)
        {
            var result = _Control.CacheList(param);

            return Ok(result);
        }

        public IHttpActionResult CacheClear(clsParametro param)
        {
            var result = _Control.CacheClear(param);

            return Ok(result);
        }

        #endregion Cache

        public IHttpActionResult AyudaLista(clsParametro param)
        {
            var result = _Control.AyudaLista(param);

            return Ok(result);
        }

        /// <summary>
        /// Requiere que el servidor donde se instale pueda enviar EMAIL, ejemplo desde base de datos.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IHttpActionResult SendMail(clsParametro param)
        {
            var result = _Control.SendMail(param);

            return Ok(result);
        }

        #endregion Public
    }
}