﻿using SISAC.Entidad.Maestro;
using SISAC.Negocio.NTCSE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SISAC.Site.NTCSE.Controllers
{
    public class IndicadorSaifiSaidiController : ApiController
    {

        #region Field

        private static readonly clsNegocioIndicadorSaifiSaidi _IndicaSaifiSaidi = clsNegocioIndicadorSaifiSaidi.Instancia;

        #endregion Field


        #region Public

        public IHttpActionResult ObtenerTabularIndicadorAcumulado(clsParametro param)
        {
            var result = _IndicaSaifiSaidi.ObtenerTabularIndicadorAcumulado(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerIndicadorAcumulado(clsParametro param)
        {
            var result = _IndicaSaifiSaidi.ObtenerIndicadorAcumulado(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerEvolucionIndicadorAcumulado(clsParametro param)
        {
            var result = _IndicaSaifiSaidi.ObtenerEvolucionIndicadorAcumulado(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerProcesosxSimulacion(clsParametro param)
        {
            var result = _IndicaSaifiSaidi.ObtenerProcesosxSimulacion(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerIndicadorAcumuladoMovil(clsParametro param)
        {
            var result = _IndicaSaifiSaidi.ObtenerIndicadorAcumuladoMovil(param);

            return Ok(result);
        }

        #endregion Public

        #region Ayuda Combo

        public IHttpActionResult AyudaLista(clsParametro param)
        {
            var result = _IndicaSaifiSaidi.AyudaLista(param);

            return Ok(result);
        }              

        #endregion
    }
}