﻿using SISAC.Entidad.Maestro;
using SISAC.Negocio.NTCSE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SISAC.Site.NTCSE.Controllers
{
    public class ConsultaInterrupcionController : ApiController
    {
        #region Field

        private static readonly clsNegocioConsultaInterrupcion _ConsulInterrup = clsNegocioConsultaInterrupcion.Instancia;

        #endregion Field


        #region Public

        public IHttpActionResult ObtenerDatosInterrupcion(clsParametro param)
        {
            var result = _ConsulInterrup.ObtenerDatosInterrupcion(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosReposicionParcialInterrupcion(clsParametro param)
        {
            var result = _ConsulInterrup.ObtenerDatosReposicionParcialInterrupcion(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosGPSSuministro(clsParametro param)
        {
            var result = _ConsulInterrup.ObtenerDatosGPSSuministro(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosGPSSuministroLejano(clsParametro param)
        {
            var result = _ConsulInterrup.ObtenerDatosGPSSuministroLejano(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerSuministrosAfectadosGPS(clsParametro param)
        {
            var result = _ConsulInterrup.ObtenerSuministrosAfectadosGPS(param);

            return Ok(result);
        }

        #endregion Public
    }
}