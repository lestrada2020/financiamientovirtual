﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Threading;

namespace SISAC.Data.Seguridad
{
    public sealed class clsPerfilUsuarioDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsPerfilUsuarioDAO> _Instancia = new Lazy<clsPerfilUsuarioDAO>(() => new clsPerfilUsuarioDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsPerfilUsuarioDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsPerfilUsuarioDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataRow ValidarToken(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("SeguridadWeb", "ValidarToken", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListAppConfig(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("SeguridadWeb", "ListAppConfig", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDatosUsuario(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("SeguridadWeb", "ObtenerDatosUsuario", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerModulosAplicacionPorUsuario(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("SeguridadWeb", "ObtenerModulosAplicacionPorUsuario", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerRolesAplicacionPorUsuario(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("SeguridadWeb", "ObtenerRolesAplicacionPorUsuario", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerOpcionesPorModuloAplicacionUsuario(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("SeguridadWeb", "ObtenerOpcionesPorModuloAplicacionUsuario", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public DataTable ObtenerRestriccionVistaUsuario(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("SeguridadWeb", "ObtenerRestriccionVistaUsuario", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #endregion Public
    }
}