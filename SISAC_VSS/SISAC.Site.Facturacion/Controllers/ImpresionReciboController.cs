﻿using SISAC.Entidad.Maestro;
using SISAC.Negocio.Facturacion;
using SISAC.Site.Facturacion.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Site.Facturacion.Controllers
{
    public class ImpresionReciboController : ApiController
    {
        #region Field

        private static readonly clsNegocioSolicitudImpresionRecibo _SolicImpresionRecibo = clsNegocioSolicitudImpresionRecibo.Instancia;

        #endregion Field


        #region Public

        public IHttpActionResult ObtenerListaSolicitudImpresionRecibos(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerListaSolicitudImpresionRecibos(param);

            return Ok(result);
        }


        public IHttpActionResult AyudaLista(clsParametro param)
        {
            var result = _SolicImpresionRecibo.AyudaLista(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerSolicitudImpresionRecibo(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerSolicitudImpresionRecibo(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleSolicitudImpresionRecibo(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerDetalleSolicitudImpresionRecibo(param);

            return Ok(result);
        }

        public IHttpActionResult ConsultarSuministrosNotificar(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ConsultarSuministrosNotificar(param);

            return Ok(result);
        }

        public IHttpActionResult ConsultarMonitoreoTareasLista(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ConsultarMonitoreoTareasLista(param);

            return Ok(result);
        }

        public IHttpActionResult ConsultarLogProceso(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ConsultarLogProceso(param);

            return Ok(result);
        }//

        public IHttpActionResult ConsultarLogTareaEjecucion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ConsultarLogTareaEjecucion(param);

            return Ok(result);
        }//

        public IHttpActionResult ConsultarLogInconsistencias(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ConsultarLogInconsistencias(param);

            return Ok(result);
        }//

        public IHttpActionResult ActualizarEstado(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ActualizarEstado(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerAlcancePersonalProveedorActividad(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerAlcancePersonalProveedorActividad(param);

            return Ok(result);
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public IHttpActionResult ObtenerListaSolicitudImpresionRecibosPaginada(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerListaSolicitudImpresionRecibosPaginada(param);

            return Ok(result);
        }

        public IHttpActionResult ProcesarArchivoImpresion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ProcesarArchivoImpresion(param);

            return Ok(result);
        }

        public IHttpActionResult NotificarClientes(clsParametro param)
        {
            var result = _SolicImpresionRecibo.NotificarClientes(param);

            return Ok(result);
        }

        public IHttpActionResult ValidarNotificarClientes(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ValidarNotificarClientes(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerConfiguracion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerConfiguracion(param);

            return Ok(result);
        }

        public IHttpActionResult GuardarConfiguracion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.GuardarConfiguracion(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerListaAplicacionNotificacion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerListaAplicacionNotificacion(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerListaProcesoNotificacion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerListaProcesoNotificacion(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerPdfSolicitudImpresion(clsParametro param)
        {
            var result = await _SolicImpresionRecibo.ObtenerPdfSolicitudImpresionAsync(param);

            if (result != null)
            {
                return new CustomFileResult(result, "application/pdf");
            }
            else
            {
                return InternalServerError();
            }
        }

        public async Task<IHttpActionResult> ProcesarDescargaDatosPdfSolicitudImpresion(
            clsParametro param)
        {
            var result =
                await _SolicImpresionRecibo.ProcesarDescargaDatosPdfSolicitudImpresionAsync(param);

            return Ok(result);
        }

        public IHttpActionResult GenerarArchivoPdfSolicitudImpresion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.GenerarArchivoPdfSolicitudImpresion(param);

            return Ok(result);
        }

        public IHttpActionResult DescargarArchivoPdfSolicitudImpresion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.DescargarArchivoPdfSolicitudImpresion(param);

            if (result != null)
            {
                return new CustomFileResult(result, "application/pdf");
            }
            else
            {
                return InternalServerError();
            }
        }

        public IHttpActionResult CopiarPdfSolicitudImpresion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.CopiarPdfSolicitudImpresion(param);

            return Ok(result);
        }

        public IHttpActionResult DescargarArchivoPdfEncriptadoSolicitudImpresion(clsParametro param)
        {
            var result =
                _SolicImpresionRecibo.DescargarArchivoPdfEncriptadoSolicitudImpresion(param);

            if (result != null)
            {
                return new CustomFileResult(result, "application/pdf");
            }
            else
            {
                return InternalServerError();
            }
        }

        public IHttpActionResult ObtenerDatosEjecucionTareaPorId(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ObtenerDatosEjecucionTareaPorId(param);

            return Ok(result);
        }

        public IHttpActionResult ReenviarCorreoProcesamientoArchivoImpresion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ReenviarCorreoProcesamientoArchivoImpresion(param);

            return Ok(result);
        }

        //CanviaDev_2020-05-27
        public IHttpActionResult GenerarArchivosPDFRecibos(clsParametro param)
        {
            var result = _SolicImpresionRecibo.GenerarArchivosPDFRecibos(param);

            return Ok(result);
        }
        //CanviaDev_2020-05-27:Fin

        //CanviaDev_2020-06-01
        public IHttpActionResult ValidarSuministrosImportarNotificacion(clsParametro param)
        {
            var result = _SolicImpresionRecibo.ValidarSuministrosImportarNotificacion(param);

            return Ok(result);
        }
        //CanviaDev_2020-06-01:Fin

        #endregion

        #endregion Public
    }
}