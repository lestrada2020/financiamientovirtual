﻿using SISAC.Entidad.Maestro;
using SISAC.Negocio.Facturacion;
using System.Web.Http;

namespace SISAC.Site.Facturacion.Controllers
{
    public class ReciboController : ApiController
    {
        #region Field

        private static readonly clsNegocioRecibo _Recibo = clsNegocioRecibo.Instancia;

        #endregion Field

        #region Public

        public IHttpActionResult DescargarRecibo(clsParametro param)
        {
            var result = _Recibo.DescargarRecibo(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerJSON(clsParametro param)
        {
            var result = _Recibo.ObtenerJSON(param);

            return Ok(result);
        }

        #endregion Public
    }
}