﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Data;
using System.Threading;

namespace SISAC.Data.GISOnWeb
{
    public sealed class clsConsultaDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsConsultaDAO> _Instancia = new Lazy<clsConsultaDAO>(() => new clsConsultaDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsConsultaDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsConsultaDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataRow GetConfiguration(clsParametro entidad)
        {
            try
            {
                if (!entidad.Parametros.ContainsKey("IdUUNN")) entidad.Parametros["IdUUNN"] = "0";
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("Servidor", "GetConfiguration", param, _DAO.GetConnectionString(3));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetConnectionString(clsParametro entidad)
        {
            try
            {
                var data = GetConfiguration(entidad);
                if (data == null) throw new Exception($"No existe configuración para la empresa {entidad.Parametros["IdEmpresa"]}");
                var result = $"Data Source={data["NombreServidor"]};Integrated Security=SSPI;Initial Catalog={data["NombreBaseDatos"]};Connect Timeout=15;APP=SISAC - GISOnWeb";

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CapaListarGeografia(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Capa", "ListarGeografia", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CapaListarElectrica(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("CapaEscalaEntidad", "Listar", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Escalaslistar(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Escalas", "Listar", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CapaListarEscalas(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("CapaEscalaEntidad", "Listar", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadListaBuscar(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Entidad", "ListaBuscar", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadListaPropiedad(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Entidad", "ListaPropiedad", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable UsuarioValida(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Usuario", "Valida", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable UsuarioPerfil(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Usuario", "Perfil", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadListPolygons(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Entidad", "ListarPoligonos", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadListLines(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Entidad", "ListarLineas", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadListPoints(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Entidad", "ListarPuntos", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadBuscar(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("DatosEntidad", "buscar", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadListar(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Entidad", "listar", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String EliminarCache(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEscalar("Entidad", "EliminarCache", param, GetConnectionString(entidad));

                return result.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EntidadListarLeyenda(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoEntidad("Entidad", "ListarLeyenda", param, GetConnectionString(entidad));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }
}