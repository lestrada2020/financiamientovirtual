﻿using SISAC.Data.Atencion;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Negocio.Proceso;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Threading;

namespace SISAC.Negocio.Atencion
{
   public class clsNegocioCobranzaEmpresa
    {

        #region Field

        private static readonly clsCobranzaEmpresaDAO _CobranzaDAO = clsCobranzaEmpresaDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly Lazy<clsNegocioCobranzaEmpresa> _Instancia = new Lazy<clsNegocioCobranzaEmpresa>(() => new clsNegocioCobranzaEmpresa(), LazyThreadSafetyMode.PublicationOnly);
        private static readonly ClsNegocioTarea _NegocioTarea = ClsNegocioTarea.Instancia;

        #endregion Field

        #region Property

        public static clsNegocioCobranzaEmpresa Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioCobranzaEmpresa()
        {

        }

        #endregion Constructor

        #region Public

        public clsResultado GrabarIntentoEmpresa(clsParametroBulkData entidad)
        {
            try
            {
                var table = _CobranzaDAO.GrabarIntentoEmpresa(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }



        public clsResultado PagarDeudaEmpresa(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.PagarDeudaEmpresa(entidad);

                int idpedido = 0;
   
                if (row.Table.Columns.Count == 2 && int.TryParse(row.ItemArray[1].ToString(), out idpedido))
                {
                     RegistrarTareaTransaccionPagoMasivoVisa(Convert.ToInt32(row["NroPedido"]));
                }

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }




        public clsResultado ListarBancos(clsParametro entidad)
        {
            try
            {
                var table = _CobranzaDAO.ListarBancos(entidad);
                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }


        public clsResultado ListarCuentas(clsParametro entidad)
        {
            try
            {
                var table = _CobranzaDAO.ListarCuentas(entidad);
                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }



        public clsResultado GrabarDepositoBancarioIntento(clsParametroBulkData entidad)
        {
            try
            {
                var table = _CobranzaDAO.GrabarDepositoBancarioIntento(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ActualizarDepositoBancarioEstado(clsParametro entidad)
        {
            try
            {
                var table = _CobranzaDAO.ActualizarDepositoBancarioEstado(entidad);


                if (table.Rows.Count > 0 && table.Rows[0][0].ToString()=="OK")
                {
                    RegistrarTareaTransaccionDepositoBancario(Convert.ToInt32(entidad.Parametros["nroOrden"]));
                }
              
                var result = new clsResultado();
                result.Datos = table;
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }





        public clsResultado RegistrarTareaTransaccionPagoMasivoVisa(int IdPedido)
        {
            var clsTareaProgramacion = new ClsTareaProgramacion
            {
                IdProceso = (int)ClienteEmpresa.TransaccionPagoMasivoVisa
            };

            try
            {
                //Obtener datos para creacion de tarea de procesamiento de archivo de impresion
                var clsParametro = new ClsParametroRegistroTransaccionPagoVisa
                {
                    IdNumeroOrden = Convert.ToInt32(IdPedido),
                    DatosVarios = string.Empty
                };

                clsTareaProgramacion.ParametroTarea = clsParametro;
                clsTareaProgramacion.Frecuencia = 3;
                clsTareaProgramacion.IntervaloEnSegundos = 300;
                //Grabar tarea
                int idTarea = _NegocioTarea.GrabarTareaInmediata(clsTareaProgramacion);
            }
            catch (Exception)
            {
                throw;
            }

            return new clsResultado();
        }




        public clsResultado RegistrarTareaTransaccionDepositoBancario(int IdPedido)
        {
            var clsTareaProgramacion = new ClsTareaProgramacion
            {
                IdProceso = (int)ClienteEmpresa.TransaccionDepositoBancario
            };

            try
            {
                //Obtener datos para creacion de tarea de procesamiento de archivo de impresion
                var clsParametro = new clsParametroTareaDepositoBancarioClienteEmpresa
                {
                    IdNumeroOrden = Convert.ToInt32(IdPedido),
                    EnumTipoOperacion = SISAC.Entidad.Facturacion.enumOperacionDepositoBancario.Conciliacion
                };

                clsTareaProgramacion.ParametroTarea = clsParametro;

                //Grabar tarea
                int idTarea = _NegocioTarea.GrabarTareaInmediata(clsTareaProgramacion);
              


            }
            catch (Exception)
            {
                throw;
            }

            return new clsResultado();
        }


     


        #endregion Public
    }
}
