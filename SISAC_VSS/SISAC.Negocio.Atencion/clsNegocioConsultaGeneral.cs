﻿using SISAC.Data.Atencion;
using SISAC.Entidad.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Threading;

namespace SISAC.Negocio.Atencion
{
    #region clsNegocioConsultaGeneral

    public sealed class clsNegocioConsultaGeneral
    {
        #region Field

        private static readonly clsConsultaGeneralDAO _ConsultaGeneralDAO = clsConsultaGeneralDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly Lazy<clsNegocioConsultaGeneral> _Instancia = new Lazy<clsNegocioConsultaGeneral>(() => new clsNegocioConsultaGeneral(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioConsultaGeneral Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioConsultaGeneral()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado GetData(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.GetData(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Resumen(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Resumen(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Basico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Basico(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Restriccion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Restriccion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado CtaCte(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var set = _ConsultaGeneralDAO.CtaCte(entidad);

                var result = new clsResultado();
                result.Datos = set;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Facturacion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Facturacion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado FacturacionConcepto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.FacturacionConcepto(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado FacturacionDocumento(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.FacturacionDocumento(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ReciboBasico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ReciboBasico(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ReciboMedidor(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ReciboMedidor(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ReciboLectura(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ReciboLectura(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ReciboConcepto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ReciboConcepto(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Atencion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Atencion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado FotoLectura(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.FotoLectura(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Lectura(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Lectura(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado AFavor(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.AFavor(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Pagos(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Pagos(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado TecnicoBasico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.TecnicoBasico(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado TecnicoMagnitud(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.TecnicoMagnitud(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado TecnicoPrecinto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.TecnicoPrecinto(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado TecnicoTransformador(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.TecnicoTransformador(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado OrdenTrabajo(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.OrdenTrabajo(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado FotoActividad(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.FotoActividad(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Foto(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.Foto(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado MedidorBasico(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var set = _ConsultaGeneralDAO.MedidorBasico(entidad);

                var result = new clsResultado();
                result.Datos = set;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado MedidorLista(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.MedidorLista(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado MedidorLecturas(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.MedidorLecturas(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado Cambios(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var set = _ConsultaGeneralDAO.Cambios(entidad);

                var result = new clsResultado();
                result.Datos = set;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConvenioLista(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ConvenioLista(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConvenioCreacion(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ConvenioCreacion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConvenioRegistro(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ConvenioRegistro(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConvenioDocumentos(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ConvenioDocumentos(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConvenioGarantias(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ConvenioGarantias(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConvenioAmortizaciones(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ConvenioAmortizaciones(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConvenioLetra(clsConsultaGeneralFiltro entidad)
        {
            try
            {
                var table = _ConsultaGeneralDAO.ConvenioLetra(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion Public
    }

    #endregion clsNegocioConsultaGeneral
}