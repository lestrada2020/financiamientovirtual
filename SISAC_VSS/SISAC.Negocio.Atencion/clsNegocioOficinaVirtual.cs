﻿using SISAC.Data.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Negocio.Atencion
{
    public sealed class clsNegocioOficinaVirtual
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly clsOficinaVirtualDAO _OficinaVirtualDAO = clsOficinaVirtualDAO.Instancia;
        private static readonly Lazy<clsNegocioOficinaVirtual> _Instancia = new Lazy<clsNegocioOficinaVirtual>(() => new clsNegocioOficinaVirtual(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioOficinaVirtual Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioOficinaVirtual()
        {
        }

        #endregion Constructor

        #region Public

        public async Task<clsResultado> ConsultaAtencionesSuministro(clsParametro entidad)
        {
            try
            {
                var table = await _OficinaVirtualDAO.ConsultaAtencionesSuministro(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> ConsultaAtencionNroAtencion(clsParametro entidad)
        {
            try
            {
                var row = await _OficinaVirtualDAO.ConsultaAtencionNroAtencion(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> RegistrarContactanos(clsParametro entidad)
        {
            try
            {
                var id = await _OficinaVirtualDAO.RegistrarContactanos(entidad);

                entidad.Parametros["FromBase64"] = "Archivo";
                entidad.Parametros["IdContactanos"] = id.ToString();

                if (!String.IsNullOrWhiteSpace(entidad.Parametros["Archivo"]) && id > 0)
                    await _OficinaVirtualDAO.RegistrarContactanosArchivo(entidad);

                var result = new clsResultado();
                result.Datos = id;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> ListarInterrupciones(clsParametro entidad)
        {
            try
            {
                var table = await _OficinaVirtualDAO.ListarInterrupciones(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> ConfirmarEmail(clsParametro entidad)
        {
            try
            {
                var row = await _OficinaVirtualDAO.ConfirmarEmail(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> RegistrarReclamo(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.RegistrarReclamo(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> InfoSuministro(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.InfoSuministro(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> ObtenerObservacion(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.ObtenerObservacion(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> DesasociarSuministro(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.DesasociarSuministro(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> CambiarPassword(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.CambiarPassword(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> ListarByNroAtencion(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.ListarByNroAtencion(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> RegistrarFotoLec(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.RegistrarFotoLec(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> VisaListFailed(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.VisaListFailed(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> VisaSendMailPayFailed(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.VisaSendMailPayFailed(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #region Cliente Empresa

        public clsResultado ValidaAccesoClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.ValidaAccesoClienteEmpresa(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado RegistrarClienteEmpresaAdmin(clsParametro entidad)
        {
            try
            {
                var value = _OficinaVirtualDAO.RegistrarClienteEmpresaAdmin(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado BuscarPorIdentidad(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.BuscarPorIdentidad(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ListarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.ListarClienteEmpresa(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ListarRoles(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.ListarRoles(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado TipoRelacion(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.TipoRelacion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado RegistrarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var value = _OficinaVirtualDAO.RegistrarClienteEmpresa(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado EditarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var value = _OficinaVirtualDAO.EditarClienteEmpresa(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado EliminarClienteEmpresa(clsParametro entidad)
        {
            try
            {
                var value = _OficinaVirtualDAO.EliminarClienteEmpresa(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ListarTarifaActiva(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.ListarTarifaActiva(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        public clsResultado ListarSuministros(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.ListarSuministros(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        public clsResultado ListarSuministrosDetallePago(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.ListarSuministrosDetallePago(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        public clsResultado EliminarSuministroAsociado(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.EliminarSuministroAsociado(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        public clsResultado BuscarSuministroAsociar(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.BuscarSuministroAsociar(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        public clsResultado RegistrarSuministroAsociar(clsParametro entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.RegistrarSuministroAsociar(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        public clsResultado ListarSuministrosCarga(clsParametroBulkData entidad)
        {
            try
            {
                var table = _OficinaVirtualDAO.ListarSuministrosCarga(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        #endregion Cliente Empresa

        public async Task<clsResultado> ObtenerFile(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.ObtenerFile(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #region Beneficiario

        public async Task<clsResultado> ObtenerBeneficiario(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.ObtenerBeneficiario(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> RegistrarBeneficiario(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.RegistrarBeneficiario(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #endregion Beneficiario

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<clsResultado> RegistrarUsuario(clsParametro entidad)
        {
            try
            {
                ///GENERANDO CODIGO SI EL NUMERO DE CELULAR HA SIDO ESTABLECIDO

                entidad.Parametros["codigoactivacion"] = Convert.ToString(new Random().Next(100000, 900000));

                //REGISTRANDO USUARIO
                var table = await _OficinaVirtualDAO.RegistrarUsuario(entidad);

                //SI EL REGISTRO FUE EXITOSO SE ENVIAR UN SMS AL CELULAR DEL USUARIO
                if (entidad.Parametros["movil"] != "")
                {
                    var error = ObtenerValor(table.ToString(), "error", "0");

                    if (error == "0") //Insercion exitosa
                    {
                        var celular = entidad.Parametros["movil"];
                        var mensaje = ObtenerValor(table.ToString(), "MessageSMS", "");

                        await EnviarMensajeSMS(mensaje, 1, celular);
                    }
                }

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<clsResultado> ActivarCuentaXsms(clsParametro entidad)
        {
            try
            {
                var table = await _OficinaVirtualDAO.ActivarCuentaXsms(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        //JOEL CORONEL - ACTIVACION SMS
        public async Task<clsResultado> ReenviarSMS(clsParametro param)
        {
            try
            {
                var codigoActivacion = Convert.ToString(new Random().Next(100000, 900000));
                var parametros = new clsParametro();
                parametros.Parametros = new Dictionary<String, String>();
                parametros.Parametros["dni"] = param.Parametros["documentoIdentidad"];
                parametros.Parametros["nuevocodigoactivacion"] = codigoActivacion;

                var mensaje = "Distriluz. Su Código de activación de cuenta es: " + codigoActivacion + ". Gracias por registrarse.";

                //ACTUALIZANDO EL NUEVO CODIGO DE ACTIVACION PARA EL USUARIO
                var table = await _OficinaVirtualDAO.ActualizarCodigoSMS(parametros);
                var celular = (table.Split(';')[2]).Split('|')[1];

                //REEENVIANDO CODIGO POR SMS AL CELULAR DEL USUARIO

                await EnviarMensajeSMS(mensaje, 1, celular);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex);
            }
        }

        //JOEL CORONEL - ACTIVACION SMS
        private async Task EnviarMensajeSMS(string mensajeNotificacion, short idEmpresa, string nroCelular)
        {
            var dtCfgGateWay = await _OficinaVirtualDAO.ObtenerConfiguracionGateWaySMS(idEmpresa);
            if (dtCfgGateWay.Rows.Count == 0) throw new Exception("No se ha encontrado configuración para el envío de mensajes SMS");

            var oEnviarSMS = new clsSendSMS();
            oEnviarSMS.Url = dtCfgGateWay.Rows[0]["Url"].ToString();
            oEnviarSMS.UserId = dtCfgGateWay.Rows[0]["UserId"].ToString();
            oEnviarSMS.Pwd = dtCfgGateWay.Rows[0]["Pwd"].ToString();
            oEnviarSMS.UserIdApiSMS = dtCfgGateWay.Rows[0]["UserIdApiSMS"].ToString();
            oEnviarSMS.PwdApiSMS = dtCfgGateWay.Rows[0]["PwdApiSMS"].ToString();
            oEnviarSMS.ApiSMS = dtCfgGateWay.Rows[0]["ApiSMS"].ToString();
            oEnviarSMS.ApiVersion = dtCfgGateWay.Rows[0]["ApiVersion"].ToString();
            oEnviarSMS.PrefijoCelular = dtCfgGateWay.Rows[0]["PrefijoCelular"].ToString();

            //ECALDAS - ACTIVACION SMS
            oEnviarSMS.EsCfgEXternaSMS = 0;
            oEnviarSMS.IdMetodoEnvioSMS = 0;
            oEnviarSMS.UsaSSL = 0;
            oEnviarSMS.UsaOperadorSMS = 0;
            oEnviarSMS.IdOperadorSMS = 0;

            if (dtCfgGateWay.Columns.Contains("EsCfgEXternaSMS"))
            {
                oEnviarSMS.EsCfgEXternaSMS = (short)dtCfgGateWay.Rows[0]["EsCfgEXternaSMS"];
            }
            if (dtCfgGateWay.Columns.Contains("IdMetodoEnvioSMS"))
            {
                oEnviarSMS.IdMetodoEnvioSMS = (short)dtCfgGateWay.Rows[0]["IdMetodoEnvioSMS"];
            }
            if (dtCfgGateWay.Columns.Contains("UsaSSL"))
            {
                oEnviarSMS.UsaSSL = (short)dtCfgGateWay.Rows[0]["UsaSSL"];
            }
            if (dtCfgGateWay.Columns.Contains("UsaOperadorSMS"))
            {
                oEnviarSMS.UsaOperadorSMS = (short)dtCfgGateWay.Rows[0]["UsaOperadorSMS"];
            }
            if (dtCfgGateWay.Columns.Contains("IdOperadorSMS"))
            {
                oEnviarSMS.IdOperadorSMS = (short)dtCfgGateWay.Rows[0]["IdOperadorSMS"];
            }
            //

            dtCfgGateWay.Dispose();

            try
            {
                string peticionNotificacion = string.Empty;
                string respuestaNotificacion = string.Empty;

                //ECALDAS - ACTIVACION SMS
                if (oEnviarSMS.EsCfgEXternaSMS == 0)
                {
                    var idCanal = await _OficinaVirtualDAO.ObtenerCanalSMS(idEmpresa);
                    respuestaNotificacion = oEnviarSMS.EviarMensajeSMS(nroCelular, mensajeNotificacion, idCanal.ToString(), ref peticionNotificacion);
                }
                else if (oEnviarSMS.IdMetodoEnvioSMS == 1) //GET
                {
                    respuestaNotificacion = oEnviarSMS.EnviarMensajeSMS(nroCelular, mensajeNotificacion, oEnviarSMS.UsaSSL, ref peticionNotificacion);
                }
                else if (oEnviarSMS.IdMetodoEnvioSMS == 2) //POST
                {
                    var listaNroCelular = new List<string>();
                    listaNroCelular.Add(nroCelular);
                    respuestaNotificacion = oEnviarSMS.EnviarMensajeSMS(listaNroCelular, mensajeNotificacion, oEnviarSMS.UsaSSL, ref peticionNotificacion);
                }
                else
                {
                    throw new NotImplementedException();
                }
                //
            }
            catch (Exception)
            {
                throw new Exception("No se pudo enviar el mensaje de texto SMS. Por favor vuelva a intentar en unos minutos.");
            }
        }

        //JOEL CORONEL - ACTIVACION SMS
        public static string ObtenerValor(string controlTag, string clave, string valorPredeterminado)
        {
            clave = clave.ToLower();

            if (string.IsNullOrEmpty(controlTag) || !controlTag.ToLower().Contains(clave)) return valorPredeterminado;

            var valor = valorPredeterminado;
            var comandos = controlTag.Split(';');

            try
            {
                foreach (var item in comandos)
                {
                    if (!string.IsNullOrEmpty(item) && item.Split('|')[0].ToString().ToLower().Equals(clave)) valor = item.Split('|')[1];
                }
            }
            catch (Exception ex)
            {
                return valorPredeterminado;
            }

            return valor;
        }

        public async Task<clsResultado> ObtenerDatosConfirmacion(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.ObtenerDatosConfirmacion(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> SolicitarNewPassword(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.SolicitarNewPassword(entidad);

                if (Convert.ToInt16(value["IsSendSMS"]) == 1) await EnviarMensajeSMS(value["MessageSMS"].ToString(), Convert.ToInt16(value["IdEmpresa"]), value["PhoneSMS"].ToString());

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> DatosNroServicio(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.DatosNroServicio(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> ListarNroServicio(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.ListarNroServicio(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> LecturaRegistrar(clsParametro entidad)
        {
            try
            {
                var value = await _OficinaVirtualDAO.LecturaRegistrar(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> AyudaLista(clsParametro entidad)
        {
            try
            {
                var table = await _OficinaVirtualDAO.AyudaLista(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ListAppConfig(clsParametro entidad)
        {
            try
            {
                var table = await _OficinaVirtualDAO.ListAppConfig(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #region Cobranza

        public async Task<clsResultado> ConsultarDeudaDetalle(clsParametro entidad)
        {
            try
            {
                var table = await _OficinaVirtualDAO.ConsultarDeudaDetalle(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> GrabarIntentoDetalle(clsParametro entidad)
        {
            try
            {
                var row = await _OficinaVirtualDAO.GrabarIntentoDetalle(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ObtenerIntento(clsParametro entidad)
        {
            try
            {
                var row = await _OficinaVirtualDAO.ObtenerIntento(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion Cobranza

        #region BonoElectrico

        public async Task<clsResultado> ObtenerBonoElectrico(clsParametro entidad)
        {
            try
            {
                var data = await _OficinaVirtualDAO.ObtenerBonoElectrico(entidad);

                var result = new clsResultado();
                result.Datos = data.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ObtenerBonoElectricoDNI(clsParametro entidad)
        {
            try
            {
                var data = await _OficinaVirtualDAO.ObtenerBonoElectricoDNI(entidad);

                var result = new clsResultado();
                result.Datos = data.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion BonoElectrico

        #endregion Public
    }
}