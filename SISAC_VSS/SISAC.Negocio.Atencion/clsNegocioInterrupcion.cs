﻿using SISAC.Data.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Negocio.Atencion
{

    #region clsNegocioInterrupcion

    public sealed class clsNegocioInterrupcion
    {
        #region Field

        private static readonly clsInterrupcionDAO _AtencionInterrupcionDAO = clsInterrupcionDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly Lazy<clsNegocioInterrupcion> _Instancia = new Lazy<clsNegocioInterrupcion>(() => new clsNegocioInterrupcion(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioInterrupcion Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioInterrupcion()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado ObtenerConfiguracionReporte(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerConfiguracionReporte(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }        

        public clsResultado AyudaLista(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.AyudaLista(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }


        public clsResultado ObtenerMotivoFomatoNotifica(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerMotivoFomatoNotifica(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerResumenxUUNNTiempo(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerResumenxUUNNTiempo(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerResumenxUUNNAreaTiempo(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerResumenxUUNNAreaTiempo(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerResumenxUUNNMotivoTiempo(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerResumenxUUNNMotivoTiempo(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerResumenxUUNNSEDTiempo(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerResumenxUUNNSEDTiempo(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerDetalleSuministros(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerDetalleSuministros(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerDetalleSEDs(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerDetalleSEDs(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado GenerarTokenReporte(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.GenerarTokenReporte(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerAtencionesxRecursoInterrupcion(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.ObtenerAtencionesxRecursoInterrupcion(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado LimpiarTablaReporte(clsParametro entidad)
        {
            try
            {
                var table = _AtencionInterrupcionDAO.LimpiarTablaReporte(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }
               

        #endregion Public
    }

    #endregion clsNegocioInterrupcion
}
