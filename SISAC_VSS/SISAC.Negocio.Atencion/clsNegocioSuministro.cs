﻿using SISAC.Data.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Negocio.Atencion
{
    public sealed class clsNegocioSuministro
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly clsSuministroDAO _SuminDAO = clsSuministroDAO.Instancia;
        private static readonly clsCobranzaDAO _CobranzaDAO = clsCobranzaDAO.Instancia;
        private static readonly clsOficinaVirtualDAO _OVDAO = clsOficinaVirtualDAO.Instancia;
        private static readonly Lazy<clsNegocioSuministro> _Instancia = new Lazy<clsNegocioSuministro>(() => new clsNegocioSuministro(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioSuministro Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioSuministro()
        {
        }

        #endregion Constructor

        #region Private

        private async Task<clsResultado> GetTipoInterrupcion(Int32 idNroServicio, Int16 idType)
        {
            try
            {
                var result = new clsResultado();
                result.Datos = new { };

                var table = await _OVDAO.ListarInterrupciones(new clsParametro() { Parametros = new Dictionary<String, String> { { "IdNroServicio", idNroServicio.ToString() } } });
                var row = (DataRow)null;

                foreach (DataRow item in table.Rows)
                {
                    var a = Convert.ToInt16(item["EsProgramada"]);

                    if ((a < 2 && idType == 1) || (a == 2 && idType == 2))
                    {
                        row = item;
                        break;
                    }
                }

                if (row != null)
                {
                    if (idType == 1)
                    {
                        result.Datos = new
                        {
                            EsProgramada = Convert.ToInt16(row["EsProgramada"]) == 1 ? "SI" : "NO",
                            FechaInicio = row["FechaInicio"],
                            FechaFin = row["FechaFin"],
                            HoraInicio = row["HoraInicio"],
                            HoraFin = row["HoraFin"]
                        };
                    }
                    else if (idType == 2)
                    {
                        result.Datos = new
                        {
                            Fecha = row["FechaInicio"],
                            Hora = row["HoraInicio"]
                        };
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion Private

        #region Public

        public async Task<clsResultado> DeudaFechaVence(clsParametro entidad)
        {
            try
            {
                Convert.ToInt32(entidad.Parametros["IdNroServicio"]);

                var result = new clsResultado();
                var data = (DataRow)null;

                try
                {
                    data = _CobranzaDAO.ConsultarDeuda(entidad);
                }
                catch (Exception ex)
                {
                    var s = ex.Message;

                    if (s.Contains("<<<"))
                    {
                        s = s.Substring(s.IndexOf("<<<") + 3);

                        if (s.Contains(">>>")) s = s.Substring(0, s.IndexOf(">>>"));
                    }

                    result.IdError = 1;
                    result.Mensaje = s;

                    return result;
                }

                result.Datos = new
                {
                    Deuda = data["ImporteTotal"],
                    FechaVencimiento = data["FechaVencimiento"]
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> InterrupcionServicio(clsParametro entidad)
        {
            try
            {
                return await GetTipoInterrupcion(Convert.ToInt32(entidad.Parametros["IdNroServicio"]), 1);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> CortePorDeuda(clsParametro entidad)
        {
            try
            {
                return await GetTipoInterrupcion(Convert.ToInt32(entidad.Parametros["IdNroServicio"]), 2);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ConsultarDatos(clsParametro entidad)
        {
            try
            {
                entidad.Parametros["AddPreFix"] = "0";

                var idNroServicio = Convert.ToInt32(entidad.Parametros["IdNroServicio"]);
                var result = new clsResultado();
                var data = await _SuminDAO.ConsultarDatos(entidad);

                result.Datos = new
                {
                    IdNroServicio = String.IsNullOrEmpty(data["IdEmpresa"].ToString()) ? 0 : idNroServicio
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> RegistroInterrupcion(clsParametro entidad)
        {
            try
            {
                var result = new clsResultado();
                entidad.Parametros["IdApp"] = "2";
                var data = await _SuminDAO.RegistrarInterrupcion(entidad);

                result.Datos = new
                {
                    NroAtencion = data["NroAtencion"]
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> ConsultarRHD(clsParametro entidad)
        {
            try
            {
                var result = new clsResultado();
                var table = new DataTable();

                #region Variable

                var dateFormat = "yyyyMMdd";
                var anio = Convert.ToInt16(entidad.Parametros["Anio"]);
                var mes = Convert.ToInt16(entidad.Parametros["Mes"]);
                var typeTime = Convert.ToInt16(entidad.Parametros["TypeTime"]);
                var idReporte = Convert.ToInt16(entidad.Parametros["IdReporte"]);
                var fechaInicio = new DateTime(anio, (mes - 1) * typeTime + 1, 1, 0, 0, 0);
                var fechaFin = fechaInicio.AddMonths(typeTime).AddSeconds(-1);

                entidad.Parametros["IdUnidadNegocio"] = "0";
                entidad.Parametros["IdCentroServicio"] = "0";
                entidad.Parametros["DateFormat"] = dateFormat;
                entidad.Parametros["FechaInicio"] = fechaInicio.ToString(dateFormat);
                entidad.Parametros["FechaFin"] = fechaFin.ToString(dateFormat);
                //param.FileName = Server.MapPath(String.Format("DataRHD/{0}/RHD1.{1}.txt", param.IdEmpresa, param.IdReporte));

                #endregion Variable

                if (idReporte == 21) table = await _SuminDAO.ConsultarRHD(entidad);
                else
                {
                    var maxCount = 10;
                    var tasks = new List<Task<DataTable>>();
                    var tables = new List<DataTable>();
                    var listccss = await _SuminDAO.ListarParaReporteCCSS(entidad);

                    using (var semaphore = new SemaphoreSlim(0, maxCount))
                    {
                        foreach (DataRow row in listccss.Rows)
                        {
                            var task = Task.Run<DataTable>(() =>
                            {
                                var p = new Hashtable(entidad.Parametros);
                                p["IdCentroServicio"] = row["IdCentroServicio"];

                                semaphore.Wait();
                                var t = _SuminDAO.ConsultarRHD(new clsParametro() { Parametros = p.ToDictionary() });
                                semaphore.Release();

                                return t;
                            });

                            tasks.Add(task);
                        }

                        Thread.Sleep(300);
                        //semaphore.Release(maxCount);
                        tables = (await Task.WhenAll(tasks)).ToList();
                    }

                    tables.ForEach((s) => { if (s != null && s.Rows.Count > 0) table.Merge(s); });
                    result.Datos = $"Tables:{tables.Count},Rows: {table.Rows.Count}";
                }

                //if (entidad.Parametros["TypeFormat"] == "1") _ParametroTarea.FileName = table.ToText(_ParametroTarea.FileName);
                //else _ParametroTarea.FileName = table.ToExcel(_ParametroTarea.FileName, false);

                //if (_ParametroTarea.IdReporte == 21 && _ParametroTarea.TypeFormat == 1 && !String.IsNullOrEmpty(_ParametroTarea.FileName))
                //{
                //    Hashtable t = new Hashtable();
                //    t["@idorganizacion"] = 1;
                //    t["@idregistro"] = _ParametroTarea.IdEmpresa;

                //    DataRow row = _DAO.EjecutarComandoRegistro("Empresa", "AyudaRegistro", t);

                //    String message = String.Format("Empresa: {0}\t Trimestre: {1}\t Año: {2}\n\n", row["NombreEmpresa"], (Int16)Math.Ceiling(_ParametroTarea.FechaInicio.Month / (Decimal)3), _ParametroTarea.FechaInicio.Year);
                //    String newFileName = Path.Combine(Path.GetDirectoryName(_ParametroTarea.FileName), String.Format("ANEXO 2AP_{0}-{1:s}.txt", row["CodigoIdentificacion"], DateTime.Now).Replace(":", "-"));
                //    File.WriteAllText(newFileName, String.Format("{0}{1}", message, File.ReadAllText(_ParametroTarea.FileName)));

                //    _ParametroTarea.FileName = newFileName;
                //}

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<clsResultado> UltimaLectura(clsParametro entidad)
        {
            try
            {
                var result = new clsResultado();
                var row = await _SuminDAO.UltimaLectura(entidad);

                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion Public
    }
}