﻿
using SISAC.Data.Atencion;
using SISAC.Entidad.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Dynamic;
using System.IO;

namespace SISAC.Negocio.Atencion
{
    public class clsNegocioFinanciamiento
    {
        #region Field
        private static readonly Lazy<clsNegocioFinanciamiento> _Instancia = new Lazy<clsNegocioFinanciamiento>(() => new clsNegocioFinanciamiento(), LazyThreadSafetyMode.PublicationOnly);
        private static readonly clsFinanciamientoDAO _FinanciamientoDAO = clsFinanciamientoDAO.Instancia;


        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;


        decimal _MontoInicial = 0;
        decimal _MontoFinanciar = 0;
        decimal _SaldoAFavorUsar = 0;
        int _IdNroServicio = 0;
        int _IdUsuario = 0;
        short _IdMoneda = 1; //Soles //Default solo pruebas

        #endregion Field

        #region Property

        public static clsNegocioFinanciamiento Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioFinanciamiento()
        {

        }

        #endregion Constructor

        #region Public
        public clsResultado ListarSuministroAsociado(clsParametro param)
        {
            clsParametro parametro;
            //Dictionary<String, String> par;
            try
            {
                //Este usuario corresponde al Usuario Web 
                var result = new clsResultado();
                if (!param.Parametros.ContainsKey("IdUsuario"))
                {
                    result.Mensaje = "Parametros no contiene IdUsuario.";
                    result.IdError = 1;

                }
                else
                {

                    if (Convert.ToInt32(param.Parametros["IdUsuario"]).GetType().Equals(typeof(int)))
                    {
                        var table = _FinanciamientoDAO.ListarSuministroAsociado(param);
                        if (table.Rows.Count == 0)
                        {
                            result.Mensaje = "Usuario no tiene suministros asociados.";
                            result.IdError = 1;
                        }
                        else
                        {
                            table.Columns.Add("InteresFechaActual", typeof(double));
                            table.Columns.Add("TotalDeuda", typeof(double));

                            foreach (DataRow item in table.Rows)
                            {
                                parametro = new clsParametro();
                                parametro.Parametros = new Dictionary<string, string>();
                                //par.Add("idnroservicio", item["IdNroServicio"].ToString());
                                parametro.Parametros.Add("idnroservicio", item["IdNroServicio"].ToString());
                                item["InteresFechaActual"] = _FinanciamientoDAO.ObtenerInteresFechaActual(parametro);
                                item["TotalDeuda"] = Convert.ToDouble(item["Deuda"].ToString()) + Convert.ToDouble(item["InteresFechaActual"].ToString());
                            }
                        }
                        result.Datos = table;
                    }

                }

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, param);
            }
        }



        #region comprobante de pago




        #endregion

        #region LESTRADA

        public clsResultado ObtenerParametrosConvenio(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerParametrosConvenio(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }
        public clsResultado ObtenerRutaVideoFinanciamiento(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerRutaVideoFinanciamiento(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerCentroServicios(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerCentroServicios(entidad);
                var result = new clsResultado();
                result.Datos = table;
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerPorcentajesConvenio(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerPorcentajesConvenio(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerConveniosPorEmpresa(clsParametro entidad)
        {
            clsParametro parametro;
            try
            {
                parametro = new clsParametro();
                parametro.Parametros = new Dictionary<string, string>();

                var table = _FinanciamientoDAO.ObtenerConveniosPorEmpresa(parametro);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerConvenioPorId(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerConvenioPorId(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerDetalleDeuda(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerDetalleDeuda(entidad);
                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerConfiguracionGlobal(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerConfiguracionGlobal(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerDetalleOrdenCobro(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ObtenerDetalleOrdenCobro(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerCabeceraOrdenCobro(clsParametro entidad)
        {
            try
            {
                var value = _FinanciamientoDAO.ObtenerCabeceraOrdenCobro(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado GrabarIntento(clsParametro entidad)
        {
            try
            {
                var row = _FinanciamientoDAO.GrabarIntento(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> GenerarNroPedido(clsParametro entidad)
        {
            try
            {
                var row = await _FinanciamientoDAO.GenerarNroPedido(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public async Task<clsResultado> VisaListFailed(clsParametro entidad)
        {
            try
            {
                var value = await _FinanciamientoDAO.VisaListFailed(entidad);

                var result = new clsResultado();
                result.Datos = value;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #region OBTENER COMPROBANTE PAGO
        public clsResultado ObtenerDocumentoComprobantePago(clsParametro entidad)
        {

            try
            {
                Int16 _eshomologada;
                EListaNotaCreditoDebitoControlGestor documento;
                var idnroservicio = Convert.ToInt32(entidad.Parametros["idnroservicio"]);
                var idnumeroorden = Convert.ToInt32(entidad.Parametros["idnumeroorden"]);

                var row = _FinanciamientoDAO.ObtenerParametrosComprobantePago(idnroservicio, idnumeroorden);

                clsParametro parametro = new clsParametro();
                parametro.Parametros = new Dictionary<string, string>();

                parametro.Parametros.Add("IdEmpresa", row["IdEmpresa"].ToString());
                parametro.Parametros.Add("IdUUNN", row["IdUUNN"].ToString());
                parametro.Parametros.Add("IdCCSS", row["IdCentroServicio"].ToString());
                parametro.Parametros.Add("IdNroServicio", row["IdNroServicio"].ToString());
                parametro.Parametros.Add("IdTipoDocumento", row["IdTipoDocumento"].ToString());
                parametro.Parametros.Add("NroDocumento", row["NroDocumento"].ToString());
                parametro.Parametros.Add("Periodo", row["Periodo"].ToString());
                parametro.Parametros.Add("IdTipoDocumentoDoc", Convert.ToString(0));
                parametro.Parametros.Add("NroDocumentoDoc", "");
                parametro.Parametros.Add("FechaInicio", null);
                parametro.Parametros.Add("FechaFinal", null);

                var table = _FinanciamientoDAO.ObtenerDocumentoComprobantePago(parametro);

                documento = new EListaNotaCreditoDebitoControlGestor(table);

                #region BOTON IMPRIMIR

                ENotaCreditoDebitoControlGestor oFilaSeleccionada = documento.Elementos[0];

                Int16 _intidempresa = Convert.ToInt16(oFilaSeleccionada.IdEmpresaNCND);
                Int16 _intidtipodocumento = Convert.ToInt16(oFilaSeleccionada.IdTipoDocumentoNCND);
                String _strnrodocumento = oFilaSeleccionada.NroDocumento;
                Int16 _intidmovimientocomercial = oFilaSeleccionada.IdMovimientoComercial;

                SISAC.Entidad.Atencion.clsEmpresa _objempresa = clsFinanciamientoDAO.ObtenerRegistroEmpresa(Convert.ToInt16(_intidempresa));
                _eshomologada = Convert.ToInt16(_FinanciamientoDAO.ObtenerDatosComplemento(_intidempresa, 2626));
                string fileName = "";
                DateTime dtFechaLimite = new DateTime(2016, 12, 01);
                clsComprobantePagoNGC ocomprobante = new clsComprobantePagoNGC();
                if (_eshomologada == (Int16)1)
                {

                    if (oFilaSeleccionada.FechaRegistro < dtFechaLimite)
                    {
                        if (_intidtipodocumento == (Int16)TipoDocumentoComercial.BoletaVenta ||
                            _intidtipodocumento == (Int16)TipoDocumentoComercial.Factura)
                        {
                            ocomprobante = _FinanciamientoDAO.ObtenerInformacionDocumentoIngresoProvisionNGC(_intidempresa, _intidtipodocumento, _strnrodocumento);
                            fileName = GenerarDocumentoComprobantePago(ocomprobante, _objempresa);
                            //RptComprobantePago frmcomprobante = new RptComprobantePago(ocomprobante, _intidtipodocumento, _objempresa, (clsTransaccion)null, "1", (Int16)12);
                            //frmcomprobante.Show();
                        }
                    }
                    else
                    {
                        ocomprobante = _FinanciamientoDAO.ObtenerInformacionDocumentoIngresoProvisionNGC(_intidempresa, _intidtipodocumento, _strnrodocumento);
                        fileName = GenerarDocumentoComprobantePago(ocomprobante, _objempresa);
                        //RptComprobantePagoElectronico frmcomprobante = new RptComprobantePagoElectronico(ocomprobante, _intidtipodocumento, _objempresa, (clsTransaccion)null, "1", (Int16)12);
                        //frmcomprobante.Show();
                    }
                }
                else
                {
                    if (_intidtipodocumento == (Int16)TipoDocumentoComercial.BoletaVenta ||
                        _intidtipodocumento == (Int16)TipoDocumentoComercial.Factura)
                    {
                        ocomprobante = _FinanciamientoDAO.ObtenerInformacionDocumentoIngresoProvisionNGC(_intidempresa, _intidtipodocumento, _strnrodocumento);
                        fileName = GenerarDocumentoComprobantePago(ocomprobante, _objempresa);
                        //RptComprobantePago frmcomprobante = new RptComprobantePago(ocomprobante, _intidtipodocumento, _objempresa, (clsTransaccion)null, "1", (Int16)12);
                        //frmcomprobante.Show();
                    }
                }




                #endregion BOTON IMPRIMIR


                var result = new clsResultado();
                parametro.Parametros = new Dictionary<string, string>();
                parametro.Parametros.Add("nombre", ConfiguracionGlobal.RutaServidorDocumentoFinanciamiento.ToString());
                DataTable dtServidor = _FinanciamientoDAO.ObtenerConfiguracionGlobal(parametro);
                string rutaServidor = dtServidor.Rows[0]["ColumnaValor"].ToString();


                dynamic resultado = new ExpandoObject();
                resultado.RutaArchivo = string.Concat(rutaServidor, fileName);
                result.Datos = resultado;
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #region GenerarDocumento
        private string GenerarDocumentoComprobantePago(clsComprobantePagoNGC _objcomprobante, SISAC.Entidad.Atencion.clsEmpresa _objempresa)
        {

            clsParametro param = new clsParametro();
            param.Parametros = new Dictionary<string, string>();
            param.Parametros.Add("nombre", ConfiguracionGlobal.RutaAlmacenarComprobantePDFFinanciamiento.ToString());
            DataTable dt = _FinanciamientoDAO.ObtenerConfiguracionGlobal(param);
            string rutaArchivo = dt.Rows[0]["ColumnaValor"].ToString();

            param.Parametros = new Dictionary<string, string>();
            param.Parametros.Add("nombre", ConfiguracionGlobal.DireccionNuevaENSA.ToString());
            dt.Clear();
            dt = _FinanciamientoDAO.ObtenerConfiguracionGlobal(param);
            string direccionEnsa = dt.Rows[0]["ColumnaValor"].ToString();



            String tieneconvenio = (_objcomprobante.ConvenioComprobantePago == null) ? "0" : "1";
            String strDirecion = _objcomprobante.IdEmpresa != 2 ? " " : direccionEnsa == "" ? " " : direccionEnsa;
            String strdireccionimagenempresa = "";

            if (_objcomprobante.ImagenEmpresa != null)
            {
                String strdirecciontempie = rutaArchivo + "ImagenEmpresa.jpg";
                strdireccionimagenempresa = @"file://" + strdirecciontempie;
                System.IO.File.WriteAllBytes(strdirecciontempie, _objcomprobante.ImagenEmpresa);
            }

            string strRutaCodBar2 = clsUtilFinanciamiento.GenerarCodigoBarraPDF47(_objcomprobante.IdEmpresa, _objcomprobante.CodigoBarra);
            string fileName = clsUtilFinanciamiento.ComprobanteElectronico(_objcomprobante, _objempresa, strdireccionimagenempresa, strRutaCodBar2, rutaArchivo);



            return fileName;
        }



        #endregion


        #endregion OBTENER COMPROBANTE PAGO

        #region OBTENER TRANSACCION EXTRAJUDICIAL

        //RECIBE SOLAMENTE EL IDORDENCOBRO
        public clsResultado GenerarDocumentoTransaccionExtrajudicial(clsParametro entidad)
        {
            GeneradorPlantillaUI objGeneraPlantilla = new GeneradorPlantillaUI();
            clsListaCampoDato objListaCampoDato = null;

            try
            {
                string rutaDocumentoGenerado = "";
                var row = _FinanciamientoDAO.ObtenerParametrosTransaccionExtrajudicial(entidad);

                clsParametro parametro = new clsParametro();
                parametro.Parametros = new Dictionary<string, string>();

                Int16 intIdEmpresa = Convert.ToInt16(row["IdEmpresa"].ToString());
                Int16 intIdUUNN = Convert.ToInt16(row["IdUUNN"].ToString());
                string strNroConvenio = row["IdNroMovimientoComercial"].ToString();
                string NroServicio = row["IdNroServicio"].ToString();


                objListaCampoDato = _FinanciamientoDAO.ObtenerDatosPlantillaTransaccionExtrajudicial(intIdEmpresa, intIdUUNN, strNroConvenio);

                Byte[] plantilla = _FinanciamientoDAO.PlantillaObtenerDocumento2((Int16)TipoDocumentoComercial.TransaccionExtrajudicial, (Int16)Servicio.EnergiaPostpago, (Int16)TipoFlujo.AtencionCliente);

                if (plantilla == null)
                    throw new Exception("No se encontró plantilla disponible para este tipo de doumento");

                if (objListaCampoDato != null)
                {
                    foreach (clsCampoDato campoDato in objListaCampoDato.Elementos)
                    {
                        if (campoDato.Campo.Trim() == "Firma")
                        {
                            String[] datosFirma = new String[campoDato.Dato.Split('/').Length];
                            datosFirma = campoDato.Dato.Split('/');
                            if (datosFirma[0].ToString().Trim() == "FIRMA")
                            {
                                campoDato.Imagen = _FinanciamientoDAO.FirmaDocumentoObtener(Convert.ToInt16(datosFirma[1].ToString()));
                            }
                        }
                        else if (campoDato.Campo.Trim() == "FirmaA")
                        {
                            String[] datosFirma = new String[campoDato.Dato.Split('/').Length];
                            datosFirma = campoDato.Dato.Split('/');
                            if (datosFirma[0].ToString().Trim() == "FIRMAA")
                            {
                                campoDato.Imagen = _FinanciamientoDAO.FirmaDocumentoObtener(Convert.ToInt16(datosFirma[1].ToString()));
                            }
                        }
                        else if (campoDato.Campo.Trim() == "FirmaB")
                        {
                            String[] datosFirma = new String[campoDato.Dato.Split('/').Length];
                            datosFirma = campoDato.Dato.Split('/');
                            if (datosFirma[0].ToString().Trim() == "FIRMAB")
                            {
                                campoDato.Imagen = _FinanciamientoDAO.FirmaDocumentoObtener(Convert.ToInt16(datosFirma[1].ToString()));
                            }
                        }
                    }
                    /*llamar variable RutaAlmacenarComprobantePDFFinanciamiento */
                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<string, string>();
                    param.Parametros.Add("nombre", ConfiguracionGlobal.RutaAlmacenarComprobantePDFFinanciamiento.ToString());
                    DataTable dt = _FinanciamientoDAO.ObtenerConfiguracionGlobal(param);

                    string ruta = dt.Rows[0]["ColumnaValor"].ToString();


                    rutaDocumentoGenerado = objGeneraPlantilla.ImprimirDocumentoPlantillaDosFirmas(objListaCampoDato, false, plantilla, ruta, NroServicio, strNroConvenio);

                }
                else
                {
                    throw new Exception("No se pudo obtener la información para generar la Transacción Extrajudicial.");
                }

                rutaDocumentoGenerado = Path.GetFileName(rutaDocumentoGenerado);


                var result = new clsResultado();
                parametro.Parametros = new Dictionary<string, string>();
                parametro.Parametros.Add("nombre", ConfiguracionGlobal.RutaServidorDocumentoFinanciamiento.ToString());
                DataTable dtServidor = _FinanciamientoDAO.ObtenerConfiguracionGlobal(parametro);
                string rutaServidor = dtServidor.Rows[0]["ColumnaValor"].ToString();


                dynamic resultado = new ExpandoObject();
                resultado.RutaArchivo = string.Concat(rutaServidor, rutaDocumentoGenerado);
                result.Datos = resultado;
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }


        #endregion OBTENER TRANSACCION EXTRAJUDICIAL


        #region PAGO NIUBIZ
        /*
        public clsResultado ObtenerPagoResumen(clsParametro entidad)
        {
            try
            {
                var row = _FinanciamientoDAO.ObtenerIntento(entidad);
                var json = row["JsonPagoResumen"] ?? "{}";

                var result = new clsResultado();
                result.Datos = _Convert.ToObject<Dictionary<String, Object>>(json.ToString());

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }
        */
        public clsResultado CobranzaOrdenVisaNET(clsParametro entidad, clsTransaccion transaccion)
        {
            try
            {
                var row = _FinanciamientoDAO.CobranzaOrdenVisaNET(entidad, transaccion);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #endregion PAGO NIUBIZ

        #region NGC ESCRITORIO
        public clsListaIngresoProvision ObtenerListaPorOrdenCobro(clsParametro entidad)
        {
            clsListaOrdenCobroDocumento result = new clsListaOrdenCobroDocumento();
            clsListaIngresoProvision _objlistaingresoprovision = new clsListaIngresoProvision();
            try
            {
                var idNumOrden = Convert.ToInt32(entidad.Parametros["idordencobro"]);
                var idOrdenCobro = Convert.ToInt32(entidad.Parametros["idmoneda"]);
                //result = _FinanciamientoDAO.ObtenerListaPorOrdenCobro(4903137, 1);
                DateTime FechaPago = Convert.ToDateTime("23/03/2021 08:00:00");
                //_objlistaingresoprovision = result.ObtenerListaIngresoProvision(2020//ucperiodo.Ayo
                //                                                         , 11//ucperiodo.Mes
                //                                                         , 501//_objcobranza.Transaccion.UsuarioPuntoAtencion.IdCentroServicio
                //                                                         , 1//_objcobranza.Transaccion.UsuarioPuntoAtencion.IdTipoAtencion
                //                                                         , 24276//_objcobranza.Transaccion.UsuarioPuntoAtencion.IdPuntoAtencion
                //                                                         , 68478//Usuario.Id
                //                                                         , FechaPago);//usrUsuarioPuntoAtencion1.Caja.FechaPago);
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
            }
            return _objlistaingresoprovision;
        }

        #endregion NGC ESCRITORIO

        #endregion LESTRADA

        #region MCASTRO

        public async Task<clsResultado> ObtenerIntento(clsParametro entidad)
        {
            try
            {
                var row = await _FinanciamientoDAO.ObtenerIntento(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }
        #endregion

        #region rberrospi

        public async Task<clsResultado> ObtenerMetodosPago(clsParametro entidad)
        {
            try
            {
                var table = await _FinanciamientoDAO.ObtenerMetodosPago(entidad);

                var result = new clsResultado();
                result.Datos = table;
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerRutaComprobante(clsParametro entidad)
        {
            try
            {
                var result = new clsResultado();
                var param = new clsParametro() { Parametros = new Dictionary<String, String>() };
                param.Parametros["rutaComprobante"] = "http://www.africau.edu/images/default/sample.pdf";
                result.Datos = param;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerRutaTransaccion(clsParametro entidad)
        {
            try
            {
                var result = new clsResultado();
                var param = new clsParametro() { Parametros = new Dictionary<String, String>() };
                param.Parametros["rutaTransaccion"] = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";
                result.Datos = param;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        // apgos
        public async Task<Boolean> ValidarPagosMultiples(clsParametro entidad)
        {
            try
            {
                Boolean res = await _FinanciamientoDAO.ValidarPagosMultiples(entidad);
                return res;
            }
            catch (Exception ex)
            {
                _Log.GrabarLog(ex, entidad);
                return false;
            }
        }

        #endregion

        #endregion Public

        #region "CMM"
        public clsResultado ObtenerDetalleFinanciamiento(clsParametro param)
        {
            //clsParametro parametro;
            //Dictionary<String, String> par;
            try
            {
                var result = new clsResultado();
                if (!param.Parametros.ContainsKey("idNroServicio"))
                {
                    result.Mensaje = "Parametros no contiene suministro.";
                    result.IdError = 1;
                }
                else
                {

                    if (Convert.ToInt32(param.Parametros["idNroServicio"]).GetType().Equals(typeof(int)))
                    {

                        var table = _FinanciamientoDAO.ObtenerDetalleFinanciamiento(param);
                        result.Datos = table;

                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, param);
            }
        }

        public clsResultado ValidarToken(clsParametro entidad)
        {
            try
            {
                var row = _FinanciamientoDAO.ValidarToken(entidad);
                var result = new clsResultado();
                result.Datos = row.ToDictionary();
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ActualizarIntento(clsParametro entidad)
        {
            try
            {
                var row = _FinanciamientoDAO.ActualizarIntento(entidad);
                var result = new clsResultado();
                result.Datos = row.ToDictionary();
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado PagarDeuda(clsParametro entidad)
        {
            try
            {
                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<string, string>();
                param.Parametros.Add("MetodoPago", entidad.Parametros["MetodoPago"]);
                param.Parametros.Add("IdNumeroOrden", entidad.Parametros["NroPedido"]);


                _FinanciamientoDAO.ActualizarMetodoPagoOrdenVisa(param);

                var row = _FinanciamientoDAO.PagarDeuda(entidad);
                var result = new clsResultado();
                result.Datos = row.ToDictionary();
                return result;
            }
            catch (Exception ex)
            {
                clsParametro parametro = new clsParametro();
                parametro.Parametros = new Dictionary<string, string>();
                parametro.Parametros.Add("IdNumeroOrden", entidad.Parametros["NroPedido"].ToString());
                parametro.Parametros.Add("IdEstado", ((short)EstadoNGC.Fallado).ToString());
                parametro.Parametros.Add("MensajeVisaNet", "DISTRILUZ - ERROR");
                _FinanciamientoDAO.ActualizarEstadoOrdenVisa(parametro);
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado EnviarEmail(clsParametro entidad)
        {
            try
            {
                var row = _FinanciamientoDAO.EnviarEmail(entidad);
                var result = new clsResultado();
                result.Datos = row.ToDictionary();
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado FinanciamientoEnviarEmailError(clsParametro entidad)
        {
            try
            {
                var row = _FinanciamientoDAO.FinanciamientoEnviarEmailError(entidad);
                var result = new clsResultado();
                result.Datos = row.ToDictionary();
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ConsultarDeudaDetalle(clsParametro entidad)
        {
            try
            {
                var table = _FinanciamientoDAO.ConsultarDeudaDetalle(entidad);
                var result = new clsResultado();
                result.Datos = table;
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ActualizarMetodoPagoOrdenVisa(clsParametro entidad)
        {
            try
            {
                _FinanciamientoDAO.ActualizarMetodoPagoOrdenVisa(entidad);
                var result = new clsResultado();
                result.Datos = "OK";
                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }


        #endregion

        #region Metodos Privados ORdenCobro
        public clsResultado GenerarOrdenCobro(clsParametro param)
        {
            clsParametro parametro;
            parametro = new clsParametro();
            var result = new clsResultado();
            var ordenCobro = new enteOrdenDeCobro();
            var ordenCobroDocumentoMontoInicial = new enteOrdenCobroDocumento();
            var DatoSuministro = new enteDatoSuministro();
            decimal valortasainteres = 0;
            _IdNroServicio = Convert.ToInt32(param.Parametros["IdNroServicio"]);
            // _IdUsuario = Convert.ToInt32(param.Parametros["IdUsuario"]);
            _IdMoneda = Convert.ToInt16(param.Parametros["IdMoneda"]);
            string abreviaturaconvenio = "RefVirt";
            string usuariofinweb = "UserFinacWeb";



            try
            {

                _MontoInicial = Convert.ToDecimal(param.Parametros["MontoInicial"]);
                _MontoFinanciar = Convert.ToDecimal(param.Parametros["MontoFinanciar"]);
                _SaldoAFavorUsar = Convert.ToDecimal(param.Parametros["SaldoAFavorUsar"]);
                param.Parametros.Add("abreviaturaconvenio", abreviaturaconvenio);
                //Obteniendo Usuario Financiamiento WEB
                _IdUsuario = _FinanciamientoDAO.ObtenerUsuarioFinanciamientoWeb(usuariofinweb);

                if (_IdUsuario == 0)
                {
                    result.Mensaje = "No se llego a encontrar un usuario de tipo financiamiento.";
                    result.IdError = 1;
                    return result;
                }
                param.Parametros.Add("IdUsuario", _IdUsuario.ToString());

                //Obteniendo datos del suministro
                DatoSuministro = _FinanciamientoDAO.ObtenerDatoSuministro(_IdNroServicio);

                param.Parametros.Add("IdTipoIdentidad", DatoSuministro.IdTipoIdentidadPropietario.ToString());
                param.Parametros.Add("NroIdentidad", DatoSuministro.NroIdentidadPropietario.ToString());
                //Obteniendo tasa de interes al dia
                var ConvenioParametros = _FinanciamientoDAO.ObtenerParametrosConvenio(param);
                foreach (DataRow item in ConvenioParametros.Rows)
                {
                    valortasainteres = Convert.ToDecimal(item["valortasainteres"]);
                }
                param.Parametros.Add("ValorTasa", valortasainteres.ToString());

                #region Validaciones
                //// Validar que haya tasa de interes.
                if (valortasainteres == 0)
                {
                    result.Mensaje = "El Administrador debe ingresar Tasa de Interés.";
                    result.IdError = 1;
                    return result;
                }
                // Validar que el monto a financiar sea mayor o igual a cero.
                if (_MontoFinanciar < 0)
                {
                    result.Mensaje = "El monto a financiar no debe ser negativo.";
                    result.IdError = 1;
                    return result;
                }

                //Validnado el monto Inicial
                if (_MontoInicial < 0)
                {
                    result.Mensaje = "El monto inicial no debe ser negativo.";
                    result.IdError = 1;
                    return result;
                }

                //Valida que tenga Empresa
                if (Convert.ToInt16(param.Parametros["IdEmpresa"]) == 0)
                {
                    result.Mensaje = "No se esta enviando una empresa.";
                    result.IdError = 1;
                    return result;

                }

                //Validar que este activo
                if (Convert.ToInt16(param.Parametros["IdEstado"]) == Convert.ToInt16(EstadoGenerales.Retirado) && Convert.ToInt16(param.Parametros["NumeroCuotas"]) > 0)
                {
                    result.Mensaje = "No es posible realizar convenio sobre suministros Retirados.  Debe realizar el Convenio por Reapertura.";
                    result.IdError = 1;
                    return result;
                }
                //Validando el plazo es mayor al permitido.
                if (Convert.ToInt16(param.Parametros["NumeroCuotas"]) > Convert.ToInt16(param.Parametros["PlazoMaximo"]))
                {
                    result.Mensaje = "Plazo es mayor al permitido.";
                    result.IdError = 1;
                    return result;
                }
                //Validando parametro de Unidad de Negocio
                if (Convert.ToInt16(param.Parametros["IdUUNN"]) == 0)
                {
                    result.Mensaje = "No Contiene Unidad de Negocio.";
                    result.IdError = 1;
                    return result;
                }
                //Validando centro de servicio
                if (Convert.ToInt16(param.Parametros["IdCentroServicio"]) == 0)
                {
                    result.Mensaje = "No Contiene Centro de Servicio.";
                    result.IdError = 1;
                    return result;
                }
                //Validando punto de atencion  /ejemplo 24261 OrdendeCobro generado
                //if (Convert.ToInt16(param.Parametros["IdPuntoAtencion"]) == 0)
                //{
                //    result.Mensaje = "No contiene punto de atención.";
                //    result.IdError = 1;
                //    return result;
                //}
                //Validando Tipo de atención  Recaudacion en linea RecLinea // VisaNet
                //if (Convert.ToInt16(param.Parametros["IdTipoAtencion"]) == 0)
                //{
                //    result.Mensaje = "No contiene tipo de atención.";
                //    result.IdError = 1;
                //    return result;
                //}
                //Validando Convenio Tipo  1 ..tmp....Facilidades de PAgo pero seria ara Recaudacion Virtual
                if (Convert.ToInt16(param.Parametros["IdConvenioTipo"]) == 0)
                {
                    result.Mensaje = "No contiene convenio tipo.";
                    result.IdError = 1;
                    return result;
                }

                //Validando si existe alguna restricción

                var restriccion = _FinanciamientoDAO.ConsultaExisteRestriccion(_IdNroServicio);
                if (restriccion != null)
                {
                    result.Mensaje = "El suministro tiene " + restriccion.ItemArray[3];
                    result.IdError = 1;
                    return result;
                }
                //Validando si contiene IdUsuario

                if (_IdUsuario == 0)
                {
                    result.Mensaje = "No contiene un IdUsuario .";
                    result.IdError = 1;
                    return result;
                }
                ////Flujo para generar una orden de Cobro /////////
                ///////////////////////////////////////////////////
                //IdEmpresa y IdUnidaddeNegocio
                //Estableciendo PeriodoPago
                int PeriodoPago = _FinanciamientoDAO.ObtenerPeriodoVigente(Convert.ToInt32(param.Parametros["IdEmpresa"]), Convert.ToInt32(param.Parametros["IdUUNN"]));
                if (PeriodoPago == 0)
                {
                    result.Mensaje = "No se ha logrado obtener Periodo Comercial Vigente .";
                    result.IdError = 1;
                    return result;
                }

                param.Parametros.Add("PeriodoPago", PeriodoPago.ToString());

                #endregion Validaciones

                //GenerandoORdenCobro IdUsuario, IdNroservicio,MontoInicial, SaldoAFavor, IdMoneda
                //Preparando Cabecera de la Orden de Cobro
                ordenCobro = PrepararOrdencobro(_IdUsuario, _IdNroServicio, _MontoInicial, _SaldoAFavorUsar, _IdMoneda);

                //Preparando documento para cuota inicial.
                ordenCobroDocumentoMontoInicial = PreparandoOrdenCobroDocumentoMontoInicial(param);

                if (ordenCobroDocumentoMontoInicial != null)
                    ordenCobro.ListaOrdenCobroDocumento.Elementos.Add(ordenCobroDocumentoMontoInicial);

                //Preparando documento para intereses
                enteOrdenCobroDocumento ordenCobroDocumentoIntereses = PreparaOrdenCobroDocumentoIntereses(param);

                if (ordenCobroDocumentoIntereses != null)
                    ordenCobro.ListaOrdenCobroDocumento.Elementos.Add(ordenCobroDocumentoIntereses);

                //Guardar y generar la orden de cobro
                string NroOrdenCobro = string.Empty;
                string ResultadoOperacion = string.Empty;
                string IdRegistroConvenio = string.Empty;

                NroOrdenCobro = _FinanciamientoDAO.GenerarOrdencobroConvenioGuardar(ordenCobro, _IdNroServicio);

                //Guardar un Registro de convenio

                IdRegistroConvenio = GestorConvenioTemporal(NroOrdenCobro, ordenCobro, param);

                //Dictionary<string,string> cobro  =new Dictionary<string, string>();

                //cobro.Add("NroOrdenCobro", NroOrdenCobro.ToString());

                result.Datos = NroOrdenCobro;


                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, param);
            }

        }
        private enteOrdenDeCobro PrepararOrdencobro(int idusuario, int idnroservicio, decimal montoinicial, decimal saldoafavorusar, short idmoneda)
        {
            enteOrdenDeCobro OrdenCobro = new enteOrdenDeCobro();

            OrdenCobro.IdUsuarioAutoriza = idusuario;
            OrdenCobro.FechaAutoriza = DateTime.Now;
            OrdenCobro.IdMovimientoComercial = enumMovimientoComercial.Convenios;
            OrdenCobro.IdNroMovimientoComercial = string.Empty;
            OrdenCobro.IdNroServicio = idnroservicio;
            OrdenCobro.Importe = montoinicial + saldoafavorusar;
            OrdenCobro.IdMoneda = Convert.ToInt16(idmoneda);
            OrdenCobro.IdEstado = EstadoGenerales.Activo;

            return OrdenCobro;
        }
        private enteOrdenCobroDocumento PreparandoOrdenCobroDocumentoMontoInicial(clsParametro param)
        {
            enteOrdenCobroDocumento ordenCobroDocumento = null;
            enteOrdenCobroDocumentoDetalle ordenCobroDetalle = new enteOrdenCobroDocumentoDetalle();


            if (_MontoInicial + _SaldoAFavorUsar > 0)
            {
                ordenCobroDocumento = new enteOrdenCobroDocumento();

                // Preparar el documento.
                ordenCobroDocumento.Descripcion = "Cuota inicial para el convenio de facilidades de pago.";
                ordenCobroDocumento.IdEmpresa = Convert.ToInt16(param.Parametros["IdEmpresa"]);
                ordenCobroDocumento.IdTipoDocumento = TipoDocumentoComercial.CuotaInicialConvenio;
                ordenCobroDocumento.Importe = _MontoInicial;// + _SaldoAFavorUsar  //No se concidera el saldo a favor;
                ordenCobroDocumento.IdMoneda = _IdMoneda; //Convert.ToInt16(param.Parametros["IdMoneda"]);
                ordenCobroDocumento.EsCobrable = 1;
                ordenCobroDocumento.EsDocumentoFavor = 0;

                // Preparar el detalle del Documento.
                ordenCobroDetalle.IdConcepto = ConceptosFacturar.CuotaInicialConvenio;
                ordenCobroDetalle.IdTipoDocumentoSustento = TipoDocumentoComercial.CuotaInicialConvenio;
                ordenCobroDetalle.Importe = ordenCobroDocumento.Importe;
                ordenCobroDetalle.PrecioUnitario = ordenCobroDocumento.Importe;
                ordenCobroDetalle.Cantidad = 1;
                ordenCobroDetalle.IndicadorIGV = false;

                //Agregar el detalle al documento
                if (ordenCobroDocumento.ListaDetalleDocumento == null)
                    ordenCobroDocumento.ListaDetalleDocumento = new enteListaOrdenCobroDocumentoDetalle();

                ordenCobroDocumento.ListaDetalleDocumento.Elementos.Add(ordenCobroDetalle);

            }

            return ordenCobroDocumento;
        }
        private enteOrdenCobroDocumento PreparaOrdenCobroDocumentoIntereses(clsParametro param)
        {
            int indice = -1;
            decimal totalImpuesto = 0;
            bool DescontarIntereses = false;
            int IdNroServicio = Convert.ToInt32(param.Parametros["IdNroServicio"]);
            int AfectoIGV = 1;
            var DatoSuministro = (enteDatoSuministro)null;


            enteOrdenCobroDocumento ordenCobroDocumento = null;
            enteListaNroServicioIntereses listaIntereses = new enteListaNroServicioIntereses();

            var obtenerListarIntereses = new enteListaNroServicioIntereses();
            //Obteniendo los Intereses
            obtenerListarIntereses = _FinanciamientoDAO.ObtenerIntereses(IdNroServicio);

            //Validando interes a la fecha
            var InteresAFecha = Convert.ToDecimal(_FinanciamientoDAO.ObtenerInteresFechaActual(param));

            //insertando intereses a la fecha

            enteNroServicioIntereses intereses = new enteNroServicioIntereses();

            intereses.IdConcepto = 4;
            intereses.NombreConcepto = "IGV";
            intereses.Importe = InteresAFecha;
            intereses.EsTributable = false;
            intereses.TasaImpuesto = 0;

            obtenerListarIntereses.Elementos.Add(intereses);


            /////




            if (!DescontarIntereses && Convert.ToDecimal(param.Parametros["InteresAFecha"]) > 0)
            {
                ordenCobroDocumento = new enteOrdenCobroDocumento();


                foreach (enteNroServicioIntereses ointeres in obtenerListarIntereses.Elementos)
                {
                    if (ointeres.IdConcepto == (Int16)ConceptosFacturar.IGV) continue;

                    indice = listaIntereses.Elementos.FindIndex(delegate (enteNroServicioIntereses e)
                    { return e.IdConcepto == ointeres.IdConcepto; });

                    // No existe, se adiciona el concepto de interes.
                    if (indice < 0)
                        listaIntereses.Elementos.Add(ointeres);
                    else
                        listaIntereses.Elementos[indice].Importe += ointeres.Importe;

                    // Si es Tributable se calcula el impuesto
                    if (ointeres.EsTributable)
                        totalImpuesto += Math.Round((ointeres.Importe * ointeres.TasaImpuesto / Convert.ToDecimal(100)), 2);
                }


                // Agregar el concepto I.G.V.
                if (AfectoIGV == 1 && totalImpuesto > 0)
                    listaIntereses.Elementos.Add(new enteNroServicioIntereses(ConceptosFacturar.IGV, totalImpuesto));

                // -----------------------------------------------
                // Preparar el documento.
                // -----------------------------------------------

                //Obteniendo los datos
                DatoSuministro = _FinanciamientoDAO.ObtenerDatoSuministro(IdNroServicio);

                ordenCobroDocumento.Descripcion = "Facturación de intereses por deuda para el convenio de facilidades de pago Nº.";
                ordenCobroDocumento.IdEmpresa = Convert.ToInt16(param.Parametros["IdEmpresa"]);
                ordenCobroDocumento.IdTipoDocumento = DatoSuministro.IdTipoIdentidad == (Int16)TipoDocumentoIdentidad.RUC
                    ? TipoDocumentoComercial.Factura : TipoDocumentoComercial.BoletaVenta;
                ordenCobroDocumento.Importe = InteresAFecha;//0.9
                ordenCobroDocumento.IdMoneda = Convert.ToInt16(param.Parametros["IdMoneda"]);//1
                ordenCobroDocumento.EsCobrable = 0;
                ordenCobroDocumento.EsDocumentoFavor = 0;

                // Agregar el detalle al documento.
                if (ordenCobroDocumento.ListaDetalleDocumento == null)
                    ordenCobroDocumento.ListaDetalleDocumento = new enteListaOrdenCobroDocumentoDetalle();

                enteOrdenCobroDocumentoDetalle ordenCobroDetalle;

                foreach (enteNroServicioIntereses ointeres in listaIntereses.Elementos)
                {
                    // Agregar al documento el detalle.
                    ordenCobroDetalle = new enteOrdenCobroDocumentoDetalle();
                    ordenCobroDetalle.IdConcepto = (ConceptosFacturar)ointeres.IdConcepto;
                    ordenCobroDetalle.IdTipoDocumentoSustento = (ordenCobroDetalle.IdConcepto == ConceptosFacturar.IGV ? ordenCobroDocumento.IdTipoDocumento : TipoDocumentoComercial.InteresDeudaEnergia);
                    ordenCobroDetalle.Importe = ointeres.Importe;
                    ordenCobroDetalle.PrecioUnitario = ointeres.Importe;
                    ordenCobroDetalle.Cantidad = 1;
                    ordenCobroDetalle.IndicadorIGV = ointeres.EsTributable;

                    ordenCobroDocumento.ListaDetalleDocumento.Elementos.Add(ordenCobroDetalle);
                }

            }

            return ordenCobroDocumento;

        }
        private string GestorConvenioTemporal(string nroordencobro, enteOrdenDeCobro ordencobro, clsParametro param)
        {
            //DescontarIntereses = !this.chkintereses.Checked;
            //DescontarDeudaPorFacturar = !this.chksaldodeuda.Checked;
            //DescontarDeudaFacturada = !this.chkdeudafacturada.Checked;
            bool DescontarDeudaPorFacturar = false;
            bool DescontarDeudaFacturada = false;

            enteNroServicioDeuda DatosSuministro = new enteNroServicioDeuda();



            string IdRegistroConvenio = string.Empty;
            try
            {
                //Obteniendo los datos para llenar la informacion de los temporales
                DatosSuministro = ObtenerDeudaParaFinanciar(param);

                _FinanciamientoDAO.AnularConvenioTemporalAnterior(ordencobro.IdNroServicio);

                IdRegistroConvenio = _FinanciamientoDAO.GuardarCabeceraConvenioTemporal(nroordencobro, ordencobro, param);

                if (!DescontarDeudaPorFacturar)
                    _FinanciamientoDAO.GuardarConveniosFinanciado(IdRegistroConvenio, DatosSuministro);

                if (!DescontarDeudaFacturada)
                    _FinanciamientoDAO.GuardarDocumentosFinanciado(IdRegistroConvenio, DatosSuministro);

                //if (MontoADescontar > 0)
                //  _FinanciamientoDAO.GuardarNotaAbono(idRegistroConvenio, convenioResumen);

                return IdRegistroConvenio;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        private enteNroServicioDeuda ObtenerDeudaParaFinanciar(clsParametro param)
        {
            enteNroServicioDeuda Datos = new enteNroServicioDeuda();
            int idnroservicio = Convert.ToInt32(param.Parametros["IdNroServicio"]);

            try
            {
                Datos = _FinanciamientoDAO.ObtenerDatoSuministro(param);
                Datos.ListaDeudaFacturada = _FinanciamientoDAO.ObtenerDeudaFacturada(idnroservicio);
                Datos.ListaDeudaPorFacturar = _FinanciamientoDAO.ObtenerDeudaPorFacturar(idnroservicio);
                Datos.ListaDocumentosAFavor = _FinanciamientoDAO.ObtenerDocumentosAFavor(idnroservicio);
                Datos.ListaIntereses = _FinanciamientoDAO.ObtenerIntereses(idnroservicio);


                return Datos;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public clsFinanciamientoOrdenVisaNet ObtenerOrdenVisaNetPorId(clsParametro param)
        {
            clsFinanciamientoOrdenVisaNet _clsResultado = new clsFinanciamientoOrdenVisaNet();

            _clsResultado = _FinanciamientoDAO.ObtenerOrdenVisaNetPorId(param);
            return _clsResultado;
        }

        #endregion Metodos privados
    }
}
