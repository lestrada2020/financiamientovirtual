﻿using SISAC.Data.Atencion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Threading;

namespace SISAC.Negocio.Atencion
{
    public sealed class clsNegocioCobranza
    {
        #region Field

        private static readonly clsCobranzaDAO _CobranzaDAO = clsCobranzaDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly Lazy<clsNegocioCobranza> _Instancia = new Lazy<clsNegocioCobranza>(() => new clsNegocioCobranza(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioCobranza Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioCobranza()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado ValidarToken(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.ValidarToken(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ConsultarDeuda(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.ConsultarDeuda(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ConsultarDeudaDetalle(clsParametro entidad)
        {
            try
            {
                var table = _CobranzaDAO.ConsultarDeudaDetalle(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado PagarDeuda(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.PagarDeuda(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado GrabarIntento(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.GrabarIntento(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado GrabarIntentoDetalle(clsParametro entidad)  
        {
            try
            {
                var row = _CobranzaDAO.GrabarIntentoDetalle(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ActualizarIntento(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.ActualizarIntento(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerPagoResumen(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.ObtenerIntento(entidad);
                var json = row["JsonPagoResumen"] ?? "{}";

                var result = new clsResultado();
                result.Datos = _Convert.ToObject<Dictionary<String, Object>>(json.ToString());

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado EnviarEmail(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.EnviarEmail(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado CobranzaOrdenVisaNET(clsParametro entidad)
        {
            try
            {
                var row = _CobranzaDAO.CobranzaOrdenVisaNET(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #endregion Public
    }
}