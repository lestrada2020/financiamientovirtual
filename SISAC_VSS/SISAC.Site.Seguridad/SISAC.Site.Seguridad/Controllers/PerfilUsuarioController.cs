﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Seguridad;
using SISAC.Negocio.Seguridad;
using System.Web.Http;

namespace SISAC.Site.Seguridad.Controllers
{
    public class PerfilUsuarioController : ApiController, IPerfilUsuario
    {
        #region Field

        private static readonly clsNegocioPerfilUsuario _PerfilUsuario = clsNegocioPerfilUsuario.Instancia;

        #endregion Field

        #region Public

        public IHttpActionResult ValidarToken(clsParametro param)
        {
            var result = _PerfilUsuario.ValidarToken(param);

            return Ok(result);
        }

        public IHttpActionResult ListAppConfig(clsParametro param)
        {
            var result = _PerfilUsuario.ValidarToken(param);

            if (result.IdError == 0) result = _PerfilUsuario.ListAppConfig(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosUsuario(clsParametro param)
        {
            var result = _PerfilUsuario.ObtenerDatosUsuario(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerModulosAplicacionPorUsuario(clsParametro param)
        {
            var result = _PerfilUsuario.ObtenerModulosAplicacionPorUsuario(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerRolesAplicacionPorUsuario(clsParametro param)
        {
            var result = _PerfilUsuario.ObtenerRolesAplicacionPorUsuario(param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerOpcionesPorModuloAplicacionUsuario(clsParametro param)
        {
            var result = _PerfilUsuario.ObtenerOpcionesPorModuloAplicacionUsuario(param);

            return Ok(result);
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public IHttpActionResult ObtenerRestriccionVistaUsuario(clsParametro param)
        {
            var result = _PerfilUsuario.ObtenerRestriccionVistaUsuario(param);

            return Ok(result);
        }

        #endregion

        #endregion Public
    }
}