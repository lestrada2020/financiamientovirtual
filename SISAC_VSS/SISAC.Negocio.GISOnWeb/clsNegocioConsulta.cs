﻿using SISAC.Data.GISOnWeb;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading;

namespace SISAC.Negocio.GISOnWeb
{
    public sealed class clsNegocioConsulta
    {
        #region Field

        private static readonly clsConsultaDAO _ConsultalDAO = clsConsultaDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;

        //private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly Lazy<clsNegocioConsulta> _Instancia = new Lazy<clsNegocioConsulta>(() => new clsNegocioConsulta(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioConsulta Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioConsulta()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado CapaListarGeografia(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.CapaListarGeografia(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado CapaListarElectrica(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.CapaListarElectrica(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado Escalaslistar(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.Escalaslistar(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado CapaListarEscalas(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.CapaListarEscalas(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado EntidadListaBuscar(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.EntidadListaBuscar(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado EntidadListaPropiedad(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.EntidadListaPropiedad(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado UsuarioValida(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.UsuarioValida(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado UsuarioPerfil(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.UsuarioPerfil(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado Poligonos(clsParametro entidad)
        {
            try
            {
                var datos = new List<Object>();
                var styles = _ConsultalDAO.EntidadListPolygons(entidad);

                #region Format

                for (Int32 i = 0; i < styles.Rows.Count; i++)
                {
                    var row = styles.Rows[i];

                    var style = new Dictionary<String, Object>();
                    style.Add("stroke", row["Stroke"]);
                    style.Add("stroke-width", 2);
                    style.Add("stroke-opacity", 1);
                    style.Add("fill", row["Fill"]);
                    style.Add("fill-opacity", 0.5);
                    style.Add("Name", row["Nombre"]);
                    style.Add("IdEntidad", row["IdEntidad"]);

                    dynamic e = new ExpandoObject();
                    e.type = "Feature";
                    e.properties = new { style = style };
                    e.geometry = new
                    {
                        type = "Polygon",
                        coordinates = new Object[] { row["GeoCampo"].ToCoordinates() }
                    };

                    datos.Add(e);
                }

                #endregion Format

                var result = new clsResultado();
                result.Datos = datos;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado Lineas(clsParametro entidad)
        {
            try
            {
                var styles = _ConsultalDAO.EntidadListLines(entidad);
                var datos = new Object[styles.Rows.Count];

                #region Format

                for (Int32 i = 0; i < datos.Length; i++)
                {
                    var row = styles.Rows[i];

                    var style = new Dictionary<String, Object>();
                    //style.Add("Tipo", "Aereo");
                    style.Add("Tipo", row["Tipo"]);
                    style.Add("Id", row["Id"]);
                    style.Add("Name", row["Nombre"]);
                    style.Add("Stroke", row["Stroke"]);
                    style.Add("StrokeSel", row["StrokeSel"]);
                    style.Add("IdEntidad", row["IdEntidad"]);

                    dynamic e = new ExpandoObject();
                    e.type = "LineString";
                    e.properties = style;
                    e.coordinates = new Object[] {
                        new String[][] {
                            new String[] {row["X1"].ToString(), row["Y1"].ToString() },
                            new String[] {row["X2"].ToString(), row["Y2"].ToString() }
                        }
                    };

                    datos[i] = e;
                }

                #endregion Format

                var result = new clsResultado();
                result.Datos = datos;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado Puntos(clsParametro entidad)
        {
            try
            {
                var styles = _ConsultalDAO.EntidadListPoints(entidad);
                var datos = new Object[styles.Rows.Count];

                #region Format

                for (Int32 i = 0; i < datos.Length; i++)
                {
                    var row = styles.Rows[i];

                    var style = new Dictionary<String, Object>();
                    style.Add("Id", row["Id"]);
                    style.Add("Etiqueta", row["Nombre"]);
                    style.Add("Estilo", row["Imagen"]);
                    style.Add("IdEntidad", row["IdEntidad"]);

                    dynamic e = new ExpandoObject();
                    e.type = "Feature";
                    e.properties = style;
                    e.geometry = new
                    {
                        type = "Point",
                        coordinates = new String[] { row["X"].ToString(), row["Y"].ToString() }
                    };

                    datos[i] = e;
                }

                #endregion Format

                var result = new clsResultado();
                result.Datos = datos;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado EntidadBuscar(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.EntidadBuscar(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado EntidadListar(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.EntidadListar(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado EliminarCache(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.EliminarCache(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        public clsResultado EntidadListarLeyenda(clsParametro entidad)
        {
            try
            {
                var table = _ConsultalDAO.EntidadListarLeyenda(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                //return _Log.GrabarLog(ex, entidad);
                return new clsResultado() { IdError = 1, Mensaje = ex.Message };
            }
        }

        #endregion Public
    }
}