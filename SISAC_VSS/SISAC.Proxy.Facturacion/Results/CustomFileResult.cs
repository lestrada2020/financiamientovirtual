﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Facturacion.Results
{
    public class CustomFileResult : IHttpActionResult
    {
        private readonly string _contentType;
        private readonly Stream _stream;

        public CustomFileResult(Stream stream, string contentType)
        {
            _stream = stream;
            _contentType = contentType;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(_stream)
                };

                response.Content.Headers.ContentType = new MediaTypeHeaderValue(_contentType);
                response.Content.Headers.ContentLength = _stream.Length;

                return response;
            }, cancellationToken);
        }
    }
}