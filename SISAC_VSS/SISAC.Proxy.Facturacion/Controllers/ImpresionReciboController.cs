﻿using SISAC.Entidad.Maestro;
using SISAC.Proxy.Facturacion.Results;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Proxy.Facturacion.Controllers
{
    public class ImpresionReciboController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteFacturacion";
                String controller = "ImpresionRecibo";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        private async Task<T> SendDataAsync<T>(string action, object param)
        {
            try
            {
                string appKeySite = "SiteFacturacion";
                string controller = "ImpresionRecibo";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite))
                    throw new Exception(string.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = string.Format("{0}/Api/{1}/{2}", site, controller, action);

                return await _Service.SendDataAsync<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        private async Task<Stream> SendDataReturnStreamAsync(string action, object param,
            double timeoutInSeconds = 100)
        {
            try
            {
                string appKeySite = "SiteFacturacion";
                string controller = "ImpresionRecibo";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite))
                    throw new Exception(string.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = string.Format("{0}/Api/{1}/{2}", site, controller, action);

                return await _Service.SendDataReturnStreamAsync(url, param, "POST", timeoutInSeconds);
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #endregion Private

        #region Public

        public IHttpActionResult ObtenerListaSolicitudImpresionRecibos(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerListaSolicitudImpresionRecibos", param);

            return Ok(result);
        }

        public IHttpActionResult AyudaLista(clsParametro param)
        {
            var result = SendData<clsResultado>("AyudaLista", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerSolicitudImpresionRecibo(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerSolicitudImpresionRecibo", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDetalleSolicitudImpresionRecibo(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDetalleSolicitudImpresionRecibo", param);

            return Ok(result);
        }

        public IHttpActionResult ConsultarSuministrosNotificar(clsParametro param)
        {
            var result = SendData<clsResultado>("ConsultarSuministrosNotificar", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ConsultarMonitoreoTareasLista(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ConsultarMonitoreoTareasLista", param);

            return Ok(result);
        }

        public IHttpActionResult ConsultarLogProceso(clsParametro param)
        {
            var result = SendData<clsResultado>("ConsultarLogProceso", param);

            return Ok(result);
        }//

        public IHttpActionResult ConsultarLogTareaEjecucion(clsParametro param)
        {
            var result = SendData<clsResultado>("ConsultarLogTareaEjecucion", param);

            return Ok(result);
        }//

        public IHttpActionResult ConsultarLogInconsistencias(clsParametro param)
        {
            var result = SendData<clsResultado>("ConsultarLogInconsistencias", param);

            return Ok(result);
        }//

        public IHttpActionResult ActualizarEstado(clsParametro param)
        {
            var result = SendData<clsResultado>("ActualizarEstado", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerAlcancePersonalProveedorActividad(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerAlcancePersonalProveedorActividad", param);

            return Ok(result);
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public async Task<IHttpActionResult> ObtenerListaSolicitudImpresionRecibosPaginada(
            clsParametro param)
        {
            var result =
                await SendDataAsync<clsResultado>("ObtenerListaSolicitudImpresionRecibosPaginada", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ProcesarArchivoImpresion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ProcesarArchivoImpresion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> NotificarClientes(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("NotificarClientes", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ValidarNotificarClientes(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ValidarNotificarClientes", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerConfiguracion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ObtenerConfiguracion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> GuardarConfiguracion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("GuardarConfiguracion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerListaAplicacionNotificacion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ObtenerListaAplicacionNotificacion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerListaProcesoNotificacion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ObtenerListaProcesoNotificacion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerPdfSolicitudImpresion(clsParametro param)
        {
            var result = await SendDataReturnStreamAsync("ObtenerPdfSolicitudImpresion", param, 180);

            if (result != null)
            {
                return new CustomFileResult(result, "application/pdf");
                //return null;
            }
            else
            {
                return null;
            }
        }

        public async Task<IHttpActionResult> ProcesarDescargaDatosPdfSolicitudImpresion(
            clsParametro param)
        {
            var result =
                await SendDataAsync<clsResultado>("ProcesarDescargaDatosPdfSolicitudImpresion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> GenerarArchivoPdfSolicitudImpresion(clsParametro param)
        {
            var result =
                await SendDataAsync<clsResultado>("GenerarArchivoPdfSolicitudImpresion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> DescargarArchivoPdfSolicitudImpresion(clsParametro param)
        {
            var result =
                await SendDataReturnStreamAsync("DescargarArchivoPdfSolicitudImpresion", param, 180);

            if (result != null)
            {
                return new CustomFileResult(result, "application/pdf");
                //return null;
            }
            else
            {
                return null;
            }
        }

        public async Task<IHttpActionResult> CopiarPdfSolicitudImpresion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("CopiarPdfSolicitudImpresion", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> DescargarArchivoPdfEncriptadoSolicitudImpresion(
            clsParametro param)
        {
            var result = await SendDataReturnStreamAsync("DescargarArchivoPdfEncriptadoSolicitudImpresion",
                param, 180);

            if (result != null)
            {
                return new CustomFileResult(result, "application/pdf");
                //return null;
            }
            else
            {
                return null;
            }
        }

        public async Task<IHttpActionResult> ObtenerDatosEjecucionTareaPorId(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ObtenerDatosEjecucionTareaPorId", param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ReenviarCorreoProcesamientoArchivoImpresion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ReenviarCorreoProcesamientoArchivoImpresion", param);

            return Ok(result);
        }

        //CanviaDev_2020-05-27
        public async Task<IHttpActionResult> GenerarArchivosPDFRecibos(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("GenerarArchivosPDFRecibos", param);

            return Ok(result);
        }
        //CanviaDev_2020-05-27:Fin

        //CanviaDev_2020-06-01
        public async Task<IHttpActionResult> ValidarSuministrosImportarNotificacion(clsParametro param)
        {
            var result = await SendDataAsync<clsResultado>("ValidarSuministrosImportarNotificacion", param);

            return Ok(result);
        }
        //CanviaDev_2020-06-01:Fin

        #endregion

        #endregion Public
    }
}