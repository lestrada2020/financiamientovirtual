﻿using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace SISAC.Proxy.Facturacion.Controllers
{
    public class ReciboController : ApiController
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteFacturacion";
                String controller = "Recibo";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public IHttpActionResult DescargarRecibo(clsParametro param)
        {
            var result = SendData<clsResultado>("DescargarRecibo", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerJSON(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerJSON", param);

            return Ok(result);
        }

        #endregion Public
    }
}