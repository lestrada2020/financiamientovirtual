﻿using SISAC.Data.Maestro;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;

namespace SISAC.Negocio.Maestro
{
    public sealed class clsNegocioException
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsExceptionDAO _ExceptionDAO = clsExceptionDAO.Instancia;
        private static readonly clsNegocioException _Instancia = new clsNegocioException();

        #endregion Field

        #region Property

        public static clsNegocioException Instancia
        {
            get { return _Instancia; }
        }

        #endregion Property

        #region Constructors

        private clsNegocioException()
        {
        }

        #endregion Constructors

        #region Private

        private clsException GetEntidad(String mensaje, Object dataIN, Object dataOUT, Int16 idAppBD)
        {
            try
            {
                String[] trace = new String[3] { "", "", "" };

                _Util.GetTraceCall(ref trace[0], ref trace[1], ref trace[2], false);

                var entidad = new clsException();
                entidad.Mensaje = mensaje;
                entidad.Metodo = trace[0];
                entidad.Servicio = trace[1];
                entidad.Host = trace[2];
                entidad.DataIN = dataIN == null ? null : _Convert.ToJson(dataIN);
                entidad.DataOUT = dataOUT == null ? null : _Convert.ToJson(dataOUT);
                entidad.IdAppBD = idAppBD;

                return entidad;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveEvent(String error, String errorOriginal)
        {
            try
            {
                _Util.SaveEvent(String.Format("No se grabo el mensaje: [{1}], por el siguiente motivo: [{0}]", error, errorOriginal));
            }
            catch (Exception ex)
            {
                _Util.SaveEvent(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public String[] Registrar(String mensaje, Object dataIN = null, Object dataOUT = null, Int16 idAppBD = 0)
        {
            String[] result = new String[2] { "1", mensaje ?? "" };

            try
            {
                if (String.IsNullOrEmpty(mensaje)) mensaje = "";
                else if (mensaje.StartsWith("<<<"))
                {
                    mensaje = mensaje.Substring(3);
                    if (mensaje.EndsWith(">>>")) mensaje = mensaje.Substring(0, mensaje.Length - 3);
                }
                else
                {
                    var entidad = GetEntidad(mensaje, dataIN, dataOUT, idAppBD);

                    //Las lineas de abajo descomentar cuando estes grabando en la BD
                    //entidad.IdException = _ExceptionDAO.Registrar(entidad);
                    //result[0] = entidad.IdException.ToString();
                    //mensaje = _Util.MessageError(entidad.IdException);
                }

                result[1] = mensaje;
            }
            catch (Exception ex)
            {
                SaveEvent(ex.Message, mensaje);
            }

            return result;
        }

        public void Grabar(String mensaje, Object dataIN = null, Object dataOUT = null, Int16 idAppBD = 0, Boolean showError = true)
        {
            try
            {
                String[] data = Registrar(mensaje, dataIN, dataOUT, idAppBD);

                if (showError) throw new Exception(data[1]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Grabar(Exception exception, Object dataIN = null, Object dataOUT = null, Int16 idAppBD = 0, Boolean showError = true)
        {
            try
            {
                String mensaje = _Util.ErrorFormat(exception);

                Grabar(mensaje, dataIN, dataOUT, idAppBD, showError);
            }
            catch (Exception ex)
            {
                SaveEvent(ex.Message, exception.Message);

                throw exception;
            }
        }

        public T Grabar<T>(String mensaje, Object dataIN = null, Object dataOUT = null, Int16 idAppBD = 0)
        {
            try
            {
                String[] data = Registrar(mensaje, dataIN, dataOUT, idAppBD);

                T result = _Util.SetError<T>(data[1], Convert.ToInt32(data[0]));

                return result;
            }
            catch (Exception ex)
            {
                SaveEvent(ex.Message, mensaje);
                return _Util.SetError<T>(mensaje);
            }
        }

        public T Grabar<T>(Exception exception, Object dataIN = null, Object dataOUT = null, Int16 idAppBD = 0)
        {
            try
            {
                String message = exception.Message;

                if (!message.StartsWith("<<<")) message = _Util.ErrorFormat(exception);

                T result = Grabar<T>(message, dataIN, dataOUT, idAppBD);

                return result;
            }
            catch (Exception ex)
            {
                SaveEvent(ex.Message, exception.Message);
                return _Util.SetError<T>(exception.Message);
            }
        }

        public clsResultado GrabarLog(String mensaje, Object dataIN = null, Object dataOUT = null, Int16 idAppBD = 0)
        {
            return Grabar<clsResultado>(mensaje, dataIN, dataOUT, idAppBD);
        }

        public clsResultado GrabarLog(Exception exception, Object dataIN = null, Object dataOUT = null, Int16 idAppBD = 0)
        {
            return Grabar<clsResultado>(exception, dataIN, dataOUT, idAppBD);
        }

        #endregion Public
    }
}