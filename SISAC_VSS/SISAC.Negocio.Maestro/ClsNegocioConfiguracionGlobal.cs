﻿using SISAC.Data.Maestro;
using SISAC.Entidad.Maestro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Negocio.Maestro
{
    public class ClsNegocioConfiguracionGlobal
    {
        #region Field

        private static readonly ClsConfiguracionGlobalDAO _configuracionGlobalDAO = ClsConfiguracionGlobalDAO.Instancia;
        private static readonly Lazy<ClsNegocioConfiguracionGlobal> _Instancia = new Lazy<ClsNegocioConfiguracionGlobal>(() => new ClsNegocioConfiguracionGlobal(), LazyThreadSafetyMode.PublicationOnly);

        #endregion

        #region Property

        public static ClsNegocioConfiguracionGlobal Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion

        #region Constructor

        private ClsNegocioConfiguracionGlobal()
        {
        }

        #endregion

        #region Public

        public ClsVariableOptimus Obtenervariable(string nombre)
        {
            return _configuracionGlobalDAO.Obtenervariable(nombre);
        }

        #endregion

    }
}
