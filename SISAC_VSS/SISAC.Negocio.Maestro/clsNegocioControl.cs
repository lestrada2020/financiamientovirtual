﻿using SISAC.Data.Maestro;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace SISAC.Negocio.Maestro
{
    #region clsNegocioControl

    public sealed class clsNegocioControl
    {
        #region Field

        private static readonly clsControlDAO _ControlDAO = clsControlDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly Lazy<clsNegocioControl> _Instancia = new Lazy<clsNegocioControl>(() => new clsNegocioControl(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioControl Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioControl()
        {
        }

        #endregion Constructor

        #region Public

        #region Cache

        public IEnumerable CacheList(clsParametro param)
        {
            try
            {
                var result = _ControlDAO.CacheList(param);

                return result;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public String CacheClear(clsParametro param)
        {
            try
            {
                var result = _ControlDAO.CacheClear(param);

                return result;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion Cache

        public clsResultado AyudaLista(clsParametro entidad)
        {
            try
            {
                var table = _ControlDAO.AyudaLista(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado SendMail(clsParametro entidad)
        {
            var result = new clsResultado();

            try
            {
                Hashtable p = null;

                if (entidad.Parametros.ContainsKey("Base64"))
                {
                    result.Mensaje = entidad.Parametros["Base64"].FromBase64String();
                    var a = _Convert.ToObject<Dictionary<String, Object>>(result.Mensaje);
                    p = new Hashtable(a);
                }
                else
                {
                    result.Mensaje = _Convert.ToJson(entidad.Parametros);
                    p = new Hashtable(entidad.Parametros);
                }

                var from = p["From"].ToString();
                var to = p["To"].ToString();
                var subject = p["Subject"].ToString();
                var body = "";
                var attachments = "";
                var alias = "Notificaciones Distriluz";

                if (p.ContainsKey("Body") && p["Body"] != null && p["Body"].ToString().Length > 0) body = p["Body"].ToString();
                if (p.ContainsKey("Attachment") && p["Attachment"] != null && p["Attachment"].ToString().Length > 0) attachments = p["Attachment"].ToString();
                if (p.ContainsKey("FromName") && p["FromName"] != null && p["FromName"].ToString().Length > 0) alias = p["FromName"].ToString();

                var message = new MailMessage()
                {
                    IsBodyHtml = true,
                    From = new MailAddress(from, alias),
                    Subject = subject,
                    Body = body,
                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure | DeliveryNotificationOptions.OnSuccess
                };

                message.CC.Add(new MailAddress(from));

                foreach (var item in to.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries)) message.To.Add(new MailAddress(item));

                if (!String.IsNullOrWhiteSpace(attachments))
                    foreach (var item in attachments.Split(new String[] { ";" }, StringSplitOptions.RemoveEmptyEntries)) message.Attachments.Add(new Attachment(item));

                var client = new SmtpClient()
                {
                    Host = "owa.fonafe.gob.pe",
                    Port = 25,
                    Credentials = CredentialCache.DefaultNetworkCredentials,
                    UseDefaultCredentials = true
                };

                client.Send(message);

                result.Datos = "OK";
            }
            catch (Exception ex)
            {
                result.IdError = 1;
                result.Mensaje = $"{ex.Message} <==> {result.Mensaje ?? "(NULL)"}";
            }

            return result;
        }

        #endregion Public
    }

    #endregion clsNegocioControl
}