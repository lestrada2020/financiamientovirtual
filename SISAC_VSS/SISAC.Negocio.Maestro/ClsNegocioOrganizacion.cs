﻿using SISAC.Data.Maestro;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Req-002 - 201909 (Mod. por: ECALDAS CANVIA)
/// </summary>
namespace SISAC.Negocio.Maestro
{
    public class ClsNegocioOrganizacion
    {
        #region Field

        private static readonly ClsOrganizacionDAO _organizacionDAO = ClsOrganizacionDAO.Instancia;
        private static readonly Lazy<ClsNegocioOrganizacion> _Instancia = new Lazy<ClsNegocioOrganizacion>(() => new ClsNegocioOrganizacion(), LazyThreadSafetyMode.PublicationOnly);
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;

        #endregion

        #region Property

        public static ClsNegocioOrganizacion Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion

        #region Constructor

        private ClsNegocioOrganizacion()
        {
        }

        #endregion

        #region Public - Empresa

        public ClsEmpresa ObtenerRegistroEmpresa(short idEmpresa)
        {
            return _organizacionDAO.ObtenerRegistroEmpresa(idEmpresa);
        }

        #region Listas maestras para SIGOF

        public clsResultadoMaestro ListarUUNNxIdEmpresa(Int16 id_empresa)
        {
            try
            {
                var dt = _organizacionDAO.ListarUUNNxIdEmpresa(id_empresa);
                var json = String.Empty;

                var jsonResult = new StringBuilder();

                if (dt.Rows.Count == 0)
                {
                    json = "[]";
                }
                else
                {
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        jsonResult.Append(row[0].ToString());
                    }

                    json = jsonResult.ToString();
                }                

                var result = new clsResultadoMaestro();
                result.error_code = "200";
                result.success = true;
                result.message = "Listado de unidades de negocio por empresa.";
                result.data = _Convert.ToObject<List<Dictionary<String, Object>>>(json.ToString());

                return result;
            }
            catch (Exception ex)
            {
                var error = new clsResultadoMaestro();
                error.error_code = "0";
                error.success = false;
                error.message = ex.Message;
                error.data = null;
                return error;
            }
        }

        public clsResultadoMaestro ListarCCSSxIdUUNN(Int16 id_uunn)
        {
            try
            {
                var dt = _organizacionDAO.ListarCCSSxIdUUNN(id_uunn);
                var json = String.Empty;

                var jsonResult = new StringBuilder();

                if (dt.Rows.Count == 0)
                {
                    json = "[]";
                }
                else
                {
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        jsonResult.Append(row[0].ToString());
                    }

                    json = jsonResult.ToString();
                }

                var result = new clsResultadoMaestro();
                result.error_code = "200";
                result.success = true;
                result.message = "Listado de centros de servicio por unidad de negocio.";
                result.data = _Convert.ToObject<List<Dictionary<String, Object>>>(json.ToString());

                return result;
            }
            catch (Exception ex)
            {
                var error = new clsResultadoMaestro();
                error.error_code = "0";
                error.success = false;
                error.message = ex.Message;
                error.data = null;
                return error;
            }
        }

        public clsResultadoMaestro ListarSectoresxIdCCSS(Int16 id_centroservicio)
        {
            try
            {
                var dt = _organizacionDAO.ListarSectoresxIdCCSS(id_centroservicio);
                var json = String.Empty;

                var jsonResult = new StringBuilder();

                if (dt.Rows.Count == 0)
                {
                    json = "[]";
                }
                else
                {
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        jsonResult.Append(row[0].ToString());
                    }

                    json = jsonResult.ToString();
                }

                var result = new clsResultadoMaestro();
                result.error_code = "200";
                result.success = true;
                result.message = "Listado de sectores de servicio por centro de servicio.";
                result.data = _Convert.ToObject<List<Dictionary<String, Object>>>(json.ToString());

                return result;
            }
            catch (Exception ex)
            {
                var error = new clsResultadoMaestro();
                error.error_code = "0";
                error.success = false;
                error.message = ex.Message;
                error.data = null;
                return error;
            }
        }

        public clsResultadoMaestro ListarRutasxIdSector(Int16 id_sectorservicio)
        {
            try
            {
                var dt = _organizacionDAO.ListarRutasxIdSector(id_sectorservicio);
                var json = String.Empty;

                var jsonResult = new StringBuilder();

                if (dt.Rows.Count == 0)
                {
                    json = "[]";
                }
                else
                {
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        jsonResult.Append(row[0].ToString());
                    }

                    json = jsonResult.ToString();
                }

                var result = new clsResultadoMaestro();
                result.error_code = "200";
                result.success = true;
                result.message = "Listado de rutas por sector de servicio.";
                result.data = _Convert.ToObject<List<Dictionary<String, Object>>>(json.ToString());

                return result;
            }
            catch (Exception ex)
            {
                var error = new clsResultadoMaestro();
                error.error_code = "0";
                error.success = false;
                error.message = ex.Message;
                error.data = null;
                return error;
            }
        }

        #endregion

        #endregion
    }
}
