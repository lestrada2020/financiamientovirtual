﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Data.General
{
    public sealed class clsProyectosDAO
    {
        #region Field

        private static readonly clsDataAccessAsync _DAA = clsDataAccessAsync.Instancia;
        private static readonly Lazy<clsProyectosDAO> _Instancia = new Lazy<clsProyectosDAO>(() => new clsProyectosDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsProyectosDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsProyectosDAO()
        {
        }

        #endregion Constructor

        #region Public

        public async Task<DataTable> AyudaLista(clsParametro entidad)
        {
            try
            {
                var entity = "#####";
                var action = "AyudaLista";

                if (entidad.Parametros.ContainsKey("Entity")) entity = entidad.Parametros["Entity"];
                if (entidad.Parametros.ContainsKey("Action")) action = entidad.Parametros["Action"];

                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync(entity, action, param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ListIniciativas(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("gnl.ProyIniciativa", "Buscar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ObtenerNivelesFase(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("gnl.ProyFase", "ObtenerNiveles", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> ObtenerUsuario(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("gnl.ProyUsuario", "Obtener", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> ObtenerIniciativa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("gnl.ProyIniciativa", "Obtener", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Object> RegistrarHito(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("gnl.ProyIniciativaHito", "Registrar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ObtenerHito(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("gnl.ProyIniciativaHito", "Obtener", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ExportarIniciativa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("gnl.ProyIniciativa", "Exportar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ExportarImporte(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("gnl.ProyIniciativaImporte", "Exportar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Object> RegistrarImporte(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteScalarAsync("gnl.ProyIniciativaImporte", "Registrar", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataTable> ObtenerImporte(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteTableAsync("gnl.ProyIniciativaImporte", "Obtener", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DataRow> EditarIniciativa(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = await _DAA.ExecuteRowAsync("gnl.ProyIniciativa", "EditarIniciativa", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }
}