﻿using SISAC.Entidad.Maestro;
using System.Web.Http;

namespace SISAC.Interface.Seguridad
{
    public interface IPerfilUsuario
    {
        IHttpActionResult ValidarToken(clsParametro param);

        IHttpActionResult ListAppConfig(clsParametro param);

        IHttpActionResult ObtenerDatosUsuario(clsParametro param);

        IHttpActionResult ObtenerModulosAplicacionPorUsuario(clsParametro param);

        IHttpActionResult ObtenerRolesAplicacionPorUsuario(clsParametro param);

        IHttpActionResult ObtenerOpcionesPorModuloAplicacionUsuario(clsParametro param);

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        IHttpActionResult ObtenerRestriccionVistaUsuario(clsParametro param);

        #endregion

    }
}