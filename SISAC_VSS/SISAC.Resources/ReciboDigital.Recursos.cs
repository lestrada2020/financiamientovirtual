﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISAC.Resources
{
    public static class clsRecursoGrafico
    {
        public static Byte[] ReporteReciboCabecera
        {
            get { return OptimusNGGrafico.RptReciboMenor_Cabecera; }
        }
        public static Byte[] ReporteReciboCabeceraUno
        {
            get { return OptimusNGGrafico.RptReciboMenorUno_Cabecera; }
        }

        public static Byte[] ReporteReciboConcepto
        {
            get { return OptimusNGGrafico.RptReciboMenor_Concepto; }
        }

        public static Byte[] ReporteReciboConceptoImporte
        {
            get { return OptimusNGGrafico.RptReciboMenor_ConceptoImporte; }
        }

        public static Byte[] ReporteReciboCabeceraMayor
        {
            get { return OptimusNGGrafico.RptReciboMayor_Cabecera; }
        }

        public static Byte[] ReporteReciboConceptoMayor
        {
            get { return OptimusNGGrafico.RptReciboMayor_Concepto; }
        }

        public static Byte[] ReciboBlanco
        {
            get { return OptimusNGGrafico.ReciboBlanco; }
        }

        public static Byte[] CaraTriste
        {
            get { return OptimusNGGrafico.CaraTriste; }
        }

        public static Byte[] CaraAlegre
        {
            get { return OptimusNGGrafico.CaraAlegre; }
        }
    }
}
