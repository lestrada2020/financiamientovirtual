﻿using SISAC.Data.DAO;
using SISAC.Data.Facturacion;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace SISAC.Negocio.Facturacion
{
    public class clsGestionarImpresionReciboIndividual
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;

        #endregion Field

        #region Campos

        private static clsGestionarImpresionReciboIndividual oGestionarRecibo;

        #endregion Campos

        #region Obtener Instancia

        public static clsGestionarImpresionReciboIndividual ObtenerInstancia()
        {
            if (oGestionarRecibo == null)
            {
                lock (typeof(clsGestionarImpresionReciboIndividual))
                {
                    if (oGestionarRecibo == null)
                    {
                        oGestionarRecibo = new clsGestionarImpresionReciboIndividual();
                    }
                }
            }
            return oGestionarRecibo;
        }

        #endregion Obtener Instancia

        #region Metodos Privados

        private void CrearTemporalMenor(SqlConnection sqlc)
        {
            Hashtable _htparametros = new Hashtable();
            String _strQuery = "";

            _htparametros.Add("@cadena", "");
            _DAO.EjecutarComando("recibomenor"
                                     , "creartablacabecera"
                                     , _htparametros, sqlc);
            _strQuery = _htparametros["@cadena"].ToString();

            _DAO.EjecutarComando(_strQuery, sqlc);

            _htparametros.Clear();

            _htparametros.Add("@cadena", "");
            _DAO.EjecutarComando("recibomenor"
                                     , "creartabladetalle"
                                     , _htparametros, sqlc);
            _strQuery = _htparametros["@cadena"].ToString();

            _DAO.EjecutarComando(_strQuery, sqlc);
        }

        private void InsertarRegistroTemporal(clsListaSuministroRecibo objlistaimprimir
                                            , SqlConnection sqlconecction)
        {
            foreach (clsSuministroRecibo oReciboMenor in objlistaimprimir.Elementos)
            {
                _DAO.EjecutarComando("insert into #Registros( IdNroServicio, Periodo ) values ("
                    + oReciboMenor.IdNroServicio.ToString().Trim()
                    + "," + oReciboMenor.Periodo.ToString().Trim() + ")", sqlconecction);
            }
        }

        public void CrearTemporalMayor(SqlConnection sqlc)
        {
            Hashtable _htparametros = new Hashtable();
            String _strQuery = "";

            _htparametros.Add("@cadena", "");
            _DAO.EjecutarComando("recibomayor"
                                     , "creartablacabecera"
                                     , _htparametros, sqlc);
            _strQuery = _htparametros["@cadena"].ToString();

            _DAO.EjecutarComando(_strQuery, sqlc);

            _htparametros.Clear();

            _htparametros.Add("@cadena", "");
            _DAO.EjecutarComando("recibomayor"
                                     , "creartabladetalle"
                                     , _htparametros, sqlc);
            _strQuery = _htparametros["@cadena"].ToString();

            _DAO.EjecutarComando(_strQuery, sqlc);

            #region Borrar

            // crear el temporal.
            //String CadenaTemporal =
            //" if object_id('tempdb..#Registros')>0 drop table #Registros " +
            //" create table #Registros " +
            //" ( IdNroServicio int  " +
            //" ,periodo int )";

            //clsGestionDato _DAO = clsGestionDato.ObtenerInstancia();
            //_DAO.EjecutarComando(CadenaTemporal, sqlc);

            // Crear la tabla temporal de los recibos
            //CadenaTemporal =
            //" if object_id('tempdb..#Recibos')>0 drop table #Recibos " +
            //" create table #Recibos " +
            //" ( IdTipoDocumento smallint " +
            //" ,NroDocumento varchar(13) " +
            //" ,NombreDistrito   varchar(60) " +
            //" ,NombreProvincia  varchar(60) " +
            //" ,IdNroServicio     int " +
            //" ,NombreNroServicio  varchar(80) " +
            //" ,Direccion          varchar(80) " +
            //" ,DireccionReferencial varchar(80) " +
            //" ,RUC   varchar(11) " +
            //" ,IdAnoComercial     int  " +
            //" ,IdMesComercial     smallint  " +
            //" ,Periodo  Int " +
            //" ,PeriodoAnterior int " +
            //" ,PeriodoConsumoInicio int "+
            //" ,FechaEmision        datetime " +
            //" ,FechaVencimiento    datetime " +
            //" ,TotalMes    money " +
            //" ,TotalAPagar   money " +
            //" ,ImporteAplicado  money " +
            //" ,DeudaAnteriorInicial  money " +
            //" ,NroMesesDeuda   smallint " +
            //" ,RutaReparto   varchar(15) " +
            //" ,IdDireccionCodificadaReparto int " +
            //" ,DireccionCodificadaReparto varchar(80) " +
            //" ,IdEmpresaServicio smallint " +
            //" ,IdMoneda    smallint " +
            //" ,SimboloMoneda   varchar(10) " +
            //" ,IdSector smallint " +
            //" ,IdEmpresa smallint " +
            //" ,ParaCorte smallint " +
            //" ,Duplicado smallint " +
            //" ,IdNroContrato int " +
            //" ,IdTipoPuntoMedicion smallint " +
            //" ,PuntoMedicion int " +
            //" ,IdTension smallint " +
            //" ,IdCiclo smallint " +
            //" ,IdIteracion smallint ) ";

            //_DAO.EjecutarComando(CadenaTemporal, sqlc);

            #endregion Borrar
        }

        #endregion Metodos Privados

        #region Metodos

        public byte[] ObtenerInformacionImpresionComunes(clsListaSuministroRecibo objlistasuministro
                                                            , Int16 esruta
                                                            , Int16 esduplicado
                                                            , String archivoblanco
                                                            , clsNroServicioBasico oDatosNroServicio
                                                            , Int32 intPeriodo
                                                            , Int16 intIdTipoDocumento)
        {
            byte[] bytReciboDigital = null;

            clsImprimirReciboMenor objimprimirsuministro = new clsImprimirReciboMenor();

            SqlConnection _objconexion = _DAO.GetSqlConnection();

            try
            {
                _objconexion.Open();

                // crear el archivo temporal de los registros.
                CrearTemporalMenor(_objconexion);

                InsertarRegistroTemporal(objlistasuministro, _objconexion);

                // invocar a la cabecera.
                objimprimirsuministro.ListaReciboMenorCabecera = new clsListaReciboMenorCabecera(clsReciboComunDAO.Instancia.ExtraerDatosMenor_Cabecera(_objconexion
                                                                , esruta
                                                                , esduplicado
                                                                , archivoblanco));
                // invocar a los conceptos.
                objimprimirsuministro.ListaReciboMenorConcepto = new clsListaReciboMenorConcepto(clsReciboComunDAO.Instancia.ExtraerDatosMenor_Concepto(_objconexion));

                // invocar a lo consumo.
                objimprimirsuministro.ListaReciboMenorConsumo = new clsListaReciboMenorConsumo(clsReciboComunDAO.Instancia.ExtraerDatosMenor_Consumo(_objconexion));

                // invocar a la lectura.
                objimprimirsuministro.ListaReciboMenorLectura = new clsListaReciboMenorLectura(clsReciboComunDAO.Instancia.ExtraerDatosMenor_Lectura(_objconexion));

                String _strrutacaraalegre = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraAlegre(Cartera.Comun);
                String _strrutacaratriste = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraTriste(Cartera.Comun);

                foreach (clsReciboMenorCabecera item in objimprimirsuministro.ListaReciboMenorCabecera.Elementos)
                {
                    if (item.IncluirCaritas == 1)
                    {
                        item.ImagenCara1 = item.IdEstrato1 == 1 ? _strrutacaraalegre : _strrutacaratriste;
                        item.ImagenCara2 = item.IdEstrato2 == 1 ? _strrutacaraalegre : _strrutacaratriste;
                        item.ImagenCara3 = item.IdEstrato3 == 1 ? _strrutacaraalegre : _strrutacaratriste;
                    }
                }

                clsGestionarEnvioReciboDigitalComun oProcesar = new clsGestionarEnvioReciboDigitalComun();
                bytReciboDigital = oProcesar.Ejecutar(objimprimirsuministro
                                , oDatosNroServicio.IdUUNN
                                , oDatosNroServicio.IdCentroServicio
                                , intPeriodo
                                , intIdTipoDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (_objconexion.State == ConnectionState.Open) _objconexion.Close();
            }

            return bytReciboDigital;
        }

        public byte[] ObtenerInformacionImpresionMayores(clsListaSuministroRecibo objlistasuministro
                                                        , Int16 esruta
                                                        , Int16 esduplicado
                                                        , String archivoblanco
                                                        , clsNroServicioBasico oDatosNroServicio
                                                        , Int32 intPeriodo
                                                        , Int16 intIdTipoDocumento)
        {
            byte[] bytReciboDigital = null;

            clsImprimirReciboMayor objimprimirsuministro = new clsImprimirReciboMayor();

            SqlConnection _objconexion = _DAO.GetSqlConnection();

            try
            {
                _objconexion.Open();

                // crear el archivo temporal de los registros.
                CrearTemporalMayor(_objconexion);

                InsertarRegistroTemporal(objlistasuministro, _objconexion);

                // invocar a la cabecera.
                objimprimirsuministro.ListaReciboMayorCabecera = new clsListaReciboMayorCabecera(clsReciboMayorDAO.Instancia.ExtraerDatosMayor_Cabecera(_objconexion
                                                                , esruta
                                                                , esduplicado
                                                                , archivoblanco));
                // invocar a los conceptos.
                objimprimirsuministro.ListaReciboMayorConcepto = new clsListaReciboMayorConcepto(clsReciboMayorDAO.Instancia.ExtraerDatosMayor_Concepto(_objconexion));

                // invocar a lo consumo.
                objimprimirsuministro.ListaReciboMayorConsumo = new clsListaReciboMayorConsumo(clsReciboMayorDAO.Instancia.ExtraerDatosMayor_Consumo(_objconexion));

                // invocar a la lectura.
                objimprimirsuministro.ListaReciboMayorLectura = new clsListaReciboMayorLectura(clsReciboMayorDAO.Instancia.ExtraerDatosMayor_Lectura(_objconexion));

                clsGestionarEnvioReciboDigitalMayor oProcesar = new clsGestionarEnvioReciboDigitalMayor();
                bytReciboDigital = oProcesar.Ejecutar(objimprimirsuministro, oDatosNroServicio.IdUUNN, oDatosNroServicio.IdCentroServicio, intPeriodo, intIdTipoDocumento);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (_objconexion.State == ConnectionState.Open)
                {
                    _objconexion.Close();
                }
            }

            return bytReciboDigital;
        }

        #endregion Metodos
    }
}