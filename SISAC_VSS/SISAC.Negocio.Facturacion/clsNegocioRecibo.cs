﻿using SISAC.Data.Facturacion;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Util.Tool;
using System;
using System.Threading;

namespace SISAC.Negocio.Facturacion
{
    public sealed class clsNegocioRecibo
    {
        #region Field

        private static readonly clsNroServicioDAO _NroServicioDAO = clsNroServicioDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly Lazy<clsNegocioRecibo> _Instancia = new Lazy<clsNegocioRecibo>(() => new clsNegocioRecibo(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioRecibo Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioRecibo()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado DescargarRecibo(clsParametro entidad)
        {
            try
            {
                Int32 intIdNroServicio = Convert.ToInt32(entidad.Parametros["IdNroServicio"]);
                Int32 intIdPeriodo = Convert.ToInt32(entidad.Parametros["IdPeriodo"]);
                Int16 intIdTipoDocumento = Convert.ToInt16(entidad.Parametros["IdTipoDocumento"]);

                clsListaSuministroRecibo oListaSuministro = new clsListaSuministroRecibo();
                clsSuministroRecibo oSuministro = new clsSuministroRecibo(intIdNroServicio, intIdPeriodo);

                oListaSuministro.Elementos.Add(oSuministro);

                clsNroServicioBasico oDatosNroServicio = _NroServicioDAO.ObtenerRegistroBasico(intIdNroServicio);

                byte[] bytReciboImagen = null;

                if (oDatosNroServicio.Cartera == "C")
                {
                    clsGestionarImpresionReciboIndividual oRecibo = new clsGestionarImpresionReciboIndividual();
                    bytReciboImagen = oRecibo.ObtenerInformacionImpresionComunes(oListaSuministro, 0, 0, String.Empty, oDatosNroServicio, intIdPeriodo, intIdTipoDocumento);
                }
                else if (oDatosNroServicio.Cartera == "M")
                {
                    clsGestionarImpresionReciboIndividual oRecibo = new clsGestionarImpresionReciboIndividual();
                    bytReciboImagen = oRecibo.ObtenerInformacionImpresionMayores(oListaSuministro, 0, 0, String.Empty, oDatosNroServicio, intIdPeriodo, intIdTipoDocumento);
                }

                String strReciboBase64 = Convert.ToBase64String(bytReciboImagen);

                var result = new clsResultado();
                result.Datos = strReciboBase64;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerJSON(clsParametro entidad)
        {
            try
            {
                var value = _NroServicioDAO.ObtenerJSONRecibo(entidad);

                var result = new clsResultado();
                result.Datos = value.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        #endregion Public
    }
}