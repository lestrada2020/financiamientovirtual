﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using SISAC.Data.Facturacion;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Facturacion.ImpresionRecibo;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Negocio.Proceso;
using SISAC.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SISAC.Negocio.Facturacion
{
    #region clsNegocioSolicitudImpresionRecibo

    public sealed class clsNegocioSolicitudImpresionRecibo
    {
        #region Field

        private static readonly clsSolicitudImpresionReciboDAO _SolicImpresionReciboDAO = clsSolicitudImpresionReciboDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly Lazy<clsNegocioSolicitudImpresionRecibo> _Instancia = new Lazy<clsNegocioSolicitudImpresionRecibo>(() => new clsNegocioSolicitudImpresionRecibo(), LazyThreadSafetyMode.PublicationOnly);
        private static readonly ClsNegocioTarea _NegocioTarea = ClsNegocioTarea.Instancia;
        private clsImprimirReciboMenor _objImprimirReciboMenor;
        private clsImprimirReciboMayor _objImprimirReciboMayor;

        #endregion Field

        #region Property

        public static clsNegocioSolicitudImpresionRecibo Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioSolicitudImpresionRecibo()
        {
        }

        #endregion Constructor

        #region Private

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        private clsParametroImprimirRecibos ObtenerParametrosTareaProcesarArchivoImpresion(clsParametro entidad,
            System.Data.DataTable table)
        {
            //var table = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);

            var clsParametroImprimirRecibos = new clsParametroImprimirRecibos
            {
                //IdUUNNPadre = (short)dtSolImpresion.Rows[0]["IdUUNN"], //De la solicitud de impresion
                //IdCentroServicioPadre = (short)dtSolImpresion.Rows[0]["IdCentroServicio"], //De la solicitud de impresion
                //Cartera = dtSolImpresion.Rows[0]["Cartera"].ToString(), //De la solicitud de impresion
                EsRuta = 1,
                //IdAno = Convert.ToInt16(dtSolImpresion.Rows[0]["IdAnioComercial"]), //De la solicitud de impresion
                //IdMes = (short)dtSolImpresion.Rows[0]["IdMesComercial"], //De la solicitud de impresion
                //IdCentroServicio = (short)dtSolImpresion.Rows[0]["IdCentroServicio"], //De la solicitud de impresion
                //IdEstrato = (short)dtSolImpresion.Rows[0]["IdEstrato"], //De la solicitud de impresion
                IdProveedorActividad = 0,
                //IdUUNN = (short)dtSolImpresion.Rows[0]["IdUUNN"], //De la solicitud de impresion
                //Periodo = (int)dtSolImpresion.Rows[0]["Periodo"], //De la solicitud de impresion
                //IdEmpresa = (short)dtSolImpresion.Rows[0]["IdEmpresa"], //De la solicitud de impresion
                //EsFacturacionMensual = ((short)dtSolImpresion.Rows[0]["IdTipoFacturacion"]) == 1, //De la solicitud de impresion
                //IdSector = (short)dtSolImpresion.Rows[0]["IdSector"], //De la solicitud de impresion
                IdNroServicioDesde = 0,
                IdNroServicioHasta = 0,
                enumTipoImpresion = TipoImpresionEnum.ImpresionRutaJSON,
                IdUsuario = Convert.ToInt32(entidad.Parametros["IdUsuario"]),
                IdSolicitudImpresion = Convert.ToInt32(entidad.Parametros["IdSolicitudImpresion"]),
                ArchivoBlanco = string.Concat(
                    "FILE://",
                    clsUtilidadesImpresionRecibo.ObtenerCarpetaYNombreArchivoReciboBlanco(table.Rows[0]["NroSolicitud"].ToString())),
                DatosVarios = string.Empty //Aqui pueden ir otros parametros que se necesiten                
                //ListaRutas = new clsListaSuministrosRutaRecibo(dtDetSolImpresion) //Detalle de Rutas de la Solicitud
            };

            #region Codigo de referencia

            //VerificarItemsRuta();
            //Validar: Esto debe ir en la Opcion Impresion recibo
            //Tener en cuenta el campo "Cartera" de la solicitud de impresion
            //clsParametroImprimirRecibos.ArchivoBlanco = cboCartera.Valor.ToString() == "C" ? "FILE://" + UtilidadesCliente.clsGestionarImpresion.CrearArchivosRecibos(Cartera.Comun, (Int16)0)
            //                                          : "FILE://" + UtilidadesCliente.clsGestionarImpresion.CrearArchivosRecibos(Cartera.Mayor, (Int16)0);
            //clsParametroImprimirRecibos.Cartera = cboCartera.Valor.ToString();
            //clsParametroImprimirRecibos.IdAno = ucPeriodoComercial1.Ayo;
            //clsParametroImprimirRecibos.IdMes = ucPeriodoComercial1.Mes;
            //clsParametroImprimirRecibos.IdCentroServicio = _intidcentroservicio; //Validar
            //clsParametroImprimirRecibos.IdEstrato = cboEstrato.Id16 == 10000 ? (Int16)0 : cboEstrato.Id16;
            //clsParametroImprimirRecibos.IdUUNN = cboUUNN.Id16;
            //clsParametroImprimirRecibos.Periodo = ucPeriodoComercial1.Valor;
            //clsParametroImprimirRecibos.IdEmpresa = cboEmpresa.Id16;
            //clsParametroImprimirRecibos.EsFacturacionMensual = opgFacturacionMensual.Checked;
            //clsParametroImprimirRecibos.IdSector = cboSector.Id16;
            //clsParametroImprimirRecibos.IdNroServicioDesde = Convert.ToInt32(txtSuministroDesde.Text);s
            //clsParametroImprimirRecibos.IdNroServicioHasta = Convert.ToInt32(txtSuministroHasta.Text);
            //clsParametroImprimirRecibos.ListaRutas = ListaRutasCheckeadas; //Detalle de Rutas de la Solicitud

            #endregion

            return clsParametroImprimirRecibos;
        }

        private ClsParametroImpresionRecibosNotificar ObtenerParametrosTareaImpresionReciboNotificar(clsParametro entidad,
            System.Data.DataTable table)
        {
            //var table = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);

            var clsParamImprimirRecibosNotificar = new ClsParametroImpresionRecibosNotificar
            {
                IdSolicitudImpresion = Convert.ToInt32(entidad.Parametros["IdSolicitudImpresion"]),
                IdEmpresa = (short)table.Rows[0]["IdEmpresa"],
                IdUsuario = Convert.ToInt32(entidad.Parametros["IdUsuario"]),
                IdUUNNPadre = (short)table.Rows[0]["IdUUNN"],
                IdCentroServicioPadre = (short)table.Rows[0]["IdCentroServicio"],
                Periodo = (int)table.Rows[0]["Periodo"],
                Cartera = table.Rows[0]["Cartera"].ToString(),
                ListaIdsSuministros = entidad.Parametros["ListaIdsSuministros"],
                DatosVarios = string.Empty, //Aqui pueden ir otros parametros que se necesiten                
            };

            return clsParamImprimirRecibosNotificar;
        }

        private clsResultado NotificarClientes(clsParametro entidad, int idProceso, TipoNotificacionEnum enumTipoNotificacion)
        {
            var clsTareaProgramacion = new ClsTareaProgramacion
            {
                IdProceso = idProceso,
                IdUsuarioGenerador = Convert.ToInt32(entidad.Parametros["IdUsuario"]),
                IdUUNN = 0 //CanviaDev_2020-05-27
            };

            try
            {
                //Obtener datos para creacion de tarea de notificacion
                var table = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);

                clsTareaProgramacion.IdEmpresa = Convert.ToInt16(table.Rows[0]["IdEmpresa"].ToString());
                //CanviaDev_2020-05-27
                //Evaluar si se envia la UUNN para la creacion de la Tarea
                string valParamNivelTarea = ObtenerValorParametroConfiguracion(clsTareaProgramacion.IdEmpresa,
                    (short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.NivelRegistroTarea);
                if (valParamNivelTarea == string.Empty)
                {
                    return new clsResultado
                    {
                        IdError = 99999,
                        Mensaje = "No existe parametro de configuracion de Nivel de Registro de Tarea (Empresa o Unidad de Negocio)."
                    };
                }
                else if (valParamNivelTarea == "2") // Nivel de UUNN
                {
                    if (string.IsNullOrWhiteSpace(entidad.Parametros["IdUUNN"]) ||
                        entidad.Parametros["IdUUNN"].Trim() == "0")
                    {
                        clsTareaProgramacion.IdUUNN = Convert.ToInt16(table.Rows[0]["IdUUNN"].ToString());
                    }
                    else
                    {
                        clsTareaProgramacion.IdUUNN = Convert.ToInt16(entidad.Parametros["IdUUNN"]);
                    }
                }
                //CanviaDev_2020-05-27:Fin

                ClsParametroImpresionRecibosNotificar clsParamImpresionRecibosNotificar =
                    ObtenerParametrosTareaImpresionReciboNotificar(entidad, table);
                clsParamImpresionRecibosNotificar.EnumTipoNotificacion = enumTipoNotificacion;

                clsTareaProgramacion.ParametroTarea = clsParamImpresionRecibosNotificar;
                //Cuando se requiere que la Tarea se ejecute recurrentemente
                //clsTareaProgramacion.Frecuencia = 3;
                //clsTareaProgramacion.IntervaloEnSegundos = 300;

                //Grabar tarea
                int idTarea = _NegocioTarea.GrabarTareaInmediata(clsTareaProgramacion);

                //Actualizar relacion entre Solicitud de impresion y tarea
                var objClsParametro = new clsParametro
                {
                    Parametros = new Dictionary<string, string>
                    {
                        { "IdSolicitudImpresion", entidad.Parametros["IdSolicitudImpresion"] },
                        { "IdTarea", idTarea.ToString() },
                        { "IdTipoTarea", ((int)enumTipoNotificacion).ToString() },
                        { "IdUsuarioGenerador", entidad.Parametros["IdUsuario"] }
                    }
                };
                _SolicImpresionReciboDAO.InsertarSolicitudImpresionTarea(objClsParametro);
            }
            catch (Exception)
            {
                throw;
            }

            return new clsResultado();
        }

        private string ObtenerRutaArchivoImpresion(short idUUNN, short idCentroServicio, int periodo)
        {
            clsGestionarIntercambio objGestorIntercambio = clsGestionarIntercambio.ObtenerInstancia();
            clsEsquemaCarpetaBase objEsquemaCarpetaBase = objGestorIntercambio.ObtenerEsquemaCarpetaBaseUUNN(idUUNN);

            var objEsquemaCarpeta = new clsEsquemaCarpeta
            {
                RutaInicio = objEsquemaCarpetaBase.RutaInicio,
                IdServicio = objEsquemaCarpetaBase.IdServicio,
                IdFlujo = objEsquemaCarpetaBase.IdFlujo,
                IdEmpresa = objEsquemaCarpetaBase.IdEmpresa,
                IdActividad = objEsquemaCarpetaBase.IdActividad,
                IdUnidadNegocio = idUUNN,
                IdCentroServicio = idCentroServicio,
                Periodo = periodo.ToString(),
                TipoIntercambio = enumTipoIntercambio.Envio
            };

            //string rutaDescargaArchivo =
            //    clsGestorEsquemaIntercambio.ObtenerEsquemaCarpetaUUNN(objEsquemaCarpeta, false);
            string rutaDescargaArchivo =
                clsGestorEsquemaIntercambio.ObtenerEsquemaCarpeta(objEsquemaCarpeta);

            if (string.IsNullOrWhiteSpace(rutaDescargaArchivo))
                throw new Exception("Error al obtener la carpeta de descarga en el servidor.");

            return rutaDescargaArchivo;
        }

        private async Task DescargarArchivosImpresionReciboAsync(ClsListaControlGenerarArchivoImpresion listaControlGenerarArchivo,
            string rutaArchivosServidor)
        {
            bool esAccesoRedLocalServidorNGC = bool.Parse(ConfigurationManager.AppSettings["esAccesoRedLocalServidorNGC"]);
            string nombreArchivoRemotoOrigen;

            if (esAccesoRedLocalServidorNGC)
            {
                #region Copiar archivo por la Red local

                foreach (var archivo in listaControlGenerarArchivo.Elementos)
                {
                    nombreArchivoRemotoOrigen = Path.GetFileName(archivo.NombreArchivo);
                    string rutaYArchivoDestino = Path.Combine(rutaArchivosServidor, nombreArchivoRemotoOrigen);

                    if (!File.Exists(rutaYArchivoDestino))
                    {
                        if (!Directory.Exists(rutaArchivosServidor))
                        {
                            Directory.CreateDirectory(rutaArchivosServidor);
                        }

                        File.Copy(archivo.NombreArchivo, rutaYArchivoDestino);
                    }
                }

                #endregion
            }
            else
            {
                #region Descargar archivo con una peticion Http

                ClsVariableOptimus variableOptimus =
                    ClsNegocioConfiguracionGlobal.Instancia.Obtenervariable("PagDescargaArchivo");
                if (variableOptimus == null)
                {
                    throw new Exception("No esta registrado el Parametro de Configuracion Global para descarga de archivos.");
                }
                string urlAplicacionDescarga = variableOptimus.Valor;
                string rutaRemotaOrigen;

                foreach (var archivo in listaControlGenerarArchivo.Elementos)
                {
                    rutaRemotaOrigen = Path.GetDirectoryName(archivo.NombreArchivo);
                    nombreArchivoRemotoOrigen = Path.GetFileName(archivo.NombreArchivo);

                    await DescargaArchivoImpresionAsync(urlAplicacionDescarga, rutaRemotaOrigen, nombreArchivoRemotoOrigen,
                        rutaArchivosServidor);
                }

                #endregion
            }
        }

        private async Task DescargaArchivoImpresionAsync(string urlAplicacionDescarga, string rutaRemotaOrigen,
            string nombreArchivoRemotoOrigen, string rutaLocalDestino)
        {
            string parametrosDescarga = string.Concat("Archivo=", nombreArchivoRemotoOrigen,
                "&RutaDescargaArchivo=", rutaRemotaOrigen);
            string urlDescargaArchivo = string.Concat(urlAplicacionDescarga, "?", parametrosDescarga);
            string rutaYArchivoDestino = Path.Combine(rutaLocalDestino, nombreArchivoRemotoOrigen);

            if (!File.Exists(rutaYArchivoDestino))
            {
                try
                {
                    await clsUtil.DownloadFileAsync(urlDescargaArchivo, rutaYArchivoDestino);
                }
                catch (HttpRequestException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            #region Codigo de referencia

            //WebClient _objclienteweb = new WebClient();

            //// Preparar parámetros.
            //_objclienteweb.QueryString.Clear();

            //_objclienteweb.QueryString.Add("idProveedorActividad", esquemaCarpeta.IdProveedorActividad.ToString());
            //_objclienteweb.QueryString.Add("idUnidadNegocio", esquemaCarpeta.IdUnidadNegocio.ToString());
            //_objclienteweb.QueryString.Add("idCentroServicio", esquemaCarpeta.IdCentroServicio.ToString());
            //_objclienteweb.QueryString.Add("Periodo", (esquemaCarpeta.Periodo == null ? String.Empty : esquemaCarpeta.Periodo.ToString()));
            //_objclienteweb.QueryString.Add("Archivo", nombreArchivo);
            //_objclienteweb.QueryString.Add("RutaDescargaArchivo", (String.IsNullOrEmpty(esquemaCarpeta.RutaDescargaArchivo) ? String.Empty : esquemaCarpeta.RutaDescargaArchivo.ToString()));

            //_objclienteweb.UseDefaultCredentials = true;
            //_objclienteweb.Credentials = CredentialCache.DefaultCredentials;
            //_objclienteweb.DownloadFileAsync(new Uri(_strdirecciondescarga), rutaDestino);

            #endregion
        }

        private async Task DescomprimirArchivosImpresionReciboAsync(
            ClsListaControlGenerarArchivoImpresion listaControlGenerarArchivo, string rutaArchivosServidor)
        {
            string nombreArchivoJson;

            try
            {
                foreach (var archivo in listaControlGenerarArchivo.Elementos)
                {
                    nombreArchivoJson = Path.Combine(rutaArchivosServidor,
                        Path.GetFileNameWithoutExtension(archivo.NombreArchivo) + ".json");

                    if (!File.Exists(nombreArchivoJson))
                    {
                        await clsUtil.DecompressFileAsync(archivo.NombreArchivo, nombreArchivoJson, false);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private Stream GenerarArchivoPDFRecibos(ClsListaControlGenerarArchivoImpresion listaControlGenerarArchivo,
            string rutaArchivosServidor, string cartera, string nroSolicitudImpresion)
        {
            var objReciboDatosProcesamiento = new ClsReciboDatosProcesamiento();
            ClsReciboDatosProcesamiento objReciboDatosProcesamientoSectorRuta;
            string nombreArchivoJson;
            string contenidoArchivoJson;

            foreach (var archivo in listaControlGenerarArchivo.Elementos)
            {
                nombreArchivoJson = Path.Combine(rutaArchivosServidor,
                    Path.GetFileNameWithoutExtension(archivo.NombreArchivo) + ".json");
                contenidoArchivoJson = File.ReadAllText(nombreArchivoJson);

                objReciboDatosProcesamientoSectorRuta = null;
                objReciboDatosProcesamientoSectorRuta =
                    JsonConvert.DeserializeObject<ClsReciboDatosProcesamiento>(contenidoArchivoJson);

                objReciboDatosProcesamiento.ReciboMenorCabecera.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorCabecera);
                objReciboDatosProcesamiento.ReciboMenorConcepto.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorConcepto);
                objReciboDatosProcesamiento.ReciboMenorConsumo.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorConsumo);
                objReciboDatosProcesamiento.ReciboMenorLectura.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorLectura);

                objReciboDatosProcesamiento.ReciboMayorCabecera.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorCabecera);
                objReciboDatosProcesamiento.ReciboMayorConcepto.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorConcepto);
                objReciboDatosProcesamiento.ReciboMayorConsumo.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorConsumo);
                objReciboDatosProcesamiento.ReciboMayorLectura.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorLectura);
            }

            if (cartera == "C")
            {
                clsImprimirReciboMenor objImprimirReciboMenor = GenerarDatosImprimirRecibosMenor(objReciboDatosProcesamiento);
                EstablecerValoresReciboMenor(objImprimirReciboMenor, cartera);

                try
                {
                    return ProcesarArchivoRecibosMenor(objImprimirReciboMenor, rutaArchivosServidor, nroSolicitudImpresion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                clsImprimirReciboMayor objImprimirReciboMayor = GenerarDatosImprimirRecibosMayor(objReciboDatosProcesamiento);
                EstablecerValoresReciboMayor(objImprimirReciboMayor, cartera);

                try
                {
                    return ProcesarArchivoRecibosMayor(objImprimirReciboMayor, rutaArchivosServidor, nroSolicitudImpresion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void GenerarYGuardarArchivoPDFRecibos(ClsListaControlGenerarArchivoImpresion listaControlGenerarArchivo,
            string rutaArchivosServidor, string cartera, string nroSolicitudImpresion)
        {
            var objReciboDatosProcesamiento = new ClsReciboDatosProcesamiento();
            ClsReciboDatosProcesamiento objReciboDatosProcesamientoSectorRuta;
            string nombreArchivoJson;
            string contenidoArchivoJson;

            foreach (var archivo in listaControlGenerarArchivo.Elementos)
            {
                nombreArchivoJson = Path.Combine(rutaArchivosServidor,
                    Path.GetFileNameWithoutExtension(archivo.NombreArchivo) + ".json");
                contenidoArchivoJson = File.ReadAllText(nombreArchivoJson);

                objReciboDatosProcesamientoSectorRuta = null;
                objReciboDatosProcesamientoSectorRuta =
                    JsonConvert.DeserializeObject<ClsReciboDatosProcesamiento>(contenidoArchivoJson);

                objReciboDatosProcesamiento.ReciboMenorCabecera.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorCabecera);
                objReciboDatosProcesamiento.ReciboMenorConcepto.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorConcepto);
                objReciboDatosProcesamiento.ReciboMenorConsumo.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorConsumo);
                objReciboDatosProcesamiento.ReciboMenorLectura.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMenorLectura);

                objReciboDatosProcesamiento.ReciboMayorCabecera.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorCabecera);
                objReciboDatosProcesamiento.ReciboMayorConcepto.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorConcepto);
                objReciboDatosProcesamiento.ReciboMayorConsumo.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorConsumo);
                objReciboDatosProcesamiento.ReciboMayorLectura.AddRange(
                    objReciboDatosProcesamientoSectorRuta.ReciboMayorLectura);
            }

            if (cartera == "C")
            {
                clsImprimirReciboMenor objImprimirReciboMenor = GenerarDatosImprimirRecibosMenor(objReciboDatosProcesamiento);
                EstablecerValoresReciboMenor(objImprimirReciboMenor, cartera);

                try
                {
                    ProcesarYGuardarArchivoRecibosMenor(objImprimirReciboMenor, rutaArchivosServidor, nroSolicitudImpresion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                clsImprimirReciboMayor objImprimirReciboMayor = GenerarDatosImprimirRecibosMayor(objReciboDatosProcesamiento);
                EstablecerValoresReciboMayor(objImprimirReciboMayor, cartera);

                try
                {
                    ProcesarYGuardarArchivoRecibosMayor(objImprimirReciboMayor, rutaArchivosServidor, nroSolicitudImpresion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private clsImprimirReciboMenor GenerarDatosImprimirRecibosMenor(ClsReciboDatosProcesamiento objReciboDatosProcesamiento)
        {
            var objImprimirReciboMenor = new clsImprimirReciboMenor();

            objImprimirReciboMenor.ListaReciboMenorCabecera.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMenorCabecera);
            objImprimirReciboMenor.ListaReciboMenorConsumo.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMenorConsumo);
            objImprimirReciboMenor.ListaReciboMenorLectura.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMenorLectura);
            objImprimirReciboMenor.ListaReciboMenorConcepto.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMenorConcepto);

            //Actualizar Nro. de Pagina A4
            int nroPaginaA4 = 0;
            int contadorFilas = 0;
            foreach (var reciboMenor in objImprimirReciboMenor.ListaReciboMenorCabecera.Elementos)
            {
                contadorFilas++;
                if (contadorFilas % 2 == 0)
                {
                    nroPaginaA4++;
                    reciboMenor.NroPaginaA4 = nroPaginaA4;
                }
                else
                {
                    reciboMenor.NroPaginaA4 = 0;
                }
            }

            return objImprimirReciboMenor;
        }

        private void EstablecerValoresReciboMenor(clsImprimirReciboMenor objImprimirReciboMenor, string cartera)
        {
            objImprimirReciboMenor.DeviceInfo.EsVertical = false;
            objImprimirReciboMenor.DeviceInfo.Formato = "PDF";
            objImprimirReciboMenor.DeviceInfo.PageWidht = (decimal)21.5;
            objImprimirReciboMenor.DeviceInfo.PageHeight = (decimal)29.7;
            objImprimirReciboMenor.DeviceInfo.MarginTop = (decimal)0;
            objImprimirReciboMenor.DeviceInfo.MarginLeft = (decimal)0;
            objImprimirReciboMenor.DeviceInfo.MarginRight = (decimal)0;
            objImprimirReciboMenor.DeviceInfo.MarginBottom = (decimal)0;
            objImprimirReciboMenor.DeviceInfo.EsCentimetros = true;
            objImprimirReciboMenor.DeviceInfo.NroColumnas = 2;

            objImprimirReciboMenor.EsDuplicado = 0;
            objImprimirReciboMenor.EsRuta = 1;
            objImprimirReciboMenor.EsCartera = cartera;
            objImprimirReciboMenor.EsVistaPrevia = 0;
            objImprimirReciboMenor.NombreImpresora = string.Empty;
            objImprimirReciboMenor.EsImpresora = false;
            objImprimirReciboMenor.Formato = "PDF";
            objImprimirReciboMenor.Render = "PDF";
            objImprimirReciboMenor.RutaSalidaReporte = string.Empty;

            string rutaCaraAlegre = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraAlegre(Cartera.Comun);
            string rutaCaraTriste = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraTriste(Cartera.Comun);


            foreach (clsReciboMenorCabecera item in objImprimirReciboMenor.ListaReciboMenorCabecera.Elementos)
            {
                if (item.IncluirCaritas == 1)
                {
                    item.ImagenCara1 = item.IdEstrato1 == 1 ? rutaCaraAlegre : rutaCaraTriste;
                    item.ImagenCara2 = item.IdEstrato2 == 1 ? rutaCaraAlegre : rutaCaraTriste;
                    item.ImagenCara3 = item.IdEstrato3 == 1 ? rutaCaraAlegre : rutaCaraTriste;
                }
            }
        }

        private Stream ProcesarArchivoRecibosMenor(clsImprimirReciboMenor objImprimirReciboMenor, string rutaArchivosServidor,
            string nroSolicitudImpresion)
        {
            #region Codigo de referencia

            //localReport.DataSources[0] = new ReportDataSource("ReciboMenor_clsReciboMenorCabecera",
            //    objImprimirReciboMenor.ListaReciboMenorCabecera.Elementos);
            //clsImprimirReporteLocal _objreporte = clsImprimirReporteLocal.ObtenerInstancia();

            #endregion

            string rutaArchivoCreado = string.Empty;
            try
            {
                string ruta = clsUtilidadesImpresionRecibo.CrearArchivosImpresionRecibos(Cartera.Comun, 0,
                    ref rutaArchivoCreado);
            }
            catch (Exception)
            {
                throw;
            }

            var localReport = new LocalReport
            {
                ReportEmbeddedResource = "ReciboDigital.ReglaNegocio.Recibo.RptReciboMenor_Cabecera.rdlc",
                EnableExternalImages = true,
                ReportPath = rutaArchivoCreado
            };
            localReport.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorCabecera",
                objImprimirReciboMenor.ListaReciboMenorCabecera.Elementos));

            _objImprimirReciboMenor = objImprimirReciboMenor;
            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteDataReciboMenor);

            //Crear el archivo con los Recibos
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;
            string deviceInfo = objImprimirReciboMenor.DeviceInfo.DeviceInfo();
            byte[] bytestDocumentoRecibo;

            try
            {
                bytestDocumentoRecibo = localReport.Render(objImprimirReciboMenor.Formato, deviceInfo, out mimeType,
                    out encoding, out filenameExtension, out streamIds, out warnings);
            }
            catch (Exception)
            {
                throw;
            }

            _objImprimirReciboMenor = null;

            string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor, string.Concat(nroSolicitudImpresion, ".pdf"));

            try
            {
                File.WriteAllBytes(rutaYArchivoPDF, bytestDocumentoRecibo);
            }
            catch (Exception)
            {
                throw;
            }

            return new MemoryStream(bytestDocumentoRecibo);

            #region Codigo de referencia

            //_objreporte.NombreFormato = objImprimirReciboMenor.Formato;
            //_objreporte.NombreRender = objImprimirReciboMenor.Render;
            //_objreporte.RutaArchivos = objImprimirReciboMenor.RutaSalidaReporte;
            //_objreporte.NombreImpresora = objImprimirReciboMenor.NombreImpresora;

            //_objreporte.ExportaraDirectorio(localReport
            //                                , objImprimirReciboMenor.Formato
            //                                , objImprimirReciboMenor.Render
            //                                , objImprimirReciboMenor.RutaSalidaReporte
            //                                , objImprimirReciboMenor.DeviceInfo);

            //String _strarchivoxmlel = "";
            //foreach (String archivo in _listaarchivos)
            //{
            //    _strarchivoxmlel = Path.Combine(Path.GetDirectoryName(archivo)
            //                      , Path.GetFileNameWithoutExtension(archivo) + ".xml");

            //    if (File.Exists(_strarchivoxmlel))
            //        File.Delete(_strarchivoxmlel);
            //}

            #endregion
        }

        private void ProcesarYGuardarArchivoRecibosMenor(clsImprimirReciboMenor objImprimirReciboMenor, string rutaArchivosServidor,
            string nroSolicitudImpresion)
        {
            string rutaArchivoCreado = string.Empty;
            try
            {
                string ruta = clsUtilidadesImpresionRecibo.CrearArchivosImpresionRecibos(Cartera.Comun, 0,
                    ref rutaArchivoCreado);
            }
            catch (Exception)
            {
                throw;
            }

            var localReport = new LocalReport
            {
                ReportEmbeddedResource = "ReciboDigital.ReglaNegocio.Recibo.RptReciboMenor_Cabecera.rdlc",
                EnableExternalImages = true,
                ReportPath = rutaArchivoCreado
            };
            localReport.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorCabecera",
                objImprimirReciboMenor.ListaReciboMenorCabecera.Elementos));

            _objImprimirReciboMenor = objImprimirReciboMenor;
            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteDataReciboMenor);

            //Crear el archivo con los Recibos
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;
            string deviceInfo = objImprimirReciboMenor.DeviceInfo.DeviceInfo();
            byte[] bytestDocumentoRecibo;

            try
            {
                bytestDocumentoRecibo = localReport.Render(objImprimirReciboMenor.Formato, deviceInfo, out mimeType,
                    out encoding, out filenameExtension, out streamIds, out warnings);
            }
            catch (Exception)
            {
                throw;
            }

            _objImprimirReciboMenor = null;

            string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor, string.Concat(nroSolicitudImpresion, ".pdf"));

            try
            {
                File.WriteAllBytes(rutaYArchivoPDF, bytestDocumentoRecibo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SubReporteDataReciboMenor(object sender, SubreportProcessingEventArgs e)
        {
            if (e.Parameters == null || e.Parameters.Count == 0)
                return;

            var idNroServicio = Convert.ToInt32(e.Parameters[1].Values[0]);
            var periodo = Convert.ToInt32(e.Parameters[2].Values[0]);

            // Para las lecturas
            if (e.ReportPath.Contains("Importe"))
            {
                List<clsReciboMenorConcepto> oConceptos = _objImprimirReciboMenor.ListaReciboMenorConcepto.Elementos
                    .FindAll(obj => obj.IdNroServicio == idNroServicio && obj.Periodo == periodo);

                int nFilas = 24;

                #region Adicionar los registros para completar las lineas.

                int x = oConceptos.Count;
                for (int y = x; y < nFilas; y++)
                {
                    oConceptos.Add(new clsReciboMenorConcepto());
                }

                #endregion

                #region Eliminar las filas superadas.

                while (oConceptos.Count > nFilas)
                {
                    oConceptos.RemoveAt(nFilas);
                }

                #endregion

                e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_clsReciboMenorConcepto", oConceptos));
            }
            else
            {
                e.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorConsumo",
                    _objImprimirReciboMenor.ListaReciboMenorConsumo.Elementos
                    .FindAll(obj => obj.idNroServicio == idNroServicio && obj.Periodo == periodo)));

                e.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorLectura",
                    _objImprimirReciboMenor.ListaReciboMenorLectura.Elementos
                    .FindAll(obj => obj.IdNroServicio == idNroServicio && obj.Periodo == periodo)));
            }
        }

        private clsImprimirReciboMayor GenerarDatosImprimirRecibosMayor(ClsReciboDatosProcesamiento objReciboDatosProcesamiento)
        {
            var objImprimirReciboMayor = new clsImprimirReciboMayor();

            objImprimirReciboMayor.ListaReciboMayorCabecera.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMayorCabecera);
            objImprimirReciboMayor.ListaReciboMayorConsumo.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMayorConsumo);
            objImprimirReciboMayor.ListaReciboMayorLectura.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMayorLectura);
            objImprimirReciboMayor.ListaReciboMayorConcepto.Elementos.AddRange(
                objReciboDatosProcesamiento.ReciboMayorConcepto);

            return objImprimirReciboMayor;
        }

        private void EstablecerValoresReciboMayor(clsImprimirReciboMayor objImprimirReciboMayor, string cartera)
        {
            objImprimirReciboMayor.DeviceInfo.EsVertical = true;
            objImprimirReciboMayor.DeviceInfo.Formato = "PDF";
            objImprimirReciboMayor.DeviceInfo.PageWidht = (decimal)21.5;
            objImprimirReciboMayor.DeviceInfo.PageHeight = (decimal)29.7;
            objImprimirReciboMayor.DeviceInfo.MarginTop = (decimal)0;
            objImprimirReciboMayor.DeviceInfo.MarginLeft = (decimal)0;
            objImprimirReciboMayor.DeviceInfo.MarginRight = (decimal)0;
            objImprimirReciboMayor.DeviceInfo.MarginBottom = (decimal)0;
            objImprimirReciboMayor.DeviceInfo.EsCentimetros = true;
            objImprimirReciboMayor.DeviceInfo.NroColumnas = 1;

            objImprimirReciboMayor.EsDuplicado = 0;
            objImprimirReciboMayor.EsRuta = 1;
            objImprimirReciboMayor.EsCartera = cartera;
            objImprimirReciboMayor.EsVistaPrevia = 0;
            objImprimirReciboMayor.NombreImpresora = string.Empty;
            objImprimirReciboMayor.EsImpresora = false;
            objImprimirReciboMayor.Formato = "PDF";
            objImprimirReciboMayor.Render = "PDF";
            objImprimirReciboMayor.RutaSalidaReporte = string.Empty;

            string rutaCaraAlegre = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraAlegre(Cartera.Mayor);
            string rutaCaraTriste = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraTriste(Cartera.Mayor);

            foreach (clsReciboMayorCabecera item in objImprimirReciboMayor.ListaReciboMayorCabecera.Elementos)
            {
                item.ImagenSector = item.ParaCorte ? rutaCaraTriste : rutaCaraAlegre;
            }
        }

        private Stream ProcesarArchivoRecibosMayor(clsImprimirReciboMayor objImprimirReciboMayor, string rutaArchivosServidor,
            string nroSolicitudImpresion)
        {
            string rutaArchivoCreado = string.Empty;
            try
            {
                string ruta = clsUtilidadesImpresionRecibo.CrearArchivosImpresionRecibos(Cartera.Mayor, 0,
                    ref rutaArchivoCreado);
            }
            catch (Exception)
            {
                throw;
            }

            var localReport = new LocalReport
            {
                ReportEmbeddedResource = "ReciboDigital.ReglaNegocio.Recibo.RptReciboMayor_Cabecera.rdlc",
                EnableExternalImages = true,
                ReportPath = rutaArchivoCreado
            };
            localReport.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorCabecera",
                objImprimirReciboMayor.ListaReciboMayorCabecera.Elementos));

            _objImprimirReciboMayor = objImprimirReciboMayor;
            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteDataReciboMayor);

            //Crear el archivo con los Recibos
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;
            string deviceInfo = objImprimirReciboMayor.DeviceInfo.DeviceInfo();
            byte[] bytestDocumentoRecibo;

            try
            {
                bytestDocumentoRecibo = localReport.Render(objImprimirReciboMayor.Formato, deviceInfo, out mimeType,
                    out encoding, out filenameExtension, out streamIds, out warnings);
            }
            catch (Exception)
            {
                throw;
            }

            _objImprimirReciboMayor = null;

            string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor, string.Concat(nroSolicitudImpresion, ".pdf"));

            try
            {
                File.WriteAllBytes(rutaYArchivoPDF, bytestDocumentoRecibo);
            }
            catch (Exception)
            {
                throw;
            }

            return new MemoryStream(bytestDocumentoRecibo);
        }

        private void ProcesarYGuardarArchivoRecibosMayor(clsImprimirReciboMayor objImprimirReciboMayor, string rutaArchivosServidor,
            string nroSolicitudImpresion)
        {
            string rutaArchivoCreado = string.Empty;
            try
            {
                string ruta = clsUtilidadesImpresionRecibo.CrearArchivosImpresionRecibos(Cartera.Mayor, 0,
                    ref rutaArchivoCreado);
            }
            catch (Exception)
            {
                throw;
            }

            var localReport = new LocalReport
            {
                ReportEmbeddedResource = "ReciboDigital.ReglaNegocio.Recibo.RptReciboMayor_Cabecera.rdlc",
                EnableExternalImages = true,
                ReportPath = rutaArchivoCreado
            };
            localReport.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorCabecera",
                objImprimirReciboMayor.ListaReciboMayorCabecera.Elementos));

            _objImprimirReciboMayor = objImprimirReciboMayor;
            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteDataReciboMayor);

            //Crear el archivo con los Recibos
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;
            string deviceInfo = objImprimirReciboMayor.DeviceInfo.DeviceInfo();
            byte[] bytestDocumentoRecibo;

            try
            {
                bytestDocumentoRecibo = localReport.Render(objImprimirReciboMayor.Formato, deviceInfo, out mimeType,
                    out encoding, out filenameExtension, out streamIds, out warnings);
            }
            catch (Exception)
            {
                throw;
            }

            _objImprimirReciboMayor = null;

            string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor, string.Concat(nroSolicitudImpresion, ".pdf"));

            try
            {
                File.WriteAllBytes(rutaYArchivoPDF, bytestDocumentoRecibo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SubReporteDataReciboMayor(object sender, SubreportProcessingEventArgs e)
        {
            if (e.Parameters == null || e.Parameters.Count == 0)
                return;

            var idNroServicio = Convert.ToInt32(e.Parameters[1].Values[0]);
            var periodo = Convert.ToInt32(e.Parameters[2].Values[0]);

            //oLectura.IdNroServicio == IdNroServicio && oLectura.Periodo == Periodo
            e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorLectura",
                _objImprimirReciboMayor.ListaReciboMayorLectura.Elementos.FindAll(
                    obj => obj.IdNroServicio == idNroServicio && obj.Periodo == periodo)));

            //oConsumo.idNroServicio == IdNroServicio && oConsumo.Periodo == Periodo;
            e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorConsumo",
                _objImprimirReciboMayor.ListaReciboMayorConsumo.Elementos.FindAll(
                    obj => obj.idNroServicio == idNroServicio && obj.Periodo == periodo)));

            //oConcepto.IdNroServicio == IdNroServicio && oConcepto.Periodo == Periodo
            List<clsReciboMayorConcepto> oConceptos = _objImprimirReciboMayor.ListaReciboMayorConcepto.Elementos
                .FindAll(obj => obj.IdNroServicio == idNroServicio && obj.Periodo == periodo);

            short intFilasLargas = 0;

            for (int i = 0; i < oConceptos.Count; i++)
            {
                if (oConceptos[i].NombreConcepto.Length > 42)
                {
                    intFilasLargas++;
                }
            }

            int nFilas = 41 - intFilasLargas;

            #region Adicionar los registros para completar las lineas.

            int x = oConceptos.Count;
            for (int y = x; y < nFilas; y++)
            {
                oConceptos.Add(new clsReciboMayorConcepto());
            }

            #endregion

            #region Eliminar las filas superadas.

            while (oConceptos.Count > nFilas)
            {
                oConceptos.RemoveAt(nFilas);
            }

            #endregion

            e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorConcepto",
                oConceptos));
        }

        //CanviaDev_2020-05-27
        private string ObtenerValorParametroConfiguracion(short idEmpresa, short idParametro)
        {
            clsResultado resultadoParametro = ObtenerConfiguracion(new clsParametro
            {
                Parametros = new Dictionary<string, string>
                    {
                        { "IdEmpresa", idEmpresa.ToString() },
                        { "IdParametro", idParametro.ToString() }
                    }
            });

            if (resultadoParametro.Datos == null ||
                ((System.Data.DataTable)resultadoParametro.Datos).Rows.Count == 0 ||
                 ((System.Data.DataTable)resultadoParametro.Datos).Rows[0]["ValorParametro"] == null ||
                 string.IsNullOrWhiteSpace(((System.Data.DataTable)resultadoParametro.Datos).Rows[0]["ValorParametro"].ToString()))
            {
                return string.Empty;
            }
            else
            {
                return ((System.Data.DataTable)resultadoParametro.Datos).Rows[0]["ValorParametro"].ToString();
            }
        }
        //CanviaDev_2020-05-27:Fin

        #endregion

        #endregion

        #region Public

        public clsResultado ObtenerListaSolicitudImpresionRecibos(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerListaSolicitudImpresionRecibos(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ConsultarSuministrosNotificar(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ConsultarSuministrosNotificar(entidad);

                var result = new clsResultado
                {
                    Datos = table,
                    Mensaje = string.Concat(entidad.Parametros["totalregistros"], "/", entidad.Parametros["totalpaginas"])
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }//ConsultarMonitoreoTareasLista

        public clsResultado ConsultarMonitoreoTareasLista(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ConsultarMonitoreoTareasLista(entidad);

                var result = new clsResultado
                {
                    Datos = table,
                    Mensaje = entidad.Parametros["totalregistros"] + "/" + entidad.Parametros["totalpaginas"]
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }
        //ConsultarLogProceso
        public clsResultado ConsultarLogProceso(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ConsultarLogProceso(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }
        //ConsultarLogTareaEjecucion
        public clsResultado ConsultarLogTareaEjecucion(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ConsultarLogTareaEjecucion(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };
                //
                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }
        //ConsultarLogInconsistencias
        public clsResultado ConsultarLogInconsistencias(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ConsultarLogInconsistencias(entidad);

                //Si la consulta en el Log de Inconsistencias no devuelve registros, realizar 
                //la consulta en el Log del Proceso
                if (table.Rows.Count == 0)
                {
                    var tableAdicinal = _SolicImpresionReciboDAO.ConsultarLogProceso(entidad);

                    System.Data.DataView dtViewTemp = new System.Data.DataView(tableAdicinal,
                        "Operacion='ErrorProceso'",
                        "Fecha DESC",
                        System.Data.DataViewRowState.CurrentRows);
                    tableAdicinal = dtViewTemp.ToTable();

                    foreach (System.Data.DataRow rowAdicional in tableAdicinal.Rows)
                    {
                        var newRow = table.NewRow();
                        newRow["IdTarea"] = rowAdicional["IdTarea"];
                        newRow["FechaRegistro"] = rowAdicional["Fecha"];
                        newRow["Mensaje"] = rowAdicional["Descripcion"];

                        table.Rows.Add(newRow);
                    }
                }

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado AyudaLista(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.AyudaLista(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }


        public clsResultado ObtenerSolicitudImpresionRecibo(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerDetalleSolicitudImpresionRecibo(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerDetalleSolicitudImpresionRecibo(entidad);

                var result = new clsResultado();
                result.Datos = table;
                result.Mensaje = entidad.Parametros["totalregistros"] + "/" + entidad.Parametros["totalpaginas"];

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ActualizarEstado(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ActualizarEstado(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerAlcancePersonalProveedorActividad(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerAlcancePersonalProveedorActividad(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public clsResultado ObtenerListaSolicitudImpresionRecibosPaginada(clsParametro entidad)
        {
            try
            {
                //CanviaDev_2020-05-27
                //var table = _SolicImpresionReciboDAO.ObtenerListaSolicitudImpresionRecibosPaginada(entidad);
                var table = _SolicImpresionReciboDAO.ObtenerSolicitudesImpresionPaginadasDeTemporal(entidad);
                //CanviaDev_2020-05-27:Fin

                var result = new clsResultado
                {
                    Datos = table,
                    Mensaje = entidad.Parametros["totalregistros"] + "/" + entidad.Parametros["totalpaginas"]
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ProcesarArchivoImpresion(clsParametro entidad)
        {
            var clsTareaProgramacion = new ClsTareaProgramacion
            {
                IdProceso = (int)ProcesoNGEnum.GenerarArchivoImpresionRecibos,
                IdUsuarioGenerador = Convert.ToInt32(entidad.Parametros["IdUsuario"]),
                IdUUNN = 0 //CanviaDev_2020-05-27
            };

            try
            {
                //Obtener datos para creacion de tarea de procesamiento de archivo de impresion
                var table = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);

                clsTareaProgramacion.IdEmpresa = Convert.ToInt16(table.Rows[0]["IdEmpresa"].ToString());
                //CanviaDev_2020-05-27
                //Evaluar si se envia la UUNN para la creacion de la Tarea
                string valParamNivelTarea = ObtenerValorParametroConfiguracion(clsTareaProgramacion.IdEmpresa,
                    (short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.NivelRegistroTarea);
                if (valParamNivelTarea == string.Empty)
                {
                    return new clsResultado
                    {
                        IdError = 99999,
                        Mensaje = "No existe parametro de configuracion de Nivel de Registro de Tarea (Empresa o Unidad de Negocio)."
                    };
                }
                else if (valParamNivelTarea == "2") // Nivel de UUNN
                {
                    if (string.IsNullOrWhiteSpace(entidad.Parametros["IdUUNN"]) ||
                        entidad.Parametros["IdUUNN"].Trim() == "0")
                    {
                        clsTareaProgramacion.IdUUNN = Convert.ToInt16(table.Rows[0]["IdUUNN"].ToString());
                    }
                    else
                    {
                        clsTareaProgramacion.IdUUNN = Convert.ToInt16(entidad.Parametros["IdUUNN"]);
                    }
                }
                //CanviaDev_2020-05-27:Fin

                clsParametroImprimirRecibos clsParametroImprimirRecibos =
                    ObtenerParametrosTareaProcesarArchivoImpresion(entidad, table);

                #region Codigo de referencia

                //clsParametroImprimirRecibos = ucImpresionReciboRutas1.DevolverParametrosTarea();
                //_escartera = cboCarteraSuministro.Valor.ToString();
                //clsParametroImprimirRecibos.IdUUNNPadre = ucEmpresaUNegocioCCSS1.IdUNegocio;
                //clsParametroImprimirRecibos.IdCentroServicioPadre = ucEmpresaUNegocioCCSS1.IdCServicio;
                //clsParametroImprimirRecibos.EsDuplicado = _esduplicado; //Se deja por defecto
                //_intperiodo = clsParametroImprimirRecibos.Periodo;
                //_objparametro.EsEnviarCorreo = this._esEnvioCorreo; //Se deja por defecto

                #endregion

                clsTareaProgramacion.ParametroTarea = clsParametroImprimirRecibos;

                //Grabar tarea
                int idTarea = _NegocioTarea.GrabarTareaInmediata(clsTareaProgramacion);

                //Actualizar relacion entre Solicitud de impresion y tarea
                entidad.Parametros.Add("IdTarea", idTarea.ToString());
                entidad.Parametros.Add("IdTipoTarea",
                    ((int)ClsConstantesImpresionRecibos.TipoTarea.ProcesamientoArchivoimpresion).ToString());
                entidad.Parametros.Add("IdUsuarioGenerador", entidad.Parametros["IdUsuario"]);
                _SolicImpresionReciboDAO.InsertarSolicitudImpresionTarea(entidad);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }

            return new clsResultado();
        }

        public clsResultado NotificarClientes(clsParametro entidad)
        {
            int idProceso = (int)ProcesoNGEnum.NotificarEmisionRecibosClientes;

            try
            {
                bool esValidarNotificacionPendiente = true;
                if (entidad.Parametros.ContainsKey("validarNotificacionPendiente"))
                {
                    esValidarNotificacionPendiente = bool.Parse(entidad.Parametros["validarNotificacionPendiente"]);
                }

                bool esNotificacionPendiente = false;
                if (esValidarNotificacionPendiente)
                {
                    esNotificacionPendiente =
                        _SolicImpresionReciboDAO.EsExisteSolicitudImpresionNotificacionPendiente(entidad);
                }

                if (!esNotificacionPendiente)
                {
                    //CanviaDev_2020-05-27
                    bool esProcNotifEmailActivo = false;
                    bool esProcNotifSMSActivo = false;
                    bool esProcNotifPushActivo = false;

                    string valParamProcsDisponibles = ObtenerValorParametroConfiguracion(
                        Convert.ToInt16(entidad.Parametros["IdEmpresa"]),
                        (short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.ProcesosDisponiblesNotificacion);

                    if (!string.IsNullOrWhiteSpace(valParamProcsDisponibles))
                    {
                        var objProcsDisponibles = JsonConvert.DeserializeObject<dynamic>(valParamProcsDisponibles);
                        foreach (var objProc in objProcsDisponibles.procs)
                        {
                            if (objProc.codProc == "PROC_EMAIL")
                            {
                                esProcNotifEmailActivo = objProc.selected;
                            }
                            else if (objProc.codProc == "PROC_SMS")
                            {
                                esProcNotifSMSActivo = objProc.selected;
                            }
                            else if (objProc.codProc == "PROC_PUSH")
                            {
                                esProcNotifPushActivo = objProc.selected;
                            }
                        }
                    }

                    if (!esProcNotifEmailActivo && !esProcNotifSMSActivo && !esProcNotifPushActivo)
                    {
                        return _Util.SetError<clsResultado>("No hay ningun proceso de Notificacion activo.", 99999);
                    }

                    clsResultado resultadoRegNotif;
                    if (esProcNotifEmailActivo)
                    {
                        resultadoRegNotif = NotificarClientes(entidad, idProceso, TipoNotificacionEnum.NotificacionCorreo);
                        if (resultadoRegNotif.IdError != 0)
                        {
                            return resultadoRegNotif;
                        }
                    }
                    if (esProcNotifSMSActivo)
                    {
                        resultadoRegNotif = NotificarClientes(entidad, idProceso, TipoNotificacionEnum.NotificacionSMS);
                        if (resultadoRegNotif.IdError != 0)
                        {
                            return resultadoRegNotif;
                        }
                    }
                    if (esProcNotifPushActivo)
                    {
                        resultadoRegNotif = NotificarClientes(entidad, idProceso, TipoNotificacionEnum.NotificacionPush);
                        if (resultadoRegNotif.IdError != 0)
                        {
                            return resultadoRegNotif;
                        }
                    }
                    //CanviaDev_2020-05-27:Fin
                }
                else
                {
                    return _Util.SetError<clsResultado>(string.Concat(
                        "Ya se ha iniciado un proceso de notificación, por favor espere a que este ",
                        "termine para iniciar un nuevo proceso."), 99999);
                }
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }

            return new clsResultado();
        }

        public clsResultado ValidarNotificarClientes(clsParametro entidad)
        {
            string idNroServicioXml = string.Empty;
            if (!string.IsNullOrWhiteSpace(entidad.Parametros["ListaIdsSuministros"]))
            {
                string[] arrayIdNroServicio = (from p in entidad.Parametros["ListaIdsSuministros"].Split(',')
                                               select string.Format("<nroservicio id=\"{0}\" />", p)).ToArray();

                idNroServicioXml = string.Concat(
                    "<nrosservicio>",
                    string.Join(string.Empty, arrayIdNroServicio),
                    "</nrosservicio>");
            }
            entidad.Parametros.Add("xmlidnroservicio", idNroServicioXml);

            try
            {
                bool esNotificacionPendiente =
                    _SolicImpresionReciboDAO.EsExisteSuministrosNotificacionPendiente(entidad);

                if (!esNotificacionPendiente)
                {
                    return new clsResultado();
                }
                else
                {
                    return _Util.SetError<clsResultado>(string.Concat(
                        "Ya se ha iniciado un proceso de notificación",
                        string.IsNullOrWhiteSpace(entidad.Parametros["ListaIdsSuministros"]) ?
                            string.Empty :
                            string.Concat(" para los Suministros: ",
                                entidad.Parametros["ListaIdsSuministros"]),
                        ". Por favor espere a que este termine para iniciar un nuevo proceso."), 99999);
                }
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerConfiguracion(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerConfiguracion(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado GuardarConfiguracion(clsParametro entidad)
        {
            short idEmpresa = Convert.ToInt16(entidad.Parametros["IdEmpresa"]);
            var oParametroBD = new clsParametro
            {
                Parametros = new Dictionary<string, string>
                {
                    { "IdParametroImpresionRecibo", decimal.Zero.ToString() },
                    { "IdEmpresa", idEmpresa.ToString() },
                    { "IdParametro", "" },
                    { "NombreParametro", "" },
                    { "ValorParametro", "" }
                }
            };

            try
            {
                //Guardar configuracion de Numeracion de Impresion
                oParametroBD.Parametros["IdParametro"] =
                    ((short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.NumeracionImpresion).ToString();
                oParametroBD.Parametros["NombreParametro"] = ClsConstantesImpresionRecibos.ConstNomParamNumeracionImpresion;
                oParametroBD.Parametros["ValorParametro"] =
                    entidad.Parametros[ClsConstantesImpresionRecibos.ConstNomParamNumeracionImpresion];
                _SolicImpresionReciboDAO.GuardarConfiguracion(oParametroBD);

                //Guardar configuracion de Aplicaciones para Notificaciones Push
                oParametroBD.Parametros["IdParametro"] =
                    ((short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.AplicacionesNotificacionPush).ToString();
                oParametroBD.Parametros["NombreParametro"] = ClsConstantesImpresionRecibos.ConstNomParamAplicacNotifPush;
                oParametroBD.Parametros["ValorParametro"] =
                    entidad.Parametros[ClsConstantesImpresionRecibos.ConstNomParamAplicacNotifPush];
                _SolicImpresionReciboDAO.GuardarConfiguracion(oParametroBD);

                //Guardar configuracion de Procesos disponibles para Notificaciones
                oParametroBD.Parametros["IdParametro"] =
                    ((short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.ProcesosDisponiblesNotificacion).ToString();
                oParametroBD.Parametros["NombreParametro"] = ClsConstantesImpresionRecibos.ConstNomParamProcsDisponiblesNotif;
                oParametroBD.Parametros["ValorParametro"] =
                    entidad.Parametros[ClsConstantesImpresionRecibos.ConstNomParamProcsDisponiblesNotif];
                _SolicImpresionReciboDAO.GuardarConfiguracion(oParametroBD);

                //Guardar configuracion de Cantidad de Filas para paginacion
                oParametroBD.Parametros["IdParametro"] =
                    ((short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.CantidadFilasPaginacion).ToString();
                oParametroBD.Parametros["NombreParametro"] = ClsConstantesImpresionRecibos.ConstNomParamCantFilasPaginacion;
                oParametroBD.Parametros["ValorParametro"] =
                    entidad.Parametros[ClsConstantesImpresionRecibos.ConstNomParamCantFilasPaginacion];
                _SolicImpresionReciboDAO.GuardarConfiguracion(oParametroBD);

                //Guardar configuracion de Intervalo de Tiempo de Consulta de Log de tarea
                oParametroBD.Parametros["IdParametro"] =
                    ((short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.TiempoConsultaLogTarea).ToString();
                oParametroBD.Parametros["NombreParametro"] = ClsConstantesImpresionRecibos.ConstNomParamTiempoConsLogTarea;
                oParametroBD.Parametros["ValorParametro"] =
                    entidad.Parametros[ClsConstantesImpresionRecibos.ConstNomParamTiempoConsLogTarea];
                _SolicImpresionReciboDAO.GuardarConfiguracion(oParametroBD);

                //CanviaDev_2020-05-27

                //Guardar configuracion de Ruta base de Archivos PDF de Recibos
                oParametroBD.Parametros["IdParametro"] =
                    ((short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.RutaBaseArchivosPDFRecibos).ToString();
                oParametroBD.Parametros["NombreParametro"] = ClsConstantesImpresionRecibos.ConstNomParamRutaBaseArchPDFRecibos;
                oParametroBD.Parametros["ValorParametro"] =
                    entidad.Parametros[ClsConstantesImpresionRecibos.ConstNomParamRutaBaseArchPDFRecibos];
                _SolicImpresionReciboDAO.GuardarConfiguracion(oParametroBD);

                //Guardar configuracion de Numeracion de Impresion
                oParametroBD.Parametros["IdParametro"] =
                    ((short)ClsConstantesImpresionRecibos.ParametroImpresionReciboEnum.NivelRegistroTarea).ToString();
                oParametroBD.Parametros["NombreParametro"] = ClsConstantesImpresionRecibos.ConstNomParamNivelRegistroTarea;
                oParametroBD.Parametros["ValorParametro"] =
                    entidad.Parametros[ClsConstantesImpresionRecibos.ConstNomParamNivelRegistroTarea];
                _SolicImpresionReciboDAO.GuardarConfiguracion(oParametroBD);
                //CanviaDev_2020-05-27:Fin

                var result = new clsResultado
                {
                    Datos = null
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerListaAplicacionNotificacion(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerListaAplicacionNotificacion(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerListaProcesoNotificacion(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerListaProcesoNotificacion(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public async Task<Stream> ObtenerPdfSolicitudImpresionAsync(clsParametro entidad)
        {
            try
            {
                #region Obtener datos de Solicitud de impresion

                var dtSolicitudImpresion = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);
                if (dtSolicitudImpresion.Rows.Count == 0)
                {
                    throw new Exception("Error al obtener la información de la Solicitud de Impresión.");
                }

                short idEmpresa = (short)dtSolicitudImpresion.Rows[0]["IdEmpresa"];
                int periodo = (int)dtSolicitudImpresion.Rows[0]["Periodo"];
                string nroSolicitudImpresion = dtSolicitudImpresion.Rows[0]["NroSolicitud"].ToString();
                string cartera = dtSolicitudImpresion.Rows[0]["Cartera"].ToString();

                ClsEmpresa objEmpresa = ClsNegocioOrganizacion.Instancia.ObtenerRegistroEmpresa(idEmpresa);
                string rutaArchivosServidor = Path.Combine(
                    ConfigurationManager.AppSettings["rutaArchivosServidor"],
                    string.Concat(objEmpresa.AbreviaEmpresa, "\\", periodo, "\\", nroSolicitudImpresion));

                //Si ya se genero anteriormente el Archivo PDF de recibos, devolverlo directamente
                string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor,
                    string.Concat(nroSolicitudImpresion, ".pdf"));
                if (File.Exists(rutaYArchivoPDF))
                {
                    var fileStream = new FileStream(rutaYArchivoPDF, FileMode.Open, FileAccess.Read); ;

                    return fileStream;
                }

                int idTarea = _SolicImpresionReciboDAO.ObtenerIdTareaSolicitudImpresion(entidad,
                    (int)ProcesoNGEnum.GenerarArchivoRecibos);

                #region Codigo de referencia

                //short idUUNN = (short)dtSolicitudImpresion.Rows[0]["IdUUNN"];
                //short idCentroServicio = (short)dtSolicitudImpresion.Rows[0]["IdCentroServicio"];

                #endregion

                #endregion

                #region Descargar archivos de impresion

                var listaControlGenerarArchivo = _SolicImpresionReciboDAO.ObtenerListaArchivosRecibos(idTarea);
                listaControlGenerarArchivo.Elementos.Sort((x, y) => x.FechaGenera.CompareTo(y.FechaGenera));

                await DescargarArchivosImpresionReciboAsync(listaControlGenerarArchivo, rutaArchivosServidor);

                #endregion

                #region Descomprimir archivos de impresion descargados

                await DescomprimirArchivosImpresionReciboAsync(listaControlGenerarArchivo, rutaArchivosServidor);

                #endregion

                #region Generar Archivos de recibos en PDF

                return GenerarArchivoPDFRecibos(listaControlGenerarArchivo, rutaArchivosServidor,
                    cartera, nroSolicitudImpresion);

                #endregion
            }
            catch (Exception)
            {
                //El valor Null le indica al Cliente que se produjo un error
                return null;
            }
        }

        public async Task<clsResultado> ProcesarDescargaDatosPdfSolicitudImpresionAsync(
            clsParametro entidad)
        {
            try
            {
                #region Obtener datos de Solicitud de impresion

                var dtSolicitudImpresion = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);
                if (dtSolicitudImpresion.Rows.Count == 0)
                {
                    throw new Exception("Error al obtener la información de la Solicitud de Impresión.");
                }

                short idEmpresa = (short)dtSolicitudImpresion.Rows[0]["IdEmpresa"];
                int periodo = (int)dtSolicitudImpresion.Rows[0]["Periodo"];
                string nroSolicitudImpresion = dtSolicitudImpresion.Rows[0]["NroSolicitud"].ToString();

                ClsEmpresa objEmpresa = ClsNegocioOrganizacion.Instancia.ObtenerRegistroEmpresa(idEmpresa);
                string rutaArchivosServidor = Path.Combine(
                    ConfigurationManager.AppSettings["rutaArchivosServidor"],
                    string.Concat(objEmpresa.AbreviaEmpresa, "\\", periodo, "\\", nroSolicitudImpresion));

                //Si ya se genero anteriormente el Archivo PDF de recibos, no realizar el procesamiento
                string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor,
                    string.Concat(nroSolicitudImpresion, ".pdf"));
                if (File.Exists(rutaYArchivoPDF))
                {
                    return new clsResultado();
                }

                int idTarea = _SolicImpresionReciboDAO.ObtenerIdTareaSolicitudImpresion(entidad,
                    (int)ProcesoNGEnum.GenerarArchivoRecibos);

                #endregion

                #region Descargar archivos de impresion

                var listaControlGenerarArchivo = _SolicImpresionReciboDAO.ObtenerListaArchivosRecibos(idTarea);
                listaControlGenerarArchivo.Elementos.Sort((x, y) => x.FechaGenera.CompareTo(y.FechaGenera));

                await DescargarArchivosImpresionReciboAsync(listaControlGenerarArchivo, rutaArchivosServidor);

                #endregion

                #region Descomprimir archivos de impresion descargados

                await DescomprimirArchivosImpresionReciboAsync(listaControlGenerarArchivo, rutaArchivosServidor);

                #endregion
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }

            return new clsResultado();
        }

        public clsResultado GenerarArchivoPdfSolicitudImpresion(clsParametro entidad)
        {
            try
            {
                #region Obtener datos de Solicitud de impresion

                var dtSolicitudImpresion = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);
                if (dtSolicitudImpresion.Rows.Count == 0)
                {
                    throw new Exception("Error al obtener la información de la Solicitud de Impresión.");
                }

                short idEmpresa = (short)dtSolicitudImpresion.Rows[0]["IdEmpresa"];
                int periodo = (int)dtSolicitudImpresion.Rows[0]["Periodo"];
                string nroSolicitudImpresion = dtSolicitudImpresion.Rows[0]["NroSolicitud"].ToString();
                string cartera = dtSolicitudImpresion.Rows[0]["Cartera"].ToString();

                ClsEmpresa objEmpresa = ClsNegocioOrganizacion.Instancia.ObtenerRegistroEmpresa(idEmpresa);
                string rutaArchivosServidor = Path.Combine(
                    ConfigurationManager.AppSettings["rutaArchivosServidor"],
                    string.Concat(objEmpresa.AbreviaEmpresa, "\\", periodo, "\\", nroSolicitudImpresion));

                //Si ya se genero anteriormente el Archivo PDF de recibos, no realizar el procesamiento
                string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor,
                    string.Concat(nroSolicitudImpresion, ".pdf"));
                if (File.Exists(rutaYArchivoPDF))
                {
                    return new clsResultado();
                }

                int idTarea = _SolicImpresionReciboDAO.ObtenerIdTareaSolicitudImpresion(entidad,
                    (int)ProcesoNGEnum.GenerarArchivoRecibos);

                #endregion

                #region Obtener lista de archivos de datos generados

                var listaControlGenerarArchivo = _SolicImpresionReciboDAO.ObtenerListaArchivosRecibos(idTarea);
                listaControlGenerarArchivo.Elementos.Sort((x, y) => x.FechaGenera.CompareTo(y.FechaGenera));

                #endregion

                #region Generar Archivos de recibos en PDF

                GenerarYGuardarArchivoPDFRecibos(listaControlGenerarArchivo, rutaArchivosServidor,
                    cartera, nroSolicitudImpresion);

                #endregion
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }

            return new clsResultado();
        }

        public Stream DescargarArchivoPdfSolicitudImpresion(clsParametro entidad)
        {
            try
            {
                #region Obtener datos de Solicitud de impresion

                var dtSolicitudImpresion = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);
                if (dtSolicitudImpresion.Rows.Count == 0)
                {
                    throw new Exception("Error al obtener la información de la Solicitud de Impresión.");
                }

                short idEmpresa = (short)dtSolicitudImpresion.Rows[0]["IdEmpresa"];
                int periodo = (int)dtSolicitudImpresion.Rows[0]["Periodo"];
                string nroSolicitudImpresion = dtSolicitudImpresion.Rows[0]["NroSolicitud"].ToString();

                ClsEmpresa objEmpresa = ClsNegocioOrganizacion.Instancia.ObtenerRegistroEmpresa(idEmpresa);
                string rutaArchivosServidor = Path.Combine(
                    ConfigurationManager.AppSettings["rutaArchivosServidor"],
                    string.Concat(objEmpresa.AbreviaEmpresa, "\\", periodo, "\\", nroSolicitudImpresion));

                #endregion

                #region Retornar archivo PDF

                string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor,
                    string.Concat(nroSolicitudImpresion, ".pdf"));
                if (File.Exists(rutaYArchivoPDF))
                {
                    var fileStream = new FileStream(rutaYArchivoPDF, FileMode.Open, FileAccess.Read); ;

                    return fileStream;
                }
                else
                {
                    //El valor Null le indica al Cliente que no se pudo obtener el archivo
                    return null;
                }

                #endregion
            }
            catch (Exception)
            {
                //El valor Null le indica al Cliente que se produjo un error
                return null;
            }
        }

        public clsResultado CopiarPdfSolicitudImpresion(clsParametro entidad)
        {
            try
            {
                #region Obtener datos de Solicitud de impresion

                var dtSolicitudImpresion = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);
                if (dtSolicitudImpresion.Rows.Count == 0)
                {
                    throw new Exception("Error al obtener la información de la Solicitud de Impresión.");
                }

                short idEmpresa = (short)dtSolicitudImpresion.Rows[0]["IdEmpresa"];
                int periodo = (int)dtSolicitudImpresion.Rows[0]["Periodo"];
                string nroSolicitudImpresion = dtSolicitudImpresion.Rows[0]["NroSolicitud"].ToString();
                short idUnidadNegocio = (short)dtSolicitudImpresion.Rows[0]["IdUUNN"];
                short idCentroServicio = (short)dtSolicitudImpresion.Rows[0]["IdCentroServicio"];

                ClsEmpresa objEmpresa = ClsNegocioOrganizacion.Instancia.ObtenerRegistroEmpresa(idEmpresa);
                string rutaArchivosServidor = Path.Combine(
                    ConfigurationManager.AppSettings["rutaArchivosServidor"],
                    string.Concat(objEmpresa.AbreviaEmpresa, "\\", periodo, "\\", nroSolicitudImpresion));

                #endregion

                #region Copiar archivo desde Carpeta de intercambio de NGC a Carpeta del Servidor de aplicaciones

                if (!Directory.Exists(rutaArchivosServidor))
                {
                    Directory.CreateDirectory(rutaArchivosServidor);
                }

                string rutaArchivoImpresion = ObtenerRutaArchivoImpresion(idUnidadNegocio, idCentroServicio, periodo);
                string rutaArchivoOrigen = Path.Combine(rutaArchivoImpresion,
                    string.Concat(nroSolicitudImpresion, ".pdf"));
                string rutaYArchivoPDF = Path.Combine(rutaArchivosServidor,
                    string.Concat(nroSolicitudImpresion, ".pdf"));

                File.Copy(rutaArchivoOrigen, rutaYArchivoPDF, true);

                #endregion

                #region Proteger archivo con Contraseña y aplicar permisos de bloqueo de edicion, copia de contenido, etc

                string rutaYArchivoPDFEncriptado = Path.Combine(rutaArchivosServidor,
                    string.Concat(nroSolicitudImpresion, "_secured.pdf"));
                string ownerPassword = Guid.NewGuid().ToString(); //Password autogenerado

                ////El Password del archivo PDF es el RUC del Proveedor de impresion
                //var dtProveedor = _SolicImpresionReciboDAO.ObtenerDatosProveedorPorIdUsuario(entidad);
                //if (dtProveedor.Rows.Count == 0)
                //{
                //    throw new Exception("Error al obtener la información del Proveedor de Impresión.");
                //}
                //string userPassword = dtProveedor.Rows[0]["NumeroIdentidad"].ToString();
                string userPassword = Guid.NewGuid().ToString(); //Password autogenerado;

                clsUtil.CreateEncryptedPdfFile(rutaYArchivoPDF, rutaYArchivoPDFEncriptado, userPassword,
                    ownerPassword, true);

                entidad.Parametros.Add("PasswordArchivoRecibos", userPassword);

                _SolicImpresionReciboDAO.ActualizarPasswordArchivoRecibos(entidad);

                #endregion

                var result = new clsResultado
                {
                    Datos = null
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public Stream DescargarArchivoPdfEncriptadoSolicitudImpresion(clsParametro entidad)
        {
            try
            {
                #region Obtener datos de Solicitud de impresion

                var dtSolicitudImpresion = _SolicImpresionReciboDAO.ObtenerSolicitudImpresionRecibo(entidad);
                if (dtSolicitudImpresion.Rows.Count == 0)
                {
                    throw new Exception("Error al obtener la información de la Solicitud de Impresión.");
                }

                short idEmpresa = (short)dtSolicitudImpresion.Rows[0]["IdEmpresa"];
                int periodo = (int)dtSolicitudImpresion.Rows[0]["Periodo"];
                string nroSolicitudImpresion = dtSolicitudImpresion.Rows[0]["NroSolicitud"].ToString();

                ClsEmpresa objEmpresa = ClsNegocioOrganizacion.Instancia.ObtenerRegistroEmpresa(idEmpresa);
                string rutaArchivosServidor = Path.Combine(
                    ConfigurationManager.AppSettings["rutaArchivosServidor"],
                    string.Concat(objEmpresa.AbreviaEmpresa, "\\", periodo, "\\", nroSolicitudImpresion));

                #endregion

                #region Retornar archivo PDF Encriptado

                string rutaYArchivoPDFEncriptado = Path.Combine(rutaArchivosServidor,
                    string.Concat(nroSolicitudImpresion, "_secured.pdf"));
                if (File.Exists(rutaYArchivoPDFEncriptado))
                {
                    var fileStream = new FileStream(rutaYArchivoPDFEncriptado, FileMode.Open, FileAccess.Read); ;

                    return fileStream;
                }
                else
                {
                    //El valor Null le indica al Cliente que no se pudo obtener el archivo
                    return null;
                }

                #endregion
            }
            catch (Exception)
            {
                //El valor Null le indica al Cliente que se produjo un error
                return null;
            }
        }

        public clsResultado ObtenerDatosEjecucionTareaPorId(clsParametro entidad)
        {
            try
            {
                var table = _SolicImpresionReciboDAO.ObtenerDatosEjecucionTareaPorId(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ReenviarCorreoProcesamientoArchivoImpresion(clsParametro entidad)
        {
            try
            {
                _SolicImpresionReciboDAO.ReenviarCorreoProcesamientoArchivoImpresion(entidad);
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }

            return new clsResultado();
        }

        //CanviaDev_2020-05-27
        public clsResultado GenerarArchivosPDFRecibos(clsParametro entidad)
        {
            int idProceso = (int)ProcesoNGEnum.GenerarArchivosPDFRecibosSolicitudImpresion;

            try
            {
                bool esValidarNotificacionPendiente = true;
                if (entidad.Parametros.ContainsKey("validarNotificacionPendiente"))
                {
                    esValidarNotificacionPendiente = bool.Parse(entidad.Parametros["validarNotificacionPendiente"]);
                }

                bool esNotificacionPendiente = false;
                if (esValidarNotificacionPendiente)
                {
                    esNotificacionPendiente =
                        _SolicImpresionReciboDAO.EsExisteSolicitudImpresionNotificacionPendiente(entidad);
                }

                if (!esNotificacionPendiente)
                {
                    clsResultado resultadoRegNotif =
                        NotificarClientes(entidad, idProceso, TipoNotificacionEnum.GeneracionArchivosPDF);
                    if (resultadoRegNotif.IdError != 0)
                    {
                        return resultadoRegNotif;
                    }
                }
                else
                {
                    return _Util.SetError<clsResultado>(string.Concat(
                        "Ya se ha iniciado un proceso de notificacion o generacion de archivos PDF, por favor espere a que este ",
                        "termine para iniciar un nuevo proceso."), 99999);
                }
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }

            return new clsResultado();
        }
        //CanviaDev_2020-05-27: Fin

        //CanviaDev_2020-06-01
        public clsResultado ValidarSuministrosImportarNotificacion(clsParametro entidad)
        {
            string[] arrayIdNroServicio = (from p in entidad.Parametros["ListaIdNroServicioXml"].Split(',')
                                           select string.Format("<nroservicio id=\"{0}\" />", p)).ToArray();
            string idNroServicioXml = string.Concat(
                "<nrosservicio>",
                string.Join(string.Empty, arrayIdNroServicio),
                "</nrosservicio>");

            entidad.Parametros["ListaIdNroServicioXml"] = idNroServicioXml;

            try
            {
                var table = _SolicImpresionReciboDAO.ValidarSuministrosImportarNotificacion(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }
        //CanviaDev_2020-06-01

        #endregion

        #endregion Public
    }

    #endregion clsNegocioSolicitudImpresionRecibo


}
