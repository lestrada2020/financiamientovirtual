﻿using Microsoft.Reporting.WebForms;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Maestro;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using static SISAC.Util.Tool.clsUtil;

namespace SISAC.Negocio.Facturacion
{


    /// <summary>
    /// Controlador que genera la clase a gestionar la digitalización de los recibos.
    /// </summary>
    //public class clsGestionarReciboDigital
    //{
    //    clsParametroImprimirRecibos _parametroImprimirRecibo;
    //    String _RutaDestino;
    //    clsSuministrosSectorRecibo _objsector;

    //    public clsGestionarReciboDigital(clsParametroImprimirRecibos parametroImprimirRecibo, String RutaDestino)
    //    {
    //        _RutaDestino = RutaDestino;
    //        _parametroImprimirRecibo = parametroImprimirRecibo;

    //    }

    //    public clsGestionarReciboDigital(clsParametroImprimirRecibos parametroImprimirRecibo, String RutaDestino, clsSuministrosSectorRecibo objsector)
    //    {
    //        _RutaDestino = RutaDestino;
    //        _parametroImprimirRecibo = parametroImprimirRecibo;
    //        _objsector = objsector;
    //    }

    //    //public clsGestionarReciboDigital(clsParametroImprimirRecibos parametroImprimirRecibo, String RutaDestino, clsSuministrosSectorRecibo objsector, clsTareaEjecucion tarea)
    //    //{
    //    //    _RutaDestino = RutaDestino;
    //    //    _parametroImprimirRecibo = parametroImprimirRecibo;
    //    //    _objsector = objsector;
    //    //    _tarea = tarea;
    //    //}

    //    public IGestionarEnvioReciboDigital CrearGestor()
    //    {
    //        IGestionarEnvioReciboDigital gestor;

    //        if (_parametroImprimirRecibo.Cartera == "M")
    //            gestor = new clsGestionarEnvioReciboDigitalMayor(_parametroImprimirRecibo, _RutaDestino, _objsector, _tarea);
    //        else
    //            gestor = new clsGestionarEnvioReciboDigitalComun(_parametroImprimirRecibo, _RutaDestino, _objsector, _tarea);

    //        return gestor;
    //    }
    //}

    /// <summary>
    /// Interfase para Gestionar la Digitalización de los Recibos.
    /// </summary>
    public interface IGestionarEnvioReciboDigital
    {
        //event EventHandler OnArchivoGenerando;
        //event EventHandler OnArchivoGenerado;
        //event EventHandler OnArchivoCopiando;
        //event EventHandler OnArchivoCopiado;
        event EventHandler OnEscribiendoMemoria;
        event EventHandler OnEscribiendoDiscoLocal;
        event EventHandler OnCopiandoFTP;
        //void Ejecutar(DataSet infoRecibos);
        ///void Ejecutar(DataSet infoRecibos, Boolean masivo);
        //void EjecutarGenerarPDF(DataSet infoRecibos);
        //String Notificar();

        byte[] Ejecutar(clsImprimirReciboMenor oParametro, Int16 intIdUUNN, Int16 intIdCentroServicio, Int32 intPeriodo, Int16 intIdTipoDocumento);

        byte[] Ejecutar(clsImprimirReciboMayor oParametro, Int16 intIdUUNN, Int16 intIdCentroServicio, Int32 intPeriodo, Int16 intIdTipoDocumento);
    }

    /// <summary>
    /// Clase que gestiona la digitalización de los recibos de clientes Mayores
    /// y lo envía por correo electrónico.
    /// </summary>
    public class clsGestionarEnvioReciboDigitalMayor : IGestionarEnvioReciboDigital
    {
        DataSet _infoRecibos;
        String _formato;
        Int16 _esduplicado;
        Int16 _esruta;
        String _escartera;
        Int16 _esvistaprevia;
        String _nombreimpresora;
        Boolean _esimpresora;
        LocalReport localReport = new LocalReport();
        clsImprimirReciboMayor _objcontenidorecibo = new clsImprimirReciboMayor();
        clsReciboMayorConcepto oConceptoBlanco = new clsReciboMayorConcepto();
        List<String> listaArchivos = new List<string>();
        string rutaDestino;
        //clsSuministrosSectorRecibo _objsector;
        String _rutarecibo;
        //clsTareaEjecucion _tarea;
        //clsParametroImprimirRecibos _parametroProceso;

        Int32 IdNroServicio { get; set; }
        Int32 Periodo { get; set; }

        public clsGestionarEnvioReciboDigitalMayor() { }

        //public clsGestionarEnvioReciboDigitalMayor(clsParametroImprimirRecibos parametroProceso)
        //{
        //    _parametroProceso = parametroProceso;
        //    _formato = "pdf";
        //    _esduplicado = 0;
        //    _esruta = 0;
        //    _escartera = parametroProceso.Cartera;
        //    _esvistaprevia = 0;
        //    _nombreimpresora = string.Empty;
        //    _esimpresora = false;
        //    rutaDestino = OptimusNG.Utilidades.UtilidadesNG.ObtenerValorDatosVarios(this._parametroProceso.DatosVarios, "rutaDestino");
        //}

        //public clsGestionarEnvioReciboDigitalMayor(clsParametroImprimirRecibos parametroProceso, String RutaDestino, clsSuministrosSectorRecibo objsector, clsTareaEjecucion tarea)
        //{
        //    _parametroProceso = parametroProceso;
        //    _formato = "pdf";
        //    _esduplicado = 0;
        //    _esruta = 0;
        //    _escartera = parametroProceso.Cartera;
        //    _esvistaprevia = 0;
        //    _nombreimpresora = string.Empty;
        //    _esimpresora = false;
        //    rutaDestino = RutaDestino;//OptimusNG.Utilidades.UtilidadesNG.ObtenerValorDatosVarios(this._parametroProceso.DatosVarios, "rutaDestino");
        //    _objsector = objsector;
        //    _tarea = tarea;
        //}

        clsImprimirReciboMayor objcontenidorecibouno = new clsImprimirReciboMayor();
        clsImprimirReciboMayor _objcontenidorecibotodosG = new clsImprimirReciboMayor();

        public event EventHandler OnEscribiendoMemoria;
        public event EventHandler OnEscribiendoDiscoLocal;
        public event EventHandler OnCopiandoFTP;

        //public void Ejecutar(DataSet infoRecibos)
        //{
        //    _infoRecibos = infoRecibos;

        //    clsImprimirReciboMayor objcontenidorecibotodos = new clsImprimirReciboMayor();

        //    objcontenidorecibotodos = PrepararEntorno();

        //    int contador = 0;
        //    foreach (clsReciboMayorCabecera item in objcontenidorecibotodos.ListaReciboMayorCabecera.Elementos)
        //    {
        //        contador++;

        //        objcontenidorecibouno = new clsImprimirReciboMayor();
        //        localReport = new LocalReport();

        //        this.IdNroServicio = item.IdNroServicio;
        //        this.Periodo = item.Periodo;

        //        objcontenidorecibouno.ListaReciboMayorCabecera.Elementos.Add(item);
        //        objcontenidorecibouno.ListaReciboMayorConcepto.Elementos = objcontenidorecibotodos.ListaReciboMayorConcepto.Elementos.FindAll(delegate (clsReciboMayorConcepto e) { return e.IdNroServicio.Equals(item.IdNroServicio); });
        //        objcontenidorecibouno.ListaReciboMayorConsumo.Elementos = objcontenidorecibotodos.ListaReciboMayorConsumo.Elementos.FindAll(delegate (clsReciboMayorConsumo e) { return e.idNroServicio.Equals(item.IdNroServicio); });
        //        objcontenidorecibouno.ListaReciboMayorLectura.Elementos = objcontenidorecibotodos.ListaReciboMayorLectura.Elementos.FindAll(delegate (clsReciboMayorLectura e) { return e.IdNroServicio.Equals(item.IdNroServicio); });

        //        SetearValores();

        //        CargarCabeceraReporte();

        //        GeneraReciboPDF();
        //    }

        //    //Notificar();
        //}

        //public void Ejecutar(DataSet infoRecibos, Boolean masivo)
        //{
        //    _infoRecibos = infoRecibos;

        //    clsImprimirReciboMayor objcontenidorecibotodos = new clsImprimirReciboMayor();

        //    _objcontenidorecibotodosG = PrepararEntorno();

        //    //recorre cada uno de las cabeceras
        //    foreach (clsReciboMayorCabecera cabecera in _objcontenidorecibotodosG.ListaReciboMayorCabecera.Elementos)
        //    {
        //        objcontenidorecibouno = new clsImprimirReciboMayor();

        //        objcontenidorecibouno.ListaReciboMayorCabecera.Elementos.Add(cabecera);

        //        //de cada cabecera obtiene su consumo

        //        foreach (clsReciboMayorConsumo consumo in _objcontenidorecibotodosG.ListaReciboMayorConsumo.Elementos)
        //        {
        //            if (objcontenidorecibouno.ListaReciboMayorCabecera.Elementos[0].IdNroServicio == consumo.idNroServicio)
        //            {
        //                objcontenidorecibouno.ListaReciboMayorConsumo.Elementos.Add(consumo);
        //            }
        //        }

        //        //de cada cabecera obtiene su lectura

        //        foreach (clsReciboMayorLectura lectura in _objcontenidorecibotodosG.ListaReciboMayorLectura.Elementos)
        //        {
        //            if (objcontenidorecibouno.ListaReciboMayorCabecera.Elementos[0].IdNroServicio == lectura.IdNroServicio)
        //            {
        //                objcontenidorecibouno.ListaReciboMayorLectura.Elementos.Add(lectura);
        //            }
        //        }

        //        //de cada cabecera obtiene su concepto

        //        foreach (clsReciboMayorConcepto concepto in _objcontenidorecibotodosG.ListaReciboMayorConcepto.Elementos)
        //        {
        //            if (objcontenidorecibouno.ListaReciboMayorCabecera.Elementos[0].IdNroServicio == concepto.IdNroServicio)
        //            {
        //                objcontenidorecibouno.ListaReciboMayorConcepto.Elementos.Add(concepto);
        //            }
        //        }

        //        GenerarReciboaEnviar();
        //    }
        //}

        public void GenerarReciboaEnviar()
        {
            SetearValores();
            localReport = new LocalReport();
            localReport.Refresh();
            CargarCabeceraReporte();
            GeneraReciboPDF(false);
            //NotificarClientes();
        }

        //private void NotificarClientes()
        //{
        //    int contador = 0;
        //    int contadorMax = 10;
        //    string cadenaArchivo = _rutarecibo;
        //    string caracterSeparador = string.Empty;
        //    string rutaftp = string.Empty;


        //    if (!string.IsNullOrEmpty(cadenaArchivo))
        //    {
        //        Notificar(cadenaArchivo);
        //    }
        //}
        //private void Notificar()
        //{
        //    int contador = 0;
        //    int contadorMax = 10;
        //    string cadenaArchivo = string.Empty;
        //    string caracterSeparador = string.Empty;

        //    foreach (string item in listaArchivos)
        //    {
        //        cadenaArchivo = string.Concat(cadenaArchivo, caracterSeparador, item);

        //        contador++;
        //        caracterSeparador = ";";

        //        if (contador.Equals(contadorMax))
        //        {
        //            EjecutarNotificacion(cadenaArchivo);
        //            contador = 0;
        //            cadenaArchivo = string.Empty;
        //            caracterSeparador = string.Empty;
        //        }
        //    }

        //    if (!string.IsNullOrEmpty(cadenaArchivo))
        //    {
        //        EjecutarNotificacion(cadenaArchivo);
        //    }
        //}
        //private Boolean Notificar(string cadenaArchivo)
        //{
        //    Boolean resultado = false;
        //    resultado = clsRecibosDAO.Instancia.NotificarReciboDigitalClientes(objcontenidorecibouno, cadenaArchivo, _tarea);

        //    if (resultado)
        //    {
        //        clsArchivo.EliminarArchivo(cadenaArchivo);
        //    }

        //    return resultado;
        //}

        //private void EjecutarNotificacion(string cadenaArchivo)
        //{
        //    String nombreRepresentante = OptimusNG.Utilidades.UtilidadesNG.ObtenerValorDatosVarios(this._parametroProceso.DatosVarios, "representante");
        //    String correo = OptimusNG.Utilidades.UtilidadesNG.ObtenerValorDatosVarios(this._parametroProceso.DatosVarios, "correo");

        //    clsRecibosDAO.Instancia.NotificarReciboDigital(nombreRepresentante, correo, _parametroProceso, cadenaArchivo);
        //}

        private clsImprimirReciboMayor PrepararEntorno()
        {
            clsImprimirReciboMayor objcontenidorecibo = new clsImprimirReciboMayor();

            try
            {
                DataSet dsData = _infoRecibos;

                foreach (DataRow oRow in dsData.Tables["clsReciboMayorCabecera"].Rows)
                {
                    objcontenidorecibo.ListaReciboMayorCabecera.Elementos.Add(new clsReciboMayorCabecera(oRow));
                }

                foreach (DataRow oRow in dsData.Tables["clsReciboMayorConsumo"].Rows)
                {
                    objcontenidorecibo.ListaReciboMayorConsumo.Elementos.Add(new clsReciboMayorConsumo(oRow));
                }

                foreach (DataRow oRow in dsData.Tables["clsReciboMayorLectura"].Rows)
                {
                    objcontenidorecibo.ListaReciboMayorLectura.Elementos.Add(new clsReciboMayorLectura(oRow));
                }

                foreach (DataRow oRow in dsData.Tables["clsReciboMayorConcepto"].Rows)
                {
                    objcontenidorecibo.ListaReciboMayorConcepto.Elementos.Add(new clsReciboMayorConcepto(oRow));
                }

                String _strrutacaraalegre = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraAlegre(Cartera.Mayor);
                String _strrutacaratriste = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraTriste(Cartera.Mayor);

                foreach (clsReciboMayorCabecera item in objcontenidorecibo.ListaReciboMayorCabecera.Elementos)
                {
                    item.ImagenSector = item.ParaCorte ? _strrutacaratriste : _strrutacaraalegre;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objcontenidorecibo;
        }

        private void SetearValores()
        {
            objcontenidorecibouno.DeviceInfo.EsVertical = true;
            objcontenidorecibouno.DeviceInfo.Formato = _formato;
            objcontenidorecibouno.DeviceInfo.PageWidht = (Decimal)21.5;
            objcontenidorecibouno.DeviceInfo.PageHeight = (Decimal)29.7;
            objcontenidorecibouno.DeviceInfo.MarginTop = (Decimal)0;
            objcontenidorecibouno.DeviceInfo.MarginLeft = (Decimal)0;
            objcontenidorecibouno.DeviceInfo.MarginRight = (Decimal)0;
            objcontenidorecibouno.DeviceInfo.MarginBottom = (Decimal)0;
            objcontenidorecibouno.DeviceInfo.EsCentimetros = true;
            objcontenidorecibouno.DeviceInfo.NroColumnas = 1;

            objcontenidorecibouno.EsDuplicado = _esduplicado;
            objcontenidorecibouno.EsRuta = _esruta;
            objcontenidorecibouno.EsCartera = _escartera;
            objcontenidorecibouno.EsVistaPrevia = _esvistaprevia;
            objcontenidorecibouno.NombreImpresora = _nombreimpresora;
            objcontenidorecibouno.EsImpresora = _esimpresora;
            objcontenidorecibouno.Formato = _formato;

            switch (objcontenidorecibouno.Formato.ToLower())
            {
                case "xls":
                    objcontenidorecibouno.Render = "Excel";
                    break;
                case "pdf":
                    objcontenidorecibouno.Render = "PDF";
                    break;
                case "tiff":
                    objcontenidorecibouno.Render = "Image";
                    break;
                default:
                    objcontenidorecibouno.Render = "";
                    break;
            }
        }

        private void CargarCabeceraReporte()
        {
            localReport.ReportEmbeddedResource = "ReciboDigital.ReglaNegocio.Recibo.RptReciboMayor_Cabecera.rdlc";
            localReport.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorCabecera"
                    , objcontenidorecibouno.ListaReciboMayorCabecera.Elementos));
        }

        private byte[] GeneraReciboPDF(Int16 intIdTipoDocumento)
        {
            byte[] bytReciboDigital = null;

            String _dire = string.Empty;
            String ruta = clsUtilidadesImpresionRecibo.CrearArchivosRecibos(Cartera.Mayor, 0, ref _dire);

            localReport.EnableExternalImages = true;
            localReport.ReportPath = _dire;

            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteData);

            // ---------------------------------------------------------------------------
            // Crear el archivo.
            // ---------------------------------------------------------------------------
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;


            String strNombreArchivo = String.Empty;
            String strFormato = String.Empty;

            if (intIdTipoDocumento == 1)
            {
                strNombreArchivo = "Recibo{0}.pdf";
                strFormato = "PDF";
            }
            else if (intIdTipoDocumento == 2)
            {
                strNombreArchivo = "Recibo{0}.png";
                strFormato = "IMAGE";
            }

            string rutaArchivoDestino = System.IO.Path.Combine(rutaDestino, string.Format(strNombreArchivo, this.IdNroServicio));

            // Eliminar archivo gif si existe.
            //clsArchivo.EliminarArchivo(rutaArchivoDestino);


            //RenderingExtension[] oObjeto = localReport.ListRenderingExtensions();

            bytReciboDigital = localReport.Render(strFormato, null, out mimeType, out encoding, out filenameExtension, out streamIds, out warnings);

            //using (FileStream fs = new FileStream(rutaArchivoDestino, FileMode.Create))
            //{
            //    fs.Write(bytes, 0, bytes.Length);
            //}

            //listaArchivos.Add(rutaArchivoDestino);

            return bytReciboDigital;
        }

        private void GeneraReciboPDF(Boolean masivo)
        {
            Int16 esreporteduplicado = Convert.ToInt16(!masivo);
            String _dire = string.Empty;
            String ruta = clsUtilidadesImpresionRecibo.CrearArchivosRecibos(Cartera.Mayor, esreporteduplicado, ref _dire);


            localReport.EnableExternalImages = true;
            localReport.ReportPath = _dire;

            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteData);

            // ---------------------------------------------------------------------------
            // Crear el archivo.
            // ---------------------------------------------------------------------------
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;

            String nombrerecibo = "";
            nombrerecibo = objcontenidorecibouno.ListaReciboMayorCabecera.Elementos[0].IdNroServicio.ToString();

            string rutaArchivoDestino = System.IO.Path.Combine(rutaDestino, string.Format("Recibo{0}.pdf", nombrerecibo));

            // Eliminar archivo si existe.
            clsArchivo.EliminarArchivo(rutaArchivoDestino);


            byte[] bytes = localReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamIds, out warnings);

            using (FileStream fs = new FileStream(rutaArchivoDestino, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            listaArchivos.Add(rutaArchivoDestino);
            _rutarecibo = rutaArchivoDestino;
        }

        public void SubReporteData(object sender, SubreportProcessingEventArgs e)
        {
            if (e.Parameters == null || e.Parameters.Count == 0)
                return;

            this.IdNroServicio = Convert.ToInt32(e.Parameters[1].Values[0]);
            this.Periodo = Convert.ToInt32(e.Parameters[2].Values[0]);

            e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorLectura"
                , objcontenidorecibouno.ListaReciboMayorLectura.Elementos));

            e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorConsumo"
            , objcontenidorecibouno.ListaReciboMayorConsumo.Elementos));

            List<clsReciboMayorConcepto> oConceptos = objcontenidorecibouno.ListaReciboMayorConcepto.Elementos;

            Int32 nFilas = 33;

            // Adicionar los registros para completar las lineas.
            Int32 x = oConceptos.Count;
            for (Int32 y = x; y < nFilas; y++)
            {
                oConceptos.Add(this.oConceptoBlanco);
            }

            // Eliminar las filas superadas.
            while (oConceptos.Count > nFilas)
            {
                oConceptos.RemoveAt(nFilas);
            }

            e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_Facturacion_clsReciboMayorConcepto", oConceptos));
        }

        public Boolean ConceptoFiltrarPorCodigo(clsReciboMayorConcepto oConcepto)
        {
            return oConcepto.IdNroServicio == IdNroServicio && oConcepto.Periodo == Periodo;
        }

        public Boolean LecturaFiltrarPorCodigo(clsReciboMayorLectura oLectura)
        {
            return oLectura.IdNroServicio == IdNroServicio && oLectura.Periodo == Periodo;
        }

        public Boolean ConsumoFiltrarPorCodigo(clsReciboMayorConsumo oConsumo)
        {
            return oConsumo.idNroServicio == IdNroServicio && oConsumo.Periodo == Periodo;
        }

        public void EjecutarGenerarPDF(DataSet infoRecibos)
        {
            throw new NotImplementedException();
        }

        public byte[] Ejecutar(clsImprimirReciboMenor oParametro, Int16 intIdUUNN, Int16 intIdCentroServicio, Int32 intPeriodo, Int16 intIdTipoDocumento)
        {
            throw new NotImplementedException();
        }

        public byte[] Ejecutar(clsImprimirReciboMayor oParametro, Int16 intIdUUNN, Int16 intIdCCSS, Int32 intPeriodo, Int16 intIdTipoDocumento)
        {
            byte[] bytReciboDigital = null;

            clsImprimirReciboMayor objcontenidorecibotodos = new clsImprimirReciboMayor();

            objcontenidorecibotodos = oParametro; //  PrepararEntorno();

            if (intIdTipoDocumento == 1)
                _formato = "pdf";
            else
                _formato = "png";

            _esduplicado = 0;
            _esruta = 0;
            _escartera = "C";
            _esvistaprevia = 0;
            _nombreimpresora = string.Empty;
            _esimpresora = false;
            rutaDestino = ObtenerRutaArchivo(intIdUUNN
                                            , intIdCCSS
                                            , intPeriodo);

            int contador = 0;

            foreach (clsReciboMayorCabecera item in objcontenidorecibotodos.ListaReciboMayorCabecera.Elementos)
            {
                contador++;

                objcontenidorecibouno = new clsImprimirReciboMayor();
                localReport = new LocalReport();

                this.IdNroServicio = item.IdNroServicio;
                this.Periodo = item.Periodo;

                objcontenidorecibouno.ListaReciboMayorCabecera.Elementos.Add(item);
                objcontenidorecibouno.ListaReciboMayorConcepto.Elementos = objcontenidorecibotodos.ListaReciboMayorConcepto.Elementos.FindAll(delegate (clsReciboMayorConcepto e) { return e.IdNroServicio.Equals(item.IdNroServicio); });
                objcontenidorecibouno.ListaReciboMayorConsumo.Elementos = objcontenidorecibotodos.ListaReciboMayorConsumo.Elementos.FindAll(delegate (clsReciboMayorConsumo e) { return e.idNroServicio.Equals(item.IdNroServicio); });
                objcontenidorecibouno.ListaReciboMayorLectura.Elementos = objcontenidorecibotodos.ListaReciboMayorLectura.Elementos.FindAll(delegate (clsReciboMayorLectura e) { return e.IdNroServicio.Equals(item.IdNroServicio); });

                SetearValores();

                CargarCabeceraReporte();

                bytReciboDigital = GeneraReciboPDF(intIdTipoDocumento);
            }

            return bytReciboDigital;
        }

        private String ObtenerRutaArchivo(Int16 iduunn
                           , Int16 idcentroservicio
                           , Int32 periodo)
        {
            clsGestionarIntercambio _objgestorintercambio = clsGestionarIntercambio.ObtenerInstancia();

            clsEsquemaCarpetaBase _objesquemacarpetabase = _objgestorintercambio.ObtenerEsquemaCarpetaBaseUUNN(iduunn);

            clsEsquemaCarpeta _objesquemacarpeta = new clsEsquemaCarpeta();

            _objesquemacarpeta.RutaInicio = _objesquemacarpetabase.RutaInicio;
            _objesquemacarpeta.IdServicio = _objesquemacarpetabase.IdServicio;
            _objesquemacarpeta.IdFlujo = _objesquemacarpetabase.IdFlujo;
            _objesquemacarpeta.IdEmpresa = _objesquemacarpetabase.IdEmpresa;
            _objesquemacarpeta.IdActividad = _objesquemacarpetabase.IdActividad;
            _objesquemacarpeta.IdUnidadNegocio = iduunn;
            _objesquemacarpeta.IdCentroServicio = idcentroservicio;
            _objesquemacarpeta.Periodo = periodo.ToString();
            _objesquemacarpeta.TipoIntercambio = enumTipoIntercambio.Envio;

            // Crea carpeta de envio.
            String _strrutaenvio = clsGestorEsquemaIntercambio.CreaEsquemaCarpeta(_objesquemacarpeta);

            // Validar carpeta creada.
            if (_strrutaenvio.Trim().Length == 0)
                throw new Exception("Error al momento de generar carpeta de envio en el servidor.");

            return _strrutaenvio;
        }

        //string IGestionarEnvioReciboDigital.Notificar()
        //{
        //    throw new NotImplementedException();
        //}
    }

    /// <summary>
    /// Clase que gestiona la digitalización de los recibos de clientes Mayores
    /// y lo envía por correo electrónico.
    /// </summary>
    public class clsGestionarEnvioReciboDigitalComun : IGestionarEnvioReciboDigital
    {
        DataSet _infoRecibos;
        String _formato;
        Int16 _esduplicado;
        Int16 _esruta;
        String _escartera;
        Int16 _esvistaprevia;
        String _nombreimpresora;
        Boolean _esimpresora;
        ReportViewer RptMenor = new ReportViewer();
        LocalReport localReport = new LocalReport();
        clsImprimirReciboMenor _objcontenidorecibo = new clsImprimirReciboMenor();
        clsReciboMenorConcepto oConceptoBlanco = new clsReciboMenorConcepto();
        List<String> listaArchivos = new List<string>();
        String rutaDestino;
        //clsSuministrosSectorRecibo _objsector;
        String _rutarecibo;
        //clsParametroImprimirRecibos _parametroProceso;
        //clsTareaEjecucion _tarea;
        private int m_currentPageIndex;
        public List<String> Archivos;
        public IList<Stream> m_streams;
        public Boolean EsImpresora;
        public String NombreImpresora;
        public String NombreFormato;
        public String NombreRender;
        public String RutaArchivos;
        public String _rutaftp;


        Int32 IdNroServicio { get; set; }
        Int32 Periodo { get; set; }

        public clsGestionarEnvioReciboDigitalComun() { }

        //public clsGestionarEnvioReciboDigitalComun(clsParametroImprimirRecibos parametroProceso, String RutaDestino, clsSuministrosSectorRecibo objsector, clsTareaEjecucion tarea)
        //{
        //    _parametroProceso = parametroProceso;
        //    _formato = "pdf";
        //    _esduplicado = 0;
        //    _esruta = 0;
        //    _escartera = parametroProceso.Cartera;
        //    _esvistaprevia = 0;
        //    _nombreimpresora = string.Empty;
        //    _esimpresora = false;
        //    rutaDestino = RutaDestino;//OptimusNG.Utilidades.UtilidadesNG.ObtenerValorDatosVarios(this._parametroProceso.DatosVarios, "rutaDestino");

        //    _objsector = objsector;
        //    _tarea = tarea;

        //}

        clsImprimirReciboMenor objcontenidorecibouno = new clsImprimirReciboMenor();
        clsImprimirReciboMenor _objcontenidorecibotodos = new clsImprimirReciboMenor();
        clsImprimirReciboMenor _objcontenidorecibotodosG = new clsImprimirReciboMenor();

        public event EventHandler OnArchivoGenerado;
        public event EventHandler OnArchivoCopiando;
        public event EventHandler OnArchivoCopiado;
        public event EventHandler OnArchivoGenerando;
        public event EventHandler OnEscribiendoMemoria;
        public event EventHandler OnEscribiendoDiscoLocal;
        public event EventHandler OnCopiandoFTP;

        //public void Ejecutar(DataSet infoRecibos)
        //{
        //    _infoRecibos = infoRecibos;

        //    clsImprimirReciboMenor objcontenidorecibotodos = new clsImprimirReciboMenor();

        //    _objcontenidorecibotodos = PrepararEntorno();

        //    SetearValores();

        //    CargarCabeceraReporte();

        //    GeneraReciboPDF();

        //    //Notificar();
        //}

        //public void EjecutarGenerarPDF(DataSet infoRecibos)
        //{
        //    _infoRecibos = infoRecibos;

        //    clsImprimirReciboMenor objcontenidorecibotodos = new clsImprimirReciboMenor();

        //    _objcontenidorecibotodos = PrepararEntorno();

        //    SetearValores();

        //    CargarCabeceraReporte();

        //    GeneraReciboPDF();
        //}

        public void Ejecutar(DataSet infoRecibos, Boolean masivo)
        {
            _infoRecibos = infoRecibos;

            //clsImprimirReciboMenor objcontenidorecibotodos = new clsImprimirReciboMenor();

            _objcontenidorecibotodosG = PrepararEntorno();

            //recorre cada uno de las cabeceras
            foreach (clsReciboMenorCabecera cabecera in _objcontenidorecibotodosG.ListaReciboMenorCabecera.Elementos)
            {
                _objcontenidorecibotodos = new clsImprimirReciboMenor();

                _objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos.Add(cabecera);

                //de cada cabecera obtiene su consumo

                foreach (clsReciboMenorConsumo consumo in _objcontenidorecibotodosG.ListaReciboMenorConsumo.Elementos)
                {
                    if (_objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos[0].IdNroServicio == consumo.idNroServicio)
                    {
                        _objcontenidorecibotodos.ListaReciboMenorConsumo.Elementos.Add(consumo);
                    }
                }

                //de cada cabecera obtiene su lectura

                foreach (clsReciboMenorLectura lectura in _objcontenidorecibotodosG.ListaReciboMenorLectura.Elementos)
                {
                    if (_objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos[0].IdNroServicio == lectura.IdNroServicio)
                    {
                        _objcontenidorecibotodos.ListaReciboMenorLectura.Elementos.Add(lectura);
                    }
                }

                //de cada cabecera obtiene su concepto

                foreach (clsReciboMenorConcepto concepto in _objcontenidorecibotodosG.ListaReciboMenorConcepto.Elementos)
                {
                    if (_objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos[0].IdNroServicio == concepto.IdNroServicio)
                    {
                        _objcontenidorecibotodos.ListaReciboMenorConcepto.Elementos.Add(concepto);
                    }
                }

                GenerarReciboaEnviar();
            }


        }

        public void GenerarReciboaEnviar()
        {
            SetearValores(false);
            localReport = new LocalReport();
            localReport.Refresh();
            CargarCabeceraReporte(false);
            GeneraReciboPDF(false);
            //NotificarClientes();
        }

        //public String Notificar()
        //{
        //    int contador = 0;
        //    int contadorMax = 10;
        //    string cadenaArchivo = string.Empty;
        //    string caracterSeparador = string.Empty;
        //    string rutaftp = string.Empty;

        //    foreach (string item in listaArchivos)
        //    {
        //        cadenaArchivo = string.Concat(cadenaArchivo, caracterSeparador, item);

        //        contador++;
        //        caracterSeparador = ";";

        //        if (contador.Equals(contadorMax))
        //        {
        //            EjecutarNotificacion(cadenaArchivo);
        //            contador = 0;
        //            cadenaArchivo = string.Empty;
        //            caracterSeparador = string.Empty;
        //        }
        //    }

        //    if (!string.IsNullOrEmpty(cadenaArchivo))
        //    {
        //        OnArchivoCopiando?.Invoke(this, null);
        //        //copia el archivo creado a ruta FTP
        //        rutaftp = CopiarArchivoFTPWeb(cadenaArchivo);

        //        OnArchivoCopiado?.Invoke(this, null);

        //        EjecutarNotificacion(rutaftp);

        //    }

        //    return rutaftp;
        //}

        //private void NotificarClientes()
        //{
        //    int contador = 0;
        //    int contadorMax = 10;
        //    string cadenaArchivo = _rutarecibo;
        //    string caracterSeparador = string.Empty;
        //    string rutaftp = string.Empty;

        //    //foreach (string item in listaArchivos)
        //    //{
        //    //    cadenaArchivo = string.Concat(cadenaArchivo, caracterSeparador, item);

        //    //    contador++;
        //    //    caracterSeparador = ";";

        //    //    if (contador.Equals(contadorMax))
        //    //    {
        //    //        EjecutarNotificacion(cadenaArchivo);
        //    //        contador = 0;
        //    //        cadenaArchivo = string.Empty;
        //    //        caracterSeparador = string.Empty;
        //    //    }
        //    //}

        //    if (!string.IsNullOrEmpty(cadenaArchivo))
        //    {
        //        //copia el archivo creado a ruta FTP
        //        //rutaftp = CopiarArchivoFTP(cadenaArchivo);

        //        Notificar(cadenaArchivo);
        //    }
        //}

        //private String CopiarArchivoFTP(String ruta)
        //{
        //    Object objrutaftp = clsEmpresaDAO.Instancia.ObtenerDatosComplemento(_parametroProceso.IdEmpresa, 2625); //ruta ftp

        //    String rutaftp = "";
        //    String direccion = "";
        //    String Usuario;
        //    String Contrasenia;

        //    direccion = objrutaftp.ToString();

        //    rutaftp = direccion.Split(';')[0].ToString();
        //    Usuario = direccion.Split(';')[1].ToString();
        //    Contrasenia = direccion.Split(';')[2].ToString();

        //    String _strpath1 = Path.Combine(rutaftp, "UN" + (_parametroProceso.IdUUNN).ToString().Trim().PadLeft(3, '0'));
        //    String _strpath2 = Path.Combine(_strpath1 + "/", (_parametroProceso.Periodo).ToString().Trim() + "/");

        //    CrearCarpetaFTP(_strpath1, Usuario, Contrasenia);
        //    CrearCarpetaFTP(_strpath2, Usuario, Contrasenia);

        //    String Rutatotal = Path.Combine((_strpath2), Path.GetFileName(ruta));

        //    string filename = Path.GetFileName(ruta);
        //    string ftpfullpath = Rutatotal;
        //    FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpfullpath);
        //    ftp.Credentials = new NetworkCredential(Usuario, Contrasenia);

        //    ftp.KeepAlive = true;
        //    ftp.UseBinary = true;
        //    ftp.Method = WebRequestMethods.Ftp.UploadFile;

        //    FileStream fs = File.OpenRead(ruta);
        //    byte[] buffer = new byte[fs.Length];
        //    fs.Read(buffer, 0, buffer.Length);
        //    fs.Close();

        //    Stream ftpstream = ftp.GetRequestStream();
        //    ftpstream.Write(buffer, 0, buffer.Length);
        //    ftpstream.Close();

        //    return ftpfullpath;
        //}

        //public Boolean CrearCarpetaFTP(String directorio, String usuario, String contrasenia)
        //{

        //    try
        //    {
        //        //create the directory
        //        FtpWebRequest requestDir = (FtpWebRequest)FtpWebRequest.Create(new Uri(directorio));
        //        requestDir.Method = WebRequestMethods.Ftp.MakeDirectory;
        //        requestDir.Credentials = new NetworkCredential(usuario, contrasenia);
        //        requestDir.UsePassive = true;
        //        requestDir.UseBinary = true;
        //        requestDir.KeepAlive = false;
        //        FtpWebResponse response = (FtpWebResponse)requestDir.GetResponse();
        //        Stream ftpStream = response.GetResponseStream();

        //        ftpStream.Close();
        //        response.Close();

        //        return true;
        //    }
        //    catch (WebException ex)
        //    {

        //        FtpWebResponse response = (FtpWebResponse)ex.Response;
        //        if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
        //        {
        //            response.Close();
        //            return true;
        //        }
        //        else
        //        {
        //            response.Close();
        //            return false;
        //        }
        //    }

        //    //
        //}

        //private String CopiarArchivoFTPWeb(String ruta)
        //{
        //    Object objrutaftp = clsEmpresaDAO.Instancia.ObtenerDatosComplemento(_parametroProceso.IdEmpresa, 2625); //ruta ftp

        //    String rutaftp = "";
        //    String direccion = "";
        //    String Usuario;
        //    String Contrasenia;
        //    ucCargarArchivo upfArchivoLectura = new ucCargarArchivo(); ;

        //    //String _strrutaxml = Utilidades.clsUtilidadesBalance.ConvertirArchivoToXml(txtArchivo.Text, (_extensionarchivo == ".XLSX" ? "Excel" : _extensionarchivo.Substring(1)) , _strnombrehoja);
        //    String _strrutaxml = ruta;
        //    clsEsquemaCarpetaCliente _objesquemacarpeta = new clsEsquemaCarpetaCliente();

        //    _objesquemacarpeta.IdCentroServicio = _parametroProceso.IdCentroServicio;
        //    _objesquemacarpeta.IdProveedorActividad = _parametroProceso.IdProveedorActividad;
        //    _objesquemacarpeta.IdUnidadNegocio = _parametroProceso.IdUUNN;
        //    _objesquemacarpeta.Periodo = _parametroProceso.Periodo.ToString();
        //    //_objesquemacarpeta.RutaDescargaArchivo = objrutaftp.ToString();

        //    _objesquemacarpeta.RutaDescargaArchivo = ObtenerRutaArchivoRecepcion(_parametroProceso.IdUUNN, _parametroProceso.IdCentroServicio, _parametroProceso.Periodo, objrutaftp.ToString());

        //    //  lblprocesoarchivo.Visible = true;

        //    List<String> _listaarchivos = new List<String>();

        //    _listaarchivos.Add(_strrutaxml);

        //    //comprime el archivo en .zip
        //    upfArchivoLectura.CargarArchivosFTP(_objesquemacarpeta, _listaarchivos);

        //    //elimina el archivo pdf original
        //    if (File.Exists(_strrutaxml))
        //        File.Delete(_strrutaxml);

        //    rutaftp = Path.Combine((_objesquemacarpeta.RutaDescargaArchivo), Path.GetFileName(ruta) + ".zip");
        //    return rutaftp;
        //}

        private String ObtenerRutaArchivoRecepcion(Int16 iduunn, Int16 idcentroservicio, Int32 periodo, String rutainicio)
        {
            clsGestionarIntercambio _objgestorintercambio = clsGestionarIntercambio.ObtenerInstancia();

            clsEsquemaCarpetaBase _objesquemacarpetabase = _objgestorintercambio.ObtenerEsquemaCarpetaBaseUUNN(iduunn);

            clsEsquemaCarpeta _objesquemacarpeta = new clsEsquemaCarpeta();

            _objesquemacarpetabase.RutaInicio = rutainicio;

            _objesquemacarpeta.RutaInicio = _objesquemacarpetabase.RutaInicio;
            _objesquemacarpeta.IdServicio = _objesquemacarpetabase.IdServicio;
            _objesquemacarpeta.IdFlujo = _objesquemacarpetabase.IdFlujo;
            _objesquemacarpeta.IdEmpresa = _objesquemacarpetabase.IdEmpresa;
            _objesquemacarpeta.IdActividad = _objesquemacarpetabase.IdActividad;
            _objesquemacarpeta.IdUnidadNegocio = iduunn;
            _objesquemacarpeta.IdCentroServicio = idcentroservicio;
            _objesquemacarpeta.Periodo = periodo.ToString();
            _objesquemacarpeta.TipoIntercambio = enumTipoIntercambio.Recepcion;

            // Crea carpeta de envio.
            String _strrutaenvio = clsGestorEsquemaIntercambio.CreaEsquemaCarpetaUUNN(_objesquemacarpeta);

            // Validar carpeta creada.
            if (_strrutaenvio.Trim().Length == 0)
                throw new Exception("Error al momento de generar carpeta de envio en el servidor.");

            return _strrutaenvio;
        }

        //private void EjecutarNotificacion(string cadenaArchivo)
        //{
        //    String nombreRepresentante = OptimusNG.Utilidades.UtilidadesNG.ObtenerValorDatosVarios(this._parametroProceso.DatosVarios, "representante");
        //    String correo = OptimusNG.Utilidades.UtilidadesNG.ObtenerValorDatosVarios(this._parametroProceso.DatosVarios, "correo");

        //    clsRecibosDAO.Instancia.NotificarReciboDigital(nombreRepresentante, correo, _parametroProceso, cadenaArchivo);
        //}

        //private Boolean Notificar(string cadenaArchivo)
        //{
        //    Boolean resultado = false;
        //    resultado = clsRecibosDAO.Instancia.NotificarReciboDigitalClientes(_objcontenidorecibotodos, cadenaArchivo, _tarea);

        //    if (resultado)
        //    {
        //        clsArchivo.EliminarArchivo(cadenaArchivo);
        //    }

        //    return resultado;
        //}

        private clsImprimirReciboMenor PrepararEntorno()
        {
            clsImprimirReciboMenor objcontenidorecibo = new clsImprimirReciboMenor();

            try
            {
                DataSet dsData = _infoRecibos;

                foreach (DataRow oRow in dsData.Tables["clsReciboMenorCabecera"].Rows)
                {
                    objcontenidorecibo.ListaReciboMenorCabecera.Elementos.Add(new clsReciboMenorCabecera(oRow));
                }

                foreach (DataRow oRow in dsData.Tables["clsReciboMenorConsumo"].Rows)
                {
                    objcontenidorecibo.ListaReciboMenorConsumo.Elementos.Add(new clsReciboMenorConsumo(oRow));
                }

                foreach (DataRow oRow in dsData.Tables["clsReciboMenorLectura"].Rows)
                {
                    objcontenidorecibo.ListaReciboMenorLectura.Elementos.Add(new clsReciboMenorLectura(oRow));
                }

                foreach (DataRow oRow in dsData.Tables["clsReciboMenorConcepto"].Rows)
                {
                    objcontenidorecibo.ListaReciboMenorConcepto.Elementos.Add(new clsReciboMenorConcepto(oRow));
                }

                String _strrutacaraalegre = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraAlegre(Cartera.Comun);
                String _strrutacaratriste = "FILE://" + clsUtilidadesImpresionRecibo.CrearCaraTriste(Cartera.Comun);


                foreach (clsReciboMenorCabecera item in objcontenidorecibo.ListaReciboMenorCabecera.Elementos)
                {
                    if (item.IncluirCaritas == 1)
                    {
                        item.ImagenCara1 = item.IdEstrato1 == 1 ? _strrutacaraalegre : _strrutacaratriste;
                        item.ImagenCara2 = item.IdEstrato2 == 1 ? _strrutacaraalegre : _strrutacaratriste;
                        item.ImagenCara3 = item.IdEstrato3 == 1 ? _strrutacaraalegre : _strrutacaratriste;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objcontenidorecibo;
        }

        private void SetearValores()
        {
            _objcontenidorecibotodos.DeviceInfo.EsVertical = false;
            _objcontenidorecibotodos.DeviceInfo.Formato = _formato;
            _objcontenidorecibotodos.DeviceInfo.PageWidht = (Decimal)21.5;
            _objcontenidorecibotodos.DeviceInfo.PageHeight = (Decimal)29.7;
            _objcontenidorecibotodos.DeviceInfo.MarginTop = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.MarginLeft = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.MarginRight = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.MarginBottom = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.EsCentimetros = true;
            _objcontenidorecibotodos.DeviceInfo.NroColumnas = 2;

            _objcontenidorecibotodos.EsDuplicado = _esduplicado;
            _objcontenidorecibotodos.EsRuta = _esruta;
            _objcontenidorecibotodos.EsCartera = _escartera;
            _objcontenidorecibotodos.EsVistaPrevia = _esvistaprevia;
            _objcontenidorecibotodos.NombreImpresora = _nombreimpresora;
            _objcontenidorecibotodos.EsImpresora = _esimpresora;
            _objcontenidorecibotodos.Formato = _formato;

            switch (_objcontenidorecibotodos.Formato.ToLower())
            {
                case "xls":
                    _objcontenidorecibotodos.Render = "Excel";
                    break;
                case "pdf":
                    _objcontenidorecibotodos.Render = "PDF";
                    break;
                case "emf":
                    _objcontenidorecibotodos.Render = "Image";
                    break;
                default:
                    _objcontenidorecibotodos.Render = "";
                    break;
            }
        }

        private void SetearValores(Boolean masivo)
        {
            _objcontenidorecibotodos.DeviceInfo.EsVertical = !masivo;
            _objcontenidorecibotodos.DeviceInfo.Formato = _formato;
            _objcontenidorecibotodos.DeviceInfo.PageWidht = (Decimal)21.5;
            _objcontenidorecibotodos.DeviceInfo.PageHeight = (Decimal)29.7;
            _objcontenidorecibotodos.DeviceInfo.MarginTop = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.MarginLeft = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.MarginRight = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.MarginBottom = (Decimal)0;
            _objcontenidorecibotodos.DeviceInfo.EsCentimetros = true;
            _objcontenidorecibotodos.DeviceInfo.NroColumnas = (masivo == false) ? (Int16)1 : (Int16)2;

            _objcontenidorecibotodos.EsDuplicado = _esduplicado;
            _objcontenidorecibotodos.EsRuta = _esruta;
            _objcontenidorecibotodos.EsCartera = _escartera;
            _objcontenidorecibotodos.EsVistaPrevia = _esvistaprevia;
            _objcontenidorecibotodos.NombreImpresora = _nombreimpresora;
            _objcontenidorecibotodos.EsImpresora = _esimpresora;
            _objcontenidorecibotodos.Formato = _formato;

            switch (_objcontenidorecibotodos.Formato.ToLower())
            {
                case "xls":
                    _objcontenidorecibotodos.Render = "Excel";
                    break;
                case "pdf":
                    _objcontenidorecibotodos.Render = "PDF";
                    break;
                case "emf":
                    _objcontenidorecibotodos.Render = "Image";
                    break;
                default:
                    _objcontenidorecibotodos.Render = "";
                    break;
            }

            //objcontenidorecibo.RutaSalidaReporte = rutasalida;
        }

        private void CargarCabeceraReporte()
        {
            localReport.ReportEmbeddedResource = "ReciboDigital.ReglaNegocio.Recibo.RptReciboMenor_Cabecera.rdlc";
            localReport.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorCabecera"
                    , _objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos));
        }

        private void CargarCabeceraReporte(Boolean masivo)
        {
            if (masivo)
            {
                localReport.ReportEmbeddedResource = "OptimusNG.ReglasNegocio.Procesos.TareasAutomatizadas.Recibo.RptReciboMenor_Cabecera.rdlc";
                localReport.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorCabecera"
                        , _objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos));
            }
            else
            {
                localReport.ReportEmbeddedResource = "OptimusNG.ReglasNegocio.Procesos.TareasAutomatizadas.Recibo.RptReciboMenorUno_Cabecera.rdlc";
                localReport.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorCabecera"
                        , _objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos));
            }


        }

        private byte[] GeneraReciboPDF(Int16 intIdTipoDocumento)
        {
            try
            {
                byte[] bitDocumentoRecibo = null;

                String _dire = string.Empty;
                String ruta = clsUtilidadesImpresionRecibo.CrearArchivosRecibos(Cartera.Comun, 0, ref _dire);


                localReport.EnableExternalImages = true;
                localReport.ReportPath = _dire;

                localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteData);


                // ---------------------------------------------------------------------------
                // Crear el archivo.
                // ---------------------------------------------------------------------------
                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string filenameExtension = string.Empty;
                string streamids = string.Empty;

                String strNombreArchivo = String.Empty;
                String strFormato = String.Empty;

                if(intIdTipoDocumento == 1)
                {
                    strNombreArchivo = "Recibo{0}.pdf";
                    strFormato = "PDF";
                }
                else if (intIdTipoDocumento == 2)
                {
                    strNombreArchivo = "Recibo{0}.png";
                    strFormato = "IMAGE";
                }

                string rutaArchivoDestino = System.IO.Path.Combine(rutaDestino, string.Format(strNombreArchivo, this.IdNroServicio));

                // Eliminar archivo si existe.
                //clsArchivo.EliminarArchivo(rutaArchivoDestino);

                //OnEscribiendoMemoria?.Invoke(this, null);

                bitDocumentoRecibo = localReport.Render(strFormato, null, out mimeType, out encoding, out filenameExtension, out streamIds, out warnings);

                //OnEscribiendoDiscoLocal?.Invoke(this, null);

                //using (FileStream fs = new FileStream(rutaArchivoDestino, FileMode.Create))
                //{
                //    fs.Write(bitDocumentoRecibo, 0, bitDocumentoRecibo.Length);
                //}

                //OnArchivoGenerado?.Invoke(this, null);

                //listaArchivos.Add(rutaArchivoDestino);

                return bitDocumentoRecibo;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void GeneraReciboPDF(Boolean masivo)
        {
            Int16 esreporteduplicado = Convert.ToInt16(!masivo);
            String _dire = string.Empty;
            String ruta = clsUtilidadesImpresionRecibo.CrearArchivosRecibos(Cartera.Comun, esreporteduplicado, ref _dire);


            localReport.EnableExternalImages = true;
            localReport.ReportPath = _dire;

            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReporteData);

            // ---------------------------------------------------------------------------
            // Crear el archivo.
            // ---------------------------------------------------------------------------
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string filenameExtension = string.Empty;
            string streamids = string.Empty;

            String nombrerecibo = "";
            nombrerecibo = _objcontenidorecibotodos.ListaReciboMenorCabecera.Elementos[0].IdNroServicio.ToString();

            string rutaArchivoDestino = System.IO.Path.Combine(rutaDestino, string.Format("Recibo{0}.pdf", nombrerecibo));

            // Eliminar archivo si existe.
            clsArchivo.EliminarArchivo(rutaArchivoDestino);


            byte[] bytes = localReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamIds, out warnings);

            using (FileStream fs = new FileStream(rutaArchivoDestino, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            listaArchivos.Add(rutaArchivoDestino);
            _rutarecibo = rutaArchivoDestino;
        }

        private void SubReporteData(object sender, SubreportProcessingEventArgs e)
        {
            if (e.Parameters == null || e.Parameters.Count == 0)
                return;

            this.IdNroServicio = Convert.ToInt32(e.Parameters[1].Values[0]);
            this.Periodo = Convert.ToInt32(e.Parameters[2].Values[0]);

            // Para las lecturas
            if (e.ReportPath.Contains("Importe"))
            {
                List<clsReciboMenorConcepto> oConceptos = _objcontenidorecibotodos.ListaReciboMenorConcepto.Elementos.FindAll(ConceptoFiltrarPorCodigo);

                Int32 nFilas = 24;

                #region Adicionar los registros para completar las lineas.

                Int32 x = oConceptos.Count;
                for (Int32 y = x; y < nFilas; y++)
                {
                    oConceptos.Add(this.oConceptoBlanco);
                }

                #endregion

                #region Eliminar las filas superadas.

                while (oConceptos.Count > nFilas)
                {
                    oConceptos.RemoveAt(nFilas);
                }

                #endregion

                e.DataSources.Add(new ReportDataSource("OptimusNG_Entidades_clsReciboMenorConcepto", oConceptos));
            }
            else
            {
                e.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorConsumo"
                    , _objcontenidorecibotodos.ListaReciboMenorConsumo.Elementos.FindAll(ConsumoFiltrarPorCodigo)));
                e.DataSources.Add(new ReportDataSource("ReciboMenor_clsReciboMenorLectura"
                    , _objcontenidorecibotodos.ListaReciboMenorLectura.Elementos.FindAll(LecturaFiltrarPorCodigo)));

            }

        }

        public Boolean ConceptoFiltrarPorCodigo(clsReciboMenorConcepto oConcepto)
        {
            return oConcepto.IdNroServicio == IdNroServicio && oConcepto.Periodo == Periodo;
        }

        public Boolean LecturaFiltrarPorCodigo(clsReciboMenorLectura oLectura)
        {
            return oLectura.IdNroServicio == IdNroServicio && oLectura.Periodo == Periodo;
        }

        public Boolean ConsumoFiltrarPorCodigo(clsReciboMenorConsumo oConsumo)
        {
            return oConsumo.idNroServicio == IdNroServicio && oConsumo.Periodo == Periodo;
        }

        public byte[] Ejecutar(clsImprimirReciboMenor oParametro, Int16 intIdUUNN, Int16 intIdCCSS, Int32 intPeriodo, Int16 intIdTipoDocumento)
        {
            byte[] bitDocumentoRecibo = null;

            clsImprimirReciboMenor objcontenidorecibotodos = new clsImprimirReciboMenor();

            _objcontenidorecibotodos = oParametro; // PrepararEntorno();


            //_parametroProceso = parametroProceso;

            if(intIdTipoDocumento == 1)
                _formato = "pdf";
            else
                _formato = "png";

            _esduplicado = 0;
            _esruta = 0;
            _escartera = "C";
            _esvistaprevia = 0;
            _nombreimpresora = string.Empty;
            _esimpresora = false;
            rutaDestino = ObtenerRutaArchivo(intIdUUNN
                                            , intIdCCSS
                                            , intPeriodo);

            SetearValores();

            CargarCabeceraReporte();

            bitDocumentoRecibo = GeneraReciboPDF(intIdTipoDocumento);

            return bitDocumentoRecibo;
        }

        public byte[] Ejecutar(clsImprimirReciboMayor oParametro, Int16 intIdUUNN, Int16 intIdCCSS, Int32 intPeriodo, Int16 intIdTipoDocumento)
        {
            throw new NotImplementedException();
        }

        private String ObtenerRutaArchivo(Int16 iduunn
                           , Int16 idcentroservicio
                           , Int32 periodo)
        {
            clsGestionarIntercambio _objgestorintercambio = clsGestionarIntercambio.ObtenerInstancia();

            clsEsquemaCarpetaBase _objesquemacarpetabase = _objgestorintercambio.ObtenerEsquemaCarpetaBaseUUNN(iduunn);

            clsEsquemaCarpeta _objesquemacarpeta = new clsEsquemaCarpeta();

            _objesquemacarpeta.RutaInicio = _objesquemacarpetabase.RutaInicio;
            _objesquemacarpeta.IdServicio = _objesquemacarpetabase.IdServicio;
            _objesquemacarpeta.IdFlujo = _objesquemacarpetabase.IdFlujo;
            _objesquemacarpeta.IdEmpresa = _objesquemacarpetabase.IdEmpresa;
            _objesquemacarpeta.IdActividad = _objesquemacarpetabase.IdActividad;
            _objesquemacarpeta.IdUnidadNegocio = iduunn;
            _objesquemacarpeta.IdCentroServicio = idcentroservicio;
            _objesquemacarpeta.Periodo = periodo.ToString();
            _objesquemacarpeta.TipoIntercambio = enumTipoIntercambio.Envio;

            // Crea carpeta de envio.
            String _strrutaenvio = clsGestorEsquemaIntercambio.CreaEsquemaCarpeta(_objesquemacarpeta);

            // Validar carpeta creada.
            if (_strrutaenvio.Trim().Length == 0)
                throw new Exception("Error al momento de generar carpeta de envio en el servidor.");

            return _strrutaenvio;
        }
    }
}
