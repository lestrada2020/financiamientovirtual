﻿using SISAC.Entidad.Maestro;
using SISAC.Resources;
using System;
using System.IO;

namespace SISAC.Negocio.Facturacion
{
    public class clsUtilidadesImpresionRecibo
    {
        public static String CrearCaraAlegre(Cartera cartera)
        {
            String _strnombrearchivo = "";
            String _strarchivojpg = "CaraAlegre.jpg";

            _strnombrearchivo = Path.Combine(ObtenerCarpetaTemporal(), _strarchivojpg);

            try
            {
                if (!File.Exists(_strnombrearchivo))
                {
                    byte[] ofileblanco = clsRecursoGrafico.CaraAlegre;
                    CrearArchivo(_strnombrearchivo, ofileblanco);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _strnombrearchivo;
        }

        public static String CrearCaraTriste(Cartera cartera)
        {
            String _strnombrearchivo = "";
            String _strarchivojpg = "CaraTriste.jpg";

            _strnombrearchivo = Path.Combine(ObtenerCarpetaTemporal(), _strarchivojpg);

            try
            {
                if (!File.Exists(_strnombrearchivo))
                {
                    byte[] ofileblanco = clsRecursoGrafico.CaraTriste;
                    CrearArchivo(_strnombrearchivo, ofileblanco);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _strnombrearchivo;
        }

        public static void CrearArchivo(String rutaarchivo, Byte[] archivo)
        {
            //if (!File.Exists(rutaarchivo))
            //{
                FileStream _file = new FileStream(rutaarchivo, FileMode.Create);
                _file.Write(archivo, 0, archivo.Length);
                _file.Close();
            //}
        }

        /// <summary>
        /// Obtiene la ruta de la carpeta temporal.
        /// </summary>
        /// <returns></returns>
        public static String ObtenerCarpetaTemporal()
        {
            return Path.GetTempPath();
        }

        public static String CrearArchivosRecibos(Cartera cartera, Int16 esreporteduplicado, ref string rutaArchivoCreado)
        {
            String _strnombrereciboblanco = CreaReciboBanco();

            String _strruta = ObtenerCarpetaTemporal();

            rutaArchivoCreado = _strruta;

            if (cartera == Cartera.Comun)
            {
                byte[] ofilecabecera = clsRecursoGrafico.ReporteReciboCabecera;
                byte[] ofilecabecerauno = clsRecursoGrafico.ReporteReciboCabeceraUno;
                byte[] ofileconcepto = clsRecursoGrafico.ReporteReciboConcepto;
                byte[] ofileconceptoimporte = clsRecursoGrafico.ReporteReciboConceptoImporte;

                if (esreporteduplicado == 0)
                {
                    rutaArchivoCreado = Path.Combine(_strruta, "RptReciboMenor_Cabecera.rdlc");
                    CrearArchivo(rutaArchivoCreado, ofilecabecera);
                }

                else
                {
                    rutaArchivoCreado = Path.Combine(_strruta, "RptReciboMenorUno_Cabecera.rdlc");

                    CrearArchivo(rutaArchivoCreado, ofilecabecerauno);
                }

                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMenor_Concepto.rdlc"), ofileconcepto);
                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMenor_ConceptoImporte.rdlc"), ofileconceptoimporte);
            }
            else
            {
                byte[] ofilecabeceramayor = clsRecursoGrafico.ReporteReciboCabeceraMayor;
                byte[] ofileconceptomayor = clsRecursoGrafico.ReporteReciboConceptoMayor;

                rutaArchivoCreado = Path.Combine(_strruta, "RptReciboMayor_Cabecera.rdlc");

                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMayor_Cabecera.rdlc"), ofilecabeceramayor);
                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMayor_Concepto.rdlc"), ofileconceptomayor);
            }

            return _strnombrereciboblanco;
        }

        public static String CrearArchivosRecibos(Cartera cartera, Int16 esreporteduplicado)
        {
            String _strnombrereciboblanco = CreaReciboBanco();

            String _strruta = System.IO.Path.GetTempPath();

            if (cartera == Cartera.Comun)
            {
                byte[] ofilecabecera = clsRecursoGrafico.ReporteReciboCabecera;
                byte[] ofilecabecerauno = clsRecursoGrafico.ReporteReciboCabeceraUno;
                byte[] ofileconcepto = clsRecursoGrafico.ReporteReciboConcepto;
                byte[] ofileconceptoimporte = clsRecursoGrafico.ReporteReciboConceptoImporte;

                if (esreporteduplicado == 0)
                    CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMenor_Cabecera.rdlc"), ofilecabecera);
                else CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMenorUno_Cabecera.rdlc"), ofilecabecerauno);

                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMenor_Concepto.rdlc"), ofileconcepto);
                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMenor_ConceptoImporte.rdlc"), ofileconceptoimporte);
            }
            else
            {
                byte[] ofilecabeceramayor = clsRecursoGrafico.ReporteReciboCabeceraMayor;
                byte[] ofileconceptomayor = clsRecursoGrafico.ReporteReciboConceptoMayor;

                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMayor_Cabecera.rdlc"), ofilecabeceramayor);
                CrearArchivo(System.IO.Path.Combine(_strruta, "RptReciboMayor_Concepto.rdlc"), ofileconceptomayor);
            }

            return _strnombrereciboblanco;
        }

        public static String CreaReciboBanco()
        {
            String _strnombrearchivo = "";
            String _strarchivojpg = "ReciboBlanco.jpg";

            _strnombrearchivo = Path.Combine(ObtenerCarpetaTemporal(), _strarchivojpg);

            try
            {
                if (!File.Exists(_strnombrearchivo))
                {
                    byte[] ofileblanco = clsRecursoGrafico.ReciboBlanco;
                    CrearArchivo(_strnombrearchivo, ofileblanco);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _strnombrearchivo;
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public static string ObtenerCarpetaYNombreArchivoReciboBlanco(string rutaRelativa)
        {
            string strArchivoJpg = "ReciboBlanco.jpg";
            string strNombreArchivo = Path.Combine(
                Path.Combine(ObtenerCarpetaTemporal(), rutaRelativa),
                strArchivoJpg);

            return strNombreArchivo;
        }

        public static string CrearArchivosImpresionRecibos(Cartera cartera, short esReporteDuplicado,
            ref string rutaArchivoCreado)
        {
            string strNombreReciboBlanco = CreaReciboBanco();

            string strRuta = ObtenerCarpetaTemporal();

            rutaArchivoCreado = strRuta;

            if (cartera == Cartera.Comun)
            {
                byte[] oFileCabecera = clsRecursoGrafico.ReporteReciboCabecera;
                byte[] oFileCabeceraUno = clsRecursoGrafico.ReporteReciboCabeceraUno;
                byte[] oFileConcepto = clsRecursoGrafico.ReporteReciboConcepto;
                byte[] oFileConceptoImporte = clsRecursoGrafico.ReporteReciboConceptoImporte;

                if (esReporteDuplicado == 0)
                {
                    rutaArchivoCreado = Path.Combine(strRuta, "RptReciboMenor_Cabecera_ImpRec.rdlc");
                    CrearArchivo(rutaArchivoCreado, oFileCabecera);
                }

                else
                {
                    rutaArchivoCreado = Path.Combine(strRuta, "RptReciboMenorUno_Cabecera_ImpRec.rdlc");

                    CrearArchivo(rutaArchivoCreado, oFileCabeceraUno);
                }

                CrearArchivo(Path.Combine(strRuta, "RptReciboMenor_Concepto_ImpRec.rdlc"),
                    oFileConcepto);
                CrearArchivo(Path.Combine(strRuta, "RptReciboMenor_ConceptoImporte_ImpRec.rdlc"),
                    oFileConceptoImporte);
            }
            else
            {
                byte[] oFileCabeceraMayor = clsRecursoGrafico.ReporteReciboCabeceraMayor;
                byte[] oFileConceptoMayor = clsRecursoGrafico.ReporteReciboConceptoMayor;

                rutaArchivoCreado = Path.Combine(strRuta, "RptReciboMayor_Cabecera_ImpRec.rdlc");

                CrearArchivo(rutaArchivoCreado, oFileCabeceraMayor);
                CrearArchivo(Path.Combine(strRuta, "RptReciboMayor_Concepto_ImpRec.rdlc"), oFileConceptoMayor);
            }

            return strNombreReciboBlanco;
        }

        #endregion

    }
}
