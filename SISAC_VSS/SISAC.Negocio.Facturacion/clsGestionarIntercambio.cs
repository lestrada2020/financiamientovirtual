using System;
using System.Data;
using System.Collections;
using System.Threading;
using System.Xml.Serialization;
using SISAC.Data.DAO;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Data.OleDb;

namespace SISAC.Negocio.Facturacion
{
    public class clsGestionarIntercambio
    {

        #region Field
            private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
            private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        #endregion

        #region Obtener Instancia

        private static clsGestionarIntercambio _objinstancia;
        private static Mutex _objmutex = new Mutex();

        public static clsGestionarIntercambio ObtenerInstancia()
        {
            _objmutex.WaitOne();

            if (_objinstancia == null)
            {
                _objinstancia = new clsGestionarIntercambio();
            }

            _objmutex.ReleaseMutex();

            return _objinstancia;

        }

        #endregion

        #region IIntercambioPerfil Members

        public clsListaEsquemaIntercambioPerfilMaestro ObtenerListaMaestroEsquemaIntercambioPerfil()
        {
            Hashtable _htparam = new Hashtable();
            clsListaEsquemaIntercambioPerfilMaestro _lstesquema;
            Hashtable oParametro = new Hashtable();

            DataTable oTabla = _DAO.EjecutarComandoEntidad("EsquemaIntercambioPerfil", "ObtenerListaMaestro", oParametro);

            _lstesquema = new clsListaEsquemaIntercambioPerfilMaestro(oTabla);

            return _lstesquema;
        }

        public clsEsquemaIntercambioPerfil ObtenerRegistroEsquemaIntercambioPerfil(Int16 id)
        {
            DataRow _objresultado;
            Hashtable _htparam = new Hashtable();
            clsEsquemaIntercambioPerfil _lstesquema;

            _htparam.Add("@id", id);

            _objresultado = _DAO.EjecutarComandoRegistro("EsquemaIntercambioPerfil", "ObtenerRegistro", _htparam);

            _lstesquema = new clsEsquemaIntercambioPerfil(_objresultado);

            return _lstesquema;
        }

        public clsEsquemaIntercambioPerfil ObtenerRegistroEsquemaIntercambioPerfilxFormato(clsEsquemaIntercambioFormato objEIFormato)
        {
            DataRow _objresultado;
            Hashtable _htparam = new Hashtable();
            clsEsquemaIntercambioPerfil _lstesquema;

            _htparam.Add("@id", objEIFormato.Id);

            _objresultado = _DAO.EjecutarComandoRegistro("EsquemaIntercambioPerfil", "ObtenerRegistroxFormato", _htparam);

            _lstesquema = new clsEsquemaIntercambioPerfil(_objresultado);

            return _lstesquema;
        }

        public Boolean InsertarEsquemaIntercambioPerfil(clsEsquemaIntercambioPerfil esquemaintercambioperfil)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@nombre", esquemaintercambioperfil.Nombre);
            _htparam.Add("@descripcion", esquemaintercambioperfil.Descripcion);
            _htparam.Add("@origenintercambio", esquemaintercambioperfil.OrigenIntercambio);
            _htparam.Add("@tipoflujodato", esquemaintercambioperfil.TipoFlujoDato);
            _htparam.Add("@idesquemaenvio", esquemaintercambioperfil.IdEsquemaIntercambioFormatoEnvio);
            _htparam.Add("@idesquemadevolucion", esquemaintercambioperfil.IdEsquemaIntercambioFormatoDevolucion);
            _htparam.Add("@formatoarchivo", esquemaintercambioperfil.FormatoArchivo);
            _htparam.Add("@fechacreacion", DateTime.Now);
            _htparam.Add("@fechavigencia", esquemaintercambioperfil.FechaVigencia);
            _htparam.Add("@fechacaduca", esquemaintercambioperfil.FechaCaduca);
            _htparam.Add("@idEstadoNGC", esquemaintercambioperfil.IdEstadoNGC);
            _htparam.Add("@nombrearchivoprefijoenvio", esquemaintercambioperfil.NombreArchivoPrefijoEnvio);
            _htparam.Add("@nombrearchivomascaraenvio", esquemaintercambioperfil.NombreArchivoMascaraEnvio);
            _htparam.Add("@nombrearchivosufijoenvio", esquemaintercambioperfil.NombreArchivoSufijoEnvio);
            _htparam.Add("@extensionarchivoenvio", esquemaintercambioperfil.ExtensionArchivoEnvio);
            _htparam.Add("@rutaenvio", esquemaintercambioperfil.RutaEnvio);
            _htparam.Add("@nombrearchivoprefijodevolucion", esquemaintercambioperfil.NombreArchivoPrefijoDevolucion);
            _htparam.Add("@nombrearchivomascaradevolucion", esquemaintercambioperfil.NombreArchivoMascaraDevolucion);
            _htparam.Add("@nombrearchivosufijodevolucion", esquemaintercambioperfil.NombreArchivoSufijoDevolucion);
            _htparam.Add("@extensionarchivodevolucion", esquemaintercambioperfil.ExtensionArchivoDevolucion);
            _htparam.Add("@rutadevolucion", esquemaintercambioperfil.RutaDevolucion);

            _intresultado = _DAO.EjecutarComando("EsquemaIntercambioPerfil", "Insertar", _htparam);

            return (_intresultado > 0);
        }

        public Boolean ActualizarEsquemaIntercambioPerfil(clsEsquemaIntercambioPerfil esquemaintercambioperfil)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@id", esquemaintercambioperfil.Id);
            _htparam.Add("@nombre", esquemaintercambioperfil.Nombre);
            _htparam.Add("@descripcion", esquemaintercambioperfil.Descripcion);
            _htparam.Add("@origenintercambio", esquemaintercambioperfil.OrigenIntercambio);
            _htparam.Add("@tipoflujodato", esquemaintercambioperfil.TipoFlujoDato);
            _htparam.Add("@idesquemaenvio", esquemaintercambioperfil.IdEsquemaIntercambioFormatoEnvio);
            _htparam.Add("@idesquemadevolucion", esquemaintercambioperfil.IdEsquemaIntercambioFormatoDevolucion);
            _htparam.Add("@formatoarchivo", esquemaintercambioperfil.FormatoArchivo);
            _htparam.Add("@fechacreacion", esquemaintercambioperfil.FechaCreacion);
            _htparam.Add("@fechavigencia", esquemaintercambioperfil.FechaVigencia);
            _htparam.Add("@fechacaduca", esquemaintercambioperfil.FechaCaduca);
            _htparam.Add("@idEstadoNGC", esquemaintercambioperfil.IdEstadoNGC);
            _htparam.Add("@nombrearchivoprefijoenvio", esquemaintercambioperfil.NombreArchivoPrefijoEnvio);
            _htparam.Add("@nombrearchivomascaraenvio", esquemaintercambioperfil.NombreArchivoMascaraEnvio);
            _htparam.Add("@nombrearchivosufijoenvio", esquemaintercambioperfil.NombreArchivoSufijoEnvio);
            _htparam.Add("@extensionarchivoenvio", esquemaintercambioperfil.ExtensionArchivoEnvio);
            _htparam.Add("@rutaenvio", esquemaintercambioperfil.RutaEnvio);
            _htparam.Add("@nombrearchivoprefijodevolucion", esquemaintercambioperfil.NombreArchivoPrefijoDevolucion);
            _htparam.Add("@nombrearchivomascaradevolucion", esquemaintercambioperfil.NombreArchivoMascaraDevolucion);
            _htparam.Add("@nombrearchivosufijodevolucion", esquemaintercambioperfil.NombreArchivoSufijoDevolucion);
            _htparam.Add("@extensionarchivodevolucion", esquemaintercambioperfil.ExtensionArchivoDevolucion);
            _htparam.Add("@rutadevolucion", esquemaintercambioperfil.RutaDevolucion);

            _intresultado = _DAO.EjecutarComando("EsquemaIntercambioPerfil", "Actualizar", _htparam);

            return (_intresultado > 0);
        }

        public Boolean CambiarEstadoNGCEsquemaIntercambioPerfil(Int16 id, EstadoNGC nuevoEstadoNGC)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@id", id);
            _htparam.Add("@idEstadoNGC", (Int16)nuevoEstadoNGC);

            _intresultado = _DAO.EjecutarComando("EsquemaIntercambioPerfil", "cambiarEstadoNGC", _htparam);

            return (_intresultado > 0);
        }

        #endregion

        #region IIntercambioFormato Members

        public clsListaEsquemaIntercambioFormato ObtenerListaMaestroEsquemaIntercambioFormato()
        {
            clsListaEsquemaIntercambioFormato _lstformato;
            Hashtable oParametro = new Hashtable();

            DataTable _objresultado = _DAO.EjecutarComandoEntidad("EsquemaIntercambioFormato", "ObtenerListaMaestro", oParametro);

            _lstformato = new clsListaEsquemaIntercambioFormato(_objresultado);

            return _lstformato;
        }

        public clsEsquemaIntercambioFormato ObtenerRegistroEsquemaIntercambioFormato(Int32 id)
        {
            clsEsquemaIntercambioFormato _objregistro;
            DataRow _drresultado;
            Hashtable _htparam = new Hashtable();

            _htparam.Add("@id", id);

            _drresultado = _DAO.EjecutarComandoRegistro("EsquemaIntercambioFormato", "ObtenerRegistro", _htparam);

            _objregistro = new clsEsquemaIntercambioFormato(_drresultado);

            return _objregistro;
        }

        public clsEsquemaIntercambioFormato ObtenerFormatoCompleto(Int32 idformato)
        {
            clsEsquemaIntercambioFormato _objregistro = null;

            try
            {
                DataSet _dsresultado;
                Hashtable _htparam = new Hashtable();

                _htparam.Add("@id", idformato);

                _dsresultado = _DAO.EjecutarComandoConjuntoEntidad
                    ("EsquemaIntercambioFormato"
                    , "ObtenerRegistroCompleto"
                    , _htparam
                    , "conjuntoformato"
                    , "dtformato"
                    , "dtcabecera1"
                    , "dtcabecera2"
                    , "dtdetalle"
                    , "dtpie"
                    , "dtcolumnas"
                    , "dtconversion");

                _objregistro = new clsEsquemaIntercambioFormato
                    (_dsresultado.Tables["dtformato"].Rows[0]
                    , _dsresultado.Tables["dtcabecera1"]
                    , _dsresultado.Tables["dtcabecera2"]
                    , _dsresultado.Tables["dtdetalle"]
                    , _dsresultado.Tables["dtpie"]
                    , _dsresultado.Tables["dtcolumnas"]
                    , _dsresultado.Tables["dtconversion"]
                    );
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            return _objregistro;

        }

        public Boolean InsertarEsquemaIntercambioFormato(clsEsquemaIntercambioFormato esquemaformato)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@nombre", esquemaformato.Nombre);
            _htparam.Add("@longitudregistro", esquemaformato.LongitudRegistro);
            _htparam.Add("@esquema", null);
            _htparam.Add("@fechacreacion", esquemaformato.FechaCreacion);
            _htparam.Add("@fechavigencia", esquemaformato.FechaVigencia);
            _htparam.Add("@fechacaduca", esquemaformato.FechaCaduca);
            _htparam.Add("@posinicio", esquemaformato.PosicionInicioDinamica);
            _htparam.Add("@idEstadoNGC", esquemaformato.IdEstadoNGC);
            _htparam.Add("@p_idtiposeparadorcolumna", esquemaformato.IdTipoSeparadorColumna);

            _intresultado = _DAO.EjecutarComando("EsquemaIntercambioFormato", "Insertar", _htparam);

            return (_intresultado > 0);
        }

        public Boolean ActualizarEsquemaIntercambioFormato(clsEsquemaIntercambioFormato esquemaformato)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@id", esquemaformato.Id);
            _htparam.Add("@nombre", esquemaformato.Nombre);
            _htparam.Add("@longitudregistro", esquemaformato.LongitudRegistro);
            _htparam.Add("@esquema", null);
            _htparam.Add("@fechacreacion", esquemaformato.FechaCreacion);
            _htparam.Add("@fechavigencia", esquemaformato.FechaVigencia);
            _htparam.Add("@fechacaduca", esquemaformato.FechaCaduca);
            _htparam.Add("@posinicio", esquemaformato.PosicionInicioDinamica);
            _htparam.Add("@idEstadoNGC", esquemaformato.IdEstadoNGC);
            _htparam.Add("@p_idtiposeparadorcolumna", esquemaformato.IdTipoSeparadorColumna);

            _intresultado = _DAO.EjecutarComando("EsquemaIntercambioFormato", "Actualizar", _htparam);

            return (_intresultado > 0);
        }

        public XmlSchemas ObtenerEsquemaIntercambioFormatoEsquema(Int32 id)
        {
            return new XmlSchemas();
        }

        public Boolean GuardarEsquemaIntercambioFormatoEsquema(XmlSchemas esquema)
        {
            return true;
        }

        public Boolean InsertarEsquemaIntercambioFormatoEstructura(clsListaEsquemaIntercambioFormatoEstructura esquemaformato)
        {
            Hashtable _htparam = new Hashtable();
            _htparam.Add("@id", esquemaformato.Formato.Id);
            _htparam.Add("@idnombre", esquemaformato.Formato.Nombre);

            return true;
        }

        public Boolean ActualizarEsquemaIntercambioFormatoEstructura(clsListaEsquemaIntercambioFormatoEstructura esquemaformato)
        {
            return true;
        }

        public clsListaEsquemaIntercambioFormatoEstructura ObtenerListaEsquemaIntercambioFormatoEstructura(int idformato)
        {
            Hashtable _htparam = new Hashtable();
            _htparam.Add("@idformato", idformato);
            DataSet _dsresultado;
            DataRow _drformato;
            DataTable _dtestructura;
            clsListaEsquemaIntercambioFormatoEstructura _objformatoestructura;

            _dsresultado = _DAO.EjecutarComandoConjuntoEntidad("EsquemaIntercambioFormato", "ObtenerFormatoEstructura", _htparam, "formatoestructura", "formato", "estructura");

            _drformato = (DataRow)_dsresultado.Tables["formato"].Rows[0];
            _dtestructura = (DataTable)_dsresultado.Tables["estructura"];

            _objformatoestructura = new clsListaEsquemaIntercambioFormatoEstructura(_drformato, _dtestructura);

            return _objformatoestructura;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idEstadoNGC"></param>
        /// <returns></returns>
        public bool EliminarEsquemaIntercambioFormato(short id, EstadoNGC idEstadoNGC)
        {
            Int32 _intresultado;
            Hashtable _htparam = new Hashtable();

            _htparam.Add("@id", id);
            _htparam.Add("@idEstadoNGC", idEstadoNGC);

            _intresultado = _DAO.EjecutarComando("esquemaintercambioformato", "eliminar", _htparam);

            return (_intresultado > 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Boolean AceptarEsquemaIntercambioFormato(Int16 id)
        {
            Int32 _intresultado;
            Hashtable _htparam = new Hashtable();

            _htparam.Add("@id", id);
            _htparam.Add("@idEstadoNGC", (Int16)EstadoNGC.Configurado);

            _intresultado = _DAO.EjecutarComando("esquemaintercambioformato", "aceptar", _htparam);

            return (_intresultado > 0);
        }

        #endregion

        #region IIntercambioEstructura Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idformato"></param>
        /// <returns></returns>
       
        /// <summary>
        /// Devuelve el numero de orden de la nueva columna a insertar.
        /// </summary>
        /// <param name="idformato"></param>
        /// <param name="tiporegistro"></param>
        /// <returns></returns>
        public Int16 EsquemaIntercambioEstructuraObtenerOrdenSiguiente(Int16 idformato, Int16 tiporegistro)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _drresultado;

            _htparam.Add("@idformato", idformato);
            _htparam.Add("@tiporegistro", tiporegistro);
            _htparam.Add("@ordensiguiente", 0);

            _drresultado = _DAO.EjecutarComando("EsquemaIntercambioEstructura", "ObtenerOrdenSiguiente", _htparam);

            return Convert.ToInt16(_htparam["@ordensiguiente"]);
        }

        #endregion

        #region IIntercambioConvertir Members

        /// <summary>
        /// Obtener los valores equivalentes de una estructura espec�fica.
        /// </summary>
        /// <param name="idestructura"></param>
        /// <returns></returns>
        public clsListaEsquemaIntercambioConversion ObtenerListaEsquemaIntercambioConversion(Int16 idestructura)
        {
            clsListaEsquemaIntercambioConversion _objlistaentidad;
            Hashtable _htparam = new Hashtable();

            _htparam.Add("@idestructura", idestructura);

            DataTable _objresultado = _DAO.EjecutarComandoEntidad("esquemaintercambioconversion", "obtenerlista", _htparam);

            _objlistaentidad = new clsListaEsquemaIntercambioConversion((DataTable)_objresultado);

            return _objlistaentidad;
        }

        /// <summary>
        /// Obtener los valores equivalentes de un formato espec�fico.
        /// </summary>
        /// <param name="idestructura"></param>
        /// <returns></returns>
        public clsListaEsquemaIntercambioConversion ObtenerListaEsquemaIntercambioConversionFormato(Int16 idformato)
        {
            clsListaEsquemaIntercambioConversion _objlistaentidad;
            Hashtable _htparam = new Hashtable();

            _htparam.Add("@idformato", idformato);

            DataTable _objresultado = _DAO.EjecutarComandoEntidad("esquemaintercambioconversion", "obtenerlistaformato", _htparam);

            _objlistaentidad = new clsListaEsquemaIntercambioConversion(_objresultado);

            return _objlistaentidad;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_objentidad"></param>
        /// <returns></returns>
        public Boolean InsertarEsquemaIntercambioConversion(clsEsquemaIntercambioConversion _objentidad)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@id", _objentidad.Id);
            _htparam.Add("@idestructura", _objentidad.IdEstructura);
            _htparam.Add("@idvalororigen", _objentidad.idValorOrigen);
            _htparam.Add("@idvalordestino", _objentidad.idValorDestino);
            _htparam.Add("@idEstadoNGC", _objentidad.IdEstadoNGC);

            _intresultado = _DAO.EjecutarComando("esquemaintercambioconversion", "insertar", _htparam);

            return (_intresultado > 0);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_objentidad"></param>
        /// <returns></returns>
        public Boolean ActualizarEsquemaIntercambioConversion(clsEsquemaIntercambioConversion _objentidad)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@id", _objentidad.Id);
            _htparam.Add("@idestructura", _objentidad.IdEstructura);
            _htparam.Add("@idvalororigen", _objentidad.idValorOrigen);
            _htparam.Add("@idvalordestino", _objentidad.idValorDestino);
            _htparam.Add("@idEstadoNGC", _objentidad.IdEstadoNGC);

            _intresultado = _DAO.EjecutarComando("esquemaintercambioconversion", "actualizar", _htparam);

            return (_intresultado > 0);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idestructura"></param>
        /// <returns></returns>
        public Boolean EliminarEsquemaIntercambioConversion(Int16 id, Int16 idestructura)
        {
            Hashtable _htparam = new Hashtable();
            Int32 _intresultado = 0;

            _htparam.Add("@id", id);
            _htparam.Add("@idestructura", idestructura);
            _htparam.Add("@idEstadoNGC", (Int16)EstadoNGC.Inactivo);

            _intresultado = _DAO.EjecutarComando("esquemaintercambioconversion", "eliminar", _htparam);

            return (_intresultado > 0);
        }

        #endregion

        #region IServicioConfiguracion Members

        /// <summary>
        /// Obtiene un registro de ServicioConfiguracion
        /// </summary>
        /// <param name="idservicioconfiguracion"></param>
        /// <returns></returns>
        public clsServicioConfiguracion ServicioConfiguracionObtenerRegistro(Int16 idservicioconfiguracion)
        {
            clsServicioConfiguracion _objentidadservicioconfiguracion;
            DataRow _drresultado;
            Hashtable _htparam = new Hashtable();

            _htparam.Add("@id", idservicioconfiguracion);

            _drresultado = _DAO.EjecutarComandoRegistro("servicioconfiguracion", "obtenerregistro", _htparam);

            _objentidadservicioconfiguracion = new clsServicioConfiguracion(_drresultado);

            return _objentidadservicioconfiguracion;
        }

        #endregion

        #region IIntercambioColumnas Members

        /// <summary>
        /// Obtener lista de la tabla EsquemaIntercambioColumnas
        /// </summary>
        /// <param name="tiporegistro"></param>
        /// <returns></returns>
        public clsListaEsquemaIntercambioColumnas EsquemaIntercambioColumnasObtenerLista(Int16 tiporegistro)
        {
            Hashtable _htparam = new Hashtable();
            DataTable _dtresultado;
            clsListaEsquemaIntercambioColumnas _objlistaentidad;

            _htparam.Add("@tiporegistro", tiporegistro);

            _dtresultado = _DAO.EjecutarComandoEntidad("EsquemaIntercambioColumnas", "ObtenerLista", _htparam);

            _objlistaentidad = new clsListaEsquemaIntercambioColumnas(_dtresultado);

            return _objlistaentidad;
        }

        /// <summary>
        /// Obtiene lista maestra de la tabla EsquemaIntercambioColumnas
        /// </summary>
        /// <param name="tiporegistro"></param>
        /// <returns></returns>
        public clsListaEntidadMaestra EsquemaIntercambioColumnasObtenerListaMaestra(Int16 tiporegistro)
        {
            Hashtable _htparam = new Hashtable();
            DataTable _dtresultado;
            clsListaEntidadMaestra _objlistaentidad;

            _htparam.Add("@tiporegistro", tiporegistro);

            _dtresultado = _DAO.EjecutarComandoEntidad("EsquemaIntercambioColumnas", "AyudaLista", _htparam);

            _objlistaentidad = new clsListaEntidadMaestra(_dtresultado);

            return _objlistaentidad;
        }

        #endregion

        #region Metodos para Gestionar Esquema de Carpetas.

        /// <summary>
        /// Obtiene el esquema de carpetas base.
        /// </summary>
        /// <param name="idproveedoractividad"></param>
        /// <returns></returns>
        public clsEsquemaCarpetaBase ObtenerEsquemaCarpetaBase(Int16 idproveedoractividad)
        {
            Hashtable _htparam = new Hashtable();
            clsEsquemaCarpetaBase _objesquema = new clsEsquemaCarpetaBase();

            _htparam.Add("@idproveedoractividad", idproveedoractividad);

            try
            {
                DataRow _drregistro = _DAO.EjecutarComandoRegistro("ProveedorActividad", "ObtenerEsquemaCarpeta", _htparam);
                _objesquema = new clsEsquemaCarpetaBase(_drregistro);
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

            return _objesquema;
        }

        /// <summary>
        /// Obtiene el esquema de carpeta en base a la unidad de negocio.
        /// </summary>
        /// <param name="idunidadnegocio"></param>
        /// <returns></returns>
        public clsEsquemaCarpetaBase ObtenerEsquemaCarpetaBaseUUNN(Int16 idunidadnegocio)
        {
            Hashtable _htparam = new Hashtable();
            clsEsquemaCarpetaBase _objesquema = new clsEsquemaCarpetaBase();

            _htparam.Add("@idunidadnegocio", idunidadnegocio);

            try
            {
                DataRow _drregistro = _DAO.EjecutarComandoRegistro("ProveedorActividad", "ObtenerEsquemaCarpetaUUNN", _htparam);
                _objesquema = new clsEsquemaCarpetaBase(_drregistro);
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

            return _objesquema;
        }

        /// <summary>
        /// Obtiene ruta completa de la carpeta de intercambio.
        /// </summary>
        /// <param name="idproveedoractividad">ID del proveedor/actividad.</param>
        /// <param name="idempresa">ID de la empresa del proceso.</param>
        /// <param name="idunidadnegocio">ID de la unidad de negocio.</param>
        /// <param name="idcentroservicio">ID del centro de servicio.</param>
        /// <param name="periodo">Periodo Comercial del proceso.</param>
        /// <returns>Retorna la ruta de intercambio completa.</returns>
        /// 
        public String ObtenerCarpetaIntercambio
            (Int16 idempresa
            , Int16 idunidadnegocio
            , Int16 idcentroservicio
            , Int16 idproveedoractividad
            , Int32 periodo
            , enumTipoIntercambio tipointercambio)
        {
            clsEsquemaCarpetaBase ocarpetabase = ObtenerEsquemaCarpetaBase(idproveedoractividad);
            clsEsquemaCarpeta ocarpeta = new clsEsquemaCarpeta();

            ocarpeta.IdActividad = ocarpetabase.IdActividad;
            ocarpeta.IdCentroServicio = idcentroservicio;
            ocarpeta.IdEmpresa = idempresa;
            ocarpeta.IdFlujo = ocarpetabase.IdFlujo;
            ocarpeta.IdServicio = ocarpetabase.IdServicio;
            ocarpeta.IdUnidadNegocio = idunidadnegocio;
            ocarpeta.Periodo = periodo.ToString();
            ocarpeta.RutaInicio = ocarpetabase.RutaInicio;
            ocarpeta.TipoIntercambio = tipointercambio;

            return clsGestorEsquemaIntercambio.CreaEsquemaCarpeta(ocarpeta);
        }

        /// <summary>
        /// Obtiene la carpeta destino.
        /// </summary>
        /// <param name="idservicio">ID del Servicio.</param>
        /// <param name="idflujo">ID del Flujo.</param>
        /// <param name="idempresa">ID de la Empresa.</param>
        /// <param name="idunidadnegocio">ID de la Unidad de Negocio.</param>
        /// <param name="idcentroservicio">ID del Centro de Servicio.</param>
        /// <param name="idactividad">ID de la Actividad.</param>
        /// <param name="periodo">Periodo Comercial.</param>
        /// <param name="tipointercambio">Enumerador Tipo de Intercambio (Envio,Recepci�n).</param>
        /// <returns></returns>
        public String ObtenerCarpetaIntercambio
            (Int16 idservicio
            , Int16 idflujo
            , Int16 idempresa
            , Int16 idunidadnegocio
            , Int16 idcentroservicio
            , Int16 idactividad
            , Int32 periodo
            , enumTipoIntercambio tipointercambio)
        {
            clsEsquemaCarpeta ocarpeta = new clsEsquemaCarpeta();

            // Obtener carpeta intercambio.

            Hashtable htParametro = new Hashtable();

            DataRow drcarpeta = _DAO.EjecutarComandoRegistro("ParametrosGlobales", "ObtenerCarpetaInicio", htParametro);

            ocarpeta.IdActividad = idactividad;
            ocarpeta.IdCentroServicio = idcentroservicio;
            ocarpeta.IdEmpresa = idempresa;
            ocarpeta.IdFlujo = idflujo;
            ocarpeta.IdServicio = idservicio;
            ocarpeta.IdUnidadNegocio = idunidadnegocio;
            ocarpeta.Periodo = periodo.ToString();
            ocarpeta.RutaInicio = drcarpeta[0].ToString();
            ocarpeta.TipoIntercambio = tipointercambio;

            return clsGestorEsquemaIntercambio.CreaEsquemaCarpeta(ocarpeta);
        }

        #endregion

    }

    public static class clsGestorEsquemaIntercambio
    {
        /// <summary>
        /// Crea el esquema de carpetas segun parametro.
        /// </summary>
        /// <param name="esquema"></param>
        public static string CreaEsquemaCarpeta(clsEsquemaCarpeta esquema)
        {
            string _stresquemacarpeta = "";

            try
            {
                // Validar Claves para Esquema
                ValidaEsquemaClave(esquema);

                // Valida Carpeta.
                // Si no existe la carpeta, la crea.
                _stresquemacarpeta = ValidaEsquemaCarpeta(esquema);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _stresquemacarpeta;
        }

        public static string CreaEsquemaCarpetaUUNN(clsEsquemaCarpeta esquema)
        {
            string _stresquemacarpeta = "";

            try
            {
                // Validar Claves para Esquema
                //ValidaEsquemaClave(esquema);

                // Valida Carpeta.
                // Si no existe la carpeta, la crea.
                _stresquemacarpeta = ObtenerEsquemaCarpetaUUNN(esquema);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _stresquemacarpeta;
        }

        public static string CreaEsquemaCarpeta
            (clsEsquemaCarpetaBase esquemabase
            , clsEsquemaCarpetaCliente esquemacliente
            , enumTipoIntercambio tipointercambio)
        {
            clsEsquemaCarpeta _objesquemacarpeta = new clsEsquemaCarpeta();

            _objesquemacarpeta.IdActividad = esquemabase.IdActividad;
            _objesquemacarpeta.IdCentroServicio = esquemacliente.IdCentroServicio;
            _objesquemacarpeta.IdEmpresa = esquemabase.IdEmpresa;
            _objesquemacarpeta.IdFlujo = esquemabase.IdFlujo;
            _objesquemacarpeta.IdServicio = esquemabase.IdServicio;
            _objesquemacarpeta.IdUnidadNegocio = esquemacliente.IdUnidadNegocio;
            _objesquemacarpeta.Periodo = esquemacliente.Periodo;
            _objesquemacarpeta.RutaInicio = esquemabase.RutaInicio;
            _objesquemacarpeta.TipoIntercambio = tipointercambio;

            return CreaEsquemaCarpeta(_objesquemacarpeta);
        }

        /// <summary>
        /// Chequea si existe el esquema de carpeta, de no existir, se procede a crear.
        /// </summary>
        /// <param name="esquema"></param>
        private static String ValidaEsquemaCarpeta(clsEsquemaCarpeta esquema)
        {
            String _stresquemacarpeta = ObtenerEsquemaCarpeta(esquema);
            DirectoryInfo _objdirectoryinfo = new DirectoryInfo(_stresquemacarpeta);

            if (!_objdirectoryinfo.Exists)
                _objdirectoryinfo.Create();

            return _stresquemacarpeta;
        }

        private static String ObtenerEsquemaCarpetaUUNN(clsEsquemaCarpeta esquema)
        {
            String _stresquemacarpeta = ObtenerEsquemaCarpetaUUNN(esquema, true);
            DirectoryInfo _objdirectoryinfo = new DirectoryInfo(_stresquemacarpeta);

            if (!_objdirectoryinfo.Exists)
                _objdirectoryinfo.Create();

            return _stresquemacarpeta;
        }



        /// <summary>
        /// Arma la ruta a la cual se tendr� acceso.
        /// </summary>
        /// <param name="esquema"></param>
        /// <returns></returns>
        public static string ObtenerEsquemaCarpeta(clsEsquemaCarpeta esquema)
        {
            try
            {
                ValidaEsquemaClave(esquema);

                String _strpath1 = Path.Combine(esquema.RutaInicio, "SE" + esquema.IdServicio.ToString().Trim().PadLeft(3, '0'));
                String _strpath2 = Path.Combine(_strpath1, "FL" + esquema.IdFlujo.ToString().Trim().PadLeft(3, '0'));
                String _strpath3 = Path.Combine(_strpath2, "AC" + esquema.IdActividad.ToString().Trim().PadLeft(3, '0'));
                String _strpath4 = Path.Combine(_strpath3, "EM" + esquema.IdEmpresa.ToString().Trim().PadLeft(3, '0'));
                String _strpath5 = Path.Combine(_strpath4, "UN" + esquema.IdUnidadNegocio.ToString().Trim().PadLeft(3, '0'));
                String _strpath6 = Path.Combine(_strpath5, "CS" + esquema.IdCentroServicio.ToString().Trim().PadLeft(3, '0'));
                String _strpath7 = Path.Combine(_strpath6, esquema.Periodo.Trim());
                String _strpathfinal = "";

                if (esquema.TipoIntercambio == enumTipoIntercambio.Envio)
                    _strpathfinal = Path.Combine(_strpath7, "Envio");
                else
                    _strpathfinal = Path.Combine(_strpath7, "Recepcion");

                return _strpathfinal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string ObtenerEsquemaCarpetaUUNN(clsEsquemaCarpeta esquema, Boolean valor)
        {
            try
            {
                ValidaEsquemaClave(esquema);

                String _strpath1 = Path.Combine(esquema.RutaInicio, "EM" + esquema.IdEmpresa.ToString().Trim().PadLeft(3, '0'));
                String _strpath2 = Path.Combine(_strpath1, "UN" + esquema.IdUnidadNegocio.ToString().Trim().PadLeft(3, '0'));
                String _strpath3 = Path.Combine(_strpath2, esquema.Periodo.Trim());
                String _strpathfinal = "";

                if (esquema.TipoIntercambio == enumTipoIntercambio.Envio)
                    _strpathfinal = Path.Combine(_strpath3, "Envio");
                else
                    _strpathfinal = Path.Combine(_strpath3, "Recepcion");

                return _strpathfinal;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string ObtenerEsquemaCarpeta
            (clsEsquemaCarpetaBase esquemabase
            , clsEsquemaCarpetaCliente esquemacliente
            , enumTipoIntercambio tipointercambio)
        {
            clsEsquemaCarpeta _objesquemacarpeta = new clsEsquemaCarpeta();

            _objesquemacarpeta.IdActividad = esquemabase.IdActividad;
            _objesquemacarpeta.IdCentroServicio = esquemacliente.IdCentroServicio;
            _objesquemacarpeta.IdEmpresa = esquemabase.IdEmpresa;
            _objesquemacarpeta.IdFlujo = esquemabase.IdFlujo;
            _objesquemacarpeta.IdServicio = esquemabase.IdServicio;
            _objesquemacarpeta.IdUnidadNegocio = esquemacliente.IdUnidadNegocio;
            _objesquemacarpeta.Periodo = esquemacliente.Periodo;
            _objesquemacarpeta.RutaInicio = esquemabase.RutaInicio;
            _objesquemacarpeta.TipoIntercambio = tipointercambio;

            return ObtenerEsquemaCarpeta(_objesquemacarpeta);
        }

        /// <summary>
        /// Valida que el esquema a crear sea valido.
        /// </summary>
        /// <param name="esquema"></param>
        private static void ValidaEsquemaClave(clsEsquemaCarpeta esquema)
        {
            if (String.IsNullOrEmpty(esquema.RutaInicio))
                throw new Exception("No se ha especificado Clave de Inicio.");

            if (esquema.IdServicio == 0)
                throw new Exception("No se ha especificado esquema carpeta Servicio.");

            if (esquema.IdFlujo == 0)
                throw new Exception("No se ha especificado esquema carpeta Flujo.");

            if (esquema.IdActividad == 0)
                throw new Exception("No se ha especificado esquema carpeta Actividad.");

            if (esquema.IdEmpresa == 0)
                throw new Exception("No se ha especificado esquema carpeta Empresa.");
            /* Comentado por JPlasencia, ya que se encesitaba consultar proceso sin centro servicio y sin UUNN para reporte N12
             * ya que esto limita q que se ingrese dichos campos para las tareas
            if (esquema.IdUnidadNegocio == 0)
                throw new Exception("No se ha especificado esquema carpeta Unidad de Negocio.");

            if (esquema.IdCentroServicio == 0)
                throw new Exception("No se ha especificado esquema carpeta Centro de Servicio.");
             */

            if (String.IsNullOrEmpty(esquema.Periodo))
                throw new Exception("No se ha especificado esquema carpeta Periodo.");
        }
    }

    public static class clsArchivo
    {
        private static String otlecturascargado;
        private static String _strarchivodestinocomprimido;
        private static String _strarchivoacopiar;
        private static String _strrutadestinoproveedor;
        //private Int16 _intidProveedorActividad;

        public static String OrdenTrabajoLecturas
        {
            get { return otlecturascargado; }
        }

        /// <summary>
        /// Obtiene solo el nombre del archivo especificado.
        /// </summary>
        /// <param name="rutanombrearchivo"></param>
        /// <returns></returns>
        public static String ObtenerNombreArchivo(String rutanombrearchivo)
        {
            return Path.GetFileName(rutanombrearchivo);
        }

        /// <summary>
        /// Obtiene el nombre del archivo sin extensi�n.
        /// </summary>
        /// <param name="rutanombrearchivo"></param>
        /// <returns></returns>
        public static String ObtenerNombreArchivoSinExtension(String rutanombrearchivo)
        {
            return Path.GetFileNameWithoutExtension(rutanombrearchivo);
        }

        /// <summary>
        /// Obtiene solo la extensi�n del archivo especificado.
        /// </summary>
        /// <param name="rutanombrearchivo"></param>
        /// <returns></returns>
        public static String ObtenerExtensionArchivo(String rutanombrearchivo)
        {
            return Path.GetExtension(rutanombrearchivo);
        }

        /// <summary>
        /// Obtiene solo la ruta del archivo especificado.
        /// </summary>
        /// <param name="rutanombrearchivo"></param>
        /// <returns></returns>
        public static String ObtenerRutaArchivo(String rutanombrearchivo)
        {
            return Path.GetDirectoryName(rutanombrearchivo);
        }

        /// <summary>
        /// Genera un nombre de archivo aleatorio con una extensi�n indicada como par�metro.
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static String ObtenerNombreAleatorio(String extension)
        {
            String _straleatorio = DateTime.Now.Hour.ToString().Trim() +
                        DateTime.Now.Minute.ToString().Trim() +
                        DateTime.Now.Second.ToString().Trim() +
                        DateTime.Now.Millisecond.ToString().Trim();

            if (_straleatorio.Length > 8)
                _straleatorio = _straleatorio.Substring(0, 8);

            return _straleatorio + "." + extension;
        }

        /// <summary>
        /// Crea temporalmente un archivo tomando como nombre el del archivo en formato DBF (8 caracteres)
        /// </summary>
        /// <param name="nombrearchivo"></param>
        /// <returns></returns>
        public static String ObtenerNombreFormatoDBF(String nombrearchivo, String CarpetaOrigen)
        {
            String tempFile = CarpetaOrigen + @"\" + ObtenerNombreAleatorio("dbf");
            File.Copy(CarpetaOrigen + @"\" + nombrearchivo, tempFile, true);
            File.SetAttributes(tempFile, FileAttributes.Hidden);

            return clsArchivo.ObtenerNombreArchivo(tempFile);
        }

        /// <summary>
        /// Obtiene la carpeta de trabajo temporal de la PC del usuario.
        /// </summary>
        /// <returns></returns>
        public static String ObtenerCarpetaTemporal()
        {
            return Path.GetTempPath();
        }

        /// <summary>
        /// Verifica si un archivo existe en la ruta correspondiente.
        /// </summary>
        /// <param name="strarchivo"></param>
        /// <returns></returns>
        public static Boolean ExisteArchivo(String strarchivo)
        {
            return File.Exists(strarchivo);
        }

        #region Validar Archivo Lecturas

        public static Boolean ValidarArchivoLecturas(String nombrearchivo, String ubicacion)
        {
            String _strnombrecolumna;

            // Contiene columnas del archivo de lecturas
            Hashtable _htcolumnaslecturas = new Hashtable();

            _htcolumnaslecturas.Add("ordtrabajo", null);
            _htcolumnaslecturas.Add("suministro", null);
            _htcolumnaslecturas.Add("concepto", null);
            _htcolumnaslecturas.Add("lecturao", null);
            _htcolumnaslecturas.Add("flectura", null);

            // Retornando el esquema del archivo dbf
            DataTable _dtlecturas = AbrirDBF(nombrearchivo, ubicacion, 1);

            // Validar que el archivo contenga las columnas necesarias.
            foreach (DictionaryEntry de in _htcolumnaslecturas)
            {
                _strnombrecolumna = de.Key.ToString();

                if (!_dtlecturas.Columns.Contains(_strnombrecolumna))
                    throw new Exception("El archivo "
                        + nombrearchivo.ToLower()
                        + " no tiene la columna "
                        + _strnombrecolumna.ToUpper());
            }

            // Obtener el ID de la OT de Lecturas Cargada en el archivo dbf
            otlecturascargado = _dtlecturas.Rows[0]["ordtrabajo"].ToString();

            return true;
        }

        /// <summary>
        /// Para retornar el contenido de un determinado archivo dbf o retornar el esquema de la tabla
        /// </summary>
        /// <param name="nombrearchivoDBF"></param>
        /// <param name="carpetaorigen"></param>
        /// <param name="cantidadregistros"></param>
        /// <returns></returns>
        public static DataTable AbrirDBF(String nombrearchivoDBF, String carpetaorigen, Int32 cantidadregistros)
        {
            String consultasql;

            nombrearchivoDBF = ObtenerNombreArchivo(nombrearchivoDBF);
            String ArchivoTemporal = nombrearchivoDBF;

            // Renombrar el archivo con el formato DBF (8 caracteres)
            if (ObtenerNombreArchivoSinExtension(nombrearchivoDBF).Length > 8)
                ArchivoTemporal = ObtenerNombreFormatoDBF(nombrearchivoDBF, carpetaorigen);

            // Obteniendo la Consulta para obtener la informacion del DBF
            if (cantidadregistros < 0)
                consultasql = "Select * From " + ArchivoTemporal;
            else
                consultasql = "Select Top " + cantidadregistros.ToString().Trim() + " * From " + ArchivoTemporal;

            // Acceso a DBF.
            OleDbConnection _objcnn = ObtenerConexion(TipoConexionOLEDB.OleDbDBF, carpetaorigen);

            // Ejecutar Consulta.
            OleDbDataAdapter _objcmd = new OleDbDataAdapter(consultasql, _objcnn);

            DataTable _dt = new DataTable();

            try
            {
                // Llenando el DataTable con la informaci�n del DBF u obteniendo el esquema del mismo
                _objcmd.Fill(_dt);
            }
            catch (OleDbException)
            {
                throw new Exception("El formato del Archivo no es el correcto");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            // Eliminando archivo temporal
            if (clsArchivo.ObtenerNombreArchivoSinExtension(nombrearchivoDBF).Length > 8)
                File.Delete(carpetaorigen + @"\" + ArchivoTemporal);

            return _dt;
        }

        /// <summary>
        /// Obtener Conexion OLEDB.
        /// </summary>
        /// <param name="tipoconexionoledb"></param>
        /// <param name="carpetaorigen"></param>
        /// <returns></returns>
        public static OleDbConnection ObtenerConexion(TipoConexionOLEDB tipoconexionoledb, String carpetaorigen)
        {
            String _strconexion = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + carpetaorigen;

            switch (tipoconexionoledb)
            {
                case TipoConexionOLEDB.OleDbDBF:
                    _strconexion += ";Extended Properties=dBASE IV;User ID=Admin;Password=;";
                    break;

                default:
                    break;
            }

            return new OleDbConnection(_strconexion);
        }

        #endregion Validar Archivo Lecturas

        #region Comprimir archivo

        public static void CargarArchivo(String nombrearchivo, String rutadestinoproveedor)
        {
            // Comprime el Archivo y luego envia
            _strrutadestinoproveedor = rutadestinoproveedor;
            ComprimirArchivo(nombrearchivo);
        }

        public static void ComprimirArchivo(String nombrearchivo)
        {
            String strdirectorio = Path.GetDirectoryName(nombrearchivo);
            String strarchivodestino = Path.GetFileName(nombrearchivo) + ".zip";

            // Ruta en la que se creara el archivo comprimido
            _strarchivoacopiar = Path.Combine(strdirectorio, strarchivodestino);

            // si ya existe el archivo en la pc cliente lo elimina
            if (File.Exists(_strarchivoacopiar)) File.Delete(_strarchivoacopiar);

            //FileStream _objstreamorigen = new FileStream(nombrearchivo, FileMode.Open, FileAccess.Read, FileShare.Read);
            //byte[] _objbuffer = new byte[_objstreamorigen.Length];

            //int count = _objstreamorigen.Read(_objbuffer, 0, _objbuffer.Length);
            //if (count != _objbuffer.Length)
            //{
            //    _objstreamorigen.Close();
            //    throw new Exception("no se puede leer el archivo");
            //}
            //_objstreamorigen.Close();

            //MemoryStream _objmemorystream = new MemoryStream();
            //GZipStream objstreamcompresion = new GZipStream(_objmemorystream, CompressionMode.Compress, true);
            //objstreamcompresion.BeginWrite(_objbuffer, 0, _objbuffer.Length, new AsyncCallback(CompresionCompletada), new Stream[] { objstreamcompresion, _objmemorystream });

            using (FileStream sourcefile = File.OpenRead(nombrearchivo))
            {
                using (FileStream destfile = File.Create(_strarchivoacopiar))
                {
                    using (GZipStream compStream = new GZipStream(destfile, CompressionMode.Compress))
                    {
                        Byte[] data = new Byte[sourcefile.Length];
                        sourcefile.Read(data, 0, data.Length);
                        compStream.Write(data, 0, data.Length);
                    }
                }
            }

            EnviarArchivoServidor();
        }

        private static void CompresionCompletada(IAsyncResult resultado)
        {
            Stream[] _lststream = (Stream[])resultado.AsyncState;
            GZipStream objstreamcompresion = (GZipStream)_lststream[0];
            MemoryStream _objmemorystream = (MemoryStream)_lststream[1];
            objstreamcompresion.Close();
            _objmemorystream.Position = 0;
            FileStream _objstreamdestino = new FileStream(_strarchivoacopiar, FileMode.OpenOrCreate);
            _objmemorystream.WriteTo(_objstreamdestino);
            _objstreamdestino.Close();
            EnviarArchivoServidor();
        }

        private static void EnviarArchivoServidor()
        {
            //FileStream destFile;
            ////String archivodestino = Path.Combine(_strrutadestinoproveedor, Path.GetFileNameWithoutExtension(_strarchivocomprimido));
            //String archivodestino = Path.Combine(_strrutadestinoproveedor, Path.GetFileName(_strarchivoacopiar));
            //String archivodestinodescomprimido = Path.Combine(_strrutadestinoproveedor, Path.GetFileNameWithoutExtension(_strarchivocomprimido));

            //// Comprobamos si el archivo ya existe para eliminarlo y crearlo nuevamente
            //if (File.Exists(archivodestino))
            //    File.Delete(archivodestino);

            //// Copiando el Archivo comprimido
            //File.Copy(_strarchivocomprimido, archivodestino);

            //// Creando el Archivo Descomprimido
            //destFile = File.Create(archivodestinodescomprimido);

            //// Ahora se descomprime el archivo
            //MemoryStream ms = new MemoryStream();
            //GZipStream objstreamcompresion = new GZipStream(ms, CompressionMode.Decompress);
            //Int32 theByte = objstreamcompresion.ReadByte();

            //while (theByte != -1)
            //{
            //    destFile.WriteByte((byte)theByte);
            //    theByte = objstreamcompresion.ReadByte();
            //}

            // 1. Copiar el Archivo comprimido
            _strarchivodestinocomprimido = Path.Combine(_strrutadestinoproveedor, Path.GetFileName(_strarchivoacopiar));
            File.Copy(_strarchivoacopiar, _strarchivodestinocomprimido, true);
            String _strarchivodestino = Path.Combine(_strrutadestinoproveedor, Path.GetFileNameWithoutExtension(_strarchivodestinocomprimido));

            // Eliminando el archivo local (comprimido)
            File.Delete(_strarchivoacopiar);

            // 2. Descomprimir el archivo enviado
            using (FileStream sourcefile = File.OpenRead(_strarchivodestinocomprimido))
            {
                using (FileStream destfile = File.Create(_strarchivodestino))
                {
                    using (GZipStream compStream = new GZipStream(sourcefile, CompressionMode.Decompress))
                    {
                        Int32 data;
                        while ((data = compStream.ReadByte()) != -1)
                        {
                            destfile.WriteByte((byte)data);
                        }
                    }
                }
            }

            // Eliminando el archivo comprimido copiado en el servidor
            File.Delete(_strarchivodestinocomprimido);
        }

        #endregion Comprimir archivo

        public static void EliminarArchivo(String argArchivoEliminar)
        {
            try
            {
                if (File.Exists(argArchivoEliminar))
                    File.Delete(argArchivoEliminar);
            }
            catch (Exception ex)
            {
                StringBuilder mensajeError = new StringBuilder();

                mensajeError.AppendLine("El archivo ya existe y se ha intentado eliminar sin �xito.");
                mensajeError.AppendLine();
                mensajeError.AppendLine("Indicaciones:");
                mensajeError.AppendLine("1.Verifique que el archivo no sea de solo lectura.");
                mensajeError.AppendLine("2.Verifique que el archivo no est� siendo usado por otro proceso.");
                mensajeError.AppendLine();
                mensajeError.AppendLine("Detalle:");
                mensajeError.AppendLine(ex.Message);

                throw new Exception(mensajeError.ToString());
            }
        }
    }
}
