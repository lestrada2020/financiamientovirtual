﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.General;
using SISAC.Negocio.General;
using System.Threading.Tasks;
using System.Web.Http;

namespace SISAC.Site.General.Controllers
{
    public class ProyectosController : ApiController, IProyectos
    {
        #region Field

        private static readonly clsNegocioProyectos _Proy = clsNegocioProyectos.Instancia;

        #endregion Field

        #region Public

        public async Task<IHttpActionResult> AyudaLista(clsParametro param)
        {
            var result = await _Proy.AyudaLista(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ListIniciativas(clsParametro param)
        {
            var result = await _Proy.ListIniciativas(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerNivelesFase(clsParametro param)
        {
            var result = await _Proy.ObtenerNivelesFase(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerUsuario(clsParametro param)
        {
            var result = await _Proy.ObtenerUsuario(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerIniciativa(clsParametro param)
        {
            var result = await _Proy.ObtenerIniciativa(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarHito(clsParametro param)
        {
            var result = await _Proy.RegistrarHito(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerHito(clsParametro param)
        {
            var result = await _Proy.ObtenerHito(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ExportarIniciativa(clsParametro param)
        {
            var result = await _Proy.ExportarIniciativa(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ExportarImporte(clsParametro param)
        {
            var result = await _Proy.ExportarImporte(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> RegistrarImporte(clsParametro param)
        {
            var result = await _Proy.RegistrarImporte(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> ObtenerImporte(clsParametro param)
        {
            var result = await _Proy.ObtenerImporte(param);

            return Ok(result);
        }

        public async Task<IHttpActionResult> EditarIniciativa(clsParametro param)
        {
            var result = await _Proy.EditarIniciativa(param);

            return Ok(result);
        }

        #endregion Public
    }
}