﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Threading;

namespace SISAC.Data.NTCSE
{
    #region clsConsultaInterrupcionDAO

    public sealed class clsConsultaInterrupcionDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsConsultaInterrupcionDAO> _Instancia = new Lazy<clsConsultaInterrupcionDAO>(() => new clsConsultaInterrupcionDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsConsultaInterrupcionDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsConsultaInterrupcionDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataTable ObtenerDatosInterrupcion(clsParametro entidad)
        {
            try
            {
                entidad.Parametros["DateFormat"] = "yyyyMMdd HH:mm:ss";

                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("InterrupcionMonitoreo", "ObtenerDatosInterrupcion", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDatosReposicionParcialInterrupcion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("InterrupcionItemMonitoreo", "ObtenerReposicionParcialInterrupcion", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDatosGPSSuministro(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("InterrupcionMonitoreo", "ObtenerDatosGPSSuministro", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDatosGPSSuministroLejano(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("InterrupcionMonitoreo", "ObtenerDatosGPSSuministroLejano", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerSuministrosAfectadosGPS(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("InterrupcionMonitoreo", "ObtenerSuministrosAfectadosGPS", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }

    #endregion clsConsultaInterrupcionDAO
}