﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Threading;

namespace SISAC.Data.NTCSE
{
    #region clsIndicadorSaifiSaidiDAO

    public sealed class clsIndicadorSaifiSaidiDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsIndicadorSaifiSaidiDAO> _Instancia = new Lazy<clsIndicadorSaifiSaidiDAO>(() => new clsIndicadorSaifiSaidiDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsIndicadorSaifiSaidiDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsIndicadorSaifiSaidiDAO()
        {
        }

        #endregion Constructor

        #region Public

        public DataTable ObtenerTabularIndicadorAcumulado(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("RptSaifiSaidiGrupoDistriluz", "ObtenerTabularIndicadorAcumulado", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerIndicadorAcumulado(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("RptSaifiSaidiGrupoDistriluz", "ObtenerIndicadorAcumulado", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerEvolucionIndicadorAcumulado(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("RptSaifiSaidiGrupoDistriluz", "ObtenerEvolucionIndicadorAcumulado", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerProcesosxSimulacion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("RptSaifiSaidiGrupoDistriluz", "ObtenerProcesosxSimulacion", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AyudaLista(clsParametro entidad)
        {
            try
            {
                String entity = "";
                String action = "AyudaLista";

                if (entidad.Parametros.ContainsKey("Entity")) entity = entidad.Parametros["Entity"];
                if (entidad.Parametros.ContainsKey("Action")) action = entidad.Parametros["Action"];

                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad(entity, action, param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable ObtenerIndicadorAcumuladoMovil(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("RptSaifiSaidiGrupoDistriluz", "ObtenerIndicadorAcumuladoMovil", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }

    #endregion clsIndicadorSaifiSaidiDAO
}