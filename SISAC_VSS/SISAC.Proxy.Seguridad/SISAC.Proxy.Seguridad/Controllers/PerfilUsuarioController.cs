﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.Seguridad;
using SISAC.Util.Tool;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace SISAC.Proxy.Seguridad.Controllers
{
    public class PerfilUsuarioController : ApiController, IPerfilUsuario
    {
        #region Field

        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsService _Service = clsService.Instancia;

        #endregion Field

        #region Private

        private T SendData<T>(String action, Object param)
        {
            try
            {
                String appKeySite = "SiteSeguridad";
                String controller = "PerfilUsuario";

                if (!ConfigurationManager.AppSettings.AllKeys.Contains(appKeySite)) throw new Exception(String.Format("No existe el AppKey \"{0}\".", appKeySite));

                var site = ConfigurationManager.AppSettings[appKeySite].ToString();
                var url = String.Format("{0}/Api/{1}/{2}", site, controller, action);

                return _Service.SendData<T>(url, param);
            }
            catch (Exception ex)
            {
                return _Util.SetError<T>(ex.Message);
            }
        }

        #endregion Private

        #region Public

        public IHttpActionResult ValidarToken(clsParametro param)
        {
            var result = SendData<clsResultado>("ValidarToken", param);

            return Ok(result);
        }

        public IHttpActionResult ListAppConfig(clsParametro param)
        {
            var result = SendData<clsResultado>("ListAppConfig", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerDatosUsuario(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerDatosUsuario", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerModulosAplicacionPorUsuario(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerModulosAplicacionPorUsuario", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerRolesAplicacionPorUsuario(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerRolesAplicacionPorUsuario", param);

            return Ok(result);
        }

        public IHttpActionResult ObtenerOpcionesPorModuloAplicacionUsuario(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerOpcionesPorModuloAplicacionUsuario", param);

            return Ok(result);
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public IHttpActionResult ObtenerRestriccionVistaUsuario(clsParametro param)
        {
            var result = SendData<clsResultado>("ObtenerRestriccionVistaUsuario", param);

            return Ok(result);
        }

        #endregion

        #endregion Public
    }
}