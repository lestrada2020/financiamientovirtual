﻿using SISAC.Data.Seguridad;
using SISAC.Entidad.Maestro;
using SISAC.Negocio.Maestro;
using SISAC.Util.Tool;
using System;
using System.Linq;
using System.Threading;

namespace SISAC.Negocio.Seguridad
{
    public sealed class clsNegocioPerfilUsuario
    {
        #region Field

        private static readonly clsPerfilUsuarioDAO _PerfilUsuario = clsPerfilUsuarioDAO.Instancia;
        private static readonly clsUtil _Util = clsUtil.Instancia;
        private static readonly clsNegocioException _Log = clsNegocioException.Instancia;
        private static readonly Lazy<clsNegocioPerfilUsuario> _Instancia = new Lazy<clsNegocioPerfilUsuario>(() => new clsNegocioPerfilUsuario(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNegocioPerfilUsuario Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNegocioPerfilUsuario()
        {
        }

        #endregion Constructor

        #region Public

        public clsResultado ValidarToken(clsParametro entidad)
        {
            try
            {
                var row = _PerfilUsuario.ValidarToken(entidad);

                var result = new clsResultado();
                result.Datos = row.ToDictionary();

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ListAppConfig(clsParametro entidad)
        {
            try
            {
                var table = _PerfilUsuario.ListAppConfig(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Log.GrabarLog(ex, entidad);
            }
        }

        public clsResultado ObtenerDatosUsuario(clsParametro entidad)
        {
            try
            {
                var table = _PerfilUsuario.ObtenerDatosUsuario(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerModulosAplicacionPorUsuario(clsParametro entidad)
        {
            try
            {
                var table = _PerfilUsuario.ObtenerModulosAplicacionPorUsuario(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerRolesAplicacionPorUsuario(clsParametro entidad)
        {
            try
            {
                var table = _PerfilUsuario.ObtenerRolesAplicacionPorUsuario(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        public clsResultado ObtenerOpcionesPorModuloAplicacionUsuario(clsParametro entidad)
        {
            try
            {
                var table = _PerfilUsuario.ObtenerOpcionesPorModuloAplicacionUsuario(entidad);

                var result = new clsResultado();
                result.Datos = table;

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public clsResultado ObtenerRestriccionVistaUsuario(clsParametro entidad)
        {
            try
            {
                var table = _PerfilUsuario.ObtenerRestriccionVistaUsuario(entidad);

                var result = new clsResultado
                {
                    Datos = table
                };

                return result;
            }
            catch (Exception ex)
            {
                return _Util.SetError<clsResultado>(ex);
            }
        }

        #endregion

        #endregion Public
    }
}