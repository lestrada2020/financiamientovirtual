﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SISAC.Util.Tool;
using System.Threading;
using SISAC.Entidad.Facturacion.ImpresionRecibo;
using System.Data.SqlClient;

namespace SISAC.Data.Facturacion
{
    #region clsSolicitudImpresionRecibo

    public sealed class clsSolicitudImpresionReciboDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsSolicitudImpresionReciboDAO> _Instancia = new Lazy<clsSolicitudImpresionReciboDAO>(() => new clsSolicitudImpresionReciboDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsSolicitudImpresionReciboDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsSolicitudImpresionReciboDAO()
        {
        }

        #endregion Constructor

        #region Public
        
        public DataTable ObtenerListaSolicitudImpresionRecibos(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresion", "ObtenerSolicitudes", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AyudaLista(clsParametro entidad)
        {
            try
            {
                String entity = "";
                String action = "AyudaLista";

                if (entidad.Parametros.ContainsKey("Entity")) entity = entidad.Parametros["Entity"];
                if (entidad.Parametros.ContainsKey("Action")) action = entidad.Parametros["Action"];

                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad(entity, action, param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerSolicitudImpresionRecibo(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresion", "ObtenerSolicitudImpresion", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerDetalleSolicitudImpresionRecibo(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                //"ImpRec.SolicitudImpresionDetalle", "ObtenerDetalleSolicitudImpresion"
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresionDetalle", "ObtenerDetalleSolicitudImpresionLista", param);
                entidad.Parametros["totalregistros"] = param["@p_totalregistros"].ToString();
                entidad.Parametros["totalpaginas"] = param["@p_totalpaginas"].ToString();
                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ActualizarEstado(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresion", "ActualizarEstado", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerAlcancePersonalProveedorActividad(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.ProveedorPersonalActividad", "ObtenerAlcance", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Req-002 - 201902 (Mod. por: AMASSA CANVIA)

        public DataTable ConsultarSuministrosNotificar(clsParametro entidad)//3
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresion", "ConsultarSuministrosNotificar", param);
                entidad.Parametros["totalregistros"] = param["@p_totalregistros"].ToString();
                entidad.Parametros["totalpaginas"] = param["@p_totalpaginas"].ToString();
                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ConsultarMonitoreoTareasLista(clsParametro entidad)//4
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("TareaEjecucion", "ConsultarMonitoreoTareasPaginadas", param);
                entidad.Parametros["totalregistros"] = param["@p_totalregistros"].ToString();
                entidad.Parametros["totalpaginas"] = param["@p_totalpaginas"].ToString();
                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConsultarLogProceso(clsParametro entidad)//5
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("TareaEjecucion", "ConsultarLogProceso", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConsultarLogTareaEjecucion(clsParametro entidad)//6
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("TareaEjecucion", "ConsultarLogTareaEjecucion", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ConsultarLogInconsistencias(clsParametro entidad)//7
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("TareaEjecucion", "ConsultarLogInconsistencias", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Req-002 - 201909 (Mod. por: ECALDAS CANVIA)

        public DataTable ObtenerListaSolicitudImpresionRecibosPaginada(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresion", "ObtenerSolicitudesPaginadas", param);
                entidad.Parametros["totalregistros"] = param["@p_totalregistros"].ToString();
                entidad.Parametros["totalpaginas"] = param["@p_totalpaginas"].ToString();

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //CanviaDev_2020-05-27
        public DataTable ObtenerSolicitudesImpresionPaginadasDeTemporal(clsParametro entidad)
        {
            using (SqlConnection sqlconnection = _DAO.GetSqlConnection())
            {
                try
                {
                    Hashtable param = entidad.Parametros.ToHashtableDAO();

                    sqlconnection.Open();

                    string strQuery = _DAO.EjecutarComandoEscalar("ImpRec.SolicitudImpresion"
                                             , "SolicitudesPaginadasCrearTemporal"
                                             , param, sqlconnection).ToString();
                    _DAO.EjecutarComando(strQuery, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasInsertarTemporalIds",
                        param, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasInsertarTemporal",
                        param, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasActualizarTotalesTemporal",
                        param, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasActualizarProcesamientoTemporal",
                        param, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasActualizarNotifCorreoTemporal",
                        param, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasActualizarNotifSMSTemporal",
                        param, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasActualizarNotifPushTemporal",
                        param, sqlconnection);

                    _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "SolicitudesPaginadasActualizarGenPDFTemporal",
                        param, sqlconnection);

                    DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresion", "ObtenerSolicitudesPaginadasTemporal",
                        param, sqlconnection);

                    entidad.Parametros["totalregistros"] = param["@p_totalregistros"].ToString();
                    entidad.Parametros["totalpaginas"] = param["@p_totalpaginas"].ToString();

                    return table;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        //CanviaDev_2020-05-27:Fin

        public DataTable InsertarSolicitudImpresionTarea(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.SolicitudImpresionTarea", "Insertar", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ObtenerConfiguracion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.ParametroImpresionRecibo", "ObtenerParametrosxEmpresa", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GuardarConfiguracion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.ParametroImpresionRecibo", "InsertarActualizarParametro", param);

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerListaAplicacionNotificacion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.AplicacionNotificacion", "ObtenerAplicacionesNotificacion", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ObtenerListaProcesoNotificacion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ImpRec.ProcesoNotificacion", "ObtenerProcesosNotificacion", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ClsListaControlGenerarArchivoImpresion ObtenerListaArchivosRecibos(int idTarea)
        {
            var param = new Hashtable
            {
                { "@idtarea", idTarea }
            };

            try
            {
                DataTable objTabla = _DAO.EjecutarComandoEntidad("recibo", "obtenerlistaarchivos", param);
                var objLista = new ClsListaControlGenerarArchivoImpresion(objTabla);

                return objLista;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int ObtenerIdTareaSolicitudImpresion(clsParametro entidad, int idProceso)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                param.Add("@p_idproceso", idProceso);
                int idTarea = (int)_DAO.EjecutarComandoEscalar("ImpRec.SolicitudImpresionTarea", "ObtenerTarea",
                    param);

                return idTarea;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool EsExisteSolicitudImpresionNotificacionPendiente(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                bool esNotificacionPendiente = (bool)_DAO.EjecutarComandoEscalar("ImpRec.SolicitudImpresion",
                    "ValidarNotificacionPendiente", param);

                return esNotificacionPendiente;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool EsExisteSuministrosNotificacionPendiente(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                bool esNotificacionPendiente = (bool)_DAO.EjecutarComandoEscalar("ImpRec.NotificacionSuministros",
                    "ValidarNotificacionPendiente", param);

                return esNotificacionPendiente;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ObtenerDatosEjecucionTareaPorId(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("TareaProgramacion", "ObtenerEjecucionPorId", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ObtenerDatosProveedorPorIdUsuario(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad("ProveedorPersonal", "ObtenerProveedorxIdUsuario", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ActualizarPasswordArchivoRecibos(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "ActualizarPasswordArchivoRecibos", param);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ReenviarCorreoProcesamientoArchivoImpresion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                _DAO.EjecutarComando("ImpRec.SolicitudImpresion", "ConfirmarTerminoProcesamiento", param);

            }
            catch (Exception)
            {
                throw;
            }
        }

        //CanviaDev_2020-06-01
        public DataTable ValidarSuministrosImportarNotificacion(clsParametro entidad)
        {
            try
            {
                Hashtable param = entidad.Parametros.ToHashtableDAO();
                DataTable table = _DAO.EjecutarComandoEntidad(
                    "ImpRec.SolicitudImpresion", "NotificacionValidarSuministrosImportar", param);

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }
        //CanviaDev_2020-06-01

        #endregion

        #endregion Public
    }

    #endregion clsSolicitudImpresionRecibo

}
