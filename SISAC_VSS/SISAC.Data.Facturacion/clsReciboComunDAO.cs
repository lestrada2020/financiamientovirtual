﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

namespace SISAC.Data.Facturacion
{
    public sealed class clsReciboComunDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsReciboComunDAO> _Instancia = new Lazy<clsReciboComunDAO>(() => new clsReciboComunDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsReciboComunDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsReciboComunDAO()
        {
        }

        #endregion Constructor

        #region Public
            public SqlDataReader ExtraerDatosMenor_Cabecera(SqlConnection sqlc
                                                           , Int16 esruta
                                                           , Int16 esduplicado
                                                           , String archivoblanco)
            {
                Hashtable _htparametros = new Hashtable();

                _htparametros["@esruta"] = esruta;
                _htparametros["@esduplicado"] = esduplicado;
                _htparametros["@archivoblanco"] = archivoblanco;
                _DAO.EjecutarComando("Recibos"
                                         , "PrepararCabeceraMenor"
                                         , _htparametros
                                         , sqlc);

                SqlCommand sqlCabecera = new SqlCommand("Recibos_ObtenerInformacionCabecera_Pa", sqlc);
                sqlCabecera.Parameters.Add(new SqlParameter("@archivoblanco", SqlDbType.VarChar, 120));
                sqlCabecera.Parameters[0].Value = archivoblanco;

                sqlCabecera.CommandType = CommandType.StoredProcedure;

                return (sqlCabecera.ExecuteReader());
            }

            public SqlDataReader ExtraerDatosMenor_Concepto(SqlConnection sqlc)
            {
                Hashtable _htparametros = new Hashtable();
                String _strQuery = "";

                #region Crear Tabla Temporal de Conceptos

                _htparametros.Add("@cadena", "");
                _DAO.EjecutarComando("recibos"
                                         , "creartabletemporalconceptos"
                                         , _htparametros, sqlc);
                _strQuery = _htparametros["@cadena"].ToString();

                _DAO.EjecutarComando(_strQuery, sqlc);

                #endregion

                #region Obtener los Concepto IGV

                _htparametros.Clear();
                _htparametros["@idigv"] = (Int16)4; // CacheConfiguracion.IdConceptoIGV;
                _DAO.EjecutarComando("recibos"
                                         , "adicionarconceptosimprimir"
                                         , _htparametros
                                         , sqlc);

                #endregion

                #region Obtener los Conceptos de Energia

                _htparametros.Clear();
                _htparametros["@idenermenor30"] = (Int16)73;
                _htparametros["@idener30y1001ra"] = (Int16)74;
                _htparametros["@idener30y1002da"] = (Int16)165;
                _htparametros["@idenermayor100"] = (Int16)75;
                _DAO.EjecutarComando("recibos"
                                         , "actualizarconceptosenergia"
                                         , _htparametros
                                         , sqlc);

                #endregion

                #region Obtener el Concepto Alumbrado Publico

                _htparametros.Clear();
                _htparametros["@idconceptoap"] = (Int16)3;
                _DAO.EjecutarComando("recibos"
                                         , "actualizarconceptoalicuota"
                                         , _htparametros
                                         , sqlc);

                #endregion

                #region Actualizar Cuotas de Convenio

                _DAO.EjecutarComando("recibos_actualizarcuotaconvenio_pa", sqlc);

                #endregion

                #region Insertar Base Imponible

                _htparametros.Clear();
                _htparametros["@grupo"] = (Int16)2;
                _DAO.EjecutarComando("recibos"
                                         , "insertarbaseimponible"
                                         , _htparametros
                                         , sqlc);
                #endregion

                #region Insertar Cargo por Reposicion y Mantenimiento

                _htparametros.Clear();
                _htparametros["@idmantenimiento"] = (Int16)12;
                _htparametros["@idreposicion"] = (Int16)13;
                _htparametros["@grupo"] = (Int16)1;
                _DAO.EjecutarComando("recibos"
                                         , "insertarcargorepomante"
                                         , _htparametros
                                         , sqlc);
                #endregion

                #region Insertar Mes y Deuda

                _htparametros.Clear();
                _htparametros["@grupo"] = (Int16)5;
                _DAO.EjecutarComando("recibos"
                                         , "insertarmesydeuda"
                                         , _htparametros
                                         , sqlc);
                #endregion

                #region Insertar Aplicaciones A Favor

                _htparametros.Clear();
                _htparametros["@grupo"] = (Int16)6;
                _DAO.EjecutarComando("recibos"
                                         , "insertaraplicacionesafavor"
                                         , _htparametros
                                         , sqlc);
                #endregion

                #region Insertar Fose

                _htparametros.Clear();
                _htparametros["@grupo"] = (Int16)7;
                _DAO.EjecutarComando("recibos"
                                         , "insertarfose"
                                         , _htparametros
                                         , sqlc);
                #endregion

                #region Insertar Mensaje Debito Automatico

                _htparametros.Clear();
                _htparametros["@grupo"] = (Int16)8;
                _DAO.EjecutarComando("recibos"
                                         , "insertarmensajesdebitoautomatico"
                                         , _htparametros
                                         , sqlc);
                #endregion

                SqlCommand sqlConcepto = new SqlCommand("recibos_ver_conceptos_menor_pa", sqlc);

                sqlConcepto.CommandType = CommandType.StoredProcedure;
                return (sqlConcepto.ExecuteReader());
            }

            public SqlDataReader ExtraerDatosMenor_Lectura(SqlConnection sqlc)
            {
                SqlCommand sqlLectura = new SqlCommand("recibos_ver_lectura_menor_pa", sqlc);
                sqlLectura.CommandType = CommandType.StoredProcedure;
                return (sqlLectura.ExecuteReader());
            }

            public DataTable ExtraerDatosMenor_Consumo(SqlConnection sqlc)
            {
                SqlCommand sqlConsumo = new SqlCommand("Recibos_Ver_Consumo_Menor_pa", sqlc);

                sqlConsumo.CommandType = CommandType.StoredProcedure;
                //return (sqlConsumo.ExecuteReader());

                SqlDataReader _sqldatareader = sqlConsumo.ExecuteReader();

                DataTable _dtesquema = _sqldatareader.GetSchemaTable();
                DataTable _dtconsumo = new DataTable("Inf");

                _dtconsumo.Load(_sqldatareader, LoadOption.OverwriteChanges);

                DataTable _dtnuevo = new DataTable("clsReciboMenorConsumo");

                foreach (DataRow drow in _dtesquema.Rows)
                {
                    String columnName = System.Convert.ToString(drow["ColumnName"]);
                    DataColumn column = new DataColumn(columnName, (Type)(drow["DataType"]));
                    _dtnuevo.Columns.Add(column);
                }

                _dtnuevo.Merge(_dtconsumo, true);

                //_dtnuevo.BeginLoadData();

                //foreach (DataRow row in _dtconsumo.Rows)
                //{
                //    _dtnuevo.LoadDataRow(row.ItemArray, true);
                //}

                //_dtnuevo.EndLoadData();

                Int32 _intperiodo;
                Int32 _intperiodoasig;
                Int16 _intidconceptoeat = (Int16)ConceptosFacturar.EAT; // 1
                Int32 _intperiodoinicial;
                Int32 _intidnroservicio = 0;
                Int32 _intcantidad = 0;
                String _strmes = "";
                Decimal _decimportetotal = 0;
                DataView _dtview = new DataView(_dtconsumo);

                foreach (DataRow fila in _dtconsumo.Rows)
                {
                    if (_intidnroservicio != Convert.ToInt32(fila["IdNroServicio"]))
                    {
                        _intidnroservicio = Convert.ToInt32(fila["IdNroServicio"]);
                        _intperiodo = Convert.ToInt32(fila["Periodo"]);
                        _intperiodoinicial = clsUtil.PeriodoADD(_intperiodo, -12);
                        _intperiodoasig = _intperiodo;
                        _decimportetotal = Convert.ToDecimal(fila["ImporteTotal"]);

                        _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'";

                        while (_intperiodoinicial < _intperiodoasig)
                        {
                            _intperiodoasig = clsUtil.PeriodoADD(_intperiodoasig, -1);

                            _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                              + "AND IdConcepto = '" + _intidconceptoeat.ToString() + "'"
                                              + "AND PeriodoConsumo = '" + _intperiodoasig.ToString() + "'";

                            _intcantidad = _dtview.ToTable().Rows.Count;

                            if (_intcantidad == 0)
                            {
                                DataRow _drw = _dtnuevo.NewRow();

                                _strmes = clsUtil.ObtenerNombreMes(_intperiodoasig);

                                _drw["IdNroServicio"] = _intidnroservicio;
                                _drw["IdConcepto"] = (Int16)1;
                                _drw["AbreviaturaConcepto"] = "EAT";
                                _drw["CantidadConsumo"] = 0.00;
                                _drw["PeriodoConsumo"] = _intperiodoasig;
                                _drw["Periodo"] = _intperiodo;
                                _drw["Mes"] = _strmes;
                                _drw["ImporteTotal"] = _decimportetotal;

                                _dtnuevo.Rows.Add(_drw);
                            }
                        }
                        _dtview.RowFilter = "";
                    }
                }

                return _dtnuevo;
            }
        #endregion Public
    }
}