﻿using SISAC.Data.DAO;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

namespace SISAC.Data.Facturacion
{
    public sealed class clsReciboMayorDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsReciboMayorDAO> _Instancia = new Lazy<clsReciboMayorDAO>(() => new clsReciboMayorDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsReciboMayorDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsReciboMayorDAO()
        {
        }

        #endregion Constructor

        #region Public

        public SqlDataReader ExtraerDatosMayor_Cabecera(SqlConnection sqlc
                                                        , Int16 esruta
                                                        , Int16 esduplicado
                                                        , String archivoblanco)
        {
            SqlCommand sqlCabecera = new SqlCommand("Recibos_Ver_Cabecera_mayor_pa", sqlc);
            sqlCabecera.Parameters.Add(new SqlParameter("@esruta", System.Data.SqlDbType.SmallInt));
            sqlCabecera.Parameters.Add(new SqlParameter("@esduplicado", System.Data.SqlDbType.SmallInt));
            sqlCabecera.Parameters.Add(new SqlParameter("@ArchivoBlanco", System.Data.SqlDbType.VarChar));


            sqlCabecera.Parameters[0].Value = esruta;
            sqlCabecera.Parameters[1].Value = esduplicado;
            sqlCabecera.Parameters[2].Value = archivoblanco;

            sqlCabecera.CommandType = CommandType.StoredProcedure;
            return (sqlCabecera.ExecuteReader());
        }

        public SqlDataReader ExtraerDatosMayor_Lectura(SqlConnection sqlc)
        {
            SqlCommand sqlCabecera = new SqlCommand("Recibos_Ver_Lectura_Mayor_pa", sqlc);
            sqlCabecera.CommandType = CommandType.StoredProcedure;
            return (sqlCabecera.ExecuteReader());
        }

        public SqlDataReader ExtraerDatosMayor_Concepto(SqlConnection sqlc)
        {
            SqlCommand sqlConcepto = new SqlCommand("Recibos_Ver_conceptos_mayor_pa", sqlc);

            sqlConcepto.CommandType = CommandType.StoredProcedure;
            return (sqlConcepto.ExecuteReader());
        }

        public DataTable ExtraerDatosMayor_Consumo(SqlConnection sqlc)
        {
            SqlCommand sqlConsumo = new SqlCommand("Recibos_Ver_Consumo_Mayor_pa", sqlc);

            sqlConsumo.CommandType = CommandType.StoredProcedure;
            //return (sqlConsumo.ExecuteReader());            

            SqlDataReader _sqldatareader = sqlConsumo.ExecuteReader();

            DataTable _dtesquema = _sqldatareader.GetSchemaTable();
            DataTable _dtconsumo = new DataTable("Inf");

            _dtconsumo.Load(_sqldatareader, LoadOption.OverwriteChanges);

            DataTable _dtnuevo = new DataTable("clsReciboMayorConsumo");

            foreach (DataRow drow in _dtesquema.Rows)
            {
                String columnName = System.Convert.ToString(drow["ColumnName"]);
                DataColumn column = new DataColumn(columnName, (Type)(drow["DataType"]));
                _dtnuevo.Columns.Add(column);
            }

            _dtnuevo.Merge(_dtconsumo, true);

            Int32 _intperiodo;
            Int32 _intperiodoasig;
            Int16 _intidmeat = (Int16)1; // EATP
            Int16 _intidmeahp = (Int16)2; // EAHP
            Int16 _intidmeafp = (Int16)3; // EAFP
            Int16 _intidmphp = (Int16)5; // PHP
            Int16 _intidmpfp = (Int16)6; // PFP
            Int32 _intperiodoinicial;
            Int32 _intidnroservicio = 0;
            Int32 _intcantidad = 0;
            String _strmes = "";
            Decimal _decimportetotal = 0;
            DataView _dtview = new DataView(_dtconsumo);

            foreach (DataRow fila in _dtconsumo.Rows)
            {
                if (_intidnroservicio != Convert.ToInt32(fila["IdNroServicio"]))
                {
                    _intidnroservicio = Convert.ToInt32(fila["IdNroServicio"]);
                    _intperiodo = Convert.ToInt32(fila["Periodo"]);
                    _intperiodoinicial = clsUtil.PeriodoADD(_intperiodo, -12);
                    _intperiodoasig = _intperiodo;
                    _decimportetotal = Convert.ToDecimal(fila["ImporteTotal"]);

                    _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                          + "AND IdConcepto = '" + _intidmeahp.ToString() + "'"
                                          + "AND PeriodoConsumo = '" + _intperiodo.ToString() + "'";

                    _intcantidad = _dtview.ToTable().Rows.Count;

                    if (_intcantidad == 1)
                    {
                        while (_intperiodoinicial < _intperiodoasig)
                        {
                            _intperiodoasig = clsUtil.PeriodoADD(_intperiodoasig, -1);

                            _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                              + "AND IdConcepto = '" + _intidmeahp.ToString() + "'"
                                              + "AND PeriodoConsumo = '" + _intperiodoasig.ToString() + "'";

                            _intcantidad = _dtview.ToTable().Rows.Count;

                            if (_intcantidad == 0)
                            {
                                DataRow _drw = _dtnuevo.NewRow();

                                _strmes = clsUtil.ObtenerNombreMes(_intperiodoasig);

                                _drw["IdNroServicio"] = _intidnroservicio;
                                _drw["IdConcepto"] = (Int16)2;
                                _drw["AbreviaturaConcepto"] = "EAHP";
                                _drw["CantidadConsumo"] = 0.00;
                                _drw["PeriodoConsumo"] = _intperiodoasig;
                                _drw["Periodo"] = _intperiodo;
                                _drw["Mes"] = _strmes;
                                _drw["EsEnergia"] = (Int16)0;
                                _drw["ImporteTotal"] = _decimportetotal;

                                _dtnuevo.Rows.Add(_drw);
                            }
                        }
                    }

                    _intperiodo = Convert.ToInt32(fila["Periodo"]);
                    _intperiodoinicial = clsUtil.PeriodoADD(_intperiodo, -12);
                    _intperiodoasig = _intperiodo;

                    _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                          + "AND IdConcepto = '" + _intidmeafp.ToString() + "'"
                                          + "AND PeriodoConsumo = '" + _intperiodo.ToString() + "'";

                    _intcantidad = _dtview.ToTable().Rows.Count;

                    if (_intcantidad == 1)
                    {
                        while (_intperiodoinicial < _intperiodoasig)
                        {
                            _intperiodoasig = clsUtil.PeriodoADD(_intperiodoasig, -1);

                            _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                              + "AND IdConcepto = '" + _intidmeafp.ToString() + "'"
                                              + "AND PeriodoConsumo = '" + _intperiodoasig.ToString() + "'";

                            _intcantidad = _dtview.ToTable().Rows.Count;

                            if (_intcantidad == 0)
                            {
                                DataRow _drw = _dtnuevo.NewRow();

                                _strmes = clsUtil.ObtenerNombreMes(_intperiodoasig);

                                _drw["IdNroServicio"] = _intidnroservicio;
                                _drw["IdConcepto"] = (Int16)3;
                                _drw["AbreviaturaConcepto"] = "EAFP";
                                _drw["CantidadConsumo"] = 0.00;
                                _drw["PeriodoConsumo"] = _intperiodoasig;
                                _drw["Periodo"] = _intperiodo;
                                _drw["Mes"] = _strmes;
                                _drw["EsEnergia"] = (Int16)0;
                                _drw["ImporteTotal"] = _decimportetotal;

                                _dtnuevo.Rows.Add(_drw);
                            }
                        }
                    }

                    _intperiodo = Convert.ToInt32(fila["Periodo"]);
                    _intperiodoinicial = clsUtil.PeriodoADD(_intperiodo, -12);
                    _intperiodoasig = _intperiodo;

                    _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                          + "AND IdConcepto = '" + _intidmphp.ToString() + "'"
                                          + "AND PeriodoConsumo = '" + _intperiodo.ToString() + "'";

                    _intcantidad = _dtview.ToTable().Rows.Count;

                    if (_intcantidad == 1)
                    {
                        while (_intperiodoinicial < _intperiodoasig)
                        {
                            _intperiodoasig = clsUtil.PeriodoADD(_intperiodoasig, -1);

                            _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                              + "AND IdConcepto = '" + _intidmphp.ToString() + "'"
                                              + "AND PeriodoConsumo = '" + _intperiodoasig.ToString() + "'";

                            _intcantidad = _dtview.ToTable().Rows.Count;

                            if (_intcantidad == 0)
                            {
                                DataRow _drw = _dtnuevo.NewRow();

                                _strmes = clsUtil.ObtenerNombreMes(_intperiodoasig);

                                _drw["IdNroServicio"] = _intidnroservicio;
                                _drw["IdConcepto"] = (Int16)5;
                                _drw["AbreviaturaConcepto"] = "PHP";
                                _drw["CantidadConsumo"] = 0.00;
                                _drw["PeriodoConsumo"] = _intperiodoasig;
                                _drw["Periodo"] = _intperiodo;
                                _drw["Mes"] = _strmes;
                                _drw["EsEnergia"] = (Int16)1;

                                _dtnuevo.Rows.Add(_drw);
                            }
                        }
                    }

                    _intperiodo = Convert.ToInt32(fila["Periodo"]);
                    _intperiodoinicial = clsUtil.PeriodoADD(_intperiodo, -12);
                    _intperiodoasig = _intperiodo;

                    _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                          + "AND IdConcepto = '" + _intidmpfp.ToString() + "'"
                                          + "AND PeriodoConsumo = '" + _intperiodo.ToString() + "'";

                    _intcantidad = _dtview.ToTable().Rows.Count;

                    if (_intcantidad == 1)
                    {
                        while (_intperiodoinicial < _intperiodoasig)
                        {
                            _intperiodoasig = clsUtil.PeriodoADD(_intperiodoasig, -1);

                            _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                              + "AND IdConcepto = '" + _intidmpfp.ToString() + "'"
                                              + "AND PeriodoConsumo = '" + _intperiodoasig.ToString() + "'";

                            _intcantidad = _dtview.ToTable().Rows.Count;

                            if (_intcantidad == 0)
                            {
                                DataRow _drw = _dtnuevo.NewRow();

                                _strmes = clsUtil.ObtenerNombreMes(_intperiodoasig);

                                _drw["IdNroServicio"] = _intidnroservicio;
                                _drw["IdConcepto"] = (Int16)6;
                                _drw["AbreviaturaConcepto"] = "PFP";
                                _drw["CantidadConsumo"] = 0.00;
                                _drw["PeriodoConsumo"] = _intperiodoasig;
                                _drw["Periodo"] = _intperiodo;
                                _drw["Mes"] = _strmes;
                                _drw["EsEnergia"] = (Int16)1;

                                _dtnuevo.Rows.Add(_drw);
                            }
                        }
                    }

                    _intperiodo = Convert.ToInt32(fila["Periodo"]);
                    _intperiodoinicial = clsUtil.PeriodoADD(_intperiodo, -12);
                    _intperiodoasig = _intperiodo;

                    _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                          + "AND IdConcepto = '" + _intidmeat.ToString() + "'"
                                          + "AND PeriodoConsumo = '" + _intperiodo.ToString() + "'";

                    _intcantidad = _dtview.ToTable().Rows.Count;

                    if (_intcantidad == 1)
                    {
                        while (_intperiodoinicial < _intperiodoasig)
                        {
                            _intperiodoasig = clsUtil.PeriodoADD(_intperiodoasig, -1);

                            _dtview.RowFilter = "IdNroServicio = '" + _intidnroservicio.ToString() + "'"
                                              + "AND IdConcepto = '" + _intidmeat.ToString() + "'"
                                              + "AND PeriodoConsumo = '" + _intperiodoasig.ToString() + "'";

                            _intcantidad = _dtview.ToTable().Rows.Count;

                            if (_intcantidad == 0)
                            {
                                DataRow _drw = _dtnuevo.NewRow();

                                _strmes = clsUtil.ObtenerNombreMes(_intperiodoasig);

                                _drw["IdNroServicio"] = _intidnroservicio;
                                _drw["IdConcepto"] = (Int16)1;
                                _drw["AbreviaturaConcepto"] = "EAT";
                                _drw["CantidadConsumo"] = 0.00;
                                _drw["PeriodoConsumo"] = _intperiodoasig;
                                _drw["Periodo"] = _intperiodo;
                                _drw["Mes"] = _strmes;
                                _drw["EsEnergia"] = (Int16)1;

                                _dtnuevo.Rows.Add(_drw);
                            }
                        }
                    }
                }
            }
            return _dtnuevo;
        }
        #endregion Public
    }
}