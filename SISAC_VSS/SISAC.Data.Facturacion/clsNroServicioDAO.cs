﻿using SISAC.Data.DAO;
using SISAC.Entidad.Facturacion;
using SISAC.Entidad.Maestro;
using SISAC.Util.Tool;
using System;
using System.Collections;
using System.Data;
using System.Threading;

namespace SISAC.Data.Facturacion
{
    public sealed class clsNroServicioDAO
    {
        #region Field

        private static readonly clsDataAccess _DAO = clsDataAccess.Instancia;
        private static readonly Lazy<clsNroServicioDAO> _Instancia = new Lazy<clsNroServicioDAO>(() => new clsNroServicioDAO(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsNroServicioDAO Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsNroServicioDAO()
        {
        }

        #endregion Constructor

        #region Public

        public clsNroServicioBasico ObtenerRegistroBasico(Int32 idnroservicio)
        {
            Hashtable _htparametros = new Hashtable();

            clsNroServicioBasico _objnroserviciobasico;
            DataRow _drwnroservicio;

            _htparametros.Add("@idnroservicio", idnroservicio);
            _drwnroservicio = _DAO.EjecutarComandoRegistro("NroServicio", "ObtenerRegistroBasico", _htparametros);

            if (_drwnroservicio == null) return (clsNroServicioBasico)null;

            _objnroserviciobasico = new clsNroServicioBasico(_drwnroservicio);

            return _objnroserviciobasico;
        }

        public DataRow ObtenerJSONRecibo(clsParametro entidad)
        {
            try
            {
                var param = entidad.Parametros.ToHashtableDAO();
                var result = _DAO.EjecutarComandoRegistro("ReciboDigital", "ObtenerJSON", param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }
}