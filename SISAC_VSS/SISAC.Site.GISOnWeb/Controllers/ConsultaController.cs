﻿using SISAC.Entidad.Maestro;
using SISAC.Interface.GISOnWeb;
using SISAC.Negocio.GISOnWeb;
using System.Web.Http;

namespace SISAC.Site.GISOnWeb.Controllers
{
    public class ConsultaController : ApiController, IConsulta
    {
        #region Field

        private static readonly clsNegocioConsulta _Consulta = clsNegocioConsulta.Instancia;

        #endregion Field

        #region Public

        public IHttpActionResult CapaListarGeografia(clsParametro param)
        {
            var result = _Consulta.CapaListarGeografia(param);

            return Ok(result);
        }

        public IHttpActionResult CapaListarElectrica(clsParametro param)
        {
            var result = _Consulta.CapaListarElectrica(param);

            return Ok(result);
        }

        public IHttpActionResult Escalaslistar(clsParametro param)
        {
            var result = _Consulta.Escalaslistar(param);

            return Ok(result);
        }

        public IHttpActionResult CapaListarEscalas(clsParametro param)
        {
            var result = _Consulta.CapaListarEscalas(param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListaBuscar(clsParametro param)
        {
            var result = _Consulta.EntidadListaBuscar(param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListaPropiedad(clsParametro param)
        {
            var result = _Consulta.EntidadListaPropiedad(param);

            return Ok(result);
        }

        public IHttpActionResult UsuarioValida(clsParametro param)
        {
            var result = _Consulta.UsuarioValida(param);

            return Ok(result);
        }

        public IHttpActionResult UsuarioPerfil(clsParametro param)
        {
            var result = _Consulta.UsuarioPerfil(param);

            return Ok(result);
        }

        public IHttpActionResult Poligonos(clsParametro param)
        {
            var result = _Consulta.Poligonos(param);

            return Ok(result);
        }

        public IHttpActionResult Lineas(clsParametro param)
        {
            var result = _Consulta.Lineas(param);

            return Ok(result);
        }

        public IHttpActionResult Puntos(clsParametro param)
        {
            var result = _Consulta.Puntos(param);

            return Ok(result);
        }

        public IHttpActionResult EntidadBuscar(clsParametro param)
        {
            var result = _Consulta.EntidadBuscar(param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListar(clsParametro param)
        {
            var result = _Consulta.EntidadListar(param);

            return Ok(result);
        }

        public IHttpActionResult EliminarCache(clsParametro param)
        {
            var result = _Consulta.EliminarCache(param);

            return Ok(result);
        }

        public IHttpActionResult EntidadListarLeyenda(clsParametro param)
        {
            var result = _Consulta.EntidadListarLeyenda(param);

            return Ok(result);
        }

        #endregion Public
    }
}