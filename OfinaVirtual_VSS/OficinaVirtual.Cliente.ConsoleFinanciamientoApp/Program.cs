﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OficinaVirtual.Util.Tool;
using System.Data;
using Newtonsoft.Json.Linq;

namespace OficinaVirtual.Cliente.ConsoleFinanciamientoApp
{
    internal class Program
    {

        #region Extra

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;
        private const int SW_MINIMIZE = 6;

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        #endregion Extra

        #region Fields
        private static readonly clsConvert _Convert = clsConvert.Instancia;

        #endregion Fields


        private static void Main(string[] args)
        {
            try
            {
                Execute().Wait();
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        private static async Task Execute()
        {
            try
            {
                ServicePointManager.Expect100Continue = false;
                ServicePointManager.DefaultConnectionLimit = Int32.MaxValue;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                await procesarPagosTruncos();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static async Task procesarPagosTruncos()
        {
            string tokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"].ToString();
            List<OrdenVisaNetFallido> listaTransacciones = await ObtenerListaTransaccionesFallidas();
            if ((listaTransacciones.Count == 0))
            {
                return;
            }

            foreach (OrdenVisaNetFallido item in listaTransacciones)
            {
                clsParametroEntrada _clsParametroEntrada = new clsParametroEntrada();

                clsParametro paramEntrada = new clsParametro();
                paramEntrada.Parametros = new Dictionary<string, string>();
                paramEntrada.Parametros.Add("NroPedido", item.IdNumeroOrden.ToString());
                paramEntrada.Parametros.Add("IdOrdenCobro", item.IdOrdenCobro.ToString());
                paramEntrada.Parametros.Add("MetodoPago", item.MetodoPago.ToString());
                paramEntrada.Parametros.Add("SessionToken", tokenDistriluz);
                paramEntrada.Parametros.Add("SessionTokenVisa", " ");
                _clsParametroEntrada.Parametros = paramEntrada.Parametros;

                clsResultadoRecaudacion _clsResultadoRecaudacion = new clsResultadoRecaudacion();
                _clsResultadoRecaudacion = await clsRecaudacion.Instancia.PagarDeudaFinanciamiento(_clsParametroEntrada);
                if (_clsResultadoRecaudacion.IdError == 0)
                {
                    var rqPay = new
                    {
                        NroPedido = item.IdNumeroOrden,
                        IdOrdenCobro = item.IdOrdenCobro,
                        SessionToken = tokenDistriluz,
                        SessionTokenVisa = "",
                        MetodoPago = item.MetodoPago
                    };
                    await ActualizarIntentoPago(item);
                    await clsRecaudacion.Instancia.FinanciamientoEnviarEmail(new clsParametroEntrada { Parametros = rqPay });
                }
                else
                {
                    var rqPay = new
                    {
                        IdNumeroOrden = item.IdNumeroOrden,
                    };

                    await clsRecaudacion.Instancia.FinanciamientoEnviarEmailError(new clsParametroEntrada { Parametros = rqPay });
                }

            }
        }

        private static async Task ActualizarIntentoPago(OrdenVisaNetFallido order)
        {
            dynamic intent = new JObject();
            intent.SessionToken = ConfigurationManager.AppSettings["TokenDistriluz"].ToString();
            intent.NroPedido = order.IdNumeroOrden;
            intent.Email = order.Email;
            intent.TransactionTokenVisa = null;
            intent.SessionTokenVisa = String.Empty;
            intent.JsonPagoVisa = (Object)null;
            intent.JsonPagoResumen = (Object)null;
            intent.JsonPagoResumen = intent.JsonPagoVisa;
            intent.IdPaso = "3";
            intent.IdEstadoPaso = "1";
            intent.MensajePaso = "DISTRILUZ - OK";
            try
            {
                await clsRecaudacion.Instancia.IntentoPagoActualizar(new clsParametroEntrada { Parametros = intent });
            }
            catch (Exception ex)
            {
            }

        }


        private static async Task<List<OrdenVisaNetFallido>> ObtenerListaTransaccionesFallidas()
        {
            clsParametro param = new clsParametro();
            param.Parametros = new Dictionary<string, string>();
            List<OrdenVisaNetFallido> listaTransacciones = new List<OrdenVisaNetFallido>();
            clsResultado result = new clsResultado();
            result = await clsFachadaFinanciamiento.Instancia.VisaListFailed(param);
            listaTransacciones = _Convert.ToObject<List<OrdenVisaNetFallido>>(result.Datos.ToString());
            return listaTransacciones;
        }
    }
}











