﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public sealed class clsConsulta
    {
        #region Field

        private static readonly String _TokenCaptchaKey = ConfigurationManager.AppSettings["TokenCaptchaKey"];
        private static readonly String _UrlSitePrincipal = ConfigurationManager.AppSettings["WebService_PRINCIPAL"];
        private static string _UrlSiteMaestro = ConfigurationManager.AppSettings["WebService_MAESTRO"];
        private static readonly String _ServiceOV = ConfigurationManager.AppSettings["WebService_ATENCION"] + "OficinaVirtual/";
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly Lazy<clsConsulta> _Instancia = new Lazy<clsConsulta>(() => new clsConsulta(), LazyThreadSafetyMode.PublicationOnly);

        private const string _UrlListaInformacionAsociados = "serviciodistriluzmobile/listarinformacionasociado";
        private const string _UrlConsultarSuministro = "servicioregistro/validarsuministroasociar";
        private const string _UrlRegistrarSuministro = "servicioregistro/confirmarsuministroasociar";
        private const string _UrlUltimaFacturacion = "serviciodistriluzmobile/consultaresumenultimafacturacion";
        private const string _UrlHistoriaConsumo = "serviciodistriluzmobile/consultaconsumografico";
        private const string _UrlHistoriaFacturacion = "serviciodistriluzmobile/consultasuministroinformacion";
        private const string _UrlUltimoRecibo = "serviciodistriluzmobile/listardetallerecibo";
        private const string _UrlCortesSuministro = "serviciodistriluzmobile/consultacortessuministro";
        private const string _UrlHistoriaPago = "serviciodistriluzmobile/consultahistoricopagos";
        private const string _UrlLugaresPago = "serviciodistriluzmobile/listarubicaciongeoreferencial";
        private const string _UrlDetalleCentro = "serviciodistriluzmobile/obtenerdetallecentro";
        private const string _UrlListaMaestros = "Control/AyudaLista";

        #endregion Field

        public enum TipoMaestroEnum
        {
            TipoCentro,
            Distritos,
            TipoDocumento
        }

        public enum urlHistoricoEnum
        {
            HistoriaConsumo,
            HistoriaFacturacion,
            UltimoRecibo,
            CortesSuministro,
            HistoriaPago
        }

        #region Property

        public static clsConsulta Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsConsulta()
        {
        }

        #endregion Constructor

        #region Metodos

        public async Task<List<clsResumenSuministro>> ListSuministroResumen(clsParametroConsulta parametro)
        {
            clsListaInformacionAsociado lsInfAso = new clsListaInformacionAsociado();
            List<clsResumenSuministro> lsResSum = new List<clsResumenSuministro>();

            try
            {
                lsInfAso = await ListarSuministroResumen(parametro);

                foreach (clsInformacionAsociado list in lsInfAso.Elementos)
                {
                    clsResumenSuministro ResSum = new clsResumenSuministro();
                    List<clsDiagramaBarras> lsDiaBar = new List<clsDiagramaBarras>();

                    ResSum.NroSuministro = list.IdNroServicio.ToString();
                    ResSum.CantDeuda = list.TotalDeuda;
                    ResSum.NombreUser = list.Nombre;
                    ResSum.CantUltRecibo = (list.ImporteUltimoRecibo != null ? list.ImporteUltimoRecibo : "0");
                    ResSum.FechEmitido = (list.FechaEmision != "" ? list.FechaEmision : "(No hay dato)");
                    ResSum.FechVencimiento = (list.FechaVcto != "" ? list.FechaVcto : "(No hay dato)");
                    ResSum.CodTarifa = (list.Tarifa != "" ? list.Tarifa : "(No hay dato)");
                    ResSum.Titulo = list.InformacionGrafico.Titulo;

                    foreach (string item in list.InformacionGrafico.ListaRegistro)
                    {
                        clsDiagramaBarras itemDiaBar = new clsDiagramaBarras();

                        itemDiaBar.y_valor = clsHelperControlTag.ObtenerValor(item, "valor", "0");
                        itemDiaBar.x_etiqueta = clsHelperControlTag.ObtenerValor(item, "etiqueta", "0");
                        itemDiaBar.color_texto = clsHelperControlTag.ObtenerValor(item, "color_texto", "0");
                        itemDiaBar.color_barra = clsHelperControlTag.ObtenerValor(item, "color_barra", "0");

                        lsDiaBar.Add(itemDiaBar);
                    }

                    ResSum.lstString = _Convert.ToJson(lsDiaBar);

                    lsResSum.Add(ResSum);
                }
            }
            catch
            {
                throw;
            }

            return lsResSum;
        }

        public async Task<clsConsultaSuministro> ConsultarSuministro2(clsParametroConsulta oParametro)
        {
            clsResultadoConsultaSuministro oDatos = new clsResultadoConsultaSuministro();
            clsConsultaSuministro oRegistro = new clsConsultaSuministro();

            try
            {
                oDatos = await ConsultarSuministro(oParametro);

                string sConfiguracion = oDatos.Configuracion == null ? "" : oDatos.Configuracion;
                string sDatoControl1 = oDatos.DatoControl1 == null ? "" : oDatos.DatoControl1;

                if (sConfiguracion == "")
                {
                    oRegistro.Exito = false;
                    oRegistro.Mensaje = "El suministro no se encuentra";
                }
                else if (sConfiguracion != "" && sDatoControl1 != "")
                {
                    oRegistro.Exito = false;
                    oRegistro.Mensaje = sDatoControl1;
                }
                else if (sConfiguracion != "" && sDatoControl1 == "")
                {
                    oRegistro.Suministro = oDatos.Titulo;
                    oRegistro.NombreCompleto = oDatos.Subtitulo1;
                    oRegistro.Direccion = oDatos.Subtitulo2;
                    oRegistro.Proveedora = oDatos.Subtitulo3;
                    oRegistro.Exito = true;
                }
            }
            catch
            {
                throw;
            }

            return oRegistro;
        }

        public async Task<clsDetalleSuministro> DetalleSuministro2(clsParametroConsulta oParametro)
        {
            clsResultadoConsultaSuministro oDatosPropietario = new clsResultadoConsultaSuministro();
            clsSuministroInformacion oDatosSuministro = new clsSuministroInformacion();
            clsDetalleSuministro oRegistro = new clsDetalleSuministro();

            try
            {
                oDatosPropietario = await ConsultarSuministro(oParametro);
                oDatosSuministro = await DetalleSuministro(oParametro);

                oRegistro.Suministro = (oDatosPropietario.Titulo == "") ? "(No hay dato)" : oDatosPropietario.Titulo;
                oRegistro.NombreCompleto = (oDatosPropietario.Subtitulo1 == "") ? "(No hay dato)" : oDatosPropietario.Subtitulo1;
                oRegistro.Direccion = (oDatosPropietario.Subtitulo2 == "") ? "(No hay dato)" : oDatosPropietario.Subtitulo2;
                oRegistro.Proveedora = (oDatosPropietario.Subtitulo3 == "") ? "(No hay dato)" : oDatosPropietario.Subtitulo3;

                foreach (clsInformacion oItem in oDatosSuministro.ListaInformacion)
                {
                    foreach (string oValores in oItem.ListaRegistro)
                    {
                        var sValor = oValores.Split('*');
                        oRegistro.Recibo = string.Format("{0}-{1}", clsHelperControlTag.ObtenerValor(sValor[11], "valor", "0").Substring(0, 3), clsHelperControlTag.ObtenerValor(sValor[11], "valor", "0").Substring(3, clsHelperControlTag.ObtenerValor(sValor[11], "valor", "0").Length - 3));
                        oRegistro.Periodo = clsHelperControlTag.ObtenerValor(sValor[0], "valor", "0");
                        oRegistro.PeriodoDesc = clsHelperControlTag.ObtenerValor(sValor[15], "valor", "0");
                        oRegistro.ImporteRecibo = clsHelperControlTag.ObtenerValor(sValor[2], "valor", "0");
                        oRegistro.FechaEmision = clsHelperControlTag.ObtenerValor(sValor[1], "valor", "0");
                        oRegistro.FechaVencimiento = clsHelperControlTag.ObtenerValor(sValor[4], "valor", "0");
                        oRegistro.CantConsumo = Convert.ToDouble(clsHelperControlTag.ObtenerValor(sValor[12], "valor", "0")).ToString("###,###.00");
                        oRegistro.ImporteDeuda = clsHelperControlTag.ObtenerValor(sValor[5], "valor", "0");
                        oRegistro.FechaUltimoPago = clsHelperControlTag.ObtenerValor(sValor[4], "valor", "0");
                        oRegistro.Telefono = clsHelperControlTag.ObtenerValor(sValor[13], "valor", "0");
                        oRegistro.Telefono = (oRegistro.Telefono == "") ? "(No hay dato)" : oRegistro.Telefono;
                        oRegistro.Correo = clsHelperControlTag.ObtenerValor(sValor[14], "valor", "0");
                        oRegistro.Correo = (oRegistro.Correo == "") ? "(No hay dato)" : oRegistro.Correo;
                    }
                }
            }
            catch
            {
                throw;
            }

            return oRegistro;
        }

        public async Task<clsInformacion> HistoricoConsumo(clsParametroConsulta oParametro)
        {
            clsInformacion oRegistroFinal = new clsInformacion();
            clsHistoricos oDatos = new clsHistoricos();
            List<clsDiagramaBarras> oDatosDiagrama = new List<clsDiagramaBarras>();

            try
            {
                oDatos = await HistorialSuministro(clsConsulta.urlHistoricoEnum.HistoriaConsumo, oParametro);

                foreach (clsInformacion oRegistro in oDatos.ListaInformacion)
                {
                    foreach (string oItem in oRegistro.ListaRegistro)
                    {
                        clsDiagramaBarras oDiagrama = new clsDiagramaBarras();
                        oDiagrama.y_valor = clsHelperControlTag.ObtenerValor(oItem, "valor", "0");
                        oDiagrama.x_etiqueta = clsHelperControlTag.ObtenerValor(oItem, "etiqueta", "0");
                        oDiagrama.color_texto = clsHelperControlTag.ObtenerValor(oItem, "color_texto", "0");
                        oDiagrama.color_barra = clsHelperControlTag.ObtenerValor(oItem, "color_barra", "0");
                        oDatosDiagrama.Add(oDiagrama);
                    }

                    oRegistro.ListaRegistroSerializado = _Convert.ToJson(oDatosDiagrama);
                    oRegistroFinal = oRegistro;
                }
            }
            catch
            {
                throw;
            }

            return oRegistroFinal;
        }

        public async Task<clsInformacion> HistoricoFacturacion(clsParametroConsulta oParametro)
        {
            clsInformacion oRegistroFinal = new clsInformacion();
            clsHistoricos oDatos = new clsHistoricos();
            List<clsDiagramaBarras> oDatosDiagrama = new List<clsDiagramaBarras>();

            try
            {
                oDatos = await HistorialSuministro(clsConsulta.urlHistoricoEnum.HistoriaFacturacion, oParametro);

                foreach (clsInformacion oRegistro in oDatos.ListaInformacion)
                {
                    foreach (string oItem in oRegistro.ListaRegistro)
                    {
                        //INICIO Tratamiento especial para esta data
                        string sNuevoItem = "";
                        string sItem = "";
                        bool bExisteSemiColon = false;
                        bool bExisteVerticalLine = false;

                        foreach (char oChar in oItem)
                        {
                            if (oChar == ';') bExisteSemiColon = true;
                            sNuevoItem = sNuevoItem + (oChar == '|' && bExisteSemiColon == false ? ';' : oChar);
                        }
                        foreach (char oChar in sNuevoItem)
                        {
                            if (oChar == '|') bExisteVerticalLine = true;
                            sItem = sItem + (oChar == ':' && bExisteVerticalLine == false ? '|' : oChar);
                        }
                        //FIN Tratamiento especial para esta data

                        clsDiagramaBarras oDiagrama = new clsDiagramaBarras();
                        oDiagrama.y_valor = clsHelperControlTag.ObtenerValor(sItem, "mes_actua", "0");
                        oDiagrama.x_etiqueta = clsHelperControlTag.ObtenerValor(sItem, "fec_factu", "0");
                        oDiagrama.color_texto = clsHelperControlTag.ObtenerValor(sItem, "color_texto", "0");
                        oDiagrama.color_barra = "#359f37"; //HelperControlTag.ObtenerValor(sItem, "color_fondo", "0");
                        oDatosDiagrama.Add(oDiagrama);
                    }

                    oRegistro.ListaRegistroSerializado = _Convert.ToJson(oDatosDiagrama);
                    oRegistroFinal = oRegistro;
                }
            }
            catch
            {
                throw;
            }

            return oRegistroFinal;
        }

        public async Task<clsUltimoReciboCabecera> UltimoReciboCabecera(clsParametroConsulta oParametro)
        {
            clsHistoricos oDatos = new clsHistoricos();
            clsUltimoReciboCabecera oDatosRecibo = new clsUltimoReciboCabecera();

            try
            {
                oDatos = await HistorialSuministro(clsConsulta.urlHistoricoEnum.UltimoRecibo, oParametro);

                if (oDatos.ListaInformacion != null)
                {
                    foreach (clsInformacion oRegistro in oDatos.ListaInformacion)
                    {
                        foreach (string oItem in oRegistro.ListaRegistro)
                        {
                            var sValor = oItem.Split('*');
                            oDatosRecibo.Tarifa = clsHelperControlTag.ObtenerValor(sValor[0], "valor", "0");
                            oDatosRecibo.Consumo = clsHelperControlTag.ObtenerValor(sValor[1], "valor", "0");
                            if (oDatosRecibo.Consumo != "---")
                                oDatosRecibo.Consumo = Convert.ToDouble(oDatosRecibo.Consumo).ToString("###,###.00");
                            oDatosRecibo.Emision = clsHelperControlTag.ObtenerValor(sValor[3], "valor", "0");
                            oDatosRecibo.Vencimiento = clsHelperControlTag.ObtenerValor(sValor[5], "valor", "0");
                            oDatosRecibo.Importe = clsHelperControlTag.ObtenerValor(sValor[6], "valor", "0");
                            oDatosRecibo.Recibo = clsHelperControlTag.ObtenerValor(sValor[8], "valor", "0");
                            if (oDatosRecibo.Recibo != "---")
                                oDatosRecibo.Recibo = string.Format("{0}-{1}", clsHelperControlTag.ObtenerValor(sValor[8], "valor", "0").Substring(0, 3), clsHelperControlTag.ObtenerValor(sValor[8], "valor", "0").Substring(3, clsHelperControlTag.ObtenerValor(sValor[8], "valor", "0").Length - 3));
                            oDatosRecibo.Deuda = clsHelperControlTag.ObtenerValor(sValor[9], "valor", "0");
                            oDatosRecibo.Tension = clsHelperControlTag.ObtenerValor(sValor[10], "valor", "0");
                            oDatosRecibo.Lectura = clsHelperControlTag.ObtenerValor(sValor[11], "valor", "0");
                            if (oDatosRecibo.Lectura != "---")
                                oDatosRecibo.Lectura = Convert.ToDouble(oDatosRecibo.Lectura).ToString("###,###.00");
                            oDatosRecibo.Circuito = clsHelperControlTag.ObtenerValor(sValor[12], "valor", "0");
                            oDatosRecibo.SED = clsHelperControlTag.ObtenerValor(sValor[13], "valor", "0");
                            oDatosRecibo.Alimentador = clsHelperControlTag.ObtenerValor(sValor[14], "valor", "0");
                            oDatosRecibo.Potencia = clsHelperControlTag.ObtenerValor(sValor[15], "valor", "0");
                            oDatosRecibo.PeriodoDes = clsHelperControlTag.ObtenerValor(sValor[16], "valor", "0");
                            break;
                        }
                        break;
                    }
                }
            }
            catch
            {
                throw;
            }

            return oDatosRecibo;
        }

        public async Task<List<clsUltimosPagos>> HistoricoPago(clsParametroConsulta oParametro)
        {
            clsHistoricos oDatos = new clsHistoricos();
            List<clsUltimosPagos> oDatosPagos = new List<clsUltimosPagos>();

            try
            {
                oDatos = await HistorialSuministro(clsConsulta.urlHistoricoEnum.HistoriaPago, oParametro);

                if (oDatos.ListaInformacion != null)
                {
                    foreach (clsInformacion oRegistro in oDatos.ListaInformacion)
                    {
                        foreach (string oItem in oRegistro.ListaRegistro)
                        {
                            //INICIO Tratamiento especial para esta data
                            string sNuevoItem = "";
                            string sItem = "";
                            bool bExisteSemiColon = false;
                            bool bExisteVerticalLine = false;

                            foreach (char oChar in oItem)
                            {
                                if (oChar == ';') bExisteSemiColon = true;
                                sNuevoItem = sNuevoItem + (oChar == '|' && bExisteSemiColon == false ? ';' : oChar);
                            }
                            foreach (char oChar in sNuevoItem)
                            {
                                if (oChar == '|') bExisteVerticalLine = true;
                                sItem = sItem + (oChar == ':' && bExisteVerticalLine == false ? '|' : oChar);
                            }
                            //FIN Tratamiento especial para esta data

                            clsUltimosPagos oPago = new clsUltimosPagos();
                            oPago.fecha_pago = clsHelperControlTag.ObtenerValor(sItem, "fecha_pago", "0");
                            oPago.lugar_pago = clsHelperControlTag.ObtenerValor(sItem, "lugar_pago", "0");
                            oPago.monto_pago = clsHelperControlTag.ObtenerValor(sItem, "monto_pago", "0");
                            oDatosPagos.Add(oPago);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            return oDatosPagos;
        }

        public async Task<List<clsCortesProgramados>> CortesProgramados(clsParametroConsulta oParametro)
        {
            clsHistoricos oDatos = new clsHistoricos();
            List<clsCortesProgramados> oDatosCortes = new List<clsCortesProgramados>();

            try
            {
                oDatos = await HistorialSuministro(clsConsulta.urlHistoricoEnum.CortesSuministro, oParametro);

                if (oDatos.ListaInformacion != null)
                {
                    foreach (clsInformacion oRegistro in oDatos.ListaInformacion)
                    {
                        foreach (string oItem in oRegistro.ListaRegistro)
                        {
                            //INICIO Tratamiento especial para esta data
                            string sNuevoItem = "";
                            string sItem = "";
                            bool bExisteSemiColon = false;
                            bool bExisteVerticalLine = false;

                            foreach (char oChar in oItem)
                            {
                                if (oChar == ';') bExisteSemiColon = true;
                                sNuevoItem = sNuevoItem + (oChar == '|' && bExisteSemiColon == false ? ';' : oChar);
                            }
                            foreach (char oChar in sNuevoItem)
                            {
                                if (oChar == '|') bExisteVerticalLine = true;
                                sItem = sItem + (oChar == ':' && bExisteVerticalLine == false ? '|' : oChar);
                            }
                            //FIN Tratamiento especial para esta data

                            clsCortesProgramados oCorte = new clsCortesProgramados();
                            oCorte.FechaInicio = clsHelperControlTag.ObtenerValor(sItem, "FechaInicio", "0");
                            oCorte.FechaFinal = clsHelperControlTag.ObtenerValor(sItem, "FechaFinal", "0");
                            oCorte.Duracion = clsHelperControlTag.ObtenerValor(sItem, "Duracion", "0");
                            oDatosCortes.Add(oCorte);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            return oDatosCortes;
        }

        #endregion Metodos

        public async Task<clsListaInformacionAsociado> ListarSuministroResumen(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsListaInformacionAsociado>(_UrlSitePrincipal + _UrlListaInformacionAsociados, param);
        }

        public async Task<clsResultadoConsultaSuministro> ConsultarSuministro(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsResultadoConsultaSuministro>(_UrlSitePrincipal + _UrlConsultarSuministro, param);
        }

        public async Task<clsResultadoOperacion> RegistrarSuministro(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsResultadoOperacion>(_UrlSitePrincipal + _UrlRegistrarSuministro, param);
        }

        public async Task<clsSuministroInformacion> DetalleSuministro(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsSuministroInformacion>(_UrlSitePrincipal + _UrlUltimaFacturacion, param);
        }

        public async Task<clsHistoricos> HistorialSuministro(urlHistoricoEnum sTipo, clsParametroConsulta param)
        {
            string sUrl = "";

            switch (sTipo)
            {
                case urlHistoricoEnum.HistoriaConsumo:
                    sUrl = _UrlHistoriaConsumo;
                    break;

                case urlHistoricoEnum.HistoriaFacturacion:
                    sUrl = _UrlHistoriaFacturacion;
                    break;

                case urlHistoricoEnum.UltimoRecibo:
                    sUrl = _UrlUltimoRecibo;
                    break;

                case urlHistoricoEnum.CortesSuministro:
                    sUrl = _UrlCortesSuministro;
                    break;

                case urlHistoricoEnum.HistoriaPago:
                    sUrl = _UrlHistoriaPago;
                    break;
            }

            return await _Service.PostAsync<clsHistoricos>(_UrlSitePrincipal + sUrl, param);
        }

        public async Task<clsResultadoListaMaestros> ListarMaestros(clsConsulta.TipoMaestroEnum sMaestro)
        {
            string param = "";

            switch (sMaestro)
            {
                case TipoMaestroEnum.TipoCentro:
                    param = "{'Parametros':{'Entity':'ntf.TipoCentro'}}";
                    break;

                case TipoMaestroEnum.Distritos:
                    param = "{'Parametros':{'Entity':'ntf.DistritoCentros'}}";
                    break;

                case TipoMaestroEnum.TipoDocumento:
                    param = "{'Parametros':{'Entity':'dbo.DocumentoIdentidadOV'}}";
                    break;
            }

            clsResultadoListaMaestros result = await _Service.PostAsync<clsResultadoListaMaestros>($"{_ServiceOV}AyudaLista", param);

            return result;
        }

        public static async Task<clsResultadoListaMaestros> ListarMaestros2(clsConsulta.TipoMaestroEnum sMaestro)
        {
            string param = "";

            switch (sMaestro)
            {
                case TipoMaestroEnum.TipoCentro:
                    param = "{'Parametros':{'Entity':'ntf.TipoCentro'}}";
                    break;

                case TipoMaestroEnum.Distritos:
                    param = "{'Parametros':{'Entity':'ntf.DistritoCentros'}}";
                    break;

                case TipoMaestroEnum.TipoDocumento:
                    param = "{'Parametros':{'Entity':'dbo.DocumentoIdentidadOV'}}";
                    break;
            }

            clsResultadoListaMaestros result = await _Service.SendData<clsResultadoListaMaestros>(_UrlSiteMaestro + _UrlListaMaestros, param, string.Empty, string.Empty, string.Empty, "Error al recibir el servicio", false);

            return result;
        }

        public async Task<clsLugaresPago> LugaresPago(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsLugaresPago>(_UrlSitePrincipal + _UrlLugaresPago, param);
        }

        public async Task<clsDetalleCentro> DetalleCentro(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsDetalleCentro>(_UrlSitePrincipal + _UrlDetalleCentro, param);
        }

        public async Task<clsResultado> AyudaLista(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}AyudaLista", param); ;
        }

        public async Task<clsResultado> ValidCaptcha(String response)
        {
            var a = "https://www.google.com/recaptcha/api/siteverify";
            var url = $"{a}?secret={_TokenCaptchaKey}&response={response}";

            return await _Service.PostAsync<clsResultado>(url, null, null, true); ;
        }
    }
}