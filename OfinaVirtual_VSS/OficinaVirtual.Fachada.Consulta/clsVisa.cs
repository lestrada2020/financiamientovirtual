﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public sealed class clsVisa
    {
        #region Field

        private static readonly clsService _Service = clsService.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly String _TokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];
        private static readonly String _ServiceOV = ConfigurationManager.AppSettings["WebService_ATENCION"] + "OficinaVirtual/";
        private static readonly Lazy<List<Dictionary<String, String>>> _ConfigMobile = new Lazy<List<Dictionary<String, String>>>(LazyConfigMobile, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<List<Dictionary<String, String>>> _ConfigWeb = new Lazy<List<Dictionary<String, String>>>(LazyConfigWeb, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<clsVisa> _Instancia = new Lazy<clsVisa>(() => new clsVisa(), LazyThreadSafetyMode.PublicationOnly);

        private static readonly String _SessionToken = ConfigurationManager.AppSettings["TokenDistriluz"];
        private static readonly String _URLSeguridadPerfil = ConfigurationManager.AppSettings["WebService_SEGURIDAD"] + "/PerfilUsuario";
        private static List<Dictionary<String, String>> _ConfigWeb2 = null;
        private static List<Dictionary<String, String>> _ConfigMobile2 = null;

        #endregion Field

        #region Property

        public static clsVisa Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsVisa()
        {
        }

        #endregion Constructor

        #region Private

        private static List<Dictionary<String, String>> LazyConfigMobile()
        {
            var result = LoadConfig(1, 1).Result;

            return result;
        }

        private static List<Dictionary<String, String>> LazyConfigWeb()
        {
            var result = LoadConfig(1, 2).Result;

            return result;
        }

        private static async Task<List<Dictionary<String, String>>> LoadConfig(Int16 idApp, Int16 idType)
        {
            try
            {
                var param = new Dictionary<String, String>();
                param["IdApp"] = idApp.ToString();
                param["IdTipo"] = idType.ToString();
                param["SessionToken"] = _TokenDistriluz;

                var data = await _Service.PostAsync<clsResultado>($"{_ServiceOV}ListAppConfig", new clsParametro() { Parametros = param }).ConfigureAwait(false);
                if (data.IdError > 0) throw new Exception(data.Mensaje);

                var result = _Convert.ToObject<List<Dictionary<String, String>>>(data.Datos.ToString());

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private String GetKey(String key, Int16 idEmpresa, Int16 idType, Boolean isDefaultNotExists = true)
        {
            try
            {
                var config = _ConfigWeb.Value;

                if (idType == 1) config = _ConfigMobile.Value;

                var list = config.Where(s => s["Key"] == key);
                var data = list.FirstOrDefault(s => s["IdEmpresa"] == idEmpresa.ToString());

                if (data == null && isDefaultNotExists) data = list.FirstOrDefault(s => s["IdEmpresa"] == "0");
                if (data == null) throw new Exception($"Falta el key [{key}] para la empresa [{idEmpresa}].");

                var result = data["Value"];

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<List<Dictionary<String, String>>> LoadConfig(Int16 idApp, Int16 idTipo, String tokenDistriluz)
        {
            try
            {
                var param = new Dictionary<String, String>();
                param.Add("IdApp", idApp.ToString());
                param.Add("IdTipo", idTipo.ToString());
                param.Add("SessionToken", tokenDistriluz);

                var data = await _Service.SendData<clsResultado>(_URLSeguridadPerfil + "/ListAppConfig", new clsParametro() { Parametros = param });
                if (data.IdError > 0) throw new Exception(data.Mensaje);

                var result = _Convert.ToObject<List<Dictionary<String, String>>>(data.Datos.ToString());

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<List<Dictionary<String, String>>> LoadConfigMobile()
        {
            try
            {
                if (_ConfigMobile2 == null) _ConfigMobile2 = await LoadConfig(1, 1, _SessionToken);

                return _ConfigMobile2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<List<Dictionary<String, String>>> LoadConfigWeb()
        {
            try
            {
                if (_ConfigWeb2 == null) _ConfigWeb2 = await LoadConfig(1, 2, _SessionToken);

                return _ConfigWeb2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<String> GetKey2(String key, Int16 idEmpresa, Int16 idTipo = 2, Boolean isDefaultNotExists = true)
        {
            try
            {
                var config = new List<Dictionary<String, String>>();

                if (idTipo == 1) config = await LoadConfigMobile();
                else if (idTipo == 2) config = await LoadConfigWeb();

                var list = config.Where(s => s["Key"] == key);
                var data = list.FirstOrDefault(s => s["IdEmpresa"] == idEmpresa.ToString());

                if (data == null && isDefaultNotExists) data = list.FirstOrDefault(s => s["IdEmpresa"] == "0");
                if (data == null) throw new Exception($"Falta el key [{key}] para la empresa [{idEmpresa}].");

                var result = data["Value"];

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<String> GetVisaUser(Int16 idEmpresa)
        {
            try
            {
                return await GetKey2("VisaUser", idEmpresa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<String> GetVisaPassword(Int16 idEmpresa)
        {
            try
            {
                return await GetKey2("VisaPassword", idEmpresa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<String> GetVisaURLToken(Int16 idEmpresa)
        {
            try
            {
                return await GetKey2("VisaURLToken", idEmpresa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<String> GetVisaURLSession(Int16 idEmpresa)
        {
            try
            {
                return await GetKey2("VisaURLSession", idEmpresa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<Dictionary<String, String>> CrearSesion(Int16 idEmpresa, String token, clsParametroVisa param)
        {
            try
            {
                var url = await GetVisaURLSession(idEmpresa);
                var id = await GetVisaMerchantId(idEmpresa);

                var result = await _Service.SendData<Dictionary<String, String>>($"{url}/{id}", param, "", "", token);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Private

        #region Public

        #region Keys

        /// <summary>
        ///
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <returns></returns>
        public String GetMerchantId(Int16 idEmpresa, Int16 idType)
        {
            try
            {
                return GetKey("VisaMerchantId", idEmpresa, idType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<String> GetVisaMerchantId(Int16 idEmpresa)
        {
            try
            {
                return await GetKey2("VisaMerchantId", idEmpresa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<String> GetVisaURLButtonJS(Int16 idEmpresa)
        {
            try
            {
                return await GetKey2("VisaURLButtonJS", idEmpresa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<String> CrearToken(Int16 idEmpresa)
        {
            try
            {
                var url = await GetVisaURLToken(idEmpresa);
                var user = await GetVisaUser(idEmpresa);
                var password = await GetVisaPassword(idEmpresa);

                var result = await _Service.SendData<String>(url, null, user, password);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <returns></returns>
        public String GetUser(Int16 idEmpresa, Int16 idType)
        {
            try
            {
                return GetKey("VisaUser", idEmpresa, idType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <returns></returns>
        public String GetPassword(Int16 idEmpresa, Int16 idType)
        {
            try
            {
                return GetKey("VisaPassword", idEmpresa, idType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetURLToken(Int16 idEmpresa)
        {
            try
            {
                return GetKey("VisaURLToken", idEmpresa, 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetURLSession(Int16 idEmpresa)
        {
            try
            {
                return GetKey("VisaURLSession", idEmpresa, 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<String> GetVisaURLAutorizacion(Int16 idEmpresa)
        {
            try
            {
                return await GetKey2("VisaURLAutorizacion", idEmpresa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetURLAuthorization(Int16 idEmpresa)
        {
            try
            {
                return GetKey("VisaURLAutorizacion", idEmpresa, 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetURLVerification(Int16 idEmpresa)
        {
            try
            {
                return GetKey("VisaURLVerificacion", idEmpresa, 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <returns></returns>
        public String GetURLButtonJS(Int16 idEmpresa)
        {
            try
            {
                return GetKey("VisaURLButtonJS", idEmpresa, 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Keys

        /// <summary>
        ///
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <returns></returns>
        public async Task<String> CreateToken(Int16 idEmpresa, Int16 idType)
        {
            try
            {
                var url = GetURLToken(idEmpresa);
                var user = GetUser(idEmpresa, idType);
                var password = GetPassword(idEmpresa, idType);
                var token = $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{user}:{password}"))}";

                var result = await _Service.PostAsync<String>(url, null, token);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <param name="token"></param>
        /// <param name="amount"></param>
        /// <param name="ip"></param>
        /// <param name="idNroServicio"></param>
        /// <returns></returns>
        public async Task<Dictionary<String, String>> CreateSession(Int16 idEmpresa, Int16 idType, String token, Decimal amount, String ip, Int32 idNroServicio)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(token)) token = await CreateToken(idEmpresa, idType);

                var url = GetURLSession(idEmpresa);
                var merchantId = GetMerchantId(idEmpresa, idType);
                var param = new
                {
                    amount = amount,
                    channel = (idType == 2 ? "web" : ""),
                    recurrenceMaxAmount = (Decimal?)null,
                    antifraud = new
                    {
                        clientIp = ip,
                        merchantDefineData = new
                        {
                            MDD1 = "web",
                            MDD2 = "Canl",
                            MDD3 = "Canl",
                            MDD32 = idNroServicio
                        }
                    }
                };

                var result = await _Service.PostAsync<Dictionary<String, String>>($"{url}/{merchantId}", param, token);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <param name="token"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<clsResultado> AuthorizeSession(Int16 idEmpresa, Int16 idType, String token, Decimal amount, Int32 idOrder, String tokenId)
        {
            try
            {
                var url = GetURLAuthorization(idEmpresa);
                var merchantId = GetMerchantId(idEmpresa, idType);
                var param = new
                {
                    antifraud = (Object)null,
                    captureType = "manual",
                    channel = "web",
                    countable = "true",
                    order = new
                    {
                        currency = "PEN",
                        amount = amount,
                        purchaseNumber = idOrder,
                        tokenId = tokenId
                    }
                };

                var result = await _Service.PostAsync<clsResultado>($"{url}/{merchantId}", param, token, true);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="idEmpresa"></param>
        /// <param name="idType">(1)Mobile, (2) Web</param>
        /// <param name="token"></param>
        /// <param name="idOrder"></param>
        /// <returns></returns>
        public async Task<clsResultado> VerifyOrder(Int16 idEmpresa, Int16 idType, String token, Int32 idOrder)
        {
            try
            {
                var url = GetURLVerification(idEmpresa);
                var merchantId = GetMerchantId(idEmpresa, idType);

                var result = await _Service.GetAsync<clsResultado>($"{url}/{merchantId}/{idOrder}", null, token, true);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<T> SolicitarAutorizacion<T>(Int16 idEmpresa, String token, clsParametroVisaAuthorization param)
        {
            try
            {
                var url = await GetVisaURLAutorizacion(idEmpresa);
                var id = await GetVisaMerchantId(idEmpresa);

                var result = await _Service.SendData<T>($"{url}/{id}", param, "", "", token, "Error VISA Pago", true, true);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Public
    }
}