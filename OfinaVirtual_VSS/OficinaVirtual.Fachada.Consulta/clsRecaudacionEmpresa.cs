﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System.Configuration;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public class clsRecaudacionEmpresa
    {
        private static readonly clsService _Service = clsService.Instancia;
        private static string _UrlSiteAtencion = ConfigurationManager.AppSettings["WebService_ATENCION"] + "Cobranza/";


        public static async Task<clsResultadoRecaudacion> IntentoPagoGrabar(clsParametroBulkData oParametro)
        {
            var result = await _Service.SendData<clsResultadoRecaudacion>(_UrlSiteAtencion + "GrabarIntentoEmpresa", oParametro);

            return result;
        }

        public static async Task<clsResultadoRecaudacion> SuministroPagarDeudaEmpresa(clsParametroEntrada oParametro)
        {
            var result = await _Service.SendData<clsResultadoRecaudacion>(_UrlSiteAtencion + "PagarDeudaEmpresa", oParametro);

            return result;
        }
        public static async Task<clsResultadoRecaudacion> ListarBancos(clsParametroEntrada oParametro)
        {
            var result = await _Service.SendData<clsResultadoRecaudacion>(_UrlSiteAtencion + "ListarBancos", oParametro);

            return result;
        }
        public static async Task<clsResultadoRecaudacion> ListarCuentas(clsParametroEntrada oParametro)
        {
            var result = await _Service.SendData<clsResultadoRecaudacion>(_UrlSiteAtencion + "ListarCuentas", oParametro);

            return result;
        }

        public static async Task<clsResultadoRecaudacion> GrabarDepositoBancarioIntento(clsParametroBulkData oParametro)
        {
            var result = await _Service.SendData<clsResultadoRecaudacion>(_UrlSiteAtencion + "GrabarDepositoBancarioIntento", oParametro);

            return result;
        }

        public static async Task<clsResultadoRecaudacion> ActualizarDepositoBancarioEstado(clsParametro oParametro)
        {
            var result = await _Service.SendData<clsResultadoRecaudacion>(_UrlSiteAtencion + "ActualizarDepositoBancarioEstado", oParametro);

            return result;
        }




    }
}
