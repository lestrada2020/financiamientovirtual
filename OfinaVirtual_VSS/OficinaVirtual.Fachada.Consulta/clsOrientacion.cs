﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public sealed class clsOrientacion
    {
        private static readonly String _ServiceDM = ConfigurationManager.AppSettings["WebService_PRINCIPAL"] + "serviciodistriluzmobile/";
        private static readonly clsService _Service = clsService.Instancia;

        public static async Task<clsListaOrientacion> ListarOrientacion()
        {
            return await _Service.PostAsync<clsListaOrientacion>($"{_ServiceDM}listarorientacion", null);
        }

        public static async Task<clsListaRequisitos> RequisitosXOrientacion(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsListaRequisitos>($"{_ServiceDM}listarequisitos", param);
        }
    }
}