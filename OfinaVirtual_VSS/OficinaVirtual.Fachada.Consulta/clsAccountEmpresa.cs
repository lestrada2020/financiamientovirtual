﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace OficinaVirtual.Fachada.Consulta
{
    public class clsAccountEmpresa
    {
        private static string _UrlSiteAtencion = ConfigurationManager.AppSettings["WebService_ATENCION"] + "OficinaVirtual/";
        private static readonly clsService _Service = clsService.Instancia;

        public static async Task<clsResultado> CallMethod(String url, clsParametro param)
        {
            var result = new clsResultado();

            try
            {
                result = await _Service.SendData<clsResultado>(url, param);
            }
            catch (Exception ex)
            {
                result.SetError(1, ex.Message);
            }

            return result;
        }

        public static async Task<clsResultado> CallMethod2(String url, clsParametroBulkData param)
        {
            var result = new clsResultado();

            try
            {
                result = await _Service.SendData<clsResultado>(url, param);
            }
            catch (Exception ex)
            {
                result.SetError(1, ex.Message);
            }

            return result;
        }


        public static async Task<clsResultado> ValidaAccesoClienteEmpresa(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "ValidaAccesoClienteEmpresa", param);
        }

        public static async Task<clsResultado> RegistrarClienteEmpresaAdmin(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "RegistrarClienteEmpresaAdmin", param);
        }

        public static async Task<clsResultado> BuscarPorIdentidad(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "BuscarPorIdentidad", param);
        }
        public static async Task<clsResultado> ListarClienteEmpresa(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "ListarClienteEmpresa", param);
        }

        public static async Task<clsResultado> ListarRoles(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "ListarRoles", param);
        }

        public static async Task<clsResultado> TipoRelacion(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "TipoRelacion", param);
        }

        public static async Task<clsResultado> RegistrarClienteEmpresa(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "RegistrarClienteEmpresa", param);
        }

        public static async Task<clsResultado> EditarClienteEmpresa(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "EditarClienteEmpresa", param);
        }

        public static async Task<clsResultado> EliminarClienteEmpresa(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "EliminarClienteEmpresa", param);
        }


        public static async Task<clsResultado> ListarTarifaActiva(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "ListarTarifaActiva", param);
        }


        public static async Task<clsResultado> ListarSuministros(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "ListarSuministros", param);
        }

        public static async Task<clsResultado> ListarSuministrosDetallePago(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "ListarSuministrosDetallePago", param);
        }

        public static async Task<clsResultado> EliminarSuministroAsociado(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "EliminarSuministroAsociado", param);
        }

        public static async Task<clsResultado> BuscarSuministroAsociar(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "BuscarSuministroAsociar", param);
        }

        public static async Task<clsResultado> RegistrarSuministroAsociar(clsParametro param)
        {
            return await CallMethod(_UrlSiteAtencion + "RegistrarSuministroAsociar", param);
        }

        public static async Task<clsResultado> ListarSuministrosCarga(clsParametroBulkData param)
        {
            return await CallMethod2(_UrlSiteAtencion + "ListarSuministrosCarga", param);
        }

    }
}