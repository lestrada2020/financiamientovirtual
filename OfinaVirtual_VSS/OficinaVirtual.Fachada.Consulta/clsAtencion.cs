﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public sealed class clsAtencion
    {
        #region Field

        private static readonly String _ServiceOV = ConfigurationManager.AppSettings["WebService_ATENCION"] + "OficinaVirtual/";
        private static readonly String _ServiceCG = ConfigurationManager.AppSettings["WebService_ATENCION"] + "ConsultaGeneral/";
        private static readonly String _ServiceRe = ConfigurationManager.AppSettings["WebService_FACTURACION"] + "Recibo/";
        private static readonly String _ServiceFN = ConfigurationManager.AppSettings["WebService_ATENCION"] + "Financiamiento/"; //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly Lazy<clsAtencion> _Instancia = new Lazy<clsAtencion>(() => new clsAtencion(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsAtencion Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsAtencion()
        {
        }

        #endregion Constructor

        public static async Task<clsResultado> CallMethod(String url, clsParametro param)
        {
            var result = new clsResultado();

            try
            {
                result = await _Service.SendData<clsResultado>(url, param);
            }
            catch (Exception ex)
            {
                result.SetError(1, ex.Message);
            }

            return result;
        }

        public async Task<clsResultado> ConsultaAtencionesSuministro(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ConsultaAtencionesSuministro", param);
        }

        public async Task<clsResultado> ConsultaAtencionNroAtencion(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ConsultaAtencionNroAtencion", param);
        }

        public async Task<clsResultado> RegistrarContactanos(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}RegistrarContactanos", param);
        }

        public async Task<clsResultado> ListarInterrupciones(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ListarInterrupciones", param);
        }

        public async Task<clsResultado> DescargarRecibo(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceRe}DescargarRecibo", param);
        }

        public async Task<clsResultado> ConfirmarEmail(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ConfirmarEmail", param);
        }

        public async Task<clsResultado> ObtenerCtaCte(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceCG}CtaCte", param);
        }

        public async Task<clsResultado> RegistrarReclamo(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}RegistrarReclamo", param);
        }

        public async Task<clsResultado> InfoSuministro(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}InfoSuministro", param);
        }

        public async Task<clsResultado> ObtenerObservacion(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ObtenerObservacion", param);
        }

        public static async Task<clsResultado> ObtenerObservacion2(clsParametro param)
        {
            return await CallMethod(_ServiceOV + "ObtenerObservacion", param);
        }

        public async Task<clsResultado> DesasociarSuministro(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}DesasociarSuministro", param);
        }

        public async Task<clsResultado> CambiarPassword(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}CambiarPassword", param);
        }

        public async Task<clsResultado> ListarByNroAtencion(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ListarByNroAtencion", param);
        }

        public async Task<clsResultado> RegistrarFotoLec(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}RegistrarFotoLec", param);
        }

        public async Task<clsResultado> VisaListFailed(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}VisaListFailed", param);
        }

        public async Task<clsResultado> VisaSendMailPayFailed(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}VisaSendMailPayFailed", param);
        }

        public async Task<clsResultado> ObtenerFile(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ObtenerFile", param);
        }

        #region Beneficiario

        public async Task<clsResultado> ObtenerBeneficiario(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ObtenerBeneficiario", param);
        }

        public async Task<clsResultado> RegistrarBeneficiario(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}RegistrarBeneficiario", param);
        }

        #endregion Beneficiario

        public async Task<clsResultado> DatosNroServicio(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}DatosNroServicio", param);
        }

        public async Task<clsResultado> ListarNroServicio(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ListarNroServicio", param);
        }

        public async Task<clsResultado> LecturaRegistrar(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}LecturaRegistrar", param);
        }

        #region Cobranza

        public async Task<clsResultado> ConsultarDeudaDetalle(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ConsultarDeudaDetalle", param);
        }

        public async Task<clsResultado> GrabarIntentoDetalle(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}GrabarIntentoDetalle", param);
        }

        public async Task<clsResultado> ObtenerIntento(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ObtenerIntento", param);
        }

        #endregion Cobranza

        #region Bono Electrico

        public async Task<clsResultado> ObtenerBonoElectrico(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ObtenerBonoElectrico", param);
        }

        public async Task<clsResultado> ObtenerBonoElectricoDNI(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceOV}ObtenerBonoElectricoDNI", param);
        }

        #endregion Bono Electrico

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        #region Distriluz-036 MCastro Financiamiento

        public async Task<ClsListaFinanciamiento> ListarSuministroAsociado(clsParametro param)
        {
            return await _Service.PostAsync<ClsListaFinanciamiento>($"{_ServiceFN}ListarSuministroAsociado", param);
        }
        public async Task<clsObtenerConfiguracionGlobalResponse> ObtenerConfiguracionGlobal(clsObtenerConfiguracionGlobalRequest param)
        {
            return await _Service.PostAsync<clsObtenerConfiguracionGlobalResponse>($"{_ServiceFN}ObtenerConfiguracionGlobal", param);
        }

        public async Task<clsResultado> AnularOrdenCobroPendiente(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}AnularOrdenCobroPendiente", param);
        }

        public async Task<clsListaDetalleDeuda> ObtenerDetalleDeuda(clsObtenerDetalleDeudaRequest param)
        {
            return await _Service.PostAsync<clsListaDetalleDeuda>($"{_ServiceFN}ObtenerDetalleDeuda", param);
        }

        public async Task<clsResultado> ObtenerDetalleOrdenCobro(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}ObtenerDetalleOrdenCobro", param);
        }

        public async Task<clsSuministroOrdenCobroResponse> DatosSuministroOrdenCobro(clsParametro param)
        {
            return await _Service.PostAsync<clsSuministroOrdenCobroResponse>($"{_ServiceFN}ObtenerCabeceraOrdenCobro", param);
        }


        public async Task<ClsListaFinanciamientoDetalle> ObtenerDetalleFinanciamiento(clsParametro param)
        {
            var result = await _Service.PostAsync<ClsListaFinanciamientoDetalle>($"{_ServiceFN}ObtenerDetalleFinanciamiento", param);
            return result;
        }

        public async Task<clsResultado> GenerarOrdenCobro(clsParametro param)
        {
            var result = await _Service.PostAsync<clsResultado>($"{_ServiceFN}GenerarOrdenCobro", param);
            return result;
        }

        public async Task<clsResultado> ObtenerCentroServicios(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}ObtenerCentroServicios", param);
        }

        public async Task<clsResultado> ObtenerPorcentajesConvenio(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}ObtenerPorcentajesConvenio", param);
        }
        /// <summary>
        /// Generar la orden de cobro
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<clsResultado> GenerarNroPedido(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}GenerarNroPedido", param); //PASO 1
        }

        public async Task<clsResultado> ObtenerMetodoPago(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}ObtenerMetodosPago", param);
        }

        public async Task<clsResultadoRecaudacion> IntentoPagoActualizarFinanciamiento(clsParametroEntrada param)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceFN}ActualizarIntentoPago", param);

            return result;
        }

        public async Task<clsResultado> ObtenerIntentoPagoFinanciamiento(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}ObtenerIntentoPagoFinanciamiento", param);
        }

        public async Task<clsResultado> ObtenerDocumentoComprobantePago(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}ObtenerDocumentoComprobantePago", param);
        }

        public async Task<clsResultado> ObtenerRutaTransaccion(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}GenerarDocumentoTransaccionExtrajudicial", param);
        }

        public async Task<clsResultado> GenerarPdf(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}generarcomprobantepago", param);
        }

        public async Task<clsResultado> EstadoPagoDistriluz(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceFN}EstadoPagoDistriluz", param);
        }

        #endregion

        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
    }
}