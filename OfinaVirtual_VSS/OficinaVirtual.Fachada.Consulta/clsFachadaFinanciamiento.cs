﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public class clsFachadaFinanciamiento
    {
        #region Field

        private static readonly String _ServiceFN = ConfigurationManager.AppSettings["WebService_ATENCION"] + "Financiamiento/";
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly Lazy<clsFachadaFinanciamiento> _Instancia = new Lazy<clsFachadaFinanciamiento>(() => new clsFachadaFinanciamiento(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsFachadaFinanciamiento Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsFachadaFinanciamiento()
        {
        }

        #endregion Constructor


        public async Task<clsResultado> VisaListFailed(clsParametro param)
        {
            try
            {
                return await _Service.PostAsync<clsResultado>($"{_ServiceFN}VisaListFailed", param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
