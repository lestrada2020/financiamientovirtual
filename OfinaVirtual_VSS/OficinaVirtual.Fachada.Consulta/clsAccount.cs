﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public sealed class clsAccount
    {
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly String _UrlSitePrincipal = ConfigurationManager.AppSettings["WebService_PRINCIPAL"];
        private static readonly String _UrlSiteAtencion = ConfigurationManager.AppSettings["WebService_ATENCION"] + "OficinaVirtual/";
        private static readonly String _UrlSiteIntegracion = ConfigurationManager.AppSettings["WebService_Integracion"];
        private static readonly String _TokenIntegracion = ConfigurationManager.AppSettings["TokenIntegracion"];

        private const string _UrlValidaAcceso = "servicioseguridad/validacredenciales";
        private const string _UrlRegistrarUsuario = "servicioseguridad/registrarusuario";
        private const string _UrlCambiarContraseña = "servicioseguridad/recuperarcontrasena";
        private const string _UrlActualizarUsuario = "servicioregistro/registrarconfiguracion";
        private const string _UrlRegistrarContactenos = "servicioregistro/registrarcontactanos";
        private const string _UrlListaEmpresasXUsuario = "serviciodistriluzmobile/comunicate";

        private static readonly Lazy<clsAccount> _Instancia = new Lazy<clsAccount>(() => new clsAccount(), LazyThreadSafetyMode.PublicationOnly);

        #region Property

        public static clsAccount Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsAccount()
        {
        }

        #endregion Constructor

        #region Public

        public async Task<clsResultado> RegistrarUsuario(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_UrlSiteAtencion}RegistrarUsuario", param);
        }

        public async Task<clsResultado> ActivarCuentaXsms(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_UrlSiteAtencion}ActivarCuentaXsms", param);
        }

        public async Task<clsResultado> ReenviarSMS(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_UrlSiteAtencion}ReenviarSMS", param);
        }

        public async Task<clsUsuario> ValidaAcceso(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsUsuario>(_UrlSitePrincipal + _UrlValidaAcceso, param);
        }

        public async Task<clsUsuarioRegistro> RegistroUsuario(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsUsuarioRegistro>(_UrlSitePrincipal + _UrlRegistrarUsuario, param);
        }

        public async Task<clsResultadoOperacion> ActualizarUsuario(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsResultadoOperacion>(_UrlSitePrincipal + _UrlActualizarUsuario, param);
        }

        public async Task<clsResultadoOperacion> CambiarContraseña(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsResultadoOperacion>(_UrlSitePrincipal + _UrlCambiarContraseña, param);
        }

        public async Task<clsEmpresasXUsuario> ListarEmpresasXUsuario(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsEmpresasXUsuario>(_UrlSitePrincipal + _UrlListaEmpresasXUsuario, param);
        }

        public async Task<clsResultadoOperacion> RegistrarContactenos(clsParametroConsulta param)
        {
            return await _Service.PostAsync<clsResultadoOperacion>(_UrlSitePrincipal + _UrlRegistrarContactenos, param);
        }

        public static async Task<clsResultado> ObtenerDatosConfirmacion(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_UrlSiteAtencion}ObtenerDatosConfirmacion", param);
        }

        public static async Task<clsResultado> SolicitarNewPassword(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_UrlSiteAtencion}SolicitarNewPassword", param);
        }

        public async Task<clsResultado> SolicitarDNIPIDE(clsParametro param)
        {
            param.Parametros["SessionToken"] = _TokenIntegracion;
            return await _Service.PostAsync<clsResultado>($"{_UrlSiteIntegracion}Reniec/Consultar", param);
        }

        #endregion Public
    }
}