﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.Consulta
{
    public sealed class clsRecaudacion
    {
        #region Field

        private static readonly clsService _Service = clsService.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly String _ServiceCo = ConfigurationManager.AppSettings["WebService_ATENCION"] + "Cobranza/";
        private static readonly String _ServiceFN = ConfigurationManager.AppSettings["WebService_ATENCION"] + "Financiamiento/"; //DISTRILUZ 036 FINANCIAMIENTO VIRTUAL

        private static readonly Lazy<clsRecaudacion> _Instancia = new Lazy<clsRecaudacion>(() => new clsRecaudacion(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsRecaudacion Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsRecaudacion()
        {
        }

        #endregion Constructor

        public async Task<clsResultadoRecaudacion> SuministroConsultarDeuda(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceCo}ConsultarDeuda", oParametro);

            return result;
        }

        public async Task<clsResultadoRecaudacion> SuministroPagarDeuda(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceCo}PagarDeuda", oParametro);

            return result;
        }

        public async Task<clsResultadoRecaudacion> IntentoPagoGrabar(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceCo}GrabarIntento", oParametro);

            return result;
        }

        public async Task<clsResultadoRecaudacion> IntentoPagoActualizar(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceCo}ActualizarIntento", oParametro);

            return result;
        }

        public static async Task<clsResultadoRecaudacion> IntentoPagoActualizar2(clsParametroEntrada oParametro)
        {
            var result = await _Service.SendData<clsResultadoRecaudacion>(_ServiceCo + "ActualizarIntento", oParametro);

            return result;
        }

        public async Task<clsResultadoRecaudacion> SuministroConsultarPago(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceCo}ObtenerPagoResumen", oParametro);

            return result;
        }

        public async Task<clsResultadoRecaudacion> SuministroEnviarCorreoPago(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceCo}EnviarEmail", oParametro);

            return result;
        }

        public async Task<clsResultado> ObtenerDeudaPeriodos(clsParametro param)
        {
            var result = await _Service.PostAsync<clsResultado>($"{_ServiceCo}ConsultarDeudaDetalle", param);

            return result;
        }

        //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
        #region Financiamiento
        //_ServiceFN
        public async Task<clsResultadoRecaudacion> PagarDeudaFinanciamiento(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceFN}PagarDeuda", oParametro);

            return result;
        }

        public async Task<clsResultadoRecaudacion> FinanciamientoEnviarEmail(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceFN}EnviarEmail", oParametro);

            return result;
        }

        public async Task<clsResultadoRecaudacion> FinanciamientoEnviarEmailError(clsParametroEntrada oParametro)
        {
            var result = await _Service.PostAsync<clsResultadoRecaudacion>($"{_ServiceFN}FinanciamientoEnviarEmailError", oParametro);

            return result;
        }


        #endregion

        //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL

    }
}