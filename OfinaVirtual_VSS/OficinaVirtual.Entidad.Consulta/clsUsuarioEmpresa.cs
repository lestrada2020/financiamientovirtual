﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Collections.Generic;

namespace OficinaVirtual.Entidad.Consulta
{
    public class clsLogin
    {
        [Display(Name = "RUC")]
        [Required(ErrorMessage = "El RUC es requerido")]
        [StringLength(12, ErrorMessage = "El RUC debe tener un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El RUC debe ser numérico")]
        public string ruc { get; set; }

        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "El Usuario es requerido")]
        [StringLength(25, ErrorMessage = "El Usuario debe tener un máximo de 25 caracteres")]
        public string usuario { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La contraseña es requerida")]
        [StringLength(25, ErrorMessage = "La contraseña debe tener un máximo de 25 caracteres")]
        [DataType(DataType.Password)]
        public string clave { get; set; }
    }

    public class clsUsuarioEmpresaLoginAcceso
    {
        public Dictionary<String, String> LoginAcceso { get; set; }
    }

    public class clsUsuarioEmpresa : clsResultadoOperacion
    {
        public int IdUsuario { get; set; }
        public string UsuarioLogin { get; set; }
        public string Token { get; set; }
        public DateTime FechaUltimoAcceso { get; set; }
        public string DatosControlTag { get; set; }
    }

    public class _clsUsuarioEmpresa : clsResultadoOperacion
    {
        [Display(Name = "RUC")]
        [Required(ErrorMessage = "El RUC es requerido")]
        [StringLength(12, MinimumLength = 8, ErrorMessage = "El RUC debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El RUC debe ser numérico")]
        public string empresaRuc { get; set; }

        [Display(Name = "Razón social")]
        [Required(ErrorMessage = "La razón social es requerido")]
        [StringLength(60, ErrorMessage = "La razón social debe tener un máximo de 60 caracteres")]
        public string razonsocial { get; set; }

        [Display(Name = "Nombres")]
        [Required(ErrorMessage = "El nombre es requerido")]
        [StringLength(60, ErrorMessage = "El nombre debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string nombres { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "El apellido paterno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido paterno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string apaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "El apellido materno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido materno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string amaterno { get; set; }

        [Display(Name = "Tipo de Documento")]
        [Required(ErrorMessage = "El tipo de documento es requerido")]
        public string IdTipoIdentidad { get; set; }

        [Display(Name = "N° de Documento")]
        [Required(ErrorMessage = "El documento de identidad es requerido")]
        [StringLength(12, MinimumLength = 8, ErrorMessage = "El documento de identidad debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El documento de identidad debe ser numérico")]
        public string nrodocumento { get; set; }

        [Display(Name = "Correo Electrónico")]
        [Required(ErrorMessage = "El correo es requerido")]
        [EmailAddress(ErrorMessage = "El correo no tiene el formato correcto")]
        [RegularExpression("^[A-Z a-z 0-9@._-]+$", ErrorMessage = "El correo no permite caracteres especiales")]
        [DataType(DataType.EmailAddress)]
        public string correo { get; set; }

        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "El usuario es requerida")]
        [StringLength(25, MinimumLength = 8, ErrorMessage = "El usuario debe tener un mínimo de 8 y un máximo de 25 caracteres")]
        public string usuario { get; set; }

        [Display(Name = "N° de Recibo (4 últimos digitos)")]
        [Required(ErrorMessage = "El número de recibo es requerido")]
        [StringLength(4, MinimumLength = 1, ErrorMessage = "El número de recibo debe tener un máximo de 4 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El número de recibo debe ser numérico")]
        public string nrorecibo { get; set; }

        [Display(Name = "N° de Suministro (4 últimos digitos)")]
        [Required(ErrorMessage = "El número de suministro es requerido")]
        [StringLength(4, MinimumLength = 1, ErrorMessage = "El número de suministro debe tener un máximo de 4 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El número de suministro debe ser numérico")]
        public string idnroservicio { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La Contraseña es requerida")]
        [StringLength(12, ErrorMessage = "La Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string clave { get; set; }

        [Display(Name = "Confirmar Contraseña")]
        [Required(ErrorMessage = "Confirmar Contraseña es requerida")]
        [Compare("clave", ErrorMessage = "Las claves no coinciden")]
        [StringLength(12, ErrorMessage = "Confirmar Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string confirmacionclave { get; set; }
    }

    public class clsBuscarPorIdentidad
    {
        public string idtipoidentidad { get; set; }
        public string nroidentidad { get; set; }
    }

    public class clsRegistrarUsuarios
    {
        [Display(Name = "Nombre")]
        public string nombre { get; set; }

        [Display(Name = "Correo Electrónico")]
        public string correo { get; set; }

        [Display(Name = "Usuario")]
        public string usuario { get; set; }

        [Display(Name = "Empresa Cliente")]
        public string empresacliente { get; set; }

        [Display(Name = "Estado")]
        public string estado { get; set; }

        [Display(Name = "Guardar")]
        public string guardar { get; set; }

    }

    public class clsNuevoUsuarioEmpresa
    {
        [Display(Name = "Empresa")]
        [Required(ErrorMessage = "La Empresa es requerido")]
        [StringLength(60, ErrorMessage = "El nombre de la empresa debe tener un máximo de 60 caracteres")]
        public string empresa { get; set; }

        [Display(Name = "Tipo de documento")]
        [Required(ErrorMessage = "El tipo de documento es requerido")]
        public string tipdoc { get; set; }

        [Display(Name = "Número de documento")]
        [Required(ErrorMessage = "El documento de identidad es requerido")]
        [StringLength(12, MinimumLength = 8, ErrorMessage = "El documento de identidad debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El documento de identidad debe ser numérico")]
        public string numdoc { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El nombre es requerido")]
        [StringLength(60, ErrorMessage = "El nombre debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "El apellido paterno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido paterno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string apaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "El apellido materno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido materno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string amaterno { get; set; }

        [Display(Name = "Correo")]
        [Required(ErrorMessage = "El correo es requerido")]
        [EmailAddress(ErrorMessage = "El correo no tiene el formato correcto")]
        [RegularExpression("^[A-Z a-z 0-9@._-]+$", ErrorMessage = "El correo no permite caracteres especiales")]
        [DataType(DataType.EmailAddress)]
        public string correo { get; set; }

        [Display(Name = "Cargo / Función")]
        [Required(ErrorMessage = "El Cargo / Función es requerido")]
        public string carfun { get; set; }

        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "El usuario es requerida")]
        [StringLength(25, MinimumLength = 8, ErrorMessage = "El usuario debe tener un mínimo de 8 y un máximo de 25 caracteres")]
        public string usuario { get; set; }

        [Display(Name = "Rol de aplicación")]
        [Required(ErrorMessage = "El Rol es requerido")]
        public string rolapl { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La Contraseña es requerida")]
        [StringLength(12, ErrorMessage = "La Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string clave { get; set; }

        [Display(Name = "Confirmar contraseña")]
        [Required(ErrorMessage = "Confirmar Contraseña es requerida")]
        [Compare("clave", ErrorMessage = "Las claves no coinciden")]
        [StringLength(12, ErrorMessage = "Confirmar Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string concla { get; set; }

        //[Display(Name = "Representante legal")]
        //public bool resleg { get; set; }

        [Display(Name = "Tipo Representante")]
        [Required(ErrorMessage = "El tipo de representante es requerido")]
        public string tiprep { get; set; }

        [Display(Name = "Número de Representante")]
        [Required(ErrorMessage = "El número de representante es requerido")]
        [StringLength(12, MinimumLength = 8, ErrorMessage = "El número de representante debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El número de representante debe ser numérico")]
        public string numrep { get; set; }

        [Display(Name = "Estado")]
        [Required(ErrorMessage = "El estado es requerido")]
        public string estado { get; set; }

    }

    public class clsEditarUsuarioEmpresa
    {

        [Display(Name = "Empresa")]
        public string empresa { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El nombre es requerido")]
        [StringLength(60, ErrorMessage = "El nombre debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "El apellido paterno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido paterno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string apaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "El apellido materno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido materno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string amaterno { get; set; }

        [Display(Name = "Correo")]
        [Required(ErrorMessage = "El correo es requerido")]
        [EmailAddress(ErrorMessage = "El correo no tiene el formato correcto")]
        [RegularExpression("^[A-Z a-z 0-9@._-]+$", ErrorMessage = "El correo no permite caracteres especiales")]
        [DataType(DataType.EmailAddress)]
        public string correo { get; set; }

        [Display(Name = "Cargo / Función")]
        [Required(ErrorMessage = "El Cargo / Función es requerido")]
        public string carfun { get; set; }

        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "El usuario es requerida")]
        [StringLength(25, MinimumLength = 8, ErrorMessage = "El usuario debe tener un mínimo de 8 y un máximo de 25 caracteres")]
        public string usuario { get; set; }

        public string idusuario { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La Contraseña es requerida")]
        [StringLength(12, ErrorMessage = "La Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string clave { get; set; }

        [Display(Name = "Confirmar contraseña")]
        [Required(ErrorMessage = "Confirmar Contraseña es requerida")]
        [Compare("clave", ErrorMessage = "Las claves no coinciden")]
        [StringLength(12, ErrorMessage = "Confirmar Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string concla { get; set; }

        [Display(Name = "Tipo Representante Legal")]
        [Required(ErrorMessage = "El tipo de documento es requerido")]
        public string tiprep { get; set; }

        [Display(Name = "Número Representante Legal")]
        [Required(ErrorMessage = "El número de documento es requerido")]
        [StringLength(12, MinimumLength = 8, ErrorMessage = "El número de documento debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El número de documento debe ser numérico")]
        public string numrep { get; set; }

        [Display(Name = "Estado")]
        [Required(ErrorMessage = "El estado es requerido")]
        public string estado { get; set; }

    }

    public class clsEliminarClienteEmpresa
    {
        public string idusuario { get; set; }
    }

}