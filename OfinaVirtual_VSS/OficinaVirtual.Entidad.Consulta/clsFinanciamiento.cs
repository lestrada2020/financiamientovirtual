﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OficinaVirtual.Entidad.General;

namespace OficinaVirtual.Entidad.Consulta
{
    public class ClsFinanciamiento
    {
        public int IdNroServicio { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Cartera { get; set; }
        public string ControlTag { get; set; }
        public string FechaEmision { get; set; }
        public string FechaVcto { get; set; }
        public double Deuda { get; set; }
        public double InteresFechaActual { get; set; }
        public double TotalDeuda { get; set; }
        public int MesesDeuda { get; set; }
        public int PuedeFinanciar { get; set; }
    }

    public class ClsListaFinanciamiento
    {
        public List<ClsFinanciamiento> Datos { get; set; }

    }

    public class ClsListaFinanciamientoDetalle
    {
        public List<ClsFinanciamientoDetalle> Datos { get; set; }
    }


    public class ClsFinanciamientoDetalle
    {
        public int IdNroServicio { get; set; }
        public int IdEmpresa { get; set; }
        public string Empresa { get; set; }
        public int IdUUNN { get; set; }
        public string UUNN { get; set; }

        public int IdCentroServicio { get; set; }
        public string AbreviaCentroServicio { get; set; }
        public string Convenio { get; set; }
        public string Servicio { get; set; }
        public string IdSuministro { get; set; }
        public string Nombre { get; set; }
        public string FechaEmision { get; set; }
        public string FechaVencimiento { get; set; }

        public int IdEstado { get; set; }
        public string Situacion { get; set; }
        public string Cartera { get; set; }
        public string Tarifa { get; set; }
        public int MesesDeuda { get; set; }
        public double DeudaFacturada { get; set; }
        public double FacturadoTerceros { get; set; }
        public double SaldoDeuda { get; set; }
        public double AFavor { get; set; }
        public double Total { get; set; }
        public double TotalFacturado { get; set; }
        public double TotalAFactura { get; set; }
        public double Interes { get; set; }
        public double TotalDeuda { get; set; }
        public double DeudaCastigo { get; set; }
        public double DeudaProvision { get; set; }
        public double TotalSaldoFavor { get; set; }
        public double SaldoFavorUsar { get; set; }
        public double Saldo { get; set; }
        public double NroCuotas { get; set; }
        public double ImporteCuota { get; set; }
        public int Moneda { get; set; }
        public double ValorTasaInteres { get; set; }
        public string IdTipoIdentidadPropietario { get; set; }
        public string NroIdentidadPropietario { get; set; }
        public int IdConvenioTipo { get; set; }

    }

    public class ClsMetodoPago
    {
        public int IdMetodo { get; set; }
        public int IdConvenioTipo { get; set; }
        public string Descripcion { get; set; }
        public string Icono { get; set; }
        public int IdEstado { get; set; }

        public string urlIcono { get; set; }
    }

    public enum MetodoPago
    {
        PagoNiubiz = 1,
        DepositoCuenta = 2,
        OrdenPago = 3
    }


    #region Distriluz-036 rberrospi

    public class clsObtenerConfiguracionGlobalResponse
    {
        public List<clsObtenerConfiguracionGlobalValor> Datos { get; set; }
    }

    public class clsObtenerConfiguracionGlobalValor
    {
        public string ColumnaValor { get; set; }
    }

    public class clsListaDetalleDeuda : ClsResultadoExt
    {
        public List<clsDetalleDeuda> Datos { get; set; }
    }

    public class clsDetalleDeuda
    {
        public string nombredocumento { get; set; }
        public string nrodocumento { get; set; }
        public int periodo { get; set; }
        public DateTime fechavencimiento { get; set; }
        public double importetotal { get; set; }
        public double saldo { get; set; }
        public double pagado { get; set; }
        public double saldofinanciar { get; set; }
        public string descripcionEstado { get; set; }
    }

    public class clsListaDetalleOrdenCobro
    {
        public List<clsDetalleOrdenCobro> Datos { get; set; }
    }

    public class clsDetalleOrdenCobro
    {
        public int IdOrdenCobroDocumento { get; set; }
        public string PeriodoComercial { get; set; }
        public int IdTipoDocumento { get; set; }
        public string Column1 { get; set; }
        public double Importe { get; set; }
        public string FechaVencimiento { get; set; }
    }

    public class clsSuministroOrdenCobroResponse
    {
        public Int32 IdError { get; set; }

        public String Mensaje { get; set; }

        public clsSuministroOrdenCobro Datos { get; set; }
    }

    public class clsSuministroOrdenCobro
    {
        public int IdNroServicio { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public double Importe { get; set; }
    }

    #endregion




    #region pagos truncos

    public class OrdenVisaNetFallido
    {
        public int IdEstado { get; set; }
        public int IdNumeroOrden { get; set; }
        public int IdOrdenCobro { get; set; }
        public int MetodoPago { get; set; }
        public string Email { get; set; }
    }

    #endregion



}