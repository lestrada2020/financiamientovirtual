﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace OficinaVirtual.Entidad.Consulta
{
    public class clsUsuarioLogin
    {
        [Display(Name = "Documento Identidad")]
        [Required(ErrorMessage = "El Documento Identidad es requerido")]
        [StringLength(12, ErrorMessage = "El Documento Identidad debe tener un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El Documento Identidad debe ser numérico")]
        public string usuario { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La contraseña es requerida")]
        [StringLength(12, ErrorMessage = "La contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string clave { get; set; }
    }

    public class clsUsuario : clsResultadoOperacion
    {
        public int IdUsuario { get; set; }
        public string UsuarioLogin { get; set; }
        public string Token { get; set; }
        public DateTime FechaUltimoAcceso { get; set; }
        public string DatosControlTag { get; set; }
    }

    public class clsUsuarioRegistro : clsResultadoOperacion
    {
        [Display(Name = "Nombres")]
        [Required(ErrorMessage = "El nombre es requerido")]
        [StringLength(60, ErrorMessage = "El nombre debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string nombres { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "El apellido paterno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido paterno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string aPaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "El apellido materno es requerido")]
        [StringLength(60, ErrorMessage = "El apellido materno debe tener un máximo de 60 caracteres")]
        [RegularExpression("^[A-Z a-zñÑáéíóúÁÉÍÓÚ]+$", ErrorMessage = "El nombre solo permite textos")]
        public string aMaterno { get; set; }

        [Display(Name = "Tipo de Documento")]
        [Required(ErrorMessage = "El tipo de documento es requerido")]
        //[StringLength(12, MinimumLength = 8, ErrorMessage = "El documento de identidad debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "El documento de identidad debe ser numérico")]
        public string tipodocumento { get; set; }

        [Display(Name = "Documento de Identidad (Usuario)")]
        [Required(ErrorMessage = "El documento de identidad es requerido")]
        [StringLength(12, MinimumLength = 8, ErrorMessage = "El documento de identidad debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El documento de identidad debe ser numérico")]
        public string documentoIdentidad { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "La fecha de nacimiento es requerido")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime fechaNacimiento { get; set; }

        [Display(Name = "Teléfono")]
        [Required(ErrorMessage = "El teléfono es requerido")]
        [StringLength(9, MinimumLength = 5, ErrorMessage = "El teléfono debe tener un mínimo de 5 y un máximo de 9 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El teléfono debe ser numérico")]
        public string telefono { get; set; }

        [Display(Name = "Correo Electrónico")]
        [Required(ErrorMessage = "El correo es requerido")]
        [EmailAddress(ErrorMessage = "El correo no tiene el formato correcto")]
        [RegularExpression("^[A-Z a-z 0-9@._-]+$", ErrorMessage = "El correo no permite caracteres especiales")]
        [DataType(DataType.EmailAddress)]
        public string correo { get; set; }

        [Display(Name = "Número de Suministro")]
        [Required(ErrorMessage = "El número de suministro es requerido")]
        [StringLength(8, MinimumLength = 1, ErrorMessage = "El número de suministro debe tener un máximo de 8 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El número de suministro debe ser numérico")]
        public string NroSuministro { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La Contraseña es requerida")]
        [StringLength(12, ErrorMessage = "La Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string clave { get; set; }

        [Display(Name = "Confirmar Contraseña")]
        [Required(ErrorMessage = "Confirmar Contraseña es requerida")]
        [Compare("clave", ErrorMessage = "Las claves no coinciden")]
        [StringLength(12, ErrorMessage = "Confirmar Contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string confirmacionclave { get; set; }


        [Display(Name = "Celular (Opcional)")]
        [StringLength(9, MinimumLength = 9, ErrorMessage = "El celular debe tener 9 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El celular debe ser numérico")]
        public string celular { get; set; }

        [Display(Name = "Codigo de Activacion")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "El codigo de activación debe tener 6 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El codigo de activación debe ser numérico")]
        public string codigoactivacion { get; set; }

        public String ExtraParametros { get; set; }
    }



    public class clsUsuarioActivacion : clsResultadoOperacion
    {

        [Display(Name = "Documento de Identidad (Usuario)")]
        [Required(ErrorMessage = "El documento de identidad es requerido")]
        [StringLength(12, MinimumLength = 8, ErrorMessage = "El documento de identidad debe tener un mínimo de 8 y un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El documento de identidad debe ser numérico")]
        public string documentoIdentidad { get; set; }

        [Display(Name = "Celular (Opcional)")]
        [StringLength(9, MinimumLength = 9, ErrorMessage = "El celular debe tener 9 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El celular debe ser numérico")]
        public string celular { get; set; }

        [Display(Name = "Codigo de Activacion")]
        [Required(ErrorMessage = "El código de activación es requerido")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "El código de activación debe tener 6 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El código de activación debe ser numérico")]
        public string codigoactivacion { get; set; }


    }





    public class clsUsuarioCambioContraseña
    {
        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "El usuario es requerido")]
        [StringLength(12, ErrorMessage = "El usuario debe tener un máximo de 12 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El usuario debe ser numérico")]
        public string usuario { get; set; }

        [Display(Name = "Correo Electrónico")]
        [Required(ErrorMessage = "El correo es requerido")]
        [EmailAddress(ErrorMessage = "El correo no tiene el formato correcto")]
        [RegularExpression("^[A-Z a-z 0-9@._-]+$", ErrorMessage = "El correo no permite caracteres especiales")]
        [DataType(DataType.EmailAddress)]
        public string correo { get; set; }
    }

    public class clsUsuarioContactenos
    {
        public string idusuario { get; set; }

        [Display(Name = "Correo Electrónico")]
        [Required(ErrorMessage = "El correo es requerido")]
        [EmailAddress(ErrorMessage = "El correo no tiene el formato correcto")]
        [RegularExpression("^[A-Z a-z 0-9@._-]+$", ErrorMessage = "El correo no permite caracteres especiales")]
        [DataType(DataType.EmailAddress)]
        public string correo { get; set; }

        [Display(Name = "Asunto")]
        [Required(ErrorMessage = "El asunto es requerido")]
        public string asunto { get; set; }

        [Display(Name = "Comentario")]
        [Required(ErrorMessage = "El comentario es requerido")]
        public string comentario { get; set; }

        public HttpPostedFileBase FileArchivo { get; set; }

        public Int16 IdEmpresa { get; set; }

        public Int32 IdNroServicio { get; set; }
    }

    public class clsUsuarioActualizar
    {
        public string idusuario { get; set; }

        [Display(Name = "Nombres")]
        public string NombreCompleto { get; set; }

        [Display(Name = "Correo")]
        [Required(ErrorMessage = "El correo es requerido")]
        [EmailAddress(ErrorMessage = "El correo no tiene el formato correcto")]
        [RegularExpression("^[A-Z a-z 0-9@._-]+$", ErrorMessage = "El correo no permite caracteres especiales")]
        [DataType(DataType.EmailAddress)]
        public string correo { get; set; }

        [Display(Name = "Teléfono")]
        [Required(ErrorMessage = "El teléfono es requerido")]
        [StringLength(9, MinimumLength = 5, ErrorMessage = "El teléfono debe tener un mínimo de 5 y un máximo de 9 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El teléfono debe ser numérico")]
        public string telefono { get; set; }

        [Display(Name = "Contraseña anterior")]
        [StringLength(12, ErrorMessage = "La contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string claveAntigua { get; set; }

        [Display(Name = "Nueva contraseña")]
        [StringLength(12, ErrorMessage = "La contraseña debe tener un máximo de 12 caracteres")]
        [DataType(DataType.Password)]
        public string clave { get; set; }

        [Display(Name = "Confirmar nueva contraseña")]
        [StringLength(12, ErrorMessage = "La nueva contraseña debe tener un máximo de 12 caracteres")]
        [Compare("clave", ErrorMessage = "Las contraseñas no coinciden")]
        [DataType(DataType.Password)]
        public string confirmacionClave { get; set; }
    }
}