﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OficinaVirtual.Entidad.Consulta
{

    public interface IControlTagBase
    {
        string ControlTag { get; set; }
    }

    public class clsHelperControlTag
    {
        public static string ObtenerValor(string controlTag, string clave, string valorPredeterminado)
        {
            clave = clave.ToLower();

            if (string.IsNullOrEmpty(controlTag) || !controlTag.ToLower().Contains(clave))
                return valorPredeterminado;

            string valor = valorPredeterminado;

            string[] comandos = controlTag.Split(';');

            try
            {
                foreach (var item in comandos)
                {
                    if (!string.IsNullOrEmpty(item)
                        && item.Split('|')[0].ToString().ToLower().Equals(clave))
                        valor = item.Split('|')[1];
                }
            }
            catch (Exception ex)
            {
                return valorPredeterminado;
            }

            return valor;
        }
    }

    public class clsControlTagBase
    {
        public int NumeroElementos
        {
            get
            {
                return this.ControlTag.Split('|').Count();
            }

        }

        public string ControlTag { get; set; }

        public object ObtenerValorControlTag(string clave, object valorPredeterminado)
        {
            clave = clave.ToLower();

            if (string.IsNullOrEmpty(ControlTag) || !ControlTag.ToLower().Contains(clave))
                return valorPredeterminado;

            object valor = valorPredeterminado;

            string[] comandos = this.ControlTag.Split(';');

            try
            {
                foreach (var item in comandos)
                {
                    if (!string.IsNullOrEmpty(item)
                        && item.Split('|')[0].ToString().ToLower().Equals(clave))
                        valor = item.Split('|')[1];
                }
            }
            catch (Exception ex)
            {
                return valorPredeterminado;
            }

            return valor;
        }
    }
}
