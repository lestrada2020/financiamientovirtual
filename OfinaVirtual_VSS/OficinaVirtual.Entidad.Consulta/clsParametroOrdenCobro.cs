﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OficinaVirtual.Entidad.Consulta
{
    public class clsParametroOrdenCobro
    {
        public int IdNroServicio { get; set; }
        public double Porcentaje { get; set; }
        public double MontoFinanciar { get; set; }
        public double MontoInicial { get; set; }
        public double InicialExigida { get; set; }
        public double TotalFacturado { get; set; }
        public double InteresAFecha { get; set; }
        public double TotalPorFacturar { get; set; }
        public double TotalSaldoAFavor { get; set; }
        public double SaldoAFavorUsar { get; set; }

        public int NumeroCuotas { get; set; }
        public int PlazoMaximo { get; set; }
        public double ImporteCuota { get; set; }
        public int IdEstado { get; set; }
        public int IdUUNN { get; set; }
        public int IdCentroServicio { get; set; }
        public int IdConvenioTipo { get; set; }
        public int IdMoneda { get; set; }
        public int IdEmpresa { get; set; }



    }
}
