﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OficinaVirtual.Entidad.Consulta
{
    public class clsSuministroInformacion : clsResultadoOperacion
    {
        public string Credenciales { get; set; }
        public List<clsInformacion> ListaInformacion { get; set; }

        public clsSuministroInformacion()
        {
        }
    }

    public class clsListaInformacionAsociado
    {
        public List<clsInformacionAsociado> Elementos { get; set; }
    }

    public class clsInformacionAsociado
    {
        public int IdNroServicio { get; set; }
        public string Nombre { get; set; }
        public string ImporteUltimoRecibo { get; set; }
        public string FechaEmision { get; set; }
        public string FechaVcto { get; set; }
        public string TotalDeuda { get; set; }
        public short MesesDeuda { get; set; }
        public string ControlTag { get; set; }
        public int ColorR { get; set; }
        public int ColorG { get; set; }
        public int ColorB { get; set; }
        public string ColorCard { get; set; }
        public string Comentario { get; set; }

        public string Tarifa { get; set; }
        public string Consumo { get; set; }
        public string Lectura { get; set; }
        public string FechaLectura { get; set; }
        public string ObservacionLectura { get; set; }

        public clsInformacion InformacionGrafico { get; set; }
    }

    public class clsHistoricos : clsResultadoOperacion
    {
        public List<clsInformacion> ListaInformacion { get; set; }
    }

    public class clsInformacion : clsResultadoOperacion
    {
        public string TipoPanel { get; set; }
        public string Configuracion { get; set; }
        public string Titulo { get; set; }
        public string Cabecera { get; set; }
        public string Pie { get; set; }
        public List<string> ListaRegistro { get; set; }
        public string ListaRegistroSerializado { get; set; }
    }

    public class clsDiagramaBarras
    {
        public string y_valor { get; set; }
        public string x_etiqueta { get; set; }
        public string color_texto { get; set; }
        public string color_barra { get; set; }
    }

    public class clsCortesProgramados : clsResultadoListaRegistro
    {
        public string FechaInicio { get; set; }
        public string FechaFinal { get; set; }
        public string Duracion { get; set; }
    }

    public class clsUltimosPagos : clsResultadoListaRegistro
    {
        public string fecha_pago { get; set; }
        public string lugar_pago { get; set; }
        public string monto_pago { get; set; }
    }

    public class clsUltimoReciboCabecera
    {
        public string Periodo { get; set; }
        public string IdNroServicio { get; set; }

        [Display(Name = "Tarifa")]
        public string Tarifa { get; set; }

        [Display(Name = "Consumo")]
        [DisplayFormat(DataFormatString = "{0:###,###.00}KWh", ApplyFormatInEditMode = true)]
        public string Consumo { get; set; }

        [Display(Name = "Emisión Actual")]
        public string Emision { get; set; }

        [Display(Name = "Vencimiento")]
        public string Vencimiento { get; set; }

        [Display(Name = "Total a Pagar")]
        [DisplayFormat(DataFormatString = "S/.{0:###,###.00}", ApplyFormatInEditMode = true)]
        public string Importe { get; set; }

        [Display(Name = "Recibo N°")]
        public string Recibo { get; set; }

        [Display(Name = "Deuda")]
        public string Deuda { get; set; }

        [Display(Name = "Tensión")]
        public string Tension { get; set; }

        [Display(Name = "Lectura Anterior")]
        [DisplayFormat(DataFormatString = "{0:###,###.00}", ApplyFormatInEditMode = true)]
        public string Lectura { get; set; }

        [Display(Name = "Circuito")]
        public string Circuito { get; set; }

        [Display(Name = "Sub estación")]
        public string SED { get; set; }

        [Display(Name = "Alimentador")]
        public string Alimentador { get; set; }

        [Display(Name = "Potencia Contratada")]
        [DisplayFormat(DataFormatString = "{0:###,###.00}KW", ApplyFormatInEditMode = true)]
        public string Potencia { get; set; }

        [Display(Name = "Periodo")]
        public string PeriodoDes { get; set; }
    }

    public class clsResumenSuministro
    {
        [Display(Name = "Nro. Suministro")]
        public string NroSuministro { get; set; }

        [Display(Name = "Deuda")]
        public string CantDeuda { get; set; }

        [Display(Name = "Nombre completo")]
        public string NombreUser { get; set; }

        [Display(Name = "Último Recibo")]
        public string CantUltRecibo { get; set; }

        [Display(Name = "Emitido")]
        public string FechEmitido { get; set; }

        [Display(Name = "Vencimiento")]
        public string FechVencimiento { get; set; }

        [Display(Name = "Tarifa")]
        public string CodTarifa { get; set; }

        public string Titulo { get; set; }

        public List<clsDiagramaBarras> lstDiagramaConsumo { get; set; }

        public string lstString { get; set; }
    }

    public class clsResultadoConsultaSuministro : clsResultadoOperacion
    {
        public string Configuracion { get; set; }
        public string DatoControl1 { get; set; }
        public string DatoControl2 { get; set; }
        public string Subtitulo1 { get; set; }
        public string Subtitulo2 { get; set; }
        public string Subtitulo3 { get; set; }
        public string Titulo { get; set; }
    }

    public class clsConsultaSuministro : clsResultadoOperacion
    {
        [Display(Name = "Número de Suministro")]
        [Required(ErrorMessage = "El número de suministro es requerido")]
        [StringLength(8, ErrorMessage = "El número de suministro debe tener un máximo de 8 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El número de suministro debe ser numérico")]
        public string Suministro { get; set; }

        [Display(Name = "Número de Recibo")]
        [Required(ErrorMessage = "El número de recibo es requerido")]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "El número de recibo debe tener un máximo de 4 caracteres")]
        [RegularExpression("([0-9]+)", ErrorMessage = "El número de recibo debe ser numérico")]
        public string Recibo { get; set; }

        [Display(Name = "Nombre Completo")]
        public string NombreCompleto { get; set; }

        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        [Display(Name = "E-Mail")]
        public string Correo { get; set; }

        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [Display(Name = "Distribuidora")]
        public string Proveedora { get; set; }
    }

    public class clsDetalleSuministro : clsConsultaSuministro
    {
        public string Periodo { get; set; }

        [Display(Name = "Periodo")]
        public string PeriodoDesc { get; set; }

        [Display(Name = "Importe")]
        [DisplayFormat(DataFormatString = "S/.{0:###,###.00}", ApplyFormatInEditMode = true)]
        public string ImporteRecibo { get; set; }

        [Display(Name = "Fecha Emisión")]
        public string FechaEmision { get; set; }

        [Display(Name = "Fecha Vencimiento")]
        public string FechaVencimiento { get; set; }

        [Display(Name = "Consumo")]
        [DisplayFormat(DataFormatString = "{0:###,###.00} KWh", ApplyFormatInEditMode = true)]
        public string CantConsumo { get; set; }

        [Display(Name = "Deuda")]
        [DisplayFormat(DataFormatString = "S/.{0:###,###.00}", ApplyFormatInEditMode = true)]
        public string ImporteDeuda { get; set; }

        [Display(Name = "Fecha último pago")]
        public string FechaUltimoPago { get; set; }

        public clsInformacion lstDiagramaConsumo { get; set; }

        public clsInformacion lstDiagramaFacturacion { get; set; }
    }

    public class clsLugaresPago : clsResultadoOperacion
    {
        public List<clsEstablecimiento> Centros { get; set; }
    }

    public class clsEstablecimiento
    {
        public string DireccionCentro { get; set; }
        public string IdCentro { get; set; }
        public string IdTipoCentro { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string NombreCentro { get; set; }
    }

    public class clsDetalleCentro : clsResultadoOperacion
    {
        public List<clsCentro> Detalle { get; set; }
        public List<string> Servicios { get; set; }
    }

    public class clsCentro
    {
        public string DireccionCentro { get; set; }
        public string Horario1 { get; set; }
        public string Horario2 { get; set; }
        public string NombreCentro { get; set; }
        public string Telefono { get; set; }
    }

    public class clsEmpresasXUsuario : clsResultadoOperacion
    {
        public List<clsEmpresa> Elementos { get; set; }
    }

    public class clsEmpresa
    {
        public string Direccion { get; set; }
        public string EMail { get; set; }
        public string Empresa { get; set; }
        public int IdEmpresa { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Tag { get; set; }
        public string Telefono { get; set; }
        public string UrlFb { get; set; }
        public string UrlTw { get; set; }
        public string UrlYb { get; set; }
    }
}