﻿namespace OficinaVirtual.Entidad.Consulta
{
    public class clsParametroConsulta
    {
        public string ControlTagCredencial { get; set; }
        public string ParametroControlTag { get; set; }
    }

    public class clsParametroEntrada
    {
        public object Parametros { get; set; }
    }

    public class clsParametroVisa
    {
        public double amount { get; set; }
        public string channel { get; set; }
        public object antifraud { get; set; }
    }

    public class clsParametroVisaAuthorizationOrden
    {
        public string amount { get; set; }
        public string currency { get; set; }
        public string purchaseNumber { get; set; }
        public string tokenId { get; set; }
    }

    public class clsParametroVisaAuthorization
    {
        public object antifraud { get; set; }
        public string captureType { get; set; }
        public string channel { get; set; }
        public string countable { get; set; }
        public clsParametroVisaAuthorizationOrden order { get; set; }
    }

    public class clsParametroVisaAuthorizationResponseHeader
    {
        public string ecoreTransactionUUID { get; set; }
        public string ecoreTransactionDate { get; set; }
        public string millis { get; set; }
    }

    public class clsParametroVisaAuthorizationResponseFulfillment
    {
        public string channel { get; set; }
        public string merchantdId { get; set; }
        public string terminalId { get; set; }
        public string captureType { get; set; }
        public string countable { get; set; }
        public string fastPayment { get; set; }
        public string signature { get; set; }
    }

    public class clsParametroVisaAuthorizationResponseOrder
    {
        public string tokenId { get; set; }
        public string purchaseNumber { get; set; }
        public string productId { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string authorizedAmount { get; set; }
        public string authorizationCode { get; set; }
        public string traceNumber { get; set; }
        public string transactionDate { get; set; }
        public string transactionId { get; set; }
    }

    public class clsParametroVisaAuthorizationResponseDataMap
    {
        public string CURRENCY { get; set; }
        public string TRANSACTION_DATE { get; set; }
        public string TERMINAL { get; set; }
        public string ACTION_CODE { get; set; }
        public string TRACE_NUMBER { get; set; }
        public string ECI_DESCRIPTION { get; set; }
        public string ECI { get; set; }
        public string CARD { get; set; }
        public string MERCHANT { get; set; }
        public string STATUS { get; set; }
        public string ADQUIRENTE { get; set; }
        public string ACTION_DESCRIPTION { get; set; }
        public string QUOTA_AMOUNT { get; set; }
        public string ID_UNICO { get; set; }
        public string AMOUNT { get; set; }
        public string PROCESS_CODE { get; set; }
        public string QUOTA_NUMBER { get; set; }
        public string VAULT_BLOCK { get; set; }
        public string RECURRENCE_STATUS { get; set; }
        public string TRANSACTION_ID { get; set; }
        public string AUTHORIZATION_CODE { get; set; }
        public string QUOTA_DEFERRED { get; set; }
    }

    public class clsParametroVisaAuthorizationResponseData
    {
        public string CURRENCY { get; set; }
        public string TRANSACTION_DATE { get; set; }
        public string TERMINAL { get; set; }
        public string ACTION_CODE { get; set; }
        public string TRACE_NUMBER { get; set; }
        public string ECI_DESCRIPTION { get; set; }
        public string ECI { get; set; }
        public string SIGNATURE { get; set; }
        public string CARD { get; set; }
        public string BRAND { get; set; }
        public string MERCHANT { get; set; }
        public string STATUS { get; set; }
        public string ADQUIRENTE { get; set; }
        public string ACTION_DESCRIPTION { get; set; }
        public string AMOUNT { get; set; }
        public string PROCESS_CODE { get; set; }
    }

    public class clsParametroVisaAuthorizationResponseMensaje
    {
        public string Mensaje { get; set; }
    }

    public class clsParametroVisaAuthorizationResponse : clsParametroVisaAuthorizationResponseMensaje
    {
        public clsParametroVisaAuthorizationResponseHeader header { get; set; }
        public clsParametroVisaAuthorizationResponseFulfillment fulfillment { get; set; }
        public clsParametroVisaAuthorizationResponseOrder order { get; set; }
        public clsParametroVisaAuthorizationResponseDataMap dataMap { get; set; }
    }

    public class clsParametroVisaAuthorizationResponseError
    {
        public string errorCode { get; set; }
        public string errorMessage { get; set; }
        public clsParametroVisaAuthorizationResponseHeader header { get; set; }
        public clsParametroVisaAuthorizationResponseData data { get; set; }
    }

    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
    #region Distriluz-036 rberrospi

    public class clsObtenerConfiguracionGlobalRequest
    {
        public clsObtenerConfiguracionGlobalParametros Parametros { get; set; }
    }

    public class clsObtenerConfiguracionGlobalParametros
    {
        public string Nombre { get; set; }
    }

    public class clsObtenerDetalleDeudaRequest
    {
        public clsObtenerDetalleDeudaParametros Parametros { get; set; }
    }

    public class clsObtenerDetalleDeudaParametros
    {
        public string IdNroServicio { get; set; }
    }


    public class clsObtenerOrdenCobroRequest
    {
        public clsObtenerOrdenCobroParametros Parametros { get; set; }
    }

    public class clsObtenerOrdenCobroParametros
    {
        public string IdOrdenCobro { get; set; }
    }

    #endregion

    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL

}