﻿using System.Collections.Generic;

namespace OficinaVirtual.Entidad.Consulta
{
    public class clsResultadoOperacion
    {
        public bool Exito { get; set; }
        public string Mensaje { get; set; }
        public string ControlTag { get; set; }
    }

    public class clsResultadoDatos
    {
        public string Direccion { get; set; }
        public string FechaEmision { get; set; }
        public string FechaVencimiento { get; set; }
        public int IdNroServicio { get; set; }
        public double ImporteMinimo { get; set; }
        public double ImporteTotal { get; set; }
        public string Nombres { get; set; }
        public short IdEmpresa { get; set; }
        public string Periodos { get; set; }
        public short FieldDeleteTrash { get; set; }
    }

    public class clsResultadoRecaudacion
    {
        public int IdAplicacion { get; set; }
        public int IdError { get; set; }
        public string Mensaje { get; set; }
        public string Token { get; set; }
        public int Tried { get; set; }
        public object Datos { get; set; }
    }

    public class clsResultadoListaRegistro
    {
        public string color_texto { get; set; }
        public string color_fondo { get; set; }
        public string tag { get; set; }
        public string negrita { get; set; }
    }

    public class clsResultadoMaestros
    {
        public int IdAplicacion { get; set; }
        public int IdError { get; set; }
        public string Mensaje { get; set; }
        public string Token { get; set; }
        public int Tried { get; set; }
        public bool Exito { get; set; }
    }

    public class clsResultadoDatosMaestros
    {
        public int Id { get; set; }
        public int IdEstado { get; set; }
        public string Nombre { get; set; }
    }

    public class clsResultadoListaMaestros : clsResultadoMaestros
    {
        public List<clsResultadoDatosMaestros> Datos { get; set; }
    }
}