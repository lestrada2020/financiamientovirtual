﻿using System.Collections.Generic;

namespace OficinaVirtual.Entidad.Consulta
{
    public class clsTipoOrientacion : clsResultadoOperacion
    {
        public int IdTipoOrientacion { get; set; }
        public string Nombre { get; set; }
    }

    public class clsListaOrientacion : clsResultadoOperacion
    {
        public List<clsTipoOrientacion> Elementos { get; set; }
        public string Comentario { get; set; }
    }

    public class clsListaRequisitos : clsResultadoOperacion
    {
        public List<string> Elementos { get; set; }
        public string Comentario { get; set; }
    }
}