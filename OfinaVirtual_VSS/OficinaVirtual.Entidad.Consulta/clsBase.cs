﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace OficinaVirtual.Entidad.Consulta
{
    #region clsBase

    public class clsBase
    {
        public String Token { get; set; }

        public Int32 Tried { get; set; }

        public Int16 IdAplicacion { get; set; }
    }

    #endregion clsBase

    #region clsParametro

    public class clsParametro : clsBase
    {
        public Dictionary<String, String> Parametros { get; set; }
    }

    public class clsParametroBulkData : clsBase
    {
        public Dictionary<String, String> Parametros { get; set; }
        public string DataParametro { get; set; }
        public DataTable Data { get; set; }
    }
    #endregion clsParametro

    #region clsParametro2

    public class clsParamFile : clsParametro
    {
        public HttpPostedFileBase FileArchivo { get; set; }

        public Byte[] GetByteFile()
        {
            try
            {
                var result = new Byte[FileArchivo.ContentLength];
                FileArchivo.InputStream.Read(result, 0, result.Length);
                FileArchivo = null;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String GetBase64File()
        {
            try
            {
                var result = Convert.ToBase64String(GetByteFile());

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    #endregion clsParametro2

    #region clsResultado

    public class clsResultado : clsBase
    {
        #region Properties

        public Int32 IdError { get; set; }

        public String Mensaje { get; set; }

        public Object Datos { get; set; }

        #endregion Properties

        #region Public

        public void SetError(Int32 idError, String message)
        {
            IdError = idError;
            Mensaje = message;
        }

        #endregion Public
    }

    #endregion clsResultado
}