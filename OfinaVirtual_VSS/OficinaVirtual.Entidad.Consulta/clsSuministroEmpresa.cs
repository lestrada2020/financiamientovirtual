﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Collections.Generic;

namespace OficinaVirtual.Entidad.Consulta
{
    public class clsSuministroEmpresaInformacion : clsResultadoOperacion
    {
        public string DeudaMinima { get; set; }
        public string DeudaTotal { get; set; }

    }


    public class clsSuministroEmpresaResumen
    {
        [Display(Name = "Deuda Minima")]
        public string DeudaMinima { get; set; }

        [Display(Name = "Deuda Total")]
        public string DeudaTotal { get; set; }

        [Display(Name = "Tarifa")]
        public string EnsaTarifa { get; set; }

        [Display(Name = "Numero Suministro")]
        public string EnsaNumeroSuministro { get; set; }

        [Display(Name = "Medio de Pago")]
        public string EnsaMedioPago { get; set; }

        [Display(Name = "Monto")]
        public string EnsaMonto { get; set; }



        [Display(Name = "Tarifa")]
        public string EnosaTarifa { get; set; }

        [Display(Name = "Numero Suministro")]
        public string EnosaNumeroSuministro { get; set; }

        [Display(Name = "Medio de Pago")]
        public string EnosaMedioPago { get; set; }

        [Display(Name = "Monto")]
        public string EnosaMonto { get; set; }


        [Display(Name = "Tarifa")]
        public string HidrandinaTarifa { get; set; }

        [Display(Name = "Numero Suministro")]
        public string HidrandinaNumeroSuministro { get; set; }

        [Display(Name = "Medio de Pago")]
        public string HidrandinaMedioPago { get; set; }

        [Display(Name = "Monto")]
        public string HidrandinaMonto { get; set; }


        [Display(Name = "Tarifa")]
        public string ElectrocentroTarifa { get; set; }

        [Display(Name = "Numero Suministro")]
        public string ElectrocentroNumeroSuministro { get; set; }

        [Display(Name = "Medio de Pago")]
        public string ElectrocentroMedioPago { get; set; }

        [Display(Name = "Monto")]
        public string ElectrocentroMonto { get; set; }

    }


    public class clsDepositoBancario
    {
        [Required(ErrorMessage = "El banco es requerido")]
        [Display(Name = "Banco")]
        public string Banco { get; set; }

        [Required(ErrorMessage = "El numero de cuenta es requerido")]
        [Display(Name = "Cuenta")]
        public string Cuenta { get; set; }

        [Required(ErrorMessage = "El número de voucher es requerido")]
        [Display(Name = "Nro° de Voucher")]
        public string Voucher { get; set; }

        [Required(ErrorMessage = "El monto es requerido")]
        [Display(Name = "Ingresar monto a pagar")]
        public string Monto { get; set; }
        public string Empresa { get; set; }
        public string CuentaSeleccionada { get; set; }
        public string MontoInicial { get; set; }
        public string NroPedido { get; set; }
    }

}
