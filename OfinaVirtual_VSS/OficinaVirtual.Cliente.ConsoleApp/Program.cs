﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace OficinaVirtual.Cliente.ConsoleApp
{
    internal class Program
    {
        #region Extra

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;
        private const int SW_MINIMIZE = 6;

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        #endregion Extra

        #region Field

        private static readonly clsVisa _Visa = clsVisa.Instancia;
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsAtencion _Atencion = clsAtencion.Instancia;
        private static readonly clsConsulta _Consulta = clsConsulta.Instancia;
        private static readonly clsRecaudacion _Recaudacion = clsRecaudacion.Instancia;

        #endregion Field

        #region Private

        /// <summary>
        /// Regulariza todos los pagos VISA.
        /// </summary>
        /// <param name="args">"2" para Procesar lista pendientes. "1" para procesar solo uno.</param>
        private static void Main(string[] args)
        {
            try
            {
                //args = new string[] { "1", "1", "1", "160549" };
                //args = new string[] { "2" };

                Execute(args).Wait();
            }
            catch (Exception ex)
            {
                ShowError(ex, args);
            }
        }

        private static async Task ShowError(Exception exception, String[] args)
        {
            try
            {
                ShowWindow(GetConsoleWindow(), SW_SHOW);
                Console.SetWindowSize(Console.WindowWidth * 16, Console.WindowHeight * 16);
                Console.Error.WriteLine(exception.Message);
                Console.WriteLine(String.Format("Parametros enviados: \"{0}\"", String.Join("\" \"", args)));
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("VisaNet Log", exception.Message, EventLogEntryType.Error);
            }
        }

        private static async Task Execute(String[] args)
        {
            try
            {
                ServicePointManager.Expect100Continue = false;
                ServicePointManager.DefaultConnectionLimit = Int32.MaxValue;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                if (args[0] == "1") await VisaVerifyOrder(args);
                else if (args[0] == "2")
                {
                    ShowWindow(GetConsoleWindow(), SW_HIDE);
                    Console.SetWindowSize(Console.WindowWidth / 16, Console.WindowHeight / 16);
                    await VisaPayFailed();
                }
            }
            catch (Exception ex)
            {
                ShowError(ex, args);
            }
        }

        private static async Task VisaVerifyOrder(String[] args)
        {
            try
            {
                var idEmpresa = Convert.ToInt16(args[1]);
                var type = Convert.ToInt16(args[2]);
                var idOrder = Convert.ToInt32(args[3]);

                var token = await _Visa.CreateToken(1, type);
                var data = await VisaVerifyStatus(idEmpresa, type, token, idOrder);

                Console.WriteLine();
                Console.WriteLine(_Convert.ToJson(data["Message"]));
                Console.WriteLine();
                Console.WriteLine($"{idOrder} ==> {data["IdStatus"]} ==> {data["MessageVisa"]}");
                Console.WriteLine($"FIN");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine($"ERROR");
            }
        }

        private static async Task VisaPayFailed()
        {
            try
            {
                var listOrders = await VisaListFailed();
                var tokenVisaAPP = await _Visa.CreateToken(1, 1);
                var tokenVisaWEB = await _Visa.CreateToken(1, 2);
                var tokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];

                foreach (var item in listOrders)
                {
                    try
                    {
                        var idOrder = Convert.ToInt32(item["IdNumeroOrden"]);
                        var idEmpresa = Convert.ToInt16(item["IdEmpresa"]);
                        var isExpire = Convert.ToBoolean(item["IsExpire"]);
                        var idType = Convert.ToInt16(item["IdTipo"]);
                        var tokenVisa = idType == 1 ? tokenVisaAPP : tokenVisaWEB;

                        var rsVerifyStatus = await VisaVerifyStatus(idEmpresa, idType, tokenVisa, idOrder);

                        dynamic param = new ExpandoObject();
                        param.NroPedido = idOrder;
                        param.SessionToken = tokenDistriluz;
                        param.SessionTokenVisa = "";
                        param.TransactionTokenVisa = tokenVisa.Substring(32);
                        param.JsonPagoResumen = "";
                        param.Email = "";
                        param.IdPaso = 4;
                        param.IdEstadoPaso = Convert.ToInt16(rsVerifyStatus["IdStatus"]);
                        param.MensajePaso = rsVerifyStatus["MessageVisa"];
                        param.JsonPagoVisa = rsVerifyStatus["Message"];

                        var dateVisa = rsVerifyStatus["DateVisa"] == "" ? DateTime.Now.AddHours(-1) : DateTime.ParseExact(rsVerifyStatus["DateVisa"], "yyMMddHHmmss", CultureInfo.InvariantCulture);
                        var diffDateVisa = DateTime.Now - dateVisa;

                        if (param.IdEstadoPaso == 0 && !isExpire) continue;
                        if (diffDateVisa.TotalSeconds < 50) continue;

                        var rqPayIntent = new clsParametroEntrada { Parametros = param };
                        await _Recaudacion.IntentoPagoActualizar(rqPayIntent);

                        if (param.IdEstadoPaso == 2)
                        {
                            var pay = await _Recaudacion.SuministroPagarDeuda(rqPayIntent);

                            if (pay.IdError == 0) await _Recaudacion.SuministroEnviarCorreoPago(rqPayIntent);
                            else await VisaSendMailPayFailed(idOrder);
                        }

                        //Console.WriteLine($"{idOrder} ==> {param.IdEstadoPaso} ===> {param.MensajePaso}  ===> {param.JsonPagoVisa}");
                        //Console.WriteLine($"");
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine($"{item["IdNumeroOrden"]} ==> {ex.Message}");
                    }
                }

                //Console.WriteLine($"FIN");
                //Console.ReadKey();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<List<Dictionary<String, String>>> Empresas()
        {
            try
            {
                var param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros.Add("Entity", "Empresa");
                param.Parametros.Add("idorganizacion", "1");
                param.Parametros.Add("AddPreFix", "0");

                var call = await _Consulta.AyudaLista(param);

                if (call.IdError > 0) throw new Exception(call.Mensaje);

                var result = _Convert.ToObject<List<Dictionary<String, String>>>(call.Datos.ToString());

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<List<Dictionary<String, String>>> VisaListFailed()
        {
            try
            {
                var param = new clsParametro();
                var call = await _Atencion.VisaListFailed(param);

                if (call.IdError > 0) throw new Exception(call.Mensaje);

                var result = _Convert.ToObject<List<Dictionary<String, String>>>(call.Datos.ToString());

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<clsResultado> VisaSendMailPayFailed(Int32 idOrder)
        {
            try
            {
                var param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros.Add("IdNumeroOrden", idOrder.ToString());

                var result = await _Atencion.VisaSendMailPayFailed(param);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task<Dictionary<String, String>> VisaVerifyStatus(Int16 IdEmpresa, Int16 idType, String token, Int32 idOrder)
        {
            try
            {
                var rsPayVerify = await _Visa.VerifyOrder(IdEmpresa, idType, token, idOrder);

                var result = new Dictionary<String, String>();
                result["IdStatus"] = "";
                result["Message"] = "";
                result["MessageVisa"] = "";
                result["DateVisa"] = "";

                if (rsPayVerify.IdError == 400)
                {
                    result["IdStatus"] = "0";
                    result["Message"] = rsPayVerify.Mensaje;
                    result["MessageVisa"] = "Transacción Fallida";
                }
                else if (rsPayVerify.IdError == 0)
                {
                    result["IdStatus"] = "1";
                    result["Message"] = rsPayVerify.Datos.ToString();

                    var dataVisa = _Convert.ToObject<Dictionary<String, Object>>(result["Message"]);
                    var dataMap = _Convert.ToObject<Dictionary<String, String>>(dataVisa["dataMap"].ToString());

                    var actionCode = Convert.ToInt32(dataMap["ACTION_CODE"]);
                    var actionDescription = dataMap.ContainsKey("ACTION_DESCRIPTION") ? dataMap["ACTION_DESCRIPTION"] : "Trama sin descripción";
                    var transactionDate = dataMap.ContainsKey("TRANSACTION_DATE") ? dataMap["TRANSACTION_DATE"] : "";

                    result["MessageVisa"] = $"[{actionCode}] - {actionDescription}";
                    result["DateVisa"] = transactionDate;

                    if (actionCode == 0)
                    {
                        result["IdStatus"] = "2";
                        result["MessageVisa"] = "Transacción correcta en VISA";
                    }
                }
                else
                {
                    throw new Exception(rsPayVerify.Mensaje);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Private
    }
}