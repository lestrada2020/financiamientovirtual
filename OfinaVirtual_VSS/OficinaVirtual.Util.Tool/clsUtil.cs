﻿using Ionic.Zip;
using Microsoft.Win32;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace OficinaVirtual.Util.Tool
{
    public sealed class clsUtil
    {
        #region Field

        private static String _MessageError = String.Empty;
        private static readonly Lazy<clsUtil> _Instancia = new Lazy<clsUtil>(() => new clsUtil(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsUtil Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsUtil()
        {
        }

        #endregion Constructor

        #region Public

        public T IsDBNullParse<T>(T valor)
        {
            if ((Object)valor == DBNull.Value)
                return (T)((Object)null);
            else
                return valor;
        }

        public void InvokeAsync(Action<Object> methodo, Object parametro)
        {
            try
            {
                ParameterizedThreadStart parameterized = new ParameterizedThreadStart(methodo);
                Thread thread = new Thread(parameterized);
                thread.IsBackground = true;
                thread.Start(parametro);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String ErrorFormat(Exception exception)
        {
            try
            {
                if (exception == null) return "Exception is null...";

                StringBuilder result = new StringBuilder();

                if (exception is SqlException)
                {
                    #region SqlException

                    SqlException sqlException = (SqlException)exception;

                    if (sqlException.Errors == null
                        || sqlException.Errors.Count == 0)
                        return "sqlException.Errors is empty...";

                    foreach (SqlError error in sqlException.Errors)
                    {
                        result.AppendLine("Mensaje: " + error.Message);
                        result.AppendLine("Numero de Error: " + error.Number);
                        result.AppendLine("Linea de Error: " + error.LineNumber);
                        result.AppendLine("Procedimiento: " + error.Procedure);
                        result.AppendLine("Server: " + error.Server);
                        result.AppendLine("Origen: " + error.Source);
                        result.AppendLine("==============================");
                    }

                    #endregion SqlException
                }
                else
                {
                    #region Default

                    if (!String.IsNullOrEmpty(exception.Message))
                        result.AppendLine("Mensaje: " + exception.Message.Trim());

                    if (!String.IsNullOrEmpty(exception.StackTrace))
                        result.AppendLine("Llamada: " + exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        if (!String.IsNullOrEmpty(exception.InnerException.Message))
                            result.AppendLine("Detalle: " + exception.InnerException.Message.Trim());

                        if (!String.IsNullOrEmpty(exception.InnerException.StackTrace))
                            result.AppendLine("Llamada: " + exception.InnerException.StackTrace.Trim());
                    }

                    #endregion Default
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                return exception.Message;
            }
        }

        public String MessageError(Int32 idLog)
        {
            try
            {
                String result = String.Empty;

                if (String.IsNullOrEmpty(_MessageError))
                {
                    _MessageError = ConfigurationManager.AppSettings["MensajeError"] ?? String.Empty;

                    if (String.IsNullOrEmpty(_MessageError)) _MessageError = "Ha ocurrido un Error en el Proceso. Favor consulte con su Administrador Informático.";
                }

                if (idLog > 0) result = String.Format("{0}[IdLog: ({1})]", _MessageError, idLog);
                else result = String.Format("{0}", _MessageError);

                return result;
            }
            catch (Exception ex)
            {
                return "Ha ocurrido un Error en el Proceso.";
            }
        }

        public Boolean SaveEvent(String message)
        {
            try
            {
                System.Diagnostics.EventLog.WriteEntry("Servicio Tarea NGC", message, System.Diagnostics.EventLogEntryType.Error);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void GetTraceCall(ref String method, ref String service, ref String host, Boolean showError = true)
        {
            try
            {
                String trace = GetTrace();

                method = trace;
                service = "";
                host = Environment.MachineName;// System.Net.Dns.GetHostName();

                if (method.Contains("."))
                {
                    method = trace.Substring(trace.LastIndexOf(".") + 1);
                    service = trace.Substring(0, trace.LastIndexOf("."));
                }
            }
            catch (Exception ex)
            {
                if (showError) throw ex;
            }
        }

        public String GetTrace()
        {
            String traceFull = String.Empty;

            return GetTrace(out traceFull);
        }

        public String GetTrace(out String traceFull)
        {
            try
            {
                String result = String.Empty;
                Int32 skipFrames = 0;

                traceFull = GetTraceFull();
                String[] traceSplit = traceFull.Split(new String[] { "\r\n" }, StringSplitOptions.None);

                for (int i = 0; i < traceSplit.Length; i++)
                {
                    String trace = traceSplit[i].Trim();

                    if (!trace.Contains("."))
                    {
                        skipFrames = i >= 1 ? i - 1 : 0;
                        break;
                    }
                    else if (trace.Contains("System."))
                    {
                        skipFrames = i >= 2 ? i - 2 : 0;
                        break;
                    }
                }

                result = traceSplit[skipFrames].Trim();

                if (result.StartsWith("en ")
                    || result.StartsWith("at "))
                    result = result.Substring(3);

                return result;
            }
            catch (Exception)
            {
                traceFull = String.Empty;
                return String.Empty;
            }
        }

        public String GetTraceFull()
        {
            try
            {
                String result = String.Empty;

                StackTrace stackTrace = new StackTrace();//stackTrace.GetFrame(0).GetMethod().Name;
                result = stackTrace.ToString();

                return result;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public String GetPropertyName<T>(T item) where T : class
        {
            try
            {
                if (item == null) return String.Empty;

                return typeof(T).GetProperties()[0].Name;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String GetContentType(String fileName)
        {
            try
            {
                String result = "application/unknown";//MediaTypeNames.Application.Octet

                if (String.IsNullOrEmpty(fileName)) return result;

                String contentType = String.Empty;
                RegistryKey reg = Registry.ClassesRoot.OpenSubKey(Path.GetExtension(fileName).ToLower());

                if (reg != null) contentType = (String)reg.GetValue("Content Type");
                if (!String.IsNullOrEmpty(contentType)) result = contentType;

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public T SetError<T>(String message, Int32 idError = 1)
        {
            try
            {
                T result;

                Type type = typeof(T);
                MethodInfo methodInfo = type.GetMethod("SetError", new Type[] { typeof(Int32), typeof(String) });

                result = (T)Activator.CreateInstance(type);

                if (methodInfo != null) methodInfo.Invoke(result, new Object[] { idError, message });

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T SetError<T>(Exception exception, Int32 idError = 1)
        {
            try
            {
                String mensaje = ErrorFormat(exception);

                return SetError<T>(mensaje, idError);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String CompressFile(String url, String fileName)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(url, "");
                    zip.Save(fileName);
                }

                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Public
    }
}