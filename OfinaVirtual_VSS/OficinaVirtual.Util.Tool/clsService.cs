﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Util.Tool
{
    public sealed class clsService
    {
        #region Field

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly Lazy<HttpClient> _HttpClient = new Lazy<HttpClient>(LazyHttpClient, LazyThreadSafetyMode.PublicationOnly);
        private static readonly Lazy<clsService> _Instancia = new Lazy<clsService>(() => new clsService(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsService Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsService()
        {
        }

        #endregion Constructor

        #region Private
        private static HttpClient GetClient(String sURL)
        {
            return GetClient(sURL, string.Empty, string.Empty);
        }

        private static HttpClient GetClient(String sURL, String sUsuario, String sClave)
        {
            return GetClient(sURL, sUsuario, sClave, string.Empty);
        }

        private static HttpClient GetClient(String sURL, String sUsuario, String sClave, String sToken)
        {
            try
            {
                var uri = new Uri(sURL);
                var auth = (AuthenticationHeaderValue)null;

                if (sUsuario != String.Empty && sClave != String.Empty) auth = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{sUsuario}:{sClave}")));
                else if (sToken != String.Empty) auth = new AuthenticationHeaderValue(sToken);

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                ServicePointManager.DefaultConnectionLimit = 9999;

                var client = new HttpClient()
                {
                    BaseAddress = uri,
                    DefaultRequestHeaders = { Authorization = auth }
                };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                return client;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private T SetError<T>(String message)
        {
            return SetError<T>(message, 1);
        }

        private T SetError<T>(String message, Int32 id)
        {
            try
            {
                Type type = typeof(T);

                T result = (T)Activator.CreateInstance(type);

                PropertyInfo p1 = type.GetProperty("Exito");
                PropertyInfo p2 = type.GetProperty("Mensaje");
                PropertyInfo p3 = type.GetProperty("IdError");

                if (p1 != null) p1.SetValue(result, false);
                if (p2 != null) p2.SetValue(result, message);
                if (p3 != null) p3.SetValue(result, id);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static HttpClient LazyHttpClient()
        {
            var result = new HttpClient()
            {
                Timeout = TimeSpan.FromMinutes(30)
            };

            return result;
        }

        private HttpContent CreateHttpContent(Object data)
        {
            try
            {
                var json = "{}";

                if (data != null)
                {
                    if (data is String) json = data.ToString();
                    else json = _Convert.ToJson(data);
                }

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                return content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private HttpRequestMessage CreateHttpRequestMessage(String url, HttpContent content, String token, HttpMethod httpMethod)
        {
            try
            {
                var request = new HttpRequestMessage()
                {
                    Method = httpMethod,
                    RequestUri = new Uri(url),
                };

                if (httpMethod != HttpMethod.Get) request.Content = content;
                if (token != null) request.Headers.Add("Authorization", token);

                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                return request;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private T CustomeHttpStatusCode<T>(String read, HttpStatusCode statusCode)
        {
            try
            {
                var message = "Favor de reintentar.";

                switch (statusCode)
                {
                    case HttpStatusCode.BadRequest:
                        if (String.IsNullOrWhiteSpace(read)) message = "Servidor no pudo atender la solicitud";
                        else message = read;
                        break;

                    case HttpStatusCode.NotFound:
                        message = "Servidor no encontrado, favor revise su conexion a internet.";
                        break;

                    case HttpStatusCode.Forbidden:
                    case HttpStatusCode.RequestTimeout:
                    case HttpStatusCode.ServiceUnavailable:
                        message = "Tenemos muchas solicitudes en este momento, favor de reintentar luego.";
                        break;

                    case HttpStatusCode.InternalServerError:
                        message = "Ocurrio un error en el servidor, favor de reintentar.";
                        break;

                    default:
                        break;
                }

                return SetError<T>(message, (Int32)statusCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Private

        #region Public

        public async Task<T> GetAsync<T>(String url, Object data, String token = null, Boolean forcedDatos = false)
        {
            return await SendAsync<T>(url, data, HttpMethod.Get, token, forcedDatos).ConfigureAwait(false);
        }

        public async Task<T> PostAsync<T>(String url, Object data, String token = null, Boolean forcedDatos = false)
        {
            return await SendAsync<T>(url, data, HttpMethod.Post, token, forcedDatos).ConfigureAwait(false);
        }

        public async Task<T> SendAsync<T>(String url, Object data, HttpMethod httpMethod, String token = null, Boolean forcedDatos = false)
        {
            try
            {
                var result = default(T);

                using (var content = CreateHttpContent(data))
                {
                    using (var request = CreateHttpRequestMessage(url, content, token, httpMethod))
                    {
                        using (var response = await _HttpClient.Value.SendAsync(request).ConfigureAwait(false))
                        {
                            var read = await response.Content.ReadAsStringAsync();

                            if (response.IsSuccessStatusCode)
                            {
                                if (typeof(T) == typeof(String)) result = (T)(Object)read;
                                else
                                {
                                    if (forcedDatos) read = _Convert.ToJson(new { Datos = read });

                                    result = _Convert.ToObject<T>(read);
                                }
                            }
                            else result = CustomeHttpStatusCode<T>(read, response.StatusCode);
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return SetError<T>(ex.Message);
            }
        }

        public async Task<T> SendData<T>(String URL)
        {
            return await SendData<T>(URL, null);
        }

        public async Task<T> SendData<T>(String URL, Object data)
        {
            return await SendData<T>(URL, data, string.Empty, string.Empty);
        }

        public async Task<T> SendData<T>(String URL, Object data, String usuario, String clave)
        {
            return await SendData<T>(URL, data, usuario, clave, string.Empty);
        }

        public async Task<T> SendData<T>(String URL, Object data, String usuario, String clave, String token)
        {
            return await SendData<T>(URL, data, usuario, clave, token, "Error al recibir el servicio");
        }

        public async Task<T> SendData<T>(String URL, Object data, String usuario, String clave, String token, String message)
        {
            return await SendData<T>(URL, data, usuario, clave, token, message, true);
        }

        public async Task<T> SendData<T>(String URL, Object data, String usuario, String clave, String token, String message, Boolean convertData)
        {
            return await SendData<T>(URL, data, usuario, clave, token, message, convertData, false);
        }

        public async Task<T> SendData<T>(String URL, Object data, String usuario, String clave, String token, String message, Boolean convertData, Boolean forcedDatos)
        {
            return await SendData<T>(URL, data, usuario, clave, token, message, convertData, forcedDatos, "POST");
        }

        public async Task<T> SendData<T>(String URL, Object data, String usuario, String clave, String token, String message, Boolean convertData, Boolean forcedDatos, String method)
        {
            try
            {
                T result = default(T);

                using (HttpClient client = GetClient(URL, usuario, clave, token))
                {
                    HttpContent context = null;

                    if (data != null)
                    {
                        var json = "{}";

                        if (convertData) json = JsonConvert.SerializeObject(data);
                        else json = data.ToString();

                        context = new StringContent(json);
                        context.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    }

                    try
                    {
                        HttpResponseMessage response = null;

                        if (method == "GET") response = await client.GetAsync(client.BaseAddress).ConfigureAwait(false);
                        else response = await client.PostAsync(client.BaseAddress, context).ConfigureAwait(false);

                        var content = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {
                            if (typeof(T) == typeof(String)) result = (T)(Object)content;
                            else
                            {
                                if (forcedDatos) content = JsonConvert.SerializeObject(new { Datos = content });
                                result = JsonConvert.DeserializeObject<T>(content);
                            }
                        }
                        else
                        {
                            result = SetError<T>(String.IsNullOrWhiteSpace(content) ? message : content, (Int32)response.StatusCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        result = SetError<T>(ex.Message);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return SetError<T>(ex.Message);
            }
        }

        #endregion Public
    }
}