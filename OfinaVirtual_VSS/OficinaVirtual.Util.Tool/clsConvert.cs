﻿using Newtonsoft.Json;
using SpreadsheetGear;
using SpreadsheetGear.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace OficinaVirtual.Util.Tool
{
    public sealed class clsConvert
    {
        #region Field

        private static readonly Lazy<clsConvert> _Instancia = new Lazy<clsConvert>(() => new clsConvert(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsConvert Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsConvert()
        {
        }

        #endregion Constructor

        #region JsonSerialize

        public String ToJson(Object data)
        {
            try
            {
                var result = JsonConvert.SerializeObject(data);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String ToJson(Stream data)
        {
            try
            {
                String result = String.Empty;

                if (data == null) return result;

                using (StreamReader sr = new StreamReader(data, Encoding.UTF8))
                {
                    result = sr.ReadToEnd();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Byte[] ToJsonOfBytes(Object data)
        {
            try
            {
                Byte[] result = new Byte[0];

                if (data == null) return result;

                JsonSerializer serializer = new JsonSerializer();

                using (MemoryStream ms = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(ms, Encoding.UTF8))
                    {
                        using (JsonWriter writer = new JsonTextWriter(sw))
                        {
                            //writer.Formatting = Formatting.Indented;
                            serializer.Serialize(writer, data);
                            sw.Flush();
                            result = ms.ToArray();
                        }
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public MemoryStream ToJsonOfStream(Object data)
        {
            try
            {
                if (data == null) return new MemoryStream();

                return new MemoryStream(ToJsonOfBytes(data));
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion JsonSerialize

        #region JsonDeserialize

        public T ToObject<T>(String data)
        {
            try
            {
                T result;

                if (String.IsNullOrEmpty(data)) return default(T);

                result = (T)JsonConvert.DeserializeObject(data, typeof(T));

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public T ToObject<T>(Byte[] data)
        {
            try
            {
                T result;

                if (data == null
                    || data.Length == 0)
                    return default(T);

                JsonSerializer serializer = new JsonSerializer();

                using (MemoryStream ms = new MemoryStream(data))
                {
                    result = ToObject<T>(ms);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public T ToObject<T>(Stream data)
        {
            try
            {
                T result;

                if (data == null) return default(T);

                JsonSerializer serializer = new JsonSerializer();

                using (StreamReader sr = new StreamReader(data, Encoding.UTF8))
                {
                    using (JsonReader reader = new JsonTextReader(sr))
                    {
                        result = (T)serializer.Deserialize(reader, typeof(T));
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion JsonDeserialize

        #region Dictionary

        public Dictionary<String, Object> ToDictionary(DataRow row)
        {
            try
            {
                if (row == null) return null;

                Dictionary<String, Object> result = new Dictionary<String, Object>();

                foreach (DataColumn col in row.Table.Columns)
                {
                    result.Add(col.ColumnName, row[col.ColumnName]);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Dictionary<String, Object>> ToDictionary(DataTable table)
        {
            try
            {
                List<Dictionary<String, Object>> result = new List<Dictionary<String, Object>>();

                if (table == null) return result;

                foreach (DataRow row in table.Rows)
                {
                    result.Add(ToDictionary(row));
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Dictionary

        #region Encrypt

        public String ToMD5(String message)
        {
            try
            {
                String result;

                using (MD5 md5 = MD5.Create())
                {
                    Byte[] bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(message));

                    result = String.Join("", bytes.Select(s => s.ToString("X2")).ToArray());
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Encrypt

        #region Excel

        public DataSet ToDataSet(Byte[] bytesExcel)
        {
            try
            {
                IWorkbookSet workbookSet = Factory.GetWorkbookSet();
                IWorkbook workbook = workbookSet.Workbooks.OpenFromMemory(bytesExcel);
                DataSet dataSet = workbook.GetDataSet(GetDataFlags.None);

                return dataSet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ToDataTable(Byte[] bytesExcel, String sheetName)
        {
            try
            {
                DataSet dataSet = ToDataSet(bytesExcel);
                DataTable table = dataSet.Tables[sheetName];

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable ToDataTable(Byte[] bytesExcel)
        {
            try
            {
                DataSet dataSet = ToDataSet(bytesExcel);
                DataTable table = dataSet.Tables[0];

                return table;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Excel
    }
}