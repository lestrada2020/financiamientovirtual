﻿using System.Configuration;

namespace OficinaVirtual.Util.Tool
{
    public class clsDatosConstantes
    {
        public sealed class EsquemasBd
        {
            /// <summary>
            /// Esquema de pagos
            /// </summary>
            public static readonly string Pago = "PGO";

            /// <summary>
            /// Esquema de publicacion
            /// </summary>
            public static readonly string Pub = "MOV";

            /// <summary>
            /// Esquema de Movimientos
            /// </summary>
            public static readonly string Mov = "MOV";

            /// <summary>
            /// Esquema de Parametros
            /// </summary>
            public static readonly string Par = "MAN";
            /// <summary>
            /// Esquema de Mantenimiento
            /// </summary>
            public static readonly string Man = "MAN";
        }
        public sealed class EstadoRegistro
        {
            /// <summary>
            /// Estado de Registro
            /// </summary>
            public static readonly string Activo = "1";

            /// <summary>
            /// Estado Inactivo
            /// </summary>
            public static readonly string Inactivo = "0";

            /// <summary>
            /// Activo
            /// </summary>
            public static readonly string EstadoActivo = "ACTIVO";

            /// <summary>
            /// Inactivo
            /// </summary>
            public static readonly string EstadoInactivo = "INACTIVO";
        }
        public sealed class Permisos
        {
            public static class Codigo
            {
                public const int Lectura = 1;
                public const int Escritura = 2;
                public const int ControlTotal = 3;
            }

            public static class Style
            {
                public const string Activo = "pointer-events:auto;opacity:1;";
                public const string Desactivo = "pointer-events:none;opacity:0.6;";
                public const string Disable = "pointer-events: none;cursor: default;";
            }
        }

        public static class Servicio
        {
            public static class Resultado
            {
                public const string Success = "Ok";
            }
        }

        public static class Formato
        {
            public static class Fecha
            {
                public const string FormatDate_Slash_MMyyyy = "MM/yyyy";
                public const string FormatDate_Slash_ddMMyyyy = "dd/MM/yyyy";
                public const string FormatDate_Slash_ddMMyyyy_HHmm = "dd/MM/yyyy HH:mm";
                public const string FormatDate_Guion_MMyyyy = "MM-yyyy";
                public const string FormatDate_Guion_ddMMyyyy = "dd-MM-yyyy";
                public const string FormatDate_Guion_ddMMyyyy_HHmm = "dd-MM-yyyy HH:mm";
            }

            public static class Decimal
            {
                public const string FormatoDosDecimales = "#0,#.00";
                public const string FormatoMoney = "#,###,##0.00";
                public const string StringFormatToNumber = "{0:#0,#.00}";
            }

            public static class Archivos
            {
                public const string Excel_xls = ".xls";
                public const string Excel_xlsx = ".xlsx";
                public const string Word_doc = ".doc";
                public const string Word_docx = ".docx";
                public const string PDF = ".pdf";
            }
        }

        public static class Datos
        {
            public const string Activo = "Activo";
            public const string Desactivo = "Desactivo";
        }

        public static class Controles
        {
            public static class Combo
            {
                public const string Seleccione = "Seleccione";
                public const string Todos = "Todos";
                public const string Otros = "Otros";
            }

            public static class Paginacion
            {
                public static class Descripcion
                {
                    public const string Primero = "Primero";
                    public const string Anterior = "Anterior";
                    public const string Siguieunte = "Siguiente";
                    public const string Ultimo = "Ultimo";
                }

                public static class Orientacion
                {
                    public const string Ascendente = "Ascending";
                    public const string Descendente = "Descending";
                }

                public static class FilasPorPagina
                {
                    public const int Normal = 10;
                    public const int Muchos = 20;
                    public const int Detalle = 5;
                    public const int Todos = 0;
                    public const int PaginaDefecto = 1;
                    public const int Maximo = 5000;
                }
            }

            public static class Modal
            {
                public const string Default = "divModalBody";

                public static class Size
                {
                    public const string SizeWide = "SIZE_WIDE";
                    public const string SizeSmall = "SIZE_SMALL";
                    public const string SizeNormal = "SIZE_NORMAL";
                }

                public static class Type
                {
                    public const string Defecto = "BootstrapDialog.TYPE_DEFAULT";
                    public const string Informativo = "BootstrapDialog.TYPE_INFO";
                    public const string Primario = "BootstrapDialog.TYPE_PRIMARY";
                    public const string Correcto = "BootstrapDialog.TYPE_SUCCESS";
                    public const string Precaucion = "BootstrapDialog.TYPE_WARNING";
                    public const string Peligro = "BootstrapDialog.TYPE_DANGER";
                }
            }

            public static class Alerta
            {
                public static class Tipo
                {
                    public const string Warning = "W";
                    public const string Error = "E";
                    public const string Success = "S";
                    public const string Info = "I";
                    public const string SessionExpired = "SE";
                }

                public static class TipoString
                {
                    public const string Warning = "warning";
                    public const string Error = "error";
                    public const string Success = "success";
                    public const string Info = "info";
                }

                public static class Glificon
                {
                    public const string Warning = "<span class=\"glyphicon glyphicon-remove\"></span>";
                    public const string Error = "<span class=\"glyphicon glyphicon-ok\"></span>";
                    public const string Succes = "<span class=\"glyphicon glyphicon-ok\"></span>";
                    public const string Info = "<span class=\"glyphicon glyphicon-ok\"></span>";
                }

                public static class Icon
                {
                    public const string Warning = "glyphicon glyphicon-warning-sign";
                    public const string Error = "glyphicon glyphicon-warning-sign";
                    public const string Success = "glyphicon glyphicon-exclamation-sign";
                    public const string Info = "glyphicon glyphicon-question-sign";
                }

                public static class From
                {
                    public const string Superior = "top";
                    public const string Inferior = "bottom ";
                }

                public static class Align
                {
                    public const string Izquierda = "left";
                    public const string Centro = "center";
                    public const string Derecha = "right";
                }

                public static class Texto
                {
                    public const string MensajeDefecto = "Ocurrió un error al realizar la operación";
                    public const string TituloDefecto = "Mensaje del sistema";
                }
            }
        }

        public static class Sesion
        {
        }

        public static class Controladores
        {
        }

        public sealed class Format
        {
            public const string DateDefault = "dd/MM/yyyy";
            public const string DateLarge = "dd/MM/yyyy : HH:mm:ss";
            public const string MoneyDefault = "###,###,##0.00";
        }

        public sealed class Response
        {
            public const string Success = "S";
            public const string Exception = "E";
            public const string Error = "R";
            public const string Warning = "W";
            public const string SessionExpired = "SE";
            public const string Yes = "Y";
            public const string No = "N";
            public const string FuncionAceptar = "F";
            public const string Confirmacion = "C";
        }

        public sealed class TypeNotify
        {
            public const string Danger = "danger";
            public const string Success = "success";
        }

        public sealed class Action
        {
            public const string New = "N";
            public const string Edit = "E";
            public const string Delete = "D";
            public const string Active = "A";
            public const string View = "V";
            //=================================
            public const string Massive = "M";
            public const string Reprogramming = "R";
            public const string Temporal = "T";
        }

        public sealed class ActionRequerimiento
        {
            public const string Confirmar = "C";
            public const string Atencion = "A";
            public const string Visualizar = "V";
        }

        public sealed class GlificonMessage
        {
            public const string Error = "<span class=\"glyphicon glyphicon-remove\"></span>";
            public const string Success = "<span class=\"glyphicon glyphicon-ok\"></span>";
        }

        public sealed class Estado
        {
            public const string Activo = "Activo";
            public const string Inactivo = "Inactivo";
        }

        public sealed class ComboDefault
        {
            public const string Ninguno = "Ninguno";
            public const string Todos = "Todos";
            public const string Seleccione = "Seleccione";
            public const string Vacio = "";
        }
        public sealed class TipoUsuario
        {
            public const string Interno = "1";
            public const string Externo = "2";
        }
        public sealed class TipoRegistro
        {
            public const int Ejecucion = 1;
            public const int LineaBase = 2;
            public const int Meta = 3;
        }

        public sealed class Periodo
        {
            public const int Diario = 1;
            public const int Semanal = 2;
            public const int Mensual = 3;
            public const int Trimestral = 4;
            public const int Semestral = 5;
            public const int Anual = 6;
        }
        public sealed class RespuestaCode
        {
            public const string Exitoso = "S";
            public const string Error = "E";
        }
        public sealed class Moneda
        {
            public const int Soles = 1;
        }

        public sealed class TipoDocumento
        {
            public const int Factura = 1;
            public const int Boleta = 2;

        }
        public sealed class TipoDocumentoIdentidad
        {
            public const int RUC = 1;
            public const int DNI = 2;

        }

        public sealed class MetodoPago
        {
            public const int Deposito = 1;
            public const int Transferencia = 2;
            public const int Efectivo = 388;
        }

        public sealed class Banco
        {
            public const int BCP = 1;
            public const int BBVA = 2;
        }
        public sealed class TipoParametro
        {
            public const int Sistema = 0;
            public const int Publico = 1;
        }

        public sealed class TipoAccion
        {
            public const int EditarDetalle = 0;
            public const int VerDetalle = 1;
            public const int FirmarDigitalmente = 2;
            public const int Reprogramar = 3;
            public const int Suspender = 4;
            public const int Anular = 5;

        }
        public sealed class ImpresionEstados
        {
            public const string Pendiente = "1";
            public const string Imprimido = "2";
            public const string Error = "3";
        }
        public sealed class TipoTeclaFuncion
        {
            public const int Escape = 1;
            public const int Borrar = 2;
            public const int BarraEspaciadora = 3;
            public const int Enter = 4;
        }
        public sealed class TipoFormato
        {
            public const int Pdf = 1;
            public const int Excel = 2;
            public const int Word = 3;
        }
        public sealed class CondicionPago
        {
            public const int Gratuito = 1;
            public const int Pagado = 2;
        }

        public sealed class TipoAdjunto
        {
            public const int principal = 1;//PendienteFormateo
            public const int adjunto = 2;
            public const int Requisito = 3;

        }
        public sealed class Cliente
        {
            public const string Natural = "Natural";
            public const string Empresa = "Empresa";
        }
    }

}

