﻿using SpreadsheetGear;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;

namespace OficinaVirtual.Util.Tool
{
    public static class clsExtension
    {
        #region Field

        private static readonly clsConvert _Convert = clsConvert.Instancia;

        #endregion Field

        #region Public

        #region DataTable

        public static Byte[] ToExcel(this DataTable table)
        {
            try
            {
                Int32 rowIni = 0;
                Int32 columnIni = 0;
                Int32 rowsTotal = table.Rows.Count;
                Int32 columnsTotal = table.Columns.Count - 1;

                IWorkbook book = Factory.GetWorkbook();
                IWorksheet sheet = book.Worksheets["Sheet1"];
                IRange range = sheet.Cells[rowIni, columnIni, rowIni, columnsTotal];

                range.CopyFromDataTable(table, SpreadsheetGear.Data.SetDataFlags.None);
                range.Range.Font.Bold = true;
                range.HorizontalAlignment = HAlign.Center;
                range.Interior.Color = SpreadsheetGear.Drawing.Color.FromArgb(184, 204, 228);

                sheet.Name = String.IsNullOrEmpty(table.TableName) ? "Data" : table.TableName;
                sheet.Cells[rowIni, columnIni, rowIni + rowsTotal, columnsTotal].Range.Borders.LineStyle = LineStyle.Continous;

                //sheet.UsedRange.Columns.AutoFit();

                return book.SaveToMemory(FileFormat.OpenXMLWorkbook);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String ToExcel(this DataTable table, String fileName)
        {
            try
            {
                Byte[] bytes = ToExcel(table);

                bytes.WriteDisk(fileName);

                return fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static Byte[] ToJsonOfBytes(this DataTable table)
        //{
        //    try
        //    {
        //        Byte[] bytes = _Convert.ToJsonOfBytes(table);

        //        return bytes;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static void ToBulkCopy(this DataTable table, String tableTarget, SqlConnection connection, params String[] columns)
        {
            try
            {
                using (var bulk = new SqlBulkCopy(connection))
                {
                    bulk.DestinationTableName = tableTarget;

                    foreach (var item in columns)
                    {
                        String[] values = item.Split(';');
                        Boolean isTwo = values.Length > 1;

                        bulk.ColumnMappings.Add(values[0], values[isTwo ? 1 : 0]);
                    }

                    bulk.WriteToServer(table);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion DataTable

        #region String

        public static String AddDateTime(this String fileName)
        {
            try
            {
                String ext = Path.GetExtension(fileName);
                String name = Path.GetFileNameWithoutExtension(fileName);

                if (String.IsNullOrEmpty(ext)) ext = ".xlsx";
                if (String.IsNullOrEmpty(name)) name = "File";

                return String.Format("{0} - {1:yyyy-MM-dd HH.mm.ss.fff}{2}", name, DateTime.Now, ext);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Byte[] ToBytes(this String text)
        {
            try
            {
                Byte[] result = Encoding.UTF8.GetBytes(text);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String ToBase64(this String text)
        {
            try
            {
                Byte[] bytes = ToBytes(text);

                return bytes.ToBase64();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Byte[] FromBase64(this String text)
        {
            try
            {
                Byte[] result = Convert.FromBase64String(text);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Byte[] OpenFile(this String text)
        {
            try
            {
                Byte[] result = File.ReadAllBytes(text);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion String

        #region Byte[]

        public static String WriteDisk(this Byte[] bytes, String fileName)
        {
            try
            {
                var d = Path.GetDirectoryName(fileName);

                if (!Directory.Exists(d)) Directory.CreateDirectory(d);

                File.WriteAllBytes(fileName, bytes);

                return fileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String ToBase64(this Byte[] bytes)
        {
            try
            {
                String result = Convert.ToBase64String(bytes);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static String ToString2(this Byte[] bytes)
        {
            try
            {
                String result = Encoding.UTF8.GetString(bytes);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable FromExcel(this Byte[] bytes)
        {
            try
            {
                DataTable result = _Convert.ToDataTable(bytes);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable FromExcel(this Byte[] bytes, String sheetName)
        {
            try
            {
                DataTable result = _Convert.ToDataTable(bytes, sheetName);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Byte[] ToThumbnail(this Byte[] image, Int32 width, Int32 height)
        {
            try
            {
                Byte[] bytes = null;

                using (MemoryStream ms = new MemoryStream())
                {
                    using (Image thumbnail = Image.FromStream(new MemoryStream(image)).GetThumbnailImage(width, height, null, new IntPtr()))
                    {
                        thumbnail.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        bytes = ms.ToArray();
                    }
                }

                return bytes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Byte[]

        #region Dictionary

        public static Hashtable ToHashtableDAO(this IDictionary param)
        {
            try
            {
                Hashtable hParam = new Hashtable();

                if (param == null) return hParam;

                foreach (DictionaryEntry item in param) hParam[String.Format("@p_{0}", item.Key)] = item.Value;

                return hParam;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Dictionary

        #region Class

        public static Hashtable ToParamDAO<T>(this T entidad) where T : class
        {
            try
            {
                var result = new Hashtable();
                var type = typeof(T);
                var typesDato = new List<String>() { "String", "Int16", "Int32", "Int64", "DateTime", "Byte", "Byte[]",
                           "Object" , "Decimal", "Double", "Single", "Boolean", "Char" };

                foreach (var item in type.GetProperties())
                {
                    var t = item.PropertyType;

                    if (typesDato.Exists(s => s == t.Name)) result[String.Format("@p_{0}", item.Name)] = item.GetValue(entidad, null);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Class

        #endregion Public
    }
}