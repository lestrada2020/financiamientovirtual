﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;

namespace OficinaVirtual.Cliente.General.App_Code
{
    public static class clsUtil
    {
        #region Enum

        private enum TypeTag
        {
            Script,
            Style
        }

        #endregion Enum

        #region Private

        private static IHtmlString TagRender(HtmlHelper help, TypeTag type, params String[] paths)
        {
            var message = new StringBuilder();
            var bundles = BundleTable.Bundles.ToList<Bundle>();

            foreach (var item in paths)
            {
                if (bundles.Exists(s => s.Path.Equals(item, StringComparison.OrdinalIgnoreCase)))
                {
                    if (type == TypeTag.Script) message.Append(Scripts.Render(item));
                    else if (type == TypeTag.Style) message.Append(Styles.Render(item));
                }
            }

            return new HtmlString(message.ToString());
        }

        #endregion Private

        #region Public

        #region HtmlHelper

        public static IHtmlString ScriptsRender(this HtmlHelper help, params String[] paths)
        {
            return TagRender(help, TypeTag.Script, paths);
        }

        public static IHtmlString StylesRender(this HtmlHelper help, params String[] paths)
        {
            return TagRender(help, TypeTag.Style, paths);
        }

        #endregion HtmlHelper

        #region Functions

        public static IHtmlString ToTagData(this HtmlHelper help, Dictionary<String, Object> data, String preFijo = "")
        {
            var s = "";

            foreach (var item in data)
            {
                if (item.Value == null || item.Value.GetType().Name.Equals("JArray")) continue;

                if (item.Value.GetType() == typeof(Dictionary<String, Object>))
                    s += ToTagData(help, (Dictionary<String, Object>)item.Value, String.Format("{0}", item.Key));
                else
                    s += String.Format(" data-{0}=\"{0};{1}\"", (String.IsNullOrWhiteSpace(preFijo) ? "" : preFijo + "-") + item.Key, item.Value ?? "");
            }

            return new HtmlString(s);
        }

        public static IHtmlString ToComboBox(this HtmlHelper help, Dictionary<String, Object> data, String preFijo = "")
        {
            var p = new Hashtable(data);
            var d = ToTagData(help, data, preFijo);
            var s = $"<div id=\"{p["Id"]}\" style=\"{p["Style"]}\" {d}>" +
                        $"<label for=\"{p["Id"]}_Combo\" class=\"form-label\" >{p["Label"]}</label>" +
                        $"<select id=\"{p["Id"]}_Combo\" name=\"{p["Id"]}_Combo\" class=\"form-control\" data-plugin=\"select2\" {p["Disabled"]} {p["Required"]} {p["Multiple"]}></select>" +
                    $"</div>";

            return new HtmlString(s);
        }

        #endregion Functions

        #endregion Public
    }
}