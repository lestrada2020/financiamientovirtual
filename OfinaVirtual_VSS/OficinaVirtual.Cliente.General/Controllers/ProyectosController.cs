﻿using OficinaVirtual.Entidad.General;
using OficinaVirtual.Fachada.General;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OficinaVirtual.Cliente.General.Controllers
{
    [SessionAuthorize]
    public class ProyectosController : BaseController
    {
        #region Fields

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsFProyectos _Proyectos = clsFProyectos.Instancia;

        #endregion Fields

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RegistrarFecha()
        {
            return View();
        }

        public ActionResult Exportar()
        {
            return View();
        }

        #region JSON

        [HttpPost]
        public async Task<JsonResult> AyudaLista(clsParametro param)
        {
            var result = await _Proyectos.AyudaLista(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ListIniciativas(clsParametro param)
        {
            var result = await _Proyectos.ListIniciativas(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerNivelesFase(clsParametro param)
        {
            var result = await _Proyectos.ObtenerNivelesFase(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerIniciativa(clsParametro param)
        {
            var result = await _Proyectos.ObtenerIniciativa(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> RegistrarHito(clsParametro param)
        {
            var result = await _Proyectos.RegistrarHito(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerHito(clsParametro param)
        {
            var result = await _Proyectos.ObtenerHito(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        public async Task<FileResult> ExportarIniciativa()
        {
            var bytes = (Byte[])null;
            var fileName = $"Iniciativas al {DateTime.Now.ToString("dd-MM-yyyy")}.xlsx";

            try
            {
                var result = await _Proyectos.ExportarIniciativa(new clsParametro());
                var table = _Convert.ToObject<DataTable>(result.Datos.ToString());

                bytes = table.ToExcel();
            }
            catch (Exception ex)
            {
                fileName = "Error.html";
                bytes = Encoding.UTF8.GetBytes(ex.Message);
            }

            Response.AddHeader("Content-Disposition", $"InLine; FileName={fileName}");

            return File(bytes, MimeMapping.GetMimeMapping(fileName));
        }

        public async Task<FileResult> ExportarImporte(String id)
        {
            var bytes = (Byte[])null;
            var fileName = $"Importes al {DateTime.Now.ToString("dd-MM-yyyy")}.xlsx";

            try
            {
                var param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["IdProyPrograma"] = id;
                var result = await _Proyectos.ExportarImporte(param);
                var table = _Convert.ToObject<DataTable>(result.Datos.ToString());

                bytes = table.ToExcel();
            }
            catch (Exception ex)
            {
                fileName = "Error.html";
                bytes = Encoding.UTF8.GetBytes(ex.Message);
            }

            Response.AddHeader("Content-Disposition", $"InLine; FileName={fileName}");

            return File(bytes, MimeMapping.GetMimeMapping(fileName));
        }

        [HttpPost]
        public async Task<JsonResult> RegistrarImporte(clsParametro param)
        {
            var result = await _Proyectos.RegistrarImporte(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerImporte(clsParametro param)
        {
            var result = await _Proyectos.ObtenerImporte(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> EditarIniciativa(clsParametro param)
        {
            var result = await _Proyectos.EditarIniciativa(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        #endregion JSON
    }
}