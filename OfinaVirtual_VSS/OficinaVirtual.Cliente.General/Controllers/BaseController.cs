﻿using OficinaVirtual.Entidad.General;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OficinaVirtual.Cliente.General.Controllers
{
    #region BaseController

    public class BaseController : Controller
    {
        #region Property

        public static String UserSessionName
        {
            get { return "UserProfile"; }
        }

        public static String UrlIndex
        {
            get { return "~/Home/Index"; }
        }

        public static String UrlLogin
        {
            get { return "~/Home/Login"; }
        }

        public Boolean IsLogged
        {
            get { return Session[UserSessionName] != null; }
        }

        public clsUsuario UserSession
        {
            set { Session[UserSessionName] = value; }
            get { return (clsUsuario)Session[UserSessionName]; }
        }

        #endregion Property

        #region Public

        public String GetPath(String folder)
        {
            return GetPath(folder, "");
        }

        public String GetPath(String folder, String area)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(area)) area = String.Format(@"\Areas\{0}", area);

                var root = Server.MapPath("~");
                var path = String.Format(@"{0}{2}\Content\Documents\{1}", root, folder, area);

                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                return path;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String GetFileName(String name, String folder)
        {
            return GetFileName(name, folder, "");
        }

        public String GetFileName(String name, String folder, String area)
        {
            try
            {
                return Path.Combine(GetPath(folder, area), Path.GetFileName(name));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String GetPathTemp(String folder)
        {
            return GetFileNameTemp(folder, "");
        }

        public String GetFileNameTemp(String folder, String fileName)
        {
            try
            {
                var n = ControllerContext.Controller.ToString();
                var r = n.Split('.');
                var t = r.FirstOrDefault(s => s.Equals("Areas"));
                var c = r[r.Length - 1].Replace("Controller", "");
                var a = t == null ? "" : String.Format(@"\{0}\{1}", t, r[r.Length - 3]);

                var p = String.Format(@"Temp{0}\{1}\{2}", a, c, folder);

                if (String.IsNullOrWhiteSpace(fileName)) return GetPath(p);
                else return GetFileName(fileName, p);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String SaveFile(Byte[] bytes, String name, String folder)
        {
            return SaveFile(bytes, name, folder, "");
        }

        public String SaveFile(Byte[] bytes, String name, String folder, String area)
        {
            try
            {
                var fileName = GetFileName(name, folder, area);

                return bytes.WriteDisk(fileName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String SaveFileTemp(Byte[] bytes, String name, String folder)
        {
            try
            {
                var fileName = GetFileNameTemp(folder, name);

                return bytes.WriteDisk(fileName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Byte[] OpenFile(String name, String folder)
        {
            return OpenFile(name, folder, "");
        }

        public Byte[] OpenFile(String name, String folder, String area)
        {
            try
            {
                var fileName = GetFileName(name, folder, area);

                var bytes = fileName.OpenFile();

                return bytes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Byte[] OpenFileTemp(String name, String folder)
        {
            try
            {
                var fileName = GetFileNameTemp(folder, name);

                var bytes = fileName.OpenFile();

                return bytes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public String GetURL(String fileName)
        {
            try
            {
                var root = Server.MapPath("~");
                var url = fileName.Replace(root, "");

                return Url.Content("~" + url);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Public
    }

    #endregion BaseController

    #region SessionAuthorizeAttribute

    //[AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class SessionAuthorizeAttribute : AuthorizeAttribute
    {
        #region Fields

        private Boolean _IsAuthenticated = false;
        private Boolean _IsAuthorized = false;
        private List<TypeAplicacion> _Aplicaciones = new List<TypeAplicacion>();

        #endregion Fields

        #region Constructors

        public SessionAuthorizeAttribute()
        {
        }

        public SessionAuthorizeAttribute(params TypeAplicacion[] types)
        {
            _Aplicaciones = types.ToList();
        }

        #endregion Constructors

        #region Private

        private Boolean CompareUrls(String url01, String url02)
        {
            url01 = url01 ?? "";
            url02 = url02 ?? "";

            url01 = url01.Replace("/", "").ToLower();
            url02 = url02.Replace("/", "").ToLower();

            return url01 == url02;
        }

        private List<Int16> AppsAllows(clsUsuario user)
        {
            try
            {
                List<Int16> result = new List<Int16>();

                if (user == null) return result;

                var appsControl = _Aplicaciones.Select(s => (Int16)s);
                var appsAssigned = user.Aplicaciones.Select(s => s.IdAplicacion);

                result = appsAssigned.Join(appsControl, s => s, t => t, (s, t) => t).ToList();

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Private

        #region Public

        public override void OnAuthorization(AuthorizationContext context)
        {
            base.OnAuthorization(context);
        }

        protected override bool AuthorizeCore(HttpContextBase context)
        {
            return context.Session[BaseController.UserSessionName] != null;
            var user = (clsUsuario)context.Session[BaseController.UserSessionName];

            #region URL

            String url = ((Route)context.Request.RequestContext.RouteData.Route).Url;
            foreach (var item in context.Request.RequestContext.RouteData.Values) url = url.Replace(String.Format("{{{0}}}", item.Key), item.Value.ToString());
            url = url.Replace("{id}", "");

            #endregion URL

            #region Security

            _IsAuthenticated = user != null;
            List<Int16> apps = AppsAllows(user);
            _IsAuthorized = _Aplicaciones.Count == 0
                            || (apps.Count > 0
                                && (user.Opciones.Exists(s => CompareUrls(s.Directorio, url))
                                    || context.Request.Params["Token"] == user.Token));

            #endregion Security

            #region Error

            if (!_IsAuthenticated) context.Response.AddHeader("Error", "Favor de actualizar la pagina.");
            else if (!_IsAuthorized) context.Response.AddHeader("Error", "Usted no esta autorizado para esta opcion.");

            #endregion Error

            return _IsAuthenticated && _IsAuthorized;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            if (context.HttpContext.Request.IsAjaxRequest()) base.HandleUnauthorizedRequest(context);
            else context.Result = new RedirectResult(_IsAuthenticated ? BaseController.UrlIndex : BaseController.UrlLogin);
        }

        #endregion Public
    }

    #endregion SessionAuthorizeAttribute
}