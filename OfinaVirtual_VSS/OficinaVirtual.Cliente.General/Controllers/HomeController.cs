﻿using OficinaVirtual.Entidad.General;
using OficinaVirtual.Fachada.General;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OficinaVirtual.Cliente.General.Controllers
{
    public class HomeController : BaseController
    {
        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsFProyectos _Proyectos = clsFProyectos.Instancia;

        public async Task<ActionResult> Index()
        {
            return RedirectToAction("Login");
        }

        public async Task<ActionResult> Login()
        {
            try
            {
                if (!String.IsNullOrEmpty(Request.Form.AllKeys.FirstOrDefault(s => s.ToLower().Equals("usuario"))))
                {
                    var param = new Dictionary<String, String>();
                    param["ADUser"] = Request.Form["Usuario"];
                    param["ADPassword"] = Request.Form["Contrasenia"];
                    param["Sistema"] = "1";

                    param["ADUser"] = param["ADUser"].ToLower().Replace("/", "").Replace("\\", "").Replace("distriluz", "");

                    var rsAD = await _Proyectos.ValidaUserAD(param);

                    if (rsAD.Datos == null) throw new Exception("<<<No se pudo validar con el AD.>>>");

                    var userAD = _Convert.ToObject<Dictionary<String, Object>>(rsAD.Datos.ToString());
                    var auth = _Convert.ToObject<Dictionary<String, String>>(userAD["getAuth"].ToString());

                    if (!Convert.ToBoolean(auth["resultAuth"])) throw new Exception($"<<<{auth["desResult"]}>>>");

                    param["NombreUsuario"] = param["ADUser"];
                    var rsUser = await _Proyectos.ObtenerUsuario(new clsParametro { Parametros = param });

                    if (rsUser.Datos == null) throw new Exception("<<<Usuario no tiene permisos.>>>");

                    var user = _Convert.ToObject<Dictionary<String, String>>(rsUser.Datos.ToString());

                    Session[UserSessionName] = new clsUsuario
                    {
                        IdUsuario = Convert.ToInt16(user["IdProyUsuario"]),
                        Nombre = user["Nombres"],
                        Usuario = user["NombreUsuario"],
                        IdEmpresa = Convert.ToInt16(user["IdEmpresa"]),
                        IdAplicacionActivo = (Int16)TypeAplicacion.Proyectos,
                        Opciones = _Convert.ToObject<List<clsOpcion>>(user["Options"])
                    };
                }

                if (Session[UserSessionName] != null) return RedirectToAction("Index", "Proyectos");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
            }

            return View();
        }

        public async Task<ActionResult> Logout()
        {
            Session.Clear();

            return RedirectToAction("Login", "Home");
        }
    }
}