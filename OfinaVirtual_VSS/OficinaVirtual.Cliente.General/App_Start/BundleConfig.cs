﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace OficinaVirtual.Cliente.General.App_Start
{
    #region BundleConfig

    public class BundleConfig
    {
        #region Fields

        private static String _ScriptPath = "JS/Scripts";
        private static String _ScriptPluginPath = "JS/Plugins";
        private static String _StylePath = "CSS/Styles";

        #endregion Fields

        #region Private

        private static String VirtualPath(String path, String area, String controller, String action)
        {
            if (String.IsNullOrWhiteSpace(area)) return String.Format("~/{0}/{1}/{2}", path, controller, action);
            else return String.Format("~/{0}/{1}/{2}/{3}", path, area, controller, action);
        }

        #region Styles

        private static void RootStyles(BundleCollection bundles)
        {
            var area = String.Empty;
            var controller = String.Empty;
            var action = String.Empty;

            #region Home

            controller = "Home";

            #region Login

            action = "Login";

            bundles.Add(new StyleBundle(VirtualPath(_StylePath, area, controller, action))
                .Include(String.Format("~/Content/Styles/{0}/{1}.css", controller, action), new clsCssRewriteUrlTransform())
                .Order());

            #endregion Login

            #endregion Home

            #region Proyectos

            controller = "Proyectos";

            #region Index

            action = "Index";

            bundles.Add(new StyleBundle(VirtualPath(_StylePath, area, controller, action))
                .Include(String.Format("~/Content/Styles/{0}/{1}.css", controller, action), new clsCssRewriteUrlTransform())
                .Order());

            #endregion Index

            #region RegistrarFecha

            action = "RegistrarFecha";

            bundles.Add(new StyleBundle(VirtualPath(_StylePath, area, controller, action))
                .Include("~/Content/Template/vendor/select2/select2.min3f0d.css",
                    "~/Content/Template/vendor/bootstrap-touchspin/bootstrap-touchspin.min3f0d.css",
                    "~/Content/Template/vendor/bootstrap-datepicker/bootstrap-datepicker.min3f0d.css",
                    "~/Content/Template/vendor/datatables-bootstrap/dataTables.bootstrap.min3f0d.css",
                    "~/Content/Template/vendor/datatables-fixedheader/dataTables.fixedHeader.min3f0d.css",
                    "~/Content/Template/vendor/datatables-responsive/dataTables.responsive.min3f0d.css"
                 ).Order()
                .Include("~/Content/Template/fonts/font-awesome/font-awesome.min3f0d.css", new clsCssRewriteUrlTransform())
                .Include(String.Format("~/Content/Styles/{0}/{1}.css", controller, action), new clsCssRewriteUrlTransform()
                ).Order());

            #endregion RegistrarFecha

            #region Exportar

            action = "Exportar";

            bundles.Add(new StyleBundle(VirtualPath(_StylePath, area, controller, action))
                .Include("~/Content/Template/vendor/select2/select2.min3f0d.css"
                 ).Order()
                .Include("~/Content/Template/fonts/font-awesome/font-awesome.min3f0d.css", new clsCssRewriteUrlTransform())
                .Include(String.Format("~/Content/Styles/{0}/{1}.css", controller, action), new clsCssRewriteUrlTransform())
                .Order());

            #endregion Exportar

            #endregion Proyectos
        }

        #endregion Styles

        #region Scripts

        private static void RootScripts(BundleCollection bundles)
        {
            var area = String.Empty;
            var controller = String.Empty;
            var action = String.Empty;

            #region Home

            controller = "Home";

            #region Index

            action = "Index";

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPath, area, controller, action))
                .Include("~/Content/Template/vendor/matchheight/jquery.matchHeight-min.js",
                    "~/Content/Template/js/components/matchheight.min.js",
                    String.Format("~/Scripts/{0}/{1}.js", controller, action)
                ).Order());

            #endregion Index

            #region Login

            action = "Login";

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPluginPath, area, controller, action))
                .Include("~/Content/Template/vendor/jquery-placeholder/jquery.placeholder.min.js"
                ).Order());

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPath, area, controller, action))
                .Include("~/Content/Template/js/components/jquery-placeholder.min.js",
                    "~/Content/Template/js/components/material.min.js",
                    "~/Content/Template/vendor/jquery-md5/jquery.md5.min.js",
                    String.Format("~/Scripts/{0}/{1}.js", controller, action)
                ).Order());

            #endregion Login

            #endregion Home

            #region Proyectos

            controller = "Proyectos";

            #region Index

            action = "Index";

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPath, area, controller, action))
                .Include("~/Content/Template/vendor/matchheight/jquery.matchHeight-min.js",
                    "~/Content/Template/js/components/matchheight.min.js",
                    String.Format("~/Scripts/{0}/{1}.js", controller, action)
                ).Order());

            #endregion Index

            #region RegistrarFecha

            action = "RegistrarFecha";

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPluginPath, area, controller, action))
                .Include("~/Content/Template/vendor/select2/select2.min.js",
                    "~/Content/Template/vendor/icheck/icheck.min.js",
                    "~/Content/Template/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js",
                    "~/Content/Template/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js",
                    "~/Content/Template/vendor/datatables/jquery.dataTables.min.js",
                    "~/Content/Template/vendor/datatables-bootstrap/dataTables.bootstrap.min.js",
                    "~/Content/Template/vendor/datatables-responsive/dataTables.responsive.js"
                ).Order());

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPath, area, controller, action))
                .Include("~/Content/Template/js/components/select2.min.js",
                    "~/Content/Template/js/components/icheck.min.js",
                    "~/Content/Template/js/components/bootstrap-datepicker.min.js",
                    "~/Content/Template/js/components/bootstrap-touchspin.min.js",
                    "~/Content/Template/js/components/datatables.min.js",
                    "~/Scripts/Shared/ComboBox.js",
                    "~/Content/Template/js/components/material.min.js",
                    String.Format("~/Scripts/{0}/{1}.js", controller, action)
                ).Order());

            #endregion RegistrarFecha

            #region Exportar

            action = "Exportar";

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPluginPath, area, controller, action))
                .Include("~/Content/Template/vendor/select2/select2.min.js"
                ).Order());

            bundles.Add(new ScriptBundle(VirtualPath(_ScriptPath, area, controller, action))
                .Include("~/Content/Template/js/components/select2.min.js",
                    "~/Scripts/Shared/ComboBox.js",
                    "~/Content/Template/js/components/material.min.js",
                    String.Format("~/Scripts/{0}/{1}.js", controller, action)
                ).Order());

            #endregion Exportar

            #endregion Proyectos
        }

        #endregion Scripts

        #endregion Private

        #region Public

        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Styles

            bundles.Add(new StyleBundle("~/CSS/Core")
                .Include("~/Content/Template/css/bootstrap.min3f0d.css",
                    "~/Content/Template/css/bootstrap-extend.min3f0d.css",
                    "~/Content/Template/assets/css/site.min3f0d.css",
                    "~/Content/Template/css/skintools.min3f0d.css"
                ).Order());

            bundles.Add(new StyleBundle("~/CSS/Plugins")
                .Include("~/Content/Template/vendor/animsition/animsition.min3f0d.css",
                    "~/Content/Template/vendor/asscrollable/asScrollable.min3f0d.css",
                    "~/Content/Template/vendor/switchery/switchery.min3f0d.css",
                    "~/Content/Template/vendor/intro-js/introjs.min3f0d.css",
                    "~/Content/Template/vendor/slidepanel/slidePanel.min3f0d.css").Order()
                .Include("~/Content/Template/vendor/flag-icon-css/flag-icon.min3f0d.css", new clsCssRewriteUrlTransform())
                .Include("~/Content/Template/vendor/waves/waves.min3f0d.css",
                    "~/Content/Template/vendor/jquery-mmenu/jquery-mmenu.min3f0d.css",
                    "~/Content/Template/vendor/sweetalert2/sweetalert2.min.css",
                    "~/Content/Styles/Shared/_Layout.css"
                ).Order());

            bundles.Add(new StyleBundle("~/CSS/Fonts")
                .Include("~/Content/Template/fonts/material-design/material-design.min3f0d.css", new clsCssRewriteUrlTransform())
                .Include("~/Content/Template/fonts/brand-icons/brand-icons.min3f0d.css", new clsCssRewriteUrlTransform())
                .Order());

            #endregion Styles

            #region Scripts

            bundles.Add(new ScriptBundle("~/JS/Modernizr")
                .Include("~/Content/Template/assets/js/sections/skintools.min.js",
                    "~/Content/Template/vendor/modernizr/modernizr.min.js",
                    "~/Content/Template/vendor/breakpoints/breakpoints.min.js"
                ).Order());

            bundles.Add(new ScriptBundle("~/JS/Core")
                .Include("~/Content/Template/vendor/jquery/jquery.min.js",
                    "~/Content/Template/vendor/bootstrap/bootstrap.min.js",
                    "~/Content/Template/vendor/animsition/animsition.min.js",
                    "~/Content/Template/vendor/asscroll/jquery-asScroll.min.js",
                    "~/Content/Template/vendor/mousewheel/jquery.mousewheel.min.js",
                    "~/Content/Template/vendor/asscrollable/jquery.asScrollable.all.min.js",
                    "~/Content/Template/vendor/ashoverscroll/jquery-asHoverScroll.min.js",
                    "~/Content/Template/vendor/waves/waves.min.js"
                ).Order());

            bundles.Add(new ScriptBundle("~/JS/Plugins")
                .Include("~/Content/Template/vendor/jquery-mmenu/jquery.mmenu.min.all.js",
                    "~/Content/Template/vendor/switchery/switchery.min.js",
                    "~/Content/Template/vendor/intro-js/intro.min.js",
                    "~/Content/Template/vendor/screenfull/screenfull.min.js",
                    "~/Content/Template/vendor/slidepanel/jquery-slidePanel.min.js",
                    "~/Content/Template/vendor/sweetalert2/sweetalert2.min.js"
                ).Order());

            bundles.Add(new ScriptBundle("~/JS/Scripts")
                .Include("~/Content/Template/js/core.min.js",
                    "~/Content/Template/assets/js/site.min.js",
                    "~/Content/Template/assets/js/sections/menubar.min.js",
                    "~/Content/Template/assets/js/sections/gridmenu.min.js",
                    "~/Content/Template/assets/js/sections/sidebar.min.js",
                    "~/Content/Template/js/configs/config-colors.min.js",
                    "~/Content/Template/assets/js/configs/config-tour.min.js",
                    "~/Content/Template/js/components/asscrollable.min.js",
                    "~/Content/Template/js/components/animsition.min.js",
                    "~/Content/Template/js/components/slidepanel.min.js",
                    "~/Content/Template/js/components/switchery.min.js",
                    "~/Content/Template/js/components/tabs.min.js",
                    "~/Content/Template/js/components/panel.min.js",
                    "~/Scripts/Shared/Util.js"
                ).Order());

            #endregion Scripts

            RootStyles(bundles);
            RootScripts(bundles);
        }

        #endregion Public
    }

    #endregion BundleConfig

    #region Internal

    internal class clsBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }

    internal class clsCssRewriteUrlTransform : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
        }
    }

    #endregion Internal

    #region Extensions

    public static class BundleExtensions
    {
        public static Bundle Order(this Bundle sb)
        {
            sb.Orderer = new clsBundleOrderer();
            return sb;
        }
    }

    #endregion Extensions
}