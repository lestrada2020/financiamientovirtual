﻿/// <reference path="../../content/template/vendor/jquery/jquery.min.js" />
/*
    Creación        : 2020-08-24
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Field

    var _List = {};
    var _U = window.nsUtil;

    //#endregion

    //#region Private

    function Default() {
        return {
            Id: '',
            IdConfig: '',
            IdJq: undefined,
            SubFijo: '_',
            Type: 'ComboBox',
            Index: 0,
            IdAplicacion: 0,
            Entity: '',
            Action: 'AyudaLista',
            ActionRecord: 'AyudaRegistro',
            Controller: 'Proyectos',
            Title: 'Seleccione...',
            TextAll: 'Todos',
            ShowAll: false,
            Value: null,
            HasIcon: false,
            IsLoad: false,
            IsLoadChange: true,
            IsAsync: true,
            IsLog: false,
            IsGetRecord: false,
            DisabledOnlyItem: false,
            SelectedOnlyItem: false,
            SelectedFirstItem: false,
            Parametros: {},
            Record: {
                IdParam: '@p_IdRegistro',
                Callback: undefined
            },
            Change: undefined,//(event, config)
            KeyPress: undefined,//(event, config)
            CallbackDone: undefined,//(data, textStatus, jqXHR)
            CallbackFail: undefined,//(jqXHR, textStatus, errorThrown)
            CallbackAlways: undefined//(data, textStatus, jqXHR)
        };
    }

    //#region Init

    function Init(config) {
        config.IdConfig = config.IdConfig || config.Id;
        config = _U.AddConfig(_List, Default(), config);

        InitControls(config);
        InitFunctions(config);
        InitSettings(config);

        if (config.IsLoad) Fill(config);

        return config;
    }

    function InitControls(config) {
        config.Controls = {
            Combo: {},
            Text: {}
        };

        _U.SetIdJq(config.Controls, config.Id, '_');

        config.IdJq = config.Controls.Root.IdJq;
    }

    function InitFunctions(config) {
        config.Clear = function () { Clear(config, true); };
        config.Fill = function () { Fill(config); };
        config.GetEntidad = function () { return GetEntidad(config); };
        config.GetRecord = function () { return GetRecord(config); };
        config.SetValue = function (value) { SetValue(config, value); };
    }

    function InitSettings(config) {
        var c = config.Controls.Combo.IdJq;

        c.on('change', function (event) { Combo_Change(event, config, this); });
    }

    //#endregion

    //#region Functions

    function GetEntidad(config) {
        var c = config.Controls.Combo.IdJq;

        var e = {
            Id: c.val(),
            Name: !c.val() ? '' : c.children('option:selected').text()
        };

        if (e.Id instanceof Array) {
            if (e.Id.length == 0) {
                e.Id = null;
                e.Name = '';
            }
            else {
                e.Name = '';
                c.children('option:selected').each(function (index, value) {
                    e.Name += $(value).text() + ', '
                });
            }
        }

        return e;
    }

    function GetRecord(config) {
        var c = config.Controls.Combo.IdJq,
            e = c.data('Record');

        return e;
    }

    function Clear(config, triggerChange) {
        var c = config.Controls.Combo.IdJq;

        c.html('');
        c.next().find('span.select2-selection__rendered').removeAttr('title').html('');

        if (triggerChange) OnChange(event, config);
    }

    function SetValue(config, value) {
        var c = config.Controls.Combo.IdJq;

        c.val(value);
        c.change();
    }

    function OnChange(event, config) {
        if (config.Change) config.Change(event, config);
    }

    function OnKeyPress(event, config) {
        if (config.KeyPress) config.KeyPress(event, config);
    }

    function Fill(config) {
        var c = config.Controls.Combo.IdJq,
            a = {
                Entity: config.Entity,
                Action: config.Action
            },
            p = $.extend(true, a, config.Parametros);

        _U.Ajax(config.Controller, config.Action, { Parametros: p }, function (data, textStatus, jqXHR) {
            if (_U.IsAjaxError(data, jqXHR)) return;
            var d = JSON.parse(data.Datos || []);

            var n = d.length;
            if (config.ShowAll) c.append($('<option />').val(0).text(config.TextAll));
            if (config.IsLog) console.log(d);

            $.each(d, function (i, v) {
                var o = $('<option />').val(v.Id).text(v.Nombre);

                var s = (config.SelectedOnlyItem && n == 1)
                    || (config.SelectedFirstItem && i == 0)
                    || (config.Value == v.Id);

                if (s) o.prop('selected', true);

                c.append(o);
            });

            if (config.DisabledOnlyItem && n == 1) c.prop('disabled', true);

            if (config.IsLoadChange) c.trigger('change');

            if (config.CallbackDone) config.CallbackDone(d, textStatus, jqXHR);
        },
            config.CallbackAlways, config.CallbackFail, config.IsAsync);
    }

    //#endregion Functions

    //#region Events

    function Combo_Change(event, config, sender) {
        var c = config.Controls.Combo.IdJq;

        if (config.IsGetRecord || config.Record.Callback) {
            c.removeData('Record');
            config.Parametros[config.Record.IdParam] = c.val();

            var e = $.extend(true, {}, config);

            delete e.Controls;
            delete e.IdJq;

            _U.Ajax(config.Controller, config.ActionRecord, e,
                function (data, textStatus, jqXHR) {
                    var record = data.Registro;
                    var exists = Object.keys(record).length > 0;

                    if (exists) c.data('Record', record);
                    if (config.Record.Callback) config.Record.Callback(config, exists ? record : undefined);
                });
        }

        OnChange(event, config);
    }

    function Text_KeyPress(event, config, sender) {
        if (event.which > 0 && event.which != 13) return;

        event.preventDefault();

        var t = config.Controls.Text.IdJq,
            c = config.Controls.Combo.IdJq;

        c.val(t.val());

        if (!c.val()) t.val('');

        OnChange(event, config);
        OnKeyPress(event, config);
    }

    //#endregion Events

    //#endregion Private

    //#region Public

    context.Init = function (config) {
        return Init(config);
    };

    context.GetConfig = function (id) {
        return id instanceof Object ? id : _List[id];
    };

    //#endregion
}(window.nsUtil.SetNameSpace('Root', 'Shared', 'ComboBox'), jQuery));