﻿/*
    Creación        : 2020-08-20
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C = {};
    var _U = window.nsUtil;

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (f) {
            window.Site.run();
            _C = _U.GetControlsJQ();
            Events();
        });
    }

    function Events() {
        _C.frmLogin.on('submit', frmLogin_Submit);
    }

    function frmLogin_Submit(e) {
        //var key = $.md5(_C.txtContrasenia.val());
        //_C.txtContrasenia.val(key.toUpperCase());

        return true;
    }

    //#endregion Private

    //#region Public

    (function () { Load(); })();

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Home', 'Login'), jQuery));