﻿/// <reference path="../../content/template/vendor/jquery/jquery.min.js" />
/// <reference path="../shared/util.js" />
/// <reference path="../shared/combobox.js" />

/*
    Creación        : 2020-08-20
    Responsable     : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C = {},
        _O = {},
        _U = window.nsUtil;

    var _cmbEmpresa,
        _cmbA,
        _cmbB,
        _cmbC,
        _cmbD,
        _cmbProgram,
        _cmbCoord;

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (f) {
            window.Site.run();

            _C = _U.GetControlsJQ();
            _S = _U.GetSession();
            _O.Months = _U.DefaultFormatDateTime().MonthNames
            _O.Tried = 0;

            ConfigGeneral();
            ConfigButton();
            ConfigComboBox();
            ConfigTable();
            ConfigDatepicker();
        });
    }

    //#region Config

    function ConfigButton() {
        _C.btnSearch.on('click', btnSearch_click);
        _C.btnSave.on('click', btnSave_click);
        _C.divP.on('click', 'button', btnPostpone_click);
        _C.btnImpPrev.on('click', btnImpPrev_click);
        _C.btnImpNext.on('click', btnImpNext_click);
        _C.btnSaveImp.on('click', btnSaveImp_click);
    }

    function ConfigGeneral() {
        _C.txtCode.on('keydown', txtCode_keydown);
        _C.divP.on('change', 'input', txtImpMonth_change);
        _C.txtCode.focus();

        if (+_S.IdEmpresa == 0) {
            _U.ToArrayJQ([_C.txtA, _C.txtB, _C.txtC, _C.txtD
                , _C.txtA.prev(), _C.txtB.prev(), _C.txtC.prev(), _C.txtD.prev()]
            ).css('display', 'none');
        }
    }

    function ConfigComboBox() {
        _C.cmbType.on('change', cmbType_change);
        _cmbEmpresa = _U.SetConfigComboBox('cmbEmpresa', cmbEmpresa_change);
        _cmbProgram = _U.SetConfigComboBox('cmbProgram', cmbProgram_change);
        _cmbCoord = _U.SetConfigComboBox('cmbCoord', cmbCoord_change);
        _C.cmbCoord.on('keydown', cmbCoord_keydown);

        if (+_S.IdEmpresa == 0) {
            _U.ToArrayJQ([_C.cmbA, _C.cmbB, _C.cmbC, _C.cmbD]).css('display', 'block');
            _cmbA = _U.SetConfigComboBox('cmbA', cmbA_change);
            _cmbB = _U.SetConfigComboBox('cmbB', cmbB_change);
            _cmbC = _U.SetConfigComboBox('cmbC', cmbC_change);
            _cmbD = _U.SetConfigComboBox('cmbD', cmbD_change);
        }
    }

    function ConfigTable02() {
        _tblMaestroLectu = _U.SetConfigDataTable({
            Id: 'tblMaestroLectu',
            URL: _U.UrlAction('', 'Proyectos'),
            Dom: 'frt',
            Columns: [
                {
                    render: function (data, type, row, meta) {
                        var b = '<div class="row-actions">' +
                            '<button data-rowindex="' + meta.row + '" type="button" class="btn btn-icon btn-sm btn-info" data-type="A" title="Editar" data-target="#modDetalle" data-toggle="modal"><i class="icon md-edit" aria-hidden="true"></i></button>' +
                            '<div class="btn-group"><button type="button" class="btn btn-icon btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-hidden="true"><i class="icon md-wrench" aria-hidden="true"></i></button><ul class="dropdown-menu" role="menu">' +
                            '<li role="presentation"><a href="javascript:void(0)" role="menuitem" data-type="B"><i class="icon md-' + (row.IdEstado == 1 ? 'close' : 'check') + '" aria-hidden="true"></i>' + (row.IdEstado == 1 ? 'Desactiva' : 'Activa') + '</a></li>' +
                            (row.IdEstado == 1 ? '<li role="presentation"><a href="javascript:void(0)" role="menuitem" data-type="C"><i class="icon md-upload" aria-hidden="true"></i>Re-Envio</a></li>' : '') +
                            '</ul></div>' +
                            '</div>';

                        return b;
                    },
                    width: '70px'
                },
                { data: 'IdProveedorPersonal', width: '50px' },
                { data: 'NombreTrabajador', width: '300px' },
                { data: 'NroIdentidad', width: '75px' },
                { data: 'Usuario', width: '100px' },
                { data: 'Rol', width: '150px' },
                { data: 'CorreoElectronico', width: '150px' },
                { data: 'FechaCreacion', width: '180px' },
                { data: 'VersionApp', width: '75px' },
                { data: 'EquipoMovil', width: '120px' },
                { data: 'DescripcionEstado', width: '75px' }
            ]
        });

        _tblMaestroLectu.IdJq.find('tbody').on('click', 'button[data-type], a[data-type]', tblMaestroLectu_RowActions);
    }

    function ConfigTable() {
        _C.tblResult.on('click', 'tbody tr td:first-child button[data-type]', tblResult_RowActions);
        _C.tblResult.on('click', 'tbody tr td:nth-child(4) a', tblResult_ColCoord);
    }

    function ConfigDatepicker() {
        $.fn.datepicker.defaults.autoclose = true;
        $.fn.datepicker.defaults.clearBtn = true;
        $.fn.datepicker.defaults.todayHighlight = true;
        $.fn.datepicker.defaults.format = "dd/mm/yyyy";
    }

    //#endregion Config

    //#region Events

    function btnSearch_click() {
        LoadTable();
    }

    function btnSave_click() {
        var l = GetListHitos(),
            d = GetRowData(),
            t = $('#chkAddETO'),
            p = {
                IdProyIniciativa: d.IdProyIniciativa,
                IdProyUsuario: _S.IdUsuario,
                Comentario: _C.txtComent.val(),
                Detalle: JSON.stringify(l),
                AddETO: t.length > 0 ? +t.prop('checked') : 0
            };

        if (p.Comentario == d.Comentario) {
            _U.AlertError('Debe de modificar el comentario');
            return;
        }

        var m = _C.divE.data('DateMin').split('/'),
            n = _C.divE.data('DateMax').split('/'),
            b = true,
            s = _U.StringFormat('Existen fechas fuera del limite.<br/>Fecha Mínima: {0}<br/>Fecha Máxima: {1}<br/><br/>Desea continuar...', m.join('/'), n.join('/'));

        m = m[2] + m[1] + m[0];
        n = n[2] + n[1] + n[0];

        for (var i = 0; i < l.length; i++) {
            let v = +l[i].Fecha;

            if (v > 0) b &= v >= m && v <= n;
        }

        if (b) RegisterHito(p);
        else _U.AlertQuestion(s, function () { RegisterHito(p); });
    }

    function btnSaveImp_click() {
        var d = GetRowData(),
            p = {
                IdProyIniciativa: d.IdProyIniciativa,
                IdProyUsuario: _S.IdUsuario,
                IdProyPrograma: _cmbProgram.GetEntidad().Id,
                Detalle: JSON.stringify(_O.ListImp)
            };

        _U.Ajax('Proyectos', 'RegistrarImporte', { Parametros: p }, function (data) {
            var d = JSON.parse(data.Datos);
            _U.AlertSuccess('Se guardo correctamente.')
        });

        _C.btnCancelImp.click();
    }

    function btnPostpone_click() {
        var s = $(this),
            x = +_O.ListImp.reduce((m, s) => +s.IdPeriodo < m ? m : +s.IdPeriodo, 0),
            m = +s.closest('.input-group').find('input').attr('id').replace('txtP', ''),
            y = _C.lblImpYear.data('year'),
            p = y * 100 + m;

        if (x == 0) return;

        var a = (_O.ListImp.find(s => +s.IdPeriodo == +p) || {}).Importe || 0,
            b = 0;
        SetImporte(p, 0);

        while (x >= p) {
            p = _U.PeriodoAdd(p, 1);
            b = (_O.ListImp.find(s => +s.IdPeriodo == +p) || {}).Importe || 0;
            SetImporte(p, a);
            a = b;
        }

        LoadP();
    }

    function btnImpPrev_click() {
        var a = _C.lblImpYear.data('year'),
            v = a - 1;

        ShowYear(v);
        LoadP();
        ValidTypeProgram();
    }

    function btnImpNext_click() {
        var a = _C.lblImpYear.data('year'),
            v = a + 1;

        ShowYear(v);
        LoadP();
        ValidTypeProgram();
    }

    function tblResult_RowActions() {
        var c = $(this),
            t = c.data('type');

        SetRowSelected(c);

        if (t == 'A') EditarRegistro();
        else if (t == 'B') EditarImporte();
    }

    function txtCode_keydown(e) {
        if (e.which == 8 || e.which == 46) _C.tblResult.find('tbody').html('');
        if (e.which == 13) btnSearch_click();
    }

    function txtImpMonth_change() {
        var e = $(this),
            y = +_C.lblImpYear.data('year'),
            m = +e.attr('id').replace('txtP', ''),
            p = y * 100 + m;

        SetImporte(p, e.val());
        SumImporte();
    }

    function cmbType_change() {
        _C.tblResult.find('tbody').html('');
        setTimeout(function () { _C.txtCode.focus().select(); }, 100);
    }

    function cmbEmpresa_change() {
        var b = _C.tblResult.find('tbody');
        b.html('');
    }

    function cmbProgram_change() {
        EditarImporte();
    }

    function cmbA_change(event, config) {
        _cmbB.Clear();
        _cmbB.Parametros.IdPadre = +_cmbA.GetEntidad().Id;

        if (_cmbB.Parametros.IdPadre == 0) return;

        _cmbB.Fill();
    }

    function cmbB_change(event, config) {
        _cmbC.Clear();
        _cmbC.Parametros.IdPadre = +_cmbB.GetEntidad().Id;

        if (_cmbC.Parametros.IdPadre == 0) return;

        _cmbC.Fill();
    }

    function cmbC_change(event, config) {
        _cmbD.Clear();
        _cmbD.Parametros.IdPadre = +_cmbC.GetEntidad().Id;

        if (_cmbD.Parametros.IdPadre == 0) return;

        _cmbD.Fill();
    }

    function cmbD_change(event, config) {
        _C.divE.html('');
        var id = +_cmbD.GetEntidad().Id;

        if (id == 0) return;

        LoadE(id);
    }

    function dtpAll_change(event) {
        var s = $(this),
            a = s.val() == '',
            t = $('#chkAddETO');

        if (t.length > 0) {
            t.prop('checked', false).prop('disabled', a);

            if (!a)
                _U.AlertQuestion({
                    html: '¿Concurso de pre-inversión, incluía ETO?',
                    confirmButtonText: 'SI',
                    cancelButtonText: 'NO',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33'
                }, function () {
                    t.prop('checked', true);
                });
        }
    }

    function cmbCoord_change() {
        var c = _cmbCoord.GetEntidad(),
            p = GetRowData();

        if (+c.Id == 0 || +c.Id == p.IdProyCoordinador) return;

        p.IdProyCoordinador = +c.Id;
        p.Coordinador = c.Name;

        EditarIniciativa(p);
        ColCoordCancel();
    }

    function tblResult_ColCoord() {
        var c = $(this);

        ColCoordShow(c);
    }

    function cmbCoord_keydown(e) {
        if (e.which == 27) ColCoordCancel();
    }

    //#endregion Events

    function RegisterHito(param) {
        _U.Ajax('Proyectos', 'RegistrarHito', { Parametros: param }, function (data) {
            var d = JSON.parse(data.Datos);

            GetLastHito();
            _U.AlertSuccess('Se guardo correctamente.');
        });

        _C.btnCancel.click();
    }

    function LoadTable() {
        var b = _C.tblResult.find('tbody'),
            p = {
                IdEmpresa: _cmbEmpresa.GetEntidad().Id,
                Nro: _C.txtCode.val(),
                Type: _C.cmbType.val()
            };

        _C.divHidden.append(_C.cmbCoord);
        b.html('');
        _U.Ajax('Proyectos', 'ListIniciativas', { Parametros: p }, function (data) {
            var d = JSON.parse(data.Datos);

            $.each(d, function (i, v) {
                var r = $('<tr/>'),
                    m = '<button type="button" class="btn btn-icon btn-sm btn-info margin-right-3" data-type="A" title="Editar" data-target="#modDetalle" data-toggle="modal"><i class="icon md-calendar" aria-hidden="true"></i></button>',
                    n = '<button type="button" class="btn btn-icon btn-sm btn-info" data-type="B" title="Program" data-target="#modProgram" data-toggle="modal"><i class="icon" aria-hidden="true">S/</i></button>',
                    c = '';

                c += _U.StringFormat('<td><div class="display-flex">{0}</div></td>', v.FechaInicia == '' && _S.IdUsuario != 79 ? '' : (m + n));
                c += _U.StringFormat('<td>{0}</td>', v.Empresa);
                c += _U.StringFormat('<td>{0}</td>', v.NroIniciativa);
                c += _U.StringFormat(_S.IdEmpresa > 0 ? '<td>{0}</td>' : '<td><a href="javascript:void(0)" class="text-info">{0}</a></td>', v.Coordinador);
                c += _U.StringFormat('<td>{0}</td>', v.NroViabilidad);
                c += _U.StringFormat('<td>{0}</td>', v.NroAPI);
                c += _U.StringFormat('<td><span class="label label-outline label-success">{0}</span></td>', v.Fase);
                c += _U.StringFormat('<td>{0}</td>', v.FechaInicia);
                c += _U.StringFormat('<td>{0}</td>', v.Descripcion);
                c += _U.StringFormat('<td>{0}</td>', v.Comentario);

                r.data('entity', v).html(_U.StringFormat('{0}', c));
                b.append(r);
            });

            _U.AlertSuccess('Se encontraron ' + d.length + ' registros.');
        });
    }

    function LoadE(idPadre) {
        var b = GetRowData(),
            p = {
                Entity: 'ProyFase',
                IdProyIniciativa: b.IdProyIniciativa,
                IdPadre: idPadre
            };

        _C.divE.html('');
        _U.Ajax('Proyectos', 'ObtenerHito', { Parametros: p }, function (data) {
            var d = JSON.parse(data.Datos);

            $.each(d, function (i, v) {
                var f = '<div class="col-sm-6 col-md-4"><div class="form-group form-material floating"><div class="input-group"><span class="input-group-addon"><i class="icon md-calendar" aria-hidden="true"></i></span><input type="text" id="dtp{0}" class="form-control" data-plugin="datepicker" value="{3}" readonly><label for="dtp{0}" class="floating-label">{2}) {1}:</label></div></div></div>',
                    t = '<div class="col-sm-6 col-md-4"><div class="form-group form-material floating"><div class="input-group"><input type="checkbox" id="chkAddETO" style="height: 1.5rem !important; width: 1.5rem;margin-right: .5rem;" value="" disabled ><label for="chkAddETO" class="form-check-label">Concurso de pre-inversión, incluía ETO</label></div></div></div>';

                _C.divE.append(_U.StringFormat(f, v.IdProyFase, v.Fase, v.IdOrden, v.Fecha));
                if (v.IdProyFase == 73) _C.divE.append(t);
                $('#dtp' + v.IdProyFase).datepicker({ startDate: '01/01/2000', endDate: '0d' }).change(dtpAll_change);
                _C.divE.data('DateMin', v.DateMin);
                _C.divE.data('DateMax', v.DateMax);
                //$('#dtp' + v.IdProyFase).datepicker({ startDate: v.DateMin, endDate: v.DateMax }).change(dtpAll_change);
            });
        });
    }

    function LoadP() {
        var a = +_C.lblImpYear.data('year'),
            n = +_O.ListImp.reduce((p, s) => +s.IdPeriodo > p ? p : +s.IdPeriodo, 999999),
            x = +_O.ListImp.reduce((p, s) => +s.IdPeriodo < p ? p : +s.IdPeriodo, 0);

        for (var i = 1; i <= 12; i++) {
            var m = $('#txtP' + i),
                p = a * 100 + i,
                o = _O.ListImp.find(s => +s.IdPeriodo == +p);

            if (!o) {
                if (p <= x && p >= n) m.val(0).removeClass('empty');
                else m.val('').addClass('empty');
            } else {
                m.val(o.Importe).removeClass('empty');
            }
        }
    }

    function EditarRegistro() {
        var a = GetRowData(),
            p = {
                IdProyFase: a.IdProyFase
            };

        _C.modFilterTitle.html(_U.StringFormat('Edición de la iniciativa:&nbsp;&nbsp;<span>{0}</span>', a.NroIniciativa));
        _C.txtComent.val(a.Comentario);

        _U.Ajax('Proyectos', 'ObtenerNivelesFase', { Parametros: p }, function (data) {
            var d = JSON.parse(data.Datos);

            if (+_S.IdEmpresa > 0) {
                LoadE(0);
                _C.txtA.val(d[0].Fase);
                _C.txtB.val(d[1].Fase);
                _C.txtC.val(d[2].Fase);
                _C.txtD.val(d[3].Fase);
            } else {
                _cmbB.Value = d[1].IdProyFase;
                _cmbC.Value = d[2].IdProyFase;
                _cmbD.Value = d[3].IdProyFase;
                _cmbA.SetValue(d[0].IdProyFase);
            }
        });
    }

    function EditarImporte() {
        var a = GetRowData(),
            p = {
                IdProyIniciativa: (a || {}).IdProyIniciativa,
                IdProyPrograma: _cmbProgram.GetEntidad().Id
            };

        if (!p.IdProyIniciativa) return;

        _C.modProgramTitle.html(_U.StringFormat('Edición de la iniciativa:&nbsp;&nbsp;<span>{0}</span>', a.NroIniciativa));
        _C.txtImpIni.val(a.Importe.toLocaleString());
        _C.txtImpAcu.val(a.ImporteAcum.toLocaleString());
        _C.txtImpMen.val(a.ImporteMenor.toLocaleString());

        _O.ListImp = [];
        _U.Ajax('Proyectos', 'ObtenerImporte', { Parametros: p }, function (data) {
            var d = JSON.parse(data.Datos);

            _O.ListImp = d;

            ShowYear();
            LoadP();
            ValidTypeProgram();
            SumImporte();
        });

        //var f = '<div class="col-sm-6 col-md-4"><div class="form-group form-material floating"><div class="input-group"><span class="input-group-addon">S/</span><div class="form-control-wrap"><input id="txtP{0}" type="number" class="form-control" value="{2}" min="0"><label for="txtP{0}" class="floating-label">{1}:</label></div><span class="input-group-btn"><button class="btn btn-info waves-effect waves-light btn-sm" type="button"><i class="icon md-alarm-snooze" aria-hidden="true"></i></button></span></div></div></div>'
        //_C.divP.append(_U.StringFormat(f, i + 1, _O.Months[i], (202000 +  i + 1).toFixed(0)));
    }

    function ShowYear(year) {
        year = year || (new Date()).getFullYear();

        _C.lblImpYear.data('year', year).html(year);
        _C.btnImpPrev.find('span span').html(year - 1);
        _C.btnImpNext.find('span span').html(year + 1);
    }

    function SumImporte() {
        var s = _O.ListImp.reduce((v, p) => v + +p.Importe, 0);

        _C.txtImpTotal.val(s.toLocaleString());
    }

    function SetImporte(idPeriodo, importe) {
        var i = _O.ListImp.findIndex(s => +s.IdPeriodo == +idPeriodo);

        if (i > -1) {
            _O.ListImp[i].Importe = +importe;
        } else {
            _O.ListImp.push({
                IdPeriodo: +idPeriodo,
                Importe: +importe
            });
        }
    }

    function GetListHitos() {
        var r = [],
            l = _C.divE.find('input[id*=dtp]');

        $.each(l, function (i, v) {
            var a = $(v),
                b = [],
                e = {
                    IdProyFase: +a.attr('id').replace('dtp', ''),
                    Fecha: a.val()
                };

            if (e.Fecha != '') {
                b = e.Fecha.split('/');
                e.Fecha = b[2] + b[1] + b[0]
            }

            r.push(e)
        });

        return r;
    }

    function GetLastHito() {
        var r = GetRowSelected(),
            d = GetRowData(),
            p = {
                IdProyIniciativa: d.IdProyIniciativa
            };

        _U.Ajax('Proyectos', 'ObtenerIniciativa', { Parametros: p }, function (data) {
            var d = JSON.parse(data.Datos),
                a = GetRowData();

            r.find('td').eq(6).find('span').html(d.Fase);
            r.find('td').eq(7).html(d.FechaInicia);
            r.find('td').eq(9).html(d.Comentario);
            r.data('entity', $.extend(true, a, d));
        });
    }

    function GetRowSelected() {
        var r = _C.tblResult.find('tr.row-select');

        return r;
    }

    function GetRowData() {
        var r = GetRowSelected().data('entity');

        return r;
    }

    function ValidTypeProgram() {
        var t = +_cmbProgram.GetEntidad().Id,
            a = +_C.lblImpYear.data('year'),
            b = _C.divP.find('button').parent(),
            d = new Date(),
            y = d.getFullYear(),
            m = d.getMonth();

        if (t == 1) {
            b.css('display', 'none');
            _C.divP.find('input').prop('disabled', true);

            if (m == 0) {
                m = 12;
                y -= 1;
            }

            if (a == y) _C['txtP' + m].prop('disabled', false);
        }
        else {
            b.css('display', 'table-cell');
            _C.divP.find('input').prop('disabled', false);
        }
    }

    function SetRowSelected(control) {
        _C.tblResult.find('tr').removeClass('row-select');
        control.closest('tr').addClass('row-select');
    }

    function ColCoordShow(control) {
        ColCoordCancel();
        SetRowSelected(control);

        var r = GetRowData(),
            p = control.closest('td');

        FillcmbCoord(r.IdEmpresa, +r.IdProyCoordinador);
        p.html('').append(_C.cmbCoord);

        _C.cmbCoord.find('#cmbCoord_Combo').focus();
    }

    function ColCoordCancel() {
        if (_C.cmbCoord.parent().attr('Id') == 'divHidden') return;

        var d = GetRowData(),
            p = _C.cmbCoord.closest('td'),
            t = '<a href="javascript:void(0)" class="text-info">{0}</a>';

        _C.divHidden.append(_C.cmbCoord);
        p.html(_U.StringFormat(t, d.Coordinador));
    }

    function EditarIniciativa(param) {
        _U.Ajax('Proyectos', 'EditarIniciativa', { Parametros: param }, function (data) {
            var d = JSON.parse(data.Datos);
        });
    }

    function FillcmbCoord(idEmpresa, value) {
        if (_cmbCoord.Parametros.IdEmpresa == idEmpresa) {
            _cmbCoord.SetValue(value);
            return;
        }

        _cmbCoord.Clear();
        _cmbCoord.Value = +value;
        _cmbCoord.Parametros.IdEmpresa = idEmpresa;

        _cmbCoord.Fill();
    }

    //#endregion Private

    //#region Public

    (function () { Load(); })();

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Proyectos', 'RegistrarFecha'), jQuery));