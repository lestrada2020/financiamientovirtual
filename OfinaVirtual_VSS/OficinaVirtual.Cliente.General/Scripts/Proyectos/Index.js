﻿/*
    Creación        : 2020-08-20
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C = {};
    var _U = window.nsUtil;

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (f) {
            window.Site.run();
            _C = _U.GetControlsJQ();
        });
    }

    //#endregion Private

    //#region Public

    (function () { Load(); })();

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Proyectos', 'Index'), jQuery));