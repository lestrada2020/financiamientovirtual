﻿/// <reference path="../../content/template/vendor/jquery/jquery.min.js" />
/// <reference path="../shared/util.js" />
/*
    Creación        : 2020-08-20
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C = {};
    var _U = window.nsUtil;

    var _cmbProgram;

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (f) {
            window.Site.run();
            _C = _U.GetControlsJQ();
            _S = _U.GetSession();

            ConfigComboBox();
            ConfigButton();

            if (+_S.IdEmpresa > 0) {
                _C.btnExportInic.remove();
                _C.btnExportImp.remove();
            }
        });
    }

    //#region Config

    function ConfigComboBox() {
        _cmbProgram = _U.SetConfigComboBox('cmbProgram');
    }

    function ConfigButton() {
        _C.btnExportImp.on('click', btnExportImp_click);
    }

    //#endregion Config

    //#region Event

    function btnExportImp_click() {
        var a = _cmbProgram.GetEntidad(),
            u = _U.StringFormat('{0}/{1}', _U.UrlAction('ExportarImporte', 'Proyectos'), +a.Id);

        window.open(u, '_blank');
    }

    //#endregion Event

    //#endregion Private

    //#region Public

    (function () { Load(); })();

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Proyectos', 'Exportar'), jQuery));