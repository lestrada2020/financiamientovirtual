﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OficinaVirtual.Cliente.WebApp.Controllers
{
    public class AccountEmpresaController : Controller
    {
        #region Fields

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly String _TokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];

        #endregion Fields

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            ViewBag.Mensaje = "";
            if (Session["IdUsuario"] != null)
                return RedirectToAction("MenuClienteEmpresa", "AccountEmpresa");
            else
                return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(clsLogin clsLog)
        {
            ViewBag.Exito = false;
            ViewBag.Mensaje = "";
            if (!ModelState.IsValid)
            {
                return View(clsLog);
            }
            try
            {
                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["usuario"] = clsLog.usuario;
                param.Parametros["clave"] = clsLog.clave;
                param.Parametros["ruc"] = clsLog.ruc;
                var result = await clsAccountEmpresa.ValidaAccesoClienteEmpresa(param);
                string IdUsuario = "", UsuarioLogin = "", Token = "";
                string FechaUltimoAcceso = "", datoscontroltag = "";
                var objresult = _Convert.ToJson(result.Datos);
                JArray objresultparse = JArray.Parse(objresult);
                foreach (var item in objresultparse)
                {
                    IdUsuario = item["IdUsuario"].ToString();
                    UsuarioLogin = item["UsuarioLogin"].ToString();
                    Token = item["Token"].ToString();
                    FechaUltimoAcceso = item["FechaUltimoAcceso"].ToString();
                    datoscontroltag = item["datoscontroltag"].ToString();
                }
                if (IdUsuario != "0" && IdUsuario != null)
                {
                    clsUsuarioEmpresaLoginAcceso acceso = new clsUsuarioEmpresaLoginAcceso();
                    acceso.LoginAcceso = new Dictionary<String, String>();
                    acceso.LoginAcceso["IdUsuario"] = IdUsuario;
                    acceso.LoginAcceso["Clave"] = clsLog.clave;
                    acceso.LoginAcceso["Correo"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "correo", "0");
                    acceso.LoginAcceso["NombreUsuario"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "nombreUsuario", "0");
                    acceso.LoginAcceso["Telefono"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "telefono", "0");
                    acceso.LoginAcceso["Nombres"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "Nombres", "0");
                    acceso.LoginAcceso["NroIdentidad"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "NroIdentidad", "0");
                    acceso.LoginAcceso["Cliente"] = clsDatosConstantes.Cliente.Empresa;
                    acceso.LoginAcceso["ruc"] = clsLog.ruc;
                    acceso.LoginAcceso["Empresa"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "Empresa", "0");
                    acceso.LoginAcceso["EmpresaNro"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "EmpresaNro", "0");
                    acceso.LoginAcceso["EmpresaDireccion"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "EmpresaDireccion", "0");
                    acceso.LoginAcceso["rol"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "rol", "0");
                    acceso.LoginAcceso["rolOpciones"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "rolOpciones", "0");
                    acceso.LoginAcceso["rolIconos"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "rolIconos", "0");
                    acceso.LoginAcceso["rolNombres"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "rolNombres", "0");
                    Session["IdUsuario"] = IdUsuario;
                    //Session["UsuarioUltimo"] = UsuarioLogin;
                    //Session["Clave"] = modelLogin.clave;
                    //Session["Token"] = Token;
                    //Session["FechaUltimoAcceso"] = FechaUltimoAcceso;
                    Session["ControlTagCredenciales"] = datoscontroltag;     
                    Session["NombreUsuario"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "nombreUsuario", "0");

                    Session["Telefono"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "telefono", "0");
                    Session["Correo"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "correo", "0");
                    Session["Nombres"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "Nombres", "0");
                    Session["NroIdentidad"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "NroIdentidad", "0");
                 

                    //Session["NtfEmision"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "ntfEmi", "0");
                    //Session["NtfVencimiento"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "ntfVcto", "0");
                    //Session["NtfCorte"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "ntfCr", "0");
                    //Session["NtfInterrupcion"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "ntfInt", "0");
                    //Session["UrlBase"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "urlbase", "0");
                    Session["ApPaterno"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "ApPaterno", "");
                    Session["ApMaterno"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "ApMaterno", "0");
                    Session["IdTipoIdentidad"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "IdTipoIdentidad", "0");
                    //Session["urlterminos"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "urlterminos", "0");
                    //Session["urlDevolucion"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "urlDevolucion", "0");
                    Session["Cliente"] = clsDatosConstantes.Cliente.Empresa;
                    //Session["rol"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "rol", "0");
                    //Session["rolOpciones"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "rolOpciones", "0");
                    Session["Empresa"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "Empresa", "0");
                    Session["EmpresaNro"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "EmpresaNro", "0");
                    Session["EmpresaDireccion"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "EmpresaDireccion", "0");

                    Session["EmpresaCorreo"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "EmpresaCorreo", "0");
                    Session["EmpresaTelefono"] = clsHelperControlTag.ObtenerValor(datoscontroltag, "EmpresaTelefono", "0");

                    Session["Global"] = acceso;
                    ViewBag.Exito = true;
                    ViewBag.Mensaje = clsHelperControlTag.ObtenerValor(datoscontroltag, "mensaje", "0");
                    return RedirectToAction("MenuClienteEmpresa", "AccountEmpresa");
                }
                else
                {
                    ViewBag.Mensaje = clsHelperControlTag.ObtenerValor(datoscontroltag, "mensaje", "0");
                    ModelState.Clear();
                    return View(clsLog);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = ex.Message; //"Error al validar usuario. Intente nuevamente"; //ModelState.AddModelError("clave", "Error al validar usuario. Intente nuevamente.");
                return View(clsLog);
            }
        }

        public ActionResult MenuClienteEmpresa()
        {
            if (Session["IdUsuario"] != null)
            {
                if (Session["Global"] != null)
                {
                    ViewBag.Global = Session["Global"];
                }
                else
                {
                    ViewBag.LoginAcceso = "";
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login", "AccountEmpresa");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> _UsuarioEmpresa()
        {
            try
            {
                 clsResultadoListaMaestros oTipoDocumento = null;
                oTipoDocumento = await clsConsulta.ListarMaestros2(clsConsulta.TipoMaestroEnum.TipoDocumento);
                if (oTipoDocumento != null)
                {
                    ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");
                    ViewBag.Mensaje = string.Empty;
                }
                else
                {
                    ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
                }
            }
            catch
            {
                ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
            }
            ViewBag.Exito = false;
            ViewBag.Nombres = string.Empty;
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _UsuarioEmpresa(_clsUsuarioEmpresa _clsUsuEmp)
        {
            clsResultadoListaMaestros oTipoDocumento = new clsResultadoListaMaestros();
            oTipoDocumento = await clsConsulta.ListarMaestros2(clsConsulta.TipoMaestroEnum.TipoDocumento);
            ViewBag.Exito = false;
            ViewBag.Mensaje = "";
            ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");

            //if (!ModelState.IsValid)
            //{
            //    return PartialView(_clsUsuEmp);
            //}

            try
            {
                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["empresaRuc"] = _clsUsuEmp.empresaRuc;
                param.Parametros["nombres"] = _clsUsuEmp.nombres;
                param.Parametros["apaterno"] = _clsUsuEmp.apaterno;
                param.Parametros["amaterno"] = _clsUsuEmp.amaterno;
                param.Parametros["IdTipoIdentidad"] = _clsUsuEmp.IdTipoIdentidad;
                param.Parametros["nrodocumento"] = _clsUsuEmp.nrodocumento;
                param.Parametros["correo"] = _clsUsuEmp.correo;
                param.Parametros["usuario"] = _clsUsuEmp.usuario;
                param.Parametros["clave"] = _clsUsuEmp.clave;
                param.Parametros["idnroservicio"] = _clsUsuEmp.idnroservicio;
                param.Parametros["nrorecibo"] = _clsUsuEmp.nrorecibo;
                var result = await clsAccountEmpresa.RegistrarClienteEmpresaAdmin(param);
                var error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "0");
                var mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");
                if (error == "0")
                {
                    ViewBag.Exito = true;
                    ViewBag.Mensaje = mensaje;
                    ModelState.Clear();
                    return PartialView();
                }
                else
                {
                    ViewBag.Mensaje = mensaje;
                    return PartialView(_clsUsuEmp);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = ex.Message;
                return PartialView(_clsUsuEmp);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> BuscarPorIdentidad(clsBuscarPorIdentidad clsBusPorIde) {
            try
            {
                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["idtipoidentidad"] = clsBusPorIde.idtipoidentidad;
                param.Parametros["nroidentidad"] = clsBusPorIde.nroidentidad;
                var result = await clsAccountEmpresa.BuscarPorIdentidad(param);
                result.Datos = _Convert.ToObject<List<Dictionary<String, String>>>(result.Datos.ToString());
                return Json(result.Datos);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult _Logout()
        {
            ViewBag.Mensaje = "";
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            Session.Remove("IdUsuario");
            //Session.Remove("UsuarioUltimo");
            //Session.Remove("Clave");
            //Session.Remove("Token");
            //Session.Remove("FechaUltimoAcceso");
            //Session.Remove("ControlTagCredenciales");
            Session.Remove("Correo");
            //Session.Remove("NombreUsuario");
            Session.Remove("Telefono");
            //Session.Remove("NtfEmision");
            //Session.Remove("NtfVencimiento");
            //Session.Remove("NtfCorte");
            //Session.Remove("NtfInterrupcion");
            //Session.Remove("UrlBase");
            //Session.Remove("ApPaterno");
            //Session.Remove("ApMaterno");
            Session.Remove("Nombres");
            //Session.Remove("urlterminos");
            //Session.Remove("urlDevolucion");
            Session.Remove("NroIdentidad");
            Session.Remove("Cliente");
            Session.Remove("Global");
            Session.Abandon();
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            HttpCookie oCookie = new HttpCookie("IdUsuario");
            oCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(oCookie);
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Login", "AccountEmpresa");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> RegistrarUsuarios(bool exito = false, string mensaje = "")
        {
            if (Session["IdUsuario"] != null)
            {
                ViewBag.Exito = exito;
                ViewBag.Mensaje = mensaje;
                ViewBag.Global = Session["Global"];
                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["nombre"] = "";
                param.Parametros["correo"] = "";
                param.Parametros["usuario"] = "";
                foreach (var item in ViewBag.Global.LoginAcceso)
                {
                    if (item.Key == "ruc")
                    {
                        param.Parametros["empresaClienteRUC"] = item.Value;
                    }
                }
                param.Parametros["estado"] = "";
                param.Parametros["idClienteEmpresa"] = "";
                var result = await clsAccountEmpresa.ListarClienteEmpresa(param);
                var objresult = _Convert.ToJson(result.Datos);
                JArray objresultparse = JArray.Parse(objresult);
                ViewBag.ListarClienteEmpresa = objresultparse;
                return View();
            }
            else
                return RedirectToAction("Login", "AccountEmpresa");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegistrarUsuarios(clsRegistrarUsuarios clsRegUsu)
        {
            if (Session["IdUsuario"] != null)
            {
                ViewBag.Exito = false;
                ViewBag.Mensaje = "";
                try
                {
                    ViewBag.Global = Session["Global"];
                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<String, String>();
                    param.Parametros["nombre"] = (clsRegUsu.nombre == null) ? clsRegUsu.nombre = "" : clsRegUsu.nombre;
                    param.Parametros["correo"] = (clsRegUsu.correo == null) ? clsRegUsu.correo = "" : clsRegUsu.correo;
                    param.Parametros["usuario"] = (clsRegUsu.usuario == null) ? clsRegUsu.usuario = "" : clsRegUsu.usuario;
                    foreach (var item in ViewBag.Global.LoginAcceso)
                    {
                        if (item.Key == "ruc")
                        {
                            param.Parametros["empresaClienteRUC"] = item.Value;
                        }
                    }
                    param.Parametros["estado"] = (clsRegUsu.estado == null) ? clsRegUsu.estado = "" : clsRegUsu.estado;
                    param.Parametros["idClienteEmpresa"] = "";
                    var result = await clsAccountEmpresa.ListarClienteEmpresa(param);
                    if (result.IdError == 0)
                    {
                        var objresult = _Convert.ToJson(result.Datos);
                        JArray objresultparse = JArray.Parse(objresult);
                        ViewBag.ListarClienteEmpresa = objresultparse;
                        //ModelState.Clear();
                        return View();
                    }
                    else
                    {
                        return View(clsRegUsu);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Mensaje = ex.Message; 
                    return View(clsRegUsu);
                }
            }
            else
                return RedirectToAction("Login", "AccountEmpresa");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> NuevoUsuarioEmpresa()
        {
            if (Session["IdUsuario"] != null)
            {
                try
                {

                    ViewBag.Global = Session["Global"];
                    foreach (var item in ViewBag.Global.LoginAcceso)
                    {
                        if (item.Key == "Empresa")
                        {
                            ViewBag.Empresa = item.Value;
                        }
                    }

                    clsResultadoListaMaestros oTipoDocumento = null;
                    oTipoDocumento = await clsConsulta.ListarMaestros2(clsConsulta.TipoMaestroEnum.TipoDocumento);

                    clsParametro param_ = new clsParametro();
                    param_.Parametros = new Dictionary<String, String>();
                    var oRoles = await clsAccountEmpresa.ListarRoles(param_);
                    var oTipoRelacion = await clsAccountEmpresa.TipoRelacion(param_);
                    if (oTipoDocumento != null)
                    {
                        if (oRoles.IdError == 0)
                        {
                            var oRolesResult = _Convert.ToJson(oRoles.Datos);
                            JArray oRolesResultParse = JArray.Parse(oRolesResult);

                            if (oTipoRelacion.IdError == 0)
                            {
                                var oTipoRelacionResult = _Convert.ToJson(oTipoRelacion.Datos);
                                JArray oTipoRelacionParse = JArray.Parse(oTipoRelacionResult);
                                ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");
                                ViewBag.Roles = oRolesResultParse;
                                ViewBag.oTipoRelacion = oTipoRelacionParse;
                                ViewBag.Mensaje = string.Empty;
                            }
                            else
                            {
                                ViewBag.Mensaje = "Se produjo un error al cargar el tipo de relación.";
                            }
                        }
                        else
                        {
                            ViewBag.Mensaje = "Se produjo un error al cargar los roles.";
                        }
                    }
                    else
                    {
                        ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
                    }
                }
                catch
                {
                    ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
                }
                ViewBag.Exito = false;
                return View();
            }
            else
                return RedirectToAction("Login", "AccountEmpresa");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> NuevoUsuarioEmpresa(clsNuevoUsuarioEmpresa clsNueUsuEmp)
        {
            if (Session["IdUsuario"] != null)
            {

                clsResultadoListaMaestros oTipoDocumento = null;
                oTipoDocumento = await clsConsulta.ListarMaestros2(clsConsulta.TipoMaestroEnum.TipoDocumento);
                ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");

                clsParametro param_ = new clsParametro();
                param_.Parametros = new Dictionary<String, String>();
                var oRoles = await clsAccountEmpresa.ListarRoles(param_);
                var oRolesResult = _Convert.ToJson(oRoles.Datos);
                JArray oRolesResultParse = JArray.Parse(oRolesResult);
                ViewBag.Roles = oRolesResultParse;

                var oTipoRelacion = await clsAccountEmpresa.TipoRelacion(param_);
                var oTipoRelacionResult = _Convert.ToJson(oTipoRelacion.Datos);
                JArray oTipoRelacionParse = JArray.Parse(oTipoRelacionResult);
                ViewBag.oTipoRelacion = oTipoRelacionParse;

                ViewBag.Exito = false;
                ViewBag.Mensaje = "";
                if (!ModelState.IsValid)
                {
                    return View(clsNueUsuEmp);
                }
                try
                {
                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<String, String>();
                    param.Parametros["IdTipoIdentidad"] = clsNueUsuEmp.tipdoc;
                    param.Parametros["nrodocumento"] = clsNueUsuEmp.numdoc;
                    param.Parametros["nombres"] = clsNueUsuEmp.nombre;
                    param.Parametros["apaterno"] = clsNueUsuEmp.apaterno;
                    param.Parametros["amaterno"] = clsNueUsuEmp.amaterno;
                    param.Parametros["correo"] = clsNueUsuEmp.correo;
                    param.Parametros["cargoFuncion"] = clsNueUsuEmp.carfun;
                    param.Parametros["IdTipoRepresentanteLegal"] = clsNueUsuEmp.tiprep;
                    param.Parametros["NroRepresentanteLegal"] = clsNueUsuEmp.numrep;
                    ViewBag.Global = Session["Global"];
                    foreach (var item in ViewBag.Global.LoginAcceso)
                    {
                        if (item.Key == "ruc")
                        {
                            param.Parametros["empresaRuc"] = item.Value;
                        }
                    }
                    param.Parametros["Rol"] = clsNueUsuEmp.rolapl;
                    param.Parametros["usuario"] = clsNueUsuEmp.usuario;
                    param.Parametros["clave"] = clsNueUsuEmp.clave;
                    param.Parametros["estado"] = clsNueUsuEmp.estado;
                    var result = await clsAccountEmpresa.RegistrarClienteEmpresa(param);
                    if (result.IdError == 0)
                    {
                        string mensaje = "";
                        string error = "";
                        mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");
                        error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "1");
                        if (error == "1")
                        {
                            ViewBag.Exito = false;
                            ViewBag.Mensaje = mensaje;
                        }
                        else {
                            ViewBag.Exito = true;
                            ViewBag.Mensaje = mensaje;
                        }
                        ModelState.Clear();
                        return View();
                    }
                    else
                    {
                        ViewBag.Mensaje = result.Mensaje;
                        return View(clsNueUsuEmp);
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.Mensaje = ex.Message;
                    return View(clsNueUsuEmp);
                }
            }
            else
                return RedirectToAction("Login", "AccountEmpresa");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> EditarUsuarioEmpresa(string idClienteEmpresa, clsEditarUsuarioEmpresa clsEmpEdiUsuEmp)
        {
            if (Session["IdUsuario"] != null)
            {
                try
                {
                    ViewBag.Global = Session["Global"];

                    clsResultadoListaMaestros oTipoDocumento = null;
                    oTipoDocumento = await clsConsulta.ListarMaestros2(clsConsulta.TipoMaestroEnum.TipoDocumento);

                    clsParametro param_ = new clsParametro();
                    param_.Parametros = new Dictionary<String, String>();
                    var oTipoRelacion = await clsAccountEmpresa.TipoRelacion(param_);
                    if (oTipoDocumento != null)
                    {
                        if (oTipoRelacion.IdError == 0)
                        {
                            var oTipoRelacionResult = _Convert.ToJson(oTipoRelacion.Datos);
                            JArray oTipoRelacionParse = JArray.Parse(oTipoRelacionResult);
                            ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");
                            ViewBag.oTipoRelacion = oTipoRelacionParse;

                            clsParametro param = new clsParametro();
                            param.Parametros = new Dictionary<String, String>();
                            param.Parametros["nombre"] = "";
                            param.Parametros["correo"] = "";
                            param.Parametros["usuario"] = "";
                            param.Parametros["empresaClienteRUC"] = "";
                            param.Parametros["estado"] = "";
                            param.Parametros["idClienteEmpresa"] = idClienteEmpresa;
                            var result = await clsAccountEmpresa.ListarClienteEmpresa(param);
                            var objresult = _Convert.ToJson(result.Datos);
                            JArray objresultparse = JArray.Parse(objresult);

                            foreach (var item in objresultparse)
                            {
                                clsEmpEdiUsuEmp.empresa = item["NombreCliente"].ToString();
                                clsEmpEdiUsuEmp.nombre = item["Nombres"].ToString();
                                clsEmpEdiUsuEmp.apaterno = item["ApellidoPaterno"].ToString();
                                clsEmpEdiUsuEmp.amaterno = item["ApellidoMaterno"].ToString();
                                clsEmpEdiUsuEmp.correo = item["Correo"].ToString();
                                clsEmpEdiUsuEmp.usuario = item["Usuario"].ToString();
                                clsEmpEdiUsuEmp.carfun = item["IdCargo"].ToString();
                                string RepresentanteLegal = item["RepresentanteLegal"].ToString();

                                if (RepresentanteLegal != "")
                                {
                                string[] tiprep = RepresentanteLegal.Split(':');
                                string[] numrep = RepresentanteLegal.Split(':');
                                clsEmpEdiUsuEmp.tiprep = tiprep[0].ToString();
                                clsEmpEdiUsuEmp.numrep = numrep[2].ToString();
                                }

                                foreach (var itemglobal in ViewBag.Global.LoginAcceso)
                                {
                                    if (itemglobal.Key == "Clave")
                                    {
                                        clsEmpEdiUsuEmp.clave = Convert.ToString(itemglobal.Value);
                                        clsEmpEdiUsuEmp.concla = Convert.ToString(itemglobal.Value);
                                    }
                                }
                                clsEmpEdiUsuEmp.estado = item["Estado"].ToString();
                                clsEmpEdiUsuEmp.idusuario = idClienteEmpresa.ToString();
                            }
                            ViewBag.Mensaje = string.Empty;
                        }
                        else
                        {
                            ViewBag.Mensaje = "Se produjo un error al cargar el tipo de relación.";
                        }
                    }
                    else
                    {
                        ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
                    }
                }
                catch
                {
                    ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
                }
                ViewBag.Exito = false;
                ModelState.Clear();
                return View(clsEmpEdiUsuEmp);
            }
            else
                return RedirectToAction("Login", "AccountEmpresa");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditarUsuarioEmpresa(clsEditarUsuarioEmpresa clsEdiUsuEmp)
        {
            if (Session["IdUsuario"] != null)
            {

                clsResultadoListaMaestros oTipoDocumento = null;
                oTipoDocumento = await clsConsulta.ListarMaestros2(clsConsulta.TipoMaestroEnum.TipoDocumento);
                ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");

                clsParametro param_ = new clsParametro();
                param_.Parametros = new Dictionary<String, String>();

                var oTipoRelacion = await clsAccountEmpresa.TipoRelacion(param_);
                var oTipoRelacionResult = _Convert.ToJson(oTipoRelacion.Datos);
                JArray oTipoRelacionParse = JArray.Parse(oTipoRelacionResult);
                ViewBag.oTipoRelacion = oTipoRelacionParse;

                ViewBag.Exito = false;
                ViewBag.Mensaje = "";
                if (!ModelState.IsValid)
                {
                    return View(clsEdiUsuEmp);
                }
                try
                {
                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<String, String>();
                    param.Parametros["nombres"] = clsEdiUsuEmp.nombre;
                    param.Parametros["apaterno"] = clsEdiUsuEmp.apaterno;
                    param.Parametros["amaterno"] = clsEdiUsuEmp.amaterno;
                    param.Parametros["correo"] = clsEdiUsuEmp.correo;
                    param.Parametros["cargoFuncion"] = clsEdiUsuEmp.carfun;
                    param.Parametros["idtiporepresentanteLegal"] = clsEdiUsuEmp.tiprep;
                    param.Parametros["NrorepresentanteLegal"] = clsEdiUsuEmp.numrep;
                    param.Parametros["clave"] = clsEdiUsuEmp.clave;
                    param.Parametros["estado"] = clsEdiUsuEmp.estado;
                    param.Parametros["IdUsuario"] = clsEdiUsuEmp.idusuario;
                    var result = await clsAccountEmpresa.EditarClienteEmpresa(param);

                    string mensaje = "";
                    string error = "";
                    mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");
                    error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "1");


                    if (error == "0")
                    {
                        ViewBag.Exito = true;
                        ViewBag.Mensaje = mensaje;
                       // ModelState.Clear();
                        return PartialView(clsEdiUsuEmp);
                    }
                    else
                    {
                        ViewBag.Mensaje = mensaje;
                        return PartialView(clsEdiUsuEmp);
                    }


                    //if (result.IdError == 0)
                    //{
                    //    string mensaje = "";
                    //    string error = "";
                    //    mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");
                    //    error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "1");
                    //    if (error == "1")
                    //    {
                    //        ViewBag.Exito = false;
                    //        ViewBag.Mensaje = mensaje;
                    //    }
                    //    else
                    //     {
                    //        ViewBag.Exito = true;
                    //        ViewBag.Mensaje = mensaje;
                    //    }
                    //    //ModelState.Clear();
                    //    return View();
                    //}
                    //else
                    //{
                    //    ViewBag.Mensaje = result.Mensaje;
                    //    return View(clsEdiUsuEmp);
                    //}

                }
                catch (Exception ex)
                {
                    ViewBag.Mensaje = ex.Message;
                    return View(clsEdiUsuEmp);
                }
            }
            else
                return RedirectToAction("Login", "AccountEmpresa");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> EliminarClienteEmpresa(string IdUsuario, clsEliminarClienteEmpresa clsEliCliEmp)
        {
            try
            {
                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                clsEliCliEmp.idusuario = IdUsuario.ToString();
                param.Parametros["IdUsuario"] = clsEliCliEmp.idusuario;
                param.Parametros["IdUser"] = "";
                var result = await clsAccountEmpresa.EliminarClienteEmpresa(param);
                string error = "";
                string mensaje = "";
                bool salida = false;
                if (result.IdError == 0)
                {
                    error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "1");
                    if (error == "1")
                    {
                        salida = false;
                        mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");
                    }
                    else
                    {
                        salida = true;
                        mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");
                    }
                }
                else
                {
                    mensaje = result.Mensaje;
                }
                clsParametro result_ = new clsParametro();
                result_.Parametros = new Dictionary<String, String>();
                result_.Parametros["errorPrin"] = result.IdError.ToString();
                result_.Parametros["error"] = error;
                result_.Parametros["mensaje"] = mensaje;
                result_.Parametros["salida"] = salida.ToString();
                return Json(result_.Parametros);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}