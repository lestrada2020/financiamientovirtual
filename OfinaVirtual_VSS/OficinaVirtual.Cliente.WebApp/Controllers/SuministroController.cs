﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using OficinaVirtual.Util.Tool;
using ReportManagement;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OficinaVirtual.Cliente.WebApp.Controllers
{
    public class SuministroController : PdfViewController
    {
        #region Fields

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsAccount _Account = clsAccount.Instancia;
        private static readonly clsAtencion _Atencion = clsAtencion.Instancia;
        private static readonly clsConsulta _Consulta = clsConsulta.Instancia;
        private static readonly clsRecaudacion _Recaudacion = clsRecaudacion.Instancia;
        private static readonly clsVisa _Visa = clsVisa.Instancia;
        private static readonly String _TokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];
        private static readonly String _IdProveedorVisa = ConfigurationManager.AppSettings["IdProveedorVisa"];

        #endregion Fields

        #region Private

        private async Task<clsResultado> ValidCaptcha(String response)
        {
            var r = new clsResultado();

            try
            {
                var a = await _Consulta.ValidCaptcha(response);
                var b = _Convert.ToObject<Dictionary<String, Object>>(a.Datos.ToString());
                var c = Convert.ToBoolean(b["success"]);

                if (c) r = null;
                else throw new Exception(b["error-codes"].ToString());
            }
            catch (Exception ex)
            {
                r.IdError = 1;
                r.Mensaje = "Favor de reintentar.";
                r.Datos = ex.Message;
            }

            return r;
        }

        #endregion Private

        public async Task<ActionResult> Resumen()
        {
            Session.Remove("Suministro");
            Session.Remove("Datos");
            Session.Remove("Paso");
            Session.Remove("NroPedido");
            Session.Remove("TipoMonto");
            Session.Remove("Monto");
            Session.Remove("SessionToken");
            Session.Remove("SessionKey");
            Session.Remove("TransactionToken");
            Session.Remove("CustomerEmail");
            Session.Remove("Mensaje");
            Session.Remove("Cerrar");

            if (Session["IdUsuario"] != null)
            {
                clsParametroConsulta parametro = new clsParametroConsulta();
                List<clsResumenSuministro> lsResSum = new List<clsResumenSuministro>();

                try
                {
                    parametro.ControlTagCredencial = $"Usuario|{Session["UsuarioUltimo"].ToString()};Token|{Session["Token"].ToString()}";
                    parametro.ParametroControlTag = $"idusuario|{Session["IdUsuario"].ToString()}";

                    lsResSum = await _Consulta.ListSuministroResumen(parametro);
                    Session["SuminAsoci"] = lsResSum.Select(s => s.NroSuministro).Distinct().ToList();

                    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL 
                    var lsResFin = new ClsListaFinanciamiento();
                    var param = new clsParametro() { Parametros = new Dictionary<String, String>() };
                    param.Parametros["IdUsuario"] = Session["IdUsuario"].ToString();
                    lsResFin = await _Atencion.ListarSuministroAsociado(param);
                    ViewBag.PuedeFinanciar = false;

                    #region Distriluz-036 Video
                    if (lsResFin.Datos != null)
                    {
                        foreach (var item in lsResFin.Datos)
                        {
                            if (item.PuedeFinanciar == 1)
                            {
                                ViewBag.PuedeFinanciar = true;
                                break;
                            }
                        }
                    }
                    #endregion                    
                    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL
                }
                catch
                {
                    throw;
                }
                return View(lsResSum);
            }
            else
                return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult _ConsultarSuministro()
        {
            if (Session["IdUsuario"] != null)
                return PartialView();
            else
                return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> _ConsultarSuministro(clsConsultaSuministro modDatos)
        {
            if (Session["IdUsuario"] != null)
            {
                clsConsultaSuministro oDatos;
                clsParametroConsulta oParametro = new clsParametroConsulta();

                if (!ModelState.IsValid)
                {
                    modDatos.Mensaje = "validar";
                    return PartialView(modDatos);
                }

                try
                {
                    StringBuilder sValores = new StringBuilder();
                    sValores.Append(string.Format("usuario|{0};", Session["UsuarioUltimo"]));
                    sValores.Append(string.Format("idnroservicio|{0};", $"{modDatos.Suministro}"));
                    sValores.Append(string.Format("nrorecibo|{0}", $"{modDatos.Recibo}"));

                    oParametro.ParametroControlTag = sValores.ToString();

                    oDatos = await _Consulta.ConsultarSuministro2(oParametro);

                    return PartialView(oDatos);
                }
                catch
                {
                    ViewBag.Mensaje = "Error al consultar el suministro. Intente nuevamente";
                    return PartialView(modDatos);
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> _RegistrarSuministro(clsConsultaSuministro modDatos)
        {
            if (Session["IdUsuario"] != null)
            {
                clsResultadoOperacion oDatos;
                clsParametroConsulta oParametro = new clsParametroConsulta();

                try
                {
                    StringBuilder sValores = new StringBuilder();
                    sValores.Append(string.Format("usuario|{0};", Session["UsuarioUltimo"]));
                    sValores.Append(string.Format("idnroservicio|{0}", $"{modDatos.Suministro}"));

                    oParametro.ParametroControlTag = sValores.ToString();

                    oDatos = await _Consulta.RegistrarSuministro(oParametro);

                    ViewBag.Exito = oDatos.Exito;
                    ViewBag.Mensaje = "Suministro agregado satisfactoriamente";
                    return PartialView("_ConsultarSuministro", modDatos);
                }
                catch
                {
                    ViewBag.Exito = false;
                    ViewBag.Mensaje = "Error al registrar el suministro. Intente nuevamente";
                    return PartialView("_ConsultarSuministro", modDatos);
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> Detalle(string Suministro)
        {
            if (Session["IdUsuario"] != null)
            {
                clsParametroConsulta oParametro = new clsParametroConsulta();
                clsDetalleSuministro oDatos = new clsDetalleSuministro();

                try
                {
                    oParametro.ParametroControlTag = $"idnroservicio|{Suministro}";
                    oDatos = await _Consulta.DetalleSuministro2(oParametro);
                }
                catch
                {
                    throw;
                }

                return View(oDatos);
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> _HistoricoConsumo(string Suministro)
        {
            clsParametroConsulta oParametro = new clsParametroConsulta();
            clsInformacion oDatos = new clsInformacion();

            try
            {
                oParametro.ParametroControlTag = $"idnroservicio|{Suministro}";
                oDatos = await _Consulta.HistoricoConsumo(oParametro);
            }
            catch
            {
                throw;
            }

            return PartialView(oDatos);
        }

        public async Task<ActionResult> _HistoricoFacturacion(string Suministro)
        {
            clsParametroConsulta oParametro = new clsParametroConsulta();
            clsInformacion oDatos = new clsInformacion();

            try
            {
                ViewBag.IdNroServicio = Suministro;
                oParametro.ParametroControlTag = $"idnroservicio|{Suministro}";
                oDatos = await _Consulta.HistoricoFacturacion(oParametro);
            }
            catch
            {
                throw;
            }

            return PartialView(oDatos);
        }

        public async Task<ActionResult> _UltimoRecibo(string Suministro, string Periodo)
        {
            clsParametroConsulta oParametro = new clsParametroConsulta();
            clsUltimoReciboCabecera oDatos = new clsUltimoReciboCabecera();

            try
            {
                oParametro.ParametroControlTag = $"idnroservicio|{Suministro};periodo|{Periodo}";
                oDatos = await _Consulta.UltimoReciboCabecera(oParametro);
                oDatos.Periodo = Periodo;
                oDatos.IdNroServicio = Suministro;
            }
            catch
            {
                throw;
            }

            return PartialView(oDatos);
        }

        public async Task<ActionResult> _HistoricoPago(string Suministro)
        {
            clsParametroConsulta oParametro = new clsParametroConsulta();
            List<clsUltimosPagos> oDatos = new List<clsUltimosPagos>();

            try
            {
                oParametro.ParametroControlTag = $"idnroservicio|{Suministro}";
                oDatos = await _Consulta.HistoricoPago(oParametro);
            }
            catch
            {
                throw;
            }

            return PartialView(oDatos);
        }

        public async Task<ActionResult> _CortesProgramados(string Suministro)
        {
            clsParametroConsulta oParametro = new clsParametroConsulta();
            List<clsCortesProgramados> oDatos = new List<clsCortesProgramados>();

            try
            {
                oParametro.ParametroControlTag = $"idnroservicio|{Suministro}";
                oDatos = await _Consulta.CortesProgramados(oParametro);
            }
            catch
            {
                throw;
            }

            return PartialView(oDatos);
        }

        public async Task<ActionResult> VisaConsulta(string Suministro)
        {
            var Terminos = 1;
            var sMensaje = (String)null;
            var idNroServicio = Session["Suministro"];

            if (Session["IdUsuario"] != null && (Suministro != null || idNroServicio != null))
            {
                dynamic oParamDatos = new JObject();

                clsParametroConsulta oParametro = new clsParametroConsulta();
                clsResultadoDatos oDatos = new clsResultadoDatos();
                clsParametroEntrada oParamRecaudacion = new clsParametroEntrada();
                clsResultadoRecaudacion oRptaConsulta = new clsResultadoRecaudacion();
                clsResultadoRecaudacion oRptaRecaudacion = new clsResultadoRecaudacion();
                clsParametroVisaAuthorizationResponse oPago = new clsParametroVisaAuthorizationResponse();
                clsEmpresasXUsuario oEmpresa = new clsEmpresasXUsuario();

                try
                {
                    if (idNroServicio == null)
                    {
                        oParamDatos.SessionToken = _TokenDistriluz;
                        oParamDatos.IdNroServicio = Suministro;
                        oParamRecaudacion.Parametros = oParamDatos;
                        oRptaConsulta = await _Recaudacion.SuministroConsultarDeuda(oParamRecaudacion);

                        if (oRptaConsulta.IdError == 0 && oRptaConsulta.Datos != null) oDatos = JsonConvert.DeserializeObject<clsResultadoDatos>(oRptaConsulta.Datos.ToString());

                        Terminos = 0;
                        Session["Suministro"] = Suministro;
                        Session["IdEmpresa"] = oDatos.IdEmpresa;
                        Session["TipoMonto"] = "1";
                        Session["Paso"] = "1";
                        Session["Datos"] = oDatos;
                        sMensaje = oRptaConsulta.Mensaje;
                    }
                    else if (Session["Paso"].ToString() == "3")
                    {
                        oParametro.ParametroControlTag = $"idusuario|{Session["IdUsuario"]};IdNroServicio|{idNroServicio}";
                        oEmpresa = await _Account.ListarEmpresasXUsuario(oParametro);

                        oParamDatos.SessionToken = _TokenDistriluz;
                        oParamDatos.NroPedido = Session["NroPedido"];
                        oParamDatos.SessionTokenVisa = Session["SessionKey"];
                        oParamRecaudacion.Parametros = oParamDatos;
                        oRptaRecaudacion = await _Recaudacion.SuministroConsultarPago(oParamRecaudacion);

                        if (oRptaRecaudacion.IdError == 0 && oRptaRecaudacion.Datos != null)
                            oPago = JsonConvert.DeserializeObject<clsParametroVisaAuthorizationResponse>(oRptaRecaudacion.Datos.ToString());
                    }

                    ViewBag.Terminos = Terminos;
                    ViewBag.Pago = oPago;
                    ViewBag.Empresa = oEmpresa;
                    ViewBag.Mensaje = sMensaje;

                    return View();
                }
                catch (Exception ex)
                {
                    return RedirectToAction("VisaFinalizar", "Suministro");
                }
            }
            else if (Session["IdUsuario"] != null && Suministro == null && idNroServicio == null)
                return RedirectToAction("VisaFinalizar", "Suministro");
            else return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> VisaValida(FormCollection oForm)
        {
            Session["TipoMonto"] = oForm["tipoMonto"];
            Session["Monto"] = oForm["rdMonto"];

            var idNroServicio = Session["Suministro"];
            var idEmpresa = Convert.ToInt16(Session["IdEmpresa"]);

            if (Session["IdUsuario"] != null && idNroServicio != null)
            {
                dynamic oParamAntiFraudeMerchant = new JObject();
                dynamic oParamAntiFraude = new JObject();
                dynamic oParamDatos = new JObject();

                var oParamRecaudacion = new clsParametroEntrada();
                var oRptaConsulta = new clsResultadoRecaudacion();
                var oRptaIntento = new clsResultadoRecaudacion();
                var oParamVisa = new clsParametroVisa();
                var oParamIntento = new clsParametroEntrada();
                var oDatos = (Dictionary<String, String>)null;

                try
                {
                    oParamAntiFraudeMerchant.MDD1 = "web";
                    oParamAntiFraudeMerchant.MDD2 = "Canl";
                    oParamAntiFraudeMerchant.MDD3 = "Canl";
                    oParamAntiFraudeMerchant.MDD32 = idNroServicio;
                    oParamAntiFraude.clientIp = Request.UserHostAddress;
                    oParamAntiFraude.merchantDefineData = oParamAntiFraudeMerchant;
                    oParamVisa.amount = Convert.ToDouble(Session["Monto"]);
                    oParamVisa.channel = "web";
                    oParamVisa.antifraud = oParamAntiFraude;

                    var amount = Convert.ToDecimal(Session["Monto"]);
                    var suministro = 0;
                    Int32.TryParse(idNroServicio == null ? "0" : idNroServicio.ToString(), out suministro);

                    Session["MerchantId"] = _Visa.GetMerchantId(idEmpresa, 2);
                    Session["URLButtonJS"] = _Visa.GetURLButtonJS(idEmpresa);
                    Session["SessionToken"] = await _Visa.CreateToken(idEmpresa, 2);

                    if (Session["SessionToken"] != null)
                    {
                        oDatos = await _Visa.CreateSession(idEmpresa, 2, Session["SessionToken"].ToString(), amount, Request.UserHostAddress, suministro);
                        Session["SessionKey"] = oDatos["sessionKey"];
                    }

                    if (Session["SessionToken"] != null && Session["SessionKey"] != null)
                    {
                        oParamDatos.SessionToken = _TokenDistriluz;
                        oParamDatos.IdNroServicio = idNroServicio;
                        oParamDatos.IdUsuario = Session["IdUsuario"];
                        oParamDatos.IdOrigen = "3";
                        oParamDatos.Importe = Session["Monto"];
                        oParamDatos.TipoImporte = Session["TipoMonto"];
                        oParamDatos.SessionTokenVisa = Session["SessionKey"];
                        oParamIntento.Parametros = oParamDatos;
                        oRptaIntento = await _Recaudacion.IntentoPagoGrabar(oParamIntento);

                        if (oRptaIntento.IdError == 0 && oRptaIntento.Datos != null)
                        {
                            Dictionary<String, String> oRptaIntentoDatos = JsonConvert.DeserializeObject<Dictionary<String, String>>(oRptaIntento.Datos.ToString());
                            Session["NroPedido"] = oRptaIntentoDatos["NroPedido"];
                            Session["Periodos"] = oRptaIntentoDatos["Periodos"];

                            oParamDatos.SessionToken = _TokenDistriluz;
                            oParamDatos.IdNroServicio = idNroServicio;
                            oParamRecaudacion.Parametros = oParamDatos;
                            oRptaConsulta = await _Recaudacion.SuministroConsultarDeuda(oParamRecaudacion);
                            ViewBag.Resultado = oRptaConsulta;

                            return View();
                        }
                        else
                        {
                            Session["Paso"] = "1";
                            Session["Mensaje"] = oRptaIntento.Mensaje;
                            return RedirectToAction("VisaConsulta", "Suministro");
                        }
                    }
                    else
                    {
                        Session["Paso"] = "1";
                        Session["Mensaje"] = "Se generó un error al autenticar con visa";
                        return RedirectToAction("VisaConsulta", "Suministro");
                    }
                }
                catch (Exception ex)
                {
                    Session["Paso"] = "1";
                    Session["Mensaje"] = "Se generó una excepción al validar con visa";

                    return RedirectToAction("VisaConsulta", "Suministro");
                }
            }
            else if (Session["IdUsuario"] != null && idNroServicio == null)
            {
                return RedirectToAction("VisaFinalizar", "Suministro");
            }
            else
                return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult VisaProcesando(FormCollection oForm)
        {
            Session["Paso"] = "2";
            Session["TransactionToken"] = oForm["transactionToken"];
            Session["CustomerEmail"] = oForm["customerEmail"];
            return RedirectToAction("VisaConsulta", "Suministro");
        }

        public async Task<ActionResult> VisaPago()
        {
            var isOK = true;
            var errorMessage = "";
            var idEmpresa = Convert.ToInt16(Session["IdEmpresa"]);

            if (Session["IdUsuario"] != null && Session["NroPedido"] != null && Session["SessionKey"] != null)
            {
                dynamic oParamDatos = new JObject();

                var oParamRecaudacion = new clsParametroEntrada();
                var oRptaRecaudacion = new clsResultadoRecaudacion();
                var oParamOrden = new clsParametroVisaAuthorizationOrden();
                var oParamVisa = new clsParametroVisaAuthorization();
                var oParamResponse = new clsParametroVisaAuthorizationResponse();
                var visaPayResponse = new clsResultado();
                try
                {
                    oParamRecaudacion.Parametros = oParamDatos;

                    #region Intento 1

                    oParamDatos.SessionToken = _TokenDistriluz;
                    oParamDatos.NroPedido = Session["NroPedido"];
                    oParamDatos.SessionTokenVisa = Session["SessionKey"];
                    oParamDatos.TransactionTokenVisa = Session["TransactionToken"];
                    oParamDatos.Email = Session["CustomerEmail"];
                    oParamDatos.JsonPagoVisa = null;
                    oParamDatos.JsonPagoResumen = null;
                    oParamDatos.IdPaso = "1";
                    oParamDatos.IdEstadoPaso = "1";
                    oParamDatos.MensajePaso = "Se validó correctamente el formulario visa";

                    oRptaRecaudacion = await _Recaudacion.IntentoPagoActualizar(oParamRecaudacion);

                    isOK = oRptaRecaudacion.IdError == 0;
                    errorMessage = oRptaRecaudacion.Mensaje;

                    #endregion Intento 1

                    if (isOK)
                    {
                        #region Pago Visa

                        oParamOrden.amount = Session["Monto"].ToString();
                        oParamOrden.currency = "PEN";
                        oParamOrden.purchaseNumber = Session["NroPedido"].ToString();
                        oParamOrden.tokenId = Session["TransactionToken"].ToString();
                        oParamVisa.antifraud = null;
                        oParamVisa.captureType = "manual";
                        oParamVisa.channel = "web";
                        oParamVisa.countable = "true";
                        oParamVisa.order = oParamOrden;
                        //oParamResponse = await clsVisa.SolicitarAutorizacion(idEmpresa, Session["SessionToken"].ToString(), oParamVisa);
                        //visaPayResponse = await clsVisa.SolicitarAutorizacion<clsResultado>(idEmpresa, Session["SessionToken"].ToString(), oParamVisa);
                        visaPayResponse = await _Visa.AuthorizeSession(idEmpresa, 2, Session["SessionToken"].ToString(), Convert.ToDecimal(Session["Monto"]), Convert.ToInt32(Session["NroPedido"]), Session["TransactionToken"].ToString());
                        oParamResponse.Mensaje = visaPayResponse.Mensaje;

                        isOK = oParamResponse.Mensaje == null;
                        errorMessage = oParamResponse.Mensaje;

                        if (isOK) oParamResponse = _Convert.ToObject<clsParametroVisaAuthorizationResponse>(visaPayResponse.Datos.ToString());

                        #endregion Pago Visa

                        #region Intento 2

                        oParamDatos.JsonPagoVisa = isOK ? visaPayResponse.Datos.ToString() : errorMessage;
                        oParamDatos.IdPaso = "2";
                        oParamDatos.IdEstadoPaso = isOK ? "1" : "0";
                        oParamDatos.MensajePaso = $"Se {(isOK ? "autorizó correctamente" : "denegó")} la transacción en visa";

                        #region Action Code

                        try
                        {
                            if (!isOK)
                            {
                                errorMessage = "Not Authorized";

                                var error1 = _Convert.ToObject<Dictionary<String, Object>>(oParamResponse.Mensaje);
                                if (error1.ContainsKey("errorMessage")) errorMessage = error1["errorMessage"].ToString();

                                var error2 = _Convert.ToObject<Dictionary<String, String>>(error1["data"].ToString());
                                if (error2.ContainsKey("ACTION_DESCRIPTION")) errorMessage = error2["ACTION_DESCRIPTION"];

                                var id = Convert.ToInt32(error2["ACTION_CODE"]);

                                var param = new Dictionary<String, String>();
                                param.Add("IdProveedor", _IdProveedorVisa);
                                param.Add("IdObservacion", id.ToString());
                                var res = await _Atencion.ObtenerObservacion(new clsParametro() { Parametros = param });

                                if (res.Datos != null)
                                {
                                    var res2 = _Convert.ToObject<Dictionary<String, String>>(res.Datos.ToString());
                                    if (res2.ContainsKey("MensajeCliente")) errorMessage = res2["MensajeCliente"];
                                    if (res2.ContainsKey("MensajeProveedor")) oParamDatos.MensajePaso = $"[{id}] - " + res2["MensajeProveedor"];
                                }
                            }
                        }
                        catch (Exception ex) { }

                        #endregion Action Code

                        try
                        {
                            oRptaRecaudacion = await _Recaudacion.IntentoPagoActualizar(oParamRecaudacion);
                        }
                        catch (Exception ex) { }

                        #endregion Intento 2

                        if (isOK)
                        {
                            #region Pago Distriluz

                            oParamDatos.SessionToken = _TokenDistriluz;
                            oParamDatos.NroPedido = Session["NroPedido"].ToString();
                            oParamDatos.SessionTokenVisa = Session["SessionKey"].ToString();

                            oRptaRecaudacion = await _Recaudacion.SuministroPagarDeuda(oParamRecaudacion);

                            isOK = oRptaRecaudacion.IdError == 0;
                            errorMessage = oRptaRecaudacion.Mensaje;

                            if (!isOK)
                            {
                                oRptaRecaudacion = await _Recaudacion.SuministroPagarDeuda(oParamRecaudacion);

                                isOK = oRptaRecaudacion.IdError == 0;
                                errorMessage = oRptaRecaudacion.Mensaje;
                            }

                            #endregion Pago Distriluz

                            #region Intento 3

                            if (isOK)
                            {
                                oParamDatos.JsonPagoResumen = _Convert.ToJson(oParamResponse);
                                oParamDatos.IdPaso = "3";
                                oParamDatos.IdEstadoPaso = "1";
                                oParamDatos.MensajePaso = "Se registró correctamente la transacción en distriluz";

                                try
                                {
                                    oRptaRecaudacion = await _Recaudacion.IntentoPagoActualizar(oParamRecaudacion);
                                }
                                catch (Exception ex) { }
                            }

                            #endregion Intento 3
                        }
                    }

                    Session["Paso"] = isOK ? "3" : "1";
                    Session["Mensaje"] = isOK ? "" : errorMessage;

                    return RedirectToAction("VisaConsulta", "Suministro");
                }
                catch (Exception ex)
                {
                    return RedirectToAction("VisaFinalizar", "Suministro");
                }
            }
            else if (Session["IdUsuario"] != null && Session["NroPedido"] == null && Session["SessionKey"] == null)
            {
                return RedirectToAction("VisaFinalizar", "Suministro");
            }
            else return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> VisaImpresion(string np = "", string sk = "")
        {
            dynamic oParamDatos = new Newtonsoft.Json.Linq.JObject();
            clsParametroConsulta oParametro = new clsParametroConsulta();
            clsParametroEntrada oParamRecaudacion = new clsParametroEntrada();
            clsResultadoRecaudacion oRecaudacion = new clsResultadoRecaudacion();
            clsParametroVisaAuthorizationResponse oPago = new clsParametroVisaAuthorizationResponse();
            clsEmpresasXUsuario oEmpresa = new clsEmpresasXUsuario();

            if (np != string.Empty && sk != string.Empty)
            {
                oParamDatos.SessionToken = _TokenDistriluz;
                oParamDatos.NroPedido = np;
                oParamDatos.SessionTokenVisa = sk;
                oParamRecaudacion.Parametros = oParamDatos;
                oRecaudacion = await _Recaudacion.SuministroConsultarPago(oParamRecaudacion);

                if (oRecaudacion.IdError == 0 && oRecaudacion.Datos != null)
                {
                    oPago = JsonConvert.DeserializeObject<clsParametroVisaAuthorizationResponse>(oRecaudacion.Datos.ToString());
                }
                ViewBag.Pago = oPago;
                ViewBag.Empresa = oEmpresa;
                return ViewPdf("ImpresionPagoVisa", "VisaImpresion", ViewBag);
            }
            else if (Session["IdUsuario"] != null && Session["NroPedido"] != null && Session["SessionKey"] != null)
            {
                oParamDatos.SessionToken = _TokenDistriluz;
                oParamDatos.NroPedido = Session["NroPedido"];
                oParamDatos.SessionTokenVisa = Session["SessionKey"];
                oParamRecaudacion.Parametros = oParamDatos;
                oRecaudacion = await _Recaudacion.SuministroConsultarPago(oParamRecaudacion);

                oParametro.ParametroControlTag = $"idusuario|{Session["IdUsuario"]};IdNroServicio|{Session["Suministro"]}";
                oEmpresa = await _Account.ListarEmpresasXUsuario(oParametro);

                if (oRecaudacion.IdError != 0)
                {
                    Session["Mensaje"] = "Se generó un error al cargar el PDF.";
                    return RedirectToAction("VisaConsulta", "Suministro");
                }
                else
                {
                    oPago = JsonConvert.DeserializeObject<clsParametroVisaAuthorizationResponse>(oRecaudacion.Datos.ToString());
                    ViewBag.Pago = oPago;
                    ViewBag.Empresa = oEmpresa;
                    return ViewPdf("ImpresionPagoVisa", "VisaImpresion", ViewBag);
                }
            }
            else if (Session["IdUsuario"] != null && Session["NroPedido"] == null && Session["SessionKey"] == null)
            {
                return RedirectToAction("VisaFinalizar", "Suministro");
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> VisaFinalizar()
        {
            bool bError = false;

            if (Session["IdUsuario"] != null)
            {
                if (Session["NroPedido"] != null && Session["SessionKey"] != null)
                {
                    if (Session["Paso"].ToString() == "3")
                    {
                        dynamic oParamDatos = new Newtonsoft.Json.Linq.JObject();
                        var oParamRecaudacion = new clsParametroEntrada();
                        var oRecaudacion = new clsResultadoRecaudacion();

                        oParamDatos.SessionToken = _TokenDistriluz;
                        oParamDatos.NroPedido = Session["NroPedido"];
                        oParamDatos.SessionTokenVisa = Session["SessionKey"];
                        oParamRecaudacion.Parametros = oParamDatos;
                        oRecaudacion = await _Recaudacion.SuministroEnviarCorreoPago(oParamRecaudacion);

                        if (oRecaudacion.IdError != 0)
                        {
                            bError = true;
                            Session["Cerrar"] = 1;
                            Session["Mensaje"] = "Se generó un error al enviar la operación por correo electrónico. El proceso finalizó de manera irregular.";
                        }
                    }
                }

                if (bError == false)
                {
                    return RedirectToAction("Resumen", "Suministro");
                }
                else
                    return RedirectToAction("VisaConsulta", "Suministro");
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> _CtaCte(clsParametro param)
        {
            var result = new List<Dictionary<String, String>>();
            var data = await _Atencion.ObtenerCtaCte(param);

            if (data.IdError == 0)
            {
                var datos = _Convert.ToObject<Dictionary<String, Object>>(data.Datos.ToString());
                result = _Convert.ToObject<List<Dictionary<String, String>>>(datos["B"].ToString());
            }

            return PartialView(result);
        }

        public async Task<ActionResult> _Atenciones(clsParametro param)
        {
            var result = new List<Dictionary<String, String>>();
            var data = await _Atencion.ConsultaAtencionesSuministro(param);

            if (data.IdError == 0) result = _Convert.ToObject<List<Dictionary<String, String>>>(data.Datos.ToString());

            return PartialView(result);
        }

        public ActionResult Reclamo(clsParametro param)
        {
            ViewBag.SuminAsoci = Session["SuminAsoci"] ?? new List<String>();

            return View();
        }

        public async Task<ActionResult> Beneficiario()
        {
            return View();
        }

        public async Task<ActionResult> MiRecibo()
        {
            return View();
        }

        public async Task<ActionResult> MiLectura()
        {
            return View();
        }

        public async Task<ActionResult> MiPago(String id)
        {
            ViewBag.IdOrder = id;
            ViewBag.IdUser = Request.Form.Get("IdUser");
            ViewBag.IdNroServicio = Request.Form.Get("Suministro");
            ViewBag.ErrorMessage = TempData["ErrorMessage"];

            return View();
        }

        public async Task<ActionResult> MyPay(String id)
        {
            var redirectTo = RedirectToAction("MiPago", new { id = id });

            if (!Request.Form.AllKeys.Contains("transactionToken")) return redirectTo;

            try
            {
                var idOrder = Convert.ToInt32(id);

                dynamic intent = new JObject();
                var response = (clsResultado)null;
                var responseRecauda = (clsResultadoRecaudacion)null;
                var isOK = false;
                var errorMessage = "";
                var intentBD = (Dictionary<String, String>)null;

                #region Obtener Intento

                var p = new Dictionary<String, String>();
                p["NroPedido"] = idOrder.ToString();
                response = await _Atencion.ObtenerIntento(new clsParametro { Parametros = p });

                if (response.IdError > 0) throw new Exception($"<<<Tuvimos problemas con su pago. Favor de volver a intentarlo.>>>{responseRecauda.Mensaje}");

                intentBD = _Convert.ToObject<Dictionary<String, String>>(response.Datos.ToString());

                if (intentBD["IdEstado"] != "1") return redirectTo;

                #endregion Obtener Intento

                var idEmpresa = Convert.ToInt16(intentBD["IdEmpresa"]);
                var amount = Convert.ToDecimal(intentBD["Importe"]);
                var payToken = Request.Form["transactionToken"];
                var payEmail = Request.Form["customerEmail"];

                #region Intento 1

                intent.SessionToken = _TokenDistriluz;
                intent.NroPedido = idOrder;
                intent.Email = payEmail;
                intent.TransactionTokenVisa = payToken;
                intent.SessionTokenVisa = String.Empty;
                intent.JsonPagoVisa = (Object)null;
                intent.JsonPagoResumen = (Object)null;
                intent.IdPaso = "1";
                intent.IdEstadoPaso = "1";
                intent.MensajePaso = "Pasarela NIUBIZ - OK";

                responseRecauda = await _Recaudacion.IntentoPagoActualizar(new clsParametroEntrada { Parametros = intent });

                if (responseRecauda.IdError > 0) throw new Exception($"<<<Tuvimos problemas con su pago. Favor de volver a intentarlo.>>>{responseRecauda.Mensaje}");

                #endregion Intento 1

                #region Pago NIUBIZ

                var token = await _Visa.CreateToken(idEmpresa, 2);
                response = await _Visa.AuthorizeSession(idEmpresa, 2, token, amount, idOrder, payToken);
                errorMessage = response.Mensaje;

                isOK = errorMessage == null;

                #endregion Pago NIUBIZ

                #region Intento 2

                intent.JsonPagoVisa = isOK ? response.Datos.ToString() : errorMessage;
                intent.IdPaso = "2";
                intent.IdEstadoPaso = isOK ? "1" : "0";
                intent.MensajePaso = isOK ? "NIUBIZ - OK" : "NIUBIZ - ERROR";

                #region Action Code

                if (!isOK)
                {
                    try
                    {
                        var error1 = _Convert.ToObject<Dictionary<String, Object>>(errorMessage);
                        if (error1.ContainsKey("errorMessage")) errorMessage = error1["errorMessage"].ToString();

                        var error2 = _Convert.ToObject<Dictionary<String, String>>(error1["data"].ToString());
                        if (error2.ContainsKey("ACTION_DESCRIPTION")) errorMessage = error2["ACTION_DESCRIPTION"];

                        var ac = Convert.ToInt32(error2["ACTION_CODE"]);

                        var param = new Dictionary<String, String>();
                        param.Add("IdProveedor", _IdProveedorVisa);
                        param.Add("IdObservacion", ac.ToString());
                        var res = await _Atencion.ObtenerObservacion(new clsParametro() { Parametros = param });

                        if (res.Datos != null)
                        {
                            var res2 = _Convert.ToObject<Dictionary<String, String>>(res.Datos.ToString());
                            if (res2.ContainsKey("MensajeCliente")) errorMessage = res2["MensajeCliente"];
                            if (res2.ContainsKey("MensajeProveedor")) intent.MensajePaso = $"[{ac}] - " + res2["MensajeProveedor"];
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMessage = "Operación Denegada. Contactar con entidad emisora de su tarjeta.";
                    }
                }

                #endregion Action Code

                try
                {
                    await _Recaudacion.IntentoPagoActualizar(new clsParametroEntrada { Parametros = intent });
                }
                catch (Exception ex) { }

                #endregion Intento 2

                if (!isOK) throw new Exception($"<<<{errorMessage}>>>");

                #region Pago Distriluz

                var rqPay = new
                {
                    SessionToken = _TokenDistriluz,
                    NroPedido = idOrder,
                    SessionTokenVisa = ""
                };
                responseRecauda = await _Recaudacion.SuministroPagarDeuda(new clsParametroEntrada { Parametros = rqPay });

                isOK = responseRecauda.IdError == 0;
                errorMessage = responseRecauda.Mensaje;

                if (!isOK)
                {
                    responseRecauda = await _Recaudacion.SuministroPagarDeuda(new clsParametroEntrada { Parametros = rqPay });

                    isOK = responseRecauda.IdError == 0;
                    errorMessage = responseRecauda.Mensaje;
                }

                try
                {
                    if (isOK) await _Recaudacion.SuministroEnviarCorreoPago(new clsParametroEntrada { Parametros = rqPay });
                }
                catch (Exception) { }

                #endregion Pago Distriluz

                #region Intento 3

                if (isOK)
                {
                    intent.JsonPagoResumen = intent.JsonPagoVisa;
                    intent.IdPaso = "3";
                    intent.IdEstadoPaso = "1";
                    intent.MensajePaso = "DISTRILUZ - OK";

                    try
                    {
                        await _Recaudacion.IntentoPagoActualizar(new clsParametroEntrada { Parametros = intent });
                    }
                    catch (Exception ex) { }
                }

                #endregion Intento 3
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
            }

            return redirectTo;
        }

        public async Task<ActionResult> MiVoucher(clsParametro param)
        {
            ViewBag.Pago = new clsParametroVisaAuthorizationResponse();
            ViewBag.Empresa = new clsEmpresasXUsuario();

            var title = $"Voucher-NoEncontrado";

            if (param != null && param.Parametros != null && param.Parametros.ContainsKey("TokenCaptcha"))
            {
                var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);

                if (result == null)
                {
                    title = $"Voucher-{param.Parametros["NroPedido"]}";
                    result = await _Atencion.ObtenerIntento(param);
                    result.Datos = _Convert.ToJson(result.Datos);

                    var data = _Convert.ToObject<Dictionary<String, Object>>(result.Datos.ToString());

                    ViewBag.PayIdNroServicio = data["IdNroServicio"].ToString();
                    ViewBag.PayPeriodos = data["Periodos"].ToString();
                    ViewBag.Pago = _Convert.ToObject<clsParametroVisaAuthorizationResponse>(data["Trama"].ToString());
                    ((clsEmpresasXUsuario)ViewBag.Empresa).Elementos = new List<clsEmpresa>
                    {
                        new clsEmpresa {
                            Empresa = data["Empresa"].ToString(),
                            Direccion = data["Direccion"].ToString(),
                            Telefono = data["Telefono"].ToString()
                        }
                    };
                }
            }

            return ViewPdf(title, "VisaImpresion", ViewBag);
        }

        #region FileResult

        public async Task<FileResult> DescargarRecibo(clsParametro param)
        {
            var bytes = (Byte[])null;
            var fileName = "";
            var fileNameFull = "";
            var isShow = param.Parametros.ContainsKey("IsShow") ? param.Parametros["IsShow"] : "0";

            if (param.Parametros.ContainsKey("PathRecibo"))
            {
                fileNameFull = param.Parametros["PathRecibo"];
                fileName = System.IO.Path.GetFileName(fileNameFull);
            }

            if (!System.IO.File.Exists(fileNameFull))
            {
                try
                {
                    fileName = $"{param.Parametros["IdNroServicio"]}_{param.Parametros["IdPeriodo"]}";
                    fileName += param.Parametros["IdTipoDocumento"] == "1" ? ".pdf" : ".png";

                    var result = await _Atencion.DescargarRecibo(param);
                    if (result.IdError > 0) throw new Exception(result.Mensaje);
                    bytes = Convert.FromBase64String(result.Datos.ToString());
                }
                catch (Exception ex)
                {
                    fileName = "Error.txt";
                    bytes = Encoding.UTF8.GetBytes(ex.Message);
                }
            }

            if (isShow == "1")
            {
                Response.AddHeader("Content-Disposition", $"InLine; FileName={fileName}");

                if (bytes == null)
                    return File(fileNameFull, MimeMapping.GetMimeMapping(fileName));
                else
                    return File(bytes, MimeMapping.GetMimeMapping(fileName));
            }
            else
            {
                if (bytes == null)
                    return File(fileNameFull, MimeMapping.GetMimeMapping(fileName), fileName);
                else
                    return File(bytes, MimeMapping.GetMimeMapping(fileName), fileName);
            }
        }

        public async Task<FileResult> DescargarRecibos(String id)
        {
            try
            {
                var base64 = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                var arr = base64.Split(';');

                var param = new Dictionary<String, String>();
                param["IdNroServicio"] = arr[0];
                param["IdPeriodo"] = arr[1];
                param["IdTipoDocumento"] = arr[2];
                param["PathRecibo"] = arr[3];

                return await DescargarRecibo(new clsParametro() { Parametros = param });
            }
            catch (Exception ex)
            {
                var fileName = "Error.txt";
                var bytes = Encoding.UTF8.GetBytes("Enlace incorrecto.");

                return File(bytes, MimeMapping.GetMimeMapping(fileName), fileName);
            }
        }

        public async Task<FileResult> DescargarArchivo(String id)
        {
            var bytes = (Byte[])null;
            var fileName = "";

            try
            {
                var base64 = Encoding.UTF8.GetString(Convert.FromBase64String(id));
                var arr = base64.Split(';');

                var param = new Dictionary<String, String>();
                param["IdContactanos"] = arr[0];
                param["IdType"] = "1";

                var result = await _Atencion.ObtenerFile(new clsParametro() { Parametros = param });
                if (result.IdError > 0) throw new Exception(result.Mensaje);

                var datos = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());

                bytes = Convert.FromBase64String(datos["File"]);
                fileName = datos["FileName"];

                var ext = Path.GetExtension(fileName).ToLower();
                if (".exe.bat.dll".Contains(ext)) throw new Exception($"Extensión peligrosa ({ext}) contacte a su administrador.");
            }
            catch (Exception ex)
            {
                fileName = "Error.html";
                bytes = Encoding.UTF8.GetBytes(ex.Message);
            }

            Response.AddHeader("Content-Disposition", $"InLine; FileName={fileName}");

            return File(bytes, MimeMapping.GetMimeMapping(fileName));
        }

        #endregion FileResult

        #region JSON

        [HttpPost]
        public async Task<JsonResult> DetalleRecibo(clsParametro param)
        {
            var cad = $"idnroservicio|{param.Parametros["IdNroServicio"]};periodo|{param.Parametros["IdPeriodo"]}";
            var tag = new clsParametroConsulta() { ParametroControlTag = cad };
            var result = await _Consulta.HistorialSuministro(clsConsulta.urlHistoricoEnum.UltimoRecibo, tag);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerRecibo(clsParametro param)
        {
            var result = await _Atencion.DescargarRecibo(param);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> InfoSuministro(clsParametro param)
        {
            var result = await _Atencion.InfoSuministro(param);

            if (result.IdError == 0) result.Datos = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> RegistrarReclamo(clsParametro param)
        {
            var result = await _Atencion.RegistrarReclamo(param);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> DesasociarSuministro(clsParametro param)
        {
            var result = await _Atencion.DesasociarSuministro(param);

            if (result.IdError == 0) result.Datos = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> AtencionesBySuministro(clsParametro param)
        {
            var result = await _Atencion.ConsultaAtencionesSuministro(param);

            if (result.IdError == 0) result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> RegistrarFotoLec(clsParamFile param)
        {
            var result = new clsResultado();

            try
            {
                param.Parametros["Archivo"] = param.GetBase64File();

                result = await _Atencion.RegistrarFotoLec(param);
            }
            catch (Exception ex)
            {
                result.SetError(1, ex.Message);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerBeneficiario(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.ObtenerBeneficiario(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> RegistrarBeneficiario(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.RegistrarBeneficiario(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> DatosNroServicio(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.DatosNroServicio(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ListarNroServicio(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.ListarNroServicio(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> LecturaRegistrar(clsParametro param)
        {
            var result = await _Atencion.LecturaRegistrar(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        #region Cobranza

        [HttpPost]
        public async Task<JsonResult> ObtenerDeudaPeriodos(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.ConsultarDeudaDetalle(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerNroPedido(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.GrabarIntentoDetalle(param);
            if (result.IdError > 0) return Json(result);

            var order = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());
            var idEmpresa = Convert.ToInt16(param.Parametros["IdEmpresa"]);
            var idNroServicio = Convert.ToInt32(param.Parametros["IdNroServicio"]);
            var amount = Convert.ToDecimal(param.Parametros["Importe"]);
            var ip = Request.UserHostAddress;
            var idType = (Int16)2;

            #region Pay

            var payToken = await _Visa.CreateSession(idEmpresa, idType, null, amount, ip, idNroServicio);
            var pay = new
            {
                Src = _Visa.GetURLButtonJS(idEmpresa),
                TimeOut = payToken["expirationTime"],
                Data = new
                {
                    sessiontoken = payToken["sessionKey"],
                    merchantid = _Visa.GetMerchantId(idEmpresa, idType),
                    purchasenumber = order["NroPedido"],
                    amount = amount
                }
            };

            #endregion Pay

            #region Result

            var data = new
            {
                Pay = pay,
                Order = order
            };

            result = new clsResultado();
            result.Datos = _Convert.ToJson(data);

            #endregion Result

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerDataPedido(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.ObtenerIntento(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        #endregion Cobranza

        #endregion JSON
    }
}