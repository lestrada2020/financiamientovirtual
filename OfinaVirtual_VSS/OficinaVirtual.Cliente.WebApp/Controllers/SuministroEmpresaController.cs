﻿using Newtonsoft.Json.Linq;
using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Data;
using System.IO.Compression;
using System.Web.Script.Serialization;

namespace OficinaVirtual.Cliente.WebApp.Controllers
{
    public class SuministroEmpresaController : Controller
    {
        #region Fields

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly String _TokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];
        private static Random random = new Random();
        #endregion Fields

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ResumenDeudas()
        {

            Session.Remove("TransactionToken");
            Session.Remove("CustomerEmail");
            Session.Remove("IdEmpresa");
            Session.Remove("NroPedido");
            Session.Remove("SessionKey");
            Session.Remove("Monto");
            Session.Remove("SessionToken");

            if (Session["IdUsuario"] != null)
            {
                try
                {
                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<String, String>();
                    var oTarifa = await clsAccountEmpresa.ListarTarifaActiva(param);

                    if (oTarifa.IdError == 0)
                    {
                        var oRolesResult = _Convert.ToJson(oTarifa.Datos);
                        JArray oRolesResultParse = JArray.Parse(oRolesResult);

                        ViewBag.Tarifa = oRolesResultParse;
                        ViewBag.VisaURLButtonJS = await clsVisa.GetVisaURLButtonJS(0);

                        return View();

                    }
                    else
                    {
                        ViewBag.Mensaje = "Se produjo un error al cargar el resumen de deudas.";
                        return View();
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Mensaje = "Se produjo un error al cargar el resumen de deudas.";
                    ViewBag.Exito = false;
                    return View();
                }
            }

            else
            {
                return RedirectToAction("Login", "AccountEmpresa");
            }

        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ListarSuministroEmpresa(string suministro, string tarifa, string empresadistriluz)
        {
            if (Session["IdUsuario"] != null)
            {

                try
                {

                    ViewBag.Global = Session["Global"];
                    string ruc = "";

                    foreach (var item in ViewBag.Global.LoginAcceso)
                    {
                        if (item.Key == "ruc")
                        {
                            ruc = item.Value;
                        }
                    }

                    if (tarifa == "") tarifa = "0";

                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<String, String>();
                    param.Parametros["rucempresa"] = ruc;
                    param.Parametros["empresadistriluz"] = empresadistriluz;
                    param.Parametros["suministro"] = suministro;
                    param.Parametros["tarifa"] = tarifa;

                    var oTarifa = await clsAccountEmpresa.ListarSuministros(param);

                    var result = _Convert.ToObject<List<Dictionary<String, String>>>(oTarifa.Datos.ToString());

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(ex.Message);
                }

            }
            else
            {
                return RedirectToAction("Login", "AccountEmpresa");
            }
        }




        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> _VerDetallePagosSuministros(string suministro)
        {
            if (Session["IdUsuario"] != null)
            {

                try
            {

                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["suministro"] = suministro;


                var olista = await clsAccountEmpresa.ListarSuministrosDetallePago(param);

                var result = _Convert.ToObject<List<Dictionary<String, String>>>(olista.Datos.ToString());

                string vista = RenderPartialToString(this, "_VerDetallePagosSuministros", result, ViewData, TempData);

                return Json(new { Html = vista, Lista = result }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

            }
            else
            {
                return RedirectToAction("Login", "AccountEmpresa");
            }
        }





        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> EliminarSuministroEmpresa(string suministro)
        {
            if (Session["IdUsuario"] != null)
            {
                try
            {

                ViewBag.Global = Session["Global"];
                string IdUsuario = "";

                foreach (var item in ViewBag.Global.LoginAcceso)
                {
                    if (item.Key == "IdUsuario")
                    {
                        IdUsuario = item.Value;
                    }
                }

                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["IdUsuario"] = IdUsuario;
                param.Parametros["suministro"] = suministro;

                var oResultado = await clsAccountEmpresa.EliminarSuministroAsociado(param);

                return Json(oResultado);

            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

            }
            else
            {
                return RedirectToAction("Login", "AccountEmpresa");
            }
        }



        public static string RenderPartialToString(Controller controller, string partialViewName, object model, ViewDataDictionary viewData, TempDataDictionary tempData)
        {
            ViewEngineResult result = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialViewName);

            if (result.View != null)
            {
                controller.ViewData.Model = model;
                StringBuilder sb = new StringBuilder();
                using (StringWriter sw = new StringWriter(sb))
                {
                    using (HtmlTextWriter output = new HtmlTextWriter(sw))
                    {
                        ViewContext viewContext = new ViewContext(controller.ControllerContext, result.View, viewData, tempData, output);
                        result.View.Render(viewContext, output);
                    }
                }

                return sb.ToString();
            }

            return String.Empty;
        }




        public ActionResult _RegistrarReclamo(clsParametro param)
        {
            if (Session["IdUsuario"] != null)
            {
                ViewBag.SuminAsoci = param.Parametros["Suministro"];

                return PartialView("_RegistrarReclamo");
            }
            else
            {
                return RedirectToAction("Login", "AccountEmpresa");
            }
        }



        [AllowAnonymous]
        [HttpGet]
        public ActionResult _AgregarSuministro(string empresa)
        {
            if (Session["IdUsuario"] != null)
            { 
                clsConsultaSuministro oDatos = new clsConsultaSuministro();
                oDatos.Proveedora = empresa;
                return PartialView(oDatos);}
            else
            { return RedirectToAction("Login", "Account");}
                
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _AgregarSuministro(clsConsultaSuministro modDatos)
        {
            if (Session["IdUsuario"] != null)
            {
                clsConsultaSuministro oDatos;
                clsParametroConsulta oParametro = new clsParametroConsulta();

                if (!ModelState.IsValid)
                {
                    modDatos.Mensaje = "val" +
                        "" +
                        "" +
                        "" +
                        "" +
                        "" +
                        "" +
                        "idar";
                    return PartialView(modDatos);
                }

                try
                {
                    StringBuilder sValores = new StringBuilder();
                    sValores.Append(string.Format("usuario|{0};", Session["UsuarioUltimo"]));
                    sValores.Append(string.Format("idnroservicio|{0};", $"{modDatos.Suministro}"));
                    sValores.Append(string.Format("nrorecibo|{0}", $"{modDatos.Recibo}"));



                    ViewBag.Global = Session["Global"];
                    string ruc = "";

                    foreach (var item in ViewBag.Global.LoginAcceso)
                    {
                        if (item.Key == "ruc")
                        {
                            ruc = item.Value;
                        }
                    }



                    oParametro.ParametroControlTag = sValores.ToString();

                    oDatos = await ConsultarSuministroDTO(ruc, modDatos.Suministro, modDatos.Recibo, modDatos.Proveedora);

                    if (oDatos.Exito == false)
                    {
                        ViewBag.Exito = false;
                        ViewBag.Mensaje = oDatos.Mensaje;
                        return PartialView(oDatos);
                    }
                    else
                    {
                        return PartialView(oDatos);
                    }


                }
                catch
                {
                    ViewBag.Mensaje = "Error al consultar el suministro. Intente nuevamente";
                    return PartialView(modDatos);
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _RegistrarSuministro(clsConsultaSuministro modDatos)
        {
            if (Session["IdUsuario"] != null)
            {
               
                clsParametroConsulta oParametro = new clsParametroConsulta();

                try
                {

                    ViewBag.Global = Session["Global"];
                    string ruc = "";

                    foreach (var item in ViewBag.Global.LoginAcceso)
                    {
                        if (item.Key == "ruc")
                        {
                            ruc = item.Value;
                        }
                    }

                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<String, String>();
                    param.Parametros["rucempresa"] = ruc;
                    param.Parametros["idnroservicio"] = modDatos.Suministro;
  

                    var oDatos = await clsAccountEmpresa.RegistrarSuministroAsociar(param);


                    if (oDatos.IdError == 0)
                    {
                        ViewBag.Exito = true;
                        ViewBag.Mensaje = "Suministro agregado satisfactoriamente";
                        return PartialView("_AgregarSuministro", modDatos);
                    }

                    else
                    {
                        ViewBag.Exito = false;
                        ViewBag.Mensaje = "Error al registrar el suministro. Intente nuevamente";
                        return PartialView("_AgregarSuministro", modDatos);
                    }
                     
                }
                catch
                {
                    ViewBag.Exito = false;
                    ViewBag.Mensaje = "Error al registrar el suministro. Intente nuevamente";
                    return PartialView("_AgregarSuministro", modDatos);
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }




        public static async Task<clsConsultaSuministro> ConsultarSuministroDTO(string rucempresa, string idnroservicio, string nrorecibo, string empresa)
        {
            clsResultadoConsultaSuministro oDatos = new clsResultadoConsultaSuministro();
            clsConsultaSuministro oRegistro = new clsConsultaSuministro();

            try
            {

                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["rucempresa"] = rucempresa;
                param.Parametros["idnroservicio"] = idnroservicio;
                param.Parametros["nrorecibo"] = nrorecibo;
                param.Parametros["empresa"] = empresa;


                var oDatos1 = await clsAccountEmpresa.BuscarSuministroAsociar(param);


                var objresult = _Convert.ToJson(oDatos1.Datos);
                JArray objresultparse = JArray.Parse(objresult);
                foreach (var item in objresultparse)
                {
                    oDatos.Titulo = item["titulo"].ToString();
                    oDatos.Subtitulo1 = item["subtitulo1"].ToString();
                    oDatos.Subtitulo2 = item["subtitulo2"].ToString();
                    oDatos.Subtitulo3 = item["subtitulo3"].ToString();
                    oDatos.DatoControl1 = item["datocontrol1"].ToString();
                    oDatos.DatoControl2 = item["datocontrol2"].ToString();
                    oDatos.Configuracion = item["configuracion"].ToString();
                }


                string sConfiguracion = oDatos.Configuracion == null ? "" : oDatos.Configuracion;
                string sDatoControl1 = oDatos.DatoControl1 == null ? "" : oDatos.DatoControl1;

                if (sConfiguracion == "")
                {
                    oRegistro.Exito = false;
                    oRegistro.Mensaje = "El suministro no existe o no se encuentra asociado a la empresa " + empresa +".";
                }
                else if (sConfiguracion != "" && sDatoControl1 != "")
                {
                    oRegistro.Exito = false;
                    oRegistro.Mensaje = sDatoControl1;
                }
                else if (sConfiguracion != "" && sDatoControl1 == "")
                {
                    oRegistro.Suministro = oDatos.Titulo;
                    oRegistro.NombreCompleto = oDatos.Subtitulo1;
                    oRegistro.Direccion = oDatos.Subtitulo2;
                    oRegistro.Proveedora = oDatos.Subtitulo3;
                    oRegistro.Exito = true;
                }
            }
            catch
            {
                throw;
            }

            return oRegistro;
        }




        [HttpPost]
        public async Task<ActionResult> ProcesarCargaMasiva(string empresa, HttpPostedFileBase file)
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    //subir el excel al servidor
                    string filePath = string.Empty;
                    string fileName = string.Empty;
                    string extension = string.Empty;

                    string random = Guid.NewGuid().ToString();

                    if (file != null)
                    {
                        string path = Server.MapPath("~/Uploads/");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        fileName = Path.GetFileName("UploadSuministrosClienteEmpresa-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + "-" + random);
                        extension = Path.GetExtension(path + file.FileName);
                        filePath = path + fileName + extension;

                        file.SaveAs(filePath);

                        var validacion = ValidarExcel(filePath);


                        if (validacion == "1")
                        {

                            System.Data.DataTable data = ExcelaDataTable(filePath);

                            if (System.IO.File.Exists(filePath))
                            {
                                System.IO.File.Delete(filePath);
                            }

                            ViewBag.Global = Session["Global"];

                            string ruc = "";

                            foreach (var item in ViewBag.Global.LoginAcceso)
                            {
                                if (item.Key == "ruc")
                                {
                                    ruc = item.Value;
                                }
                            }


                            clsParametroBulkData param = new clsParametroBulkData();
                            param.Parametros = new Dictionary<string, string>();
                            param.Parametros["rucempresa"] = ruc;
                            param.Parametros["empresadistriluz"] = empresa;
                            param.DataParametro = "listaSuministro";
                            param.Data = data;

                            var oResultado = await clsAccountEmpresa.ListarSuministrosCarga(param);

                            var result = _Convert.ToObject<List<Dictionary<String, String>>>(oResultado.Datos.ToString());

                            return Json(new { Lista = result, Mensaje = validacion }, JsonRequestBehavior.AllowGet);

                        }

                        else
                        {
                            if (System.IO.File.Exists(filePath))
                            {
                                System.IO.File.Delete(filePath);
                            }

                            return Json(new { Lista = "", Mensaje = validacion }, JsonRequestBehavior.AllowGet);

                        }
                    }


                    else
                    {
                        return Json(new { Lista = "", Mensaje = "Ha ocurrido un problema al cargar el archivo al servidor" }, JsonRequestBehavior.AllowGet);
                    }

                  
                }
            catch (Exception ex)

                {
                  return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { Lista = "", Mensaje = "Ha ocurrido un problema al cargar el archivo al servidor" }, JsonRequestBehavior.AllowGet);
            }


        }


        public static string ValidarExcel(string path)
        {
            string mensaje = "1";
            string columnas = "";
            string cabecera1 = "";
            string cabecera2 = "";
 
            FileInfo fi = new FileInfo(path);

            using (var pck = new OfficeOpenXml.ExcelPackage())
            {
                using (var stream = fi.OpenRead())
                {
                    pck.Load(stream);
                }

                var ws = pck.Workbook.Worksheets.First();

                var rowcount = ws.Dimension.Rows;
                var columncount = ws.Dimension.Columns;

                if (columncount > 2)
                {
                    mensaje = "El numero de columnas del archivo excel supera a las 2 columnas establecidas.";
                }

                if (rowcount <= 1)
                {
                    mensaje = "El archivo excel adjuntado no cuenta con registros.";          
                }

                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    columnas = columnas + firstRowCell.Text + ",";
                }

                string[] cols = columnas.Split(',');

                cabecera1 = cols[0].ToString();
                cabecera2 = cols[1].ToString();

                if (cabecera1 != "Suministro" || cabecera2 != "TipoDeuda")
                {
                    mensaje = "El nombre de algunas de las columnas no son correctas, las correctas son ('Suministro' y 'TipoDeuda').";
                }

                return mensaje;
            }
        }

        public static System.Data.DataTable ExcelaDataTable(string path, bool hasHeader = true)
        {
         
            FileInfo fi = new FileInfo(path);
           
            using (var pck = new OfficeOpenXml.ExcelPackage())
            {
                using (var stream = fi.OpenRead())
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                System.Data.DataTable tbl = new System.Data.DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                   
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }




        public async Task<ActionResult> _ValidarVisa(clsParametro param)
        {
  
            if (Session["IdUsuario"] != null)
            {
                double monto = 0;
                var suministros = param.Parametros["Suministros"];
                var empresa = Convert.ToInt16(param.Parametros["Empresa"]);
                var suministrosseleccionados = _Convert.ToObject<List<Dictionary<String, String>>>(suministros);

                //Agregando lista a datatable y calculando monto

                System.Data.DataTable tblsuministros = new System.Data.DataTable();
                tblsuministros.Columns.Add("suministro");
                tblsuministros.Columns.Add("tipodeuda");
                tblsuministros.Columns.Add("monto");

                for (int i = 0; i < suministrosseleccionados.Count; i++)
                {
                    tblsuministros.Rows.Add(new Object[] { suministrosseleccionados[i]["suministros"].ToString(), suministrosseleccionados[i]["tipodeuda"].ToString(), suministrosseleccionados[i]["monto"].ToString() });
                    monto = monto + Convert.ToDouble(suministrosseleccionados[i]["monto"].ToString());
                }

                dynamic oParamAntiFraudeMerchant = new JObject();
                dynamic oParamAntiFraude = new JObject();
                dynamic oParamDatos = new JObject();

                clsParametroEntrada oParamRecaudacion = new clsParametroEntrada();
                clsResultadoRecaudacion oRptaConsulta = new clsResultadoRecaudacion();
                clsResultadoRecaudacion oRptaIntento = new clsResultadoRecaudacion();
                clsParametroVisa oParamVisa = new clsParametroVisa();
                clsParametroEntrada oParamIntento = new clsParametroEntrada();
                Dictionary<String, String> oDatos;

                try
                {
                    //obteniendo los parametros de visa

                    oParamAntiFraudeMerchant.MDD1 = "web";
                    oParamAntiFraudeMerchant.MDD2 = "Canl";
                    oParamAntiFraudeMerchant.MDD3 = "Canl";
                    oParamAntiFraudeMerchant.MDD32 = "Canl";
                    oParamAntiFraude.clientIp = Request.UserHostAddress;
                    oParamAntiFraude.merchantDefineData = oParamAntiFraudeMerchant;
                    oParamVisa.amount = Convert.ToDouble(monto);
                    oParamVisa.channel = "web";
                    oParamVisa.antifraud = oParamAntiFraude;

                    var MerchantId = await clsVisa.GetVisaMerchantId(empresa);
                    var URLButtonJS = await clsVisa.GetVisaURLButtonJS(empresa);
                    var SessionToken = await clsVisa.CrearToken(empresa);
                    var SessionKey="";

                    if (SessionToken != null)
                    {
                        oDatos = await clsVisa.CrearSesion(empresa, SessionToken, oParamVisa);
                        SessionKey = oDatos["sessionKey"];
                      
                    }

                   
                  
                    if (SessionToken != null && SessionKey != null) 
                    {
                        //registrando y obteniendo el numero de pedido

                        ViewBag.Global = Session["Global"];


                        clsParametroBulkData oparamIntentograbar = new clsParametroBulkData();
                        oparamIntentograbar.Parametros = new Dictionary<string, string>();
                        oparamIntentograbar.Parametros["IdOrigen"] = "3";
                        oparamIntentograbar.Parametros["SessionTokenVisa"] = SessionToken;
                        oparamIntentograbar.DataParametro = "listaSuministro";
                        oparamIntentograbar.Data = tblsuministros;
                        oparamIntentograbar.Token = _TokenDistriluz;

                        foreach (var item in ViewBag.Global.LoginAcceso)
                        {
                            if (item.Key == "IdUsuario")
                            {
                                oparamIntentograbar.Parametros["IdUsuario"] = item.Value;
                            }
                        }

                        var oResultado = await clsRecaudacionEmpresa.IntentoPagoGrabar(oparamIntentograbar);
                     
                        if (oResultado.IdError == 0 && oResultado.Datos != null)
                        {

                            var NroPedido = _Convert.ToObject<List<Dictionary<String, String>>>(oResultado.Datos.ToString());
                            Dictionary<string, string> ParametrosVisa = new Dictionary<string, string>();
                            ParametrosVisa.Add("VisaMerchantId", MerchantId);
                            ParametrosVisa.Add("Monto", monto.ToString());
                            ParametrosVisa.Add("VisaSessionKey", SessionKey);
                            ParametrosVisa.Add("SuministroSeleccionados", suministrosseleccionados.Count().ToString());
                            ParametrosVisa.Add("NumeroPedido", NroPedido[0]["NroPedido"]);
                            ParametrosVisa.Add("Action", Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~") + "SuministroEmpresa/VisaProcesando");
                            ParametrosVisa.Add("TimeOutUrl", Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~"));

                            //seteando sesiones

                            Session["IdEmpresa"] = empresa;
                            Session["NroPedido"] = NroPedido[0]["NroPedido"];
                            Session["SessionKey"] = SessionKey;
                            Session["Monto"] = monto.ToString();
                            Session["SessionToken"] = SessionToken;


                            return Json(new { data = ParametrosVisa, mensaje = "" }, JsonRequestBehavior.AllowGet);

                        }
                        else
                        {
                            return Json(new { data = "", mensaje = oResultado.Mensaje }, JsonRequestBehavior.AllowGet);
                        }

                }

                    else
                    {
                        return Json(new { data = "", mensaje = "Se generó un error al autenticar con visa" }, JsonRequestBehavior.AllowGet);
                    }

                }

                catch (Exception ex)
                {
                    return Json(new { data = "", mensaje = "Se generó un error al autenticar con visa" }, JsonRequestBehavior.AllowGet);
                }

            }

            else
            {
                return RedirectToAction("Login", "AccountEmpresa");
            }
        }


        [HttpPost]
        public ActionResult VisaProcesando(FormCollection oForm)
        {
            Session["TransactionToken"] = oForm["transactionToken"];
            Session["CustomerEmail"] = oForm["customerEmail"];

            return View();
        }



        public async Task<ActionResult> VisaPago()
        {
            var TransactionToken = Session["TransactionToken"];
            var CustomerEmail = Session["CustomerEmail"];
            var IdEmpresa = Session["IdEmpresa"];
            var NroPedido = Session["NroPedido"];
            var SessionKey = Session["SessionKey"];
            var Monto = Session["Monto"];
            var SessionToken = Session["SessionToken"];

            var isOK = true;
            var errorMessage = "";

            if (Session["IdUsuario"] != null && Session["NroPedido"] != null && Session["SessionKey"] != null)
            {

                dynamic oParamDatos = new JObject();

                var oParamRecaudacion = new clsParametroEntrada();
                var oRptaRecaudacion = new clsResultadoRecaudacion();
                var oParamOrden = new clsParametroVisaAuthorizationOrden();
                var oParamVisa = new clsParametroVisaAuthorization();
                var oParamResponse = new clsParametroVisaAuthorizationResponse();
                var visaPayResponse = new clsResultado();

                try
                {
                    oParamRecaudacion.Parametros = oParamDatos;

                    #region Intento 1

                    oParamDatos.SessionToken = _TokenDistriluz;
                    oParamDatos.NroPedido = NroPedido;
                    oParamDatos.SessionTokenVisa = SessionKey;
                    oParamDatos.TransactionTokenVisa = TransactionToken;
                    oParamDatos.Email = CustomerEmail;
                    oParamDatos.JsonPagoVisa = null;
                    oParamDatos.JsonPagoResumen = null;
                    oParamDatos.IdPaso = "1";
                    oParamDatos.IdEstadoPaso = "1";
                    oParamDatos.MensajePaso = "Se validó correctamente el formulario visa";

                    oRptaRecaudacion = await clsRecaudacion.IntentoPagoActualizar2(oParamRecaudacion);

                    isOK = oRptaRecaudacion.IdError == 0;
                    errorMessage = oRptaRecaudacion.Mensaje;

                    #endregion Intento 1


                    if (isOK)
                    {
                        #region Pago Visa

                        oParamOrden.amount = Monto.ToString();
                        oParamOrden.currency = "PEN";
                        oParamOrden.purchaseNumber = NroPedido.ToString();
                        oParamOrden.tokenId = TransactionToken.ToString();
                        oParamVisa.antifraud = null;
                        oParamVisa.captureType = "manual";
                        oParamVisa.channel = "web";
                        oParamVisa.countable = "true";
                        oParamVisa.order = oParamOrden;
                        visaPayResponse = await clsVisa.SolicitarAutorizacion<clsResultado>(Convert.ToInt16(IdEmpresa), SessionToken.ToString(), oParamVisa);
                        oParamResponse.Mensaje = visaPayResponse.Mensaje;

                        isOK = oParamResponse.Mensaje == null;
                        errorMessage = oParamResponse.Mensaje;

                        if (isOK) oParamResponse = _Convert.ToObject<clsParametroVisaAuthorizationResponse>(visaPayResponse.Datos.ToString());

                        #endregion Pago Visa

                        #region Intento 2

                        oParamDatos.JsonPagoVisa = isOK ? visaPayResponse.Datos.ToString() : errorMessage;
                        oParamDatos.IdPaso = "2";
                        oParamDatos.IdEstadoPaso = isOK ? "1" : "0";
                        oParamDatos.MensajePaso = $"Se {(isOK ? "autorizó correctamente" : "denegó")} la transacción en visa";

                        #region Action Code

                        try
                        {
                            if (!isOK)
                            {
                                errorMessage = "Not Authorized";

                                var error1 = _Convert.ToObject<Dictionary<String, Object>>(oParamResponse.Mensaje);
                                if (error1.ContainsKey("errorMessage")) errorMessage = error1["errorMessage"].ToString();

                                var error2 = _Convert.ToObject<Dictionary<String, String>>(error1["data"].ToString());
                                if (error2.ContainsKey("ACTION_DESCRIPTION")) errorMessage = error2["ACTION_DESCRIPTION"];

                                var id = Convert.ToInt32(error2["ACTION_CODE"]);
                                var idProveedor = ConfigurationManager.AppSettings["IdProveedorVisa"];

                                var param = new Dictionary<String, String>();
                                param.Add("IdProveedor", idProveedor);
                                param.Add("IdObservacion", id.ToString());

                                var res = await clsAtencion.ObtenerObservacion2(new clsParametro() { Parametros = param });
                                if (res.Datos != null)
                                {
                                    var res2 = _Convert.ToObject<Dictionary<String, String>>(res.Datos.ToString());
                                    if (res2.ContainsKey("MensajeCliente")) errorMessage = res2["MensajeCliente"];
                                    if (res2.ContainsKey("MensajeProveedor")) oParamDatos.MensajePaso = $"[{id}] - " + res2["MensajeProveedor"];
                                }
                            }
                        }
                        catch (Exception ex) { }

                        #endregion Action Code

                        try
                        {
                            oRptaRecaudacion = await clsRecaudacion.IntentoPagoActualizar2(oParamRecaudacion);
                        }
                        catch (Exception ex) { }

                        #endregion Intento 2

                        if (isOK)
                        {
                            #region Pago Distriluz

                            oParamDatos.SessionToken = _TokenDistriluz;
                            oParamDatos.NroPedido = NroPedido;
                            oParamDatos.SessionTokenVisa = SessionKey.ToString();

                            oRptaRecaudacion = await clsRecaudacionEmpresa.SuministroPagarDeudaEmpresa(oParamRecaudacion);

                            isOK = oRptaRecaudacion.IdError == 0;
                            errorMessage = oRptaRecaudacion.Mensaje;

                            if (!isOK)
                            {
                                oRptaRecaudacion = await clsRecaudacionEmpresa.SuministroPagarDeudaEmpresa(oParamRecaudacion);

                                isOK = oRptaRecaudacion.IdError == 0;
                                errorMessage = oRptaRecaudacion.Mensaje;
                            }

                            #endregion Pago Distriluz

                            #region Intento 3

                            if (isOK)
                            {
                                oParamDatos.JsonPagoResumen = _Convert.ToJson(oParamResponse);
                                oParamDatos.IdPaso = "3";
                                oParamDatos.IdEstadoPaso = "1";
                                oParamDatos.MensajePaso = "Se registró correctamente la transacción en distriluz";

                                try
                                {
                                    oRptaRecaudacion = await clsRecaudacion.IntentoPagoActualizar2(oParamRecaudacion);
                                }
                                catch (Exception ex) { }
                            }

                            #endregion Intento 3
                        }

                    }

                    var mensaje = isOK ? "" : errorMessage;

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string jsonData = js.Serialize(oParamResponse);
                            
                    return Json(new { data = jsonData, mensaje = mensaje }, JsonRequestBehavior.AllowGet);
                   
                }

                catch (Exception ex)
                {
                    return Json(new { data = "", mensaje = "Ha ocurrido un problema con el proceso de pago visa." }, JsonRequestBehavior.AllowGet);
                }
            }

            else if (Session["IdUsuario"] != null && Session["NroPedido"] == null && Session["SessionKey"] == null)
            {
                return Json(new { data = "", mensaje = "Hubo un error con la sesion visa." }, JsonRequestBehavior.AllowGet);
            }

            else return RedirectToAction("Login", "Account");

        }




        public async Task<ActionResult> PagoDepositoBancario(clsParametro param)
        {
            if (Session["IdUsuario"] != null)
            {

                clsParametroEntrada parametros = new clsParametroEntrada();
                dynamic oParamDatos = new JObject();
                parametros.Parametros = oParamDatos;
                oParamDatos.SessionToken = _TokenDistriluz;
                var oBancos = await clsRecaudacionEmpresa.ListarBancos(parametros);

                if (oBancos.IdError == 0)
                {
                    var oBancosResult = _Convert.ToJson(oBancos.Datos);
                    JArray oBancosResultParse = JArray.Parse(oBancosResult);

                    ViewBag.Bancos = oBancosResultParse;
                    ViewBag.Empresa = param.Parametros["Empresa"];
                    ViewBag.Monto = param.Parametros["Monto"];
                    ViewBag.Global = Session["Global"];

                    //REGISTRO INICIAL DEL DEPOSITO BANCARIO


                    double monto = 0;
                    var suministros = param.Parametros["Suministros"];
                    var suministrosseleccionados = _Convert.ToObject<List<Dictionary<String, String>>>(suministros);

                    //Agregando lista a datatable y calculando monto

                    System.Data.DataTable tblsuministros = new System.Data.DataTable();
                    tblsuministros.Columns.Add("suministro");
                    tblsuministros.Columns.Add("tipodeuda");
                    tblsuministros.Columns.Add("monto");

                    for (int i = 0; i < suministrosseleccionados.Count; i++)
                    {
                        tblsuministros.Rows.Add(new Object[] { suministrosseleccionados[i]["suministros"].ToString(), suministrosseleccionados[i]["tipodeuda"].ToString(), suministrosseleccionados[i]["monto"].ToString() });
                        monto = monto + Convert.ToDouble(suministrosseleccionados[i]["monto"].ToString());
                    }

                    clsParametroBulkData oparamIntentograbar = new clsParametroBulkData();
                    oparamIntentograbar.Parametros = new Dictionary<string, string>();
                    oparamIntentograbar.DataParametro = "listaSuministro";
                    oparamIntentograbar.Data = tblsuministros;
                    oparamIntentograbar.Token = _TokenDistriluz;

                    foreach (var item in ViewBag.Global.LoginAcceso)
                    {
                        if (item.Key == "IdUsuario")
                        {
                            oparamIntentograbar.Parametros["IdUsuario"] = item.Value;
                        }
                    }

                    var oResultado = await clsRecaudacionEmpresa.GrabarDepositoBancarioIntento(oparamIntentograbar);

                    if (oResultado.IdError == 0 && oResultado.Datos != null)
                    {
                        var NroPedido = _Convert.ToObject<List<Dictionary<String, String>>>(oResultado.Datos.ToString());
                        ViewBag.NroPedido = NroPedido[0]["NroPedido"];
                    }

                }

                return PartialView("_PagoDepositoBancario");
            }

            else

            {
                return RedirectToAction("Login", "AccountEmpresa");
            }

        }


        [HttpPost]
        public async Task<ActionResult> ListarCuentas(clsParametro param)
        {

           var empresa =  param.Parametros["Empresa"];
           var banco = param.Parametros["Banco"];


            clsParametroEntrada parametros = new clsParametroEntrada();
            dynamic oParamDatos = new JObject();
            parametros.Parametros = oParamDatos;
            oParamDatos.SessionToken = _TokenDistriluz;
            oParamDatos.Empresa = empresa;
            oParamDatos.IdBanco = banco;
            var oCuentas = await clsRecaudacionEmpresa.ListarCuentas(parametros);

            if (oCuentas.IdError == 0)
            {
               
                var result = _Convert.ToObject<List<Dictionary<String, String>>>(oCuentas.Datos.ToString());

                return Json(new { data = result, mensaje = "" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { data = "", mensaje = "Ha ocurrido un error al cargas las cuentas." }, JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
     public async Task<ActionResult> _PagoDepositoBancario(clsDepositoBancario modDatos)

        {
            if (Session["IdUsuario"] != null)
            {
             
                clsParametroEntrada parametros = new clsParametroEntrada();
                dynamic oParamDatos = new JObject();
                parametros.Parametros = oParamDatos;
                oParamDatos.SessionToken = _TokenDistriluz;
                var oBancos = await clsRecaudacionEmpresa.ListarBancos(parametros);

                if (oBancos.IdError == 0)
                {
                    var oBancosResult = _Convert.ToJson(oBancos.Datos);
                    JArray oBancosResultParse = JArray.Parse(oBancosResult);

                    ViewBag.Bancos = oBancosResultParse;
                }

                ViewBag.Empresa = modDatos.Empresa;
                ViewBag.CuentaSeleccionada = modDatos.Cuenta;
                ViewBag.Monto = modDatos.MontoInicial;
                ViewBag.NroPedido = modDatos.NroPedido;

                if (!ModelState.IsValid)
                {
                    ViewBag.Exito = false;
                    return PartialView(modDatos);
                }

                decimal Monto = 0;

                if (!decimal.TryParse(modDatos.Monto, out Monto))
                {
                    ViewBag.Exito = false;
                    ViewBag.Mensaje = "El monto ingresado no es numérico";
                    return PartialView(modDatos);
                }

                if (Monto != Convert.ToDecimal(modDatos.MontoInicial))
                {
                    ViewBag.Exito = false;
                    ViewBag.Mensaje = "Los montos no coinciden";
                    return PartialView(modDatos);
                }


                try
                {
      
                    clsParametro param = new clsParametro();
                    param.Parametros = new Dictionary<string, string>();
                    param.Parametros["SessionToken"] = _TokenDistriluz;
                    param.Parametros["nroOrden"] = modDatos.NroPedido;
                    param.Parametros["IdBanco"] = modDatos.Banco;
                    param.Parametros["IdCuenta"] = modDatos.Cuenta;
                    param.Parametros["NroVoucher"] = modDatos.Voucher;
                    param.Parametros["IdCobranzaExterna"] = "0";
                    param.Parametros["NroTransaccion"] = "0";
                    param.Parametros["IdEstado"] = "2";
                    param.Parametros["MensajeExcepcion"] = "";

                    var oDatos = await clsRecaudacionEmpresa.ActualizarDepositoBancarioEstado(param);

                    if (oDatos.IdError == 0)
                    {
                        ViewBag.Exito = true;
                        ViewBag.Mensaje = "Depósito bancario registrado correctamente.";
                        return PartialView(modDatos);             
                    }
                    else
                    {
                        ViewBag.Exito = false;
                        ViewBag.Mensaje = oDatos.Mensaje;
                        return PartialView(oDatos);
                    }

                }
                catch
                {
                    ViewBag.Exito = false;
                    ViewBag.Mensaje = "Error al realizar el depósito bancario. Intente nuevamente";
                    return PartialView(modDatos);
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }





    }

}