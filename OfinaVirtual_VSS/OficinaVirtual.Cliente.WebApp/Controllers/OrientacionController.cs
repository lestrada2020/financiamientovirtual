﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OficinaVirtual.Cliente.WebApp.Controllers
{
    public class OrientacionController : Controller
    {
        // GET: Orientacion
        public async Task<ActionResult> Index()
        {
            clsListaOrientacion oDatos = new clsListaOrientacion();

            try
            {
                oDatos = await clsOrientacion.ListarOrientacion();
            }
            catch
            {
                throw;
            }

            return View(oDatos);
        }

        public async Task<ActionResult> _Requisitos(int IdTipoOrientacion)
        {
            clsParametroConsulta oParametro = new clsParametroConsulta();
            clsListaRequisitos oDatos = new clsListaRequisitos();

            try
            {
                oParametro.ParametroControlTag = $"IdTipoOrientacion|{IdTipoOrientacion}";
                oDatos = await clsOrientacion.RequisitosXOrientacion(oParametro);
            }
            catch
            {
                throw;
            }

            return PartialView(oDatos);
        }

        public async Task<ActionResult> DecretoUrgencia035()
        {
            return View();
        }

        public async Task<ActionResult> BeneficiarioPreguntas()
        {
            return View();
        }

        public async Task<ActionResult> Tema(String id)
        {
            var v = ViewEngines.Engines.FindView(this.ControllerContext, id, null);

            if (v.View == null) return View();

            return View(id);
        }
    }
}