﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OficinaVirtual.Cliente.WebApp.Controllers
{
    public class AccountController : Controller
    {
        #region Fields

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsAccount _Account = clsAccount.Instancia;
        private static readonly clsAtencion _Atencion = clsAtencion.Instancia;
        private static readonly clsConsulta _Consulta = clsConsulta.Instancia;
        private static readonly clsRecaudacion _Recaudacion = clsRecaudacion.Instancia;
        private static readonly String _TokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];        
        private static readonly String _PIDEDNIQx = ConfigurationManager.AppSettings["PIDEDNIQx"];

        #endregion Fields

        #region Private

        private async Task<Boolean> ValidDNIPIDE(clsParametro param, clsResultado result)
        {
            if (String.IsNullOrEmpty(_PIDEDNIQx)
                || param.Parametros["IdTipoIdentidad"] != "1") return true;

            var response = true;
            var credentialDNI = _PIDEDNIQx.Split(new String[] { "::" }, StringSplitOptions.None);
            var rqDNIPIDE = new clsParametro();
            rqDNIPIDE.Parametros = new Dictionary<String, String>();
            rqDNIPIDE.Parametros["nuDniConsulta"] = param.Parametros["dni"];
            rqDNIPIDE.Parametros["nuRucUsuario"] = "20132023540";
            rqDNIPIDE.Parametros["nuDniUsuario"] = credentialDNI[0];
            rqDNIPIDE.Parametros["password"] = credentialDNI[1];

            try
            {
                var rsDNIPIDE = await _Account.SolicitarDNIPIDE(rqDNIPIDE);

                if (rsDNIPIDE.IdError == 0)
                {
                    var message = "";
                    var rsDNIPIDE01 = _Convert.ToObject<Dictionary<String, Object>>(_Convert.ToJson(rsDNIPIDE.Datos));
                    var rsDNIPIDE02 = _Convert.ToObject<Dictionary<String, Object>>(_Convert.ToJson(rsDNIPIDE01["consultarResponse"]));
                    var rsDNIPIDE03 = _Convert.ToObject<Dictionary<String, Object>>(_Convert.ToJson(rsDNIPIDE02["return"]));

                    if (Convert.ToInt32(rsDNIPIDE03["coResultado"]) == 0)
                    {
                        param.Parametros["JSONPIDE"] = _Convert.ToJson(rsDNIPIDE03["datosPersona"]);
                        var rsDNIPIDE04 = _Convert.ToObject<Dictionary<String, String>>(param.Parametros["JSONPIDE"]);

                        if (!rsDNIPIDE04["apPrimer"].Replace(" ", "").ToLower().Equals(param.Parametros["apaterno"].ToLower()))
                        {
                            message += "Su apellido paterno";
                            response = false;
                        }

                        if (!rsDNIPIDE04["apSegundo"].Replace(" ", "").ToLower().Equals(param.Parametros["amaterno"].ToLower()))
                        {
                            message += (message.Length > 0 ? ", " : "") + "Su apellido materno";
                            response = false;
                        }

                        var IsValidName = false;
                        var rqNames = param.Parametros["nombres"].ToLower().Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (var item in rqNames) IsValidName |= rsDNIPIDE04["prenombres"].ToLower().Contains(item);

                        if (!IsValidName)
                        {
                            message += (message.Length > 0 ? ", " : "") + "Su nombres";
                            response = false;
                        }

                        if (!response) result.Datos = $"error|1;mensaje|{message}, no coiciden con los datos de su DNI";
                    }
                    else
                    {
                        response = false;
                        result.Datos = $"error|1;mensaje|({rsDNIPIDE03["coResultado"]}): {rsDNIPIDE03["deResultado"]}";
                    }
                }
            }
            catch (Exception ex)
            {
                response = true;
            }

            return response;
        }

        #endregion

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            ViewBag.Mensaje = "";

            if (Session["IdUsuario"] != null)
                return RedirectToAction("Resumen", "Suministro");
            else
                return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(clsUsuarioLogin modelLogin)
        {
            clsUsuario oDatos;
            clsUsuarioActivacion modelActivacion = new clsUsuarioActivacion();
            clsParametroConsulta oParametro = new clsParametroConsulta();

            ViewBag.Mensaje = "";

            if (!ModelState.IsValid)
            {
                return View(modelLogin);
            }

            try
            {
                oParametro.ParametroControlTag = $"usuario|{modelLogin.usuario};clave|{modelLogin.clave}";
                oDatos = await _Account.ValidaAcceso(oParametro);

                if (!oDatos.Exito)
                {
                    ViewBag.Mensaje = oDatos.Mensaje;
                    string tieneSMS = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "tieneSMS", "0");

                    if (tieneSMS == "1")
                    {
                        ViewBag.Celular = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "celular", "");
                        ViewBag.Usuario = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "NroIdentidad", "");
                        ViewBag.Nombres = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "nombreUsuario", "");

                        return View("ActivarCuenta", modelActivacion);
                    }
                    else
                    {
                        return View(modelLogin);
                    }
                }
                else
                {
                    Session["IdUsuario"] = oDatos.IdUsuario.ToString();
                    Session["UsuarioUltimo"] = oDatos.UsuarioLogin;
                    Session["Clave"] = modelLogin.clave;
                    Session["Token"] = oDatos.Token;
                    Session["FechaUltimoAcceso"] = oDatos.FechaUltimoAcceso.ToLocalTime().ToString();
                    Session["ControlTagCredenciales"] = oDatos.DatosControlTag;
                    Session["Correo"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "correo", "0");
                    Session["NombreUsuario"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "nombreUsuario", "0");
                    Session["Telefono"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "telefono", "0");
                    Session["NtfEmision"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "ntfEmi", "0");
                    Session["NtfVencimiento"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "ntfVcto", "0");
                    Session["NtfCorte"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "ntfCr", "0");
                    Session["NtfInterrupcion"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "ntfInt", "0");
                    Session["UrlBase"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "urlbase", "0");
                    Session["ApPaterno"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "ApPaterno", "");
                    Session["ApMaterno"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "ApMaterno", "0");
                    Session["Nombres"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "Nombres", "0");
                    Session["IdTipoIdentidad"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "IdTipoIdentidad", "0");
                    Session["NroIdentidad"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "NroIdentidad", "0");
                    Session["urlterminos"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "urlterminos", "0");
                    Session["urlDevolucion"] = clsHelperControlTag.ObtenerValor(oDatos.DatosControlTag, "urlDevolucion", "0");
                    Session["Cliente"] = clsDatosConstantes.Cliente.Natural;

                    return RedirectToAction("Resumen", "Suministro");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = ex.Message; //"Error al validar usuario. Intente nuevamente"; //ModelState.AddModelError("clave", "Error al validar usuario. Intente nuevamente.");
                return View(modelLogin);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> _Usuario()
        {
            try
            {
                clsResultadoListaMaestros oTipoDocumento = null;
                oTipoDocumento = await _Consulta.ListarMaestros(clsConsulta.TipoMaestroEnum.TipoDocumento);

                if (oTipoDocumento != null)
                {
                    ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");
                    ViewBag.Mensaje = string.Empty;
                }
                else
                {
                    ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
                }
            }
            catch
            {
                ViewBag.Mensaje = "Se produjo un error al cargar el registro de usuario.";
            }

            ViewBag.Exito = false;
            ViewBag.Nombres = string.Empty;
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _Usuario(clsUsuarioRegistro modelRegistro)
        {
            clsParametroConsulta oParametro = new clsParametroConsulta();
            clsResultadoListaMaestros oTipoDocumento = new clsResultadoListaMaestros();
            oTipoDocumento = await _Consulta.ListarMaestros(clsConsulta.TipoMaestroEnum.TipoDocumento);

            ViewBag.Exito = false;
            ViewBag.Mensaje = "";
            ViewBag.TipoDocumentos = new SelectList(oTipoDocumento.Datos, "Id", "Nombre");

            if (!ModelState.IsValid)
            {
                return PartialView(modelRegistro);
            }

            try
            {
                string celular = "";

                if (modelRegistro.celular != null)
                {
                    celular = modelRegistro.celular;
                }

                string sFecha = modelRegistro.fechaNacimiento.ToString("yyyyMMdd");
                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["dni"] = modelRegistro.documentoIdentidad.Replace(" ", "");
                param.Parametros["nombres"] = modelRegistro.nombres.Trim();
                param.Parametros["apaterno"] = modelRegistro.aPaterno.Replace(" ", "");
                param.Parametros["amaterno"] = modelRegistro.aMaterno.Replace(" ", "");
                param.Parametros["correo"] = modelRegistro.correo;
                param.Parametros["fechanacimiento"] = sFecha;
                param.Parametros["movil"] = celular;
                param.Parametros["clave"] = modelRegistro.clave;
                param.Parametros["idnroservicio"] = modelRegistro.NroSuministro;
                param.Parametros["nrorecibo"] = string.Empty;
                param.Parametros["IdTipoIdentidad"] = modelRegistro.tipodocumento;
                param.Parametros["JSONPIDE"] = "";

                if (!String.IsNullOrEmpty(modelRegistro.ExtraParametros))
                {
                    var json = _Convert.ToObject<Dictionary<String, String>>(modelRegistro.ExtraParametros);

                    foreach (var item in json)
                        if (!item.Key.StartsWith("XN")) 
                            param.Parametros[item.Key] = item.Value;
                }

                var result = new clsResultado();

                if (await ValidDNIPIDE(param, result)) result = await _Account.RegistrarUsuario(param);

                var error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "0");
                var mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");

                if (celular == "") //No se ingreso el celular
                {
                    if (error == "1")
                    {
                        ViewBag.Mensaje = mensaje;
                        return PartialView(modelRegistro);
                    }
                    else if (error == "2")
                    {
                        ViewBag.Celular = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "celular", "");
                        ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                        ViewBag.Nombres = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "nombrecliente", "");
                        ViewBag.Mensaje = mensaje;
                        return PartialView("_ActivarCuenta");
                    }
                    else
                    {
                        ViewBag.Exito = true;
                        ViewBag.Mensaje = mensaje;
                        ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                        ViewBag.Nombres = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "nombrecliente", "");
                        return PartialView(modelRegistro);
                    }
                }
                else //Se ingreso el celular
                {
                    if (error == "1")
                    {
                        ViewBag.Mensaje = mensaje;
                        return PartialView(modelRegistro);
                    }
                    else if (error == "2")
                    {
                        ViewBag.Celular = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "celular", "");
                        ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                        ViewBag.Nombres = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "nombrecliente", "");
                        ViewBag.Mensaje = mensaje;
                        return PartialView("_ActivarCuenta");
                    }
                    else
                    {
                        ViewBag.Exito = true;
                        ViewBag.Mensaje = mensaje;
                        ViewBag.Celular = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "celular", "");
                        ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                        ViewBag.Nombres = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "nombrecliente", "");
                        return PartialView("_ActivarCuenta");
                    }
                }
            }
            catch(Exception ex)
            {
                ViewBag.Mensaje = "Error al registrar al usuario. Intente nuevamente";
                return PartialView(modelRegistro);
            }
        }

        [HttpPost]
        public async Task<JsonResult> ReenviarSMS(clsParametro param)
        {
            var result = await _Account.ReenviarSMS(param);
            return Json(result);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _ActivarCuenta(clsUsuarioActivacion modelRegistro)
        {
            ViewBag.Exito = false;
            ViewBag.Mensaje = "";

            if (!ModelState.IsValid)
            {
                ViewBag.Usuario = modelRegistro.documentoIdentidad;
                ViewBag.Celular = modelRegistro.celular;
                return PartialView(modelRegistro);
            }

            try
            {
                string celular = "";

                if (modelRegistro.celular != null)
                {
                    celular = modelRegistro.celular;
                }

                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["dni"] = modelRegistro.documentoIdentidad;
                param.Parametros["codigoactivacion"] = modelRegistro.codigoactivacion;

                var result = await _Account.ActivarCuentaXsms(param);
                var error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "0");
                var mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");

                if (error == "1")
                {
                    ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                    ViewBag.Celular = modelRegistro.celular;
                    ViewBag.Mensaje = mensaje;
                    return PartialView(modelRegistro);
                }
                else
                {
                    ViewBag.Exito2 = true;
                    ViewBag.Mensaje = mensaje;
                    ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                    ViewBag.Nombres = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "nombrecliente", "");
                    return PartialView("_ActivarCuenta", modelRegistro);
                }
            }
            catch
            {
                ViewBag.Usuario = modelRegistro.documentoIdentidad;
                ViewBag.Celular = modelRegistro.celular;
                ViewBag.Mensaje = "Error al activar la cuenta. Intente nuevamente";
                return PartialView(modelRegistro);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ActivarCuenta(clsUsuarioActivacion modelRegistro)
        {
            ViewBag.Exito = false;
            ViewBag.Mensaje = "";

            if (!ModelState.IsValid)
            {
                ViewBag.Usuario = modelRegistro.documentoIdentidad;
                ViewBag.Celular = modelRegistro.celular;
                return View(modelRegistro);
            }

            try
            {
                string celular = "";

                if (modelRegistro.celular != null)
                {
                    celular = modelRegistro.celular;
                }

                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["dni"] = modelRegistro.documentoIdentidad;
                param.Parametros["codigoactivacion"] = modelRegistro.codigoactivacion;

                var result = await _Account.ActivarCuentaXsms(param);
                var error = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "error", "0");
                var mensaje = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "mensaje", "0");

                if (error == "1")
                {
                    ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                    ViewBag.Celular = modelRegistro.celular;
                    ViewBag.Mensaje = mensaje;
                    return View(modelRegistro);
                }
                else
                {
                    ViewBag.Exito2 = true;
                    ViewBag.Mensaje = mensaje;
                    ViewBag.Usuario = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "usuario", "");
                    ViewBag.Nombres = clsHelperControlTag.ObtenerValor(result.Datos.ToString(), "nombrecliente", "");
                    return View("ActivarCuenta", modelRegistro);
                }
            }
            catch
            {
                ViewBag.Usuario = modelRegistro.documentoIdentidad;
                ViewBag.Celular = modelRegistro.celular;
                ViewBag.Mensaje = "Error al activar la cuenta. Intente nuevamente";
                return View(modelRegistro);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult _CambiarClave()
        {
            ViewBag.Error = string.Empty;
            ViewBag.Mensaje = string.Empty;
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _CambiarClave(clsUsuarioCambioContraseña modDatos)
        {
            clsResultadoOperacion oDatos;
            clsParametroConsulta oParametro = new clsParametroConsulta();

            ViewBag.Error = "";
            ViewBag.Mensaje = "";

            if (!ModelState.IsValid)
            {
                return PartialView(modDatos);
            }

            try
            {
                StringBuilder sValores = new StringBuilder();
                sValores.Append(string.Format("usuario|{0};", $"{modDatos.usuario}"));
                sValores.Append(string.Format("correo|{0}", $"{modDatos.correo}"));
                oParametro.ParametroControlTag = sValores.ToString();

                oDatos = await _Account.CambiarContraseña(oParametro);

                if (!oDatos.Exito)
                {
                    ViewBag.Mensaje = oDatos.Mensaje; //ModelState.AddModelError("", oDatos.Mensaje);
                    return PartialView(modDatos);
                }
                else
                {
                    ViewBag.Error = clsHelperControlTag.ObtenerValor(oDatos.ControlTag, "error", "");
                    ViewBag.Mensaje = clsHelperControlTag.ObtenerValor(oDatos.ControlTag, "mensaje", "");
                    return PartialView(modDatos);
                }
            }
            catch
            {
                ViewBag.Mensaje = "Error al cambiar la clave. Intente nuevamente";
                return PartialView(modDatos);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult _ActualizarDatos()
        {
            if (Session["IdUsuario"] != null)
            {
                clsUsuarioActualizar modUsuario = new clsUsuarioActualizar();
                modUsuario.NombreCompleto = Session["NombreUsuario"].ToString();
                modUsuario.telefono = Session["telefono"].ToString();
                modUsuario.correo = Session["Correo"].ToString();
                ViewBag.Exito = false;
                ViewBag.Mensaje = "";
                return View(modUsuario);
            }
            else
                return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _ActualizarDatos(clsUsuarioActualizar modUsuario)
        {
            if (Session["IdUsuario"] != null)
            {
                clsResultadoOperacion oDatos = new clsResultadoOperacion();
                clsParametroConsulta oParametro = new clsParametroConsulta();

                ViewBag.Exito = false;
                ViewBag.Mensaje = "";
                modUsuario.NombreCompleto = Session["NombreUsuario"].ToString();

                if (!ModelState.IsValid)
                {
                    return View(modUsuario);
                }

                try
                {
                    StringBuilder sValores = new StringBuilder();
                    sValores.Append(string.Format("idusuario|{0};", $"{Session["IdUsuario"]}"));
                    sValores.Append(string.Format("Correo|{0};", $"{modUsuario.correo}"));
                    sValores.Append(string.Format("Telefono|{0};", $"{modUsuario.telefono}"));
                    sValores.Append(string.Format("Clave|{0};", $"{modUsuario.clave}"));
                    sValores.Append(string.Format("Emision|{0};", $"1"));
                    sValores.Append(string.Format("Vencimiento|{0};", $"1"));
                    sValores.Append(string.Format("Corte|{0};", $"1"));
                    sValores.Append(string.Format("Interrupcion|{0}", $"1"));

                    oParametro.ParametroControlTag = sValores.ToString();
                    oDatos = await _Account.ActualizarUsuario(oParametro);

                    if (oDatos.Exito == true)
                    {
                        Session["Correo"] = modUsuario.correo;
                        Session["Telefono"] = modUsuario.telefono;
                        Session["clave"] = modUsuario.clave;
                        modUsuario.claveAntigua = "";
                        modUsuario.clave = "";
                        modUsuario.confirmacionClave = "";
                        ViewBag.Exito = oDatos.Exito;
                        ModelState.Clear();
                    }
                    return View(modUsuario);
                }
                catch
                {
                    ViewBag.Mensaje = "Error al actualizar al usuario. Intente nuevamente";
                    return View(modUsuario);
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult _Logout()
        {
            ViewBag.Mensaje = "";
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            Session.Remove("IdUsuario");
            Session.Remove("UsuarioUltimo");
            Session.Remove("Clave");
            Session.Remove("Token");
            Session.Remove("FechaUltimoAcceso");
            Session.Remove("ControlTagCredenciales");

            Session.Remove("Correo");
            Session.Remove("NombreUsuario");
            Session.Remove("Telefono");
            Session.Remove("NtfEmision");
            Session.Remove("NtfVencimiento");
            Session.Remove("NtfCorte");
            Session.Remove("NtfInterrupcion");
            Session.Remove("UrlBase");

            Session.Remove("ApPaterno");
            Session.Remove("ApMaterno");
            Session.Remove("Nombres");
            Session.Remove("urlterminos");
            Session.Remove("urlDevolucion");

            Session.Abandon();
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            HttpCookie oCookie = new HttpCookie("IdUsuario");
            oCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(oCookie);

            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmarEmail(String id)
        {
            var param = new clsParametro();
            param.Parametros = new Dictionary<String, String>();
            param.Parametros["Guid"] = id;

            var result = await _Atencion.ConfirmarEmail(param);

            return View(result);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> RestablecerPassword(String id)
        {
            ViewBag.Id = id;

            return View();
        }

        [HttpPost]
        public async Task<JsonResult> CambiarPassword(clsParametro param)
        {
            var result = await _Atencion.CambiarPassword(param);

            if (result.IdError == 0) result.Datos = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());

            return Json(result);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<String> VisaPayFailed(String id)
        {
            var result = "";
            var data = "NULL";

            try
            {
                var a = id.Split(';');

                if (a.Length == 1) return "Usted no tiene autorización para realizar esta acción...";
                if (a[1] != "000") return "Clave erronea para realizar esta acción...";

                data = Encoding.UTF8.GetString(Convert.FromBase64String(a[0]));

                var c = data.Split(';');
                var d = DateTime.ParseExact(c[1].Substring(0, 10), "yyyy-MM-dd", CultureInfo.InvariantCulture);

                if (DateTime.Now.Date != d.Date) return "Pedido ya caducó...";

                var param = new Dictionary<String, String>();
                param["NroPedido"] = Convert.ToInt32(c[0]).ToString();
                param["SessionTokenVisa"] = "";
                param["SessionToken"] = ConfigurationManager.AppSettings["TokenDistriluz"];

                var rq = new clsParametroEntrada() { Parametros = param };

                #region Pay

                var pay = await _Recaudacion.SuministroPagarDeuda(rq);

                if (pay.IdError > 0) pay = await _Recaudacion.SuministroPagarDeuda(rq);

                result = pay.Mensaje;

                if (pay.IdError == 0)
                {
                    _Recaudacion.SuministroEnviarCorreoPago(rq);

                    result = "Se grabó correctamente";
                }

                #endregion Pay
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return $"{result} ==> {data}";
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerDatosConfirmacion(clsParametro param)
        {
            var result = await clsAccount.ObtenerDatosConfirmacion(param);

            if (result.IdError == 0) result.Datos = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> SolicitarNewPassword(clsParametro param)
        {
            var result = await clsAccount.SolicitarNewPassword(param);

            if (result.IdError == 0) result.Datos = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());

            return Json(result);
        }
    }
}