﻿using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OficinaVirtual.Cliente.WebApp.Controllers
{
    public class HomeController : Controller
    {
        #region Fields

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsAccount _Account = clsAccount.Instancia;
        private static readonly clsAtencion _Atencion = clsAtencion.Instancia;
        private static readonly clsConsulta _Consulta = clsConsulta.Instancia;

        #endregion Fields

        #region Private

        private async Task<clsResultado> ValidCaptcha(String response)
        {
            var r = new clsResultado();

            try
            {
                var a = await _Consulta.ValidCaptcha(response);
                var b = _Convert.ToObject<Dictionary<String, Object>>(a.Datos.ToString());
                var c = Convert.ToBoolean(b["success"]);

                if (c) r = null;
                else throw new Exception(b["error-codes"].ToString());
            }
            catch (Exception ex)
            {
                r.IdError = 1;
                r.Mensaje = "Favor de reintentar.";
                r.Datos = ex.Message;
            }

            return r;
        }

        #endregion Private

        #region ActionResult

        public async Task<ActionResult> Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Contactenos()
        {
            ViewBag.Exito = false;
            ViewBag.Mensaje = string.Empty;

            if (Session["IdUsuario"] != null)
            {
                clsUsuarioContactenos modDatos = new clsUsuarioContactenos();
                modDatos.correo = Session["Correo"].ToString();
                return View(modDatos);
            }
            else
                return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contactenos(clsUsuarioContactenos modDatos)
        {
            Int32 iIdUsuario = 0;
            String fileBase64 = "";
            String fileName = "";

            ViewBag.Exito = false;
            ViewBag.Mensaje = string.Empty;

            if (!ModelState.IsValid) return View(modDatos);

            try
            {
                if (Session["IdUsuario"] != null) iIdUsuario = Convert.ToInt32(Session["IdUsuario"]);

                if (modDatos.FileArchivo != null)
                {
                    fileName = modDatos.FileArchivo.FileName;

                    var bytes = new Byte[modDatos.FileArchivo.ContentLength];
                    modDatos.FileArchivo.InputStream.Read(bytes, 0, bytes.Length);
                    fileBase64 = Convert.ToBase64String(bytes);
                }

                clsParametro param = new clsParametro();
                param.Parametros = new Dictionary<String, String>();
                param.Parametros["IdUsuario"] = iIdUsuario.ToString();
                param.Parametros["IdEmpresa"] = modDatos.IdEmpresa.ToString();
                param.Parametros["IdNroServicio"] = modDatos.IdNroServicio.ToString();
                param.Parametros["Correo"] = modDatos.correo;
                param.Parametros["Asunto"] = modDatos.asunto;
                param.Parametros["Comentario"] = modDatos.comentario;
                param.Parametros["ArchivoNombre"] = fileName;
                param.Parametros["Archivo"] = fileBase64;

                var result = await _Atencion.RegistrarContactanos(param);

                if (result.IdError == 0)
                {
                    ViewBag.Exito = true;
                    ViewBag.Mensaje = "Mensaje enviado correctamente [" + result.Datos + "]";
                    ModelState.Clear();

                    return View();
                }
                else
                {
                    ViewBag.Mensaje = result.Mensaje;

                    return View(modDatos);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = ex.Message;

                return View(modDatos);
            }
        }

        public async Task<ActionResult> _EmpresasXUsuario()
        {
            int iIdUsuario = 0;
            clsParametroConsulta oParametro = new clsParametroConsulta();
            clsEmpresasXUsuario oDatos = new clsEmpresasXUsuario();

            try
            {
                if (Session["IdUsuario"] != null) iIdUsuario = Convert.ToInt32(Session["IdUsuario"]);
                oParametro.ParametroControlTag = $"idusuario|{iIdUsuario};IdNroServicio|0";
                oDatos = await _Account.ListarEmpresasXUsuario(oParametro);
            }
            catch
            {
                throw;
            }

            return PartialView(oDatos);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> LugaresPago()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ListarTipoCentro()
        {
            clsResultadoListaMaestros oDatos = new clsResultadoListaMaestros();
            oDatos = await _Consulta.ListarMaestros(clsConsulta.TipoMaestroEnum.TipoCentro);

            return Json(oDatos.Datos, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ListarDistritos()
        {
            clsResultadoListaMaestros oDatos = new clsResultadoListaMaestros();

            oDatos = await _Consulta.ListarMaestros(clsConsulta.TipoMaestroEnum.Distritos);

            return Json(oDatos.Datos, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ListarLugaresPago(string TipoCentro, string Distrito)
        {
            clsLugaresPago oDatos = new clsLugaresPago();
            clsParametroConsulta oParametro = new clsParametroConsulta();
            StringBuilder sValores = new StringBuilder();
            sValores.Append(string.Format("idtipo|{0};", $"{TipoCentro}"));
            sValores.Append(string.Format("iddistrito|{0}", $"{Distrito}"));
            oParametro.ParametroControlTag = sValores.ToString();

            oDatos = await _Consulta.LugaresPago(oParametro);

            return Json(oDatos, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> DetalleCentro(string IdCentro)
        {
            clsDetalleCentro oDatos = new clsDetalleCentro();
            clsParametroConsulta oParametro = new clsParametroConsulta();
            StringBuilder sValores = new StringBuilder();
            sValores.Append(string.Format("idcentro|{0}", $"{IdCentro}"));
            oParametro.ParametroControlTag = sValores.ToString();

            oDatos = await _Consulta.DetalleCentro(oParametro);

            return Json(oDatos, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Interrupciones()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> TerminosCondiciones()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> PoliticasDevoluciones()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Atenciones()
        {
            return View();
        }

        public async Task<ActionResult> MiContacto()
        {
            return View();
        }

        public async Task<ActionResult> MiBono()
        {
            return View();
        }

        public async Task<ActionResult> TerminosMiContacto()
        {
            return View();
        }

        #endregion ActionResult

        #region JsonResult

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> Interrupciones(clsParametro param)
        {
            var result = new clsResultado();

            try
            {
                result = await _Atencion.ListarInterrupciones(param);

                result.Datos = _Convert.ToJson(result.Datos);
            }
            catch (Exception ex)
            {
                result.SetError(1, ex.Message);
            }

            return Json(result);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> AyudaLista(clsParametro param)
        {
            var result = new clsResultado();

            try
            {
                result = await _Consulta.AyudaLista(param);

                if (result.IdError == 0)
                    result.Datos = _Convert.ToObject<List<Dictionary<String, String>>>(result.Datos.ToString());
            }
            catch (Exception ex)
            {
                result.SetError(1, ex.Message);
            }

            return Json(result);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> Atenciones(clsParametro param)
        {
            var result = await _Atencion.ListarByNroAtencion(param);

            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> MiBonoAsync(clsParametro param)
        {
            var result = (clsResultado)null;
            if (!param.Parametros.ContainsKey("IdException") || param.Parametros["IdException"] != "F8E78531-A4D8-4711-9E99-CF197D635024")
            {
                result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
                if (result != null) return Json(result);
            }

            var idType = param.Parametros["IdType"];

            if (idType == "1") result = await _Atencion.ObtenerBonoElectrico(param);
            else result = await _Atencion.ObtenerBonoElectricoDNI(param);

            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        #endregion JsonResult
    }
}