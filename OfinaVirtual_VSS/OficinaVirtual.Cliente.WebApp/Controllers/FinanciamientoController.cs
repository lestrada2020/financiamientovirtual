﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OficinaVirtual.Entidad.Consulta;
using OficinaVirtual.Fachada.Consulta;
using OficinaVirtual.Util.Tool;
using ReportManagement;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OficinaVirtual.Cliente.WebApp.Controllers
{
    public class FinanciamientoController : PdfViewController
    {
        #region Fields

        private static readonly clsConvert _Convert = clsConvert.Instancia;
        private static readonly clsAccount _Account = clsAccount.Instancia;
        private static readonly clsAtencion _Atencion = clsAtencion.Instancia;
        private static readonly clsConsulta _Consulta = clsConsulta.Instancia;
        private static readonly clsRecaudacion _Recaudacion = clsRecaudacion.Instancia;
        private static readonly clsVisa _Visa = clsVisa.Instancia;
        private static readonly String _TokenDistriluz = ConfigurationManager.AppSettings["TokenDistriluz"];
        private static readonly String _IdProveedorVisa = ConfigurationManager.AppSettings["IdProveedorVisa"];
        private static bool isRedirectRegistro = false;
        private static bool isRedirectPago = false;

        [System.ComponentModel.Browsable(false)]
        public bool IsPostBack { get; }
        #endregion Fields

        #region Querys

        [HttpPost]
        public async Task<JsonResult> ObtenerPorcentajesConvenio(clsParametro param)
        {
            var result = await _Atencion.ObtenerPorcentajesConvenio(param);
            if (result.IdError == 0) result.Datos = _Convert.ToJson(result.Datos);
            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerCentroServicios(clsParametro param)
        {
            var result = await _Atencion.ObtenerCentroServicios(param);
            if (result.IdError == 0) result.Datos = _Convert.ToJson(result.Datos);
            return Json(result);
        }



        [HttpPost]
        public async Task<JsonResult> ObtenerDetalleDeuda(string idSuministro)
        {
            var requestDetalleDeudaValor = new clsObtenerDetalleDeudaParametros() { IdNroServicio = idSuministro };
            var requestDetalleDeuda = new clsObtenerDetalleDeudaRequest() { Parametros = requestDetalleDeudaValor, };
            var lsDetalleDeuda = await _Atencion.ObtenerDetalleDeuda(requestDetalleDeuda);
            var result = GetDetalle(lsDetalleDeuda); //  GetDetalle(lsDetalleDeuda);

            var jsonData = new
            {
                data = result,
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerTerminos(clsParametro param)
        {
            var result = await _Atencion.ObtenerPorcentajesConvenio(param);
            if (result.IdError == 0) result.Datos = _Convert.ToJson(result.Datos);
            return Json(result);
        }

        public async Task<ActionResult> Resumen()
        {
            Session.Remove("Suministro");
            Session.Remove("Datos");
            Session.Remove("Paso");
            Session.Remove("NroPedido");
            Session.Remove("TipoMonto");
            Session.Remove("Monto");
            Session.Remove("SessionToken");
            Session.Remove("SessionKey");
            Session.Remove("TransactionToken");
            Session.Remove("CustomerEmail");
            Session.Remove("Mensaje");
            Session.Remove("Cerrar");

            if (Session["IdUsuario"] != null)
            {
                var lsResFin = new ClsListaFinanciamiento();
                var param = new clsParametro() { Parametros = new Dictionary<String, String>() };
                param.Parametros["IdUsuario"] = Session["IdUsuario"].ToString();
                lsResFin = await _Atencion.ListarSuministroAsociado(param);

                #region Distriluz-036 Video
                foreach (var item in lsResFin.Datos)
                {
                    if (item.PuedeFinanciar == 1)
                    {
                        var requestVideoValor = new clsObtenerConfiguracionGlobalParametros() { Nombre = "VideoFinanciamiento" };
                        var requestVideo = new clsObtenerConfiguracionGlobalRequest() { Parametros = requestVideoValor, };
                        var response = await _Atencion.ObtenerConfiguracionGlobal(requestVideo);
                        ViewBag.urlVideo = response.Datos.Count > 0 ? response.Datos[0].ColumnaValor : null;
                        break;
                    }
                }
                #endregion

                isRedirectRegistro = false;
                isRedirectPago = false;
                return View(lsResFin.Datos);
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> Registro(string idSuministro)
        {
            var lsResFin = new ClsListaFinanciamientoDetalle();
            ViewBag.IdOrder = idSuministro;
            ViewBag.IdUser = Request.Form.Get("IdUser");
            ViewBag.IdNroServicio = Request.Form.Get("Suministro");
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            //   public const string RefinanciamientoVirtual = "RefVirt";

            if (idSuministro != null)
            {
                Session["IdNroServicio"] = idSuministro;
            }

            if (Session["IdUsuario"] != null)
            {
                if (isRedirectRegistro)
                {
                    return RedirectToAction("Resumen", "Financiamiento");
                }
                var requestVideoValor = new clsObtenerConfiguracionGlobalParametros() { Nombre = "TerminosCondicionesFinanciamiento" };
                var requestVideo = new clsObtenerConfiguracionGlobalRequest() { Parametros = requestVideoValor, };
                var response = await _Atencion.ObtenerConfiguracionGlobal(requestVideo);
                ViewBag.urlTerminosCondiciones = response.Datos.Count > 0 ? response.Datos[0].ColumnaValor : null;

                var param = new clsParametro() { Parametros = new Dictionary<String, String>() };
                param.Parametros["idNroServicio"] = Session["IdNroServicio"].ToString();
                param.Parametros["abreviaturaconvenio"] = "RefVirt";

                lsResFin = await _Atencion.ObtenerDetalleFinanciamiento(param);

                if (lsResFin.Datos.Count == 0)
                {
                    return RedirectToAction("Resumen", "Financiamiento");
                }

                if (lsResFin.Datos[0].ValorTasaInteres == 0)
                {
                    ViewBag.ErrorMessage = "No existe tasa de interés para la fecha";
                }

                isRedirectRegistro = true;
                return View(lsResFin.Datos);
            }
            else
                return RedirectToAction("Login", "Account");
        }

        #endregion

        #region MiPago
        public async Task<ActionResult> GenerarOrdenCobro(clsParametroOrdenCobro param)
        {
            var paramet = new clsParametro() { Parametros = new Dictionary<String, String>() };
            paramet.Parametros["SessionToken"] = ConfigurationManager.AppSettings["TokenDistriluz"];
            paramet.Parametros["IdNroServicio"] = param.IdNroServicio.ToString();
            paramet.Parametros["MontoFinanciar"] = param.MontoFinanciar.ToString();
            paramet.Parametros["MontoInicial"] = param.MontoInicial.ToString();
            paramet.Parametros["InicialExigida"] = param.InicialExigida.ToString();
            paramet.Parametros["TotalFacturado"] = param.TotalFacturado.ToString();
            paramet.Parametros["InteresAFecha"] = param.InteresAFecha.ToString();
            paramet.Parametros["Porcentaje"] = param.Porcentaje.ToString();
            paramet.Parametros["TotalPorFacturar"] = param.TotalPorFacturar.ToString();
            paramet.Parametros["TotalSaldoAFavor"] = param.TotalSaldoAFavor.ToString();
            paramet.Parametros["SaldoAFavorUsar"] = param.SaldoAFavorUsar.ToString();
            paramet.Parametros["NumeroCuotas"] = param.NumeroCuotas.ToString();
            paramet.Parametros["PlazoMaximo"] = param.PlazoMaximo.ToString();
            paramet.Parametros["ImporteCuota"] = param.ImporteCuota.ToString();
            paramet.Parametros["IdEstado"] = param.IdEstado.ToString();
            paramet.Parametros["IdUUNN"] = param.IdUUNN.ToString();
            paramet.Parametros["IdCentroServicio"] = param.IdCentroServicio.ToString();
            paramet.Parametros["IdConvenioTipo"] = param.IdConvenioTipo.ToString();
            paramet.Parametros["IdMoneda"] = param.IdMoneda.ToString();
            paramet.Parametros["IdEmpresa"] = param.IdEmpresa.ToString();
            var result = await _Atencion.GenerarOrdenCobro(paramet);

            if (result.Datos == null)
            {
                ViewBag.IdOrdenCobro = 0;
                Session["idOrdenCobro"] = 0;
                ViewBag.ErrorMessage = result.Mensaje;
            }
            else
            {
                string idOrdenCobro = result.Datos.ToString();
                ViewBag.IdOrder = idOrdenCobro;
                Session["idOrdenCobro"] = idOrdenCobro;
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
                ViewBag.IdOrdenCobro = Session["idOrdenCobro"];
            }

            ViewBag.IdInteresGenerado = (Int16)Entidad.General.TipoDocumento.InteresGenerado;
            ViewBag.IdInteresGeneradoJuridico = (Int16)Entidad.General.TipoDocumento.InteresGeneradoJuridico;
            ViewBag.IdUser = Request.Form.Get("IdUser");
            ViewBag.IdNroServicio = Request.Form.Get("Suministro");

            if (Session["IdUsuario"] != null && Session["idOrdenCobro"] != null)
            {
                if (isRedirectPago)
                {
                    return RedirectToAction("Resumen", "Financiamiento");
                }

                var requestVideoValor = new clsObtenerConfiguracionGlobalParametros() { Nombre = "TerminosCondicionesFinanciamientoNiubiz" };
                var requestVideo = new clsObtenerConfiguracionGlobalRequest() { Parametros = requestVideoValor, };
                var response = await _Atencion.ObtenerConfiguracionGlobal(requestVideo);
                ViewBag.urlTerminosCondiciones = response.Datos.Count > 0 ? response.Datos[0].ColumnaValor : null;

                isRedirectPago = true;
                return View();
            }
            else
                return RedirectToAction("Login", "Account");
        }


        public async Task<ActionResult> MiPagoNiubiz(string id, string sender)
        {
            ViewBag.IdOrder = id;
            ViewBag.IdOrden = sender;
            ViewBag.IdUser = Request.Form.Get("IdUser");
            ViewBag.IdNroServicio = Request.Form.Get("Suministro");
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> ObtenerSuministroOrdenCobro(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            clsSuministroOrdenCobroResponse response = await _Atencion.DatosSuministroOrdenCobro(param);

            if (response.IdError == 0)
            {
                var nombre = response.Datos.nombre;
                var direccion = response.Datos.direccion;
                response.Datos.nombre = nombre.Length <= 6 ? nombre : $"{nombre.Substring(0, 3)} *** {nombre.Substring(nombre.Length - 3, 3)}";
                response.Datos.direccion = direccion.Length <= 6 ? direccion : $"{direccion.Substring(0, 3)} *** {direccion.Substring(direccion.Length - 3, 3)}";
            }

            return Json(response);
        }


        [HttpPost]
        public async Task<JsonResult> ObtenerDetalleOrdenCobro(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.ObtenerDetalleOrdenCobro(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> GenerarPedido(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.GenerarNroPedido(param);
            if (result.IdError > 0) return Json(result);
            result.Datos = _Convert.ToJson(result.Datos);


            var order = _Convert.ToObject<Dictionary<String, String>>(result.Datos.ToString());
            var idEmpresa = Convert.ToInt16(param.Parametros["IdEmpresa"]);
            var idNroServicio = Convert.ToInt32(param.Parametros["IdNroServicio"]);
            var amount = Convert.ToDecimal(param.Parametros["Importe"]);
            var ip = Request.UserHostAddress;
            var idType = (Int16)2;

            #region Pay

            var payToken = await _Visa.CreateSession(idEmpresa, idType, null, amount, ip, idNroServicio);
            var pay = new
            {
                Src = _Visa.GetURLButtonJS(idEmpresa),
                TimeOut = payToken["expirationTime"],
                Data = new
                {
                    sessiontoken = payToken["sessionKey"],
                    merchantid = _Visa.GetMerchantId(idEmpresa, idType),
                    purchasenumber = order["NroPedido"],
                    amount = amount
                }
            };

            #endregion Pay

            #region Result

            var data = new
            {
                Pay = pay,
                Order = order
            };

            result = new clsResultado();
            result.Datos = _Convert.ToJson(data);

            #endregion Result

            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerMetodoPago(clsParametro param)
        {
            var result = await ValidCaptcha(param.Parametros["TokenCaptcha"]);
            if (result != null) return Json(result);

            result = await _Atencion.ObtenerMetodoPago(param);
            if (result.IdError > 0) return Json(result);


            var method = _Convert.ToObject<List<ClsMetodoPago>>(result.Datos.ToString());
            object payOrder = new string[0];

            string nroPedido = param.Parametros["NroPedido"];
            foreach (var item in method)
            {
                if (item.IdMetodo == (int)MetodoPago.PagoNiubiz)
                {
                    var resul_Datos = string.Format(" NroPedido: {0} ", nroPedido);
                    resul_Datos = "{ " + resul_Datos + " }";

                    var order = _Convert.ToObject<Dictionary<String, String>>(resul_Datos.ToString());
                    var idEmpresa = Convert.ToInt16(param.Parametros["IdEmpresa"]);
                    var idNroServicio = Convert.ToInt32(param.Parametros["IdNroServicio"]);
                    var amount = Convert.ToDecimal(param.Parametros["Importe"]);
                    var ip = Request.UserHostAddress;
                    var idType = (Int16)2;

                    #region Pay

                    var payToken = await _Visa.CreateSession(idEmpresa, idType, null, amount, ip, idNroServicio);
                    var pay = new
                    {
                        Src = _Visa.GetURLButtonJS(idEmpresa),
                        TimeOut = payToken["expirationTime"],
                        Data = new
                        {
                            sessiontoken = payToken["sessionKey"],
                            merchantid = _Visa.GetMerchantId(idEmpresa, idType),
                            purchasenumber = order["NroPedido"],
                            amount = amount
                        }
                    };

                    #endregion Pay

                    #region Result

                    var data = new
                    {
                        Pay = pay,
                        Order = order
                    };

                    payOrder = data;
                    //payOrder = _Convert.ToJson(data);

                    #endregion Result

                    break;
                }
            }

            var dataResult = new
            {
                NroPedido = nroPedido,
                Method = result.Datos,
                PayOrder = payOrder,
            };

            result = new clsResultado();
            result.Datos = _Convert.ToJson(dataResult);

            return Json(result);
        }

        public async Task<ActionResult> MyPay(string id, string sender, string idMetodo)
        {
            var redirectTo = RedirectToAction("MiPagoNiubiz", new { id = id, sender = sender });

            if (!Request.Form.AllKeys.Contains("transactionToken")) return redirectTo;

            try
            {
                var idOrder = Convert.ToInt32(id);

                dynamic intent = new JObject();
                var response = (clsResultado)null;
                var responseRecauda = (clsResultadoRecaudacion)null;
                var isOK = false;
                var errorMessage = "";
                var intentBD = (Dictionary<String, String>)null;

                #region Obtener Intento

                var p = new Dictionary<String, String>();
                p["NroPedido"] = idOrder.ToString();
                response = await _Atencion.ObtenerIntento(new clsParametro { Parametros = p });

                if (response.IdError > 0) throw new Exception($"<<<Tuvimos problemas con su pago. Favor de volver a intentarlo.>>>{responseRecauda.Mensaje}");

                intentBD = _Convert.ToObject<Dictionary<String, String>>(response.Datos.ToString());

                if (intentBD["IdEstado"] != "1") return redirectTo;

                #endregion Obtener Intento

                var idEmpresa = Convert.ToInt16(intentBD["IdEmpresa"]);
                var amount = Convert.ToDecimal(intentBD["Importe"]);
                var payToken = Request.Form["transactionToken"];
                var payEmail = Request.Form["customerEmail"];

                #region Intento 1

                intent.SessionToken = _TokenDistriluz;
                intent.NroPedido = idOrder;
                intent.Email = payEmail;
                intent.TransactionTokenVisa = payToken;
                intent.SessionTokenVisa = String.Empty;
                intent.JsonPagoVisa = (Object)null;
                intent.JsonPagoResumen = (Object)null;
                intent.IdPaso = "1";
                intent.IdEstadoPaso = "1";
                intent.MensajePaso = "Pasarela NIUBIZ - OK";

                responseRecauda = await _Atencion.IntentoPagoActualizarFinanciamiento(new clsParametroEntrada { Parametros = intent });

                if (responseRecauda.IdError > 0) throw new Exception($"<<<Tuvimos problemas con su pago. Favor de volver a intentarlo.>>>{responseRecauda.Mensaje}");

                #endregion Intento 1

                #region Pago NIUBIZ

                var token = await _Visa.CreateToken(idEmpresa, 2);
                response = await _Visa.AuthorizeSession(idEmpresa, 2, token, amount, idOrder, payToken);
                errorMessage = response.Mensaje;

                isOK = errorMessage == null;

                #endregion Pago NIUBIZ

                #region Intento 2

                intent.JsonPagoVisa = isOK ? response.Datos.ToString() : errorMessage;
                intent.IdPaso = "2";
                intent.IdEstadoPaso = isOK ? "1" : "0";
                intent.MensajePaso = isOK ? "NIUBIZ - OK" : "NIUBIZ - ERROR";

                #region Action Code

                if (!isOK)
                {
                    try
                    {
                        var error1 = _Convert.ToObject<Dictionary<String, Object>>(errorMessage);
                        if (error1.ContainsKey("errorMessage")) errorMessage = error1["errorMessage"].ToString();

                        var error2 = _Convert.ToObject<Dictionary<String, String>>(error1["data"].ToString());
                        if (error2.ContainsKey("ACTION_DESCRIPTION")) errorMessage = error2["ACTION_DESCRIPTION"];

                        var ac = Convert.ToInt32(error2["ACTION_CODE"]);

                        var param = new Dictionary<String, String>();
                        param.Add("IdProveedor", _IdProveedorVisa);
                        param.Add("IdObservacion", ac.ToString());
                        var res = await _Atencion.ObtenerObservacion(new clsParametro() { Parametros = param });

                        if (res.Datos != null)
                        {
                            var res2 = _Convert.ToObject<Dictionary<String, String>>(res.Datos.ToString());
                            if (res2.ContainsKey("MensajeCliente")) errorMessage = res2["MensajeCliente"];
                            if (res2.ContainsKey("MensajeProveedor")) intent.MensajePaso = $"[{ac}] - " + res2["MensajeProveedor"];
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMessage = "Operación Denegada. Contactar con entidad emisora de su tarjeta.";
                    }
                }

                #endregion Action Code

                try
                {
                    await _Atencion.IntentoPagoActualizarFinanciamiento(new clsParametroEntrada { Parametros = intent });
                }
                catch (Exception ex) { }

                #endregion Intento 2

                if (!isOK) throw new Exception($"<<<{errorMessage}>>>");

                #region Pago Distriluz

                var rqPay = new
                {
                    NroPedido = idOrder,
                    IdOrdenCobro = sender,
                    SessionToken = _TokenDistriluz,
                    SessionTokenVisa = "",
                    MetodoPago = idMetodo
                };
                responseRecauda = await _Recaudacion.PagarDeudaFinanciamiento(new clsParametroEntrada { Parametros = rqPay });

                isOK = responseRecauda.IdError == 0;
                errorMessage = responseRecauda.Mensaje;

                if (!isOK)
                {
                    responseRecauda = await _Recaudacion.PagarDeudaFinanciamiento(new clsParametroEntrada { Parametros = rqPay });

                    isOK = responseRecauda.IdError == 0;
                    errorMessage = responseRecauda.Mensaje;
                }

                try
                {
                    if (isOK) await _Recaudacion.FinanciamientoEnviarEmail(new clsParametroEntrada { Parametros = rqPay });
                }
                catch (Exception) { }

                #endregion Pago Distriluz

                #region Intento 3

                if (isOK)
                {
                    intent.JsonPagoResumen = intent.JsonPagoVisa;
                    intent.IdPaso = "3";
                    intent.IdEstadoPaso = "1";
                    intent.MensajePaso = "DISTRILUZ - OK";

                    try
                    {
                        await _Recaudacion.IntentoPagoActualizar(new clsParametroEntrada { Parametros = intent });
                    }
                    catch (Exception ex) { }
                }

                #endregion Intento 3
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
            }

            return redirectTo;
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerDocumentoComprobantePago(clsParametro param)
        {
            clsResultado result = await _Atencion.ObtenerDocumentoComprobantePago(param);
            result.Datos = _Convert.ToJson(result.Datos);

            return Json(result);
        }

        [HttpPost]
        public async Task<FileResult> descargarArchivo(clsParametro param)
        {
            var bytes = (Byte[])null;
            bytes = Convert.FromBase64String(param.Parametros["fullName"]);
            string filePath = param.Parametros["fullName"];
            string type = param.Parametros["docType"];

            Response.AddHeader("Content-Disposition", $"InLine; FileName={filePath}");


            return File(bytes, MimeMapping.GetMimeMapping(filePath));

            //return File(filePath, $"application/{type}");
        }

        [HttpPost]
        public async Task<JsonResult> ObtenerRutaTransaccion(clsParametro param)
        {
            clsResultado result = await _Atencion.ObtenerRutaTransaccion(param);
            result.Datos = _Convert.ToJson(result.Datos);
            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> MiVoucher(clsParametro param)
        {
            clsResultado result = await _Atencion.GenerarPdf(param);
            string Uri = (string)result.Datos;
            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ValidarEstadoPagoDistriluz(clsParametro param)
        {
            clsResultado result = await _Atencion.EstadoPagoDistriluz(param);
            return Json(result);
        }


        #endregion

        #region Funciones
        private List<string[]> GetDetalle(clsListaDetalleDeuda obj)
        {
            var listArrayString = new List<string[]>();
            string[] arrayString;
            if (obj.IdError == 0)
            {
                foreach (var item in obj.Datos)
                {
                    arrayString = new string[9];
                    arrayString[0] = item.nombredocumento;
                    arrayString[1] = item.nrodocumento;
                    arrayString[2] = item.periodo.ToString();
                    arrayString[3] = item.fechavencimiento.ToString("dd/MM/yyyy");
                    arrayString[4] = item.importetotal.ToString("#,##0.00");
                    arrayString[5] = item.saldo.ToString("#,##0.00");
                    arrayString[6] = item.pagado.ToString("#,##0.00");
                    arrayString[7] = item.saldofinanciar.ToString("#,##0.00");
                    arrayString[8] = item.descripcionEstado;
                    listArrayString.Add(arrayString);
                }
            }
            return listArrayString;
        }

        #endregion

        #region Private

        private async Task<clsResultado> ValidCaptcha(String response)
        {
            var r = new clsResultado();

            try
            {
                var a = await _Consulta.ValidCaptcha(response);
                var b = _Convert.ToObject<Dictionary<String, Object>>(a.Datos.ToString());
                var c = Convert.ToBoolean(b["success"]);

                if (c) r = null;
                else throw new Exception(b["error-codes"].ToString());
            }
            catch (Exception ex)
            {
                r.IdError = 1;
                r.Mensaje = "Favor de reintentar.";
                r.Datos = ex.Message;
            }

            return r;
        }

        #endregion Private


    }
}