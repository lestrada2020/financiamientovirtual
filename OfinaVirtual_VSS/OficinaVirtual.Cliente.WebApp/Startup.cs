﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OficinaVirtual.Cliente.WebApp.Startup))]
namespace OficinaVirtual.Cliente.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
