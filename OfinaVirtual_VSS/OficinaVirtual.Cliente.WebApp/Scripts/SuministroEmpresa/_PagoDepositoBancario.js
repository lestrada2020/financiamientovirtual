﻿
$(document).ready(function () {

    $(".frmDeposito").submit(function (event) {
        event.preventDefault();
        click_btnPagar($(this));
    });

    $('#btnSalirDetalleSuministro').click(btnSalir_click);
    $('#btnPagar').click(btnPagar_click);

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-colour-1').addClass('btn-colour-2');
            $item.addClass('btn-colour-1');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }


    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-colour-1').trigger('click');

});



function click_btnPagar(oForm) {

    var a = $('#modDepositoBancario');
    var empresa = $('#Empresa').val();


    $('.modPreload').modal('show');

    jQuery.ajax({
        type: oForm[0].method,
        url: oForm[0].action,
        data: $(oForm[0]).serialize(),
        dataType: 'HTML',
        success: function (oDatos) {


            $('.modPreload').modal('hide');

            a.find('.modal-body').html(oDatos);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');

            var idbanco = $("#Banco").val();
            if (idbanco === undefined) {
                Buscar_Suministro_Por_Empresa(empresa);
            }

            setTimeout(function () { $('div.setup-panel div a.btn-colour-2').click() }, 10);
            var cuentaseleccionada = $('#CuentaSeleccionada').attr("value");
            LLenarListaCuenta(cuentaseleccionada);

        },
        error: function (xhr) {
       //     debugger;
            MsgBox("Error de Sistema", "Error");
            $('#modPreload').modal('hide');
        }
    });
}



function LLenarListaCuenta(cuentaseleccionada) {

    var idbanco = $("#Banco").val();
    var idempresa = $('#Empresa').attr("value");

    if (idbanco !== undefined) {

        var Parametros = {
            Parametros: {
                Banco: idbanco,
                Empresa: idempresa,
            }
        };

        jQuery.ajax({

            type: 'POST',
            url: "/SuministroEmpresa/ListarCuentas",
            data: Parametros,
            beforeSend: function () {

                $('#modPreload').modal('show');

            },
            success: function (data) {

             
                $('#Cuenta').empty();
                var items = "";

                for (var i = 0; data.data.length > i; i++) {

                    if (cuentaseleccionada === data.data[i].idCuentaBancaria) {
                        items = items + '<option value=' + data.data[i].idCuentaBancaria + ' selected>' + data.data[i].NroCuenta + '</option>';

                    }
                    else {
                        items = items + '<option value=' + data.data[i].idCuentaBancaria + ' >' + data.data[i].NroCuenta + '</option>';
                    }
                }

                document.getElementById('Cuenta').innerHTML = "";
                document.getElementById('Cuenta').innerHTML = items;

                $('#modPreload').modal('hide');

            },
            error: function (xhr) {

                $('#modPreload').modal('hide');
                MsgBox("Error de Sistema", "Error");
            }
        });


    }

}

function btnSalir_click() {

    var a = $('#modDepositoBancario');
    a.modal('hide');

}

$("#Monto").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value) {
            return value.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
});

$('#Banco').change(function () {

    var idbanco = $("#Banco").val();
    var idempresa = $("#empresaid").val();


    var Parametros = {
        Parametros: {
            Banco: idbanco,
            Empresa: idempresa,
        }
    };

    jQuery.ajax({

        type: 'POST',
        url: "/SuministroEmpresa/ListarCuentas",
        data: Parametros,
        beforeSend: function () {

            $('#modPreload').modal('show');

        },
        success: function (data) {

            $('#Cuenta').empty();
            var items = "";

            for (var i = 0; data.data.length > i; i++) {
                items = items + '<option value=' + data.data[i].idCuentaBancaria + ' >' + data.data[i].NroCuenta + '</option>';
            }

            document.getElementById('Cuenta').innerHTML = "";
            document.getElementById('Cuenta').innerHTML = items;

            $('#modPreload').modal('hide');

        },
        error: function (xhr) {


            $('#modPreload').modal('hide');
            MsgBox("Error de Sistema", "Error");
        }
    });

})

