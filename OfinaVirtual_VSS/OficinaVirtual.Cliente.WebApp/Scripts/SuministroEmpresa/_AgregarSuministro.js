﻿$(document).ready(inicialize);

function inicialize() {

    $(".frmConsultar").submit(function (event) {
        event.preventDefault();
        click_btnBuscar($(this));
    });

    $(".frmRegistrar").submit(function (event) {
        event.preventDefault();
        click_btnRegistrar($(this));
    });

}

function click_btnBuscar(oForm) {

    var a = $('#modDetalleSuministro');
    a.find('div.modal-dialog').removeClass('modal-md').addClass('modal-lg');
    
    $('.modPreload').modal('show');


    jQuery.ajax({
        type: oForm[0].method,
        url: oForm[0].action,
        data: $(oForm[0]).serialize(),
        dataType: 'HTML',
        success: function (oDatos) {

            $('.modPreload').modal('hide');

            a.find('.modal-title').html("Agregar Suministro - " + oForm[0][1].value);
            a.find('.modal-body').html(oDatos);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');

        },
        error: function (xhr) {
            debugger;

            $('#modPreload').modal('hide');
        }
    });
}

function click_btnRegistrar(oForm) {

    var a = $('#modDetalleSuministro');
    a.find('div.modal-dialog').removeClass('modal-md').addClass('modal-lg');


    $('.modPreload').modal('show');

    jQuery.ajax({
        type: oForm[0].method,
        url: oForm[0].action,
        data: $(oForm[0]).serialize(),
        dataType: 'HTML',
        success: function (oDatos) {

            $('.modPreload').modal('hide');

            a.find('.modal-title').html("Agregar Suministro - " + oForm[0][1].value);
            a.find('.modal-body').html(oDatos);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');

            Buscar_Suministro_Por_Empresa(oForm[0][1].value);

        },
        error: function (xhr) {
            debugger;

            $('#modPreload').modal('hide');
        }
    });



    //jQuery.ajax({
    //    type: oForm[0].method,
    //    url: oForm[0].action,
    //    data: $(oForm[0]).serialize(),
    //    dataType: 'HTML',
    //    success: function (oDatos) {
    //        jQuery('.modResumen').find('.modal-body').html(oDatos);
    //        jQuery(".modResumen").fadeTo(1000, 100).slideUp(1000, function () {               
    //            jQuery('.frmResumen').submit();
    //        });
    //    }
    //})






}