﻿/// <reference path="../jquery-1.10.2.min.js" />

var DeudaEnsa = new Array();
var DeudaEnosa = new Array();
var DeudaHidrandina = new Array();
var DeudaElectrocentro = new Array();



$(document).ready(function () {

    //$(".preload").fadeOut(2000, function () {
    //    $("body").fadeIn(1000);
    //});

    var Empresas = ['Ensa', 'Enosa', 'Hidrandina','Electrocentro'];

    Empresas.forEach(function (elemento, indice, array) {


            $('#tbl_listar_Suministro_' + elemento).DataTable({
            searching: false,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "&nbsp;",
                "sEmptyTable": "&nbsp;",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
                },
                info: false, 
            responsive: true,
            pageLength: 10,
                lengthChange: false


        });

    });

    $('.btnBuscar').click(btnBuscar_click);
    $('.btnLimpiar').click(btnLimpiar_click);
    $('.AgregarSuministro').click(btnAgregarSuministro_click);
    $('.btnProcesarArchivo').click(btnProcesarArchivo_click);
    $('.btnPagar').click(btnPagar_click);
    $('#btnSalirmodPago').click(btnSalir_click);

    $("body").off('click', '.verDetalle');
    $("body").on('click', '.verDetalle', function () {    

        var a = $('#modDetalleSuministro');
        a.find('div.modal-dialog').removeClass('modal-md').addClass('modal-lg');

        var suministro =  $(this).attr('id');

        suministro = suministro.replace("verDetalle_", "");

        var obj = new Object();
        obj.suministro = suministro;

        jQuery.ajax({

            type: 'POST',
            url: "/SuministroEmpresa/_VerDetallePagosSuministros",
            data: obj,
            dataType: 'json',
            beforeSend: function () {

                $('#modPreload').modal('show');

            },
            success: function (data) {


                a.find('.modal-title').html("Periodos de Suministro");
                a.find('.modal-body').html(data.Html);
                a.modal({ backdrop: 'static', keyboard: true });
                a.modal('show');

                ///////////////////////////////////////////////////////7

                    $('#tbl_listar_Pagos_Suministro').DataTable({
                        data: data.Lista,
                        "language": {
                            "sProcessing": "Procesando...",
                            "sLengthMenu": "Mostrar _MENU_ registros",
                            "sZeroRecords": "No se encontraron resultados",
                            "sEmptyTable": "Ningún dato disponible en esta tabla",
                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                            "sInfoPostFix": "",
                            "sSearch": "Buscar:",
                            "sUrl": "",
                            "sInfoThousands": ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        },
                        responsive: true,
                        pageLength: 5,
                        lengthChange: false,

                        "columns": [

                            { "data": "NroSuministro" },
                            { "data": "NroRecibo" },
                            { "data": "Periodo" },
                            { "data": "FechaEmision" },
                            { "data": "FechaVencimiento" },
                            { "data": "ImportePago" }

                            ,

                             {
                                 data: "NroSuministro",
                                render: function (data, type, row) {

                                    if (type === 'display') {
                                        return '<img class="VerReciboPdf" name= "' + row.Periodo +'" id = "VerReciboPdf_' + data +'" src="/Content/img/pdf_32.png" style="cursor:pointer;"/>';
                                    }
                                    return data;

                                }
                            }

                      
                        ]

                    });


                $('#modPreload').modal('hide');

            },
            error: function (xhr) {

                $('#modPreload').modal('hide');
                MsgBox("Error de Sistema", "Error");

            }
        });

    });

    $("body").off('click', '.VerReciboPdf');
    $("body").on('click', '.VerReciboPdf', function () {    

        var suministro = $(this).attr('id');
        var periodo = $(this).attr('name');

        suministro = suministro.replace("VerReciboPdf_", "");


        var obj = {
            Parametros: {
                IdTipoDocumento : "1",
                IdNroServicio: suministro,
                IdPeriodo: periodo
            }
        };


        jQuery.ajax({

            type: 'POST',
            url: "/Suministro/ObtenerRecibo",
            data: obj,
            beforeSend: function () {

                $('#modPreload').modal('show');

            },
            success: function (data) {

                var win = window.open();
                win.document.write('<embed width= "100%" height= "100%" src="data:application/pdf;base64,' + data.Datos + '"type="application/pdf"/>');
                win.document.title = obj.Parametros.IdNroServicio + "_" + obj.Parametros.IdPeriodo;

                $('#modPreload').modal('hide');

            },
            error: function (xhr) {

                $('#modDetalleSuministro').modal('hide');
                $('#modPreload').modal('hide');
                MsgBox("Error de Sistema.", "Error");
            }
        });




    });

    $("body").off('click', '.eliminarSuministro');
    $("body").on('click', '.eliminarSuministro', function () {

        var suministro = $(this).attr('id');
        var empresa = $(this).attr('name');


        suministro = suministro.replace("eliminarSuministro_", "");

        var question = '¿Deseas quitar el suministro <strong>' + suministro +'</strong> ?';

        var callback = function () {

        var obj = new Object();
            obj.suministro = suministro;

         jQuery.ajax({
                    type: 'POST',
                    url: "/SuministroEmpresa/EliminarSuministroEmpresa",
                    data: obj,
                    dataType: 'json',
                    beforeSend: function () {

                        $('#modPreload').modal('show');

                    },
                    success: function (data) {

                        Buscar_Suministro_Por_Empresa(empresa);

                        $('#modPreload').modal('hide');

                    },
                    error: function (xhr) {

                        $('#modPreload').modal('hide');
                        MsgBox("Error de Sistema", "Error");
                    }
                });


        };

        confirm(question, "", "", callback);

    });

    $("body").off('click', '.aReclamar');
    $("body").on('click', '.aReclamar', function () {

        var suministro = $(this).attr('id');

        var a = $('#modReclamo');

        suministro = suministro.replace("aReclamar_", "");

        var obj = {
            Parametros: {
                Suministro: suministro
            }
        };

        jQuery.ajax({

            type: 'POST',
            url: "/SuministroEmpresa/_RegistrarReclamo",
            data: obj,
            beforeSend: function () {

                $('#modPreload').modal('show');

            },
            success: function (data) {


                a.find('.modal-title').html("Registrar Reclamo");
                a.find('.modal-body').html(data);

                a.modal({ backdrop: 'static', keyboard: true });
                a.modal('show');

                ///////////////////////////////////////////////////////7


                $('#modPreload').modal('hide');

            },
            error: function (xhr) {

                $('#modPreload').modal('hide');
                MsgBox("Error de Sistema", "Error");
            }
        });




    });

    $("body").off('change', '.chkdeudaminima');
    $("body").on('change', '.chkdeudaminima', function () {    

        var empresa = $(this).attr('name');
        var suministro = $(this).attr('id');
        var monto = $(this).attr('value');

        suministro =  suministro.replace("DeudaMinima", "");


        if ($(this)[0].checked === true) {

            $('#DeudaTotal' + suministro).prop('checked', false);

            CalcularDeudaPorEmpresa(suministro, "1", monto, empresa, true);
        }
        else
        {
            CalcularDeudaPorEmpresa(suministro, "1", monto, empresa, false);
        }

   
    });

    $("body").off('change', '.chkdeudatotal');
    $("body").on('change', '.chkdeudatotal', function () {

        var empresa = $(this).attr('name');
        var suministro = $(this).attr('id');
        var monto = $(this).attr('value');

        suministro = suministro.replace("DeudaTotal", "");

        if ($(this)[0].checked === true) {
            $('#DeudaMinima' + suministro).prop('checked', false);

            CalcularDeudaPorEmpresa(suministro, "2", monto, empresa ,true);
        }
        else
        {
            CalcularDeudaPorEmpresa(suministro, "2", monto, empresa, false);
        }

    });
   
    function CalcularDeudaPorEmpresa(suministro, tipo, monto, empresa, check) {
    
        var Total = 0;

        if (check === true) {

            if (eval("Deuda" + empresa).length === 0) {

                eval("Deuda" + empresa).push({ "suministro": suministro, "tipodeuda": tipo, "monto": monto });
                //calcular total
                Total = CalcularTotal(empresa);
                Total = Total.replace(/\D/g, "")
                    .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                $('#' + empresa + 'Monto').val(Total);
                return false;
            }

            else {

                for (var i = 0; i < eval("Deuda" + empresa).length; i++) {

                    if (eval("Deuda" + empresa)[i].suministro === suministro) {

                        //eliminando el elemento antiguo
                        eval("Deuda" + empresa).splice(i, 1);
                        //agregando el nuevo elemento
                        eval("Deuda" + empresa).push({ "suministro": suministro, "tipodeuda": tipo, "monto": monto });
                        //calcular total
                        Total = CalcularTotal(empresa);
                        Total = Total.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                        $('#' + empresa + 'Monto').val(Total);
                        return false;
                    }

                }

                //el elemento es ingresado por primera vez

                eval("Deuda" + empresa).push({ "suministro": suministro, "tipodeuda": tipo, "monto": monto });

                //calcular total

                Total = CalcularTotal(empresa);
                Total = Total.replace(/\D/g, "")
                    .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                $('#' + empresa + 'Monto').val(Total);
                return false;
            }

        }
        else {// eliminar el elemento del array

            for (var e = 0; e < eval("Deuda" + empresa).length; e++) {

                if (eval("Deuda" + empresa)[e].suministro === suministro) {

                    //eliminando el elemento antiguo
                    eval("Deuda" + empresa).splice(e, 1);
                 
                    //calcular total
                    Total = CalcularTotal(empresa);
                    Total = Total.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    $('#' + empresa + 'Monto').val(Total);
                    return false;
                }

            }
        }

    }

    function CalcularTotal(empresa) {

        var total = 0;

        for (var i = 0; i < eval("Deuda" + empresa).length; i++) {
            total = total + parseFloat(eval("Deuda" + empresa)[i].monto); 
        }

        return parseFloat(total).toFixed(2);
    }

});


function btnSalir_click() {
    var a = $('#modPago');
    a.modal('hide');
}

function btnPagar_click() {

    var empresa = $(this).attr('name');
    var empresaid = $(this).attr('id');
    var mediopago = $("#" + empresa + 'MedioPago').val();
    var monto = $("#" + empresa + 'Monto').val();
    var a = $('#modPago');
  

    if (mediopago === "") {
        MsgBox("Seleccione el medio de pago.", "Info");
        return false;
    }

    if (monto === "0.00" || monto === '') {
        MsgBox("No puede realizar el pago con un monto nulo.", "Info");
        return false;
    }


    var array = new Array();


    for (var i = 0; i < eval("Deuda" + empresa).length; i++) {

        array.push({ "suministros": eval("Deuda" + empresa)[i].suministro, "tipodeuda": eval("Deuda" + empresa)[i].tipodeuda, "monto": eval("Deuda" + empresa)[i].monto });

    }


    
    var suministros = JSON.stringify(array);

    var obj = {
        Parametros: {
            Empresa: empresaid,
            Monto: monto,
            Suministros: suministros
        }
    };

    //////////////////PAGO VISA//////////////////////
    if (mediopago === "0") {


        jQuery.ajax({

            type: 'POST',
            url: "/SuministroEmpresa/_ValidarVisa",
            data: obj,
            beforeSend: function () {

                $('#modPreload').modal('show');

            },
            success: function (data) {

                //Almacenando variables

                $('#NroPedido').val(data.data.NumeroPedido);

                var MontoResumen = 0;
                MontoResumen = parseFloat(data.data.Monto);
              
                var parts = MontoResumen.toFixed(2).split(".");
                var num = parts[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +
                    (parts[1] ? "." + parts[1] : "");


                if (data.data !== "") {

                    var params = {
                        action: data.data.Action,
                        amount: data.data.Monto,
                        merchantlogo: "http://www.distriluz.com.pe/templates/lb_hct/images/logo.png",
                        merchantname: "DISTRILUZ",
                        sessiontoken: data.data.VisaSessionKey,
                        purchasenumber: data.data.NumeroPedido,
                        merchantid: data.data.VisaMerchantId,
                        timeouturl: data.data.TimeOutUrl,
                        documenttype: "0",
                        channel: "web"
                    };


                    $('#p_numeropedido').html("<span class='bold'>Número de pedido:</span> " + data.data.NumeroPedido);
                    $('#p_numerosuministros').html("<span class='bold'>Número de suministros seleccionados:</span> " + data.data.SuministroSeleccionados);
                    $('#p_monto').html("<span class='bold'>Ud. ha seleccionado el monto de:</span> " + num);

                    VisanetCheckout.configure(params);

                    a.find('.modal-title').html("Pago de Suministros - " + empresa);
                    $('#modPreload').modal('hide');
                    $('#modPago').modal('show');
                }

                else {
                    $('#modPreload').modal('hide');
                    MsgBox(data.mensaje, "Info");
                }
               

            },
            error: function (xhr) {

                $('#modPreload').modal('hide');
                MsgBox("Error de Sistema", "Error");
            }
        });



    }

    else {

       //////////////////DEPOSITO BANCARIO//////////////////////

        var b = $('#modDepositoBancario');

   
        var Parametros = {
            Parametros: {
                Empresa: empresa,
                Monto: monto,
                Suministros: suministros
            }
        };

        jQuery.ajax({

            type: 'POST',
            url: "/SuministroEmpresa/PagoDepositoBancario",
            data: Parametros,
            beforeSend: function () {

                $('#modPreload').modal('show');

            },
            success: function (data) {

                b.find('.modal-title').html("Pago de Suministros - Depósito Bancario - "+ empresa );
                b.find('.modal-body').html(data);
                b.modal({ backdrop: 'static', keyboard: true });
                b.modal('show');

                $('#modPreload').modal('hide');

            },
            error: function (xhr) {

                $('#modPreload').modal('hide');
                MsgBox("Error de Sistema", "Error");
            }
        });




    }


}


function CalcularResumenCargaExcel(lista,empresa) {

    var Total = 0;

    for (var i = 0; lista.length > i; i++) {


        if (lista[i].TipoDeuda === "T") {

           if (lista[i].DeudaTotal !== "0.0") {

                eval("Deuda" + empresa).push({ "suministro": lista[i].Nro, "tipodeuda": "2", "monto": lista[i].DeudaTotal });
                Total = Total + parseFloat(lista[i].DeudaTotal);

            }
        }
       else {

           if (lista[i].DeudaMinima !== "0.0") {

               eval("Deuda" + empresa).push({ "suministro": lista[i].Nro, "tipodeuda": "1", "monto": lista[i].DeudaMinima });
               Total = Total + parseFloat(lista[i].DeudaMinima);

           }
       }

    }

    Total = CalcularTotalExcel(empresa);

    Total = Total.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");

    $('#' + empresa + 'Monto').val(Total);
    return false;

}

function CalcularTotalExcel(empresa) {

    var total = 0;

    for (var i = 0; i < eval("Deuda" + empresa).length; i++) {
        total = total + parseFloat(eval("Deuda" + empresa)[i].monto);
    }

    return parseFloat(total).toFixed(2);
}

function CalcularResumen(Tipo, empresa) {

        var Total = 0;

        var myTable = $('#tbl_listar_Suministro_' + empresa).DataTable();
        var form_data = myTable.rows().data();

        if (Tipo === "Minima") {

            for (var i = 0; form_data.length > i; i++) {

              Total = Total + parseFloat(form_data[i].DeudaMinima);
            }
        }

        else {
            for (var e = 0; form_data.length > e; e++) {

              Total = Total + parseFloat(form_data[e].DeudaTotal);
            }

        }

        return parseFloat(Total).toFixed(2);
    }

function btnProcesarArchivo_click() {

    var empresa = $(this).attr('name');


    if ($("#excelfile").val() !== "") {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
        if (!regex.test($("#" + empresa+"_document_file").val().toLowerCase())) {
            MsgBox("Por favor cargue un archivo excel válido", "Info");
            return false;
        }
        else {

          
            var fl = $("#" + empresa + "_document_file").get(0).files[0];

            if (fl !== undefined) {

                var formData = new FormData();
                var file = $("#" + empresa + "_document_file").get(0).files[0];

                formData.append("empresa", empresa);
                formData.append("file", file);

            }

            else { return false;}


            $.ajax({
                type: "POST",
                url: "/SuministroEmpresa/ProcesarCargaMasiva",
                contentType: false,
                processData: false,
                data: formData,
                beforeSend: function () {

                    $('#modPreload').modal('show');
                },
                success: function (result) {

         
                    if (result.Mensaje === "1") {


                        table = $('#tbl_listar_Suministro_' + empresa).DataTable();
                        table.destroy();


                        $('#tbl_listar_Suministro_' + empresa).DataTable({
                            data: result.Lista,
                            searching: false,
                            "language": {
                                "sProcessing": "Procesando...",
                                "sLengthMenu": "Mostrar _MENU_ registros",
                                "sZeroRecords": "No se encontraron resultados",
                                "sEmptyTable": "Ningún dato disponible en esta tabla",
                                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix": "",
                                "sSearch": "Buscar:",
                                "sUrl": "",
                                "sInfoThousands": ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst": "Primero",
                                    "sLast": "Último",
                                    "sNext": "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                            responsive: true,
                            pageLength: 10,
                            lengthChange: false,

                            "columns": [

                                { "data": "Nro" },
                                { "data": "Tarifa" },
                                { "data": "direccion" },
                                { "data": "TipoPropietario" },
                                { "data": "FechaVencimiento" },

                                {
                                    data: "DeudaMinima",
                                    render: function (data, type, row) {


                                        if (type === 'display') {


                                            if (row.DeudaMinima !== "0.0") {

                                                if (row.TipoDeuda === 'M') {
                                                    return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaMinima' + row.Nro + '" class="chkdeudaminima" style="vertical-align: middle;cursor:pointer;" checked>';
                                                }
                                                else {
                                                    return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaMinima' + row.Nro + '" class="chkdeudaminima" style="vertical-align: middle;cursor:pointer;">';
                                                }
                                            }

                                            else {
                                                return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaMinima' + row.Nro + '" class="chkdeudaminima" style="vertical-align: middle;cursor:pointer;" disabled>';
                                            }

                                        }
                                        return data;

                                    }
                                },

                                { "data": "DeudaMinima", render: $.fn.dataTable.render.number(',', '.', 2) },
                               // { "data": "DeudaMinima" },

                                {
                                    data: "DeudaTotal",
                                    render: function (data, type, row) {
                                        if (type === 'display') {


                                            if (row.DeudaTotal !== "0.0") {

                                                if (row.TipoDeuda === 'T') {
                                                    return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaTotal' + row.Nro + '" class="chkdeudatotal" style="vertical-align: middle;cursor:pointer;" checked>';
                                                }
                                                else {
                                                    return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaTotal' + row.Nro + '" class="chkdeudatotal" style="vertical-align: middle;cursor:pointer;">';
                                                }

                                            }
                                            else {
                                                return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaTotal' + row.Nro + '" class="chkdeudatotal" style="vertical-align: middle;cursor:pointer;" disabled>';
                                            }

                                        }
                                        return data;
                                    }
                                },

                              //  { "data": "DeudaTotal" },
                                { "data": "DeudaTotal", render: $.fn.dataTable.render.number(',', '.', 2) },
                                {
                                    data: "Nro",
                                    render: function (data, type, row) {
                                        if (type === 'display') {
                                            return '<a href="#verDetalle" name= "' + empresa + '" id="verDetalle_' + data + '" class="fa fa-file-text-o verDetalle"></a>';
                                        }
                                        return data;
                                    }
                                },
                                {
                                    data: "Nro",
                                    render: function (data, type, row) {
                                        if (type === 'display') {
                                            return '<a href="#eliminarSuministro" name= "' + empresa + '" id="eliminarSuministro_' + data + '" class="far fa-trash-alt eliminarSuministro"></a>';
                                        }
                                        return data;
                                    }
                                },
                                {
                                    data: "Nro",
                                    render: function (data, type, row) {
                                        if (type === 'display') {
                                            return '<a href="#aReclamar" id="aReclamar_' + data + '" class="fa fa-commenting-o aReclamar"></a>';
                                        }
                                        return data;
                                    }
                                }
                            ],

                             "aoColumnDefs": [
                                { "bSortable": false, "aTargets": [5, 7] }
                            ]

                        });


                        var totalminimoresumen = CalcularResumen("Minima", empresa);
                        var totaltotalresumen = CalcularResumen("Total", empresa);

                        totalminimoresumen = totalminimoresumen.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");

                        totaltotalresumen = totaltotalresumen.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");

                        $("#TotalDeudaMinima_" + empresa).text(totalminimoresumen);
                        $("#TotalDeudaTotal_" + empresa).text(totaltotalresumen);

                        CalcularResumenCargaExcel(result.Lista,empresa);

                        $('#modPreload').modal('hide');

                    }


                    else {


                        MsgBox(result.Mensaje, "Info");

                    }


                    $('#modPreload').modal('hide');

                },
                error: function (xhr, status, p3, p4) {
                    $('#modPreload').modal('hide');

                    MsgBox("Error de Sistema", "Error");


                    return false;
                }
            });



        }
    }
    else {
        MsgBox("Por favor cargue un archivo excel", "Info");
        return false;
    }

}

function btnAgregarSuministro_click() {

    var empresa = $(this).attr('name');
    var a = $('#modDetalleSuministro');
    a.find('div.modal-dialog').removeClass('modal-md').addClass('modal-lg');

    var obj = new Object();
    obj.empresa = empresa;

    jQuery.ajax({

        type: 'GET',
        url: "/SuministroEmpresa/_AgregarSuministro",
        data: obj,
        beforeSend: function () {

            $('#modPreload').modal('show');

        },
        success: function (data) {
            //debugger;

            a.find('.modal-title').html("Agregar Suministro - " + empresa);
            a.find('.modal-body').html(data);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');


            $('#modPreload').modal('hide');

        },
        error: function (xhr) {

            $('#modPreload').modal('hide');
            MsgBox("Error de Sistema", "Error");
        
        }
    });
 


}

function btnLimpiar_click() {

    var empresa = $(this).attr('name');

    Limpiar_Busqueda_Por_Empresa(empresa);
}

function Limpiar_Busqueda_Por_Empresa(empresa) {

    $('#' + empresa + 'NumeroSuministro').val('');
    var tarifa = $('#' + empresa +'Tarifa');
    tarifa[0].selectedIndex = 0;
}

function btnBuscar_click() {

    var empresa = $(this).attr('name');
 
    Buscar_Suministro_Por_Empresa(empresa);

}

function Buscar_Suministro_Por_Empresa(empresa)
{

    var suministro = $('#'+ empresa+'NumeroSuministro').val();
    var tarifa = $('#' + empresa + 'Tarifa').val();
    var empresadistriluz = empresa;

    $("#TotalDeudaMinima").text('');
    $("#TotalDeudaTotal").text('');
    $('#' + empresa + 'Monto').val('');

    var obj = new Object();
    obj.suministro = suministro;
    obj.tarifa = tarifa;
    obj.empresadistriluz = empresadistriluz;

    jQuery.ajax({

        type: 'POST',
        url: "/SuministroEmpresa/ListarSuministroEmpresa",
        data: obj,
        dataType: 'Json',
        beforeSend: function () {
           
            $('#modPreload').modal('show');
        },
        success: function (data) {
           
           
            table = $('#tbl_listar_Suministro_' + empresa).DataTable();
            table.destroy();


            $('#tbl_listar_Suministro_' + empresa).DataTable({
                data: data,
                searching: false,
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                responsive: true,
                pageLength: 10,
                lengthChange: false,

                "columns": [

                    { "data": "Nro" },
                    { "data": "Tarifa" },
                    { "data": "direccion" },
                    { "data": "TipoPropietario" },
                    { "data": "FechaVencimiento" },
                  

                    {
                        data: "DeudaMinima",
                        render: function (data, type, row) {


                            if (type === 'display') {


                                if (row.DeudaMinima !== "0.0") {

                                    return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaMinima' + row.Nro + '" class="chkdeudaminima" style="vertical-align:middle;cursor:pointer;">';

                                }
                                else {

                                    return '<input type="checkbox" value="' + row.DeudaMinima +'" name= "' + empresa + '" id="DeudaMinima' + row.Nro + '" class="chkdeudaminima" style="vertical-align:middle;cursor:pointer;" disabled>';

                                }

                            }
                            return data;

                        }
                    },

                  { "data": "DeudaMinima", render: $.fn.dataTable.render.number(',', '.', 2) },
                  
                     // { "data": "DeudaMinima" },
                    {
                        data: "DeudaTotal",
                        render: function (data, type, row) {

                            if (type === 'display') {

                                if (row.DeudaTotal !== "0.0") {

                                    return '<input type="checkbox" value="' + row.DeudaTotal +'" name= "' + empresa + '" id="DeudaTotal' + row.Nro + '" class="chkdeudatotal" style="vertical-align:middle;cursor:pointer;">';

                                }
                                else {

                                    return '<input type="checkbox" value="' + row.DeudaTotal +'" name= "' + empresa + '" id="DeudaTotal' + row.Nro +  '" class="chkdeudatotal" style="vertical-align:middle;cursor:pointer;" disabled>';

                                }


                            }
                            return data;

                        }
                    },

                 //   { "data": "DeudaTotal" },
                    { "data": "DeudaTotal", render: $.fn.dataTable.render.number(',', '.', 2) },
                    {
                        data: "Nro",
                        render: function (data, type, row) {

                            if (type === 'display') {
                                return '<a href="#verDetalle" name= "' + empresa +'" id="verDetalle_' + data + '" class="fa fa-file-text-o verDetalle"></a>';   
                            } 
                            return data;

                        }
                    },


                    {
                        data: "Nro",
                        render: function (data, type, row) {

                            if (type === 'display') {
                                return '<a href="#eliminarSuministro" name= "' + empresa+'" id="eliminarSuministro_' + data + '" class="far fa-trash-alt eliminarSuministro"></a>';
                            }
                            return data;

                        }
                    },


                    {
                        data: "Nro",
                        render: function (data, type, row) {

                            if (type === 'display') {
                                return '<a href="#aReclamar" id="aReclamar_' + data + '" class="fa fa-commenting-o aReclamar"></a>';
                            }
                            return data;

                        }
                    }

                ],

                 "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [5,7] }
                ]

            });

      
            window["Deuda" + empresa] = [];
            $('#' + empresa + 'Monto').val('0.00');

            var totalminimo = CalcularResumen("Minima", empresa);
            var totaltotal = CalcularResumen("Total", empresa);

            totalminimo = totalminimo.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");

            totaltotal = totaltotal.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");

            $("#TotalDeudaMinima_" + empresa).text(totalminimo);
            $("#TotalDeudaTotal_" + empresa).text(totaltotal);
            $('#modPreload').modal('hide');

        },
        error: function (xhr) { 

            $('#modPreload').modal('hide');
            window.location.href = '/SuministroEmpresa/ResumenDeudas';
           //MsgBox("Error de Sistema", "Error");
        }    
        
    });

}

$(function () {
    var $tabButtonItem = $('#tab-button li'),
        $tabSelect = $('#tab-select'),
        $tabContents = $('.tab-contents'),
        activeClass = 'is-active';

    $tabButtonItem.first().addClass(activeClass);
    $tabContents.not(':first').hide();

    $tabButtonItem.find('a').on('click', function (e) {
        var target = $(this).attr('href');

        $tabButtonItem.removeClass(activeClass);
        $(this).parent().addClass(activeClass);
        $tabSelect.val(target);
        $tabContents.hide();
        $(target).show();
        e.preventDefault();
    });

    $tabSelect.on('change', function () {
        var target = $(this).val(),
            targetSelectNum = $(this).prop('selectedIndex');

        $tabButtonItem.removeClass(activeClass);
        $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
        $tabContents.hide();
        $(target).show();
    });
});

$('input[type="file"]').on('change', function (e) {
    var fileName = e.target.files[0].name;
    if (fileName) {
        $(e.target).parent().attr('data-message', fileName);
    }
});

$(document).on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
    if ($('input[type="file"]').length) {
        if (['dragover', 'dragenter'].indexOf(e.type) > -1) {
            if (window.dragTimeout)
                clearTimeout(window.dragTimeout);
            $('body').addClass('dragged');
        } else if (['dragleave', 'drop'].indexOf(e.type) > -1) {
            // Without the timeout, some dragleave events are triggered
            // when the :after appears, making it blink...
            window.dragTimeout = setTimeout(function () {
                $('body').removeClass('dragged');
            }, 100);
        }
    }
});










