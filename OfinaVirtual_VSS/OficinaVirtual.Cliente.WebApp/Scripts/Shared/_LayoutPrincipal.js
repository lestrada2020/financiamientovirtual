﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-04-27
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
            _O.Month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];

            ShowLogoVisaDefault();
        });
    }

    //#region Events

    //#endregion Events

    function ShowLogoVisaDefault() {
        var u = location.href,
            l = [
                'Suministro/VisaConsulta',
                'Suministro/VisaValida',
                'Suministro/VisaFinalizar'
            ];

        for (var i = 0; i < l.length; i++) {
            if (u.indexOf(l[i]) > 0) {
                ShowLogoVisa();
                return;
            }
        }
    }

    function ShowLogoVisa() {
        $('img.logoVisa').closest('div').removeClass('d-none');
    }

    function Loading(sender, isShowPage) {
        if (+isShowPage == 1) {
            sender.prop('disabled', false).addClass('btn-success');
            _C.modPreload.modal('hide');
        } else {
            sender.prop('disabled', true).removeClass('btn-success');
            _C.modPreload.modal('show');
        }
    }

    function ClearAlert(fun) {
        _C.divAlert.removeAttr('class').html('');
        if (fun) fun();
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    context.Loading = Loading;
    context.ClearAlert = ClearAlert;
    context.ShowLogoVisa = ShowLogoVisa;

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Shared', 'LayoutPrincipal'), jQuery));

function CargarModal(event, obj) {
    event.preventDefault();

    jQuery.ajax({
        type: "GET",
        async: true,
        url: obj.href,
        success: function (oDatos) {
            jQuery('.modLayout').find('.modal-body').html(oDatos);
            jQuery('.modLayout').modal({ backdrop: 'static', keyboard: false });
            jQuery('.modLayout').modal('show');
        }
    });
}