﻿///// <reference path="../jquery-3.2.1.min.js" />
///// <reference path="../Util.js" />
//$(document).ready(inicialize);

//function inicialize() {
//    //$(".frmConsultaVisa").submit(function (event) { 
//    //    $('.modPreload').modal('show');
//    //    $(".btnPagar").prop('disabled', true);
//    //});

//    $('#videoModal').on('hidden.bs.modal', function () {
//        $('#idVideoModal').trigger('pause');
//    });
//};

(function () {
    var frmObject = function () {
        function filter(__val__) {
            var preg = /^([0-9]+\.?[0-9]{0,2})$/;
            if (preg.test(__val__) === true) {
                return true;
            } else {
                return false;
            }
        };
        var ctrl = {
            cboEmpresa: $("#cboEmpresa"),
        };
        var _that = {
            Init: function () {
                this.Consultas.CargarCombos();
                this.Eventos.Dom();
                this.Eventos.keypress();
                this.Eventos.LostFocus();
            },
            Consultas: {
                CargarCombos: function () {
                },
            },
            Eventos: {
                Opciones: {
                    Init: function () { }
                },
                InitModal: function () { },
                Dom: function () {
                },
                keypress: function () {
                },
                LostFocus: function () {
                },
            },
        };
        return _that;
    }
    $(document).ready(function () {

        //#region Load
        frmObject().Init();
        inicialize();

        //#endregion 

        //#region Object-Event
        //#endregion 

        //#region AJAX Request
        //#endregion 

        //#region Functions

        function inicialize() {
            //$(".frmConsultaVisa").submit(function (event) { 
            //    $('.modPreload').modal('show');
            //    $(".btnPagar").prop('disabled', true);
            //});

            if ($("#urlVideo").val().length > 0 ) {
                $("#videoModal").modal('show');
            }

            $('#videoModal').on('hidden.bs.modal', function () {
                $('#idVideoModal').trigger('pause');
            });

            $(".frmConsultaVisa").submit(function (event) { 
                $(".btnPagar").prop('disabled', true);
            });

        };
        //#endregion 

        //#region Varios
        //#endregion 
    });
})();