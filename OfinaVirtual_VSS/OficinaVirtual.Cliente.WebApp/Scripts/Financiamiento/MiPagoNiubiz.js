﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-04-22
    Responsable : JONATHAN VALDERRAMA
*/
var stepper1;
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal'),
        _O = $.extend({}, {});
    _O.IdOrder = "";
    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
            _L.ShowLogoVisa();
            _O.IsMobil = window.matchMedia('(max-width: 767px)').matches;
            _O.Data = {};
            stepper1 = new Stepper($('#stepper1')[0], {
                linear: true,
                animation: true
            });

            _O.IdOrder = _C.txtIdNroServicio.data('idorder');
            _O.IdOrden = _C.txtIdNroServicio.data('idorden');
            _O.IdNroServicio = _C.txtIdNroServicio.data('idnroservicio');
            _O.IdUser = _C.txtIdNroServicio.data('iduser');
            _C.btnModalTransaccion.on('click', btnModalTransaccion_click);
            _C.btnModalComprobante.on('click', btnModalComprobante_click);

            CheckPayStatus() || CheckHasSumin();
            stepper1.to(4);
        });
    }

    //#region Events

    function btnSearch_click() {
        _L.ClearAlert();
        _L.Loading(_C.btnSearch, 0);

        if (!ValidSearch()) {
            _L.Loading(_C.btnSearch, 1);
            return;
        }

        GetSuministro();
    }

    function btnFinish_click(e) {
        location.href = _U.UrlAction('MiPago', 'Suministro');
    }

    function btnModalTransaccion_click() {
        _L.Loading(_C.btnModalTransaccion, 0);
        GetRutaExtrajudicial();
    }
    function btnModalComprobante_click() {
        _L.Loading(_C.btnModalComprobante, 0);
        GetRutaComprobante();
    }


    function GetRutaExtrajudicial() {
        debugger;

        var IdOrdenCobroTemporal = "";
        if (_O.IdOrder != null && _O.IdOrden != undefined) {
            IdOrdenCobroTemporal = _O.IdOrden;
        }
        var p = {
            IdOrdenCobro: IdOrdenCobroTemporal
        };

        _U.Ajax('financiamiento', 'ObtenerRutaTransaccion', _U.AddToFormData({ Parametros: p }), function (data) {
            obj = JSON.parse(data.Datos),
                showDocument(obj.RutaArchivo);
        }, function (data) { _L.Loading(_C.btnModalTransaccion, 1); });
    }

    function GetRutaComprobante() {
        var p = {
            idnroservicio: _C.txtIdNroServicio.val(),
            idnumeroorden: document.getElementById("lblPayNroPedido").innerHTML
        };


        _U.Ajax('financiamiento', 'ObtenerDocumentoComprobantePago', _U.AddToFormData({ Parametros: p }), function (data) {
            obj = JSON.parse(data.Datos),
                showDocument(obj.RutaArchivo);
        }, function (data) { _L.Loading(_C.btnModalComprobante, 1); });


    }
    //#endregion Events

    //#region Show
    function showDocument(url) {
        debugger;
        window.open(url, "_blank");
    }


    function ShowResult() {
        _C.divSearch.addClass('d-none');
        _C.divResult.removeClass('d-none')
        _C.divMiPago.addClass('bn-container-full');

        ShowDataSumin();

        // mostrar o ocultar los botones de generación de documentos
        var p2 = {
            IdNroOrden: _O.IdOrder
        };
        _U.Ajax('Financiamiento', 'ValidarEstadoPagoDistriluz', _U.AddToFormData({ Parametros: p2 }), function (data) {
            ocultarBotonesGenerarDocumento(data);
        }, function (data) { _L.Loading(_C.btnTerminar, 1); });
        

    }

    function ShowDataSumin() {
        var a = _O.Data.Suministro,
            b = _O.Data.Deuda || [],
            c = b.length > 0 ? b[b.length - 1] : {};

        console.log(c);

        _C.lblSuministro.html(a.IdNroServicio);
        _C.lblNombres.html(a.nombre);
        _C.lblDireccion.html(a.direccion || '');
        //_C.lblDeudaTotal.html('S/ ' + b.reduce((r, s) => +s.ImporteTotal + +r, 0).toFixed(2));
    }

    function showDataVoucher(data) {
        debugger;
        var t = '',
            s = '',
            a = JSON.parse(data.Trama || '{}'),
            b = _C.txtIdNroServicio.data('errormessage');

        if (!a.header) {
            t = 'PAGO EN PROCESO...';
            s = 'Actualice la página en unos minutos.';
            if (b) s += ' <br/>detalle: ' + b;
        } else if (a.errorCode == 400) {
            t = 'PAGO NO REALIZADO';
            s = _U.StringFormat('Motivo: [{0}] - {1}', a.data.ACTION_CODE || '---', a.data.ACTION_DESCRIPTION || 'No existe.');
        }

        if (t.length > 0) {
            //_C.lblPayTitle.html(t);
            _C.lblPaySubTitle.html(s);
            //_C.btnPrint.remove();
            _C.divDataPay.remove();

            return;
        }
        var f = '20' + a.order.transactionDate;

        _C.lblPayNroPedido.html(a.order.purchaseNumber);
        _C.lblPayNroTarjeta.html(a.dataMap.CARD);
        _C.lblPayFecha.html(_U.StringFormat('{0}-{1}-{2} &nbsp{3}:{4}:{5}', f.substr(0, 4), f.substr(4, 2), f.substr(6, 2), f.substr(8, 2), f.substr(10, 2), f.substr(12, 2)));
        _C.lblPayImporte.html(a.order.amount.toFixed(2));
        _C.lblPayMoneda.html(a.order.currency);
        _C.lblPaySuministro.html(data.IdNroServicio);
        //_C.lblPayPeriodo.html(data.Periodos);
        _C.lblPayCodigoAutoriza.html(a.order.authorizationCode);
        _C.lblPayDescripcionAutoriza.html(a.dataMap.ACTION_DESCRIPTION);
        _C.lblPayComercio.html(data.Empresa);
        _C.lblPayComercioTelefono.html(data.Telefono);
        _C.lblPayComercioDireccion.html(data.Direccion);

        
        
    }

    //#endregion Show

    function ValidSearch() {
        var v = _C.txtIdNroServicio.val();

        _C.txtIdNroServicio.next().hide();

        var r = true;
        r &= _C.txtIdNroServicio[0].checkValidity();

        if (v.length < 6) {
            r = false;
            _C.txtIdNroServicio.next().html(_U.StringFormat('Suministro incorrecto, debe tener mas de 5 dígitos')).show();
        }

        if (isNaN(v)) {
            r = false;
            _C.txtIdNroServicio.next().html(_U.StringFormat('Suministro no es valido, ingrese solo números', b)).show();
        }

        if (!r) _C.divSearch.addClass('was-validated');

        return r;
    }

    function CheckPayStatus() {
        var a = +_O.IdOrder;

        if (a == 0) return false;
        if (isNaN(a)) btnFinish_click();
        _L.Loading(_C.btnSearch, 0);
        GetPay();

        return true;
    }

    function CheckHasSumin() {
        var a = +_O.IdNroServicio;

        if (a == 0) return false;
        _C.txtIdNroServicio.val(a);
        btnSearch_click();

        return true;
    }

    //#region Ajax

    function GetSuministro() {
                var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdOrdenCobro: _O.IdOrden
        };

        _U.Ajax('financiamiento', 'ObtenerSuministroOrdenCobro', _U.AddToFormData({ Parametros: p }), function (data) {
            console.log(data.Datos);
            var d = data.Datos;

            _O.Data.Suministro = d;
            setTimeout(function () { GetDeuda(); }, 500);
        }, function (data) {
            if (data.IdError > 0) _L.Loading(_C.btnSearch, 1);
        });

    }

    function GetDeuda() {
        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdNroServicio: _C.txtIdNroServicio.val()
        };

        _U.Ajax('Suministro', 'ObtenerDeudaPeriodos', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            _O.Data.Deuda = d;
            ShowResult();
        }, function (data) {
            if (data.IdError > 0) {
                if (data.Mensaje.indexOf('no tiene') > 0 && +_O.IdOrder > 0) {
                    _L.ClearAlert();
                    ShowResult();
                }
            }
            _L.Loading(_C.btnSearch, 1);
        });
    }

    function GetPay() {
        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            NroPedido: _O.IdOrder
        };

        _U.Ajax('Suministro', 'ObtenerDataPedido', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            if (!d || +d.IdNroServicio == 0) {
                _U.AlertError('Nro. pedido no existe.');
                setTimeout(function () { btnFinish_click(); }, 3000);
                return;
            }

            _O.Data.Intent = d;
            showDataVoucher(d);
            _C.txtIdNroServicio.val(d.IdNroServicio);
            setTimeout(function () { btnSearch_click(); }, 1000);
        }, function (data) { _L.Loading(_C.btnTerminar, 1); });
    }
    function ocultarBotonesGenerarDocumento(data) {
        if (data.Datos == 169) {
            var element = document.getElementById("showButtonDocuments");
            element.classList.add("d-none");
        }
    }

    //#endregion Ajax
    //#endregion Private

    //#region Public

    (function () { _U.CaptchaReady(Load); }());
    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'MiPago'), jQuery));