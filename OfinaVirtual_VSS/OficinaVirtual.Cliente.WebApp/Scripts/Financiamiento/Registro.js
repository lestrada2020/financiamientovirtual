﻿//var oTableA;
var oimHead = `<thead><tr><th style='text-align:center'>Documento</th><th style='text-align:center'>N\u00FAmero</th><th style='text-align:center'>Periodo</th>
<th style='text-align:center'>Vencimiento</th><th style='text-align:center'>Importe <br>Recibo</th><th style='text-align:center'>Saldo <br>Terceros</th><th style='text-align:center'>Pagado</th>
<th style='text-align:center'>Saldo <br>Financiar</th><th style='text-align:center'>Estado <br>Deuda</th></tr></thead>`;
(function (context, $, undefined) {
    //#region Fields
    var _S, _M,
        _U = window.nsUtil,
        _O = $.extend({}, {}),
        _C = _U.GetControlsJQ(),
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal');
    //#endregion Fields

    //#region Events
    function Load() {
        $(document).ready(function (a) {
            _O.IdUser = _C.txtIdUser.val();
            _M = model;
            //_O.IdPig = 2;
            //_C.txtIdUser.remove();           

            loadConvenio();
            initializeDatatable();
            getDeuda(_C.idSuministro.val());
            //#region Events
            _C.cmbConv.on('change', cmbConv_change);
            _C.arDeuda.on('click', arDeuda_click);
            _C.openTerminos.on('click', loadPdf);
            if (_C.valorTasaInteres.val() !== "0") {
                _C.chkTerminos.on('click', chkTerminos_click);
                //_C.btnGenerate.on('click', btnGenerate_click);
            }
            //#endregion
            _O.oTableA;

            $(".frmGenerarOrdenCobro").submit(function (event) {
                $("#btnGenerate").prop('disabled', true);
            });
        });
    }

    function loadPdf() {
        var iframe = document.getElementById('iframeTerminos');
        iframe.src = $("#urlTerminosCondiciones").val();
    }

    function cmbConv_change() {
        generateImporte(this);
    }


    //function cmbCServicio_change() {
    //    _C.IdCentroServicio.val($("option:selected", this).val());
    //}

    function arDeuda_click() {
        showModal();
    }
    function chkTerminos_click() {
        enabledBtnGen();
    }
    //#endregion Events

    //#region AJAX Request
    function loadConvenio() {
        var p = {
            Parametros: {
                Cartera: 'C',
                Importe: _C.resIdTotalDeuda.text(),
                AbreviaturaConvenio: 'RefVirt'
            }
        };

        var p1 = {
            Parametros: {
                iduunn: _M.financiamiento.iduunn
            }
        };
        //_U.Ajax('financiamiento', 'obtenercentroservicios', _U.AddToFormData(p1), function (data) {
        //    let option,
        //        d = JSON.parse(data.Datos);
        //    if (d.length > 0) {
        //        $.each(d, function (item, value) {
        //            option += '<option value="' + value.id + '">' + value.nombre + '</option>';
        //        });
        //        _C.cmbCServicio.append(option);
        //        //generateImporte(_C.cmbConv);
        //    }
        //    _C.IdCentroServicio.val($("option:selected", $("#cmbCServicio")).val());
        //}, function (data) { });

        _U.Ajax('financiamiento', 'obtenerporcentajesconvenio', _U.AddToFormData(p), function (data) {
            let option,
                d = JSON.parse(data.Datos);
            if (d.length > 0) {
                $.each(d, function (item, value) {
                    option += '<option value="' + value.IdConvenio + '" data-plazo="' + this.PlazoMaximo + '">' + value.PorcentajeInicial + '</option>';
                });
                _C.cmbConv.append(option);
                generateImporte(_C.cmbConv);
            }
        }, function (data) { });
    }
    function getDeuda(id) {
        var p = {
            idSuministro: id
        };
        _U.Ajax('financiamiento', 'obtenerdetalledeuda', _U.AddToFormData(p), function (json) {
            $("#gridReport").dataTable().fnDestroy();
            $("#gridReport").empty();
            $("#gridReport").append(oimHead);
            _O.oTableA = $('#gridReport').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                data: json.data,
                fixedHeader: true,
                //select: true,
                searching: false,
                lengthChange: false,
                searching: false,
                //"paging": false,
                "info": false,
                //bFilter: false,
                pageLength: 10,
                //"lengthMenu": [7, 14, 21, 29],
                //"scrollY": 100,
                "scrollX": true,
                "columnDefs": columnDefs(),
            });
        }, function (data) { });
    };

    //#endregion 


    //#region Functions
    function calcularValorCuota(montoprincipal, tasainteres, plazomeses) {

        if (montoprincipal == 0 || plazomeses == 0) return 0;
        let _decvalorcuota = 0;
        // Convertir la tasas de interes en tanto x 1.
        tasainteres = tasainteres / 100;

        // Si la tasa de interes es cero (0).
        if (tasainteres == 0) {
            _decvalorcuota = montoprincipal / plazomeses;
        }
        else {
            // Si el plazo es cero (0).
            if (plazomeses <= 0) {
                _decvalorcuota = montoprincipal;
            }
            else {
                if (montoprincipal > 0 && tasainteres > 0) {
                    // Calcular el valor de la cuota.
                    // Se agregó el ELSE, para los casos en que el plazomeses > 0 solo en ese caso se debe de calcular el Valor de la Cuota.
                    // Carlos López -> Coordinado con Nixon Carrión -- 11/02/2009.
                    _decvalorcuota = ((montoprincipal * tasainteres) /
                        (1 - (Math.pow((1 + tasainteres), (plazomeses * -1)))));
                }
            }
        }
        return _decvalorcuota;
    }

    function generateImporte(obj) {
        let plazo = $("option:selected", obj).attr('data-plazo')

        let porc = $("option:selected", obj).text();
        //debugger;
        _C.Porcentaje.val(porc / 100);
        let negImporte = ((_M.importe.total + _M.importe.interesAlaFecha) * (parseFloat(porc) / 100)).toFixed(2);
        let factSaldo = ((_M.importe.total + _M.importe.interesAlaFecha) - negImporte).toFixed(2);
        let facImporte = calcularValorCuota(factSaldo, _M.importe.valorTasaInteres, plazo).toFixed(2);   // (factSaldo / plazo).toFixed(2); //importe de cuotas     
        let interes = ((facImporte * plazo) - factSaldo).toFixed(2);

        _C.negImporte.val(negImporte);
        _C.facSaldo.text(factSaldo);
        _C.facImporte.text(facImporte);
        _C.facCuota.text(plazo);
        _C.facInteres.text(interes);
        //Para Generar Cobro
        _C.MontoFinanciar.val(factSaldo);
        _C.MontoInicial.val(negImporte);
        _C.NumeroCuotas.val(plazo);
        _C.ImporteCuota.val(facImporte);
        _C.InicialExigida.val(negImporte)
        _C.PlazoMaximo.val(plazo);

    };
    function initializeDatatable() {
        let arrayLocal = [];
        $("#gridReport").empty();
        $("#gridReport").append(oimHead);
        $('#gridReport').dataTable({
            "scrollX": true,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
            data: arrayLocal,
            "oLanguage": {
                "sEmptyTable": "..."
            },
            "columnDefs": columnDefs()
        });
    };
    function columnDefs() {
        let col = [
            {
                //className: "text-right",
                "targets": [4, 5, 6, 7],
                "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).addClass('text-right');
                }
            },
        ];
        return col;
    }
    function showModal() {
        if (_U.isEmptyFunc(_O.oTableA)) {
            getDeuda(_C.idSuministro.val());
        }
        $('#deudaModal').modal("show");
        setTimeout(function () {
            let table = $('#gridReport').DataTable();
            table.columns.adjust();
        }, 250);
    }
    function validarFiltro() {
        if (!_C.chkTerminos.prop('checked')) {
            _U.AlertError('Debe de aceptar los términos y condiciones');
            return false;
        }
        return true;
    };
    function enabledBtnGen() {
        _L.ClearAlert();
        let a = _C.chkTerminos.prop('checked');
        _C.btnGenerate.prop('disabled', !a);

        if (a)
            _C.btnGenerate.removeClass('btn-outline-success').addClass('btn-success');
        else
            _C.btnGenerate.removeClass('btn-success').addClass('btn-outline-success');
        return a;
    }
    function finish() {
        location.href = _U.UrlAction('resumen', 'financiamiento');
    }
    //#endregion 

    //#region Public
    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Financiamiento', 'Registro'), jQuery));
