﻿var stepper1;
(function (context, $, undefined) {
    //#region Fields
    var _C, _S,
        _U = window.nsUtil,
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal'),
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Events
    function Load() {
        window.onload = function () {
            // regex for finding "loaded" query string parameter
            var qsRegex = /^(\?|.+&)loaded=\d/ig;
            if (!qsRegex.test(location.search)) {
                window.history.replaceState(null, document.title, '/Financiamiento/Resumen');
            }
        };
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
            stepper1 = new Stepper($('#stepper1')[0], {
                linear: true,
                animation: true
            });

            _O.Data = {};

            //#region Events
            _O.IdOrden = _C.lstOrdenCobro.data('idorder');
            _O.IdUser = _C.lstOrdenCobro.data('iduser');
            _O.IdInteresGenerado = _C.lstIdTipoDocumento.data('idinteresgenerado');
            _O.IdInteresGeneradoJuridico = _C.lstIdTipoDocumento.data('idinteresgeneradojuridico');
            if (_O.IdOrden != "0") {
                $("#autVisa").removeClass('d-none');
                _C.btnNext01.on('click', btnNext01_click);
                _C.btnNext02.on('click', btnNext02_click);
                _C.chkTerminos.on('click', chkTerminos_change);
                _C.openTerminos.on('click', loadPdf);
                _C.btnModalTransaccion.on('click', btnModalTransaccion_click);
                _C.btnModalComprobante.on('click', btnModalComprobante_click);
                GetSuministroOrden();
            }
            //#endregion

            stepper1.to(1);
        });
    }
    function loadPdf() {
        var iframe = document.getElementById('iframeTerminos');
        iframe.src = $("#urlTerminosCondiciones").val();
    }
    function btnNext01_click() {
        if (!EnabledButtonNext01()) return;
        _L.Loading(_C.btnNext01, 0);
        GetPedido();
    }
    function btnNext02_click() {
        _L.Loading(_C.btnNext02, 0);
        GetMetodoPago();
    }
    function chkTerminos_change() {
        EnabledButtonNext01();
    }
    function btnModalTransaccion_click() {
        _L.Loading(_C.btnModalTransaccion, 0);
        GetRutaExtrajudicial();
    }
    function btnModalComprobante_click() {
        _L.Loading(_C.btnModalComprobante, 0);
        GetRutaComprobante();
    }
    //#endregion Events

    //#region AJAX Request
    function GetSuministroOrden() {
        _L.Loading(_C.tienePedido, 0);

        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdOrdenCobro: _O.IdOrden
        };

        _U.Ajax('financiamiento', 'ObtenerSuministroOrdenCobro', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = data.Datos;

            _O.Data.Suministro = d;
            setTimeout(function () { GetDetalleOrden(); }, 500);
        }, function (data) {
            if (data.IdError > 0) _L.Loading(_C.tienePedido, 1);
        }, function (xhr, error, code) {
            _L.Loading(_C.tienePedido, 1);
        });
    }
    function GetDetalleOrden() {
        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdOrdenCobro: _O.IdOrden
        };

        _U.Ajax('financiamiento', 'ObtenerDetalleOrdenCobro', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);
            _O.Data.DetalleOrden = d;

            ShowResult();
        }, function (data) {
            setTimeout(function () { _L.Loading(_C.tienePedido, 1); }, 300);
        });
    }
    function GetPedido() {
        var a = RowsSelecteds(),
            b = a[0],
            p = {
                TokenCaptcha: _U.CaptchaToken(),
                IdNroServicio: _O.Data.Suministro.IdNroServicio,
                idordencobro: _O.IdOrden,
                IdUser: +_O.IdUser > 0 ? +_O.IdUser : 1,
                IdOrigen: 3,
                Importe: _O.PayImporte,
                IdEmpresa: b.IdEmpresa,
                Detalle: JSON.stringify(a)
            };

        _U.Ajax('financiamiento', 'GenerarPedido', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            console.log(d);

            _O.Data.DetallePedido = d;
            ShowDataConfirma(d);
            stepper1.to(2);
        }, function (data) { _L.Loading(_C.btnNext01, 1); });
    }

    function GetMetodoPago() {
        var a = RowsSelecteds(),
            b = a[0],
            p = {
                TokenCaptcha: _U.CaptchaToken(),
                IdNroServicio: _O.Data.Suministro.IdNroServicio,
                idordencobro: _O.IdOrden,
                IdUser: +_O.IdUser > 0 ? +_O.IdUser : 1,
                IdOrigen: 3,
                Importe: _O.PayImporte,
                IdEmpresa: b.IdEmpresa,
                Detalle: JSON.stringify(a),
                NroPedido: _O.Data.DetallePedido.Order.NroPedido,
                IdEstado: 1
            };

        _U.Ajax('financiamiento', 'ObtenerMetodoPago', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);
            _O.Data.MetodoPago = d;
            stepper1.to(3);
            ShowDataMethod(d);
        }, function (data) {
                debugger;
            _L.Loading(_C.btnNext02, 1);
        });
    }

    function GetRutaExtrajudicial() {
        var p = {
        };

        _U.Ajax('financiamiento', 'ObtenerRutaTransaccion', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);
            var iframeContent = document.getElementById("iframeContent");
            iframeContent.innerHTML = "";
            var iframe = document.createElement('iframe');
            iframe.setAttribute('src', d.Parametros.rutaTransaccion);
            iframe.setAttribute('type', 'application/pdf');
            iframe.setAttribute('height', '640');
            iframe.setAttribute('width', '100%');
            iframeContent.appendChild(iframe);

            $('#pdfTitulo').text('Transacción Extrajudicial');
            $('#pdfModal').modal('show');
        }, function (data) { _L.Loading(_C.btnModalTransaccion, 1); });
    }

    function GetRutaComprobante() {
        var p = {
        };

        _U.Ajax('financiamiento', 'ObtenerRutaComprobante', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);
            var iframeContent = document.getElementById("iframeContent");
            iframeContent.innerHTML = "";
            var iframe = document.createElement('iframe');
            iframe.setAttribute('src', d.Parametros.rutaComprobante);
            iframe.setAttribute('type', 'application/pdf');
            iframe.setAttribute('height', '640');
            iframe.setAttribute('width', '100%');
            iframeContent.appendChild(iframe);

            $('#pdfTitulo').text('Comprobante BV/FAC');
            $('#pdfModal').modal('show');
        }, function (data) { _L.Loading(_C.btnModalComprobante, 1); });
    }

    //#endregion 


    //#region Functions
    function EnabledButtonNext01() {
        _L.ClearAlert();
        var a = _C.chkTerminos.prop('checked');
        _C.btnNext01.prop('disabled', !a);

        if (a) _C.btnNext01.removeClass('btn-outline-success').addClass('btn-success');
        else _C.btnNext01.removeClass('btn-success').addClass('btn-outline-success');

        return a;
    }
    function ShowResult() {
        ShowDataSumin();
        ShowDataDetalleOrden();
    }
    function ShowDataSumin() {
        var a = _O.Data.Suministro;
        _C.lblSuministro.text(a.IdNroServicio);
        _C.lblNombre.text(a.nombre);
        _C.lblDireccion.text(a.direccion);
    }
    function ShowDataDetalleOrden() {
        var s = _O.Data.Suministro;

        var a = _O.Data.DetalleOrden,
            b = _U.DefaultFormatDateTime().MonthNamesShort;

        for (var i = 0; i < a.length; i++) {
            var isInteres = a[i].IdTipoDocumento == _O.IdInteresGenerado || a[i].IdTipoDocumento == _O.IdInteresGeneradoJuridico;
            var r = isInteres ? $('<tr id="trInteres" class="d-none"/>') : $('<tr/>'),
                p = a[i],
                m = '';

            //m += isInteres ? '<td class="p-2"></td>' : '<td class="details-control p-2"></td>';
            m += _U.StringFormat('<td>{0}</td>', p.PeriodoComercial);
            m += _U.StringFormat('<td>{0}</td>', p.TipoDocumento);
            m += _U.StringFormat('<td>{0}</td>', p.FechaVencimiento);
            m += _U.StringFormat('<td>{0}</td>', p.Importe.toFixed(2));
            p.Index = i;
            r.data('entidad', p).html(m);
            _C.tblDetalleOrden.find('tbody').append(r);
        }

        $('#tblDetalleOrden tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = $("#trInteres");

            if (tr.hasClass("shown")) {
                row.addClass("d-none");
                tr.removeClass('shown');
            }
            else {
                row.removeClass("d-none");
                tr.addClass('shown');
            }
        });

        var r = _C.tblDetalleOrden.find('tfoot tr th:last-child');
        _O.PayImporte = s.Importe;
        r.html(parseFloat(_O.PayImporte).toFixed(2));
    }

    function RowsSelecteds() {
        var a = [];

        _C.tblDetalleOrden.find('tbody tr td:first-child').each(function (i, v) {
            var m = $(v),
                n = m.closest('tr').data('entidad');

            a.push(n);
        });

        return a;
    }
    function ShowDataConfirma(data) {
        var a = data;
        _C.lblNroPedido.html(a.Order.NroPedido);
        _C.lblImportePedido.html(parseFloat(_O.PayImporte).toFixed(2));
        _C.lblFechaPedido.html(a.Order.FechaRegistro);
    };


    function ShowDataMethod(data) {
        var a = data;
        console.log(data);
        if (data.Method.length > 0) {
            data.Method.forEach(function (element) {
                switch (element.IdMetodo) {
                    case 1:
                        $("#metodosPago").append(`
                            <div class="form-group row mt-3 mb-0">
                                <label class="col-sm-6 text-right hide-sm">`+ element.Descripcion + `: </label>
                                <label class="col-12 d-lg-none d-md-none">`+ element.Descripcion + `: </label>
                                <div class="col-12 col-lg-4 mr-auto">
                                    <form id="frmPagoCard" action="" method="post">
                                        <input type="hidden" id="idMetodoPago" value="`+ element.IdMetodo + `" />
                                    </form>
                                </div>
                            </div>
                        `);

                        $("#frmPagoCard").attr('action', _U.UrlContentFull('Financiamiento/MyPay/' + a.NroPedido + '/' + _O.IdOrden + '?idMetodo=' + element.IdMetodo));
                        $("#frmPagoCard").click(function () {
                            debugger;
                           // _L.Loading(_C.tienePedido, 0);
                        });

                        //$("#visaNetJS").click(function () {
                        //    if ($('#visaNetJS').css('display') == 'none') {
                        //        _L.Loading(_C.tienePedido, 1);
                        //    }
                        //});

                        setTimeout(function () {
                            window.addEventListener('focus', function () {
                                if ($('#visaNetJS').css('display') == 'none') {
                                    _L.Loading(_C.tienePedido, 1);
                                }
                                console.log('Wow! wind Click!');
                            });
                        }, 3000);

                         //_U.Ajax('financiamiento', 'MyPay/' + a.NroPedido + '/' + _O.IdOrden,
                         //    function (data) {
                         //        var d = data.Datos;
                         //        stepper1.to(4);
                         //        // console.log(d);
                         //    },
                         //    function (data) {
                         //        if (data.IdError > 0) _L.Loading(_C.tienePedido, 1);
                         //    }, function (xhr, error, code) {
                         //        _L.Loading(_C.tienePedido, 1);
                         //    });

                        LoadPayButton(data.PayOrder.Pay);
                        break;
                    default:

                        var icon = "";

                        if (element.Icono == 'personalizado') {
                            icon = `<img src="` + element.urlIcono + `" alt="icon" height="32">`;
                        } else {
                            icon = `<i class="` + element.Icono + `"></i>`;
                        }

                        $("#metodosPago").append(`
                            <div class="form-group row mt-3 mb-0">
                                <label class="col-sm-6 text-right hide-sm">`+ element.Descripcion + `: </label>
                                <label class="col-12 d-lg-none d-md-none">`+ element.Descripcion + `: </label>
                                <div class="col-12 col-lg-4 mr-auto">` + icon + `</div>
                                <input type="hidden" id="idMetodoPago" value="`+ element.IdMetodo + `" />
                            </div>
                        `);
                        break;
                }
            });
        }
    };

    function LoadPayButton(data) {
        //console.log(data);
        var s = document.createElement('script');
        s.setAttribute('src', data.Src);
        $.each(data.Data, (k, v) => s.setAttribute(_U.StringFormat('data-{0}', k), v));
        s.setAttribute('data-channel', 'web');
        s.setAttribute('data-merchantlogo', 'https://www.distriluz.com.pe/templates/lb_hct/images/logo.png');
        s.setAttribute('data-merchantname', 'DISTRILUZ');
        s.setAttribute('data-expirationminutes', '5');
        s.setAttribute('data-timeouturl', _U.UrlContentFull('Financiamiento/MiPagoNiubiz'));

        document.getElementById('frmPagoCard').appendChild(s);
    }
    //#endregion 

    //#region Public
    (function () {
        _U.CaptchaReady(Load);
    }());
    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Financiamiento', 'MiPago'), jQuery));
