﻿/// <reference path="jquery-3.2.1.min.js" />
/*
    Creación        : 2017-03-24
    Responsable     : JONATHAN VALDERRAMA
*/

window.nsUtil = window.nsUtil || {};

(function (context, $, undefined) {
    //#region Field

    var _S = {};
    var _CodesHTML = [
        ['&', '&amp;'],
        ['"', '&quot;'],
        ["'", '&#39;'],
        ['<', '&lt;'],
        ['>', '&gt;']
    ];

    //#endregion

    //#region Property

    context.DateFormat = 'd MM, yy';
    context.DateTimeFormat = 'd MM, yy hh:ii:ss';
    context.ActionDeleteHtml = '<a class="btn-floating waves-effect waves-light btn-del" title="Eliminar Registro"><i class="mdi-content-clear"></i></a>';
    context.ActionDownloadHtml = '<a class="btn-floating waves-effect waves-light cyan darken-2 btn-dow" title="Descargar {Archivo}"><i class="mdi-file-file-download"></i></a>';

    //#endregion Property

    //#region Private

    //#region Default

    function DefaultAjax() {
        return {
            controller: '',
            action: '',
            data: {},
            cache: true,
            processData: true,
            async: true,
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            callbackDone: undefined,
            callbackFail: undefined,
            callbackAlways: undefined
        };
    }

    function DefaultFormatDateTime() {
        return {
            MonthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            MonthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic'],
            MonthNamesShortEn: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            DayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
            DayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            AmpmNames: ['AM', 'PM'],
            GetSuffix: function (num) {
                if (num > 3 && num < 21) {
                    return 'th';
                }

                switch (num % 10) {
                    case 1: return "st";
                    case 2: return "nd";
                    case 3: return "rd";
                    default: return "th";
                }
            },
            Format: 'dd/mm/yy hh:ii:ss',
            FormatSource: 'd MM, yy'
        }
    }

    function SetDefaultEditable(settings) {
        return {
            Mode: 'popup',//popup/inline
            Buttons: '<button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="icon md-check" aria-hidden="true"></i></button><button type="button" class="btn btn-default btn-sm editable-cancel"><i class="icon md-close" aria-hidden="true"></i></button>'
        };
    }

    //#endregion Default

    //#region Config

    function AddConfigObject(obj, defaults, options) {
        if (obj.hasOwnProperty(options.Id)) {
            $.extend(true, obj[options.Id], options);
        } else {
            obj[options.Id] = $.extend(true, {}, defaults, options);
        }

        return obj[options.Id];
    }

    function AddConfigArray(list, defaults, options) {
        var i = -1;

        $.each(list, function (index, item) {
            if (item.Id == options.Id)
                i = index;
        })

        if (i == -1) {
            var settings = $.extend(true, {}, defaults, options);

            i = list.length;
            list.push(settings);
        }
        else {
            $.extend(true, list[i], options);
        }

        list[i].Index = i;

        return list[i];
    }

    //#endregion Config

    function Ajax(data) {
        var settings = $.extend(true, {}, DefaultAjax(), data);
        var url = context.UrlAction(settings.action, settings.controller);

        $.ajax({
            url: url,
            async: settings.async,
            cache: settings.cache,
            contentType: settings.contentType,
            data: settings.data,
            dataType: settings.dataType,
            processData: settings.processData,
            type: settings.type
        }).done(function (data, textStatus, jqXHR) {
            if (!context.IsAjaxError(data, jqXHR) && settings.callbackDone != undefined) settings.callbackDone(data, textStatus, jqXHR);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            //console.log('FAIL'); console.log(jqXHR.getAllResponseHeaders()); console.log(jqXHR); console.log(textStatus); console.log(errorThrown);
            if (settings.callbackFail != undefined) settings.callbackFail(jqXHR, textStatus, errorThrown);
            else context.IsAjaxError(undefined, jqXHR);
        }).always(function (data, textStatus, jqXHR) {
            if (settings.callbackAlways != undefined) settings.callbackAlways(data, textStatus, jqXHR);
        });
    }

    function ParseDateTime(date, data) {
        if (!date) return date;

        var settings = $.extend(true, {}, DefaultFormatDateTime(), data);

        if (!(date instanceof Date)) {
            if ((/^\//).test(date)) date = eval('new ' + date.replace(/\//g, ''));
            else if (settings.FormatSource.startsWith('d MM, yy')) {
                var ar = date.split(',');
                date = ar[1];
                ar = ar[0].split(' ');
                date = settings.MonthNamesShortEn[settings.MonthNames.indexOf(ar[1])] + ' ' + ar[0] + ',' + date;
                date = new Date(date);
            } else if (settings.FormatSource.startsWith('dd/mm/yy')) {
                var ar = date.split('/');
                var indexHour = ar[2].lastIndexOf(':');

                date = settings.MonthNamesShortEn[ar[1] - 1] + ' ' + Number(ar[0]) + ',' + (indexHour > -1 ? ar[2].substring(0, indexHour + 3) : ar[2]);
                date = new Date(date);
            } else if (settings.FormatSource.startsWith('mm/dd/yy')) {
                var ar = date.split('/');
                var indexHour = ar[2].lastIndexOf(':');

                date = settings.MonthNamesShortEn[ar[0] - 1] + ' ' + Number(ar[1]) + ',' + (indexHour > -1 ? ar[2].substring(0, indexHour + 3) : ar[2]);
                date = new Date(date);
            } else date = undefined;
        }

        return date;
    }

    function FormatDateTime(date, data) {
        if (!date) return '';

        //#region Format

        /*
            a - Ante meridiem and post meridiem
            d  - day of month (no leading zero)
            dd - day of month (two digit)
            o  - day of year (no leading zeros)
            oo - day of year (three digit)
            D  - day name short
            DD - day name long
            g  - 12-hour hour format of day (no leading zero)
            gg - 12-hour hour format of day (two digit)
            h  - 24-hour hour format of day (no leading zero)
            hh - 24-hour hour format of day (two digit)
            u  - millisecond of second (no leading zeros)
            uu - millisecond of second (three digit)
            i  - minute of hour (no leading zero)
            ii - minute of hour (two digit)
            m  - month of year (no leading zero)
            mm - month of year (two digit)
            M  - month name short
            MM - month name long
            S  - ordinal suffix for the previous unit
            s  - second of minute (no leading zero)
            ss - second of minute (two digit)
            y  - year (two digit)
            yy - year (four digit)
            @  - Unix timestamp (ms since 01/01/1970)
            !  - Windows ticks (100ns since 01/01/0001)
            '...' - literal text
            '' - single quote
        */

        //#endregion Format

        var settings = $.extend(true, {}, DefaultFormatDateTime(), data);

        date = ParseDateTime(date, settings);

        //#region Function

        var ticksTo1970 = (((1970 - 1) * 365 + Math.floor(1970 / 4) - Math.floor(1970 / 100) + Math.floor(1970 / 400)) * 24 * 60 * 60 * 10000000);
        var output = '';
        var literal = false;
        var iFormat = 0;
        var format = settings.Format;

        // Check whether a format character is doubled
        var lookAhead = function (match) {
            var matches = (iFormat + 1 < format.length
                && format.charAt(iFormat + 1) == match);
            if (matches) {
                iFormat++;
            }
            return matches;
        };

        // Format a number, with leading zero if necessary
        var formatNumber = function (match, value, len) {
            var num = '' + value;
            if (lookAhead(match)) {
                while (num.length < len) {
                    num = '0' + num;
                }
            }
            return num;
        };

        // Format a name, short or long as requested
        var formatName = function (match, value, shortNames, longNames) {
            return (lookAhead(match) ? longNames[value] : shortNames[value]);
        };

        // Get the value for the supplied unit, e.g. year for y
        var getUnitValue = function (unit) {
            switch (unit) {
                case 'y': return date.getFullYear();
                case 'm': return date.getMonth() + 1;
                case 'd': return date.getDate();
                case 'g': return date.getHours() % 12 || 12;
                case 'h': return date.getHours();
                case 'i': return date.getMinutes();
                case 's': return date.getSeconds();
                case 'u': return date.getMilliseconds();
                default: return '';
            }
        };

        //#endregion Function

        //#region Format

        for (iFormat = 0; iFormat < format.length; iFormat++) {
            if (literal) {
                if (format.charAt(iFormat) == "'" && !lookAhead("'")) {
                    literal = false;
                }
                else {
                    output += format.charAt(iFormat);
                }
            } else {
                switch (format.charAt(iFormat)) {
                    case 'a':
                        output += date.getHours() < 12
                            ? settings.AmpmNames[0]
                            : settings.AmpmNames[1];
                        break;
                    case 'd':
                        output += formatNumber('d', date.getDate(), 2);
                        break;
                    case 'S':
                        var v = getUnitValue(iFormat && format.charAt(iFormat - 1));
                        output += (v && (settings.GetSuffix || $.noop)(v)) || '';
                        break;
                    case 'D':
                        output += formatName('D',
                            date.getDay(),
                            settings.DayNamesShort,
                            settings.DayNames);
                        break;
                    case 'o':
                        var end = new Date(date.getFullYear(),
                            date.getMonth(),
                            date.getDate()).getTime();
                        var start = new Date(date.getFullYear(), 0, 0).getTime();
                        output += formatNumber(
                            'o', Math.round((end - start) / 86400000), 3);
                        break;
                    case 'g':
                        output += formatNumber('g', date.getHours() % 12 || 12, 2);
                        break;
                    case 'h':
                        output += formatNumber('h', date.getHours(), 2);
                        break;
                    case 'u':
                        output += formatNumber('u', date.getMilliseconds(), 3);
                        break;
                    case 'i':
                        output += formatNumber('i', date.getMinutes(), 2);
                        break;
                    case 'm':
                        output += formatNumber('m', date.getMonth() + 1, 2);
                        break;
                    case 'M':
                        output += formatName('M',
                            date.getMonth(),
                            settings.MonthNamesShort,
                            settings.MonthNames);
                        break;
                    case 's':
                        output += formatNumber('s', date.getSeconds(), 2);
                        break;
                    case 'y':
                        output += (lookAhead('y')
                            ? date.getFullYear()
                            : (date.getYear() % 100 < 10 ? '0' : '')
                            + date.getYear() % 100);
                        break;
                    case '@':
                        output += date.getTime();
                        break;
                    case '!':
                        output += date.getTime() * 10000 + ticksTo1970;
                        break;
                    case "'":
                        if (lookAhead("'")) {
                            output += "'";
                        } else {
                            literal = true;
                        }
                        break;
                    default:
                        output += format.charAt(iFormat);
                }
            }
        }

        //#endregion

        return output;
    };

    function Alert(data) {
        var s = {},
            b = data.CallbackDone,
            c = data.CallbackCancel;

        if (typeof data.Options == 'function') b = data.Options;
        else if (typeof data.Options == 'string') {
            s.html = data.Options;
            if (typeof data.CallbackDone == 'string') {
                b = undefined;
                s.title = data.CallbackDone;
            }
        }
        else if (typeof data.Options == 'object') {
            if (data.Options instanceof Array) s.html = '<ol><li>' + data.Options.join('</li><li>') + '</li></ol>';
            else s = data.Options;
        }

        s = $.extend(true, {}, data.Defaults, s);

        var f1 = function () { if (b) b(); },
            f2 = function (dismiss) { if (c) c(dismiss); };// dismiss can be 'cancel', 'overlay','close', and 'timer'

        AlertTemp(s);//swal(s).then(f1, f2);
    }

    function AlertTemp(data) {
        var a = $('#divAlert'),
            r = data.html || '',
            m = '<span class="font-weight-bold"><i class="fa fa-info-circle fa-1x" aria-hidden="true"></i> ';

        if (a.length == 0) {
            console.log(data);
            return;
        }

        a.html('');
        a.removeAttr('class');
        a.attr('role', 'alert');
        a.addClass('alert');

        if (data.type == 'success') {
            a.addClass('alert-success');
        } else if (data.type == 'error') {
            a.addClass('alert-danger');
            var r1 = r.indexOf('<<<'),
                r2 = r.indexOf('>>>');

            if (r1 > -1 && r2 > -1) r = r.substring(r1 + 3, r2);
            else if (r.length > 150) r = data.title || 'Error';
            console.log(data);
        }

        m += r + '</span>';
        a.html(m);
    }
    //#endregion Private

    //#region Public

    context.AddConfig = function (list, defaults, options) {
        if (list instanceof Array)
            return AddConfigArray(list, defaults, options);
        else
            return AddConfigObject(list, defaults, options);
    };

    context.GetConfigIndex = function (list, id) {
        $.each(list, function (index, item) {
            if (item.Id == id)
                return index;
        })

        return -1;
    };

    context.Ajax = function (controller, action, data, callbackDone, callbackAlways, callbackFail, async) {
        var p = data instanceof FormData;

        if (p) data.set('Token', _S.Token);
        else if (data instanceof Object) data.Token = _S.Token;

        var settings = {
            controller: controller,
            action: action,
            async: async,
            data: p ? data : JSON.stringify(data),
            contentType: p ? false : undefined,
            cache: !p,
            processData: !p,
            callbackDone: callbackDone,//(data, textStatus, jqXHR)
            callbackFail: callbackFail,//(jqXHR, textStatus, errorThrown)
            callbackAlways: callbackAlways//(data, textStatus, jqXHR)
        };

        Ajax(settings);
    };

    context.UrlAction = function (action, controller, area) {
        area = !area ? '' : area + '/';
        controller = !controller ? '' : controller + '/';

        var url = _RootPath + area + controller + action;

        return url;
    };

    context.SetIdJq = function (controls, idConfig, subFijo) {
        controls = controls || {};
        idConfig = idConfig || '';
        subFijo = subFijo || '';

        if (!controls.Root) controls.Root = { Id: idConfig };

        $.each(controls, function (key, value) {
            value.IdConfig = idConfig;

            if (value.Id == undefined || value.Id == '')
                value.Id = idConfig + subFijo + key;

            value.IdJq = $('#' + value.Id);
        });
    };

    context.NumberOnly = function (id) {
        //' abcdefghijklmnñopqrstuvwxyzáéiou'  //Url de codigos de tecla http://help.adobe.com/es_ES/AS2LCR/Flash_10.0/help.html?content=00000525.html
        var b = '0123456789';
        $('#' + id).keypress(function (a) {
            var c = a.which, d = a.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
            (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
        });
    };

    context.Decimal = function (id) {
        var b = '0123456789.';
        $('#' + id).keypress(function (a) {
            var c = a.which, d = a.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
            (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
        });
    };

    context.GetArray = function (array, field) {
        var arr = [];

        $.each(array, function (index, val) {
            arr.push(val[field]);
        });

        return arr;
    };

    context.Clear = function (target, exceptionArr) {
        $.each($('#' + target + ' input'), function (index, val) {
            if ($.inArray(val.id, exceptionArr) == -1)
                $('#' + val.id).val('');
        });

        $.each($('#' + target + ' select'), function (index, val) {
            if ($.inArray(val.id, exceptionArr) == -1)
                $('#' + val.id).val(0);
        });
    };

    context.EncodeHTML = function (text) {
        var result = text;

        $.each(_CodesHTML, function (index, value) {
            result = result.replace(new RegExp(value[0], 'g'), value[1]);
        });

        return result;
    };

    context.DecodeHTML = function (text) {
        var result = text;

        $.each(_CodesHTML, function (index, value) {
            result = result.replace(new RegExp(value[1], 'g'), value[0]);
        });

        return result;
    };

    context.Redirect = function (action, controller, area) {
        var url = context.UrlAction(action, controller, area);

        location.href = url;
    };

    context.ParseDate = function (date, format) {
        var s = { FormatSource: format || 'dd/mm/yy' };

        return ParseDateTime(date, s);
    };

    context.CompareDate = function (dateMin, dateMax, formatMin, formatMax) {
        var result;

        dateMin = context.ParseDate(dateMin, formatMin);
        dateMax = context.ParseDate(dateMax, formatMax);

        var ticksMin = dateMin.getTime();
        var ticksMax = dateMax.getTime();

        if (ticksMin < ticksMax) result = 1;
        else if (ticksMin == ticksMax) result = 0;
        else if (ticksMin > ticksMax) result = -1;

        return result;
    };

    context.FormatDate = function (date, formatOut, formatIn) {
        var s = {
            Format: 'dd/mm/yy',
            FormatSource: context.DateFormat
        };

        if (formatOut) {
            if (typeof formatOut == 'string') {
                s.Format = formatOut;
                if (formatIn) s.FormatSource = formatIn;
            }
            else s = formatOut;
        }

        return FormatDateTime(date, s);
    };

    //*****************************************************************************************************************

    //#region GET Config

    context.GetConfigDataTable = function (data) {
        return context.GetNameSpace('Root', 'UserControl', 'DataTable').GetConfig(data);
    };

    context.GetConfigComboBox = function (data) {
        return context.GetNameSpace('Root', 'UserControl', 'ComboBox').GetConfig(data);
    };

    context.GetConfigChart = function (data) {
        return context.GetNameSpace('Root', 'UserControl', 'Chart').GetConfig(data);
    };

    //#endregion GET Config

    //#region SET Config

    context.SetConfigDataTable = function (data) {
        data.Params = data.Params || {};
        data.Params.Token = _S.Token;

        return context.GetNameSpace('Root', 'UserControl', 'DataTable').Init(data);
    }

    context.SetConfigComboBox = function (data, options) {
        if (typeof data == 'string') {
            data = context.GetAttrData(data);
            if (typeof options == 'function') data.Change = options;
            else if (typeof options == 'object') $.extend(true, data, options);
        }

        data.Parametros = data.Parametros || {};
        data.Parametros.Token = _S.Token;

        return context.GetNameSpace('Root', 'UserControl', 'ComboBox').Init(data);
    }

    context.SetConfigChart = function (data) {
        data.Params = data.Params || {};
        data.Params.Token = _S.Token;

        return context.GetNameSpace('Root', 'UserControl', 'Chart').Init(data);
    }

    context.SetConfigMapa = function (data) {
        data.Params = data.Params || {};
        data.Params.Token = _S.Token;

        return context.GetNameSpace('Root', 'UserControl', 'Mapa').SetConfig(data);
    }

    //#endregion SET Config

    //#region Alert

    context.Alert = function (options, callbackDone) {
        var data = {
            Options: options,
            CallbackDone: callbackDone,
            Defaults: {
                title: '',
                html: ''
            }
        };

        Alert(data);
    }

    context.AlertSuccess = function (options, callbackDone) {
        var data = {
            Options: options,
            CallbackDone: callbackDone,
            Defaults: {
                title: 'CORRECTO!',
                html: 'Se registro correctamente!',
                type: 'success'
            }
        };

        Alert(data);
    }

    context.AlertError = function (options, callbackDone) {
        var data = {
            Options: options,
            CallbackDone: callbackDone,
            Defaults: {
                title: 'ERROR!',
                html: 'Ocurrió un error!',
                type: 'error'
            }
        };

        Alert(data);
    }

    context.AlertQuestion = function (options, callbackDone, callbackCancel) {
        var data = {
            Options: options,
            CallbackDone: callbackDone,
            CallbackCancel: callbackCancel,
            Defaults: {
                title: 'Confirmar!',
                html: 'Desea continuar con la operación.?',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }
        };

        Alert(data);
    }

    //#endregion Alert

    context.RowAddToTable = function (table, data) {
        if (typeof table == 'string')
            table = $('#' + table);

        var tr = $('<tr/>');

        $.each(data, function (index, value) {
            var td = $('<td/>');
            td.html(value);
            tr.append(td);
        });

        table.find('tbody').append(tr);

        return tr;
    };

    context.RowIndexToTable = function (table, colIndex) {
        if (typeof table == 'string')
            table = $('#' + table);

        colIndex = colIndex || 0;

        table.find('tbody tr').each(function (index, value) {
            $(value).find('td').eq(colIndex).html(index + 1);
        });
    };

    context.RowAddSummary = function (table, data, entidad, summary) {
        var row = context.RowAddToTable(table, data);

        function DownloadFile(sender) {
            var e = $(sender).closest('tr').data('Entidad');

            if (e instanceof Array) e = e[0];

            context.SendForm(context.UrlAction('Maestro/UserControl/FileDownload'), e);
        }

        row.data('Entidad', entidad);
        row.data('Summary', summary);//No lee si lo colocamos en tabla mostraria "<table><tr"
        row.on('dblclick', function (e) { context.Alert($(this).data('Summary')); });
        row.find('a.btn-del').on('click', function (event) { $(this).closest('tr').remove(); context.RowIndexToTable(table); });
        row.find('a.btn-dow').on('click', function (event) { DownloadFile(this); });

        context.RowIndexToTable(table);
    };

    context.AddToFormData = function (data, formData, prefix, key, isLog) {
        if (formData == undefined) formData = new FormData();

        if (prefix == undefined) prefix = '';
        else if (prefix == '') prefix = key;
        else if (typeof key == 'number')
            prefix = prefix + '[' + key + ']';
        else if (key != undefined)
            prefix = prefix + '.' + key;

        if (data instanceof Object && !(data instanceof File) && !(data instanceof ArrayBuffer)) {
            $.each(data, function (k, v) {
                context.AddToFormData(v, formData, prefix, k, isLog);
            });
        } else {
            if (isLog) {
                var log = { Key: prefix, Value: data };
                console.log(log);
            }

            formData.append(prefix, data);
        }

        return formData;
    };

    context.HasEvent = function (element, event) {
        if (!element.toString().endsWith('Element]'))//includes
            element = element[0];

        var events = $._data(element, 'events');

        return events != undefined && events[event] != undefined;
    };

    context.IsValidValue = function (data, exceptions) {
        var r = true;

        exceptions = exceptions || [];

        $.each(data, function (k, v) {
            if (r && exceptions.indexOf(k) == -1) {
                //console.log(k + ': ' + v + ': ' + typeof v);
                r = v != undefined

                if (r) {
                    var t = typeof v;

                    if (t == 'number') r = v > 0;
                    else if (t == 'string') r = v != '';
                }
            }
        });

        return result;
    };

    context.MessageValidate = function (data, isAnimated, typeAnimation) {
        var r = {
            IsValid: data.length == 0
        };

        if (r.IsValid) return r;

        var p,
            l = [],
            h = '<dl class="message-validate">';

        $.each(data, function (i, v) {
            var c = v.Control,
                dt = v.Title,
                dd = v.Detail;

            if (v.Config) {
                c = v.Config.Id;
                dt = v.Config.Label;
            }

            if (c) {
                if (typeof c == 'string') c = $('#' + c);

                if (!p) {
                    if (c.is('input')) p = c;
                    else if (c.find('input').length > 0) p = c.find('input').eq(0);
                }

                l.push(c);
            }

            h += '<dt><b>' + (i + 1) + ') ' + dt + '</dt></b>' + (dd ? '<dd>' + dd + '</dd>' : '');
        });

        h += '</dl>';

        context.AlertError(
            { title: 'Campos no validos', html: h },
            function () { if (isAnimated) r.IdTimeOutAnimation = context.AnimationControls(l, typeAnimation); }
        );

        if (p) p.focus();

        return r;
    };

    context.AnimationControls = function (data, typeAnimation) {
        var controls = [];
        var idTimeOutAnimation;

        if (data instanceof Array) controls = data;
        else if (data) controls.push(data);

        function AddAnimation(control, isAdd, typeAnimation) {
            if (typeAnimation == undefined) typeAnimation = 'zoomIn';

            typeAnimation = 'infinite animated ' + typeAnimation;

            if (typeof control == 'string') control = $('#' + control);

            if (isAdd == undefined || isAdd) control.addClass(typeAnimation);
            else control.removeClass(typeAnimation);
        }

        $.each(controls, function (index, value) {
            AddAnimation(value, true, typeAnimation);

            (typeof value == 'string' ? $('#' + value) : value)
                .on('mouseover', function (event) {
                    AddAnimation($(this), false, typeAnimation);
                });
        });

        idTimeOutAnimation = setTimeout(function (a, b) {
            $.each(controls, function (index, value) {
                AddAnimation(value, false, typeAnimation);
            });
        }, 1500);

        return idTimeOutAnimation;
    };

    context.AddClassCondition = function (controls, value, classTrue, classFalse) {
        var c = [];

        if (controls instanceof Array) c = controls;
        else if (controls) c.push(controls)

        $.each(c, function (i, v) {
            if (typeof v == 'string') v = $(v);

            v.removeClass(value ? classFalse : classTrue);
            v.addClass(value ? classTrue : classFalse);
        });
    };

    context.SendForm = function (action, data, close) {
        var settings = {
            Action: '',
            Method: 'POST',
            Target: (new Date()).getTime(),
            Data: {},
            WinOpen: 1,
            Close: undefined
        };

        if (action instanceof Object) settings = $.extend(true, {}, settings, action);
        else {
            settings.Action = action;
            settings.Data = data || settings.Data;
            settings.Close = close || settings.Close;
        }

        settings.Data.Token = _S.Token;

        var frm = $('<form/>');

        $(document.body).append(frm);

        $.each(settings, function (k, v) {
            if (k != 'Data') frm.attr(k, v);
        });

        $.each(settings.Data, function (k, v) {
            if (v instanceof Object && !(data instanceof ArrayBuffer)) {
                $.each(v, function (kk, vv) {
                    var t = $('<input/>').attr('name', k + '.' + kk).attr('value', vv);
                    frm.append(t);
                });
            } else {
                var t = $('<input/>').attr('name', k).attr('value', v);
                frm.append(t);
            }
        });

        if (settings.WinOpen) {
            var win = window.open(null, settings.Target);

            if (settings.Close) {
                var inter = window.setInterval(function () {
                    if (win == null || win.closed) {
                        settings.Close();
                        window.clearInterval(inter);
                    }
                }, 1000);
            }
        }

        frm.submit().remove();
    };

    context.GetAttrData = function (control, save) {
        var d = context.GetObjectJq(control || 'divData'),
            o = $.extend(true, {}, d.data()),
            r = {};

        $.each(o, function (k, v) {
            var x = k.match(/[A-Z]/g) || [];
            for (var i = 0; i < x.length; i++) k = context.StringReplace(k, x[i], '-' + x[i].toLowerCase());

            if (!save) {
                d.removeData(k);
                d[0].removeAttribute('data-' + k);//d.removeAttr('data-' + k);
            }

            v += '';
            var a = v.split(';');

            if (a.length > 1) {
                k = a[0];
                a.shift();
                v = a.join(';');
            }

            a = v.toLowerCase();
            if (/^(true|false)$/.test(a)) v = a == 'true';
            else if (a == 'null') v = null;

            ObjectRecursive(r, k, v);
        });

        function ObjectRecursive(r, k, v) {
            var a = k.split('-');

            if (a.length > 1) {
                k = a.shift();
                r[k] = r[k] || {};

                ObjectRecursive(r[k], a.join('-'), v);
            } else {
                r[k] = v;
            }
        }

        return r;
    };

    context.Distinct = function (arr) {
        return $.grep(arr, function (v, i) {
            return $.inArray(v, arr) === i;
        });
    };

    context.GetDataOfRow = function (sender, table) {
        var t = table.DataTable();
        var s = context.GetObjectJq(sender);
        var w = s.prop('nodeName') == 'TR' ? s : s.closest('tr');
        var q = s.data('rowindex');
        var r;

        if (q) r = t.row(+q); else r = t.row(w);

        var d = r.data();

        return $.extend(true, {}, d);
    };

    context.Standby = function (isWait) {
        if (isWait) $('body').removeClass('loaded');
        else $('body').addClass('loaded');
    };

    context.ParseToTable = function (data, attr) {
        var t = $('<table/>');

        $.each(attr, function (k, v) {
            t.attr(k, v);
        });

        $.each(data, function (k, v) {
            var r = $('<tr/>');
            r.append($('<th/>').html(k + ':'));
            r.append($('<td/>').html(v));
            t.append(r);
        });

        return t.prop('outerHTML');
    };

    //*****************************************************************************************************************

    context.SetNameSpace = function (areaName, controllerName, pageName) {
        window.nsWeb = window.nsWeb || {};
        window.nsWeb[areaName] = window.nsWeb[areaName] || {};
        window.nsWeb[areaName][controllerName] = window.nsWeb[areaName][controllerName] || {};
        window.nsWeb[areaName][controllerName][pageName] = window.nsWeb[areaName][controllerName][pageName] || {};

        return window.nsWeb[areaName][controllerName][pageName];
    }

    context.GetNameSpace = function (areaName, controllerName, pageName) {
        return window.nsWeb[areaName][controllerName][pageName];
    }

    context.GetControlsJQ = function (idControl) {
        var a = idControl ? '#' + idControl : 'body';
        var r = {};

        $.each($(a).find('[id]'), function (i, v) {
            r[v.id] = $('#' + v.id);
        });

        return r;
    }

    context.GetSession = function (idControl) {
        _S = context.GetAttrData(idControl || 'divDataUser');

        return _S;
    }

    context.IsAjaxError = function (data, jqXHR) {
        var r = false,
            t = 'Error',
            m = 'Se perdió la conexión con el servidor, favor actualice al pagina.';

        if (data) {
            r = +!!data.IdError + +!!data.Exito > 0;
            m = data.Mensaje || m;
        } else if (jqXHR && jqXHR.status != 200) {
            r = true;
            m = jqXHR.getResponseHeader('Error') || jqXHR.responseText || m;
            t = jqXHR.statusText || t;
        } else if (jqXHR.responseText.includes('<!DOCTYPE html>')) {
            r = true;
            m = 'Favor de volver a iniciar sesión.';
            t = 'Actualizar Página';
        }
        //console.log(data); console.log(jqXHR); console.log(r); console.log(t); console.log(m);
        if (r) {
            console.log(data);
            context.AlertError(m, t);
        }

        return r;
    }

    context.ToBase64 = function (bytes) {
        return btoa(String.fromCharCode.apply(null, bytes));
    }

    context.FromBase64 = function (data) {
        return atob(data).split('').map(function (c) { return c.charCodeAt(0); });
    }

    context.SetDefaultEditable = function (options) {
        var settings = $.extend(true, {}, SetDefaultEditable(), options);

        //$.fn.editabletypes.datefield.defaults.inputclass = "form-control input-sm";
        //$.fn.editable.defaults.url = "/post";
        $.fn.editable.defaults.mode = settings.Mode;
        $.fn.editableform.buttons = settings.Buttons;
    }

    context.ToArrayJQ = function (array) {
        return $(array).map(function (i, v) { return v.toArray(); });
    }

    context.StringFormat = function (text) {
        if (arguments.length < 2) return text;

        for (var i = 1; i < arguments.length; i++) {
            text = text.replace(new RegExp('\\{' + (i - 1) + '\\}', 'g'), arguments[i]);
        }

        return text;
    }

    context.GetColumn = function (row, columnName) {
        row = context.GetObjectJq(row);

        var r = row.prop('nodeName') == 'TR' ? row : row.closest('tr'),
            i = r.closest('table').find(context.StringFormat('thead th[data-column="{0}"]', columnName)).index();

        return r.find(context.StringFormat('td:nth-child({0})', ++i));
    }

    context.GetObjectJq = function (data) {
        return data instanceof jQuery ? data : (typeof data == 'string' ? $('#' + data) : $(data));
    }

    context.StringInsert = function (data, value, index) {
        data = (data || '') + '';
        value = value || '';
        index = index || 0;

        return data.substr(0, index) + value + data.substr(index);
    }

    context.GetMaxZIndex = function (selector) {
        selector = selector || 'body div';
        var r = $.map($(selector), function (v, i) { return parseInt($(v).css('z-index')) || 1; });

        return Math.max.apply(null, r);
    }

    context.StringReplace = function (text, oldText, newText, flags) {
        var r = text;
        if (oldText instanceof Array) for (var i = 0; i < oldText.length; i++) r = r.replace(new RegExp(oldText[i], flags || 'g'), newText);
        else r = r.replace(new RegExp(oldText, flags || 'g'), newText);
        return r;
    }

    context.GetDataTable = function (entidad, action, param, done, always, fail, async) {
        param = param || {};

        var p = {
            Entidad: entidad,
            AyudaLista: action,
            Parametros: param,
            Tried: param.Tried,
            Token: _S.Token,
            IdAplicacion: param.IdApp || 1
        };

        context.Ajax('UserControl', 'GetDataTable', p,
            function (data, textStatus, jqXHR) { if (done) done(data, textStatus, jqXHR); },
            function (data, textStatus, jqXHR) { if (always) always(data, textStatus, jqXHR); },
            function (jqXHR, textStatus, errorThrown) { if (fail) fail(jqXHR, textStatus, errorThrown); },
            async);
    }

    context.Distance = function (x1, y1, x2, y2) {
        return context.GetNameSpace('Root', 'UserControl', 'Mapa').Distance(x1, y1, x2, y2);
    }

    context.CurrentScript = function () {
        var a = document.currentScript || function () {
            var a = document.getElementsByTagName("script");
            return a[a.length - 1]
        }();

        return a;
    };

    context.DefaultFormatDateTime = function () { return DefaultFormatDateTime(); };

    context.ConvertPeriodo = function (periodo) {
        var a = periodo.split('-'),
            m = DefaultFormatDateTime().MonthNamesShort.indexOf(a[0]),
            n = Number(a[1]),
            p = ((n > 70 ? 1900 : 2000) + n) * 100 + (m + 1);

        return p;
    }

    context.GetDataFromTag = function (data, tipo) {
        var d = [],
            r = [];

        if (data.TipoPanel == 7) d = data.ListaRegistro;
        else if (data.TipoPanel == 8) d = data.ListaRegistro[0].split('*');

        for (var i = 0; i < d.length; i++) {
            var a = {},
                b = d[i].split(';');

            for (var j = 0; j < b.length; j++) {
                var aa = b[j].split('|'),
                    bb = b[j].split(':'),
                    cc = [];

                if (aa.length > 1 && bb.length == 1) cc = aa;
                else if (aa.length == 1 && bb.length > 1) cc = bb;
                else {
                    a.clave = aa[0].split(':')[1];
                    a.valor = aa[1].split(':')[1];
                }

                if (cc.length > 0) a[cc[0]] = cc[1];
            }

            r.push(a);
        }

        if (tipo == 1) return r;
        else {
            var t = {};
            for (var i = 0; i < r.length; i++) t[r[i].clave] = r[i].valor;
            return t;
        }
    }

    context.DownloadFile = function (url, data, target, winOpen) {
        var d = {
            Action: url,
            Data: data,
            WinOpen: winOpen == undefined ? 1 : winOpen,
            Target: target || '_SELF'
        };

        context.SendForm(d);
    }

    context.CaptchaReady = function (fun) {
        grecaptcha.ready(function () {
            context.CaptchaNew(fun);
            setInterval(function () { context.CaptchaNew(); }, 115000);
        });
    }

    context.CaptchaReady02 = function (fun) {
        grecaptcha.ready(function () {
            if (fun) fun();
        });
    }

    context.CaptchaNew = function (fun) {
        grecaptcha.execute(_TokenCaptcha, { action: 'social' }).then(function (token) {
            _S.TokenCaptcha = token;
            if (fun) fun(token);
        });
    }

    context.CaptchaToken = function () {
        var r = _S.TokenCaptcha;
        context.CaptchaNew();
        return r;
    }

    context.UrlContent = function (url) {
        return _RootPath + url;
    }

    context.UrlContentFull = function (url) {
        return location.origin + _RootPath + url;
    }

    context.isEmptyFunc = function (val) {

        return val === undefined || val === null || val === '' || (Array.isArray(val) && val.length === 0)
    };

    context.FillComboBox = function (control, param) {
        control.html('');

        var tg = false;
        if (!param.IsNotAll) control.html('<option value="0">---Seleccione---</option>');

        nsUtil.Ajax('Home', 'AyudaLista', { 'Parametros': param }, function (data) {
            $.each(data.Datos, function (i, v) {
                var a = v.Id || v.id,
                    b = v.Nombre || v.nombre,
                    c = (param.Value || control.data('value')) == a,
                    t = context.StringFormat('<option value="{0}" {2}>{1}</option>', a, b, c ? 'selected' : '');

                tg |= c;

                control.append(t);
            });

            if (param.Select2) control.select2({
                closeOnSelect: false,
                placeholder: 'Seleccione...',
                allowHtml: true,
                allowClear: true,
                tags: true
            });

            if (tg) control.trigger('change');
        });
    }

    //#endregion Public
})(window.nsUtil, jQuery);