﻿/* function */
/* 
 * Fehca: 01/10/2019
 * Autor: Jesús Alberto Peña Trujillo
 */

/*
 * confirm(question, cancelButtonTxt, okButtonTxt, callback)
 * @param {any} question --  La Pregunta
 * @param {any} cancelButtonTxt -- Boton Cancelar
 * @param {any} okButtonTxt Boton -- Aceptar
 * @param {any} callback -- Funcion que se realiaza 
 */
    function confirm(question, cancelButtonTxt, okButtonTxt, callback) {
        if (cancelButtonTxt == "") { cancelButtonTxt = "Cancelar" }
        if (okButtonTxt == "") { okButtonTxt = "Aceptar" }
        var confirmModal =
            $('<div class="modal fade">' +
                '<div class="modal-dialog modal-sm">' +
                '<div class="modal-content">' +
                '<div class="modal-body">' +
                '<p class="text-center">' + question + '</p>' +
                '<div class="form-group">' +
                '<div class= "row" >' +
                '<div class="col-sm-6 text-center">' +
                '<button type="button" class="btn btn-red font-montserrat" data-dismiss="modal" id="btnCerrar"><i class="fa fa-close" aria-hidden="true"></i>' + cancelButtonTxt + '</button>' +
                '</div>' +
                '<div class="col-sm-6 text-center">' +
                '<button type="submit" class="btn btn-green font-montserrat" id="btnAceptar"><i class="" aria-hidden="true"></i>' + okButtonTxt + '</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
        confirmModal.find('#btnAceptar').click(function (event) {
            callback();
            confirmModal.modal('hide');
        });
        confirmModal.modal('show');
};



function MsgBox(mensaje, tipo) {

    if (tipo === "Error")
    {       

        jQuery('#modError').find('.modal-body').html(mensaje);
        jQuery('#modError').modal('show');
    }

    else if (tipo === "Info")
    {
        jQuery('#modInfo').find('.modal-body').html(mensaje);
        jQuery('#modInfo').modal('show');
    }


}








