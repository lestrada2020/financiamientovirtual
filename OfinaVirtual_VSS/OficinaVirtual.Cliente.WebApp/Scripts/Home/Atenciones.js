﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación        : 2019-03-01
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _C.btnBuscar.on('click', btnBuscar_Click);
            _C.txtNroAtencion.on('keydown', txtNroAtencion_keydown);
        });
    }

    //#region Events

    //#endregion Events

    function txtNroAtencion_keydown(e) {
        if (e.which == 13) _C.btnBuscar.click();
    }

    function btnBuscar_Click(event) {
        event.preventDefault();

        var p = {
            Parametros: {
                NroAtencion: _C.txtNroAtencion.val()
            }
        };

        if (p.Parametros.NroAtencion.length == 0) {
            _U.AlertError('Favor de ingresar un nro. de atención.');
            return;
        }

        $('#modPreload').modal('show');
        _C.tblHeader.find('tbody').html('');
        _C.tblDetail.find('thead, tbody').html('');
        _C.btnBuscar.prop('disabled', true).removeClass('btn-green');

        _U.Ajax('Home', 'Atenciones', _U.AddToFormData(p), function (data) {
            var d = JSON.parse(data.Datos),
                r = _C.tblHeader.find('tbody'),
                s = _C.tblDetail.find('tbody'),
                c = 0;

            if (d.Header.length == 0) {
                _U.AlertError('No existe información para mostrar, para la Atención: ' + p.Parametros.NroAtencion);
                return;
            }

            r.data('info', d.Header[0]);
            $.each(d.Header[0], function (k, v) {
                if (k.startsWith('-')) return;
                r.append(_U.StringFormat('<tr><th>{0}</th><td>{1}</td></tr>', k, v));
            });

            _U.AlertSuccess('Se encontraron (' + d.Detail.length + ') eventos, para la atención: ' + p.Parametros.NroAtencion);
            for (var i = 0; i < d.Detail.length; i++) {
                var aa = d.Detail[i],
                    tr = $('<tr/>'),
                    th = $('<tr/>');

                tr.data('info', aa);
                $.each(aa, function (k, v) {
                    if (k.startsWith('-')) return;

                    th.append(_U.StringFormat('<th>{0}</th>', k));
                    tr.append(_U.StringFormat('<td>{0}</td>', v));
                });

                if (i == 0) _C.tblDetail.find('thead').append(th);
                s.append(tr);
            }
        }, function (data) {
            $('#modPreload').modal('hide');
            _C.btnBuscar.prop('disabled', false).addClass('btn-green');
        });
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Home', 'Atenciones'), jQuery));