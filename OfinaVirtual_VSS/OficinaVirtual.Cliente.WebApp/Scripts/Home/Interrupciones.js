﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación        : 2019-03-01
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _C.btnBuscar.on('click', btnBuscar_Click);
        });
    }

    //#region Events

    //#endregion Events

    function btnBuscar_Click(event) {
        var p = {
            Parametros: {
                IdNroServicio: _C.txtSuministro.val(),
            }
        };

        $('#modPreload').modal('show');
        _C.btnBuscar.prop('disabled', true).removeClass('btn-green');

        _U.Ajax('Home', 'Interrupciones', _U.AddToFormData(p), function (data) {
            var d = JSON.parse(data.Datos),
                r = _C.divResult,
                c = 0;

            r.html('');

            for (var i = 0; i < d.length; i++) {
                var v = d[i],
                    a = _C.tmpInterrupcion.clone().removeAttr('id'),
                    t = '';

                if (v.EsProgramada == 2) continue;

                if (v.EsProgramada == 0) t = _U.StringFormat('Se comunica a los clientes y opinión pública que se produjo un evento inesperado el cual interrumpió el servicio eléctrico el día {0} desde las {1}. <br /><b>Zona(s) afectadas:<b/> {2}', v.FechaInicio, v.HoraInicio, v.Localidades);
                else if (v.EsProgramada == 1) t = _U.StringFormat('Se comunica a los clientes y opinión pública con la finalidad de realizar trabajos de mantenimiento preventivos se interrumpirá el servicio eléctrico el día {0} desde las {1} hasta las {2}. <br /><b>Zona(s) afectadas:<b/> {3}', v.FechaInicio, v.HoraInicio, v.HoraFin, v.Localidades);

                a.find('.card-header > span:first-child').html(v.TipoRegistro.toUpperCase());
                a.find('.card-header > span:last-child').html(v.FechaInicio);
                a.find('.card-text').html(t);

                r.append(a);
                c++;
            }

            if (c == 0) _U.AlertError('No existe información para mostrar, para el suministro: ' + p.Parametros.IdNroServicio);
            else _U.AlertSuccess('Se encontraron (' + c + ') eventos, para el suministro: ' + p.Parametros.IdNroServicio);
        }, function (data) {
            $('#modPreload').modal('hide');
            _C.btnBuscar.prop('disabled', false).addClass('btn-green');
        });
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Home', 'Interrupciones'), jQuery));