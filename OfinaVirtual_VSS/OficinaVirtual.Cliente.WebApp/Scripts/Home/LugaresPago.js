﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-05-20
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            FillEmpresa();
            _C.cmbEmpresa.on('change', cmbEmpresa_change);
        });
    }

    //#region Events

    function cmbEmpresa_change() {
        var a = _C.cmbEmpresa.val(),
            b = '<iframe src="https://www.google.com/maps/d/embed?mid={0}" width="640" height="480"></iframe>',
            c = '';

        switch (+a) {
            case 1:
                c = '1b0TGpCNpu1PUaA6Z6tVYHDmIPxfri8ku';
                break;
            case 2:
                c = '1BE53p5QCofrTDfDx5rnSogfPlYWFGJcK';
                break;
            case 3:
                c = '1DAHS_HazZZtgPQIFB8J4WxQsCGZo3aOS';
                break;
            case 4:
                c = '1y4pyWZ_Q-KyP1WSYN7zpt6ZwhoVtxFcv&hl=es';
                break;
            default:
                b = '';
                break;
        }

        _C.mapa.html(_U.StringFormat(b, c));
    }

    //#endregion Events

    function FillComboBox(control, param) {
        control.html('<option value="0">---Seleccione su empresa---</option>');
        nsUtil.Ajax('Home', 'AyudaLista', { 'Parametros': param }, function (data) {
            $.each(data.Datos, function (i, v) {
                var k = v.Id || v.id,
                    t = _U.StringFormat('<option value="{0}" {2}>{1}</option>', k, v.Nombre || v.nombre, (+k == +param.Value ? 'selected' : ''));

                control.append(t);
            });
        });
    }

    function FillEmpresa() {
        var p = {
            Entity: 'Empresa',
            idorganizacion: 1,
            AddPreFix: 0,
            Value: 0
        };

        FillComboBox(_C.cmbEmpresa, p);
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Home', 'LugaresPago'), jQuery));