﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-07-24
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal'),
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function () {
            _C = _U.GetControlsJQ();
            _O.Month = _U.DefaultFormatDateTime().MonthNames;

            $('[data-toggle="tooltip"]').tooltip();

            _C.txtIdNroServicio.on('keypress', txtIdNroServicio_keypress);
            _C.txtDNI.on('keypress', txtDNI_keypress);
            _C.txtPhone.on('keypress', txtPhone_keypress);
            _C.btnCancel.on('click', btnCancel_click);
            _C.btnSave.on('click', btnSave_click);
            _C.txtIdNroServicio.focus();
        });
    }

    function ValidSave() {
        _C.txtDNI.next().hide();
        _C.txtPhone.next().hide();
        _C.rdTitularSi.closest('.row').find('> span').hide();
        _C.chkTerminos.parent().next().hide();

        var r = true;
        r &= _C.txtIdNroServicio[0].checkValidity();
        r &= _C.txtDNI[0].checkValidity();
        r &= _C.txtNombres[0].checkValidity();
        r &= _C.txtApellidoP[0].checkValidity();
        r &= _C.txtApellidoM[0].checkValidity();

        if (_C.txtEmail.val().length > 0) {
            if (!_C.txtEmail[0].checkValidity()) {
                r = false;
                _C.txtEmail.next().html(_C.txtEmail[0].validationMessage).show();
            }
        }

        if (_C.txtDNI.val().length < 8) {
            r = false;
            _C.txtDNI.next().html('DNI incompleto').show();
        }

        if (isNaN(_C.txtDNI.val())) {
            r = false;
            _C.txtDNI.next().html('DNI no valido').show();
        }

        if (_C.txtPhone.val().length < 9) {
            r = false;
            _C.txtPhone.next().html('Celular incompleto').show();
        }

        if (_C.txtPhone.val().substr(0, 1) != 9) {
            r = false;
            _C.txtPhone.next().html('Celular debe empezar con 9').show();
        }

        if (isNaN(_C.txtPhone.val())) {
            r = false;
            _C.txtPhone.next().html('Celular no valido').show();
        }

        if (!_C.rdTitularSi.prop('checked') && !_C.rdTitularNo.prop('checked')) {
            r = false;
            _C.rdTitularSi.closest('.row').find('> span').show();
        }

        if (!_C.chkTerminos.prop('checked')) {
            r = false;
            _C.chkTerminos.parent().next().show();
        }

        if (!r) _C.frmUpdate.addClass('was-validated');

        return r;
    }

    //#region AJAX

    function GetSuministro(token) {
        var p = {
            TokenCaptcha: token,
            IdNroServicio: _C.txtIdNroServicio.val()
        };

        _U.Ajax('Suministro', 'ObtenerBeneficiario', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            _C.txtIdNroServicio.data('sumin', d);
            _U.CaptchaNew(SaveContacto);
        }, function (data) {
            if (+data.IdError > 0) {
                _C.txtIdNroServicio.next().html('Suministro no valido.');
                setTimeout(function () { _C.txtIdNroServicio.focus().select(); }, 300);
                _L.Loading(_C.btnSave, 1);
            }
        });
    }

    function SaveContacto(token) {
        var d = _C.txtIdNroServicio.data('sumin'),
            p = {
                TokenCaptcha: token,
                DNI: _C.txtDNI.val(),
                Nombres: _C.txtNombres.val(),
                ApePaterno: _C.txtApellidoP.val(),
                ApeMaterno: _C.txtApellidoM.val(),
                NroCeular: _C.txtPhone.val(),
                Correo: _C.txtEmail.val(),
                EsTitular: _C.rdTitularSi.prop('checked'),
                IdNroServicio: d.IdNroservicio,
                PFactura: +d.PFactura + '03',
                ConsumoKWH: +d.ConsumoKWH,
                IdTipoRegistro: 3
            };

        _U.Ajax('Suministro', 'RegistrarBeneficiario', _U.AddToFormData({ Parametros: p }), function (data) {
            var s = 'Se registro correctamente sus datos.'
            
            _C.btnSave.remove();
            _U.AlertSuccess(s);
            _C.btnCancel.html('Salir');
        }, function () { _L.Loading(_C.btnSave, 1); });
    }

    //#endregion

    //#region Events

    function txtIdNroServicio_keypress(e) {
        if (e.which < 48 || e.which > 57 || _C.txtIdNroServicio.val().length > 7) return false;

        return true;
    }

    function txtDNI_keypress(e) {
        if (e.which < 48 || e.which > 57 || _C.txtDNI.val().length > 7) return false;

        return true;
    }

    function txtPhone_keypress(e) {
        if (e.which < 48 || e.which > 57 || _C.txtPhone.val().length > 8) return false;

        return true;
    }

    function btnSave_click() {
        _L.Loading(_C.btnSave, 0);
        _L.ClearAlert();

        if (!ValidSave()) {
            _L.Loading(_C.btnSave, 1);
            return;
        }

        _U.CaptchaNew(GetSuministro);
    }

    function btnCancel_click() {
        location.href = _U.UrlAction('MiContacto', 'Home');;
    }

    //#endregion Events

    //#endregion Private

    //#region Public

    (function () { _U.CaptchaReady02(Load); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Home', 'MiContacto'), jQuery));