﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-07-16
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal'),
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            $('[data-toggle="tooltip"]').tooltip();

            _C.txtIdNroServicio.on('keypress', txtIdNroServicio_keypress);
            _C.cmbType.on('change', cmbType_change);
            _C.btnSearch.on('click', btnSearch_click);
            _C.txtIdNroServicio.focus();
        });
    }

    //#region Events

    function btnSearch_click() {
        _L.Loading(_C.btnSearch, 0);

        if (!Valid()) {
            _L.Loading(_C.btnSearch, 1);
            return;
        }

        GetBono();
    }

    function txtIdNroServicio_keypress(e) {
        Clear();

        if (e.which == 13) btnSearch_click();
        else if (e.which < 48 || e.which > 57 || _C.txtIdNroServicio.val().length > 8) return false;

        return true;
    }

    function cmbType_change(e) {
        Clear();

        var a = _C.cmbType.val(),
            b = _C.cmbType.find('> option:selected').html();

        _C.txtIdNroServicio.val('').focus();
        _C.txtIdNroServicio.attr('placeholder', _U.StringFormat('Ingrese {0}', b));
        _C.txtIdNroServicio.next().hide().html(_U.StringFormat('El {0} es obligatorio.', b));
    }

    //#endregion Events
    function Clear() {
        _L.ClearAlert();
        _C.divResult.addClass('d-none');
    }

    function GetEntidadSearch() {
        var e = {
            IdType: _C.cmbType.val(),
            IdNroServicio: _C.txtIdNroServicio.val(),
            DNI: _C.txtIdNroServicio.val()
        };

        return e;
    }

    function Valid() {
        var a = _C.cmbType.val(),
            b = _C.cmbType.find('> option:selected').html(),
            v = _C.txtIdNroServicio.val();

        _C.txtIdNroServicio.next().hide();

        var r = true;
        r &= _C.txtIdNroServicio[0].checkValidity();

        if (v.length != 8) {
            if (a == 2) {
                r = false;
                _C.txtIdNroServicio.next().html('DNI incorrecto, debe tener 8 dígitos').show();
            } else if (v.length < 6) {
                r = false;
                _C.txtIdNroServicio.next().html(_U.StringFormat('{0} incorrecto, debe tener mas de 5 dígitos', b)).show();
            }
        }

        if (isNaN(v)) {
            r = false;
            _C.txtIdNroServicio.next().html(_U.StringFormat('{0} no es valido, ingrese solo números', b)).show();
        }

        //if (!r) _C.frmQuery.addClass('was-validated');

        return r;
    }

    function ShowResult(param, data) {
        var r = param.IdType == 1 ? data.IdNroServicio : data.DNI,
            d = data,
            c = 'alert alert-warning',
            s = _C.divHideNo.html();

        if (+r > 0) {
            c = 'alert alert-success';
            s = _U.StringFormat(_C.divHideYes.html(), d.Nombres);
        }

        _C.divResult.removeClass().addClass(c).html(s);
    }

    //#region Ajax

    function GetBono() {
        var p = GetEntidadSearch();

        p.TokenCaptcha = _U.CaptchaToken(),
            _U.Ajax('Home', 'MiBonoAsync', _U.AddToFormData({ Parametros: p }), function (data) {
                var d = JSON.parse(data.Datos);
                ShowResult(p, d);
            }, function (data) { _L.Loading(_C.btnSearch, 1); });
    }

    //#endregion Ajax

    //#endregion Private

    //#region Public

    (function () { _U.CaptchaReady(Load); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Home', 'MiBono'), jQuery));