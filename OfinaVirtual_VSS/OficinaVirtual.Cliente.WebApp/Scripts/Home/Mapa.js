﻿$(document).ready(inicialize);

var map = null;
var creaMarcador = [];
var dibujaCirculo = [];

function inicialize() {
    map = null;
    creaMarcador = [];
    dibujaCirculo = [];

    listarTipoCentro();
    listarDistrito();

    $("#TipoCentro").on("change", function () {
        $("#TipoCentro").prop('disabled', true);
        $("#Distrito").prop('disabled', true);

        var sTipoCentro = $("#TipoCentro").val();
        var sDistrito = $("#Distrito").val();

        listarLugaresPago(sTipoCentro, sDistrito);
    });

    $("#Distrito").on("change", function () {
        $("#TipoCentro").prop('disabled', true);
        $("#Distrito").prop('disabled', true);

        var sTipoCentro = $("#TipoCentro").val();
        var sDistrito = $("#Distrito").val();

        listarLugaresPago(sTipoCentro, sDistrito);
    });
}

function initMap() {
    var l = new google.maps.LatLng(-8.113241, -79.0262146);
    map = new google.maps.Map(document.getElementById('mapa'), { zoom: 13, center: l });
    listarLugaresPago("0", "0");
    MyGeoLocation();
}

function MyGeoLocation() {
    //return;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            MapCenter(position.coords.latitude, position.coords.longitude);
        });
    }
}

function MapCenter(latitud, longitud) {
    var p = new google.maps.LatLng(latitud, longitud);
    map.setCenter(p);
}

function listarTipoCentro() {
    var iDefault = "<option value=\"0\">Todos</option>";
    $("select#TipoCentro").empty();
    $(iDefault).appendTo("select#TipoCentro");

    $.ajax({
        url: 'ListarTipoCentro',
        type: 'POST',
        async: true,
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function (oData) {
            for (let oFila in oData) {
                var row = "<option value=\"" + oData[oFila].Id + "\">" + oData[oFila].Nombre + "</option>";
                $(row).appendTo("select#TipoCentro");
            }
        },
        error: function (oData) {
        }
    });
}

function listarDistrito() {
    var iDefault = "<option value=\"0\">Todos</option>";
    $("select#Distrito").empty();
    $(iDefault).appendTo("select#Distrito");

    $.ajax({
        url: 'ListarDistritos',
        type: 'POST',
        async: true,
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function (oData) {
            $.each(oData, function (i, item) {
                var row = "<option value=\"" + item.Id + "\">" + item.Nombre + "</option>";
                $(row).appendTo("select#Distrito");
            });
        },
        error: function (oData) {
        }
    });
}

function listarLugaresPago(sTipoCentro, sDistrito) {
    sTipoCentro = 1;

    limpiarMapa();

    $.ajax({
        url: 'ListarLugaresPago',
        data: { TipoCentro: sTipoCentro, Distrito: sDistrito },
        type: 'POST',
        async: false,
        dataType: 'JSON',
        success: function (oData) {
            if (oData.Mensaje != null) {
                alert(oData.Mensaje);
                return;
            }

            if (oData.Centros.length != null) creaMarcador.length = oData.Centros.length;
            if (oData.Centros.length > 0 && parseInt(sDistrito) > 0) MapCenter(oData.Centros[0].Latitud, oData.Centros[0].Longitud);
            else MyGeoLocation();

            for (let oFila in oData.Centros) {
                $.ajax({
                    url: 'DetalleCentro',
                    data: { IdCentro: oData.Centros[oFila].IdCentro },
                    type: 'POST',
                    async: false,
                    dataType: 'JSON',
                    success: function (oDatos) {
                        var sServicios = '';
                        for (let i in oDatos.Servicios) {
                            sServicios = sServicios + '<span class="dlHora">' + oDatos.Servicios[i] + '</span>';
                        }

                        var popup = '<div class="dlPopup">' +
                            '<span class="dlTitular">' + oDatos.Detalle[0].NombreCentro + '</span>' +
                            '<span class="dlDireccion">' + oDatos.Detalle[0].DireccionCentro + '</span>' +
                            '<span class="dlInfo">Horario de atención:</span>' +
                            '<span class="dlHora">' + oDatos.Detalle[0].Horario1 + '</span>' +
                            '<span class="dlHora2">' + oDatos.Detalle[0].Horario2 + '</span>' +
                            '<span class="dlInfo">Servicios:</span>' + sServicios +
                            '</div>';

                        crearElementoMapa(oFila, oData.Centros[oFila].Latitud, oData.Centros[oFila].Longitud, 80, 100, popup);
                    },
                    error: function (oDatos) {
                    }
                });
            }
        },
        error: function (oData) {
        }
    });

    $("#TipoCentro").prop('disabled', false);
    $("#Distrito").prop('disabled', false);
}

function limpiarMapa() {
    //alert("Entro LimpiarMapa");
    for (var i = 0; i < creaMarcador.length; i++) {
        creaMarcador[i].setMap(null);
        creaMarcador[i] = null;
    };
    //alert("Salio LimpiarMapa");
}

function crearElementoMapa(idElemento, latitud, longitud, diametro, x, Centro) {
    //alert("Entro crearElementoMapa");
    if (creaMarcador[idElemento] == 'undefined' || creaMarcador[idElemento] == null) {
        //alert(idElemento);
        var coordenadas = new google.maps.LatLng(latitud, longitud);

        creaMarcador[idElemento] = new google.maps.Marker({
            position: coordenadas,
            map: map
        });

        var infowindow = new google.maps.InfoWindow({
            content: Centro
        });

        google.maps.event.addListener(infowindow, 'domready', function () {
            // Reference to the DIV that wraps the bottom of infowindow
            var iwOuter = $('.gm-style-iw');

            /* Since this div is in a position prior to .gm-div style-iw.
             * We use jQuery and create a iwBackground variable,
             * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
            */
            var iwBackground = iwOuter.prev();

            // Removes background shadow DIV
            iwBackground.children(':nth-child(2)').css({ 'display': 'none' });

            // Removes white background DIV
            iwBackground.children(':nth-child(4)').css({ 'display': 'none' });

            // Moves the infowindow 115px to the right.
            iwOuter.parent().parent().css({ left: '0px' });

            // Moves the shadow of the arrow 76px to the left margin.
            iwBackground.children(':nth-child(1)').attr('style', function (i, s) { return s + 'left: 76px !important;' });

            // Moves the arrow 76px to the left margin.
            iwBackground.children(':nth-child(3)').attr('style', function (i, s) { return s + 'left: 76px !important;' });

            // Changes the desired tail shadow color.
            iwBackground.children(':nth-child(3)').find('div').children().css({ 'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index': '1' });

            // Reference to the div that groups the close button elements.
            var iwCloseBtn = iwOuter.next();

            // Apply the desired effect to the close button
            iwCloseBtn.css({ opacity: '1', right: '38px', top: '-3px', border: '0px solid #48b5e9', background: '#48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9' });

            // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
            if ($('.iw-content').height() < 140) {
                $('.iw-bottom-gradient').css({ display: 'none' });
            }

            // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
            iwCloseBtn.mouseout(function () {
                $(this).css({ opacity: '1' });
            });
        });

        creaMarcador[idElemento].addListener('click', function () {
            infowindow.open(map, creaMarcador[idElemento]);
        });

        creaMarcador[idElemento].setMap(map);
    } else {
        eliminarElementoMapa(idElemento);
    }
    //alert("Salio crearElementoMapa");
}

function eliminarElementoMapa(idElemento) {
    //alert("Entro eliminarElementoMapa");
    creaMarcador[idElemento].setMap(null);
    dibujaCirculo[idElemento].setMap(null);
    creaMarcador[idElemento] = null;
    dibujaCirculo[idElemento] = null;
    //alert("Salio eliminarElementoMapa");
}