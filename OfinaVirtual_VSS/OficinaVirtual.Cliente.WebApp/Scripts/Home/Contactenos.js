﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2019-03-01
    Responsable : JONATHAN VALDERRAMA
*/

function CargarPartialViewAsync(Ruta, Contenedor) {
    jQuery.ajax({
        type: "GET",
        async: true,
        url: Ruta,
        success: function (oDatos) {
            jQuery(Contenedor).html(oDatos);
        }
    });
}

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            FillEmpresa();
            $('form.frmContactenos').on('submit', frmContactenos_submit);
            _C.txtIdNroServicio.on('keypress', txtIdNroServicio_keypress);
        });
    }

    //#region Events

    function frmContactenos_submit(e) {
        var a = _C.cmbEmpresa.val();

        if (_C.txtIdNroServicio.val() == '') _C.txtIdNroServicio.val('0');

        if (+a == 0) {
            e.preventDefault();
            _C.cmbEmpresa.next().removeClass('d-none');
        }
    }

    function txtIdNroServicio_keypress(e) {
        if (e.which < 48 || e.which > 57 || _C.txtIdNroServicio.val().length > 8)
            return false;
        return true;
    }

    //#endregion Events

    function FillComboBox(control, param) {
        control.html('<option value="0">---Seleccione---</option>');
        nsUtil.Ajax('Home', 'AyudaLista', { 'Parametros': param }, function (data) {
            $.each(data.Datos, function (i, v) {
                var k = v.Id || v.id,
                    t = _U.StringFormat('<option value="{0}" {2}>{1}</option>', k, v.Nombre || v.nombre, (+k == +param.Value ? 'selected' : ''));

                control.append(t);
            });
        });
    }

    function FillEmpresa() {
        var p = {
            'Entity': 'Empresa',
            'idorganizacion': 1,
            'AddPreFix': 0,
            'Value': _C.cmbEmpresa.data('value')
        };

        FillComboBox(_C.cmbEmpresa, p);
    }

    function ValidSuministro() {
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Home', 'Contactenos'), jQuery));