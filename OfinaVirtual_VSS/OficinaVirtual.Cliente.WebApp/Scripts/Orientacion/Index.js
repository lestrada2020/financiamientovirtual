﻿$(document).ready(inicialize);

function inicialize() {

    var List = document.querySelector('.navOrientacion');
    List.children[0].className = 'btnList d-flex justify-content-between actionColor';

    $(".navOrientacion a").click(function (event) {
        var Ruta = $(this).attr("data-url");
        var IdTipo = $(this).attr("data-id");

        var List = document.querySelector('.navOrientacion');

        for (var p = 0; p <= List.children.length - 1; p++) {
            List.children[p].className = 'btnList d-flex justify-content-between';
        }
        $(this).parent().attr('class', $(this).parent().attr('class') + ' actionColor');

        CargarPartialViewAsync(Ruta, ".divRequisitos", IdTipo);
    });

}

function CargarPartialViewAsync(Ruta, Contenedor, Parametro) {
    jQuery.ajax({
        type: "GET",
        async: true,
        url: Ruta,
        data: { IdTipoOrientacion: Parametro },
        success: function (oDatos) {
            jQuery(Contenedor).html(oDatos);
        }
    });
}