﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-10-14
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            $('img.logoVisa').parent().hide();

            _C.imgHead1.on('click', imgHead1_click);
            _C.imgHead2.on('click', imgHead2_click);
            _C.imgHead3.on('click', imgHead3_click);

            _C.crdAccordion.on('click', '.card-header button', buttonLinkHead_click);
            _C.crdAccordion.on('click', '.card-header .bn-card-ico', spanLinkHead_click);
            CreateAccordion(_C.crdAccordion, GetData());
        });
    }

    //#region Events

    function buttonLinkHead_click() {
        var c = 'bn-color-active',
            s = $(this);

        _C.crdAccordion.find('.card-header button').removeClass(c).next().find('i').removeClass('fa-minus ' + c).addClass('fa-plus');
        s.addClass(c).next().find('i').removeClass('fa-plus').addClass('fa-minus ' + c);
    }

    function spanLinkHead_click() {
        $(this).prev().click();
    }

    function imgHead1_click() {
        imgHeadDefault();
        CreateAccordion(_C.crdAccordion, GetData());
        _C.imgHead1.attr('src', _U.UrlContent('/Content/img/ReciboPreguntas/FacturacionActive.png'));
    }

    function imgHead2_click() {
        imgHeadDefault();
        CreateAccordion(_C.crdAccordion, GetDataFraccionamiento());
        _C.imgHead2.attr('src', _U.UrlContent('/Content/img/ReciboPreguntas/FraccionamientoActive.png'));
    }

    function imgHead3_click() {
        imgHeadDefault();
        CreateAccordion(_C.crdAccordion, GetDataReclamos());
        _C.imgHead3.attr('src', _U.UrlContent('/Content/img/ReciboPreguntas/ReclamosActive.png'));
    }

    //#endregion Events

    function imgHeadDefault() {
        _C.imgHead1.attr('src', _U.UrlContent('/Content/img/ReciboPreguntas/Facturacion.png'));
        _C.imgHead2.attr('src', _U.UrlContent('/Content/img/ReciboPreguntas/Fraccionamiento.png'));
        _C.imgHead3.attr('src', _U.UrlContent('/Content/img/ReciboPreguntas/Reclamos.png'));
    }

    function CreateAccordion(sender, data) {
        var d = data;

        sender.html('');

        for (var i = 0; i < d.length; i++) {
            d[i].Index = i + 1;

            CreateCard(d[i], sender);
        }
    }

    function CreateCard(item, parent) {
        var a = 'crdHead' + item.Index,
            b = 'crdBody' + item.Index,
            h = '<div id="{0}" class="card-header"><h5 class="mb-0"><button class="btn btn-link bn-text-header bn-color" data-toggle="collapse" data-target="#{1}" aria-expanded="true" aria-controls="{1}">{3}) {2}</button><span class="bn-card-ico bn-color"><i class="fa fa-plus" aria-hidden="true"></i></span></h5></div>',
            t = '<div id="{1}" class="collapse" aria-labelledby="{0}" data-parent="#{3}"><div class="card-body">{2}</div></div>',
            r = '<div class="card">{0}{1}</div>';

        h = _U.StringFormat(h, a, b, item.Request, (+item.Index < 10 ? '0' : '') + item.Index);
        t = _U.StringFormat(t, a, b, item.Response, parent[0].id);
        r = _U.StringFormat(r, h, t);

        parent.append(r);
    }

    function GetData() {
        var r = [{
            Request: '¿Por qué el monto de mi recibo varía en algunos meses?',
            Response: 'Es importante tener en cuenta que el monto que observas en tu recibo está relacionado directamente con tu consumo de electricidad. <b>No todos los meses se tiene los mismos hábitos de consumo</b>, en algunos meses el gasto es mayor como en Diciembre, y en otros es menor como en Febrero.'
        }, {
            Request: '¿Hidrandina incrementa el monto de mis recibos?',
            Response: '<b>No es correcto</b>. Hidrandina no determina el consumo que tiene el cliente, <b>eso lo hace el hábito de consumo que es registrado por</b> el medidor, el cual calcula el monto de energía consumida. Nuestros técnicos toman lectura de este, para luego <b>restarlo con la lectura tomada en el mes inmediato anterior</b> para calcular cuál ha sido el consumo del mes que puede apreciarse en tu recibo.'
        }, {
            Request: '¿Cómo es que mi consumo puede duplicarse en algunos meses?',
            Response: 'En algunos casos, los hábitos de consumo cambian radicalmente, por lo que se puede ver un aumento considerable en tu recibo. Un caso recurrente es cuando en el hogar hay un aumento del número de habitantes o visitas frecuentes, <b>artefactos en mal estado, conexiones eléctricas antiguas</b> esto puede influir mucho en los montos que facturas mes a mes, pero si en tu caso no ha sucedido algo similar, comunícate con nosotros al 0801-71001 o nuestro Whatsapp 948 327 474'
        }];

        return r;
    }

    function GetDataFraccionamiento() {
        var r = [{
            Request: '¿Por qué aparece en mi recibo una nota de débito/crédito?',
            Response: 'Durante los meses de estado de emergencia, <b>Hidrandina no pudo realizar la lectura de medidores</b>, por lo que se facturó <b>el consumo promedio del usuario</b>, más no su consumo real. Una vez levantado el estado de emergencia, Hidrandina realizó la liquidación del consumo del usuario, es decir, se sinceró el consumo verdadero con la lectura del medidor. En algunos suministros, <b>el consumo había sido menor al promedio</b> tomado en cuenta por Hidrandina, y para compensar por ese sobrecosto, <b>tienen una nota de CRÉDITO</b>, que es saldo a favor del cliente, pero en los casos <b>donde el consumo había sido mayor al promedio</b>, se observará una <b>nota de DÉBITO</b>, que es un saldo que el cliente debe cancelar.'
        }, {
            Request: '¿Cómo sé si mi bono ya se aplicó?',
            Response: 'El bono fue aplicado <b>automáticamente</b> a las deudas de todos los beneficiarios, por lo que no es necesario acercarse a las oficinas de Hidrandina para consultar sobre esto. <b>Si desea confirmar la aplicación de su bono</b>, puede hacerlo a través de la Oficina Virtual de Distriluz viendo sus recibos de meses previos.'
        }, {
            Request: '¿Por qué aún tengo deudas pendientes si mi bono se aplicó?',
            Response: 'El bono electricidad se aplicó a todos los beneficiarios con deudas pendientes, si el monto de su <b>deuda era superior a S/. 160.00</b>, entonces su deuda se redujo. Sin embargo, si la deuda era inferior a dicho monto, esta se ha cancelado y el beneficiario tiene saldo a favor para aplicarse en sus próximos recibos.'
        }, {
            Request: '¿Cómo puedo acceder a un fraccionamiento de deuda?',
            Response: 'Si el consumo de tu suministro es <b>menor a 400kW mensual</b>, entonces estás apto para aplicar al fraccionamiento de tu deuda registrada durante los meses de estado de emergencia. Si deseas fraccionar tu deuda, contáctanos al 0801-71001 o nuestro Whatsapp 948 327 474.'
        }];

        return r;
    }

    function GetDataReclamos() {
        var r = [{
            Request: '¿Por qué no obtengo respuesta a los reclamos que realizo?',
            Response: 'El centro de atención al cliente tiene un límite de tiempo para atender los reclamos, <b>Osinergmin dispuso regular un procedimiento especial aprobada mediante Resolución Osinergmin Nº 079-2020-OS/CD aplicable únicamente para los reclamos de facturación de recibos emitidos en el mes de marzo del 2020 o que comprendan algún consumo realizado durante el Estado de Emergencia Nacional, el mismo que tiene un plazo de atención de (30 días hábiles para emitir carta informativa luego de dicho plazo 30 días hábiles para resolver el reclamo)</b>,por lo cuál te solicitamos que seas paciente, pues todos los clientes son importantes para nosotros. Si tu reclamo sigue sin atenderse después de este límite de tiempo, te pedimos que nos lo hagas saber por nuestros canales para tener conocimiento de esto.'
        }, {
            Request: '¿Por qué no me están llegando mis recibos físicos?',
            Response: 'Debido a las medidas de aislamiento social para frenar <b>la propagación del Covid-19</b>, se resolvió suspender la entrega de los recibos <b>desde marzo a octubre del 2020</b>, dado que dicho reparto representaba un potencial foco de contagio para la población. Sin embargo, puede visibilizarlos en nuestra oficina virtual de Distriluz.'
        }];

        return r;
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Orientacion', 'ReciboPreguntas'), jQuery));