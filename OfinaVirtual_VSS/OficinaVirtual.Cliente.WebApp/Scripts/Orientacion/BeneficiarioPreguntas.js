﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-04-11
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
			
			$('img.logoVisa').parent().hide();
            CreateAccordion(_C.crdAccordion);
        });
    }

    //#region Events

    //#endregion Events

    function CreateAccordion(sender) {
        var d = GetData();

        sender.html('');

        for (var i = 0; i < d.length; i++) {
            d[i].Index = i + 1;

            CreateCard(d[i], sender);
        }
    }

    function CreateCard(item, parent) {
        var a = 'crdHead' + item.Index,
            b = 'crdBody' + item.Index,
            h = '<div id="{0}" class="card-header"><h5 class="mb-0"><button class="btn btn-link" data-toggle="collapse" ' +
                'data-target="#{1}" aria-expanded="true" aria-controls="{1}">{3}) {2}</button></h5></div>',
            t = '<div id="{1}" class="collapse" aria-labelledby="{0}" data-parent="#{3}"><div class="card-body">{2}</div></div>',
            r = '<div class="card">{0}{1}</div>';

        h = _U.StringFormat(h, a, b, item.Request, item.Index);
        t = _U.StringFormat(t, a, b, item.Response, parent[0].id);
        r = _U.StringFormat(r, h, t);

        parent.append(r);
    }

    function GetData() {
        var r = [{
            Request: '¿Cuál es el origen del beneficio de fraccionar el pago de los recibos?',
            Response: 'Debido a la declaración de emergencia nacional por el Covid-19, las Empresas del Grupo Distriluz (Enosa, Ensa. Hidrandina y Electrocentro) otorgarán facilidades de pago a sus usuarios en concordancia con el Decreto de Urgencia N° 035-2020 emitido por el Gobierno peruano.'
        }, {
            Request: '¿En qué consiste el fraccionamiento del recibo?',
            Response: 'En fraccionar la deuda por el servicio eléctrico hasta en 24 meses a la población considerada como vulnerable. El fraccionamiento se aplicará a los recibos que se encuentren pendientes de pago y que se hayan emitido en el mes de marzo 2020 o que comprendan algún consumo realizado durante el Estado de Emergencia nacional.'
        }, {
            Request: '¿Quiénes acceden al beneficio?',
            Response: 'Los usuarios residenciales del servicio eléctrico con un consumo de hasta 100 kWh mensuales (aprox. S/. 63 mes) y a los usuarios que están conectados a sistemas eléctricos rurales no convencionales abastecidos con paneles fotovoltaicos (energía solar).'
        }, {
            Request: '¿El fraccionamiento genera moras?',
            Response: 'El fraccionamiento de los recibos del servicio eléctrico no aplicará intereses moratorios, cargos fijos por mora, o cualquier otro concepto vinculado al incumplimiento de pago.'
        }, {
            Request: '¿Debo inscribirme o en qué momento se aplicará este fraccionamiento?',
            Response: '<p>No será necesario inscribirse, el fraccionamiento de los usuarios residenciales calificados como vulnerables se realizará en forma automática dentro de los treinta (30) días hábiles posteriores de concluido el Estado de Emergencia Nacional dividiendo el monto pendiente de pago en 24 cuotas iguales. Hasta que ello se realice, los recibos se facturarán normalmente y podrán pagarse, pero no están sujetos a corte de suministro por deuda. El usuario podría solicitar un menor plazo de financiamiento si así lo requiere.</p>Conoce si eres beneficiario accediendo a nuestra página Web <a href="https://www.distriluz.com.pe/">https://www.distriluz.com.pe/</a>, a nuestra APP DISTRILUZ, o en el siguiente enlace: <a href="https://servicios.distriluz.com.pe/Fraccionamiento.html">https://servicios.distriluz.com.pe/Fraccionamiento.html</a>'
        }, {
            Request: '¿A cuántos usuarios y personas beneficia las medidas implementadas por las empresas del Grupo Distriluz?',
            Response: 'A nivel de las 4 empresa del Grupo Distriluz (Enosa, Ensa, Hidrandina y Electrocentro) este fraccionamiento de los recibos de electricidad beneficia directamente a 1 millón 900 mil clientes aproximadamente, lo que representa 9 millones y medio de personas beneficiadas.'
        }, {
            Request: '¿Por qué no se están repartiendo recibos actualmente?',
            Response: 'El Decreto de Urgencia N° 035-2020, suspendió la lectura de consumos, y la entrega física de los recibos o facturas de los servicios públicos de electricidad, gas natural y telecomunicaciones. Asimismo, se dispone que se facture con promedios utilizando los últimos seis meses.'
        }, {
            Request: '¿Por qué no funcionan las oficinas de atención al cliente?',
            Response: '<p>A fin de salvaguardar la salud de las personas, el Decreto de Urgencia N° 035-2020, suspendió la obligación de atender físicamente a los usuarios finales a través de los Centros de Atención.</p> <p>Los usuarios pueden utilizar los medios virtuales y telefónicos para realizar sus transacciones o consultas, poniendo a disposición la Web <a href="https://www.distriluz.com.pe/">https://www.distriluz.com.pe/</a>, nuestra APP DISTRILUZ, y el Contac Center:</p><p>Enosa 073 284050 (Piura) / 072 522493 (Tumbes) </p><p>Ensa 074 481200</p><p>Hidrandina 0801 71001 </p>Electrocentro 0801 71002 / 064 481313'
        }, {
            Request: '¿Por qué es importante que quienes puedan pagar sus recibos lo hagan?',
            Response: 'Sí, es muy importante. Las empresas eléctricas venimos manteniendo la operatividad del servicio eléctrico especialmente en las instalaciones de salud, fuerzas armadas, las relacionadas a la cadena de abastecimiento y venta de alimentos, las viviendas y las vías públicas. La empresa no viene realizando cortes de servicio por falta de pago en este periodo de emergencia. No obstante, se invoca a los usuarios no vulnerables a utilizar los medios físicos o virtuales para realizar en forma responsable el pago del servicio de manera que se mantenga la cadena de pagos y el suministro de electricidad para todo el país.'
        }, {
            Request: '¿Qué pasa si no soy un usuario vulnerable?',
            Response: '<p>Si no está calificado como un  usuario vulnerable, puede verificar el monto y realizar el pago de sus recibos a través de los bancos, agentes autorizados o utilizando la Oficina Virtual <a href="https://servicios.distriluz.com.pe/OficinaVirtual">https://servicios.distriluz.com.pe/OficinaVirtual</a> o el App Distriluz. </p><p>Las empresas darán las facilidades para que los usuarios no vulnerables, que por temas económicos no puedan pagar su recibo, accedan a nuestros mecanismos de financiamiento de recibos. Esto se realizará una vez concluido el Estado de Emergencia Nacional por el Covid-19.</p><p>Pueden realizar sus consultas en los contac center de las empresas:</p><p>Enosa 073 284050 (Piura) / 072 522493 (Tumbes) </p><p>Ensa 074 481200</p><p>Hidrandina 0801 71001 </p>Electrocentro 0801 71002 / 064 481313'
        }];

        return r;
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'BeneficiarioPreguntas'), jQuery));