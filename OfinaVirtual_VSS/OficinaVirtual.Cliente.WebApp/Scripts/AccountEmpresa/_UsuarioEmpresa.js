﻿/// <reference path="../jquery-1.10.2.min.js" />
$(document).ready(inicialize);
function inicialize() {
    $(".frmRegistro").submit(function (event) {
        event.preventDefault();
        click_btnRegistrar($(this));
    });
    $("#empresaRuc").keyup(function (event) {
        event.preventDefault();
        if ((event.keyCode || event.which) == 13) {
            empresaRuc_text();
        }
    });
    setTimeout(function () { $('#nombres').focus(); }, 500);



    $("#buscaempresaxruc").click(function (event) {
        event.preventDefault();
    
            empresaRuc_text();
      
    });


}

function Validate_Datos() {
    var correo = $('#correo'),
        txtConfirmaEmail = $('#txtConfirmaEmail'),
        clave = $('#clave'),
        confirmacionclave = $('#confirmacionclave'),
        chkTerminos = $('#chkTerminos');

    txtConfirmaEmail.next().html('');
    confirmacionclave.next().html('');
    chkTerminos.next().next().html('');

    if (correo.val() != txtConfirmaEmail.val()) {
        txtConfirmaEmail.next().html('Las direcciones de correo electrónico no coiciden.');
        return false;
    }

    if (clave.val() != confirmacionclave.val()) {
        confirmacionclave.next().html('Las contraseñas no coinciden');
        return;
    }

    if (!chkTerminos.prop('checked')) {
        chkTerminos.next().next().html('Debe de aceptar los términos y condiciones.');
        return false;
    }

    return true;
}

function click_btnRegistrar(oForm) {
    if(!Validate_Datos()) return;
    jQuery.ajax({
        type: oForm[0].method,
        url: oForm[0].action,
        data: $(oForm[0]).serialize(),
        dataType: 'HTML',
        success: function (oDatos) {
            jQuery('.modLogin').find('.modal-body').html(oDatos);
        }
    });
}

function empresaRuc_text() {
    var obj = new Object();
    obj.idtipoidentidad = "3";
    obj.nroidentidad = $("#empresaRuc").val();
    jQuery.ajax({
        type: 'Post',
        url: '/AccountEmpresa/BuscarPorIdentidad',
        data: obj,
        dataType: 'Json',
        beforeSend: function () {

            $('#modPreload').modal('show');

        },




        success: function (oDatos) {


            if (oDatos.length === 0) {

                    $("#razonsocial").val('Empresa no encontrada');
            }


                else {

                    for (var x in oDatos) {
                            var NombreCliente = oDatos[x].NombreCliente;
                              $("#razonsocial").val(NombreCliente);
                        }

                     }

            $('#modPreload').modal('hide');

        }
    });
}