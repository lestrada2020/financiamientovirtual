﻿/// <reference path="../jquery-1.10.2.min.js" />
$(document).ready(inicialize);

function inicialize() {
    /* TabCiente */ TabCiente();
}

function TabCiente() {
    $("#myTabLogin li").click(function () {
        if ($("a", $(this)).attr("id") == "natural-tab") {
            location.href = "../../Account/Login";
        } else if ($("a", $(this)).attr("id") == "empresa-tab") {
            location.href = "../../AccountEmpresa/Login";
        }
    });
}

function NuevoRegistro(event, obj) {
    event.preventDefault();

    var a = $('#modLogin');
    a.find('div.modal-dialog').removeClass('modal-md').addClass('modal-lg');

    jQuery.ajax({
        type: "GET",
        async: true,
        url: obj.href,
        success: function (oDatos) {
            a.find('.modal-title').html("NUEVO REGISTRO");
            a.find('.modal-body').html(oDatos);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');
        }
    });
}

function CambiarContraseña(event, obj) {
    event.preventDefault();

    var a = $('#modLogin');
    a.find('div.modal-dialog').removeClass('modal-lg').addClass('modal-md');

    jQuery.ajax({
        type: "GET",
        async: true,
        url: obj.href,
        success: function (oDatos) {
            a.find('.modal-title').html("RECUPERAR CLAVE");
            a.find('.modal-body').html(oDatos);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');
        }
    });
}

function Imprimir(event, url) {
    event.preventDefault();

    jQuery.ajax({
        type: "GET",
        async: true,
        url: url,
        success: function (oDatos) {
            alert("Imprimió");
        }
    });
}