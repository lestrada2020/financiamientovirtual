﻿$(document).ready(function () {
    /* Process */ Design();
    /* Process */ Process();
    function Design() { }
    function Process() {
        /* Validar */ Validar();
        /* Evento */ Evento();
        /* Action */ Action();
        function Validar() { }
        function Evento() {
            /* tabUsuEmp_DataTable */ tabUsuEmp_DataTable();
            function tabUsuEmp_DataTable() {
                $('#tabUsuEmp').DataTable({
                    searching: false,
                    columns: [
                        { "orderable": true, "className":'text-center' },
                        { "orderable": true, "className": 'text-center' },
                        { "orderable": true, "className": 'text-center' },
                        { "orderable": true, "className": 'text-center' },
                        { "orderable": true, "className": 'text-center' },
                        { "orderable": true, "className": 'text-center' },
                        { "orderable": false, "className": 'text-center' },
                        { "orderable": false, "className": 'text-center' }
                    ],
                    pageLength: 10,
                    lengthChange: false,
                    deferRender: true,
                    scrollX: 200,
                    scrollCollapse: true,
                    scroller: false,
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            }
        }
        function Action() {
            /* aEli_Click */ aEli_Click();
            function aEli_Click() {
                $("body" ).off('click', '.aEli_');
                $("body").on('click', '.aEli_', function () {
                    console.log("1231");
                    var aEli_ = $(this).attr("id");
                    var aEli_Split = aEli_.split("_");
                    var question = '¿Deseas eliminar este registro?';
                    var callback = function () {
                        var obj = new Object();
                        obj.IdUsuario = aEli_Split[1].toString();
                        jQuery.ajax({
                            type: 'Post',
                            url: '/AccountEmpresa/EliminarClienteEmpresa',
                            data: obj,
                            dataType: 'Json',
                            success: function (oDatos) {
                                var errorPrin = oDatos.errorPrin;
                                var error = oDatos.error;
                                var mensaje = oDatos.mensaje;
                                var salida = oDatos.salida;
                                if (errorPrin == "0") {
                                    if (error == "1") {
                                        window.location = "../../AccountEmpresa/RegistrarUsuarios?exito=" + salida + "&&mensaje=" + mensaje + "";
                                    } else {
                                        window.location = "../../AccountEmpresa/RegistrarUsuarios?exito=" + salida + "&&mensaje=" + mensaje + "";
                                    }
                                }
                                else {
                                    window.location = "../../AccountEmpresa/RegistrarUsuarios?exito=" + salida + "&&mensaje=" + mensaje + "";
                                }
                            }
                        });
                    };
                    confirm(question, "", "", callback);
                });
            }
  
            $("#btnLimpiar").click(function (event) {
 
                var nombre = $('#nombre');
                var correo = $('#correo');
                var usuario = $('#usuario');
                var estado = $('#estado');

                nombre[0].defaultValue = "";
                correo[0].defaultValue = "";
                usuario[0].defaultValue = "";
                estado[0].selectedIndex = -1;

                estado[0].innerHTML = "<option value='' selected='selected' >Seleccionar</option> <option value='Deshabilitado'>Deshabilitado</option> <option value='Activo'>Activo</option>";

                

               

            });

        }
    }
});