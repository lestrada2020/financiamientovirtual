﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Modificación    : 2019-05-01
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
            _O.Id = _C.txtValues.data('id');
            _O.UrlLogin = _C.txtValues.data('login');

            _C.txtValues.remove();
            _C.btnSave.on('click', btnSave_click);
        });
    }

    //#region Events

    //#endregion Events

    function GetEntidad() {
        var r = {
            Guid: _O.Id,
            Password: _C.txtPassword.val()
        };

        return r;
    }

    function Valid(data) {
        var d = data,
            r = true,
            p = _C.txtPassword,
            q = _C.txtPasswordRe;

        p.next().hide();
        q.next().hide();

        r &= _C.txtPassword[0].checkValidity();

        if (p.val().length == 0) {
            r = false;
            p.next().html('La contraseña es obligatoria.').show();
        }
        else if (p.val().length < 4) {
            r = false;
            p.next().html('La contraseña debe de tener minimo 4 caracteres').show();
        }

        if (p.val() != q.val()) {
            r = false;
            q.next().html('Las contraseñas no coinciden.').show();
        }

        if (!r) _C.divStep01.addClass('was-validated');

        return r;
    }

    function btnSave_click() {
        var e = GetEntidad();

        if (!Valid(e)) return;

        _C.btnSave.prop('disabled', true).removeClass('btn-green');
        $('#modPreload').modal('show');

        _U.Ajax('Account', 'CambiarPassword', _U.AddToFormData({ Parametros: e }), function (data) {
            var d = data.Datos;

            if (+d.IdError > 0) {
                _U.AlertError(d.Mensaje);
            }

            _U.AlertSuccess('Se cambio correctamente la constraseña.');

            setTimeout(function () {
                window.location.href = _O.UrlLogin;
            }, 2000);
        }, function (data) {
            $('#modPreload').modal('hide');
            _C.btnSave.prop('disabled', false).addClass('btn-green');
        });
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Account', 'RestablecerPassword'), jQuery));