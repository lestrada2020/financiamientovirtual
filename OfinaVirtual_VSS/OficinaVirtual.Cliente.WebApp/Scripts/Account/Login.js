﻿/// <reference path="../jquery-1.10.2.min.js" />

$(document).ready(inicialize);

function inicialize() {
    TabCiente();

    //$('#lnkVersionWeb').on('click', lnkVersionWeb_click);
    //$('#btnOptions').on('click', btnOptions_click);
}

function TabCiente() {
    console.log("entro 1");

    $("#myTabLogin li").click(function () {
        if ($("a", $(this)).attr("id") == "natural-tab") {
            console.log("entro 2");

            location.href = "../../Account/Login";
        } else if ($("a", $(this)).attr("id") == "empresa-tab") {

            console.log("entro 3");

            location.href = "../../AccountEmpresa/Login";
        }
    });
}

//$(".formSignin").submit(function (event) {    
//    //event.preventDefault();
//    click_btnIngresar($(this));
//});

function click_btnIngresar(oForm) {
    $(".formSignin").prop('disabled', true);

    jQuery.ajax({
        type: oForm[0].method,
        url: oForm[0].action,
        data: $(oForm[0]).serialize(),
        dataType: 'HTML',
        success: function (oDatos) {
        }
    });
}

function NuevoRegistro(event, obj) {
    event.preventDefault();

    var a = $('#modLogin');
    a.find('div.modal-dialog').removeClass('modal-md').addClass('modal-lg');

    jQuery.ajax({
        type: "GET",
        async: true,
        url: obj.href,
        success: function (oDatos) {
            a.find('.modal-title').html("NUEVO REGISTRO");
            a.find('.modal-body').html(oDatos);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');
        }
    });
}

function CambiarContraseña(event, obj) {
    event.preventDefault();

    var a = $('#modLogin');
    a.find('div.modal-dialog').removeClass('modal-lg').addClass('modal-md');

    jQuery.ajax({
        type: "GET",
        async: true,
        url: obj.href,
        success: function (oDatos) {
            a.find('.modal-title').html("RECUPERAR CLAVE");
            a.find('.modal-body').html(oDatos);
            a.modal({ backdrop: 'static', keyboard: true });
            a.modal('show');
        }
    });
}

function Imprimir(event, url) {
    event.preventDefault();

    jQuery.ajax({
        type: "GET",
        async: true,
        url: url,
        success: function (oDatos) {
            alert("Imprimió");
        }
    });
}

function lnkVersionWeb_click() {
    $('body').find('.downapp').css('display', 'none');
    $('body').find('.login').css('display', 'block');
    $('body').find('.options').removeClass('d-none');
}

function btnOptions_click() {
    var a = $('body').find('.navLateral');

    a.css('display', a.css('display') == 'none' ? 'block' : 'none');
}