﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Modificación    : 2020-04-21
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _C.txtDNI.focus();
            _C.txtDNI.on('keydown', txtDNI_keydown);
            _C.txtEmail.on('keydown', txtEmail_keydown);
            _C.txtPhone.on('keydown', txtPhone_keydown);
            _C.btnSiguiente.on('click', btnSiguiente_click);
            _C.btnAtras.on('click', btnAtras_click);
            _C.btnSave.on('click', btnSave_click);
            _C.rdConfirmEmail.on('change', rdConfirmEmail_change);
            _C.rdConfirmPhone.on('change', rdConfirmPhone_change);
        });
    }

    //#region Events

    function btnAtras_click(e) {
        ClearQuery();
        _C.divStep01.removeClass('d-none');
        _C.divStep02.addClass('d-none');
        _C.txtEmail.val('');
        _C.txtPhone.val('');
        _C.txtDNI.focus();
    }

    function btnSiguiente_click() {
        ClearQuery();
        Loading(_C.btnSiguiente, 0);

        if (!ValidUser()) {
            Loading(_C.btnSiguiente, 1);
            return;
        }

        var p = {
            Parametros: {
                Usuario: _C.txtDNI.val()
            }
        };

        _U.Ajax('Account', 'ObtenerDatosConfirmacion', _U.AddToFormData(p), function (data) {
            if (data.IdError == 0) {
                var d = data.Datos;

                if (!+d.IsEMail && !+d.IsPhone) {
                    _U.AlertError('Su cuenta aún no se encuentra activa.');
                    return;
                }

                _C.txtDNI.data('dataUser', d);
                _C.divStep01.addClass('d-none');
                _C.divStep02.removeClass('d-none');
                _C.divTypeConfirmStep02.addClass('d-none');
                _C.hTitleStep02.html(_U.StringFormat('Usuario: <b>{0}</b>, favor de confirmar su <span>Correo electrónico: <b>{1}</b></span>', p.Parametros.Usuario, d.EMail));

                if (+d.IsEMail && +d.IsPhone) {
                    _C.rdConfirmEmail.click();
                    _C.divTypeConfirmStep02.removeClass('d-none');
                }
                else if (+d.IsEMail) _C.rdConfirmEmail.click();
                else if (+d.IsPhone) _C.rdConfirmPhone.click();

                _C.txtEmail.focus();
            }
            else _U.AlertError(data.Mensaje);
        }, function () { Loading(_C.btnSiguiente, 1); });
    }

    function btnSave_click() {
        ClearQuery();
        Loading(_C.btnSave, 0);

        if (!ValidConfirm()) {
            Loading(_C.btnSave, 1);
            return;
        }

        var p = {
            Parametros: GetEntidadConfirm()
        };

        _U.Ajax('Account', 'SolicitarNewPassword', _U.AddToFormData(p), function (data) {
            if (data.IdError == 0) {
                var d = data.Datos;

                _U.AlertSuccess(d.Message);
                _C.btnSave.remove();

                setTimeout(function () { _C.txtDNI.closest('.modal-content').find('> .modal-header button').click(); }, 3000);
            }
            else _U.AlertError(data.Mensaje);
        }, function () { Loading(_C.btnSave, 1); });
    }

    function txtDNI_keydown(e) {
        ClearQuery();
        if (e.which == 13) btnSiguiente_click();
    }

    function txtEmail_keydown(e) {
        ClearQuery();
        if (e.which == 13) btnSave_click();
    }

    function txtPhone_keydown(e) {
        ClearQuery();
        if (e.which == 13) btnSave_click();
    }

    function rdConfirmEmail_change(e) {
        var a = _C.rdConfirmEmail.prop('checked'),
            d = _C.txtDNI.data('dataUser');

        if (a) {
            _C.divConfirmEmailStep02.removeClass('d-none');
            _C.divConfirmPhoneStep02.addClass('d-none');
            _C.hTitleStep02.find('span').html(_U.StringFormat('Correo electrónico: <b>{0}</b>', d.EMail));
        }
    }

    function rdConfirmPhone_change(e) {
        var a = _C.rdConfirmPhone.prop('checked'),
            d = _C.txtDNI.data('dataUser');

        if (a) {
            _C.divConfirmEmailStep02.addClass('d-none');
            _C.divConfirmPhoneStep02.removeClass('d-none');
            _C.hTitleStep02.find('span').html(_U.StringFormat('Nro. Celular: <b>{0}</b>', d.Phone));
        }
    }

    //#endregion Events

    function Loading(sender, isShowPage) {
        if (+isShowPage == 1) {
            sender.prop('disabled', false).addClass('btn-success');
            $('#modPreload').modal('hide');
        } else {
            sender.prop('disabled', true).removeClass('btn-success');
            $('#modPreload').modal('show');
        }
    }

    function ClearQuery() {
        _C.divAlert.removeAttr('class').html('');
    }

    function GetEntidadConfirm() {
        var e = {
            Usuario: _C.txtDNI.val(),
            IdType: _C.rdConfirmEmail.prop('checked') ? 1 : 2,
            Value: _C.txtEmail.val()
        };

        if (e.IdType == 2) e.Value = _C.txtPhone.val()

        return e;
    }

    function ValidUser() {
        _C.txtDNI.next().hide();

        var r = true;
        r &= _C.txtDNI[0].checkValidity();

        if (_C.txtDNI.val().length < 8) {
            r = false;
            _C.txtDNI.next().html('Nro. documento incompleto').show();
        }

        if (isNaN(_C.txtDNI.val())) {
            r = false;
            _C.txtDNI.next().html('Nro. documento no es valido, ingrese solo números').show();
        }

        if (!r) _C.divStep01.addClass('was-validated');

        return r;
    }

    function ValidConfirm() {
        _C.txtPhone.next().hide();

        var r = true,
            a = _C.rdConfirmEmail.prop('checked');

        if (a) {
            if (_C.txtEmail.val().length > 0) {
                if (!_C.txtEmail[0].checkValidity()) {
                    r = false;
                    _C.txtEmail.next().html(_C.txtEmail[0].validationMessage).show();
                }
            }
        }
        else {
            if (_C.txtPhone.val().length < 9) {
                r = false;
                _C.txtPhone.next().show();
            }

            if (isNaN(_C.txtPhone.val())) {
                r = false;
                _C.txtPhone.next().html('Celular no es valido, ingrese solo números').show();
            }
        }

        if (!r) _C.divStep01.addClass('was-validated');

        return r;
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Account', 'RecuperarPassword'), jQuery));