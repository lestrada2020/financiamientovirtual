﻿$(document).ready(inicialize);

function inicialize() {

    $(".oFrm ").submit(function (event) {
        //event.preventDefault();

        var sClaveGuardada = $("#claveGuardada").val();
        var sClaveAntigua = $("#claveAntigua").val();
        var sClave = $("#clave").val();
        var sConfirmacionClave = $("#confirmacionClave").val();

        if (sClaveAntigua != '' || sClave != '' || sConfirmacionClave != '') {

            if (sClaveAntigua.length > 12) {
                $(".ValClave").html("La clave debe tener un máximo de 12 caracteres");
                return false;
            }
            else if (sClave == '' || sConfirmacionClave == '') {
                $(".ValClave").html("Debe ingresar todos los campos de esta sección");
                return false;
            }
            else if (sClaveGuardada != sClaveAntigua) {
                $(".ValClave").html("La clave ingresada no coinciden con la actual");
                return false;
            }
            //else if (sClave != sConfirmacionClave) {
            //    $(".ValConfirmacionClave").html("Debe ingresar todos los campos de esta sección");
            //    return false;
            //}
        }
    });

}