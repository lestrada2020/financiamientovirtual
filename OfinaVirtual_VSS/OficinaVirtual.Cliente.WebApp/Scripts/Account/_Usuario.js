﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-03-20
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _C.btnSiguiente.on('click', btnSiguiente_click);
            _C.btnSiguiente011.on('click', btnSiguiente011_click);
            _C.btnAnterior.on('click', btnAnterior_click);
            _C.btnAnterior011.on('click', btnAnterior011_click);
            _C.cmbDepNac.on('change', cmbDepNac_change);
            _C.cmbProNac.on('change', cmbProNac_change);
            _C.chkTerminos.on('change', chkTerminos_change);
            $('.frmRegistro').on('submit', frmRegistro_submit);

            var vr = ValuesReload();
            SettingsDatepicker();
            FillDepNac();

            if (vr == 0) setTimeout(function () { _C.documentoIdentidad.focus(); }, 300);

            //$('#fechaNacimiento').val('01/01/3000');
            $('#telefono').val('00000');
            $('#NroSuministro').val('0');
        });
    }

    //#region Events

    function cmbDepNac_change() {
        FillProNac();
    }

    function cmbProNac_change() {
        FillDisNac();
    }

    function btnSiguiente_click() {
        if (!ValidStep01()) return;

        _C.divStep01.addClass('d-none');
        _C.divStep011.removeClass('d-none');

        _C.nombres.focus();
    }

    function btnSiguiente011_click() {
        if (!ValidStep011()) return;

        _C.divStep011.addClass('d-none');
        _C.divStep02.removeClass('d-none');

        _C.correo.focus();
    }

    function Grabar(form) {
        if (!ValidStep02()) return;

        _C.btnRegistrar.prop('disabled', true);

        var p = {
            txtConfirmaEmail: _C.txtConfirmaEmail.val(),
            txtConfirmacelular: _C.txtConfirmacelular.val(),
            chkTerminos: _C.chkTerminos.val(),
            IdDepartamentoNac: _C.cmbDepNac.val(),
            IdProvinciaNac: _C.cmbProNac.val(),
            IdDistritoNac: _C.cmbDisNac.val()
        };

        _C.ExtraParametros.val(JSON.stringify(p));

        $.ajax({
            type: form[0].method,
            url: form[0].action,
            data: $(form[0]).serialize(),
            dataType: 'HTML',
            success: function (datos) {
                $('.modLogin').find('.modal-body').html(datos);
            }
        });
    }

    function btnAnterior011_click() {
        _C.divStep01.removeClass('d-none');
        _C.divStep011.addClass('d-none');

        _C.documentoIdentidad.focus();
    }

    function btnAnterior_click() {
        _C.divStep011.removeClass('d-none');
        _C.divStep02.addClass('d-none');

        _C.documentoIdentidad.focus();
    }

    function chkTerminos_change() {
    }

    function frmRegistro_submit(e) {
        e.preventDefault();
        Grabar($(this));
    }

    //#endregion Events

    function ValuesReload() {
        var r = 0,
            d = {},
            v = _C.ExtraParametros.val();

        if (v == '') return 0;

        d = JSON.parse(v);

        _C.txtConfirmaEmail.val(d.txtConfirmaEmail);
        _C.txtConfirmacelular.val(d.txtConfirmacelular);
        _C.chkTerminos.prop('checked', d.chkTerminos);
        _C.cmbDepNac.data('value', d.IdDepartamentoNac);
        _C.cmbProNac.data('value', d.IdProvinciaNac);
        _C.cmbDisNac.data('value', d.IdDistritoNac);

        return r;
    }

    function ValidStep01() {
        var r = true;

        _C.documentoIdentidad.next().html('');
        _C.clave.next().html('');
        _C.confirmacionclave.next().html('');

        if (_C.documentoIdentidad.val() == '') {
            _C.documentoIdentidad.next().html('El documento de identidad es requerido.');
            if (r) _C.documentoIdentidad.focus();
            r = false;
        }

        if (_C.clave.val() == '') {
            _C.clave.next().html('La Contraseña es requerida.');
            if (r) _C.clave.focus();
            r = false;
        } else if (_C.clave.val().length < 4) {
            _C.clave.next().html('La contraseña debe de tener mínimo 4 caracteres.');
            if (r) _C.clave.focus();
            r = false;
        } else if (_C.clave.val() != _C.confirmacionclave.val()) {
            _C.confirmacionclave.next().html('Las contraseñas no coinciden.');
            if (r) _C.clave.focus();
            r = false;
        }

        return r;
    }

    function ValidStep011() {
        var r = true;

        _C.nombres.next().html('');
        _C.aPaterno.next().html('');
        _C.aMaterno.next().html('');
        _C.fechaNacimiento.next().html('');
        _C.cmbDepNac.next().html('');
        _C.cmbProNac.next().html('');
        _C.cmbDisNac.next().html('');

        if (_C.nombres.val() == '') {
            _C.nombres.next().html('El nombre es requerido.');
            if (r) _C.nombres.focus();
            r = false;
        }

        if (_C.aPaterno.val() == '') {
            _C.aPaterno.next().html('El apellido paterno es requerido.');
            if (r) _C.aPaterno.focus();
            r = false;
        }

        if (_C.aMaterno.val() == '') {
            _C.aMaterno.next().html('El apellido materno es requerido.');
            if (r) _C.aMaterno.focus();
            r = false;
        }

        if (_C.fechaNacimiento.val() == '') {
            _C.fechaNacimiento.next().html('La fecha de nacimiento es requerido.');
            if (r) _C.fechaNacimiento.focus();
            r = false;
        }

        if (+_C.cmbDepNac.val() == 0) {
            _C.cmbDepNac.next().html('El departamento de nacimiento es requerido.');
            if (r) _C.cmbDepNac.focus();
            r = false;
        }

        if (+_C.cmbProNac.val() == 0) {
            _C.cmbProNac.next().html('La provincia de nacimiento es requerido.');
            if (r) _C.cmbProNac.focus();
            r = false;
        }

        if (+_C.cmbDisNac.val() == 0) {
            _C.cmbDisNac.next().html('El distrito de nacimiento es requerido.');
            if (r) _C.cmbDisNac.focus();
            r = false;
        }

        return r;
    }

    function ValidStep02() {
        var r = true;

        _C.correo.next().html('');
        _C.txtConfirmaEmail.next().html('');
        _C.chkTerminos.next().next().html('');
        _C.txtConfirmacelular.next().html('');

        if (_C.correo.val() == '') {
            _C.correo.next().html('El correo es requerido.');
            if (r) _C.correo.focus();
            r = false;
        } else if (_C.correo.val() != _C.txtConfirmaEmail.val()) {
            _C.txtConfirmaEmail.next().html('Las direcciones de correo electrónico no coinciden.');
            if (r) _C.correo.focus();
            r = false;
        }

        if (_C.celular.val() != _C.txtConfirmacelular.val()) {
            _C.txtConfirmacelular.next().html('Los números de celular no coinciden.');
            if (r) _C.celular.focus();
            r = false;
        }

        if (!_C.chkTerminos.prop('checked')) {
            _C.chkTerminos.next().next().html('Debe de aceptar los términos y condiciones.');
            if (r) _C.chkTerminos.focus();
            r = false;
        }

        return r;
    }

    function FillDepNac() {
        var p = {
            Entity: 'Departamento',
            AddPreFix: 0
        };

        _U.FillComboBox(_C.cmbDepNac, p);
    }

    function FillProNac() {
        var p = {
            Entity: 'Provincia',
            AddPreFix: 0,
            IdDepartamento: +_C.cmbDepNac.val()
        };

        _U.FillComboBox(_C.cmbProNac, p);
    }

    function FillDisNac() {
        var p = {
            Entity: 'Distrito',
            AddPreFix: 0,
            IdProvincia: +_C.cmbProNac.val()
        };

        _U.FillComboBox(_C.cmbDisNac, p);
    }

    function SettingsDatepicker() {
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"],
            daysShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Limpiar"
        };

        var now = new Date();

        _C.fechaNacimiento.datepicker({
            language: 'es',
            autoclose: true,
            format: 'dd/mm/yyyy',
            endDate: new Date(now.getFullYear() - 17, now.getMonth(), now.getDate()),
            startDate: new Date(now.getFullYear() - 100, now.getMonth(), now.getDate()),
            todayHighlight: true
        });
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Account', '_Usuario'), jQuery));