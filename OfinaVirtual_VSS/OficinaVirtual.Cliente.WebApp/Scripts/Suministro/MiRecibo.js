﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-04-22
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal'),
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        _U.CaptchaReady();
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
            _O.IsMobil = window.matchMedia('(max-width: 767px)').matches;

            $('img.logoVisa').parent().hide();
            $('[data-toggle="tooltip"]').tooltip();

            _C.txtIdNroServicio.on('keydown', txtIdNroServicio_keydown);
            _C.cmbType.on('change', cmbType_change);
            _C.btnSearch.on('click', btnSearch_click);
            _C.btnSave.on('click', btnSave_click);
            _C.btnCancel.on('click', btnCancel_click);
            _C.lstSumin.on('click', 'a', lstSuminItem_click);
            _C.txtIdNroServicio.focus();
        });
    }

    //#region Events

    function btnSearch_click() {
        _L.Loading(_C.btnSearch, 0);

        var a = _C.cmbType.val();

        if (!Valid()) {
            _L.Loading(_C.btnSearch, 1);
            return;
        }

        if (+a == 1) LoadRecibo(_C.txtIdNroServicio.val());
        else if (+a == 2) LoadListSumin(_C.txtIdNroServicio.val());
    }

    function btnSave_click() {
        _L.Loading(_C.btnSave, 0);

        if (!ValidSave()) {
            _L.Loading(_C.btnSave, 1);
            return;
        }

        var p = GetEntidadBen();

        _U.Ajax('Suministro', 'RegistrarBeneficiario', _U.AddToFormData({ Parametros: p }), function (data) {
        }, function () { });

        DownloadRecibo(_C.btnSave);
    }

    function btnCancel_click() {
        _C.frmQuery.removeClass('d-none');
        _C.frmUpdate.addClass('d-none');
        _C.txtIdNroServicio.removeData('datasum');
        _C.txtIdNroServicio.val('');
        _C.txtPhone.val('');
        _C.txtEmail.val('');

        setTimeout(function () { _C.txtIdNroServicio.focus(); }, 300);
    }

    function txtIdNroServicio_keydown(e) {
        ClearQuery();
        if (e.which == 13) btnSearch_click();
    }

    function cmbType_change(e) {
        ClearQuery();
        var a = _C.cmbType.val(),
            b = _C.cmbType.find('> option:selected').html();

        _C.txtIdNroServicio.val('').focus();
        _C.txtIdNroServicio.attr('placeholder', _U.StringFormat('Ingrese {0}', b));
        _C.txtIdNroServicio.next().hide().html(_U.StringFormat('El {0} es obligatorio.', b));
    }

    function lstSuminItem_click(e) {
        LoadRecibo($(this).html());
        _C.modList.modal('hide');
    }

    //#endregion Events

    function ClearQuery() {
        _C.divAlert.removeAttr('class')
        _C.frmUpdate.addClass('d-none');
    }

    function Valid() {
        var a = _C.cmbType.val(),
            b = _C.cmbType.find('> option:selected').html(),
            v = _C.txtIdNroServicio.val();

        _C.txtIdNroServicio.next().hide();

        var r = true;
        r &= _C.txtIdNroServicio[0].checkValidity();

        if (v.length != 8) {
            if (a == 2) {
                r = false;
                _C.txtIdNroServicio.next().html('DNI incorrecto, debe tener 8 dígitos').show();
            } else if (v.length < 6) {
                r = false;
                _C.txtIdNroServicio.next().html(_U.StringFormat('{0} incorrecto, debe tener mas de 5 dígitos', b)).show();
            }
        }

        if (isNaN(v)) {
            r = false;
            _C.txtIdNroServicio.next().html(_U.StringFormat('{0} no es valido, ingrese solo números', b)).show();
        }

        if (!r) _C.frmQuery.addClass('was-validated');

        return r;
    }

    function ValidSave() {
        _C.txtEmail.next().hide();
        _C.txtPhone.next().hide();

        var r = true;
        r &= _C.txtPhone[0].checkValidity();
        r &= _C.txtEmail[0].checkValidity();

        if (!_C.txtPhone[0].checkValidity()) {
            r = false;
            _C.txtPhone.next().html(_C.txtPhone[0].validationMessage).show();
        }

        if (!_C.txtEmail[0].checkValidity()) {
            r = false;
            _C.txtEmail.next().html(_C.txtEmail[0].validationMessage).show();
        }

        if (_C.txtPhone.val().length == 0 && _C.txtEmail.val().length == 0) {
            r = false;
            _C.txtPhone.next().html('Ingrese celular').show();
        }

        if (!r) _C.frmUpdate.addClass('was-validated');

        return r;
    }

    function LoadRecibo(d) {
        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdNroServicio: d
        };

        _U.Ajax('Suministro', 'DatosNroServicio', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);
            _C.txtIdNroServicio.data('datasum', d);

            if (+d.HasReciboOV == 1) {
                DownloadRecibo(_C.btnSearch);
            } else {
                _C.frmQuery.addClass('d-none');
                _C.frmUpdate.removeClass('d-none');
                _C.frmUpdate.find('>p:first-child span').html(d.IdNroServicio);
                _L.Loading(_C.btnSearch, 1);
                setTimeout(function () { _C.txtPhone.focus(); }, 100);
            }
        }, function (data) {
            if (data.IdError) _L.Loading(_C.btnSearch, 1);
        });
    }

    function LoadListSumin(d) {
        _C.lstSumin.html('');

        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdTipoIdentidad: 1,
            NroIdentidad: d
        };

        _U.Ajax('Suministro', 'ListarNroServicio', _U.AddToFormData({ Parametros: p }), function (data) {
            if (data.IdError == 0) {
                var d = JSON.parse(data.Datos);

                if (d.length == 0) _U.AlertError('DNI no tine suministros asociados.');
                else if (d.length == 1) LoadRecibo(d[0].IdNroServicio);
                else {
                    $.each(d, function (i, v) {
                        var a = '<li><a href="javascript:void(0)">{0}</a></li>';
                        _C.lstSumin.append(_U.StringFormat(a, v.IdNroServicio));
                    });

                    _C.modList.modal('show');
                }
            }
            else _U.AlertError(data.Mensaje);
        }, function () { _L.Loading(_C.btnSearch, 1); });
    }

    function GetEntidadRec() {
        var d = _C.txtIdNroServicio.data('datasum'),
            a = {
                IdNroServicio: d.IdNroServicio,
                IdPeriodo: d.IdPeriodoLastFac,
                IdTipoDocumento: 1, //(1)PDF,(2)PNG
                IsShow: 1, //(0)Descargar, (1)Ver
                PathRecibo: d.PathRecibo
            };

        return a;
    }

    function GetEntidadBen() {
        var d = _C.txtIdNroServicio.data('datasum'),
            a = {
                TokenCaptcha: _U.CaptchaToken(),
                DNI: '',
                Nombres: 'Recibo',
                ApePaterno: '',
                ApeMaterno: '',
                NroCeular: _C.txtPhone.val(),
                Correo: _C.txtEmail.val(),
                EsTitular: false,
                IdNroServicio: d.IdNroServicio,
                PFactura: +d.IdPeriodoLastFac,
                ConsumoKWH: 0
            };

        return a;
    }

    function DownloadRecibo(button) {
        var p = GetEntidadRec();

        if (_O.IsMobil) {
            _L.Loading(button, 0);
            var b = _U.StringFormat('{0};{1};{2};{3}', p.IdNroServicio, p.IdPeriodo, p.IdTipoDocumento, p.PathRecibo),
                l = _U.StringFormat('Suministro/DescargarRecibos/{0}', btoa(b));

            location.href = _U.UrlAction(l);
            setTimeout(function () { _L.Loading(button, 1); }, 3000);
        }
        else {
            _L.Loading(button, 1);
            _U.DownloadFile(_U.UrlAction('Suministro/DescargarRecibo'), { Parametros: p }, 'blank', 0);
        }

        btnCancel_click();
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'MiRecibo'), jQuery));