﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
$(document).ready(inicialize);

function inicialize() {
    //$(".frmConsultaVisa").submit(function (event) { 
    //    $('.modPreload').modal('show');
    //    $(".btnPagar").prop('disabled', true);
    //});
};

function ConsultarSuministro(event, obj) {
    event.preventDefault();

    jQuery.ajax({
        type: "GET",
        async: true,
        url: obj.href,
        success: function (oDatos) {
            jQuery('.modResumen').find('.modal-title').html("AGREGAR SUMINISTRO");
            jQuery('.modResumen').find('.modal-body').html(oDatos);
            jQuery('.modResumen').modal({ backdrop: 'static', keyboard: false });
            jQuery('.modResumen').modal('show');
        }
    });
}

function LlamarDetalle(NroSum) {
    window.location.replace("../Suministro/Detalle?Suministro=" + NroSum);
}

function LlamarDatosBarra(lsbar, canvas) {
    var dataLabel = [];
    var dataValor = [];
    var dataColor = [];
    var dataTexto = [];

    $(jQuery.parseJSON(lsbar)).each(function () {
        dataLabel.push($(this).attr('x_etiqueta'));
        dataValor.push($(this).attr('y_valor'));
        dataColor.push($(this).attr('color_barra'));
        dataTexto.push($(this).attr('color_texto'));
    });

    var data = {
        labels: dataLabel,
        datasets: [{
            data: dataValor,
            backgroundColor: dataColor,
            borderColor: dataTexto,
            borderWidth: 1
        }]
    };

    var options = {
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                font: {
                    size: 8
                },
                formatter: function (value, context) {
                    //return context.chart.data.labels[context.dataIndex];
                    return dataValor[context.dataIndex];
                }
            }
        },
        animationEnabled: false,
        title: {
            display: true,
            text: "",
            position: 'top',
            fontSize: 5,
            fontColor: '#003591',
            fontStyle: 'bold',
            padding: 5
        },
        legend: {
            display: false,
            position: 'top',
            labels: {
                fontColor: 'black',
                fontSize: 18,
                boxWidth: 80
            }
        },
        scales: {
            yAxes: [{
                stacked: true,
                gridLines: {
                    display: false
                },
                ticks: {
                    display: false,
                    fontSize: 8
                },
                scaleLabel: {
                    display: false,
                    labelString: "Consumo en kW/h",
                    fontColor: "red"
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    display: true,
                    fontSize: 7,
                    padding: 0
                },
                scaleLabel: {
                    display: false,
                    labelString: "Ultimos 06 meses",
                    fontColor: "red"
                }
            }]
        }
    };

    var ctx = $(canvas);

    var ChartPrueba = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });
}

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _O.IdUser = _C.txtIdUser.val();
            _C.txtIdUser.remove();

            LoadLastEvent();
            $('.infoSuministro').on('click', '.card-icon .desasociar', btnDesasociar_click);
        });
    }

    //#region Events

    //#endregion Events

    function LoadLastEvent() {
        var l = $('.infoSuministro .cuadroSuministro .cardCabecera .txtSuministro');

        for (var i = 0; i < l.length; i++) {
            LoadLastAtencion(l.eq(i));
            LoadLastInterrupcion(l.eq(i));
        }
    }

    function LoadLastAtencion(sender) {
        var p = {
            Parametros: {
                IdNroServicio: sender.html()
            }
        };

        _U.Ajax('Suministro', 'AtencionesBySuministro', _U.AddToFormData(p), function (data) {
            var d = JSON.parse(data.Datos),
                b = sender.closest('.cuadroSuministro').find('.infoLastAtencion'),
                t = ' No hay información.';

            if (d.length > 0) {
                var a = d[0];

                t = _U.StringFormat('<a href="{0}Suministro/Detalle?Suministro={1}#lnkAtenciones" target="_blank"> {2}, <b>Estado:</b> {3}, <b>Fecha:</b> {4}</a>', _RootPath, p.Parametros.IdNroServicio, a.Recurso, a.SeAtendio == 1 ? 'Terminado' : 'Pendiente', a.SeAtendio == 1 ? a.FechaTermino : a.FechaRegistro);
            }

            b.html(t);
        }, function (data) { });
    }

    function LoadLastInterrupcion(sender) {
        var p = {
            Parametros: {
                IdNroServicio: sender.html()
            }
        };

        _U.Ajax('Home', 'Interrupciones', _U.AddToFormData(p), function (data) {
            var d = JSON.parse(data.Datos),
                b = sender.closest('.cuadroSuministro').find('.infoLastInterrupcion'),
                t = ' No hay información.';

            if (d.length > 0) {
                var a = d[0];

                t = _U.StringFormat('<a href="{0}Suministro/Detalle?Suministro={1}#lnkInterrupciones" target="_blank"> {2}, <b>Inicio:</b> {3}{4}</a>', _RootPath, p.Parametros.IdNroServicio, a.TipoRegistro, a.FechaInicio + ' ' + a.HoraInicio, a.EsProgramada == 1 ? (', <b>Fin:</b> ' + a.FechaFin + ' ' + a.HoraFin) : '');
            }

            b.html(t);
        }, function (data) { });
    }

    function btnDesasociar_click() {
        var s = $(this),
            p = {
                Parametros: {
                    IdUsuarioWeb: _O.IdUser,
                    IdNroServicio: s.parent().parent().find('.cardCabecera .txtSuministro').html()
                }
            };

        $('#modPreload').modal('show');

        _U.Ajax('Suministro', 'DesasociarSuministro', _U.AddToFormData(p), function (data) {
            var d = data.Datos;

            if (+d.IdError > 0) {
                _U.AlertError(d.Mensaje);
            }

            s.closest('.ContDetalle').remove();
            //DISTRILUZ 036 - INICIO FINANCIAMIENTO VIRTUAL
            if (!$('div.ContDetalle').length) {
                $('#procFinan').remove();
            }
            //DISTRILUZ 036 - FIN FINANCIAMIENTO VIRTUAL_U.AlertSuccess('Se quitó de su lista el suministro [' + p.Parametros.IdNroServicio + '] correctamente.');
        }, function (data) { $('#modPreload').modal('hide'); });
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'Resumen'), jQuery));