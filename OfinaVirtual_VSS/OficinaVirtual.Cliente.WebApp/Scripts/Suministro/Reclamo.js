﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2019-03-01
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _C.btnRegistrar.on('click', btnRegistrar_click);
            _C.btnPrevious.on('click', btnPrevious_click);
            _C.btnNext.on('click', btnNext_click);

            _C.cmbIdNroServicio.on('change', cmbIdNroServicio_change);
            _C.txtIdNroServicio.on('blur', txtIdNroServicio_blur);
            _C.txtIdNroServicio.on('keydown', txtIdNroServicio_keydown);

            FillEmpresa();
            FillTipoRelacion();
            FillTipoDocumento();
            FillClasificacion();

            SettingSuministro();

            _C.txtNombres.focus();
        });
    }

    //#region Events

    function btnRegistrar_click() {
        if (!ValidStep02()) return;

        _C.btnRegistrar.prop('disabled', true).removeClass('btn-green');
        $('#modPreload').modal('show');

        var p = {
            'Parametros': GetEntidad()
        };

        _U.Ajax('Suministro', 'RegistrarReclamo', _U.AddToFormData(p), function (data) {
            if (data.IdError == 0) {
                _C.divStep01.parent().remove();
                var s = 'Se registró correctamente, su código de registro es [' + data.Datos + '].'
                _U.AlertSuccess(s);
            }
            else _U.AlertError(data.Mensaje);
        }, function () {
            $('#modPreload').modal('hide');
            _C.btnRegistrar.prop('disabled', false).addClass('btn-green');
        });
    }

    function btnNext_click() {
        if (!ValidStep01()) return;

        _C.divStep01.addClass('d-none');
        _C.divStep02.removeClass('d-none');

        _C.cmbEmpresa.focus();
    }

    function btnPrevious_click() {
        _C.divStep01.removeClass('d-none');
        _C.divStep02.addClass('d-none');

        _C.txtNombres.focus();
    }

    function txtIdNroServicio_blur() {
        var s = _C.txtIdNroServicio.val();

        if (s.length == 0) return;

        if (isNaN(s) || s.includes('.') || +s > 9999999999) {
            _C.txtIdNroServicio.next().html(_C.txtIdNroServicio[0].validationMessage).show();
            return;
        }

        _U.Ajax('Suministro', 'InfoSuministro', { 'Parametros': { 'IdNroServicio': s } }, function (data) {
            var d = data.Datos;

            if (+d.IdEmpresa == 0) _C.txtIdNroServicio.next().html('Suministro no existe').show();
            else {
                _C.txtIdNroServicio.data('suminvalid', 1);
                _C.cmbEmpresa.val(d.IdEmpresa);
                FillPeriodos(s);
            }
        }, undefined, undefined, false);
    }

    function txtIdNroServicio_keydown() {
        _C.txtIdNroServicio.data('suminvalid', 0);
        _C.txtIdNroServicio.next().hide();
        _C.cmbPeriodos.html('');
    }

    function cmbIdNroServicio_change() {
        var s = _C.cmbIdNroServicio.val();

        if (+s > 0) {
            _C.txtIdNroServicio.val(s);
            FillPeriodos(s)
        }
    }

    //#endregion Events

    function GetEntidad() {
        var a = {
            IdReclamoOV: 0,
            IdEmpresa: _C.cmbEmpresa.val(),
            Nombres: _C.txtNombres.val(),
            IdTipoDocumento: _C.cmbIdTipoDocumento.val(),
            NroDocumento: _C.txtNroDocumento.val(),
            Telefono: _C.txtTelefono.val(),
            Email: _C.txtEmail.val(),
            EmailRe: _C.txtEmailRe.val(),
            DireccionNotifica: _C.txtDireccion.val(),
            IdNroServicio: _C.cmbIdNroServicio.find('option').length > 0 ? _C.cmbIdNroServicio.val() : _C.txtIdNroServicio.val(),
            IdTipoRelacion: _C.cmbTipoRelacion.val(),
            IdClasificacion:_C.cmbClasificacion.val(),
            PeriodoConcepto: _C.cmbPeriodos.val().join(';'),
            NotificarEmail: +_C.chkNotificaEmail.prop('checked'),
            Petitorio: _C.txtPetitorio.val()
        };

        if (a.IdNroServicio == '') a.IdNroServicio = 0;

        return a;
    }

    function ValidStep01() {
        _C.txtEmail.next().hide();
        _C.txtEmailRe.next().hide();
        _C.cmbIdTipoDocumento.next().hide();
        _C.txtNroDocumento.next().hide();

        var r = true;
        r &= _C.txtNombres[0].checkValidity();//_C.txtNombres[0].validity.valid
        r &= _C.cmbIdTipoDocumento[0].checkValidity();
        r &= _C.txtNroDocumento[0].checkValidity();
        r &= _C.txtDireccion[0].checkValidity();

        if (+_C.cmbIdTipoDocumento.val() == 0) {
            r = false;
            _C.cmbIdTipoDocumento.next().show();
        }

        if (_C.txtNroDocumento.val().length < 8) {
            _C.txtNroDocumento.next().html('Nro. documento esta incompleto').show();
        }

        if (isNaN(_C.txtNroDocumento.val())) {
            _C.txtNroDocumento.next().html('Nro. documento no es valido, ingrese solo números').show();
        }

        if (!_C.txtEmail[0].checkValidity()) {
            r = false;
            _C.txtEmail.next().html(_C.txtEmail[0].validationMessage).show();
        }

        if (!_C.txtEmailRe[0].checkValidity()) {
            r = false;
            _C.txtEmailRe.next().html(_C.txtEmailRe[0].validationMessage).show();
        } else if (_C.txtEmail.val() != _C.txtEmailRe.val()) {
            r = false;
            _C.txtEmailRe.next().html('Email no coinciden').show();
        }

        if (!r) _C.divStep01.addClass('was-validated');

        return r;
    }

    function ValidStep02() {
        console.log(GetEntidad());
        _C.cmbEmpresa.next().hide();
        _C.cmbClasificacion.next().hide();

        var r = true;
        r &= _C.txtPetitorio[0].checkValidity();

        if (+_C.cmbClasificacion.val() == 0) {
            r = false;
            _C.cmbClasificacion.next().show();
        }

        if (_C.cmbEmpresa.parent().css('display') == 'block') {
            r &= _C.txtIdNroServicio.next().css('display') == 'none';

            if (+_C.cmbEmpresa.val() == 0) {
                r = false;
                _C.cmbEmpresa.next().show();
            }

            if (!_C.txtIdNroServicio[0].checkValidity()) {
                r = false;
                _C.txtIdNroServicio.next().html(_C.txtIdNroServicio[0].validationMessage).show();
            }
        }

        if (!r) _C.divStep02.addClass('was-validated');

        return r;
    }

    function FillComboBox(control, param) {
        control.html('');

        if (!param.IsNotAll) control.html('<option value="0">---Seleccione---</option>');

        nsUtil.Ajax('Home', 'AyudaLista', { 'Parametros': param }, function (data) {
            $.each(data.Datos, function (i, v) {
                var a = v.Id || v.id,
                    b = v.Nombre || v.nombre,
                    c = param.Value == a,
                    t = _U.StringFormat('<option value="{0}" {2}>{1}</option>', a, b, c ? 'selected' : '');

                control.append(t);
            });

            if (param.Select2) control.select2({
                closeOnSelect: false,
                placeholder: "Peridos a reclamar",
                allowHtml: true,
                allowClear: true,
                tags: true // создает новые опции на лету
            });
        });
    }

    function FillEmpresa() {
        var p = {
            Entity: 'Empresa',
            idorganizacion: 1,
            AddPreFix: 0
        };

        FillComboBox(_C.cmbEmpresa, p);
    }

    function FillTipoRelacion() {
        var p = {
            Entity: 'TipoRelacion',
            Value: _C.cmbIdTipoDocumento.data('value')
        };

        FillComboBox(_C.cmbTipoRelacion, p);
    }

    function FillTipoDocumento() {
        var p = {
            Entity: 'DocumentoIdentidadOV',
            Value: _C.cmbIdTipoDocumento.data('value')
        };

        FillComboBox(_C.cmbIdTipoDocumento, p);
    }

    function FillClasificacion() {
        var p = {
            Entity: 'Clasificacion',
            AddPreFix: 0,
            IdRecurso: 1
        };

        FillComboBox(_C.cmbClasificacion, p);
    }

    function FillPeriodos(idNroServicio) {
        var p = {
            Entity: 'PeriodosFacturados',
            IdNroServicio: idNroServicio,
            IsNotAll: true,
            Select2: true
        };

        FillComboBox(_C.cmbPeriodos, p);
    }

    function SettingSuministro() {
        var a = _C.cmbIdNroServicio.find('option').length > 0;

        if (a) {
            _C.txtIdNroServicio.parent().hide();
            _C.cmbEmpresa.parent().hide();
            FillPeriodos(_C.cmbIdNroServicio.val());
        } else {
            _C.cmbIdNroServicio.parent().hide();
        }
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'Reclamo'), jQuery));