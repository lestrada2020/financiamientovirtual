﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Modificación    : 2019-03-01
    Responsable     : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            // _C = _U.GetControlsJQ();

            var c = $('#imgUltimoReciboDescargar');
            c.on('click', imgUltimoReciboDescargar_Click);
        });
    }

    function imgUltimoReciboDescargar_Click(event) {
        var n = $('#txtUltimoRecibo_IdNroServicio').val(),
            p = $('#txtUltimoRecibo_Periodo').val();

        window.nsUtil.GetNameSpace('Root', 'Suministro', 'Detalle').ShowRecibo(n, p);
    }

    //#region Events

    //#endregion Events

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', '_UltimoRecibo'), jQuery));