﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Modificación    : 2019-03-01
    Responsable     : JONATHAN VALDERRAMA
*/

function LlamarDatosBarra(lsbar, canvas) {
    var dataLabel = [];
    var dataValor = [];
    var dataColor = [];
    var dataTexto = [];

    $(jQuery.parseJSON(lsbar)).each(function () {
        dataLabel.push($(this).attr('x_etiqueta'));
        dataValor.push($(this).attr('y_valor').replace(/,/g, ''));
        dataColor.push($(this).attr('color_barra'));
        dataTexto.push($(this).attr('color_texto'));
    });

    var data = {
        labels: dataLabel,
        datasets: [{
            label: "",
            data: dataValor,
            backgroundColor: dataColor,
            borderColor: dataTexto,
            borderWidth: 1
        }]
    };

    var options = {
        onHover: function (event, data) {
            if (canvas == '.cnvFacturacion') {
                var a = data.length > 0;
                $(canvas).css('cursor', a ? 'pointer' : 'default');
            }
        },
        onClick: function (event, data) {
            if (canvas != '.cnvFacturacion' || !data[0]) return;
            var p = window.nsUtil.ConvertPeriodo(data[0]._model.label);

            window.nsUtil.GetNameSpace('Root', 'Suministro', '_Historico').DetalleRecibo(p);
        },
        plugins: {
            datalabels: {
                align: 'end',
                anchor: 'end',
                //color: 'black',
                font: {
                    size: 12
                },
                formatter: function (value, context) {
                    //return context.chart.data.labels[context.dataIndex];
                    return dataValor[context.dataIndex];
                }
            }
        },
        title: {
            display: true,
            text: "",
            position: 'top',
            fontSize: 5,
            fontColor: '#003591',
            fontStyle: 'bold',
            padding: 5
        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                stacked: true,
                position: "bottom",
                gridLines: {
                    display: false,
                },
                //offset: false,
                ticks: {
                    display: false,
                    fontSize: 12
                },
                scaleLabel: {
                    display: false,
                    labelString: "Consumo en KW.h",
                    fontColor: "red"
                },
            }],
            xAxes: [{
                gridLines: {
                    display: false,
                },
                //offset: false,
                ticks: {
                    display: true,
                    fontSize: 12,
                },
                scaleLabel: {
                    display: false,
                    labelString: "Ultimos 06 meses",
                    fontColor: "red"
                }
            }]
        }
    };

    var ctx = $(canvas);

    var ChartPrueba = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });
}

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            // _C = _U.GetControlsJQ();

            $('#imgReciboDescargar').on('click', imgReciboDescargar_Click);
        });
    }

    function imgReciboDescargar_Click(event) {
        var n = $(this),
            s = n.data('IdNroServicio'),
            p = n.data('IdPeriodo');

        window.nsUtil.GetNameSpace('Root', 'Suministro', 'Detalle').ShowRecibo(s, p);
    }

    function FillDetail(header, detalle) {
        $('#spnHisFac_Periodo').html(header.PeriodoDes);
        $('#lblHisFac_NroRecibo').html(header.Recibo);
        $('#lblHisFac_Importe').html(header.Importe);
        $('#lblHisFac_Tarifa').html(header.Tarifa);
        $('#lblHisFac_Consumo').html(header.Consumo);
        $('#lblHisFac_Emision').html(header.Emision);
        $('#lblHisFac_Vencimiento').html(header.Vencimiento);

        var r = ''
        $.each(detalle, function (k, v) {
            r += window.nsUtil.StringFormat('<tr><td>{0}</td><td>{1}</td></tr>', k, v);
        })

        $('#tblHisFac_Detalle').find('tbody').html(r);
        $('#modHisFac_Detalle').modal('show');
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    context.DetalleRecibo = function (idPeriodo) {
        var n = $('#imgReciboDescargar'),
            d = {
                Parametros: {
                    IdNroServicio: $('#txtHisFac_IdNroServicio').val(),
                    IdPeriodo: idPeriodo
                }
            };

        n.data('IdNroServicio', d.Parametros.IdNroServicio);
        n.data('IdPeriodo', d.Parametros.IdPeriodo);

        _U.Ajax('Suministro', 'DetalleRecibo', _U.AddToFormData(d), function (data) {
            var a = _U.GetDataFromTag(data.ListaInformacion[0], 2),
                b = _U.GetDataFromTag(data.ListaInformacion[1], 2);

            FillDetail(a, b);
        }, function (data) { });
    }

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', '_Historico'), jQuery));