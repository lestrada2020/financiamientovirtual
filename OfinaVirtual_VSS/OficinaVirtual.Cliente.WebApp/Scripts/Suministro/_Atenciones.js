﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación        : 2019-03-01
    Responsable     : JONATHAN VALDERRAMA
*/

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _C.tblAtencion_Detalle.on('click', 'tbody td a', colNroAtencion_Click);
        });
    }

    //#region Events

    //#endregion Events

    function colNroAtencion_Click(event) {
        var p = {
            Parametros: {
                NroAtencion: $(this).html()
            }
        };
        var r = _C.tblHeader.find('tbody'),
            s = _C.tblDetail.find('tbody');

        $('#modPreload').modal('show');
        r.html('');
        s.html('');
        _C.tblDetail.find('thead').html('');

        _U.Ajax('Home', 'Atenciones', _U.AddToFormData(p), function (data) {
            var d = JSON.parse(data.Datos),
                h = d.Header[0];

            $('#modAtencion_Detalle').modal('show');

            r.data('info', h);
            r.append('<tr><th>Petitorio</th><td>' + h['Petitorio'] + '</td></tr>');

            //console.log(h);
            for (var i = 0; i < d.Detail.length; i++) {
                var aa = d.Detail[i],
                    tr = $('<tr/>'),
                    th = $('<tr/>');

                tr.data('info', aa);
                $.each(aa, function (k, v) {
                    if (k.startsWith('-')) return;

                    th.append(_U.StringFormat('<th>{0}</th>', k));
                    tr.append(_U.StringFormat('<td>{0}</td>', v));
                });

                if (i == 0) _C.tblDetail.find('thead').append(th);
                s.append(tr);
            }
        }, function (data) {
            $('#modPreload').modal('hide');
        });
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', '_Atenciones'), jQuery));