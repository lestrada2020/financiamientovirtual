﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Modificación    : 2019-03-01
    Responsable     : JONATHAN VALDERRAMA
*/
function CargarPartialViewAsync(Ruta, Contenedor, Parametro) {
    var a = { Suministro: Parametro };

    if (Parametro instanceof Object) a = Parametro

    jQuery.ajax({
        type: "GET",
        async: true,
        url: Ruta,
        data: a,
        success: function (oDatos) {
            jQuery(Contenedor).html(oDatos);
        }
    });
}

function CargarPartialViewAsync2(Ruta, Contenedor, Parametro1, Parametro2) {
    jQuery.ajax({
        type: "GET",
        async: true,
        url: Ruta,
        data: { Suministro: Parametro1, Periodo: Parametro2 },
        success: function (oDatos) {
            jQuery(Contenedor).html(oDatos);
        }
    });
}

(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            ListarCortes();
            AddOptionFotoLec();

            _C.btnFotoLecSave.on('click', btnFotoLecSave_click);
            _C.filArchivo.on('change', filArchivo_change);
        });
    }

    //#region Events

    function btnFotoLectura_click(e) {
        $('#modFotoLec').modal('show');
    }

    function btnFotoLecSave_click(e) {
        var p = {
            FileArchivo: _C.filArchivo[0].files[0],
            Parametros: {
                IdNroServicio: $('#lblIdNroServicio').prev().html(),
                Archivo: '',
                FromBase64: 'Archivo'
            }
        };

        if (!ValidFotoLec(p.FileArchivo)) return;

        _C.btnFotoLecSave.prop('disabled', true);
        $('#modPreload').modal('show');

        _U.Ajax('Suministro', 'RegistrarFotoLec', _U.AddToFormData(p), function (data) {
            var c = _C.filArchivo.closest('.contCard').find('.alert');

            c.removeClass('d-none');
            c.removeClass('alert-danger');
            c.html('Se grabo correctamente [' + data.Datos + '].').addClass('alert-success');
            _C.filArchivo[0].value = '';
        }, function (data) {
            $('#modPreload').modal('hide');
            _C.btnFotoLecSave.prop('disabled', false);
        });
    }

    function filArchivo_change() {
        var c = _C.filArchivo.closest('.contCard').find('.alert');

        c.addClass('d-none');
    }
    //#endregion Events

    function ListarCortes() {
        var p = {
            Parametros: {
                IdNroServicio: $('#lblIdNroServicio').prev().html()
            }
        };

        _U.Ajax('Home', 'Interrupciones', _U.AddToFormData(p), function (data) {
            var d = JSON.parse(data.Datos),
                r = _C.divInterrupciones;

            r.html('');

            for (var i = 0; i < d.length; i++) {
                var v = d[i],
                    a = _C.tmpInterrupcion.clone().removeAttr('id'),
                    t = '';

                if (v.EsProgramada == 0) t = _U.StringFormat('Evento inesperado el cual interrumpió el servicio eléctrico el día {0} desde las {1}.', v.FechaInicio, v.HoraInicio);
                else if (v.EsProgramada == 1) t = _U.StringFormat('Mantenimiento preventivo se interrumpirá el servicio eléctrico el día {0} desde las {1} hasta las {2}.', v.FechaInicio, v.HoraInicio, v.HoraFin);
                else t = _U.StringFormat('Se le ralizon un corte del servicio eléctrico el día {0} a las {1}.', v.FechaInicio, v.HoraInicio, v.HoraFin, v.Localidades);
                a.find('.media-body > p:first-child').html((i + 1) + ') ' + v.TipoRegistro);
                a.find('.media-body > p:last-child').html(t);

                r.append(a);
            }
        }, function (data) { });
    }

    function AddOptionFotoLec() {
        var a = $('#navbarDs');

        a.find('ul').prepend('<li class="nav-item"><a id="btnFotoLectura" class="nav-link" href="javascript:void(0)"><img src="' + _RootPath + '/Content/img/ico_alert.png" alt="Foto Lectura"> Lectura</a></li>');
        _C.btnFotoLectura = $('#btnFotoLectura');
        _C.btnFotoLectura.on('click', btnFotoLectura_click);
    }

    function ValidFotoLec(file) {
        var r = true,
            t = '',
            c = _C.filArchivo.closest('.contCard').find('.alert');

        c.addClass('d-none');
        c.removeClass('alert-success').addClass('alert-danger');

        if (!file.type.startsWith('image/')) {
            r = false;
            t = 'Archivo no valido, favor de subir solo fotos.';
        }

        if (file.size > 1024 * 1024 * 3) {
            r = false;
            t = 'Archivo demasiado grande, lo permitido es máximo 3 MB';
        }

        if (!r) c.html(t).removeClass('d-none');

        return r;
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    context.ShowRecibo = function (idNroServicio, idPeriodo) {
        if ($('#modReciboDig').length == 0) $('body').append('<div class="modal fade" id="modReciboDig" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog" style="max-width: 90%;"><div class="modal-content" style="height: calc(100vh - 30px);"><div class="modal-header"><label class="modal-title font-montserrat font-weight-bold hint-text"></label><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"></div></div></div></div>');

        var m = $('#modReciboDig'),
            a = m.find('.modal-body'),
            b = m.find('.modal-header label'),
            r = nsUtil.DefaultFormatDateTime().MonthNames,
            d = {
                Parametros: {
                    IdNroServicio: idNroServicio,
                    IdPeriodo: idPeriodo,
                    IdTipoDocumento: 1 //(1)PDF,(2)PNG
                }
            };;

        m.modal('show');
        a.html('');
        b.html('Cargando...');

        $('#modPreload').modal('show');

        nsUtil.Ajax('Suministro', 'ObtenerRecibo', d, function (data) {
            var y = parseInt(d.Parametros.IdPeriodo / 100),
                t = d.Parametros.IdPeriodo % 100
            f = '<embed width="100%" height="100%" src="data:application/pdf;base64,' + data.Datos + '" type="application/pdf" />';

            a.html(f);
            b.html('Recibo ' + r[t - 1] + '-' + y);
        }, function (data) {
            setTimeout(function () { $('#modPreload').modal('hide'); }, 1000);
        });
    }

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'Detalle'), jQuery));