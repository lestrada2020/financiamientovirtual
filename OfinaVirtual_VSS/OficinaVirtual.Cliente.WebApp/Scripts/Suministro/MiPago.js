﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-04-22
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal'),
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
            _L.ShowLogoVisa();
            _O.IsMobil = window.matchMedia('(max-width: 767px)').matches;
            _O.Data = {};
            _O.IdOrder = _C.txtIdNroServicio.data('idorder');
            _O.IdNroServicio = _C.txtIdNroServicio.data('idnroservicio');
            _O.IdUser = _C.txtIdNroServicio.data('iduser');

            //$('[data-toggle="tooltip"]').tooltip();
            _C.txtIdNroServicio.on('keypress', txtIdNroServicio_keypress);
            _C.btnSearch.on('click', btnSearch_click);
            _C.btnPrevious01.on('click', btnPrevious01_click);
            _C.btnPrevious02.on('click', btnPrevious02_click);
            _C.btnNext01.on('click', btnNext01_click);
            _C.chkTerminos.on('click', chkTerminos_change);
            _C.btnPrint.on('click', btnPrint_click);
            _C.btnFinish.on('click', btnFinish_click);
            _C.tblDeuda.on('change', 'tbody tr td:first-child input', chkPeriodos_change);
            _C.txtIdNroServicio.focus();

            CheckPayStatus() || CheckHasSumin();
        });
    }

    //#region Events

    function btnSearch_click() {
        _L.ClearAlert();
        _L.Loading(_C.btnSearch, 0);

        if (!ValidSearch()) {
            _L.Loading(_C.btnSearch, 1);
            return;
        }

        GetSuministro();
    }

    function txtIdNroServicio_keypress(e) {
        if (e.which == 13) btnSearch_click();
        else if (e.which < 48 || e.which > 57 || _C.txtIdNroServicio.val().length > 8) return false;

        return true;
    }

    function btnPrevious01_click() {
        btnFinish_click();
    }

    function btnNext01_click(e) {
        if (!EnabledButtonNext01()) return;

        _L.Loading(_C.btnNext01, 0);

        GetPedido();
    }

    function btnPrevious02_click(e) {
        ShowWizard(1);
    }

    function chkPeriodos_change() {
        var a = $(this),
            b = a.closest('tr'),
            c = a.attr('id').replace('chk', ''),
            d = RowsSelecteds(),
            r = a.closest('table').find('tfoot tr th:last-child');

        if (a.prop('checked')) b.addClass('table-success');
        else b.removeClass('table-success');

        _O.PayImporte = d.reduce((r, s) => +s.ImporteTotal + +r, 0).toFixed(2);

        r.html(_O.PayImporte);
        EnabledButtonNext01();
    }

    function chkTerminos_change() {
        EnabledButtonNext01();
    }

    function btnPrint_click(e) {
        var u = _U.UrlAction('MiVoucher', 'Suministro'),
            p = {
                TokenCaptcha: _U.CaptchaToken(),
                NroPedido: _O.IdOrder
            };

        _U.SendForm(u, { Parametros: p });
    }

    function btnFinish_click(e) {
        location.href = _U.UrlAction('MiPago', 'Suministro');
    }

    //#endregion Events

    //#region Wizard

    function ShowWizard(idStep) {
        var a = _U.ToArrayJQ([_C.divStep01, _C.divStep02, _C.divStep03]),
            b = _U.ToArrayJQ([_C.imgPago01, _C.imgPago02, _C.imgPago03]),
            c = _U.ToArrayJQ([_C.numPago01, _C.numPago02, _C.numPago03]);

        a.addClass('d-none');
        b.find('>img').attr('src', _U.UrlContent('Content/img/rayoStep0.png'));
        c.removeClass('lighBlue');

        switch (+idStep) {
            case 1:
                ShowStep01();
                break;
            case 2:
                ShowStep02();
                break;
            case 3:
                ShowStep03();
                break;
            default:
                break;
        }
    }

    function ShowStep01() {
        ShowStepHeader(1);
        _C.divStep01.removeClass('d-none');
    }

    function ShowStep02() {
        ShowStepHeader(1, 2);
        _C.divStep02.removeClass('d-none');
    }

    function ShowStep03() {
        ShowStepHeader(1, 2, 3);
        _C.divStep03.removeClass('d-none');
        _U.ToArrayJQ([_C.divStep01, _C.divStep02]).html('');
    }

    function ShowStepHeader(step) {
        for (var i = 1; i <= arguments.length; i++) {
            _C['numPago0' + i].addClass('lighBlue');
            _C['imgPago0' + i].find('>img').attr('src', _U.UrlContent(_U.StringFormat('Content/img/rayoStep{0}.png', i)));
        }
    }

    //#endregion Wizard

    //#region Show

    function ShowResult() {
        _C.divSearch.addClass('d-none');
        _C.divResult.removeClass('d-none')
        _C.divMiPago.addClass('bn-container-full');

        ShowDataSumin();

        if (+_O.IdOrder > 0) {
            ShowWizard(3);
        } else {
            ShowDataDeuda();
            ShowWizard(1);
        }
    }

    function ShowDataSumin() {
        var a = _O.Data.Suministro,
            b = _O.Data.Deuda || [],
            c = b.length > 0 ? b[b.length - 1] : {};

        _C.lblSuministro.html(a.IdNroServicio);
        _C.lblNombres.html(a.NombreAnonimo);
        //_C.lblDireccion.html(c.Direccion || '');
        //_C.lblFechaEmision.html(c.FechaEmision || '');
        //_C.lblFechaVencimiento.html(c.FechaVencimiento || '');
        _C.lblDeudaTotal.html('S/ ' + b.reduce((r, s) => +s.ImporteTotal + +r, 0).toFixed(2));
    }

    function ShowDataDeuda() {
        var a = _O.Data.Deuda,
            b = _U.DefaultFormatDateTime().MonthNamesShort;

        for (var i = 0; i < a.length; i++) {
            var r = $('<tr/>'),
                p = a[i],
                m = '',
                n = p.Periodo.toString().substr(0, 4),
                l = b[p.Periodo.toString().substr(4, 2) - 1],
                o = _U.StringFormat('<input id="chk{0}" type="checkbox" />', p.Periodo);

            m += _U.StringFormat('<td>{0}</td>', o);
            m += _U.StringFormat('<td>{0}</td>', l + '-' + n);
            m += _U.StringFormat('<td>{0}</td>', p.TipoDocumento);
            //m += _U.StringFormat('<td>*****{0}</td>', p.NroDocumento.substr(p.NroDocumento.length - 3));
            m += _U.StringFormat('<td>{0}</td>', p.FechaVencimiento);
            m += _U.StringFormat('<td>{0}</td>', p.ImporteTotal.toFixed(2));

            p.Index = i;
            r.data('entidad', p).html(m);
            _C.tblDeuda.find('tbody').append(r);
        }
    }

    function ShowDataConfirma(data) {
        var a = data.Order;

        _C.lblNroPedido.html(a.NroPedido);
        _C.lblImportePedido.html(_O.PayImporte);
        _C.lblFechaPedido.html(a.FechaRegistro);
        _C.frmPagoCard.attr('action', _U.UrlContentFull('Suministro/MyPay/' + a.NroPedido));

        LoadPayButton(data.Pay);
    }

    function showDataVoucher(data) {
        var t = '',
            s = '',
            a = JSON.parse(data.Trama || '{}'),
            b = _C.txtIdNroServicio.data('errormessage');

        if (!a.header) {
            t = 'PAGO EN PROCESO...';
            s = 'Actualice la página en unos minutos.';
            if (b) s += ' <br/>detalle: ' + b;
        } else if (a.errorCode == 400) {
            t = 'PAGO NO REALIZADO';
            s = _U.StringFormat('Motivo: [{0}] - {1}', a.data.ACTION_CODE || '---', a.data.ACTION_DESCRIPTION || 'No existe.');
        }

        if (t.length > 0) {
            _C.lblPayTitle.html(t);
            _C.lblPaySubTitle.html(s);
            _C.btnPrint.remove();
            _C.divDataPay.remove();

            return;
        }
        var f = '20' + a.order.transactionDate;

        _C.lblPayNroPedido.html(a.order.purchaseNumber);
        _C.lblPayNroTarjeta.html(a.dataMap.CARD);
        _C.lblPayFecha.html(_U.StringFormat('{0}-{1}-{2} &nbsp{3}:{4}:{5}', f.substr(0, 4), f.substr(4, 2), f.substr(6, 2), f.substr(8, 2), f.substr(10, 2), f.substr(12, 2)));
        _C.lblPayImporte.html(a.order.amount.toFixed(2));
        _C.lblPayMoneda.html(a.order.currency);
        _C.lblPaySuministro.html(data.IdNroServicio);
        _C.lblPayPeriodo.html(data.Periodos);
        _C.lblPayCodigoAutoriza.html(a.order.authorizationCode);
        _C.lblPayDescripcionAutoriza.html(a.dataMap.ACTION_DESCRIPTION);
        _C.lblPayComercio.html(data.Empresa);
        _C.lblPayComercioTelefono.html(data.Telefono);
        _C.lblPayComercioDireccion.html(data.Direccion);
    }

    //#endregion Show

    function LoadPayButton(data) {
        var s = document.createElement('script');

        s.setAttribute('src', data.Src);
        $.each(data.Data, (k, v) => s.setAttribute(_U.StringFormat('data-{0}', k), v));
        s.setAttribute('data-channel', 'web');
        s.setAttribute('data-merchantlogo', 'https://www.distriluz.com.pe/templates/lb_hct/images/logo.png');
        s.setAttribute('data-merchantname', 'DISTRILUZ');
        s.setAttribute('data-expirationminutes', '5');
        s.setAttribute('data-timeouturl', _U.UrlContentFull('Suministro/MiPago'));

        document.getElementById('frmPagoCard').appendChild(s);
    }

    function ValidSearch() {
        var v = _C.txtIdNroServicio.val();

        _C.txtIdNroServicio.next().hide();

        var r = true;
        r &= _C.txtIdNroServicio[0].checkValidity();

        if (v.length < 6) {
            r = false;
            _C.txtIdNroServicio.next().html(_U.StringFormat('Suministro incorrecto, debe tener mas de 5 dígitos')).show();
        }

        if (isNaN(v)) {
            r = false;
            _C.txtIdNroServicio.next().html(_U.StringFormat('Suministro no es valido, ingrese solo números', b)).show();
        }

        if (!r) _C.divSearch.addClass('was-validated');

        return r;
    }

    function ValidSelectPeriodos() {
        var b = RowsSelecteds(),
            c = -1,
            r = true;

        c = b.reduce((max, s) => +s.Index > +max ? s.Index : max, b[0].Index);

        for (var i = c; i > -1; i--) r = r && b.some(s => +s.Index == i);

        return r;
    }

    function RowsSelecteds() {
        var a = [];

        _C.tblDeuda.find('tbody tr td:first-child input').each(function (i, v) {
            var m = $(v),
                n = m.closest('tr').data('entidad');

            if (m.prop('checked')) a.push(n);
        });

        return a;
    }

    function EnabledButtonNext01() {
        _L.ClearAlert();
        var a = _C.chkTerminos.prop('checked'),
            b = RowsSelecteds().length > 0,
            c = b && ValidSelectPeriodos();

        a = a && b && c;

        _C.btnNext01.prop('disabled', !a);

        if (a) _C.btnNext01.removeClass('btn-outline-success').addClass('btn-success');
        else _C.btnNext01.removeClass('btn-success').addClass('btn-outline-success');

        if (!c) _U.AlertError('Debe seleccionar el periodo más antiguo, para continuar.');

        return a;
    }

    function CheckPayStatus() {
        var a = +_O.IdOrder;

        if (a == 0) return false;
        if (isNaN(a)) btnFinish_click();
        _L.Loading(_C.btnSearch, 0);
        GetPay();

        return true;
    }

    function CheckHasSumin() {
        var a = +_O.IdNroServicio;

        if (a == 0) return false;
        _C.txtIdNroServicio.val(a);
        btnSearch_click();

        return true;
    }

    //#region Ajax

    function GetSuministro() {
        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdNroServicio: _C.txtIdNroServicio.val()
        };

        _U.Ajax('Suministro', 'DatosNroServicio', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            _O.Data.Suministro = d;
            setTimeout(function () { GetDeuda(); }, 1000);
        }, function (data) {
            if (data.IdError > 0) _L.Loading(_C.btnSearch, 1);
        });
    }

    function GetDeuda() {
        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdNroServicio: _C.txtIdNroServicio.val()
        };

        _U.Ajax('Suministro', 'ObtenerDeudaPeriodos', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            _O.Data.Deuda = d;
            ShowResult();
        }, function (data) {
            if (data.IdError > 0) {
                if (data.Mensaje.indexOf('no tiene') > 0 && +_O.IdOrder > 0) {
                    _L.ClearAlert();
                    ShowResult();
                }
            }
            _L.Loading(_C.btnSearch, 1);
        });
    }

    function GetPedido() {
        var a = RowsSelecteds(),
            b = a[0],
            p = {
                TokenCaptcha: _U.CaptchaToken(),
                IdEmpresa: b.IdEmpresa,
                IdNroServicio: b.IdNroServicio,
                Importe: _O.PayImporte,
                IdOrigen: 3,
                IdUser: +_O.IdUser > 0 ? +_O.IdUser : 1,
                Detalle: JSON.stringify(a)
            };

        _U.Ajax('Suministro', 'ObtenerNroPedido', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            ShowDataConfirma(d)
            ShowWizard(2);
        }, function (data) { _L.Loading(_C.btnNext01, 1); });
    }

    function GetPay() {
        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            NroPedido: _O.IdOrder
        };

        _U.Ajax('Suministro', 'ObtenerDataPedido', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            if (!d || +d.IdNroServicio == 0) {
                _U.AlertError('Nro. pedido no existe.');
                setTimeout(function () { btnFinish_click(); }, 3000);
                return;
            }

            _O.Data.Intent = d;
            showDataVoucher(d);
            _C.txtIdNroServicio.val(d.IdNroServicio);
            setTimeout(function () { btnSearch_click(); }, 1000);
        }, function (data) { _L.Loading(_C.btnNext01, 1); });
    }

    //#endregion Ajax

    //#endregion Private

    //#region Public

    (function () { _U.CaptchaReady(Load); }());
    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'MiPago'), jQuery));