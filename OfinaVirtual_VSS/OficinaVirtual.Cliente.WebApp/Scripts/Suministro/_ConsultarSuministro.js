﻿$(document).ready(inicialize);

function inicialize() {

    $(".frmConsultar").submit(function (event) {
        event.preventDefault();
        click_btnBuscar($(this));
    });

    $(".frmRegistrar").submit(function (event) {
        event.preventDefault();
        click_btnRegistrar($(this));
    });

}

function click_btnBuscar(oForm) {

    $(".btnBuscar").prop('disabled', true);

    $('.modPreload').modal('show');

    jQuery.ajax({
        type: oForm[0].method,
        url: oForm[0].action,
        data: $(oForm[0]).serialize(),
        dataType: 'HTML',
        success: function (oDatos) {
            $('.modPreload').modal('hide');
            jQuery('.modResumen').find('.modal-body').html(oDatos);
        }
    });
}

function click_btnRegistrar(oForm) {

    $(".btnRegistrar").prop('disabled', true);

    $('.modPreload').modal('show');

    jQuery.ajax({
        type: oForm[0].method,
        url: oForm[0].action,
        data: $(oForm[0]).serialize(),
        dataType: 'HTML',
        success: function (oDatos) {
            jQuery('.modResumen').find('.modal-body').html(oDatos);
            jQuery(".modResumen").fadeTo(1000, 100).slideUp(1000, function () {               
                jQuery('.frmResumen').submit();
            });
        }
    })
}