﻿$(document).ready(inicialize);

function inicialize() {

    //jQuery.ajax({
    //    type: oForm[0].method,
    //    url: oForm[0].action,
    //    data: $(oForm[0]).serialize(),
    //    dataType: 'HTML',
    //    success: function (oDatos) {
    //        jQuery('.modResumen').find('.modal-body').html(oDatos);
    //        jQuery(".modResumen").fadeTo(1000, 100).slideUp(1000, function () {
    //            jQuery('.frmResumen').submit();
    //        });
    //    }
    //})

}

$(document).ready(function () {

    var sPaso = jQuery('.Paso').val();
    var sCerrar = jQuery('.Cerrar').val();
    var sMensaje = jQuery('.modPagoVisa').find('.modal-body').html().trim();

    if (sPaso == "2") {
        window.location.replace("../Suministro/VisaPago");

        //jQuery.ajax({
        //    type: "POST",
        //    async: true,
        //    url: "/Suministro/VisaPago", //oForm[0].action,
        //    dataType: 'HTML',
        //    success: function (oDatos) {
        //        $('#step02').html(oDatos)                 
        //    }
        //});
    }

    if (sMensaje != "") {
        jQuery('.modPagoVisa').modal('show');
        if (sCerrar == 1) {
            jQuery(".modPagoVisa").fadeTo(1000, 100).slideUp(1000, function () {
                jQuery('.frmVisaConsultaResumen').submit();
            });
        }
    }

    $("#chkTerminos").on("change", function () {
        var isChecked = document.getElementById("chkTerminos").checked;
        var btn02 = document.getElementById("btnSiguiente02");
        if (!isChecked) {
            btn02.classList.remove("btn-green");
            btn02.setAttribute("disabled", "true");
        } else {
            btn02.disabled = !isChecked;
            btn02.classList.add("btn-green");

        }
    });
})

$(document).on('click', '.contStepago button', function (obj) {
    obj.preventDefault();
    sId = $(this).attr('id');

    $('#modPreload').modal('show');

    if (sId == 'btnSiguiente02') {

        var Tipo = $('input[name="rdMonto"]:checked').attr('id');
        var chkTerminos = document.getElementById("chkTerminos").checked;
        $("#tipoMonto").val(Tipo);

        if (chkTerminos) {
            $("#frmVisaValida").submit();
        }
        else {
            alert("Debe seleccionar terminos y condiciones");
        }
    }
    else if (sId == 'btnFinalizar') {
        $("#frmVisaFinalizar").submit();
    }
});
