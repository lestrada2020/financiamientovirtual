﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-04-11
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _L = _U.GetNameSpace('Root', 'Shared', 'LayoutPrincipal'),
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        _U.CaptchaReady();
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();

            _C.txtIdNroServicio.focus();
            _C.txtIdNroServicio.on('keydown', txtIdNroServicio_keydown);
            _C.btnSearch.on('click', btnSearch_click);
            _C.btnCancel.on('click', btnCancel_click);
            _C.btnSave.on('click', btnSave_click);
            _C.divResult.find('>a').on('click', lnkaqui_click);
        });
    }

    //#region Events

    function btnSearch_click() {
        _L.Loading(_C.btnSearch, 0);

        if (!ValidQuery()) {
            _L.Loading(_C.btnSearch, 1);
            _C.txtIdNroServicio.focus();
            return;
        }

        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdNroServicio: _C.txtIdNroServicio.val()
        };

        _U.Ajax('Suministro', 'DatosNroServicio', _U.AddToFormData({ Parametros: p }), function (data) {
            var d = JSON.parse(data.Datos);

            _C.txtIdNroServicio.data('data', d);

            _C.divResult.find('>span').html(d.NombreAnonimo);
            _C.divResult.removeClass('d-none');
        }, function () { _L.Loading(_C.btnSearch, 1); });
    }

    function txtIdNroServicio_keydown(e) {
        _L.ClearAlert(ClearQuery);
        if (e.which == 13) btnSearch_click();
    }

    function btnCancel_click() {
        location.reload();
    }

    function btnSave_click() {
        _L.Loading(_C.btnSave, 0);

        if (!ValidSave()) {
            _L.Loading(_C.btnSave, 1);
            return;
        }

        var p = GetEntidad();

        if (p.HasLecturaOV) {
            var b = confirm('Ud. ya tiene un registro anterior, desea reemplazarlo.');

            if (!b) {
                _L.Loading(_C.btnSave, 1);
                return;
            }
        }

        _U.Ajax('Suministro', 'LecturaRegistrar', _U.AddToFormData({ Parametros: p }), function (data) {
            var s = 'Se registró correctamente su lectura.'

            _C.btnSave.remove();
            _U.AlertSuccess(s);
            setTimeout(btnCancel_click, 3000);
        }, function () { _L.Loading(_C.btnSave, 1); });
    }

    function lnkaqui_click() {
        _C.btnSearch.remove();
        _C.txtIdNroServicio.prop('disabled', true);
        _C.frmUpdate.removeClass('d-none');
        _C.divResult.addClass('d-none');

        setTimeout(function () { _C.txtLectura.focus(); }, 100);
    }

    //#endregion Events

    function ValidQuery() {
        _C.txtIdNroServicio.next().hide();

        var r = true;
        r &= _C.txtIdNroServicio[0].checkValidity();

        if (!_C.txtIdNroServicio[0].checkValidity()) {
            r = false;
            _C.txtIdNroServicio.next().html(_C.txtIdNroServicio[0].validationMessage).show();
        }

        if (_C.txtIdNroServicio.val().length < 6) {
            r = false;
            _C.txtIdNroServicio.next().html('Suministro incorrecto, debe tener mas de 5 dígitos').show();
        }

        if (!r) _C.frmQuery.addClass('was-validated');

        return r;
    }

    function GetEntidad() {
        var d = _C.txtIdNroServicio.data('data'),
            r = {
                IdNroServicio: d.IdNroServicio,
                Lectura: _C.txtLectura.val(),
                Telefono: _C.txtPhone.val(),
                Email: _C.txtEmail.val(),
                HasLecturaOV: +d.HasLecturaOV
            };

        return r;
    }

    function ValidSave() {
        _C.txtLectura.next().hide();
        _C.txtEmail.next().hide();
        _C.txtPhone.next().hide();

        var r = true;
        r &= _C.txtLectura[0].checkValidity();
        r &= _C.txtPhone[0].checkValidity();
        r &= _C.txtEmail[0].checkValidity();

        if (!_C.txtLectura[0].checkValidity()) {
            r = false;
            _C.txtLectura.next().html(_C.txtLectura[0].validationMessage).show();
        }

        if (_C.txtLectura.val().length == 0) {
            r = false;
            _C.txtLectura.next().html('Lectura no valida, ingrese un valor').show();
        }

        if (!_C.txtPhone[0].checkValidity()) {
            r = false;
            _C.txtPhone.next().html(_C.txtPhone[0].validationMessage).show();
        }

        if (!_C.txtEmail[0].checkValidity()) {
            r = false;
            _C.txtEmail.next().html(_C.txtEmail[0].validationMessage).show();
        }

        if (!r) _C.frmUpdate.addClass('was-validated');

        return r;
    }

    function ClearQuery() {
        _C.divResult.addClass('d-none');
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'MiLectura'), jQuery));