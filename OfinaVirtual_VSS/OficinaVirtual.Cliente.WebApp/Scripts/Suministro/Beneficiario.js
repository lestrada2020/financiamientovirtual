﻿/// <reference path="../jquery-3.2.1.min.js" />
/// <reference path="../Util.js" />
/*
    Creación    : 2020-04-11
    Responsable : JONATHAN VALDERRAMA
*/
(function (context, $, undefined) {
    //#region Fields

    var _C, _S,
        _U = window.nsUtil,
        _O = $.extend({}, {});

    //#endregion Fields

    //#region Private

    function Load() {
        _U.CaptchaReady();
        $(document).ready(function (a) {
            _C = _U.GetControlsJQ();
            _O.Month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];

            $('img.logoVisa').parent().hide();
            $('[data-toggle="tooltip"]').tooltip();

            _C.txtIdNroServicio.focus();
            _C.txtIdNroServicio.on('keydown', txtIdNroServicio_keydown);
            _C.btnSearch.on('click', btnSearch_click);
            _C.btnCancel.on('click', btnCancel_click);
            _C.btnSave.on('click', btnSave_click);
        });
    }

    //#region Events

    function btnSearch_click() {
        Loading(_C.btnSearch, 0);

        var p = {
            TokenCaptcha: _U.CaptchaToken(),
            IdNroServicio: _C.txtIdNroServicio.val()
        };

        if (+p.IdNroServicio < 1) {
            Loading(_C.btnSearch, 1);
            _U.AlertError('Suministro no valido');
            return;
        };

        _U.Ajax('Suministro', 'ObtenerBeneficiario', _U.AddToFormData({ Parametros: p }), function (data) {
            if (data.IdError == 0) {
                var d = JSON.parse(data.Datos),
                    c = 'alert-success',
                    a = '<div>Complete sus datos para mantenerle informado sobre este y otros temas. Ingrese <a id="btnAqui" href="javascript:void(0)" class="badge badge-info">Aqui</a></div>',
                    s = _U.StringFormat('Sr(a). {0}, Usted <b>SI ES BENEFICIARIO</b> del fraccionamiento de pago del recibo', d.NombreBeneficiario);

                $('.bn-container').data('data', d);

                if (+d.EsBeneficiario == 0) {
                    c = 'alert-warning';
                    s = _U.StringFormat('Sr(a). {0}, Usted NO ES BENEFICIARIO del fraccionamiento de pago del recibo.', d.NombreBeneficiario);

                    if (+d.PFactura > 0) {
                        s += _U.StringFormat('<div><b>Consumo periodo {2} {1}:</b> {0} kW/h</div>', d.ConsumoKWH, d.PFactura.substr(0, 4), _O.Month[+d.PFactura.substr(-2) - 1]);
                        s += _U.StringFormat('<div><b>Importe de recibo:</b> S/ {0}', (+d.Importe).toFixed(2));
                        //s += _U.StringFormat('<div><b>Fecha vencimiento:</b> {0}.', d.FechaVencimiento);
                    }
                }

                _C.divResult.removeClass('d-none').addClass(c).html(s + a);
                $('#btnAqui').on('click', btnAqui_click);
            }
            else _U.AlertError(data.Mensaje);
        }, function () { Loading(_C.btnSearch, 1); });
    }

    function txtIdNroServicio_keydown(e) {
        ClearQuery();
        if (e.which == 13) btnSearch_click();
    }

    function btnAqui_click() {
        var d = $('.bn-container').data('data'),
            h = _U.StringFormat('Suministro: {0}', d.IdNroservicio);

        ClearQuery();
        $('.bn-header span').html(h);
        _C.frmQuery.addClass('d-none');
        _C.frmUpdate.removeClass('d-none');
    }

    function btnCancel_click() {
        _C.modPreload.modal('show');
        location.reload();
    }

    function btnSave_click() {
        Loading(_C.btnSave, 0);

        if (!ValidSave()) {
            Loading(_C.btnSave, 1);
            return;
        }

        var p = {
            Parametros: GetEntidad()
        };

        _U.Ajax('Suministro', 'RegistrarBeneficiario', _U.AddToFormData(p), function (data) {
            if (data.IdError == 0) {
                var s = 'Se actualizo correctamente sus datos.'

                _C.btnSave.remove();
                _U.AlertSuccess(s);
                setTimeout(btnCancel_click, 3000);
            }
            else _U.AlertError(data.Mensaje);
        }, function () { Loading(_C.btnSave, 1); });
    }

    //#endregion Events

    function ClearQuery() {
        _C.divAlert.removeAttr('class').html('');
        _C.divResult.addClass('d-none').removeClass('alert-success').removeClass('alert-warning');
    }

    function GetEntidad() {
        var d = $('.bn-container').data('data'),
            a = {
                TokenCaptcha: _U.CaptchaToken(),
                DNI: _C.txtDNI.val(),
                Nombres: _C.txtNombres.val(),
                ApePaterno: _C.txtApellidoP.val(),
                ApeMaterno: _C.txtApellidoM.val(),
                NroCeular: _C.txtPhone.val(),
                Correo: _C.txtEmail.val(),
                EsTitular: _C.rdTitularSi.prop('checked'),
                IdNroServicio: d.IdNroservicio,
                PFactura: +d.PFactura,
                ConsumoKWH: +d.ConsumoKWH
            };

        return a;
    }

    function ValidSave() {
        _C.txtDNI.next().hide();
        _C.txtPhone.next().hide();
        _C.rdTitularSi.closest('.row').find('> span').hide();

        var r = true;
        r &= _C.txtDNI[0].checkValidity();
        r &= _C.txtNombres[0].checkValidity();
        r &= _C.txtApellidoP[0].checkValidity();
        r &= _C.txtApellidoM[0].checkValidity();

        if (_C.txtEmail.val().length > 0) {
            if (!_C.txtEmail[0].checkValidity()) {
                r = false;
                _C.txtEmail.next().html(_C.txtEmail[0].validationMessage).show();
            }
        }

        if (_C.txtDNI.val().length < 8) {
            r = false;
            _C.txtDNI.next().html('DNI incompleto').show();
        }

        if (isNaN(_C.txtDNI.val())) {
            r = false;
            _C.txtDNI.next().html('DNI no es valido, ingrese solo números').show();
        }

        if (_C.txtPhone.val().length < 9) {
            r = false;
            _C.txtPhone.next().show();
        }

        if (isNaN(_C.txtPhone.val())) {
            r = false;
            _C.txtPhone.next().html('Celular no es valido, ingrese solo números').show();
        }

        if (!_C.rdTitularSi.prop('checked') && !_C.rdTitularNo.prop('checked')) {
            r = false;
            _C.rdTitularSi.closest('.row').find('> span').show();
        }

        if (!r) _C.frmUpdate.addClass('was-validated');

        return r;
    }

    function Loading(sender, isShowPage) {
        if (+isShowPage == 1) {
            sender.prop('disabled', false).addClass('btn-success');
            $('#modPreload').modal('hide');
        } else {
            sender.prop('disabled', true).removeClass('btn-success');
            $('#modPreload').modal('show');
        }
    }

    //#endregion Private

    //#region Public

    (function () { Load(); }());

    //#endregion Public
}(window.nsUtil.SetNameSpace('Root', 'Suministro', 'Beneficiario'), jQuery));