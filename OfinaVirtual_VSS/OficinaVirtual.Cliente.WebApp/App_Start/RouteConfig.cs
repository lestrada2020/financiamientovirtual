﻿using System.Web.Mvc;
using System.Web.Routing;

namespace OficinaVirtual.Cliente.WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                // DISTRILUZ 036 - INICIO FINANCIAMIENTO VIRTUAL
                url: "{controller}/{action}/{id}/{sender}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional, sender = UrlParameter.Optional }
                // DISTRILUZ 036 - FIN FINANCIAMIENTO VIRTUAL
            );
        }
    }
}