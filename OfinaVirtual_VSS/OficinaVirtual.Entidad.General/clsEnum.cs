﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OficinaVirtual.Entidad.General
{
    public enum TypeAplicacion
    {
        Default = 0,
        Proyectos = 1
    }

    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL

    public enum TipoDocumento
    {
        InteresGenerado = 2,
        InteresGeneradoJuridico = 3
    }
    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL

}
