﻿using System;
using System.Collections.Generic;
using System.Data;

namespace OficinaVirtual.Entidad.General
{
    public class clsBase
    {
        public String Token { get; set; }

        public Int32 Tried { get; set; }

        public Int16 IdAplicacion { get; set; }
    }

    public class clsParametro : clsBase
    {
        public Dictionary<String, String> Parametros { get; set; }
    }

    public class clsResultado : clsBase
    {
        #region Properties

        public Int32 IdError { get; set; }

        public String Mensaje { get; set; }

        public Object Datos { get; set; }

        #endregion Properties

        #region Public

        public void SetError(Int32 idError, String message)
        {
            IdError = idError;
            Mensaje = message;
        }

        #endregion Public
    }

    //DISTRILUZ 036 INICIO FINANCIAMIENTO VIRTUAL
    #region Distriluz-036 MCastro Financiamiento
    public class ClsResultadoExt : clsBase
    {
        #region Properties
        public int IdError { get; set; }
        public string Mensaje { get; set; }
        #endregion Properties
    }

    #endregion

    //DISTRILUZ 036 FIN FINANCIAMIENTO VIRTUAL

    public class clsUsuario : clsResultado
    {
        #region Properties

        public Int32 IdUsuario { get; set; }

        public String Usuario { get; set; }

        public string Nombre { get; set; }

        public string NombreProveedor { get; set; }

        public DateTime FechaUltimoAcceso { get; set; }

        public Int16 IdProveedor { get; set; }

        public Int16 IdUUNN { get; set; }

        public string NombreUnidadNegocio { get; set; }

        public Int16 IdEmpresa { get; set; }

        public string NombreEmpresa { get; set; }

        public int IdTipoProveedor { get; set; }

        public string TipoProveedor { get; set; }
        //Extra

        public List<clsAplicacion> Aplicaciones { get; set; }

        public List<clsRol> Roles { get; set; }

        public List<clsOpcion> Opciones { get; set; }

        public Int32 IdAplicacionActivo { get; set; }

        public String URL { get; set; }

        #endregion Properties

        #region Constructors

        public clsUsuario()
        {
        }

        public clsUsuario(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdUsuario") && !row.IsNull("IdUsuario")) IdUsuario = Convert.ToInt32(row["IdUsuario"]);
            if (columns.Contains("Usuario") && !row.IsNull("Usuario")) Usuario = row["Usuario"].ToString();
            if (columns.Contains("Nombre") && !row.IsNull("Nombre")) Nombre = row["Nombre"].ToString();
            if (columns.Contains("NombreProveedor") && !row.IsNull("NombreProveedor")) NombreProveedor = row["NombreProveedor"].ToString();
            if (columns.Contains("FechaUltimoAcceso") && !row.IsNull("FechaUltimoAcceso")) FechaUltimoAcceso = Convert.ToDateTime(row["FechaUltimoAcceso"]);
            if (columns.Contains("IdProveedor") && !row.IsNull("IdProveedor")) IdProveedor = Convert.ToInt16(row["IdProveedor"]);
            if (columns.Contains("IdUUNN") && !row.IsNull("IdUUNN")) IdUUNN = Convert.ToInt16(row["IdUUNN"]);
            if (columns.Contains("UnidadNegocio") && !row.IsNull("UnidadNegocio")) NombreUnidadNegocio = Convert.ToString(row["UnidadNegocio"]);
            if (columns.Contains("IdEmpresa") && !row.IsNull("IdEmpresa")) IdEmpresa = Convert.ToInt16(row["IdEmpresa"]);
            if (columns.Contains("Empresa") && !row.IsNull("Empresa")) NombreEmpresa = Convert.ToString(row["Empresa"]);
            if (columns.Contains("IdTipoProveedor") && !row.IsNull("IdTipoProveedor")) IdTipoProveedor = Convert.ToInt16(row["IdTipoProveedor"]);
            if (columns.Contains("TipoProveedor") && !row.IsNull("TipoProveedor")) TipoProveedor = Convert.ToString(row["TipoProveedor"]);
            if (columns.Contains("URL") && !row.IsNull("URL")) URL = Convert.ToString(row["URL"]);
        }

        #endregion Constructors
    }

    public class clsAplicacion
    {
        #region Properties

        public Int16 IdAplicacion { get; set; }

        public String Nombre { get; set; }

        public String Descripcion { get; set; }

        public String Directorio { get; set; }

        public String FotoPerfil { get; set; }

        public String FotoPortada { get; set; }

        public Int16 IdEstado { get; set; }

        #endregion Properties

        #region Constructors

        public clsAplicacion()
        {
        }

        public clsAplicacion(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdAplicacion") && !row.IsNull("IdAplicacion")) IdAplicacion = Convert.ToInt16(row["IdAplicacion"]);
            if (columns.Contains("Nombre") && !row.IsNull("Nombre")) Nombre = row["Nombre"].ToString();
            if (columns.Contains("Descripcion") && !row.IsNull("Descripcion")) Descripcion = row["Descripcion"].ToString();
            if (columns.Contains("Directorio") && !row.IsNull("Directorio")) Directorio = row["Directorio"].ToString();
            if (columns.Contains("FotoPerfil") && !row.IsNull("FotoPerfil")) FotoPerfil = row["FotoPerfil"].ToString();
            if (columns.Contains("FotoPortada") && !row.IsNull("FotoPortada")) FotoPortada = row["FotoPortada"].ToString();
            if (columns.Contains("IdEstado") && !row.IsNull("IdEstado")) IdEstado = Convert.ToInt16(row["IdEstado"]);
        }

        #endregion Constructors
    }

    public class clsOpcion
    {
        #region Properties

        public Int32 IdOpcion { get; set; }

        public Int32? IdOpcionPadre { get; set; }

        public Int16 IdAplicacion { get; set; }

        public String Nombre { get; set; }

        public String Directorio { get; set; }

        public String Icono { get; set; }

        public Int16 IdEstado { get; set; }

        #endregion Properties

        #region Constructors

        public clsOpcion()
        {
        }

        public clsOpcion(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdOpcion") && !row.IsNull("IdOpcion")) IdOpcion = Convert.ToInt32(row["IdOpcion"]);
            if (columns.Contains("IdOpcionPadre") && !row.IsNull("IdOpcionPadre")) IdOpcionPadre = Convert.ToInt32(row["IdOpcionPadre"]);
            if (columns.Contains("IdAplicacion") && !row.IsNull("IdAplicacion")) IdAplicacion = Convert.ToInt16(row["IdAplicacion"]);
            if (columns.Contains("Nombre") && !row.IsNull("Nombre")) Nombre = row["Nombre"].ToString();
            if (columns.Contains("Directorio") && !row.IsNull("Directorio")) Directorio = row["Directorio"].ToString();
            if (columns.Contains("Icono") && !row.IsNull("Icono")) Icono = row["Icono"].ToString();
            if (columns.Contains("IdEstado") && !row.IsNull("IdEstado")) IdEstado = Convert.ToInt16(row["IdEstado"]);
        }

        #endregion Constructors
    }

    public class clsRol
    {
        #region Properties

        public Int32 IdRol { get; set; }

        public Int16 IdAplicacion { get; set; }

        public String Nombre { get; set; }

        public Int16 IdEstado { get; set; }

        #endregion Properties

        #region Constructors

        public clsRol()
        {
        }

        public clsRol(DataRow row)
        {
            if (row == null) return;

            DataColumnCollection columns = row.Table.Columns;

            if (columns.Contains("IdRol") && !row.IsNull("IdRol")) IdRol = Convert.ToInt32(row["IdRol"]);
            if (columns.Contains("IdAplicacion") && !row.IsNull("IdAplicacion")) IdAplicacion = Convert.ToInt16(row["IdAplicacion"]);
            if (columns.Contains("Nombre") && !row.IsNull("Nombre")) Nombre = row["Nombre"].ToString();
            if (columns.Contains("IdEstado") && !row.IsNull("IdEstado")) IdEstado = Convert.ToInt16(row["IdEstado"]);
        }

        #endregion Constructors
    }

    public class clsDataTablePag : clsParametro
    {
        public Int32 Draw { get; set; }

        public Int32 Start { get; set; }

        public Int32 Length { get; set; }

        public Dictionary<String, String> Search { get; set; }

        public List<clsDataTablePagOrder> Order { get; set; }
    }

    public class clsDataTablePagOrder
    {
        public Int32 Column { get; set; }

        public String Dir { get; set; }
    }

    public class clsDataTablePagResult<T> : clsResultado
    {
        #region Properties

        public Int32 draw { get; set; }

        public List<T> Data { get; set; }

        public Int32 recordsTotal { get; set; }

        public Int32 recordsFiltered { get; set; }

        #endregion Properties

        #region Constructors

        public clsDataTablePagResult()
        {
            Data = new List<T>();
        }

        public clsDataTablePagResult(DataSet set)
        {
            var data = set.Tables[0];
            var info = set.Tables[1];

            Data = new List<T>();

            if (data == null || info == null || info.Rows.Count == 0) return;

            foreach (var row in data.Rows) Data.Add((T)Activator.CreateInstance(typeof(T), row));

            DataRow r = info.Rows[0];

            if (info.Columns.Contains("Total") && !r.IsNull("Total")) recordsTotal = Convert.ToInt32(r["Total"]);
            if (info.Columns.Contains("Filter") && !r.IsNull("Filter")) recordsFiltered = Convert.ToInt32(r["Filter"]);
        }

        #endregion Constructors
    }
}