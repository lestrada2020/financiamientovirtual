﻿using OficinaVirtual.Entidad.General;
using OficinaVirtual.Util.Tool;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace OficinaVirtual.Fachada.General
{
    public sealed class clsFProyectos
    {
        #region Field

        private static readonly String _ServiceAD = ConfigurationManager.AppSettings["WSAD"] + "ActiveDirectory/";
        private static readonly String _ServiceProy = ConfigurationManager.AppSettings["WSGeneral"] + "Proyectos/";
        private static readonly clsService _Service = clsService.Instancia;
        private static readonly Lazy<clsFProyectos> _Instancia = new Lazy<clsFProyectos>(() => new clsFProyectos(), LazyThreadSafetyMode.PublicationOnly);

        #endregion Field

        #region Property

        public static clsFProyectos Instancia
        {
            get { return _Instancia.Value; }
        }

        #endregion Property

        #region Constructor

        private clsFProyectos()
        {
        }

        #endregion Constructor

        public async Task<clsResultado> ValidaUserAD(Dictionary<String, String> param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceAD}ValidarDato", param, null, true);
        }

        public async Task<clsResultado> AyudaLista(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}AyudaLista", param);
        }

        public async Task<clsResultado> ListIniciativas(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ListIniciativas", param);
        }

        public async Task<clsResultado> ObtenerNivelesFase(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ObtenerNivelesFase", param);
        }

        public async Task<clsResultado> ObtenerUsuario(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ObtenerUsuario", param);
        }

        public async Task<clsResultado> ObtenerIniciativa(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ObtenerIniciativa", param);
        }

        public async Task<clsResultado> RegistrarHito(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}RegistrarHito", param);
        }

        public async Task<clsResultado> ObtenerHito(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ObtenerHito", param);
        }

        public async Task<clsResultado> ExportarIniciativa(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ExportarIniciativa", param);
        }

        public async Task<clsResultado> ExportarImporte(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ExportarImporte", param);
        }

        public async Task<clsResultado> RegistrarImporte(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}RegistrarImporte", param);
        }

        public async Task<clsResultado> ObtenerImporte(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}ObtenerImporte", param);
        }

        public async Task<clsResultado> EditarIniciativa(clsParametro param)
        {
            return await _Service.PostAsync<clsResultado>($"{_ServiceProy}EditarIniciativa", param);
        }
    }
}